function [step_size, objective, alpha, F] = find_optimal_stepsize( ...
    Dij, w, w_direction, d_min, d_max, q_under, q_over, first_guess, ...
    max_step_size, max_iterations)
% find_optimal_stepsize  Find optimal stepsize for IMRT optimization
%
% [step_size, objective] = find_optimal_stepsize( ...
%    Dij, w, w_direction, d_min, d_max, q_under, q_over, first_guess, ...
%    max_step_size)
%
%  For more verbose output:
%    [step_size, objective, alpha_hist, F_hist] = find_optimal_stepsize(...);
%    plot(alpha,F,':',alpha,F,'x',step_size,F_hist,'o');
%    title('Finding the optimal alpha');
%    ylabel('Objective(\alpha)');
%    xlabel('\alpha');

drawnow
 

if(~exist('max_iterations'))
    max_iterations = 5;
end
iterations = -2;


alpha_min = 0;
F_alpha_min = calculate_objective(Dij*w, d_min, d_max, q_under, q_over);



alpha = alpha_min;
F = F_alpha_min;

alpha_temp = first_guess;
% Calculate F(alpha_temp)
d_temp = Dij * max(0,(w + alpha_temp * w_direction));
F_alpha_temp = calculate_objective(d_temp, d_min, d_max, q_under, q_over);

alpha(end+1) = alpha_temp;
F(end+1) = F_alpha_temp;

if(F_alpha_temp > F_alpha_min)
    % The alpha_max is valid, so we need to find alpha_mid
    alpha_max = alpha_temp;
    F_alpha_max = F_alpha_temp;

    alpha_mid = mean([alpha_min, alpha_max]);
    % Calculate F(alpha_mid)
    d_temp = Dij * max(0,(w + alpha_mid * w_direction));
    F_alpha_mid = calculate_objective(d_temp, d_min, d_max, q_under, q_over);

    alpha(end+1) = alpha_mid;
    F(end+1) = F_alpha_mid;

    while(F_alpha_mid > F_alpha_min & (iterations < max_iterations))
        iterations = iterations + 1;
        alpha_max = alpha_mid;
        F_alpha_max = F_alpha_mid;

        alpha_mid = mean([alpha_min, alpha_max]);
        % Calculate F(alpha_temp)
        d_temp = Dij * max(0,(w + alpha_mid * w_direction));
        F_alpha_mid = calculate_objective(d_temp, d_min, d_max, q_under, q_over);

        alpha(end+1) = alpha_mid;
        F(end+1) = F_alpha_mid;
    end

else
    % We need to find alpha_max
    alpha_mid = alpha_temp;
    F_alpha_mid = F_alpha_temp;

    alpha_max = min(max_step_size, 2 * alpha_mid);
    % Calculate F(alpha_max)
    d_temp = Dij * max(0,(w + alpha_max * w_direction));
    F_alpha_max = calculate_objective(d_temp, d_min, d_max, q_under, q_over);

    alpha(end+1) = alpha_max;
    F(end+1) = F_alpha_max;

    while((F_alpha_mid > F_alpha_max) & (iterations < max_iterations))
        iterations = iterations + 1;
        alpha_min = alpha_mid;
        F_alpha_min = F_alpha_mid;
        alpha_mid = alpha_max;
        F_alpha_mid = F_alpha_max;

	alpha_max = min(max_step_size, 2 * alpha_mid);
        % Calculate F(alpha_max)
        d_temp = Dij * max(0,(w + alpha_max * w_direction));
        F_alpha_max = calculate_objective(d_temp, d_min, d_max, q_under, q_over);

        alpha(end+1) = alpha_max;
        F(end+1) = F_alpha_max;
    end
end

% The following conditions are now true:
% alpha_min < alpha_mid < alpha_max
% F_alpha_min > F_alpha_mid
% F_alpha_max > F_alpha_mid

if(alpha_mid > alpha_max || alpha_mid < alpha_min)
    alpha_min
    alpha_mid
    alpha_max
    error('Something is wrong and the alphas are out of order');
end

if(F_alpha_mid > F_alpha_max || F_alpha_mid > F_alpha_min)
    warning(['Unable to properly initialize line-minimization,', ...
            ' F_min = ', num2str(F_alpha_min), ...
            ' F_mid = ', num2str(F_alpha_mid), ...
            ' F_max = ', num2str(F_alpha_max)]);
end


while(iterations < max_iterations)
    iterations = iterations + 1;

    % Place alpha_temp at the minimum of the quadratic polynomial that goes
    % through alpha_min, alpha_mid, and alpha_max
    % (from _Nonlinear Programming_ by Dimitry P. Bertsekas, pg. 725)
    alpha_temp = 0.5 * (F_alpha_min * (alpha_max^2 - alpha_mid^2) ...
        + F_alpha_mid * (alpha_min^2 - alpha_max^2) ...
        + F_alpha_max * (alpha_mid^2 - alpha_min^2)) ...
        / (F_alpha_min * (alpha_max - alpha_mid) ...
        + F_alpha_mid * (alpha_min - alpha_max) ...
        + F_alpha_max * (alpha_mid - alpha_min));

    if(alpha_temp >= alpha_max || alpha_temp <= alpha_min ...
            || alpha_temp == alpha_mid)
        % The algorithm has broken down, so go with the best alpha found
        % thus far.  This typically is a rounding error in the above
        % function when alpha_min is very close to either alpha_max or
        % alpha_min.
        break;
    end

    % Calculate F(alpha_temp)
    d_temp = Dij * max(0,(w + alpha_temp * w_direction));
    F_alpha_temp = calculate_objective(d_temp, d_min, d_max, q_under, q_over);

    alpha(end+1) = alpha_temp;
    F(end+1) = F_alpha_temp;

    if(alpha_temp > alpha_mid)
        if(F_alpha_temp < F_alpha_mid)
            alpha_min = alpha_mid;
            F_alpha_min = F_alpha_mid;

            alpha_mid = alpha_temp;
            F_alpha_mid = F_alpha_temp;
        elseif(F_alpha_temp > F_alpha_mid)
            alpha_max = alpha_temp;
            F_alpha_max = F_alpha_temp;
        else
            disp('(F_alpha_temp == F_alpha_mid)');
            break;
        end
    elseif(alpha_temp < alpha_mid)
        if(F_alpha_temp < F_alpha_mid)
            alpha_max = alpha_mid;
            F_alpha_max = F_alpha_mid;

            alpha_mid = alpha_temp;
            F_alpha_mid = F_alpha_temp;
        elseif(F_alpha_temp > F_alpha_mid)
            alpha_min = alpha_temp;
            F_alpha_min = F_alpha_temp;
        else
            disp('(F_alpha_temp == F_alpha_mid)');
            break;
        end
    else
        disp('alpha_temp == alpha_mid');
        break;
    end
end

step_size = alpha_mid;
objective = F_alpha_mid;

global DEBUG;
if(DEBUG)
    for(alpha_temp = [1:10] * max(alpha) / 10)
        % Calculate F(alpha_temp)
        d_temp = Dij * max(0,(w + alpha_temp * w_direction));
        F_alpha_temp = calculate_objective(d_temp, d_min, d_max, q_under, q_over);

        alpha(end+1) = alpha_temp;
        F(end+1) = F_alpha_temp;
    end
end


   
return
