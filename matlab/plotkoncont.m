function [curve,oarname,posout]=plotkoncont(oar,slice,xfactor,xoff,yoff,VDXfile,line,lw,fpos)
%   Function PLOTKONCONT: plot Konrad contour using the VDX data
%                   Usage: plotkoncont(oar,pos_z,CTatts,VDXatts)
%                           oar == oar number
%                           slice == VDX slice number 
%                           xfactor == CT pixel size / 16
%                           xoff,yoff == offsets between the CT and VDX files
%                           slice == VDX slice number 
%                           VDXfile == VDX file name
%                           line == linestyle
%                           lw = line width

if nargin<9
    fpos = 0;
end
[curve,oarname,posout] = read_vdx(VDXfile,oar,slice,fpos);

if ~isempty(curve) 
curve(:,2) = 512*16+1-curve(:,2);
plot(curve(:,1)*xfactor-xoff,curve(:,2)*xfactor-yoff,line,'LineWidth',lw)
end % plotting