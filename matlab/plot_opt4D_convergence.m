function plot_opt4D_convergence(plan_file, file_prefix, ...
legends, discard_estimated_objectives, average_repeated_runs, ...
dont_plot_multi_objective)
%plot_opt4D_convergence Plots the objective history for an opt4D optimization.
%   This is useful for understanding how the various algorithms are running.
%   Also plots the multi-objective history for each optimization if
%   appropriate.
%
% plot_opt4D_convergence(plan, file_prefix) Plots the convergence for the
%   given plan and out file prefix.  The file_prefix can be a cell array
%   of strings.  If the file_prefix refers to a repeated run of opt4D, the
%   various runs will be plotted together with a single legend line.
%
% plot_opt4D_convergence(plan, file_prefix, legends) Allows the user to
%   override the default legends.
%
% plot_opt4D_convergence(plan, file_prefix, legends, ...
%   discard_estimated_objectives) Doesn't show the estimated objectives
%   along with the true objectives.
%
% plot_opt4D_convergence(plan, file_prefix, legends, ...
%   discard_estimated_objectives, average_repeated_runs) Shows the average
%   of the repeated runs instead of showing all the lines.
%
% See also read_opt4D_convergence, plot_opt4D_sampling_history.

% Call up a dialog to choose a plan file if it is not included already.
if(~exist('plan_file'))
    plan_file = ui_get_plan_file_name;
end

% Read plan from file if a file name was given
if(isstr(plan_file))
    plan = read_pln(plan_file);
else
    plan = plan_file;
end

% Make sure plan has all required parts
if(isstruct(plan))
    if(~isfield(plan,'title'))
	plan.title = '';
    end
    % if( || ~isfield(plan,'vois'))
	% error('plan is not a valid structure.');
    % end
else
    error('plan is not a structure.');
end

% Make file_prefix into a cell array
if(~iscell(file_prefix))
    file_prefix = {file_prefix};
end

% Set default discard_estimated_objectives
if(~exist('discard_estimated_objectives') || ...
        isempty(discard_estimated_objectives))
    discard_estimated_objectives = false;
end

% Set default average_repeated_runs
if(~exist('average_repeated_runs') || isempty(average_repeated_runs))
    average_repeated_runs = false;
end

% Set default dont_plot_multi_objective
if(~exist('dont_plot_multi_objective') || isempty(dont_plot_multi_objective))
    dont_plot_multi_objective = false;
end

% Set default multi_run_mapping
[multi_run_mapping, single_file_prefix] = find_multirun_mapping(file_prefix);

% Setup reverse mapping
nGroups = numel(file_prefix);
reverse_mapping = zeros(nGroups,1);
for(i = 1:nGroups)
    if(isempty(find(multi_run_mapping == i)))
        error('Missing group in mapping for some reason.');
    else
        reverse_mapping(i) = min(find(multi_run_mapping(:)==i));
    end
end


% Figure out the legends if needed
if(~exist('legends') || isempty(legends))
    legends = cell(nGroups,1);
    for(iGroup = 1:numel(legends))
        [pathstr,name,ext] = fileparts(file_prefix(iGroup));
	legends{iGroup} = strrep([name ext], '_', ' ');
    end
end

% Read files
[obj_hist, time_hist, false_obj_hist, multi_obj_hist, false_multi_obj_hist, ...
objective_names] = read_opt4D_convergence(single_file_prefix);

% Pad data
while(numel(multi_obj_hist) < size(obj_hist,2))
    multi_obj_hist{end+1} = [];
end
while(numel(false_multi_obj_hist) < size(obj_hist,2))
    false_multi_obj_hist{end+1} = [];
end
while(numel(objective_names) < size(obj_hist,2))
    objective_names{end+1} = {};
end

% Average repeated runs if needed
if(average_repeated_runs)
    % Calculate averages
    for(iGroup = 1:nGroups)
        if(sum(multi_run_mapping(:) == iGroup) > 1)
            % Multiple runs present
            obj_hist(:,reverse_mapping(iGroup)) ...
                = mean(obj_hist(:,multi_run_mapping(:) == iGroup),2);
            time_hist(:,reverse_mapping(iGroup)) ...
                = mean(time_hist(:,multi_run_mapping(:) == iGroup),2);
            false_obj_hist(:,reverse_mapping(iGroup)) ...
                = mean(false_obj_hist(:,multi_run_mapping(:) == iGroup),2);
            multi_obj_hist{reverse_mapping(iGroup)} = ...
                array_mean(multi_obj_hist(multi_run_mapping(:) == iGroup),nan);
            false_multi_obj_hist{reverse_mapping(iGroup)} = ...
                array_mean(false_multi_obj_hist(multi_run_mapping(:) == ...
                iGroup),nan);
        end
    end

    % Rearrange data
    obj_hist = obj_hist(:,reverse_mapping);
    time_hist = time_hist(:,reverse_mapping);
    false_obj_hist = false_obj_hist(:,reverse_mapping);
    multi_obj_hist = multi_obj_hist(reverse_mapping);
    false_multi_obj_hist = false_multi_obj_hist(reverse_mapping);
    objective_names = objective_names(reverse_mapping);

    % Rewrite mapping
    multi_run_mapping = 1:nGroups;
    reverse_mapping = 1:nGroups;
end


% Prepare colormap
cmap = lines(nGroups);

% Plot objective history
if(any(isfinite(obj_hist(:))))

    figure;
    handles = semilogy(obj_hist);
    for(iGroup = 1:nGroups)
        set(handles(multi_run_mapping == iGroup), 'color', cmap(iGroup,:));
    end
    xlabel('Optimization step');
    ylabel('Objective Function');
    title(['Objective history for ' plan.title]);
    if(numel(single_file_prefix) > 1)
	legend(handles(reverse_mapping), legends);
    end
    if(~discard_estimated_objectives && any(isfinite(false_obj_hist(:))))
        hold on;
        handles = semilogy(false_obj_hist, '.');
        for(iGroup = 1:nGroups)
            set(handles(multi_run_mapping == iGroup), 'color', cmap(iGroup,:));
        end
        hold off;
    end

    if(any(isfinite(time_hist(:))))
        figure;
	handles = semilogy(time_hist,obj_hist);
        for(iGroup = 1:nGroups)
            set(handles(multi_run_mapping == iGroup), 'color', cmap(iGroup,:));
        end
        xlabel('Elapsed time (s)');
        ylabel('Objective Function');
	title(['Objective history vs. time for ' plan.title]);
        if(numel(single_file_prefix) > 1)
	    legend(handles(reverse_mapping), legends);
	end
        if(~discard_estimated_objectives && any(isfinite(false_obj_hist(:))))
	    hold on;
            handles = semilogy(time_hist, false_obj_hist, '.');
            for(iGroup = 1:nGroups)
                set(handles(multi_run_mapping == iGroup), 'color',...
                    cmap(iGroup,:));
            end
	    hold off;
	end
    end
end

if(~dont_plot_multi_objective)
    % Plot multi objective histories

    for(iGroup = 1:nGroups)
        % first figure out if group has any members with multi-objective
        % histories
        skip = true;
        runs_in_group = find(multi_run_mapping(:) == iGroup)';
        for(iRun = runs_in_group)
            if(numel(multi_obj_hist) >= iRun && ...
                any(isfinite(multi_obj_hist{iRun}(:))))
                skip = false;
            end
        end
        if(skip)
            continue
        end


        % Now we know that at least one member of the group has a
        % multi-objective history
        first = true;
        figure('name',['Multi_obj: ' legends{iGroup}]);
        for(iRun = runs_in_group)
            if(numel(multi_obj_hist) >= iRun || ...
                    any(isfinite(multi_obj_hist{iRun}(:))))
                if(first && ~isempty(multi_obj_hist{iRun}))
                    [handles] = semilogy(multi_obj_hist{iRun});
                    hold on;
                    first = false;
                elseif(~isempty(multi_obj_hist{iRun}))
                    semilogy(multi_obj_hist{iRun});
                end
            end
            if(~discard_estimated_objectives ...
                    && numel(false_multi_obj_hist) >= iRun...
                    && any(isfinite(false_multi_obj_hist{iRun}(:))))
                if(~isempty(false_multi_obj_hist{iRun}))
                    semilogy(false_multi_obj_hist{iRun}, '.');
                end
            end
        end
        legend(handles, objective_names{reverse_mapping(iGroup)});
        xlabel('Optimization step');
        ylabel('Objective Function');
        if(numel(single_file_prefix) > 1)
            title(legends{iGroup});
        else
            title(['Multi Objective history for ' plan.title]);
        end
    end
end

return

% ---------------------------
function [a] = array_mean(arrays, default_value);

nArrays = numel(arrays);

% Find the maximum size
max_size = [0 0];
for(iArray = 1:nArrays)
    max_size = max(max_size, size(arrays{iArray}));
end

a = zeros(max_size);

% Pad arrays as needed
for(iArray = 1:nArrays)
    temp_size = size(arrays{iArray});
    if(any(temp_size < max_size))
        % Padding needed
        a = a + [arrays{iArray}, repmat(temp_size(1), ...
            max_size(2) - temp_size(2), default_value); ...
            repmat(max_size(1) - temp_size(1), max_size(2), default_value)];
    else
        a = a + arrays{iArray};
    end
end

a = a ./ nArrays;

return
