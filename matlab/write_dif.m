function write_dif(dif_file, dif)
%write_dif Writes Konrad dimention information file.
%
%   write_dif(dif_file, dif) Writes the dif file.
%
%   The dif format is based on the Konrad geometry, so x, y, and z correspond
%   to column, slice, and row in the CERR geometry, respectively.
%
%     .dif file        dif structure
%
%   Delta-X ----------- dif.dX
%   Delta-Y ----------- dif.dY
%   Delta-Z ----------- dif.dZ
%   Dimension-CT-X ---- dif.ct_nX
%   Dimension-CT-Y ---- dif.ct_nY
%   Dimension-CT-Z ---- dif.ct_nZ
%   Dimension-Dose-X -- dif.nX
%   Dimension-Dose-Y -- dif.nY
%   Dimension-Dose-Z -- dif.nZ
%   ISO-Index-CT-X ---- dif.ct_isoX
%   ISO-Index-CT-Y ---- dif.ct_isoY
%   ISO-Index-CT-Z ---- dif.ct_isoZ
%   ISO-Index-Dose-X -- dif.isoX
%   ISO-Index-Dose-Y -- dif.isoY
%   ISO-Index-Dose-Z -- dif.isoZ


% First open the file for writing
fid = fopen(dif_file,'wt');

fprintf(fid, 'Delta-X %d\n', dif.dX);
fprintf(fid, 'Delta-Y %d\n', dif.dY);
fprintf(fid, 'Delta-Z %d\n', dif.dZ);
fprintf(fid, 'Dimension-CT-X %d\n', dif.ct_nX);
fprintf(fid, 'Dimension-CT-Y %d\n', dif.ct_nY);
fprintf(fid, 'Dimension-CT-Z %d\n', dif.ct_nZ);
fprintf(fid, 'Dimension-Dose-X %d\n', dif.nX);
fprintf(fid, 'Dimension-Dose-Y %d\n', dif.nY);
fprintf(fid, 'Dimension-Dose-Z %d\n', dif.nZ);
fprintf(fid, 'ISO-Index-CT-X %d\n', dif.ct_isoX);
fprintf(fid, 'ISO-Index-CT-Y %d\n', dif.ct_isoY);
fprintf(fid, 'ISO-Index-CT-Z %d\n', dif.ct_isoZ);
fprintf(fid, 'ISO-Index-Dose-X %d\n', dif.isoX);
fprintf(fid, 'ISO-Index-Dose-Y %d\n', dif.isoY);
fprintf(fid, 'ISO-Index-Dose-Z %d\n', dif.isoZ);

fclose(fid);
