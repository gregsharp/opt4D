function flag = write_moving_RTOG_phantom(outFile,nBeams,nInstances)
% writeRtogPhantom  Writes Konrad formated files for a phantom
%     [flag] = writeRtogPhantom(outFile, nBeams, nInstances)
%     writes out Konrad files for a moving phantom.
%     Creates .dij, .stf, .bwf, .dif, .voi, and .voi2 files.
%
%     IM is the data structure with information about the problem.
%
%     outFile is the path and name for the Konrad files.
%
%     structNums is an array of structure numbers to include in the Konrad
%     files.  Any voxels outside of the structures will be assumed to be air.
%     The first voxels will take priority.  Use structNums = [] to include all
%     structures that have a goal defined.
%
%     beamWeights is an array containing beam weights to use for the start of
%     optimization.  This should be a vector the same length as the number of
%     beamlets.  Use beamWeights = [] to set all weights to 1.
%
%     beamletThreshold is a value between 0 and 1 to only allow beamlets with at
%     least one Dij entry greater than beamletThreshold times the maximum Dij
%     entry.
%
%     flag returns 'error' or 'no error'.
%
%     For example,
%
%         [flag] = writeKonradFiles(IM, 'default')
%
%     creates files named default.voi, default.dif, default_1.dij,
%     default_2.dij, etc.
%
%     Notes:
%     * There are many non-essential values that were given arbitrary defaults.
%       This shouldn't be a problem for optimization, but better values are
%       obviously prefered.
%
%     * To avoid having to change the counting of the indexes of the voxels,
%       this function currently just assigns all of the voxels removed in
%       downsampling to be air.  Run showVoi to see the effect.  This
%       shouldn't be a huge deal since everthing other than the voi is in a
%       sparse format, but it is better avoided.
%
%     * Note that writing out multiple beams will only work if the
%       IM.beams(beam).beamNumber is set correctly.


% ----------------------------------------------------------------- %
% Make sure the inputs to the function are correct.

% Both the output file prefix and the number of beams are required
if(~exist('outFile') || ~exist('nBeams'))
    error('Both the output file prefix and the plan structure are required');
end

% Multiple instances has not been implemented yet.
if(~exist('nInstances'))
    nInstances = 1;
elseif(nInstances > 1)
%    error('multiple instances is not implemented yet');
end

% ------------------------------------------------------------------- %
% General plan description for creating plan file

% The RTOG phantom is based on the following geometry:
% tx1 = 1.3;              %  half axis of ellipse x, target inner
% tz1 = 1.3;              %                       z, target inner
% tx2 = 3.8;              %  half axis of ellipse x, target outer
% tz2 = 3.8;              %  half axis of ellipse z, target outer
% 
% rx = 1.0;               %  half axis of ellipse x, OAR
% rz = 1.0;               %                       z, OAR

Plan.title       = 'RTOG Phantom';
Plan.description = 'RTOG Phantom';
Plan.nInstances  = 1;%nInstances;
Plan.nBeams      = nBeams;
Plan.file_root   = outFile;
Plan.params = {'ctatts_file_name','DVH_bin_size'};
Plan.values = {'/home/bmartin/cerr_ex3/cwg_plan1_7beams.ctatts','0.2'};

Plan.vois(1).name = 'target';
Plan.vois(1).color = '1 0 0';
Plan.vois(1).is_target = 'true';
Plan.vois(1).max_DVH = '200 0%';
Plan.vois(1).min_DVH = '200 100%';
Plan.vois(1).weight_over = '1';
Plan.vois(1).weight_under = '1';
Plan.vois(1).geometry = ['(z >= 0)' ...
                         '&((x.^2)/(3.8^2) + (z.^2)/(3.8^2) <= 1.0)' ...
                         '& ((x.^2)/(1.3^2) + (z.^2)/(1.3^2) >= 1.0)'];
Plan.vois(1).offset = ['[ ' num2str(0*[1:nInstances]) '; ' ...
                       num2str(0*[1:nInstances]) '; ' ...
                       num2str(2*(cos(pi*[1:nInstances]/nInstances)).^4) ']' ];

Plan.vois(2).name = 'organ at risk';
Plan.vois(2).color = '0 0 1';
Plan.vois(2).max_DVH = '120 0%';
Plan.vois(2).weight_over = '1';
Plan.vois(2).geometry = '(x.^2)/(1.0^2) + (z.^2)/(1.0^2) <= 1';

Plan.vois(3).name = 'undifferentiated patient';
Plan.vois(3).color = '.5 .5 .5';
Plan.vois(3).max_DVH = '120 15%';
Plan.vois(3).weight_over = '5';
Plan.vois(3).geometry = '(x.^2)/(7.0^2) + (z.^2)/(7.0^2) <= 1';


sizeX = 8;   sizeY = 1;  sizeZ = 8;
dX    = 0.2; dY   = 0.3; dZ    = 0.1;
write_moving_phantom(outFile, Plan, sizeX, sizeY, sizeZ, dX, dY, dZ);
