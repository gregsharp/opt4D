function writeDif(DijStruct, outFile)
% writeDif(DijStruct, outfile)
%
% Write the dimension information file based on the information passed in
% DijStruct.
%
% outFile is the output file name.
%
% Throws an error if file i/o fails

% Here's the dif file format:
% 
% The dimension information file (dif) is really straightforward.  It
% is a human readable file that tells the dimensions of the CT data and
% the dose cube.  It simply prints out the information in the order
% presented below.  All Konrad distances are measured in millimeters

% Open file for writing (unix style carriage returns)
[outfid, message] = fopen(outFile, 'w');
if ~isempty(message)
	error(['Error writing stf file: ' message]);
end

% Dose Cube Voxel size
fprintf(outfid,'%s %8.6f\n','Delta-X', DijStruct.voxeldC * 10); 
fprintf(outfid,'%s %8.6f\n','Delta-Y', DijStruct.voxeldS * 10);
fprintf(outfid,'%s %8.6f\n','Delta-Z', DijStruct.voxeldR * 10);

% CT Dimensions 
fprintf(outfid,'%s %d\n','Dimension-CT-X', DijStruct.ctNc);
fprintf(outfid,'%s %d\n','Dimension-CT-Y', DijStruct.ctNs);
fprintf(outfid,'%s %d\n','Dimension-CT-Z', DijStruct.ctNr);

% Dose Cube dimensions
fprintf(outfid,'%s %d\n','Dimension-Dose-X', DijStruct.doseCubeNc);
fprintf(outfid,'%s %d\n','Dimension-Dose-Y', DijStruct.doseCubeNs);
fprintf(outfid,'%s %d\n','Dimension-Dose-Z', DijStruct.doseCubeNr);

% Index of isocenter within the CT data
fprintf(outfid,'%s %d\n','ISO-Index-CT-X', DijStruct.ctIndexC);
fprintf(outfid,'%s %d\n','ISO-Index-CT-Y', DijStruct.ctIndexS);
fprintf(outfid,'%s %d\n','ISO-Index-CT-Z', DijStruct.ctIndexR);

% Index of isocenter within the dose cube
fprintf(outfid,'%s %d\n','ISO-Index-Dose-X', DijStruct.doseIsoIndexC);
fprintf(outfid,'%s %d\n','ISO-Index-Dose-Y', DijStruct.doseIsoIndexS);
fprintf(outfid,'%s %d\n','ISO-Index-Dose-Z', DijStruct.doseIsoIndexR);

% Close file
status = fclose(outfid);
if status ~= 0
	error(['Error writing dif file']);
end
