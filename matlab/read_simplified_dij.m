function [Dij, bixel_info] = read_simplified_dij(plan)
%read_simplified_dij Reads all of the dij files for a plan into a sparse matrix
%
%   [Dij, bixel_info] = read_simplified_dij(plan) Reads the dij files for the
%   plan and returns a sparse Dij matrix and a structure of arrays describing
%   each beamlet.  plan can be either the name of a plan file or a plan
%   structure (see read_pln).
%
%   bixel_info contains the following fields:
%     bixel_info.instanceNo is the beam that each beamlet comes from.
%     bixel_info.beamNo is the beam that each beamlet comes from.
%     bixel_info.energy is the energy of that beam
%     bixel_info.spotX and bixel_info.spotY are the position of the beamlet
%       within the beam (in cm)
% 
%   See also read_pln, read_dij.

% Read plan file if given as a file name
if(isstr(plan))
    plan = read_pln(plan);
end

dif = read_dif(plan.dif_file_name);
nVoxels = dif.nX * dif.nZ * dif.nY;

bixel_info = struct('instanceNo',[], 'beamNo',[], 'energy',[], ...
            'spotX',[], 'spotY',[]);
temp_voxel_No = [];
temp_bixel_No = [];
temp_value = [];
bixel_offset = 0;
for(iInstance = 1:plan.nInstances)
    h = waitbar(0, ['Reading instance ' num2str(iInstance)]);
    for(iBeam = 1:plan.nBeams)
        waitbar((iBeam-1)/plan.nBeams,h, ...
            ['Reading instance ' num2str(iInstance), ' beam ' num2str(iBeam)]);
        if(iInstance == 1)
	    dij_name = [plan.dij_file_root, ...
                    '_', num2str(iBeam), '.dij'];
        else
	    dij_name = [plan.dij_file_root, ...
                    '_', num2str(iInstance), ...
                    '_', num2str(iBeam), '.dij'];
        end
        DijStruct = read_dij(dij_name);
        bixel_info.instanceNo = [bixel_info.instanceNo; ...
                             iInstance * ones(DijStruct.numPencilBeams, 1)];
        bixel_info.beamNo = [bixel_info.beamNo; ...
                             iBeam * ones(DijStruct.numPencilBeams, 1)];
        bixel_info.energy = [bixel_info.energy; DijStruct.energy];
        bixel_info.spotX = [bixel_info.spotX; DijStruct.spotX];
        bixel_info.spotY = [bixel_info.spotY; DijStruct.spotY];
 
        nEntries = 0;
        for(iBixel = 1:DijStruct.numPencilBeams)
            nEntries = nEntries + length(DijStruct.index{iBixel});
        end
        
        t_offset = 0;
        t_bixel_No = zeros(nEntries,1);
        t_voxel_No = zeros(nEntries,1);
        t_value = zeros(nEntries,1);
        for(iBixel = 1:DijStruct.numPencilBeams)
            % waitbar((iBeam-1+iBixel/DijStruct.numPencilBeams)/plan.nBeams,...
            %  h);
            t_bixel_No(t_offset+1:t_offset+length(DijStruct.index{iBixel}))=...
                (iBixel + bixel_offset) * ones(size(DijStruct.index{iBixel}));
            t_voxel_No(t_offset+1:t_offset+length(DijStruct.index{iBixel}))=...
                DijStruct.index{iBixel};
            t_value(t_offset+1:t_offset+length(DijStruct.index{iBixel}))=...
                DijStruct.doseCoefficient * DijStruct.value{iBixel};
            t_offset = t_offset + length(DijStruct.index{iBixel});
        end
        bixel_offset = bixel_offset + DijStruct.numPencilBeams;
        temp_bixel_No = [temp_bixel_No; t_bixel_No];
        temp_voxel_No = [temp_voxel_No; t_voxel_No];
        temp_value = [temp_value; t_value];
    end
    close(h);
end

disp('Storing info in sparse Dij matrix');
Dij = sparse(temp_voxel_No, temp_bixel_No, temp_value, nVoxels, bixel_offset);
