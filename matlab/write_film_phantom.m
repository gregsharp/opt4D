function flag = write_film_phantom(file_root)
%write_film_phantom Creates Konrad files for a film phantom
%
%    write_film_phantom(file_root) creates 
%    Creates .dij, .stf, .bwf, .dif, .voi, and .voi2 files.
%
%     For example,
%
%         write_film_phantom('default')
%
%     creates files named default.pln default.voi, default.dif, default_1.dij,
%     default_2.dij, etc.
%
%     Notes:
%     * There are many non-essential values that were given arbitrary defaults.
%       This shouldn't be a problem for optimization, but better values are
%       obviously prefered.
%


% ----------------------------------------------------------------- %
% Make sure the inputs to the function are correct.

% Both the output file prefix and the number of beams are required
if(~exist('file_root'))
    error('The output file prefix is required');
end

% ------------------------------------------------------------------- %
% General plan description for creating plan file

Plan.title       = 'Rectangle Phantom';
Plan.description = 'Rectangle Phantom';
Plan.nInstances  = 1;
Plan.nBeams      = 1;
Plan.plan_file_root   = file_root;
Plan.DVH_bin_size = '0.01';

Plan.vois(1).name = 'target';
Plan.vois(1).color = '1 0 0';
Plan.vois(1).is_target = 'true';
Plan.vois(1).max_DVH_dose = '100';
Plan.vois(1).max_DVH_volume = '0';
Plan.vois(1).min_DVH_dose = '100';
Plan.vois(1).min_DVH_volume = '100';
Plan.vois(1).weight_over = '50';
Plan.vois(1).weight_under = '100';
Plan.vois(1).geometry = ['(abs(z) <= .5)' ...
			 '&(abs(x) <= 2.5)' ...
			 '&(abs(y) <= 2.5)'];

		 
Plan.vois(2).name = 'organ at risk';
Plan.vois(2).color = '0 0 1';
Plan.vois(2).max_DVH_dose = '20';
Plan.vois(2).max_DVH_volume = '0';
Plan.vois(2).weight_over = '1';
Plan.vois(2).geometry = ['(abs(z) <= .5)' ...
                         '&(abs(x) <= 8.0)' ...
                         '&(abs(y) <= 8.0)'];

% Write pln file
write_pln([file_root '.pln'], Plan);

% ---------------------------
% Define geometry (all in cm)
sizeX = 20; sizeY = 20; sizeZ = 0.1;
dif.dX = 0.5;
dif.dY = 0.5;
dif.dZ = 0.1;

% Find size of dose cube in voxels
dif.nX = round(sizeX / dif.dX);
dif.nY = round(sizeY / dif.dY);
dif.nZ = round(sizeZ / dif.dZ);

% Define index of isocenter (0,0,0) in x,y,z space
dif.isoX = round(dif.nX / 2);
dif.isoY = round(dif.nY / 2);
dif.isoZ = round(dif.nZ / 2);

% Define RCS coordinates for later convenience
nR = dif.nZ; isoR = dif.isoZ;
nC = dif.nX; isoC = dif.isoX;
nS = dif.nY; isoS = dif.isoY;

nVoxels = nR * nC * nS;

% Define other dif values
dif.ct_nX = dif.nX;
dif.ct_nY = dif.nY;
dif.ct_nZ = dif.nZ;
dif.ct_isoX = dif.isoX;
dif.ct_isoY = dif.isoY;
dif.ct_isoZ = dif.isoZ;

% Write dif file
write_dif([Plan.plan_file_root '.dif'], dif);

% ------------------------------------------------------------------- %
% Create volume of interest and influence matrix information
% for phantom

% Create empty VOI cube
voiField = zeros(nR,nC,nS);

% Calculate position for every point in VOI cube
[x, y, z] = get_xyz_table(dif, [1:nVoxels]);

% Add each VOI to voiField
for(iVoi = length(Plan.vois):-1:1)
    % Check if geometry defined
    if(isfield(Plan.vois(iVoi),'geometry')&&~isempty(Plan.vois(iVoi).geometry))
        voi_mask = eval(Plan.vois(iVoi).geometry);
	voiField(voi_mask) = iVoi;
    end
end

% Write voi file
write_voi([file_root '.voi'], voiField);


% ------------------------------------------------------------------- %
% Create ct attributes file for phantom

% Dummy random values
ctatts.cti_file_name='/home/trofimov/konrad/data/pat/McGrath/MCGRATHIMRT000.CTI';
ctatts.slice_dimension =  512;
ctatts.slice_number =  140;
ctatts.pixel_size = 0.7300;
ctatts.slice_distance = 2.5000;
ctatts.Pos_Isocenter_X = 152;
ctatts.Pos_Isocenter_Y = 231.2500;
ctatts.Pos_Isocenter_Z = 22.4400;
ctatts.Number_Of_Voxels_in_CT_Cube_Z = 82;

% Write ctatts file
write_ctatts([file_root '.ctatts'], ctatts);


% ------------------------------------------------------------------- %
% Compute Dij and bixel_info for each beam
%

% geometry of Siemens MLC: 40x40 cm^2, leaves are 1 cm thick, the outer two
% leaf pairs are 6.5 cm, but that is not being modeled here.
beam_SAD = 100.;     % 100 cm source to axis distance
beam_SAD = 100000000000000000.;     % 100 cm source to axis distance
beam_nX=79;
beam_nY=29;

beam_dx_min = 0.4;  %cm
beam_dy_min = 1; %cm
beam_dx = beam_dx_min * ones(beam_nX, 1); 
beam_dy = beam_dy_min * ones(beam_nY, 1);
% beam_dy = [6.5; beam_dy_min * ones(beam_Ny - 2, 1); 6.5];

% Find center of each column of bixels
beam_xpos = cumsum(beam_dx)-sum(beam_dx(1:ceil(end/2)));
beam_ypos = cumsum(beam_dy)-sum(beam_dy(1:ceil(end/2)));

% Initialize bixel_info
bixel_info = struct('instanceNo', [], 'beamNo', [], 'energy', [],...
                    'spotX', [], 'spotY', []);
beam_weights = [];

bixel_offset = 0;
iInstance = 1;
for iBeam = 1

    % Beam Properties
    machine_name   = 'phantom';
    beam_energy = 6.0;

    % Beam geometry
    table_angle = 0;
    gantry_angle = 360 / Plan.nBeams * (iBeam - 1);
    collimator_angle  = 0.0;

    % Bixel dimensions
    spot_dx  = beam_dx_min;
    spot_dy  = beam_dy_min;

    % Beam aperture
    beam_aperture = true(beam_nX, beam_nY);

    nBixels = beam_nX * beam_nY;

    % Store information about all bixels (makes comparing plans easier)
    [xi, yi] = ind2sub([beam_nX, beam_nY], [1:nBixels]');

    % Bixel information
    bixel_info.instanceNo = [bixel_info.instanceNo; iInstance*ones(nBixels,1)];
    bixel_info.beamNo = [bixel_info.beamNo; iBeam*ones(nBixels,1)]; 
    bixel_info.energy = [bixel_info.energy; beam_energy*ones(nBixels,1)];
    bixel_info.spotX  = [bixel_info.spotX; beam_xpos(xi)];
    bixel_info.spotY  = [bixel_info.spotY; beam_ypos(yi)];

    % Set beam weight to zero for bixels outside aperture
    beam_weights = [beam_weights; beam_aperture];

    % Calculate beamlet influence matrix.  Each column corresponds to the
    % influence from one beamlet.
    Dij_T = spalloc(nBixels, nVoxels, nVoxels*10);

    % Precalculate position for all points within dose cube
    [x, y, z] = get_xyz_table(dif, [1:nR*nC*nS]);

    % - Convert to position within cone coordinate system.
    position = table_to_gantry(table_angle, gantry_angle, [x; y; z]);
    position = gantry_to_cone(beam_SAD, position);
    position(3,:) = radioactive_path_length(beam_SAD, 1, 20, position(3,:));

    % - Calculate influence of all bixels in aperture
    h = waitbar(0,'Calculating Dij');
    for(iBixel=1:nBixels)
        % Skip all this if bixel outside of aperture
        if(~beam_aperture(iBixel))
            continue
        end

        waitbar(iBixel/nBixels,h);

        % Find position within pencil beam
        pencil_pos = [position(1,:) - bixel_info.spotX(iBixel) + .01;
                      position(2,:) - bixel_info.spotY(iBixel);
                      position(3,:)];

        % Find dose within pencil beam
	Dij_T(iBixel,:) = pencil_beam_dose( ...
		spot_dx, spot_dy, 'ideal rectangular', pencil_pos)';
    end
    close(h);
    Dij = Dij_T';
    clear Dij_T;

    % Now write the dij file out for this beam
    DijStruct.machineName     = machine_name;
    DijStruct.beamNumber      = iBeam;
    DijStruct.gantryAngle     = gantry_angle;
    DijStruct.tableAngle      = table_angle;
    DijStruct.collimatorAngle = collimator_angle;
    DijStruct.spot_dx         = spot_dx;
    DijStruct.spot_dy         = spot_dy;
    DijStruct.voxel_dX        = dif.dX;
    DijStruct.voxel_dY        = dif.dY;
    DijStruct.voxel_dZ        = dif.dZ;
    DijStruct.doseCubeNc      = dif.nZ;
    DijStruct.doseCubeNs      = dif.nX;
    DijStruct.doseCubeNr      = dif.nY;
    DijStruct.numPencilBeams  = nBixels;
    DijStruct.energy          = bixel_info.energy;
    DijStruct.spotX           = bixel_info.spotX;
    DijStruct.spotY           = bixel_info.spotY;
    DijStruct.Dij             = Dij;
    DijStruct.beamWeight      = beam_weights;
    write_dij([file_root '_' num2str(iBeam) '.dij'], DijStruct);
    write_stf([file_root '_' num2str(iBeam) '.stf'], DijStruct);
    write_bwf([file_root '_' num2str(iBeam) '.bwf'], DijStruct);
end


