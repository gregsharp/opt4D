function [objective_names, sampling_fraction_hist]...
= read_opt4D_sampling_history(out_file_prefix)
%read_opt4D_sampling_history Reads opt4D sampling history file(s)
%
%  [objective_names, sampling_hist] =
%    read_opt4D_sampling_history(out_file_prefix) Reads the sampling history
%    files based on the out_file_prefix.  If the prefix is empty or ommitted,
%    the user will be given the oportunity to pick a file.
%
%   The out_file_prefix can be a cell array of strings, in which case the
%   returned values will also be cell arrays.


% Check if a sampling file is included already.
if(~exist('out_file_prefix'))
    % Call up a dialog to choose a sampling file
    [file_name, path_name, filter_index] = uigetfile( ...
    {'*obj_sampling_history.dat', 'Opt4D Sampling History File (*.dat)'}, ...
    'Pick sampling history file');
    if(filter_index == 0)
        error( 'User did not choose a sampling history file.');
    elseif(filter_index == 1)
        history_file = [path_name, file_name];
    end

    % Strip off sampling_history.dat from end of file name
    out_file_prefix = history_file(1:end-length('obj_sampling_history.dat'));
end

% Check if out_file_prefix is a cell array
if(~iscell(out_file_prefix))
    % Not a cell array, so just read one file
    file_name = [out_file_prefix 'obj_sampling_history.dat'];
    if(exist(file_name,'file'))
        [objective_names, sampling_fraction_hist] = ...
            read_obj_sampling_history(file_name);
    else
        error(['Sampling history file: ' file_name ' does not exist']);
    end
else
    
    objective_names = {};
    sampling_fraction_hist = {};

    for(iRun = 1:numel(out_file_prefix))
        % Read sampling fraction history if present
        file_name = [out_file_prefix{iRun} 'obj_sampling_history.dat'];
        if(exist(file_name,'file'))
            [objective_names{iRun}, sampling_fraction_hist{iRun}] = ...
            read_obj_sampling_history(file_name);
        end
    end
end

return

% ---------------------------------------------------------%
function [objective_descriptions, sampling_fract_hist] = ...
read_obj_sampling_history(file_name)
%

[fid, message] = fopen(file_name);
if(fid == -1)
    error(['Error reading: "' file_name '": ' message]);
end

% Throw out first line
dummy = fgetl(fid);

% Read objective descriptions
objective_descriptions = {};
line = fgetl(fid);
while(~strcmp(lower(line),'sampling fraction history:'))
    objective_descriptions{end+1} = line;
    line = fgetl(fid);
end

sampling_fract_hist = ...
reshape(fscanf(fid,'%lf'),numel(objective_descriptions),[])';

