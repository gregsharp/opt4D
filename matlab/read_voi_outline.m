function [voi_outline, voi_cmap] = read_voi_outline( file_name, nRows, nColumns)
%read_voi_outline Finds the outline of a volume of information cube.
%    from either a voi file or a plan file
% [voi_outline, voi_cmap] = read_voi_outline( file_name, nRows, nColumns)

% Find file to read if it is not passed in
if(nargin < 1)
    [file_name, path_name, filter_index] = uigetfile( ...
        {'*.pln', 'Plan file (*.pln)'; ...
         '*.voi', 'VOI file (*.voi)'}, ...
         'Load voi file');
    if(filter_index == 0)
        disp( 'User pressed cancel.');
        return;
    else
        file_name = [path_name, file_name];
    end

    if(filter_index == 2)
        nRows = input('Rows in dose cube? ');
        nColumns = input('Columns in dose cube? ');
    end
end

if(~exist('nRows'))
    % Reading a plan file
    if(isstr(file_name))
        plan = read_pln(file_name);
    else
        plan = file_name;
    end

    % Read dimension information file
    dif = read_dif([plan.dif_file_name]);

    nRows = dif.nZ;
    nColumns = dif.nX;
    nSlices = dif.nY;

    dx = dif.dX;
    dy = dif.dY;
    dz = dif.dZ;

    % Find Voi file name
    voiFile = plan.voi_file_name;
else
    dx = 1; dy = 1; dz = 1;
    voiFile = file_name;
end

[voiField] = read_voi( voiFile, nRows, nColumns);
[voi_outline] = find_outline(voiField);

nVois = double(max(voi_outline(:)));
voi_cmap = [0 0 0; rand(nVois, 3)];
if(exist('plan') && isfield(plan.VOI, 'color'))
    for(iVoi = 1:nVois)
        if(iVoi <= length(plan.VOI) && ~isempty(plan.VOI(iVoi).color))
            voi_cmap(iVoi+1,:) = plan.VOI(iVoi).color;
        end
    end
end

return


% ------------------------------------------------------------------- %
function [voi_outline] = find_outline(voiField)

voi_outline = voiField;
voi_size = size(voiField);

if(size(voi_outline,3) == 1)
    insides_mask = [(voiField(1:end-1,:) == voiField(2:end,:)) ; ...
	false(1,voi_size(2))] ...
    & [false(1,voi_size(2)); ...
       (voiField(1:end-1,:) == voiField(2:end,:))] ...
    & [(voiField(:,1:end-1) == voiField(:,2:end)), ...
       false(voi_size(1), 1)] ...
    & [false(voi_size(1), 1), ...
       (voiField(:,1:end-1) == voiField(:,2:end))];
elseif(size(voi_outline,1) == 1)
    insides_mask = [(voiField(:,1:end-1,:) == voiField(:,2:end,:)), ...
        false(voi_size(1), 1, voi_size(3))] ...
        & [false(voi_size(1), 1, voi_size(3)),...
        (voiField(:,1:end-1,:) == voiField(:,2:end,:))] ...
        & cat(3, (voiField(:,:,1:end-1) == voiField(:,:,2:end)),...  
              false(voi_size(1), voi_size(2), 1)) ...
        & cat(3, false(voi_size(1), voi_size(2), 1),...
              (voiField(:,:,1:end-1) == voiField(:,:,2:end,:)));
elseif(size(voi_outline,2) == 1)
    insides_mask = [(voiField(1:end-1,:,:) == voiField(2:end,:,:)); ...  
        false(1,voi_size(2), voi_size(3))] ...
        & [false(1,voi_size(2), voi_size(3));...
        (voiField(1:end-1,:,:) == voiField(2:end,:,:))] ...
        & cat(3, (voiField(:,:,1:end-1) == voiField(:,:,2:end,:)), ...
        false(voi_size(1), voi_size(2), 1)) ...
        & cat(3, false(voi_size(1), voi_size(2), 1), ...
        (voiField(:,:,1:end-1) == voiField(:,:,2:end,:)));
else
    insides_mask = [(voiField(1:end-1,:,:) == voiField(2:end,:,:)) ; ...
        false(1,voi_size(2), voi_size(3))] ...
        & [false(1,voi_size(2), voi_size(3)); ...
        (voiField(1:end-1,:,:) == voiField(2:end,:,:))] ...
        & [(voiField(:,1:end-1,:) == voiField(:,2:end,:)), ...
        false(voi_size(1), 1, voi_size(3))] ...
        & [false(voi_size(1), 1, voi_size(3)), ...
        (voiField(:,1:end-1,:) == voiField(:,2:end,:))] ...
        & cat(3, (voiField(:,:,1:end-1) == voiField(:,:,2:end,:)), ...
        false(voi_size(1), voi_size(2), 1)) ...
        & cat(3, false(voi_size(1), voi_size(2), 1), ...
        (voiField(:,:,1:end-1) == voiField(:,:,2:end,:)));
end

voi_outline(insides_mask) = 0;
  
return
