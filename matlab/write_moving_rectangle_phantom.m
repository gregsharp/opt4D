function flag = write_moving_rectangle_phantom(outFile,nBeams,nInstances)
% writeRtogPhantom  Writes Konrad formated files for a phantom
%     [flag] = writeRtogPhantom(outFile, nBeams, nInstances)
%     writes out Konrad files for a moving phantom.
%     Creates .dij, .stf, .bwf, .dif, .voi, and .voi2 files.
%
%     IM is the data structure with information about the problem.
%
%     outFile is the path and name for the Konrad files.
%
%     structNums is an array of structure numbers to include in the Konrad
%     files.  Any voxels outside of the structures will be assumed to be air.
%     The first voxels will take priority.  Use structNums = [] to include all
%     structures that have a goal defined.
%
%     beamWeights is an array containing beam weights to use for the start of
%     optimization.  This should be a vector the same length as the number of
%     beamlets.  Use beamWeights = [] to set all weights to 1.
%
%     beamletThreshold is a value between 0 and 1 to only allow beamlets with at
%     least one Dij entry greater than beamletThreshold times the maximum Dij
%     entry.
%
%     flag returns 'error' or 'no error'.
%
%     For example,
%
%         [flag] = writeKonradFiles(IM, 'default')
%
%     creates files named default.voi, default.dif, default_1.dij,
%     default_2.dij, etc.
%
%     Notes:
%     * There are many non-essential values that were given arbitrary defaults.
%       This shouldn't be a problem for optimization, but better values are
%       obviously prefered.
%
%     * To avoid having to change the counting of the indexes of the voxels,
%       this function currently just assigns all of the voxels removed in
%       downsampling to be air.  Run showVoi to see the effect.  This
%       shouldn't be a huge deal since everthing other than the voi is in a
%       sparse format, but it is better avoided.
%
%     * Note that writing out multiple beams will only work if the
%       IM.beams(beam).beamNumber is set correctly.


% ----------------------------------------------------------------- %
% Make sure the inputs to the function are correct.

% Both the output file prefix and the number of beams are required
if(~exist('outFile') || ~exist('nBeams'))
    error('Both the output file prefix and the plan structure are required');
end

% Multiple instances has not been implemented yet.
if(~exist('nInstances'))
    nInstances = 1;
elseif(nInstances > 1)
%    error('multiple instances is not implemented yet');
end

% ------------------------------------------------------------------- %
% General plan description for creating plan file

Plan.title       = 'Rectangle Phantom';
Plan.description = 'Rectangle Phantom';
Plan.nInstances  = 1;%nInstances;
Plan.nBeams      = nBeams;
Plan.file_root   = outFile;
Plan.params = {'ctatts_file_name','DVH_bin_size'};
Plan.values = {'/home/bmartin/cerr_ex3/cwg_plan1_7beams.ctatts','0.01'};

Plan.vois(1).name = 'target';
Plan.vois(1).color = '1 0 0';
Plan.vois(1).is_target = 'true';
Plan.vois(1).max_DVH = '100 0%';
Plan.vois(1).min_DVH = '100 100%';
Plan.vois(1).weight_over = '50';
Plan.vois(1).weight_under = '100';
Plan.vois(1).geometry = ['(abs(z) <= .5)' ...
                         '&(abs(x) <= .5)' ...
                         '&(abs(y) <= 5.0)'];
Plan.vois(1).offset = ['[ ' num2str(0*[1:nInstances]) '; ' ...
                  num2str(2*(sin(pi*[0:nInstances-1]/nInstances)).^4) '; ' ...
                  num2str(0*[1:nInstances]) ']' ];

figure; plot(eval(Plan.vois(1).offset)');
		 
Plan.vois(2).name = 'organ at risk';
Plan.vois(2).color = '0 0 1';
Plan.vois(2).max_DVH = '0 0%';
Plan.vois(2).weight_over = '1';
Plan.vois(2).geometry = ['(abs(z) <= .5)' ...
                         '&(abs(x) <= 8.0)' ...
                         '&(abs(y) <= 8.0)'];
Plan.vois(2).offset = ['[ ' num2str(0*[1:nInstances]) '; ' ...
                  num2str(2*(sin(pi*[0:nInstances-1]/nInstances)).^4) '; ' ...
                  num2str(0*[1:nInstances]) ']' ];

% Plan.vois(3).name = 'undifferentiated patient';
% Plan.vois(3).color = '.5 .5 .5';
% Plan.vois(3).max_DVH = '120 15%';
% Plan.vois(3).weight_over = '5';
% Plan.vois(3).geometry = '(x.^2)/(7.0^2) + (z.^2)/(7.0^2) <= 1';

sizeX = 0.1; sizeY = 14; sizeZ = 0.1;
dX    = 0.1; dY   = 0.1; dZ    = 0.1;
write_moving_phantom(outFile, Plan, sizeX, sizeY, sizeZ, dX, dY, dZ);
