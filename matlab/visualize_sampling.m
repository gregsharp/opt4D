function visualize_sampling( plan, sampling_fractions, sampling_movie_file )
%visualize_sampling  Explore a sampled volume of information cube.
%   To navigate:
%     Left click on a face to cut it away,
%     Right click to add a face back,
%     middle click to restore it completely
%
%   visualize_sampling without any arguments calls up a diologue box
%       to enter the file names that you wish to visualize.
%
%   visualize_sampling( plan, sampling_fractions) explores the volumes of
%       intrest for the given plan.  The plan can either be a structure
%       (see read_pln) or a file name.
%
%   visualize_sampling( plan, sampling_fractions, sampling_movie_file)
%       writes an animated gif to the desired file name.  If
%       sampling_fractions is a cell array, each frame of the movie will have
%       the corresponding sampling fraction.


% Find file to read if it is not passed in
if(~exist('plan') || isempty(plan))
    plan = ui_get_plan_file_name;
end

if(isstr(plan))
    plan = read_pln(plan);
end

dif_file_name = plan.dif_file_name;
voi_file_name = plan.voi_file_name;

% Read dimension information file
dif = read_dif(dif_file_name);

% Pull attributes from dif structure
nRows = dif.nZ;
nColumns = dif.nX;
nSlices = dif.nY;

dx = dif.dX;
dy = dif.dY;
dz = dif.dZ;

[voiField] = readVoi( voi_file_name, nRows, nColumns);

nSlices = size(voiField, 3);

% Find all VOIs in cube
voiNos = double(unique(voiField(:)));
nVois = max(voiNos);

% Use colormap from plan file if it exists
cmap = rand(voiNos(end)+1,3);
cmap(1,:) = [1 1 1];
if(isfield(plan.VOI, 'color'))
    for(iVoi = voiNos')
        if(iVoi > 0 && ~isempty(plan.VOI(iVoi).color))
            cmap(iVoi+1,:) = plan.VOI(iVoi).color;
        end
    end
end


% Add washed out versions of each color to colormap
for(iVoi = 1:nVois)
    cmap(iVoi+1+nVois,:) = (cmap(iVoi+1,:) + [1 1 1])./ 2;
end

% Ask user for sampling rates if they weren't supplied
if(~exist('sampling_fractions') || isempty(sampling_fractions))
    sampling_fractions = zeros(nVois,1);
    for(iVoi = voiNos')
        if(iVoi > 0)
            sampling_fractions(iVoi) = input(...
            ['What is the sampling fraction for VOI ' num2str(iVoi) ...
            ' (' plan.VOI(iVoi).name, ')? ']);
        end
    end
end

% Make sampling fraction into a cell array if needed
if(iscell(sampling_fractions))
    cell_sampling_fractions = sampling_fractions;
    sampling_fractions = cell_sampling_fractions{1};
    nFrames = numel(cell_sampling_fractions);
    use_cell_array = true;
else
    nFrames = 30;
    use_cell_array = false;
end

% Create the sampled voi field
sampled_voi_field = voiField;
rand_field = rand(size(sampled_voi_field));
for(iVoi = voiNos')
    if(iVoi > 0)
        % Change the numbering for unsampled voxels
        sampled_voi_field((voiField == iVoi) & ...
        (rand_field > sampling_fractions(iVoi))) = iVoi + nVois;
    end
end

% [voi_outline] = find_outline(voiField);
% explore_solid_RCS(voi_outline, cmap, dx, dy, dz);

if(~exist('sampling_movie_file'))
    % We aren't going to make a movie, so create a 3D figure
    figure(fig);
    explore_solid_RCS(sampled_voi_field, cmap, dx, dy, dz);
else

    % Get a slice to work with (isocenter slice)
    voiSlice = voiField(:,:,dif.isoY);

    % Find outlines for use later
    [outlines_x, outlines_y] = find_edges(voiSlice);

    % Initialize Movie
    M = moviein(nFrames);
    movie_fig = figure('name','Movie_fig');
    colormap(cmap);
    for(iFrame = 1:nFrames)
        % Create the sampled voi field
        sampled_voi_slice = voiSlice;
        rand_slice = rand(size(sampled_voi_slice));
        for(iVoi = voiNos')
            if(iVoi > 0)
                if(use_cell_array)
                    % Change the numbering for unsampled voxels
                    sampled_voi_slice((voiSlice == iVoi) & ...
                    (rand_slice > cell_sampling_fractions{iFrame}(iVoi))) =...
                    iVoi+nVois;
                else
                    sampled_voi_slice((voiSlice == iVoi) & ...
                    (rand_slice > sampling_fractions(iVoi))) =...
                    iVoi+nVois;
                end
            end
        end
        image(sampled_voi_slice); axis xy; axis image;
        h = line(outlines_x,outlines_y);
        set(h,'color',[0 0 0]);
        if(use_cell_array)
            text(1,1,num2str(iFrame));
        end
        drawnow;
        M(iFrame) = getframe;
    end
    close(movie_fig);

    movie2gif(M, sampling_movie_file, ...
    '--delay 10 -l -t#FFFFFF');
end

return



% ------------------------------------------------------------------- %
function [voiField] = readVoi( voiFile, nRows, nColumns)

  [fid,message]=fopen(voiFile);
  if(~isempty(message))
      error(['Error reading voi file: ' voiFile ' (' message ')']);
  end

  voiField = uint8(fread(fid,inf,'uint8') + 1);
  
  voiField = reshape(voiField, nColumns, [], nRows);
  voiField = permute(voiField, [3 1 2]);

  voiField(voiField == 128) = 0;
  
return


% ------------------------------------------------------------------- %
function [voi_outline] = find_outline(voiField)

voi_outline = voiField;
voi_size = size(voiField);

insides_mask = [(voiField(1:end-1,:,:) == voiField(2:end,:,:)) ; false(1,voi_size(2), voi_size(3))] ...
    & [false(1,voi_size(2), voi_size(3)); (voiField(1:end-1,:,:) == voiField(2:end,:,:))] ...
    & [(voiField(:,1:end-1,:) == voiField(:,2:end,:)), false(voi_size(1), 1, voi_size(3))] ...
    & [false(voi_size(1), 1, voi_size(3)), (voiField(:,1:end-1,:) == voiField(:,2:end,:))] ...
    & cat(3, (voiField(:,:,1:end-1) == voiField(:,:,2:end,:)), false(voi_size(1), voi_size(2), 1)) ...
    & cat(3, false(voi_size(1), voi_size(2), 1), (voiField(:,:,1:end-1) == voiField(:,:,2:end,:)));
voi_outline(insides_mask) = 0;
  
return



% -------------------------------------------
function [x,y] = find_edges(slice)

% Creating x,y coordinate system relative to 0,0 position in 2-d array passed, 
% normalized to each pixel = 1 unit

% Find horizontal edges
[i, j] = find(slice(1:end-1,:) ~= slice(2:end, :));
x = [j'-.5; j'+.5];
y = [i'+.5; i'+.5];

% Find vertical edges
[i, j] = find(slice(:,1:end-1) ~= slice(:,2:end));
x = [x, [j'+.5; j'+.5]];
y = [y, [i'-.5; i'+.5]];

% line(y,x);

