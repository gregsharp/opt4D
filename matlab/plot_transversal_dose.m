function plot_transversal_dose(dose_file_path, pln_file_name, slice_dose, range_indices, beam_indices)

% print dose distributions for one slice in the dose cube with contours
% from the vdx file

% parameters:

% dose_file_path: string containing the path to the directory where dose
% results are stored. Name of dose file must be "dose.dat"

% pln_file_name: string containing the plan file name and path

% slice_dose: integer for the dose cube slice

% range_indices: (optional argument) vector of integers containing the
% auxiliary numbers of auxiliary dose distributions
% dose file names must be e.g. "dose_aux_1.dat"

% beam_indices: (optional argument) vector of integers containing the
% beam numbers to plot dose distributions of individual beams
% dose file names must be e.g. "dose_beam_1.dat"

% in addition a parameter file read_plot_parameters.m is needed which
% contains more plot parameters


if(~exist('dose_file_path') || ~isstr(dose_file_path))
    error('no directory path to dose files');
end

if(~exist('pln_file_name') || ~isstr(pln_file_name))
    error('no plan file given');
end

if(~exist('slice_dose'))
    error('no slice number in dose cube given');
end

if(~exist('range_indices'))
    range_indices = [];
end

if(~exist('beam_indices'))
    beam_indices = [];
end

% read parameters
plot_param = set_plot_parameters_bed();

% nominal dose
plot_param.IsoDoseLevels = plot_param.IsoDoseLevelsDose;
plot_param.MaxDose = plot_param.MaxDoseCum;
plot_param.MinDose = plot_param.MinDoseCum;
plot_param.Norm = plot_param.NormDose;
dose_file_root = 'dose';
plot_trans_slice(plot_param, dose_file_path, dose_file_root, pln_file_name, slice_dose);

% dose distributions
if length(range_indices) > 0

    for i=1:length(range_indices)

        plot_param.IsoDoseLevels = plot_param.IsoDoseLevelsDose;
        plot_param.MaxDose = plot_param.MaxDoseCum;
        plot_param.MinDose = plot_param.MinDoseCum;
        dose_file_root = ['scenario_0' num2str(range_indices(i),'%10.1d') '_dose'];
        %dose_file_root = ['aux_' int2str(range_indices(i))];
        plot_trans_slice(plot_param, dose_file_path, dose_file_root, pln_file_name, slice_dose);

    end
end

% individual beams
if length(beam_indices) > 0

    for i=1:length(beam_indices)

        plot_param.IsoDoseLevels = plot_param.IsoDoseLevelsBeam;
        plot_param.MaxDose = plot_param.MaxDoseBeam;
        plot_param.MinDose = plot_param.MinDoseBeam;
        %dose_file_root = ['dose_beam_' int2str(beam_indices(i))];
        dose_file_root = ['beam_' num2str(beam_indices(i),'%10.1d') '_dose'];
        plot_trans_slice(plot_param, dose_file_path, dose_file_root, pln_file_name, slice_dose);

    end
end

% variance
if(exist([dose_file_path '/' 'variance.dat'], 'file'))
    plot_param.IsoDoseLevels = plot_param.IsoDoseLevelsVariance ;
    plot_param.MaxDose = plot_param.MaxVar;
    plot_param.MinDose = plot_param.MinVar;
    plot_param.Norm = plot_param.NormVar;
    dose_file_root = ['variance'];
    plot_trans_slice(plot_param, dose_file_path, dose_file_root, pln_file_name, slice_dose);
else
    disp('variance file does not exist');
end

return