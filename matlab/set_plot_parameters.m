function [plot_param] = set_plot_parameters()

error('This function should not be called. Use local copy in result directory');

plot_param = struct;

% voi contours to read
plot_param.VoisToRead = [1,3,4,5,6,14,15,16];

% 100 percent isodose level in Gray
% set value to zero to normalize to the maximum dose
plot_param.NormDose = 70;

% VDX file name
plot_param.vdx_file_name = 'hoyle3958800000_old.VDX';

% parameters form HED file and KonRad Plan file
plot_param.CT_slice_thickness = 5.0;
plot_param.CT_size = 512;
plot_param.CT_voxel_size = 0.801;
plot_param.CT_isocenter_X = 201.0;
plot_param.CT_isocenter_Y = 247.0;
plot_param.CT_isocenter_Z = 198.25;

% isodose lines
plot_param.IsoDoseLevelsDose = [0.2 0.4 0.6 0.7 0.8 0.9 0.95 1.0 1.05 1.1 1.2 1.3 1.4];
plot_param.IsoDoseLevelsBeam = linspace(0.05,0.8,16);
plot_param.IsoDoseLevels = [];

% maximum/minimum dose values in the colormap
plot_param.MaxDoseCum = 1.2;     % cumulative dose
plot_param.MinDoseCum = 0.2;
plot_param.MaxDoseBeam = 0.8;    % individual beam
plot_param.MinDoseBeam = 0.05;
plot_param.MaxDose = 0;
plot_param.MinDose = 0;

% plot window
plot_param.axisLeft = -100;
plot_param.axisRight = 100;
plot_param.axisBottom = -70;
plot_param.axisTop = 50;

plot_param.plot_width = 16; % in cm

return
