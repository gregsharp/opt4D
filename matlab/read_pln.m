function [plan] = read_pln(pln_file)
%read_pln Read opt4D plan file.
%   plan = read_pln(pln_file_name) Reads the plan file into a structure.
%
%   See also write_pln for plan structure information.

% Read the plan file into memory
try
    [command, value] = textread(pln_file, '%s %[^\n]', 'commentstyle', 'shell');
catch
    error(['Unable to read plan file: ' pln_file]);
end

% Initialize the plan structure
plan = struct('title', '');

% Check if file is valid
if(isempty(command) || ~strcmp(command{1}, 'pln_file_version') ...
    || ~strcmp(value{1},'0.2'))
    % The file is not a valid version 0.1 pln file
    error([pln_file ' is not a valid version 0.2 pln file.']);
end

% Process each line of the file
for(iCommand = 1:numel(command))
    temp_command = command{iCommand};
    temp_value = value{iCommand};

    switch(temp_command)
    case 'VOI'
        % VOI command, so process specially

	plan = process_voi_command(temp_value, plan);

    case 'instance'
        % instance command, so process specially
        
        % Break the instance command into parts
        [instanceNo, instance_command, instance_data] =...
        strread(temp_value,'%d %s %[^\n]');

        % For now, this is good enough
        plan.instance(instanceNo).(instance_command) = instance_data;


    otherwise
        % Normal command, so add it to plan structure

        % Convert to number if appropriate (nInstances, etc.)
        temp_num = str2double(temp_value);

        if(isnan(temp_num))
            % Not a number, so store the string
            plan.(temp_command) = temp_value;
        else
            % Evaluated as a valid number
            plan.(temp_command) = temp_num;
        end
    end
end

plan = process_file_names(plan, pln_file);

return


%---------------------------------------
function plan = process_file_names(plan, pln_file)

  % Process file names and load default file names for unsupplied names
  if(isfield(plan, 'plan_file_root'))
      fields = {'bwf_file_root'; 
      'dij_file_root';
      'stf_file_root';
      'ct_file_name';
      'ctatts_file_name';
      'dif_file_name';
      'voi_file_name'};

      generic_values = {plan.plan_file_root,
      plan.plan_file_root;
      plan.plan_file_root;
      [plan.plan_file_root '.ct'];
      [plan.plan_file_root '.ctatts'];
      [plan.plan_file_root '.dif'];
      [plan.plan_file_root '.voi']};

      for(i = 1:numel(fields))
	  if(~isfield(plan, fields{i}))
	      plan.(fields{i}) = generic_values{i};
	    end
	end
    end


    % Replace relative file names with absolute names if needed
    [pln_file_pathstr,name,ext,versn] = fileparts(pln_file);
	
    % Add path to file names if neccessary
    file_name_fields = {...
	'plan_file_root';
	'bwf_file_root';
	'ct_file_name';
	'ctatts_file_name';
	'dif_file_name';
	'stf_file_root';
	'voi_file_name'};
    for(i = 1:numel(file_name_fields))
	[temp_pathstr,name,ext,versn] = fileparts(plan.(file_name_fields{i}));
	if(isempty(temp_pathstr))
	    plan.(file_name_fields{i}) =...
            fullfile(pln_file_pathstr, [name ext]);
	end
    end

    return

%----------------------------------------------------
function [plan] = process_voi_command(voi_data, plan)

  % Break the voi command into parts
  [voiNo, voi_command, voi_command_data] = strread(voi_data,'%d %s %[^\n]');
  voi_command = voi_command{1};
  voi_command_data = voi_command_data{1};


  % Process parts
  switch( lower(voi_command))
  case 'color'
      plan.VOI(voiNo).color = strread(voi_command_data, '%n').';
  case 'dose'
      % Objective, so unpack
      [comparitor, desired_dose, weight] = ...
      strread(voi_command_data, '%[<>=] %n weight %n');

      % Apply default weight if not supplied
      if(isempty(weight))
	  weight = 1;
      end

      switch comparitor{1}
      case '<'
          is_min_constraint = false;
          is_max_constraint = true;
          is_equality_constraint = false;
      case '>'
          is_min_constraint = true;
          is_max_constraint = false;
          is_equality_constraint = false;
	  plan.VOI(voiNo).isTarget = true;
      case '='
          is_min_constraint = false;
          is_max_constraint = false;
          is_equality_constraint = true;
	  plan.VOI(voiNo).isTarget = true;
      end

      temp_objective = struct('voiNo', voiNo,...
      'desired_dose', desired_dose, 'weight', weight,...
      'is_max_constraint', is_max_constraint,...
      'is_min_constraint', is_min_constraint,...
      'is_equality_constraint', is_equality_constraint);

      if(isfield(plan, 'objectives'))
          plan.objectives(end+1) = temp_objective;
      else
          plan.objectives = temp_objective;
      end

  otherwise
      % Convert to number if appropriate (nInstances, etc.)
      temp_num = str2double(voi_command_data);

      if(isnan(temp_num))
	  % Not a number, so store the string
	  plan.VOI(voiNo).(voi_command) = voi_command_data;
      else
	  % Evaluated as a valid number
          plan.VOI(voiNo).(voi_command) = temp_num;
      end
  end

  
  return

% Read information about the volumes of interest
vois = struct([]);
for i = find(strcmp(command, 'VOI'))'
    voi_data = '';
    [voiNo, voi_command, voi_data] = strread(value{i},'%d %s %[^\n]');
    if(strcmp(voi_command, 'name'))
        vois(voiNo).name = voi_data{:};
    elseif(strcmp(voi_command, 'color'))
        vois(voiNo).color = strread(voi_data{1}, '%n').';
    elseif(strcmp(voi_command, 'EUD_p'))
	vois(voiNo).EUD_p = parse_value(voi_data{1});
    elseif(strcmp(voi_command, 'weight_over'))
	vois(voiNo).weight_over = parse_value(voi_data{1});
    elseif(strcmp(voi_command, 'weight_under'))
	vois(voiNo).weight_under = parse_value(voi_data{1});
    elseif(strcmp(voi_command, 'sampling_fraction'))
        vois(voiNo).sampling_fraction = parse_value(voi_data{1});
    elseif(strcmp(voi_command, 'min_DVH'))
	vois(voiNo).isTarget = true;
	space_pos = find(voi_data{1} == ' ');
	dose = parse_value(voi_data{1}(1:space_pos));
	volume = parse_value(voi_data{1}(space_pos+1:end));
        if(isfield(vois,'min_DVH') && isstruct(vois(voiNo).min_DVH))
	    vois(voiNo).min_DVH.dose(end+1) = dose;
	    vois(voiNo).min_DVH.volume(end+1) = volume;
	else
	    vois(voiNo).min_DVH.dose = dose;
	    vois(voiNo).min_DVH.volume = volume;
	end
    elseif(strcmp(voi_command, 'max_DVH'))
	space_pos = find(voi_data{1} == ' ');
	dose = parse_value(voi_data{1}(1:space_pos));
	volume = parse_value(voi_data{1}(space_pos+1:end));
        if(isfield(vois,'max_DVH') && isstruct(vois(voiNo).max_DVH))
	    vois(voiNo).max_DVH.dose(end+1) = dose;
	    vois(voiNo).max_DVH.volume(end+1) = volume;
	else
	    vois(voiNo).max_DVH.dose = dose;
	    vois(voiNo).max_DVH.volume = volume;
	end
    end
end
plan.vois = vois;

return

% ---------------------------
function [value] = parse_value(str)
%



% Throw away any initial space
while(str(1) == ' ')
    str(1) = [];
end

if(str(1) == '#')
    % Throw away initial # sign
    str = str(2:end);

    % Read paramater number
    param_no = 0;
    while(str(1) >= '0' && str(1) <= '9')
	param_no = param_no * 10 + (str(1) - '0');
	str = str(2:end);
    end
    
    % Read default value
    value = 0;
    if(str(1) == '[')
	value = strread(str(2:(find(str == ']')-1)), '%n');
    end
else
    num_str = '';
    i = 1;
    while(i <= length(str) ...
	&& (str(i) == '.' || str(i) == '-' || (str(i) >= '0' && str(i) <= '9')))
	num_str = [num_str str(i)];
	i = i + 1;
    end

    value = strread(num_str, '%n');
end
