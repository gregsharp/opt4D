function [multirun_mapping, single_file_prefixes] = ...
find_multirun_mapping(out_file_prefixes)
%find_multirun_mapping Find the multirun mapping for the given prefixes.
%
% [multirun_mapping, single_file_prefixes] = find_multirun_mapping(...
%   file_prefixes) returns the mapping based on the prefixes.  file_prefixes
%   can be a string or a cell array of strings.
%
%  See plot_opt4D_convergence.

% Make sure out_file_prefixes is a cell array
if(~iscell(out_file_prefixes))
    out_file_prefixes = {out_file_prefixes};
end

nPrefixes = numel(out_file_prefixes);
multirun_mapping = [];
single_file_prefixes = {};

for(i = 1:nPrefixes)
    if(exist([out_file_prefixes{i} 'obj_history.dat'], 'file'))
        % A single run exists
        multirun_mapping(end+1) = i;
        single_file_prefixes{end+1} = out_file_prefixes{i};
    elseif(exist([out_file_prefixes{i} '0001_obj_history.dat'],'file'))
        iRepeat = 1;
        temp_prefix = [out_file_prefixes{i} sprintf('%04u_', iRepeat)];
        while(exist([temp_prefix 'obj_history.dat'], 'file'))
            % A repeated run exists
            multirun_mapping(end+1) = i;
            single_file_prefixes{end+1} = temp_prefix;
            iRepeat = iRepeat + 1;
            temp_prefix = [out_file_prefixes{i} sprintf('%04u_', iRepeat)];
        end
    else
        % The file doesn't exist, so return unchanged
        multirun_mapping(end+1) = i;
        single_file_prefixes{end+1} = out_file_prefixes{i};
    end
end


