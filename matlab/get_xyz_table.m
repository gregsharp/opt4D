function [x, y, z] = get_xyz_table(dif, voxel_index)
%get_xyz_table Finds the position in the table's coordinate system for a voxel
%
%    [x, y, z] = get_xyz_table(dif, voxelNo) Calculates x,y,z position of voxel
%    in table coordinate system where dif contains the geometric information
%    (see read_dif for format information) and voxelNo is an array of voxel 
%    indexes (in RCS coordinate system).  x, y, and z are the same size as
%    voxelNo.
%
%    [position] = get_xyz_table(dif, voxelNo) Calculates x,y,z position of
%    each voxel in table coordinate system.  Position is a 3xn array such that
%    each column corresponds to the x, y, and z postition of one voxel from
%    voxelNo.

% Find row, column, and slice number from index
[row, column, slice] = ind2sub([dif.nZ, dif.nX, dif.nY], voxel_index);

if(nargout == 3)
    % Convert to x, y, and z
    x =  (column - dif.isoX) * dif.dX;
    y =  (slice  - dif.isoY) * dif.dY;
    z =  (row    - dif.isoZ) * dif.dZ;
else
    x = [(column(:)' - dif.isoX) * dif.dX;
         (slice(:)'  - dif.isoY) * dif.dY;
         (row(:)'    - dif.isoZ) * dif.dZ];
end


return

