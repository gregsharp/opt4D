function make_neighbors_v_influence_plot(plan, beamNo, beamletNo)
%make_neighbors_v_influence_plot(plan, beamNo, beamletNo)
%   Makes a plot of the statistics within a Dij file, relating the number of
%   neighbors around a point to the influence of that point.  Useful for
%   identifying beams that are sparsely sampled.  If the beamletNo is not
%   listed, all beamlets are included.

if(isstr(plan))
    plan = read_pln(plan);
end

[DijStruct] = read_dij([plan.dij_file_root '_' num2str(beamNo) '.dij']);
dim = [DijStruct.doseCubeNr, DijStruct.doseCubeNc, DijStruct.doseCubeNs];

total_nnz_neighbors = [];
total_d = [];

if(~exist('beamletNo'))
    beamletNo = 1:numel(DijStruct.index);
end

fig = figure('name', plan.title);

for(iBeamlet = beamletNo(:)')
    d = zeros(dim);
    d(DijStruct.index{iBeamlet}) = DijStruct.value{iBeamlet} * DijStruct.doseCoefficient;
    d_mask = (d > 0);

    nnz_neighbors = zeros(size(d));
    nnz_neighbors = nnz_neighbors + (d_mask & cat(1, true(1,dim(2),dim(3)), d_mask(1:end-1,:,:)));
    nnz_neighbors = nnz_neighbors + (d_mask & cat(1, d_mask(2:end,:,:), true(1,dim(2),dim(3))));
    nnz_neighbors = nnz_neighbors + (d_mask & cat(2, true(dim(1),1,dim(3)), d_mask(:,1:end-1,:)));
    nnz_neighbors = nnz_neighbors + (d_mask & cat(2, d_mask(:,2:end,:), true(dim(1),1,dim(3))));
    nnz_neighbors = nnz_neighbors + (d_mask & cat(3, true(dim(1),dim(2),1), d_mask(:,:,1:end-1)));
    nnz_neighbors = nnz_neighbors + (d_mask & cat(3, d_mask(:,:,2:end), true(dim(1),dim(2),1)));

    total_nnz_neighbors = [total_nnz_neighbors; nnz_neighbors(d_mask)];
    total_d = [total_d; d(d_mask)];

    figure(fig);
    subplot(2,1,1);
    hist(total_nnz_neighbors, [0:6]);
    subplot(2,1,2);
    plot(total_nnz_neighbors +.5*rand(size(total_d)), total_d, '.');
end

return
