function [plan_name] = example_moving_phantom(plan_name);
%example_moving_phantom Script to test phantom

if(~exist('plan_name'))
    nBeams = 7;
    nInstances = 1;
    plan_name = tempname;
    write_moving_RTOG_phantom(plan_name, nBeams,nInstances)
else
    Plan = read_pln([plan_name '.pln']);
    nBeams = Plan.nBeams;
    nInstances = Plan.nInstances;
end


figure('name','Phantom Volumes of Interest');
explore_voi([plan_name '.pln']);

evaluate_plan([plan_name '.pln'],[plan_name '_results_']);

optimize_plan([plan_name '.pln'],[plan_name '_results_']);

Plan = read_pln([plan_name '.pln'])

if(nargout == 0)
    eval(['!rm ' plan_name '*'])
end
