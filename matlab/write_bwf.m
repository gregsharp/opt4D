function write_bwf(bwf_file_name, bwf_struct)
%write_bwf   Write the beam weight file
%
% write_bwf(bwf_file_name, bwf_struct) writes a bwf formatted file to the given
%    file name.  bwf_struct is a structure with (at least) the following
%    fields:
%
%   bwf_struct.beamNumber       - The number of this beam (starting at one)
%   bwf_struct.numPencilBeams   - The number of pencil beams in this beam
%   bwf_struct.beamWeight       - Array containing the dose for each beamlet
%   bwf_struct.spotX            - Array containing the position of the beamlet
%   bwf_struct.spotY            - Array containing the position of the beamlet
%
%    Throws an error if the file writing fails.  See write_bwf.m for a
%    description of the bwf file format.
%
%   See also read_bwf, write_dij, writeKonradFiles.

% Here's the bwf file format:
% 
% The bwf file is a human readible file that gives information about each
% beamlet within a beam.  It starts with a header that gives the beam number,
% the number of pencil beams.  This is followed by a table header and a table of
% values.  The header in more detail:
%
% DB            beamlet number
% Weight        beamlet weight
% SSD           ???
% NP            ???
% Used          ??? - probably 1 if the beamlet is used, 0 otherwise
% cFree         ???
% index         ???
% ray-x y       Position of beamlet relative to beam isocenter in mm
%
% Until we have better information about what these columns mean, default values
% have been used.

% Open file for writing (unix style carriage returns)
[outfid, message] = fopen(bwf_file_name, 'w');
if ~isempty(message)
        error(['Error writing bwf file: ' message]);
end


% Print header information
fprintf(outfid,'%s %d\n','#Beam Number:', bwf_struct.beamNumber-1);
fprintf(outfid,'%s %d\n','#Number of DBs:', bwf_struct.numPencilBeams);

% Print beam weights, etc.  This information should go in bwf_struct instead
% of being hard coded.
fprintf(outfid,'%s','#DB Weight SSD NP Used cFree index ray-x y');
for count=1:bwf_struct.numPencilBeams
    fprintf(outfid,'\n%d %8.6f %8.6f %d %d %d %d %8.6f %8.6f',...
        count-1, bwf_struct.beamWeight(count), 1000.0, 5000, 1, 3, 1500,...
        bwf_struct.spotX(count) * 10, bwf_struct.spotY(count) * 10);
end    

% Close file
status = fclose(outfid);
if status ~= 0
        error(['Error writing bwf file:' lasterr]);
end
