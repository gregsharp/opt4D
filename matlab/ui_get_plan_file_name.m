function [plan_file] = ui_get_plan_file_name
% ui_get_plan_file_name Query user for plan file name
%   Throws an error if the user presses cancel.
%
%   [plan_file_name] = ui_get_plan_file_name

[file_name, path_name, filter_index] = uigetfile( ...
{'*.pln', 'Opt4D Plan File (*.pln)'}, ...
'Pick pln file');
if(filter_index == 0)
    error( 'User did not choose a plan file.');
elseif(filter_index == 1)
    plan_file = [path_name, file_name];
end

