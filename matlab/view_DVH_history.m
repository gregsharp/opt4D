function view_DVH_history(file_prefix, plan)

% Read the DVH history
[dvh, dvh_dose] = read_DVH([file_prefix 'DVH_history.dat']);

% Figure out which step each line in DVH belongs to
belongs_to = cumsum([0; dvh_dose(1:end-1) >= dvh_dose(2:end)]) + 1;

if(isstr(plan))
    plan = read_pln(plan);
end

objective = load([file_prefix 'obj_history.dat']) * [0;1];

constraint_legends = cell(2*numel(plan.VOI));
for(i = 1:numel(plan.VOI))
    constraint_legends{2*i-1} = [plan.VOI(i).name ' underdose'];
    constraint_legends{2*i} = [plan.VOI(i).name ' overdose'];
end
    
multi_objective = load([file_prefix 'multi_obj_history.dat']);
DVH = [dvh_dose, dvh];

temp_legends = {'Overall Objective'};
temp_obj = objective;
for(iConstraint = 1:size(multi_objective,2))
    if(any(isfinite(multi_objective(:,iConstraint)) & ...
            multi_objective(:,iConstraint) > 0))

        temp_legends{end+1} = constraint_legends{iConstraint};
        temp_obj = [temp_obj, multi_objective(:,iConstraint)];
    end
end


figure;
subplot(1,2,2);
semilogy(temp_obj);
legend(temp_legends);
line_h = line([1 1], [min(temp_obj(temp_obj(:) > 0)) max(temp_obj(:))]);
set(line_h, 'color', [0 0 0], 'linestyle', ':');
for(i = min(belongs_to):max(belongs_to))
    subplot(1,2,1);
    plot_DVH(DVH(belongs_to == i, :), plan);
    subplot(1,2,2);
    set(line_h,'XData', [i i])
    if(i < 0 || i > size(temp_obj,1))
        keyboard
    end
    drawnow;
end
