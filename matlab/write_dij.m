function write_dij(outFile, DijStruct)
% write_dij  Writes beam influence matrix in Konrad format
%
%   write_dij(outFile, DijStruct) Writes influence matrix out in bixel format.
%
%   DijStruct is a structure with the following fields:
%
%       gantryAngle: in degrees
%        tableAngle: in degrees
%   collimatorAngle: in degrees
%           spot_dx: width of beamlet (cm)
%           spot_dy: height of beamlet (cm)
%          voxel_dX: column width (cm)
%          voxel_dY: slice thickness (cm)
%          voxel_dZ: row height (cm)
%        doseCubeNc: number of columns
%        doseCubeNs: number of slices
%        doseCubeNr: number of rows
%    numPencilBeams: number of pencil beams in beam
%            energy: array of beam energy used in each beamlet
%             spotX: beamlet location for each beamlet (cm)
%             spotY: beamlet location for each beamlet (cm)
%
% { doseCoefficient: multiply every number in the value cell by this
% {       numVoxels: number of voxels influenced by each beam
% {           index: cell array with index of each influenced voxel
% {           value: cell array with scaled value of each influenced voxel
% or
% {             Dij: sparse influence matrix such that Dij(i,j) is
% {                  the dose to voxel i from beamlet j.
%
%   Throws an error if the file writing fails.
%
%   See also read_dij.

if(~exist('outFile'))
    outFile = [pwd '/default.dij'];
end


outfid = fopen(outFile, 'w');

% Here's the Dij file format:
% it'a a binary file with no "separators" between the entries
% 
% File header:   
% 
% float    a_g, a_t, a_c  ------ Gantry, table and collimator angles
%   
% float    dx_b, dy_b  --- bixel dimensions (in mm)
%   
% float    dx, dy, dz  ---- voxel dimensions (in mm)
% 
% int       Nx, Ny, Nz  ---- dose cube dimensions (number of voxels)
% 
% int       Npb     -------- number of pencil beams (bixels) used for this field
% 
% float	Dose-scalefactor   ---- since the doses are stored as short
%                               integers, this is the conversion factor to the
%		absolute dose ---- for the best resolution,	this will be:
%			max(Dij_entry)/max(short) = max(Dij_entry)/(2^15-1)
% 
%
% This is followed by "Npb" series of entries for individual beam elements
% 
%   
% Beam header:  
% 
% float     Energy
%
% float     spot_x, spot_y ------ position of the bixel with respect to the
%					central axis of the field (in mm)
% int     Nvox   ---- number of voxels with non-zero dose from this beamlet
%   
% Now for each Nvox:
%   
% int     VoxelNumber     ---- voxel ID in the cube (between 1 and Nx*Ny*Nz)
%				this can be ?
% 
% short    Value  ----- multiply this by Dose-scalefactor to get the dose
%			deposited by this beamlet to the voxel VoxelNumber,
%			assuming the beamlet weight of 1
% 
%   
% Now same for the next beamlet etc.
% 
% Of course, the index VoxelNumber is related to the voxel position in the cube.
%   
% KonRad uses a right-handed Cartesian coordinate system for the dose cube:
% 
% for gantry at 0, in beam's eye view, x is pointing to the right, y == up,
% and z == towards the gantry.
% 
% So , the origin of the cube is in the distal lower left corner.
% 
% For a voxel at (x,y,z) w/ respect to the cube origin,  the VoxelNumber =
%					x/dx + ( y/dy + z/dz*Ny )*Nx 
% 

% Convert sparse Dij to proper format if neccessary
if(isfield(DijStruct, 'Dij'))
    % Preallocate information about beamlets
    DijStruct.index = cell(DijStruct.numPencilBeams,1);
    DijStruct.value = cell(DijStruct.numPencilBeams,1);
    DijStruct.numVoxels = zeros(DijStruct.numPencilBeams, 1);

    dummy = full(max(max(DijStruct.Dij)));
    %maxdose = 1;
    maxdose = dummy(1,1)
    DijStruct.doseCoefficient = maxdose / (2^15 - 1);

    % Scale beam_influence by dose coefficient
    beam_influence = DijStruct.Dij ./ DijStruct.doseCoefficient;

    % Store bixel influence data in DijStruct
    for(iBixel=1:DijStruct.numPencilBeams)

        % Find voxels with non-zero contributions from this pencil beam
        [DijStruct.index{iBixel}, dummy, DijStruct.value{iBixel}]...
                = find(beam_influence(:,iBixel));

        DijStruct.numVoxels(iBixel) = length(DijStruct.index{iBixel});
    end

end

% Write header to file:
tmp = [ DijStruct.gantryAngle;
        DijStruct.tableAngle;
        DijStruct.collimatorAngle;
	DijStruct.spot_dx * 10;
	DijStruct.spot_dy * 10;
	DijStruct.voxel_dX * 10;
	DijStruct.voxel_dY * 10;
	DijStruct.voxel_dZ * 10];
fwrite(outfid, tmp, 'float32');
disp(tmp);
disp(DijStruct.voxel_dX * 10);


tmp = [ DijStruct.doseCubeNc;
        DijStruct.doseCubeNs;
        DijStruct.doseCubeNr;
        DijStruct.numPencilBeams];
fwrite(outfid, tmp, 'int');

disp(DijStruct.doseCubeNc);
disp(DijStruct.doseCubeNs);
disp(DijStruct.doseCubeNr);
          

clear tmp;
tmp = [DijStruct.doseCoefficient]
fwrite(outfid, tmp, 'float32');

% Write information for each beamlet
for count=1:DijStruct.numPencilBeams;
    % Beam header
    tmp = [ DijStruct.energy(count);
            DijStruct.spotX(count) * 10;
            DijStruct.spotY(count) * 10];
    fwrite(outfid, tmp, 'float32');
    tmp = [ DijStruct.numVoxels(count)];
    fwrite(outfid, tmp, 'int');
    
    % Write voxel data if there are any voxels
    if(DijStruct.numVoxels(count) > 0)

		% Convert CERR indexes to Konrad indexing
		% First read coordinates in Konrad system from CERR index values
		[tempZ,tempX,tempY] = ind2sub([DijStruct.doseCubeNr,...
			DijStruct.doseCubeNc,  DijStruct.doseCubeNs],...
			DijStruct.index{count});
		% Then calculate indexes for Konrad indexing
		indexV = sub2ind([DijStruct.doseCubeNc, DijStruct.doseCubeNs,...
				DijStruct.doseCubeNr], tempX, tempY, tempZ) - 1;

		% Now rearrange data for storage and write it out
        tmp = uint16(zeros(3,DijStruct.numVoxels(count)));
        tmp(1,:) = bitshift(indexV', 0, 16);
        tmp(2,:) = bitshift(indexV', -16, 16);
        tmp(3,:) = DijStruct.value{count}';
        fwrite(outfid,tmp,'ushort');    
    end
end

% Close file
status = fclose(outfid);
if status ~= 0
	error(['Error writing dij file']);
end
