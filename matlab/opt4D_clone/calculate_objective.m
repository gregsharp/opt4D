function [objective, gradient] = calculate_objective(d, ...
d_min, d_max, q_under, q_over)
%[objective] = calculate_objective(d, d_min, d_max, q_under, q_over)
%
%  Calculate the quadradic objective function.  
%
%  [objective, gradient] = calculate_objective(...) also calculates the 
%  gradient with respect to the dose.

objective = q_over' * (max(0,d-d_max).^2) + q_under' * (max(0,d_min-d).^2);

if(nargout > 1)
    
    % Find gradient
    d_goal = min(d_max, max(d_min,d));
    q_active = q_over;
    q_active(d < d_min) = q_under(d < d_min);

    gradient = 2 * (q_active .* (d - d_goal));

end

return
