function [w, full_objective_history, time_history] = optimize_IMRT(...
                                Dij, d_min, d_max, q_under, q_over, ...
                                w_initial, options);
%OPTIMIZE_IMRT Optimizes IMRT plan in Matlab
%   [w] = optimize_IMRT(Dij, d_min, d_max, q_under, q_over, w_initial)
%   finds the optimium beam weights to use for an IMRT plan.  d_min and d_max
%   are a minimum and maximum constraint for each voxel.  q_under and q_over
%   are the penalty for overdosing and underdosing each voxel.  w_initial is
%   the starting beam weights.
%
%   [w] = optimize_IMRT(..., options) uses the optimization options from the
%   options structure with the following fields possible:
%
%   options.max_steps
%   options.max_time
%   options.min_improvement
%   options.voxel_grid_size
%   options.step_size       The step size rule to use.  Can be 'optimized',
%                           'approximately optimized', or a number.
%   options.step_direction  The step direction rule to use.  Can be
%                            'steepest descent', 'newton',                      
%                            'diagonallized newton', 'conjugate gradient',
%                            'momentum', or 'delta-bar-delta'
%   options.reduce_problem  If true, runs calculate_reduced_problem
%   options.reduction_threshold Threshold of acceptable combination
%   options.dif
%   options.verbosity        How much info to give (0 for silent, 1 for
%                            default, 2 for excessive.)
%
%   options.step_decrement_factor  For fixed step size, decreases step size 
%                            with number of steps.  a / (step + a - 1)

start_time = cputime;

if(~exist('options') || ~isstruct(options))
    options = struct;
end

options

if(~isfield(options, 'verbosity'))
    options.verbosity = 1;
end

if(~isfield(options, 'max_steps'))
    options.max_steps = 100;
end

if(~isfield(options, 'min_improvement'))
    options.min_improvement = 0;
end

if(~isfield(options, 'max_time'))
    options.max_time = inf;
end

if(~isfield(options, 'voxel_grid_size'))
    options.voxel_grid_size = [];
end

if(~isfield(options, 'step_size'))
    options.step_size = 'optimized';
end

if(~isfield(options, 'linesearch_steps'))
    options.linesearch_steps = 3;
end

if(~isfield(options, 'step_direction'))
    options.step_direction = 'diagonallized newton';
end

if(~isfield(options, 'reduce_problem'))
    options.reduce_problem = false;
end

if(~isfield(options, 'reduction_threshold'))
    options.reduction_threshold = 0;
end

if(~isfield(options, 'sampling_fraction'))
    options.sampling_fraction = 1;
else
    % sampling fraction is included, so make sure other sampling
    % variables are specified.
    if(~isfield(options, 'resample_period'))
	options.resample_period = 1;
    end

    if(~isfield(options, 'weighted_sampling'))
	options.weighted_sampling = false;
    end

    if(~isfield(options, 'sampling_weight_update_period'))
	options.sampling_weight_update_period = 1;
    end

    if(~isfield(options, 'adapt_sampling'))
        options.adapt_sampling = false;
    end

    if(~isfield(options, 'sampling_adaptation_exponent'))
        options.sampling_adaptation_exponent = 1.5;
    end
end

if(~isfield(options, 'step_decrement_factor'))
    options.step_decrement_factor = [];
end



%
% optimization_options
%  

% Reduce problem if it is called for
if(options.reduce_problem && isfield(options, 'dif'))
    t = cputime;
    [Cij, d_min, d_max, q_under, q_over, c_to_d] = ...
    calculate_reduced_problem(Dij, d_min, d_max, q_under, q_over, ...
    options.dif, options.reduction_threshold, options.reduction_fraction);
    disp(['Reducing the problem from ' num2str(size(c_to_d, 2)) ' voxels'...
          ' to ' num2str(size(c_to_d, 1)) ' voxels'...
	  ' took ' num2str(cputime - t) ' seconds.']);

    dif = options.dif;
    dose_difference = reshape(full(sum(Dij,2) - sum(c_to_d*Cij,2)), ...
         [dif.nZ dif.nX dif.nY]);
    for(iSlice = 1:dif.nY)
        figure('name', ['Slice ' num2str(iSlice)]);
        imagesc(dose_difference(:,:,iSlice));
        title('Difference between nominal dose');
        colorbar;
    end
    Dij = Cij;
    clear Cij dose_difference;
end

% Find mask of sampled voxels if needed
if(options.sampling_fraction < 1)
    sample_probability = repmat(options.sampling_fraction, numel(d_min), 1);

    sample_mask = rand(size(d_min)) < sample_probability;

    t = cputime;
    full_Dij_t = Dij';
    full_d_min = d_min;
    full_d_max = d_max;
    full_q_under = q_under;
    full_q_over = q_over;

    Dij = full_Dij_t(:,sample_mask)';
    d_min = full_d_min(sample_mask);
    d_max = full_d_max(sample_mask);
    q_under = full_q_under(sample_mask);
    q_over = full_q_over(sample_mask);

    disp(['Sampling the problem from ' num2str(numel(full_d_min)) ' voxels'...
          ' to ' num2str(numel(d_min)) ' voxels'...
	  ' took ' num2str(cputime - t) ' seconds.']);

end


% Store useful information
objective_history = zeros(1,options.max_steps);
full_objective_history = zeros(1,options.max_steps);
alpha_history = zeros(1,options.max_steps);
time_history = zeros(1,options.max_steps);
nVoxels = numel(d_min);
nNonairVoxels = sum(d_max < inf);

% Calculate and print zero-dose objective
[zero_dose_objective, x] = calculate_objective(zeros(nVoxels, 1), ...
d_min, d_max, q_under, q_over);

zero_dose_objective
zero_dose_gradient = (x' * Dij);

% Calculate initial objective
objective = calculate_objective(Dij * w_initial, ...
d_min, d_max, q_under, q_over);
if(options.sampling_fraction < 1)
    full_objective = calculate_objective((w_initial' * full_Dij_t)', ...
       full_d_min, full_d_max, full_q_under, full_q_over);
else
    full_objective = objective;
end
alpha_history(1) = 0;
objective_history(1) = objective;
full_objective_history(1) = full_objective;
time_history(1) = cputime - start_time;




% Precompute Transpose

% Dij_transpose = Dij';
% Dij_transpose_sum_of_squares_inv = sum(Dij.^2,2).^-1;

% Run first step of optimization
step_size = 1;
max_step_size = inf;

% Start with initial beam weights
w = w_initial;
d = Dij * w;

% Find goal dose
% d_goal = d;
d_goal = min(d_max, max(d_min,d));

% Find cost of error
q_active = ones(size(d));
q_active(d < d_min) = q_under(d < d_min);
q_active(d > d_max) =  q_over(d > d_max);

% Store starting objective
objective = q_active' * ((d - d_goal).^2);


% Determine initial direction to move
gradient = 2 * ((q_active .* (d - d_goal))' * Dij)';
switch lower(options.step_direction)
  case {'steepest descent'}
    w_direction = -gradient;
  case {'diagonallized newton'}
    w_direction = -0.5 * (q_active' * (Dij.^2)).^-1' .* gradient;
  case {'newton'}
    w_direction = -0.5 * inv(Dij' * sparse(diag(q_active)) * Dij) * gradient;
  case {'conjugate gradient'}
    w_direction = -gradient;
    if(isnumeric(options.step_size))
	error('Momentom method requires optimized step size');
    end
  case {'momentum'}
    if(~isfield(options, 'momentum_factor'))
	error('Momentum method used, but momentum factor not supplied.');
    end
    if(~isnumeric(options.step_size))
	error('Momentom method requires fixed step size');
    end
    w_direction = -gradient;
  case {'delta-bar-delta'}
    if(~isfield(options, 'learning_step_size'))
	error('delta-bar-delta method used, but learning step size not supplied.');
    end
    if(~isfield(options, 'learning_decrement_factor'))
	error('delta-bar-delta method used, but learning_decrement_factor not supplied.');
    end
    if(~isfield(options, 
	    'gradient_smoothing_coefficient'))
	error('delta-bar-delta method used, but gradient_smoothing_coefficient not supplied.');
    end
    if(~isnumeric(options.step_size))
	error('delta-bar-delta method requires fixed step size');
    end
    w_direction = -gradient;
    smoothed_last_gradient = gradient;
    learning_rate = ones(size(gradient));
  case {'local gradient step size adaptation'}
    if(~isfield(options, 'meta_step_size'))
        error('local gradient step size adaptation used, but meta_step_size not supplied.');
    end
    if(~isfield(options, 'gradient_smoothing_coefficient'))
        error('local gradient step size adaptation used, but gradient_smoothing_coefficient not supplied.');
    end
    if(~isnumeric(options.step_size))
        error('local gradient step size adaption requires fixed step size');
    end
    w_direction = -gradient;
    smoothed_last_gradient = gradient;
    learning_rate = ones(size(gradient));
  case {'stochastic meta descent'}
    if(~isnumeric(options.step_size))
        error('stochastic meta descent requires fixed step size');
    end
    if(~isfield(options, 'SMD_smoothing_coefficient'))
        error(['stochastic meta descent used, but ', ...
        'gradient_smoothing_coefficient not supplied.']);
    end
    if(~isfield(options, 'SMD_meta_stepsize'))
        error(['stochastic meta descent used, but ', ...
        'gradient_smoothing_coefficient not supplied.']);
    end
    w_direction = -gradient;
    smd_v_vector = zeros(size(gradient));
    learning_rate = ones(size(gradient));


  otherwise
    error('Unknown descent method.');
end

if(options.verbosity >= 1)
    fig = figure;
end

% % Determine initial damping factor
if(isnumeric(options.step_size))
    d_temp = Dij * max(0, w + step_size * w_direction);
    prescribed_dose = sum(d_max(d_min > 0))
    delivered_dose = sum(d_temp(d_min > 0))
    step_size =  prescribed_dose / delivered_dose;
else
    iterations = options.linesearch_steps;

    % Find optimal step size
    if(options.verbosity >= 1)
	[step_size, objective, alpha_hist, F_hist] = ...
	    find_optimal_stepsize(Dij, w, w_direction, d_min, d_max, ...
	    q_under, q_over, step_size, max_step_size, iterations);

	figure(fig); subplot(2,2,3);
	plot(alpha_hist,F_hist,':',alpha_hist,F_hist,'x',step_size,objective,'o');
        title('Finding the optimal alpha');
        ylabel('Objective(\alpha)');
        xlabel('\alpha');

    else
        [step_size, objective] = find_optimal_stepsize( ...
            Dij, w, w_direction, d_min, d_max, q_under, q_over, ...
            step_size, max_step_size, iterations);
    end
end

max_step_size = 10 * step_size;
max_step_size = inf;


% Take first step according to step size figured out
w = max(0, w + step_size * w_direction);

% Scale future steps according to step size multiplier if needed
if(isnumeric(options.step_size))
    step_size = step_size * options.step_size
    kappa = step_size;
end

% Set initial learning rate and step size if needed
if(exist('learning_rate'))
    learning_rate = learning_rate * step_size;
    step_size = 1;
end

% Calculate objective and full objective if needed
objective = calculate_objective(Dij * w, d_min, d_max, q_under, q_over);
if(options.sampling_fraction < 1)
    full_objective = calculate_objective((w' * full_Dij_t)', ...
       full_d_min, full_d_max, full_q_under, full_q_over);
else
    full_objective = objective;
end

alpha_history(2) = step_size;
objective_history(2) = objective;
full_objective_history(2) = full_objective;
time_history(2) = cputime - start_time;

steps = 2;
last_objective = objective;
while(steps < options.max_steps && cputime - start_time < options.max_time)
    steps = steps + 1;

    % Resample if needed
    if(options.sampling_fraction < 1 ...
	&& (mod(steps,options.resample_period) == 0))

        if(options.adapt_sampling && (objective > last_objective))
            sample_probability = sample_probability * ...
            options.sampling_adaptation_exponent;
        end

	% Update sampling probabilities if needed
	if(options.weighted_sampling == true ...
	    && mod(steps,options.sampling_weight_update_period) == 0)

            % Calculate dose
            d = (w' * full_Dij_t)';

	    % Calculate objective for each voxel
	    obj = (max(d - full_d_max, 0) .^2) .* full_q_over ...
		+ (max(full_d_min - d, 0) .^2) .* full_q_under;
	    figure(180); hist(obj);


	    sampling_threshold = 10^-20;
	    sample_probability = max(sample_probability ...
		, min(obj ./ sampling_threshold, 1));
	    mean(sample_probability)
	    options.sampling_fraction
	    % keyboard
            % sample_probability = options.sampling_fraction;
        
	end

	sample_mask = rand(size(full_d_min)) < sample_probability;
	Dij = full_Dij_t(:,sample_mask)';
	d_min = full_d_min(sample_mask);
	d_max = full_d_max(sample_mask);
	q_under = full_q_under(sample_mask) ./ ...
	    sample_probability(sample_mask);
	q_over = full_q_over(sample_mask) ./ ...
	    sample_probability(sample_mask);
    end


    % Calculate dose
    d = Dij * w;

    if(options.verbosity >= 1)
        % Show current progress
        figure(fig);
        subplot(2,2,1);
        plot(w);
        title('Input Fluence');
        figure(fig);
        subplot(2,2,2);
        if(~isempty(options.voxel_grid_size))
            if(options.reduce_problem)
                mesh(reshape(c_to_d * d,options.voxel_grid_size));
            else
                mesh(reshape(d,options.voxel_grid_size));
            end
        else
            if(options.reduce_problem)
                plot(c_to_d * d);
	    else
		plot(d);
	    end
	end
	title('Expected Dose');
	subplot(2,2,4);
	semilogy(full_objective_history);
	title('Objective History');
	ylabel('Objective');
	xlabel('Optimization Step');
    end

    % Find goal dose
    d_goal = min(d_max, max(d_min,d));

    % Find cost of error
    q_active = ones(size(d));
    q_active(d < d_min) = q_under(d < d_min);
    q_active(d > d_max) =  q_over(d > d_max);

    % Determine direction to move
    last_direction = w_direction;
    last_gradient = gradient;
    gradient = 2 * ((q_active .* (d - d_goal))' * Dij)';
    % gradient(gradient < 0 & w < 0) = 0;
    switch lower(options.step_direction)
      case {'steepest descent'}
	w_direction = -gradient;
      case {'diagonallized newton'}
        % q_active = (q_under .* (d < d_min) + q_over .* (d > d_max));
	w_direction = - (((sqrt(2*q_active') * Dij).^2).^-1)' .* gradient;
	% w_direction = -0.5 * (q_active' * (Dij.^2)).^-1' .* gradient;
      case {'newton'}
	q_active = (q_under .* (d < d_min) + q_over .* (d > d_max));
	w_direction = -.5 * inv(Dij'*sparse(diag(q_active))*Dij)*gradient;
      case {'conjugate gradient'}
	beta = gradient' * (gradient - last_gradient) ...
	      / (last_gradient' * last_gradient);
	% Apply reset
	% if(mod(steps,10) == 0)
	%     beta = 0;
	% end
	w_direction = -gradient + beta * last_direction;
      case {'momentum'}
	w_direction = -(1 - options.momentum_factor) * gradient ...
	+ options.momentum_factor * last_direction;
      case {'delta-bar-delta'}
	smoothed_last_gradient = ...
	    (1 - options.gradient_smoothing_coefficient) * last_gradient ...
	    + options.gradient_smoothing_coefficient * smoothed_last_gradient;
	
	learning_rate(smoothed_last_gradient .* gradient > 0) = ...
	    learning_rate(smoothed_last_gradient .* gradient > 0) ...
	    + options.learning_step_size;
	learning_rate(smoothed_last_gradient .* gradient < 0) = ...
	    learning_rate(smoothed_last_gradient .* gradient < 0) ...
	    * (1 - options.learning_decrement_factor);

	if(options.verbosity >= 2)
	    figure(100); plot(learning_rate);
	end

        w_direction = -gradient .* learning_rate;
      case {'local gradient step size adaptation'}
        smoothed_last_gradient = ...
            (1 - options.gradient_smoothing_coefficient) * last_gradient ...
            + options.gradient_smoothing_coefficient * smoothed_last_gradient;
	

            % figure;
            % plot(options.meta_step_size * gradient .* learning_rate .* ...
            % last_gradient);
            % keyboard;

        learning_rate = learning_rate .* max(.5, 1 + options.meta_step_size ...
            * gradient .* learning_rate .* smoothed_last_gradient);

	if(options.verbosity >= 2)
	    figure(100); plot(learning_rate);
	end
	w_direction = -gradient .* learning_rate;
      case {'stochastic meta descent'}
          lambda = options.SMD_smoothing_coefficient;
	  mu = options.SMD_meta_stepsize;

          % Calculate new learing rate for each bixel
          learning_rate = learning_rate .* max(.5, 1 + ...
	  mu .* smd_v_vector .* gradient);

          % Calculate direction of motion
          w_direction = - learning_rate .* gradient;
          
          % Calculate constrained gradient
          w_next = max(0, w + w_direction);
          constrained_gradient = (w - w_next) ./ learning_rate;

          % Calculate Hessian * smd v vector
          Hv = 2 * ((q_active .* (Dij * smd_v_vector))'*Dij)';
          
          % Update smd v vector
          smd_v_vector = lambda .* smd_v_vector ...
          + learning_rate .*(constrained_gradient - lambda * Hv);

          figure(200);
          subplot(2,2,1);
          plot(learning_rate); title('learning rate');
          subplot(2,2,2); plot([gradient, constrained_gradient]); 
          % legend('gradient', 'constrainted gradient');
          subplot(2,2,3); plot(Hv); title('Hv');
          subplot(2,2,4); plot(smd_v_vector); title('v');

          % keyboard
      otherwise
        disp('Unknown method.')
    end

    last_objective = objective;

    % Determine step size
    if(isnumeric(options.step_size))

        % Decrease step size if desired
	if(~isempty(options.step_decrement_factor))
            step_size = kappa * (options.step_decrement_factor)...
            / (options.step_decrement_factor + steps - 1);
        end

        % Calculate objective
        objective = calculate_objective(Dij * max(0, w + step_size * w_direction), d_min, d_max, q_under, q_over);

    elseif(strcmpi(options.step_size, 'optimized'))
        
	% Find optimal step size
	if(options.verbosity >= 1)
	    [step_size, objective, alpha_hist, F_hist] = ...
                find_optimal_stepsize(Dij, w, w_direction, d_min, d_max, ...
                q_under, q_over, step_size, max_step_size, ...
                options.linesearch_steps);

	    figure(fig); subplot(2,2,3);
	    plot(alpha_hist,F_hist,':',alpha_hist,F_hist,'x', ...
	    step_size,objective,'o');
	    title('Finding the optimal alpha');
	    ylabel('Objective(\alpha)');
	    xlabel('\alpha');
            line([0 step_size], ...
            F_hist(1) + [0, w_direction' * gradient * step_size]);
            gradient(gradient < 0 & w < 0) = 0;
            line([0 step_size], ...
            F_hist(1) + [0, w_direction' * gradient * step_size]);
        else
	    [step_size, objective] = find_optimal_stepsize( ...
		Dij, w, w_direction, d_min, d_max, q_under, q_over, ...  
                step_size, max_step_size, ...
                options.linesearch_steps);
	end

    elseif( strcmpi(options.step_size, 'approximately optimized'))
        
        % Find optimal step size
        [step_size, objective] = find_approximately_optimal_stepsize( ...
            Dij*w, Dij*w_direction, d_min, d_max, q_under, q_over, step_size);
    else
        error(['options.step_size option unrecognized: ' ...
               options.step_size]);
    end 

    % objective = sqrt(objective / nNonairVoxels);
 
    alpha_history(steps) = step_size;
    objective_history(steps) = objective;

    if(options.verbosity >= 1)
	disp(['Step: ' num2str(steps) ' Objective: ' num2str(objective) ...
	' time: ' num2str(cputime - start_time)]);
    end

    w = max(0,w + step_size * w_direction);

    % Calculate the objective for the full case
    if(options.sampling_fraction < 1)
	full_objective = calculate_objective((w' * full_Dij_t)', ...
           full_d_min, full_d_max, full_q_under, full_q_over);
    else
        full_objective = objective;
    end
    full_objective_history(steps) = full_objective;
    time_history(steps) = cputime - start_time;

    % Test if stopping criteria ia met
    if(objective <= 0 || isinf(objective) || ...
        abs(last_objective - objective)/objective < options.min_improvement)
        break
    end

end

disp(['Optimizing the problem for ' num2str(steps) ' steps'...
      ' took ' num2str(cputime - start_time) ' seconds.']);

if(options.verbosity >= 2)
    figure('name','Objective History');
    subplot(1,2,1);
    semilogy(objective_history);
    title('Objective History');
    xlabel('Step');
    ylabel('Objective Function');

    subplot(1,2,2);
    plot(alpha_history);
    title('Step Size History');
    xlabel('Step');
    ylabel('Optimal Step Size');

    if(options.sampling_fraction < 1)
	figure('name', 'Full Objective History');
	semilogy([objective_history', ...
	    full_objective_history']);
	legend('Sampled Objective', 'True Objective');
	title(['Sampled vs. True Objective, ('...
	    num2str(options.sampling_fraction) ')']);
	xlabel('Step');
	ylabel('Objective Value');
    end
end


return



% -----------------------------------------
function [position_offsets] = calculate_position_offset(nPositions)
% [position_offsets] = calculate_position_offset(nPositions)
%
%  Generate position offsets


% Generate setup error
% Assumes zero mean normally distributed error of variance one
% setup_error = randn(2,nPositions);
setup_error = 2 * randn(2,nPositions) + 2 * randn(2,1) * ones(1,nPositions);

% Generate starting phase for breathing
breathing_start_phase = rand(1,nPositions);

% Generate corresponding breathing position
breathing_position = [zeros(1,nPositions);...
                      20 * cos(pi*breathing_start_phase).^6];

% Combind breathing and setup errors:
position_offsets = setup_error + breathing_position;
% position_offsets = 4* setup_error;


return

