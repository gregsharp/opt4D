function [w] = optimize_plan(plan, options, legends)
%optimize_plan Optimize a konrad plan using Matlab
%
%    w = optimize_plan(plan) uses default settings.
%
%    w = optimize_plan(plan, options, legends) uses a cell array of options
%    and legends and prints a comparison plot.

% Read plan file
if(isstr(plan))
    plan = read_pln(plan);
end

% Prepare input variables
if(~exist('options'))
    options = struct;
end

if(~iscell(options))
    options = {options};
end

if(~exist('legends'))
    legends = 'With default options';
end

if(~iscell(legends))
    legends = {legends};
end


% Read dimension information file
dif = read_dif(plan.dif_file_name);

nRows = dif.nZ;
nColumns = dif.nX;
nSlices = dif.nY;
nVoxels = nRows * nColumns * nSlices;
nVois = length(plan.VOI);

% Read Voi file
voi_list = reshape(read_voi(plan), nVoxels, 1);

% Find prescriptions for each voxel
[d_min, d_max, q_under, q_over] = find_voxel_constraints(plan);

global Dij;
global bixel_info;
if(isempty(Dij))
    [Dij, bixel_info] = read_simplified_dij(plan);
end
nBixels = length(bixel_info.beamNo);

w_initial = zeros(nBixels, 1);


fig = figure('name','Convergence of IMRT optimization');

obj_histories = [];
time_histories = [];


for(iOption = 1:numel(options))
    temp_options = options{iOption};
    temp_options.dif = dif;
    [w, obj_hist, time_hist] = optimize_IMRT(Dij, d_min, d_max, ...
        q_under, q_over, w_initial, temp_options);

    obj_histories = [obj_histories, obj_hist(:)];
    time_histories = [time_histories, time_hist(:)];

    figure(fig);
    subplot(2,1,1);
    semilogy(obj_histories);
    xlabel('Optimization Step');
    subplot(2,1,2);
    semilogy(time_histories, obj_histories);
    xlabel('Time (s)');
end

figure(fig);
subplot(2,1,1);
legend(legends);
subplot(2,1,2);
legend(legends);

return

dose = Dij * w;

% Examine results

% Show DVH
figure;
dvh = calculate_dvh(dose, 0.1, voi_list, nVois);
plot_DVH(dvh, plan);

% Show intensity maps
figure;
plot_intensity_maps(w, bixel_info);

% figure;
figure;
explore_dose(dose,plan);

