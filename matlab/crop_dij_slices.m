function crop_dij_slices(plan, output_file_root, min_slice, max_slice, ...
beamlet_threshold)
%crop_dij_slices  Creates new Konrad files with fewer slices.
%   The resulting plan file can be run quicker than the original for 
%   optimization.
%
%   crop_dij_slices(plan, output_file_root, min_slice, max_slice)
%   will create a new plan based on the original plan with the slices between
%   min_slice and max_slice included.  If output_file_root is 'cropped' and the 
%   plan has 5 beams, the following files will be created in the current
%   working directory:
%                       cropped.pln
%                       cropped.dif
%                       cropped.voi
%                       cropped_1.dij
%                           :
%                       cropped_5.dij
%
%   crop_dij_slices(plan, output_file_root) will crop to just the isocenter
%   slice.
%
%   crop_dij_slices(plan, output_file_root, min_slice, max_slice, threshold)
%   will only include beamlets that have an influence above the given
%   threshold.  A threshold can be anywhere from 0 to 65535, but I'd reccomend
%   a threshold from 1,000 to 10,000.  Thresholding is useful to remove the
%   beamlets that only contribute scatter to the cropped plan.

% Test for valid inputs
if(~exist('output_file_root'))
    error('Output folder must be defined');
end

if(~exist('beamlet_threshold'))
    beamlet_threshold = 0;
end

% Read plan file if given as a file name
if(isstr(plan))
    plan = read_pln(plan);
end

% Define new plan structure
new_plan = plan;
new_plan.plan_file_root = output_file_root;
new_plan.dif_file_name = [output_file_root '.dif'];
new_plan.voi_file_name = [output_file_root '.voi'];

% Write new plan file
write_pln([output_file_root '.pln'], new_plan);

% Read dif
old_dif = read_dif(plan.dif_file_name);
if(~exist('min_slice'))
    min_slice = old_dif.isoY;
    max_slice = old_dif.isoY;
end

slices = [min_slice:max_slice]';

% Create new dif file
new_dif = old_dif;
new_dif.nY = length(slices);
if(ismember(old_dif.isoY,slices))
    new_dif.isoY = find(slices==old_dif.isoY);
else
    new_dif.isoY = 1;
end
write_dif(new_plan.dif_file_name, new_dif);

% Create cropped voi file
voiField = read_voi(plan);
voiField = voiField(:,:,slices);
write_voi(new_plan.voi_file_name, voiField);
clear voiField;


% Crop dij files
for(iInstance = 1:plan.nInstances)
    for(iBeam = 1:plan.nBeams)
        disp(['Cropping instance: ' num2str(iInstance) ', beam: ' num2str(iBeam)]);
        if(iInstance == 1)
            dij_suffix = ['_' num2str(iBeam) '.dij'];
        else
            dij_suffix = ['_' num2str(iInstance), '_' num2str(iBeam) '.dij'];
        end
        DijStruct = read_dij([plan.plan_file_root dij_suffix]);
	new_DijStruct = DijStruct;
	new_DijStruct.doseCubeNs = length(slices);

        for(iBeamlet = 1:DijStruct.numPencilBeams)
            index = DijStruct.index{iBeamlet};
            value = DijStruct.value{iBeamlet};
        
            [tempR, tempC, tempS] = ind2sub([DijStruct.doseCubeNr,...
		DijStruct.doseCubeNc,  DijStruct.doseCubeNs], ...
                index);
        
            mask = ismember(tempS, slices);
            new_DijStruct.value{iBeamlet} = value(mask);

            % Eliminate beamlet if it is below threshold
            % disp(num2str(max(new_DijStruct.value{iBeamlet})));
            if(max(new_DijStruct.value{iBeamlet}) < beamlet_threshold)
                new_DijStruct.index{iBeamlet} = [];
                new_DijStruct.value{iBeamlet} = [];
            else
                % Renumber indexes
                new_tempR = tempR(mask);
                new_tempC = tempC(mask);
                new_tempS = tempS(mask) - min_slice + 1;
                            
                index = sub2ind([new_DijStruct.doseCubeNr,...
                    new_DijStruct.doseCubeNc, new_DijStruct.doseCubeNs], ...
                    new_tempR, new_tempC, new_tempS);
                
                new_DijStruct.index{iBeamlet} = index;
            end

            new_DijStruct.numVoxels(iBeamlet) = ...
                    length(new_DijStruct.index{iBeamlet});

        end
	write_dij([new_plan.plan_file_root dij_suffix], new_DijStruct);
    end
end
        
