function [c_to_d, d_to_c] = calculate_voxel_clustering(D, voi_list, dif, ...
    threshold, fraction_reduction)
%calculate_voxel_clustering Calculate best voxel clustering for IMRT problem
%   Finds the clustering that results in the least error in the dose
%   calculation of the reduced problem.  Return the reduction matrix d_to_c and
%   the expansion matrix, c_to_d, such that
%      c = d_to_c * d
%   and
%      d = c_to_d * c
%
%   This mapping allows one to generate a reduced version of an IMRT problem.
%
%   [c_to_d, d_to_c] = calculate_voxel_clustering(D, voi_list, dif, ...
%   threshold, fraction_reduction) calculates the two mapping matrices based
%   on dose-influence matrix D, a list of the voi that each voxel belongs to,
%   voi_list, a dif structure (see read_dif), and (optionally) the threshold
%   of voxels to be combined and the fraction of voxels to combine.
%
%   To see the resultant mapping, the following code is useful:
%       belongs_to = zeros(nVoxels, 1);
%       for(i = 1:nVoxels)
%           belongs_to(i) = find(d_to_c(:,i));
%       end
%       figure;
%       image(reshape(mod(belongs_to(dif.nX * dif.nZ + (1:dif.nX*dif.nZ)),...  
%           256), dif.nZ, dif.nX));
%       colormap(rand(256, 3));
%   
%   See also calculate_reduced_problem.

if(~exist('threshold'))
    threshold = 0;
end

if(~exist('fraction_reduction'))
    fraction_reduction = 0;
end

nVoxels = size(D,1);
nBixels = size(D, 2);
nRows = dif.nZ;
nColumns = dif.nX;
nSlices = dif.nY;

% Split D into rows
tic
D = D';
disp('Transposed D');
toc

C_row = cell(nVoxels,1);
h = waitbar(0, 'Split D into rows'); tic
for(iVoxel = 1:nVoxels)
    C_row{iVoxel} = D(:,iVoxel);
    if(mod(iVoxel, dif.nX * dif.nZ) == 0)
        waitbar(iVoxel/nVoxels, h);
    end
end
close(h); toc
D = D';


% List each member voxel of the new merged voxels
members = cell(nVoxels,1);
for(i = 1:nVoxels)
    members{i} = [i];
end

% Find the neighbors for each voxel
neighbors = cell(nVoxels,1);

relative_neighbors = [- nRows * nColumns; -nRows; -1; ...
                1; nRows; nRows * nColumns];

h = waitbar(0, 'Finding neighbors'); tic
iVoxel = 0;
for(iSlice = 1:nSlices)
    for(iRow = 1:nRows)
        for(iColumn = 1:nColumns)
            iVoxel = iVoxel + 1;
            temp_neighbors = iVoxel + relative_neighbors;
            temp_neighbors = temp_neighbors(temp_neighbors > 0 & ...
                temp_neighbors <= nVoxels);

            neighbors{iVoxel} = temp_neighbors( ...
                voi_list(temp_neighbors) == voi_list(iVoxel));
        end
    end
    waitbar(iSlice / nSlices);
end
close(h); toc

% Find the distance from each voxel to each neighbor
neighbor_distances = cell(nVoxels, 1);
min_neighbor_distance = repmat(inf, nVoxels, 1);
closest_neighbor = ones(nVoxels,1);

h = waitbar(0, 'Finding distance to neighbors'); tic
for(iVoxel = 1:nVoxels)
    temp_neighbors = neighbors{iVoxel};
    temp_neighbor_distances = zeros(size(temp_neighbors));
    
    if(numel(temp_neighbors) > 0)
        for(iNeighbor = 1:numel(temp_neighbors))
            temp_neighbor_distances(iNeighbor) = mean(...
                (C_row{iVoxel} - C_row{temp_neighbors(iNeighbor)}).^2);
        end
    
        [min_neighbor_distance(iVoxel), closest_neighbor(iVoxel)] = ...
            min(temp_neighbor_distances);
    end
    if(mod(iVoxel, dif.nX * dif.nZ) == 0)
        waitbar(iVoxel/nVoxels, h);
    end

    neighbor_distances{iVoxel} = temp_neighbor_distances;
end
close(h); toc

nCombined = 0;

[smallest_distance, closest_voxel] = min(min_neighbor_distance);
while(smallest_distance <= threshold ...
    || (isfinite(smallest_distance) ...
        && nCombined < (nVoxels * fraction_reduction)))

    nCombined = nCombined + 1;

    % Combine one voxel with it's neighbor
    % Keep C_row, neighbors, neighbor_distances,
    % min_neighbor_distance, closest_neighbor,
    % closest_voxel, and smallest_distance valid by end of each iteration

    voxelNo = closest_voxel;
    neighborNo = neighbors{voxelNo}(closest_neighbor(voxelNo));

    % Remove voxelNo from neighbor's neighbor list
    neighbor_distances{neighborNo} = ...
        neighbor_distances{neighborNo}(neighbors{neighborNo} ~= voxelNo);
    neighbors{neighborNo} = ...
        neighbors{neighborNo}(neighbors{neighborNo} ~= voxelNo);

    % Remove neigborNo from voxel's neighbor list
    neighbor_distances{voxelNo} = ...
        neighbor_distances{voxelNo}(neighbors{voxelNo} ~= neighborNo);
    neighbors{voxelNo} = ...
        neighbors{voxelNo}(neighbors{voxelNo} ~= neighborNo);

    if(smallest_distance == 0)
        % The rows are the same, so no need to update distances

        % Update the neighbors of the neighbors to point at voxelNo
        neighbors_of_neighbor = neighbors{neighborNo};
        for(i = 1:numel(neighbors_of_neighbor))
            % This might leave the neighbor of the neighbor pointing at voxelNo
            % more than once, but since the distance between voxelNo and 
            % neighborNo is zero, the distances do not need to be updated
            neighbors{neighbors_of_neighbor(i)}(...
                neighbors{neighbors_of_neighbor(i)} == neighborNo) = voxelNo;

            % Note that the closest neighbor does not need to be updated since
            % it is relative to the order that they are stored, which is not 
            % changed
        end

        % Find all neighbors of new combined voxel
        temp_neighbors = [neighbors{voxelNo}; neighbors{neighborNo}];
        temp_neighbor_distances = ...
            [neighbor_distances{voxelNo}; neighbor_distances{neighborNo}];

        % Make neighbors unique
        [temp_neighbors, I, J] = unique(temp_neighbors);
        temp_neighbor_distances = temp_neighbor_distances(I);

        neighbors{voxelNo} = temp_neighbors;
        neighbor_distances{voxelNo} = temp_neighbor_distances; 

        % Find new closest neighbor
        if(numel(temp_neighbors) > 0)
            [min_neighbor_distance(voxelNo), closest_neighbor(voxelNo)] = ...
                min(temp_neighbor_distances);
        else
            min_neighbor_distance(voxelNo) = inf;
            closest_neighbor(voxelNo) = 0;
        end
    else
        % Find the new weight that is the average of all combined voxels
        C_row{closest_voxel} = (numel(members{voxelNo}) .* C_row{voxelNo}...
            + numel(members{neighborNo}) .* C_row{neighborNo}) ...
            ./ (numel(members{voxelNo}) + numel(members{neighborNo}));


        % Update the neighbors of the neighbors to point at voxelNo
        neighbors_of_neighbor = neighbors{neighborNo};
        for(i = 1:numel(neighbors_of_neighbor))
            % This might leave the neighbor of the neighbor pointing at voxelNo
            % more than once
            neighbors{neighbors_of_neighbor(i)}(...
                neighbors{neighbors_of_neighbor(i)} == neighborNo) = voxelNo;
        end

        % Find all neighbors of new combined voxel
        temp_neighbors = unique([neighbors{voxelNo}; neighbors{neighborNo}]);
        temp_neighbor_distances = zeros(size(temp_neighbors));

        % Compute new neighbor distances
        for(iNeighbor = 1:numel(temp_neighbors))
            temp_neighborNo = temp_neighbors(iNeighbor);
            temp_distance = mean((C_row{voxelNo} - C_row{temp_neighborNo}).^2);

            temp_neighbor_distances(iNeighbor) = temp_distance;
            
            % Update distance stored in neighbor
            neighbor_distances{temp_neighborNo}(...
                neighbors{temp_neighborNo} == voxelNo) = temp_distance;

            % update closest voxels for neighbor
            [min_neighbor_distance(temp_neighborNo), ...
                closest_neighbor(temp_neighborNo)] = min(...
                    neighbor_distances{temp_neighborNo});
        end

        neighbors{voxelNo} = temp_neighbors;
        neighbor_distances{voxelNo} = temp_neighbor_distances; 

        % Find new closest neighbor
        if(numel(temp_neighbors) > 0)
            [min_neighbor_distance(voxelNo), closest_neighbor(voxelNo)] = ...
                min(temp_neighbor_distances);
        else
            min_neighbor_distance(voxelNo) = inf;
            closest_neighbor(voxelNo) = 0;
        end
    end

    % Update member lists
    members{voxelNo} = [members{voxelNo}; members{neighborNo}];
    members{neighborNo} = [];

    % Zero out neighbor
    C_row{neighborNo} = spalloc(1, nBixels, 0);
    neighbors{neighborNo} = [];
    neighbor_distances{neighborNo} = [];
    closest_neighbor(neighborNo) = 0;
    min_neighbor_distance(neighborNo) = inf;

    % Find new closest voxel to combine
    [smallest_distance, closest_voxel] = min(min_neighbor_distance);
end

% Initialize c_to_d matrix such that d is approximately equal to c_to_d * c
% When we combine voxels, we will add the column of the neighbor to the column
% of the voxel.  d_to_c is the transpose of c_to_d but with each row normalized
i = [1:nVoxels].';
j = ones(nVoxels, 1);
s = ones(nVoxels, 1);
for(iVoxel = 1:nVoxels)
    j(members{iVoxel}) = iVoxel;
end
c_to_d = sparse(i,j,s,nVoxels,nVoxels);

% Squeeze c_to_d
tic
voxel_volume = sum(c_to_d);
c_to_d = c_to_d(:, voxel_volume > 0);
spy(c_to_d);
drawnow
toc

% Calculate d_to_c
tic
voxel_volume = sum(c_to_d);
[i, j, s] = find(c_to_d);
d_to_c = sparse(j, i, s ./ voxel_volume(j)');
toc

return
