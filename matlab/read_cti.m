function [cti] = read_cti(cti_file_name, hed_file_name)
% read cti file 

% Make sure CTI file exists
if(findstr(cti_file_name, 'CTI') || findstr(cti_file_name, 'cti'))
    if(~exist(cti_file_name, 'file'))
	error(['CTI file name invalid: ' cti_file_name]);
    end
elseif(exist([cti_file_name '.CTI'], 'file'))
    cti_file_name = [cti_file_name '.CTI'];
elseif(exist([cti_file_name '.cti'], 'file'))
    cti_file_name = [cti_file_name '.cti'];
else
    error(['CTI file name invalid: ' cti_file_name]);
end


% Find hed file name
if(~exist('hed_file_name'))
    if(findstr(cti_file_name, 'CTI'))
	if(exist(strrep(cti_file_name, 'CTI', 'HED'), 'file'))
	    hed_file_name = strrep(cti_file_name, 'CTI', 'HED');
	elseif(exist(strrep(cti_file_name, 'CTI', 'hed'), 'file'))
	    hed_file_name = strrep(cti_file_name, 'CTI', 'hed');
	end
    elseif(findstr(cti_file_name, 'cti'))
	if(exist(strrep(cti_file_name, 'cti', 'HED'), 'file'))
	    hed_file_name = strrep(cti_file_name, 'cti', 'HED');
	elseif(exist(strrep(cti_file_name, 'cti', 'hed'), 'file'))
	    hed_file_name = strrep(cti_file_name, 'cti', 'hed');
	end
    end
end

% Make sure HED file exists
if(~exist('hed_file_name'))
    error(['HED file name undefined']);
elseif(~exist(hed_file_name, 'file'))
    error(['HED file name invalid: ' hed_file_name]);
end

% Read header file
hed = read_hed(hed_file_name);

dims = [hed.dimx, hed.dimy, hed.dimz]';

% Open CTI file
[fid,message] = fopen(cti_file_name,'r');
if(fid == -1)
    error(['Error opening CTI file: ' fname '.CTI', ...
           '  System message: ' message]);
end
    
% Read CTI file
cti = zeros(dims(1),dims(2),dims(3));
for ii =1:dims(3)
    tmp2 = fread(fid,[dims(1) dims(2)],'short');      % CTI data
    cti(:,:,ii) = tmp2;
end

% Close CTI file
status = fclose(fid);

% use trimcti.m if needed for CT data

% only save the core of the dose-cube for photon pencil beam dose distribution
%Dosdata = tmp(112:143,112:143,1:end);

return
