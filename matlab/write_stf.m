function write_stf(outFile, stf)
%WRITE_STF Write a .stf file (beam steering file)
%
%   WRITE_STF(file_name, stf) Writes the beam steering file based on
%   information in the stf structure.  stf has the following fields:
%
%      stf.machineName          Machine name (string)
%      stf.gantryAngle          Gantry angle in degrees
%      stf.tableAngle           Table angle in degrees
%      stf.collimatorAngle      Collimator angle in degrees
%      stf.spot_dx              Bixel width (cm)
%      stf.spot_dy              Bixel height (cm)
%      stf.energy               Energy for each beamlet (n x 1)
%      stf.spotX                Bixel x position in cm (nx1)
%      stf.spotY                Bixel y position in cm (nx1)
%
%   Throws an error if the file writing fails
%
%   See also read_stf.

% Here's the stf file format:
% 
% The stf file is a human readible file that gives beam steering information
% about each beamlet within a beam.  It starts with a header that gives the
% machine name, gantry angle, table angle, collimator angle (in degrees) and
% bixel size (in mm).  This is followed by a table header and a table of values.
%
% The table columns are:
% DB            beamlet number
% Energy        beamlet energy
% ???           ???
% ???           ???
% ???           ???
% ray-x y       Position of beamlet relative to beam isocenter (in mm)
%
% Until we have better information about what all the columns mean, default
% values have been used for some values.

% Open file for writing (unix style carriage returns)
[outfid, message] = fopen(outFile, 'w');
if ~isempty(message)
        error(['Error writing stf file: ' message]);
end


% Print header information
fprintf(outfid,'%s\n','# Header information');
fprintf(outfid,'%s %s\n','# Machinename:', stf.machineName);
fprintf(outfid,'%s %8.6f\n','# Gantry-Angle:', stf.gantryAngle);
fprintf(outfid,'%s %8.6f\n','# Table-Angle:', stf.tableAngle);
fprintf(outfid,'%s %8.6f\n','# Colli-Angle:', stf.collimatorAngle);
fprintf(outfid,'%s %8.6f %s %8.6f','# Delta X:', stf.spot_dx * 10,...
                                  'Delta Y:', stf.spot_dy * 10);

% Print information about each beamlet
for count = 1:numel(stf.energy)
        fprintf(outfid,'\n%d %8.6f %8.6f %s %8.6f %8.6f %8.6f', ...
                count-1, stf.energy(count), 0., 'R', 0.,...
                stf.spotX(count) * 10, stf.spotY(count) * 10);
end    

% Close file
status = fclose(outfid);
if status ~= 0
        error(['Error writing stf file']);
end
