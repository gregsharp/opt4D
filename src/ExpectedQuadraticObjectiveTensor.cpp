/**
 * @file ExpectedQuadraticObjectiveTensor.cpp
 * ExpectedQuadraticObjectiveTensor class header
 *
 */

#include <vector>
#include <string>
#include <iostream>
#include <fstream>

#include "DoseVector.hpp"
#include "BixelVector.hpp"
#include "DoseInfluenceMatrix.hpp"
#include "ExpectedQuadraticObjectiveTensor.hpp"



/**
 * constructor 
 *
 */
ExpectedQuadraticObjectiveTensor::ExpectedQuadraticObjectiveTensor(
        Voi *the_voi,
        unsigned int objNo,
        DoseInfluenceMatrix &Dij,
        DoseDeliveryModel &Uncertainty,
        float desired_dose,
        float weight,
        string qij_file_root,
        bool read_qjk)
:Objective(objNo, weight),
    the_voi_(the_voi),
    the_DoseInfluenceMatrix_(&Dij),
    the_UncertaintyModel_(&Uncertainty),
    QuadraticTerms_(),
    LinearTerms_(),
    desired_dose_(desired_dose)
{
    cout << "Constructing ExpectedQuadraticObjectiveTensor for Voi " << the_voi_->get_name() << endl;

    // make sure quadratic objective is supported
    if(!the_UncertaintyModel_->supports_calculate_quadratic_objective_tensor()) {
	cout << the_UncertaintyModel_->get_uncertainty_model() 
	     << " does not support the quadratic objective" << endl;
	throw(std::runtime_error("Cannot use ExpectedQuadraticObjectiveTensor"));
    }

    objNo_ = objNo;

    // set weight
    weight_ = weight;

    // allocate memory for the vectors
    LinearTerms_.reserve(the_DoseInfluenceMatrix_->get_nBixels());

    for (unsigned int i=0; i<the_DoseInfluenceMatrix_->get_nBixels(); i++)
    {
	LinearTerms_.push_back(0);
	QuadraticTerms_.push_back( vector<double>(the_DoseInfluenceMatrix_->get_nBixels(),0) );
    }

    if (read_qjk) {
	// read previously calculated beamlet-pair contributions to objective
	read_contributions(qij_file_root);
    }
    else {
	// calculate beamlet-pair contributions to objective
	calculate_contributions();
	write_contributions(qij_file_root);
    }

    is_initialized_ = true;    
}

// destructor
ExpectedQuadraticObjectiveTensor::~ExpectedQuadraticObjectiveTensor()
{
}

/**
 * Calculate the tensor of size nBixels times nBixels to store contribution of a bixel pair to the quadratic objective value of a structure. This involves an integration over the voxels of the structure and the instances
 * 
 */
void ExpectedQuadraticObjectiveTensor::calculate_contributions()
{
    unsigned int voxelNo, iBixelj, iBixelk;

    //temporary storage of bixel dose contributions
    vector<float> Temp_DijRow(the_DoseInfluenceMatrix_->get_nBixels(),0);
    vector<float> Temp_LinearTerms(the_DoseInfluenceMatrix_->get_nBixels(),0);
   
    cout << "calculate beamlet-pair contributions to objective for Voi ";
    cout << the_voi_->get_name();
    cout << ", number of voxels: ";
    cout << the_voi_->get_nVoxels() << endl;

    // loop over the voxels of the Voi
    for(size_t iVoxel=0; iVoxel<the_voi_->get_nVoxels(); iVoxel++)
    {
	// get voxel index
	voxelNo = the_voi_->get_voxel(iVoxel);
	
	cout << "working on voxel " << iVoxel << " of " << the_voi_->get_nVoxels() << endl;

	// call dose delivery model to calculate the contribution of this voxel
	the_UncertaintyModel_->calculate_bixel_pair_contributions_to_quadratic_objective(
	    voxelNo, QuadraticTerms_, LinearTerms_,
	    Temp_DijRow, Temp_LinearTerms);

    } 

    // copy elements to the other triangle of the symmetric matrix
    for (iBixelj=0; iBixelj<the_DoseInfluenceMatrix_->get_nBixels(); iBixelj++)
    {
	// calculate quadratic terms
	for (iBixelk=iBixelj+1; iBixelk<the_DoseInfluenceMatrix_->get_nBixels(); iBixelk++)
	{
	    QuadraticTerms_[iBixelj][iBixelk] = QuadraticTerms_[iBixelk][iBixelj];
	}
    }
}    



/**
 * calculate gradient and objective
 *
 * @param Dij isn't actually used in this function
 * @param use_voxel_sampling isn't actually used in this function
 * @param estimated_ssvo isn't actually used in this function
 */
double ExpectedQuadraticObjectiveTensor::calculate_objective_and_gradient(
    const BixelVector & beam_weights,
    DoseInfluenceMatrix & Dij,
    BixelVectorDirection & gradient,
        float gradient_multiplier,
        bool use_voxel_sampling,
        double &estimated_ssvo)
{
    estimated_ssvo = 0;

  //  cout << "calculate_objective_and_gradient in ExpectedQuadraticObjectiveTensor called" << endl;

    // Exit if there are no voxels
    if(the_voi_->get_nVoxels() == 0) {
        return(0);
    }


  // check if passed Dij and included Dij are the same
  if ( !(&Dij == the_DoseInfluenceMatrix_) )
  {
      cout << "error in ExpectedQuadraticObjectiveTensor::calculate_objective_and_gradient" << endl;
      cout << "passed and included Dij are apparently not the same" << endl;
      exit(-1);
  }

  assert(is_initialized_);

  double objective = 0;

  assert(beam_weights.get_nBixels() == gradient.get_nBixels());
 
  for (unsigned int iBixelj=0; iBixelj<Dij.get_nBixels(); iBixelj++)
  {
      // gradient

      // quadratic terms  
      for (unsigned int iBixelk=0; iBixelk<Dij.get_nBixels(); iBixelk++)
      {
	  if ( beam_weights[iBixelk] > 0 )
	  {
	      //gradient
	      gradient[iBixelj] += gradient_multiplier * 2 * (weight_ / the_voi_->get_nVoxels()) * beam_weights[iBixelk] * get_QuadraticTerm(iBixelj,iBixelk);
	  }
      }

      // add linear term contribution
      gradient[iBixelj] += gradient_multiplier * - 2 * (weight_ / the_voi_->get_nVoxels()) * desired_dose_ * get_LinearTerm(iBixelj);

      // objective

      if ( beam_weights[iBixelj] > 0 )
      {
	  // quadratic terms  
	  for (unsigned int iBixelk=0; iBixelk<Dij.get_nBixels(); iBixelk++)
	  {
	      if ( beam_weights[iBixelk] > 0 )
	      {
		  //objective
		  objective += beam_weights[iBixelj] * beam_weights[iBixelk] * get_QuadraticTerm(iBixelj,iBixelk);
	      }
	  }
	  
	  // add linear term contribution
	  objective += - 2*desired_dose_ * beam_weights[iBixelj] * get_LinearTerm(iBixelj);
      }

  }

  // add constant term
  objective += desired_dose_ * desired_dose_ * the_voi_->get_nVoxels();

  return weight_ * objective / the_voi_->get_nVoxels();

}


/**
 * calculate objective 
 *
 * @param Dij isn't actually used in this function
 * @param use_voxel_sampling isn't actually used in this function
 * @param estimated_ssvo isn't actually used in this function
 */
double ExpectedQuadraticObjectiveTensor::calculate_objective(
        const BixelVector & beam_weights,
        DoseInfluenceMatrix & Dij,
        bool use_voxel_sampling,
        double &estimated_ssvo)
{
    estimated_ssvo = 0;

    // check if passed Dij and included Dij are the same
    if ( !(&Dij == the_DoseInfluenceMatrix_) )
    {
	cout << "error in ExpectedQuadraticObjectiveTensor::calculate_objective_and_gradient" << endl;
	cout << "passed and included Dij are apparently not the same" << endl;
	exit(-1);
    }

    assert(is_initialized_);
    
    double objective = 0;
    
    // bixel loop
    for (unsigned int iBixelj=0; iBixelj<Dij.get_nBixels(); iBixelj++)
    {
	// quadratic terms  
	for (unsigned int iBixelk=0; iBixelk<Dij.get_nBixels(); iBixelk++)
	{
	    //objective
	    objective += beam_weights[iBixelj] * beam_weights[iBixelk] * get_QuadraticTerm(iBixelj,iBixelk);
	}
	
	// add linear term contribution
	objective += 2*desired_dose_ * beam_weights[iBixelj] * get_LinearTerm(iBixelj);
    }
    
    // add constant term
    objective += desired_dose_ * desired_dose_ * the_voi_->get_nVoxels();
    
    if(the_voi_->get_nVoxels() > 0) {
	return weight_ * objective / the_voi_->get_nVoxels();
    }
    else {
	return(0);
    }
    
}

void ExpectedQuadraticObjectiveTensor::read_contributions(string qjk_file_root)
{
  string qjk_file_name;
  std::ifstream qjk_file;
  unsigned int temp_int;
  float temp_float;

  // check for correct size of variables
  if (   (sizeof(float) != 4)
      || (sizeof(int)   != 4)
      || (sizeof(unsigned int)   != 4)
      || (sizeof(short) != 2) ) {
    cout << "Can not read data: wrong byte size";
    exit (-1);
  }

  // Derive file name
  qjk_file_name = qjk_file_root + "_" + the_voi_->get_name() + ".qjk";

  // open file
  qjk_file.open(qjk_file_name.c_str(), std::ios::binary);
  std::cout << "Reading Qjk file: " << qjk_file_name << endl;

  // check if the file exists
  if ( !qjk_file.is_open() ) {
      std::cerr << "Can't open file: " << qjk_file_name << std::endl;
      throw("Unable to open Qjk file");
  }

  // read the header
  qjk_file.read((char*) &temp_int, sizeof(unsigned int));
  if(temp_int!=the_DoseInfluenceMatrix_->get_nBixels()) {
      throw("Wrong number of bixels in Qjk file");
  }

  // bixel loop
  for (unsigned int iBixelj=0; iBixelj<the_DoseInfluenceMatrix_->get_nBixels(); iBixelj++)
  {
      // bixel number
      qjk_file.read((char*) &temp_int, sizeof(unsigned int));
      if(temp_int!=iBixelj) {
	  std::cerr << "Error reading file: " << qjk_file_name << std::endl;
	  throw("Wrong bixel index");
      }
 
      // linear terms
      qjk_file.read((char*) &temp_float, sizeof(float));
      LinearTerms_[iBixelj] += temp_float;
 
      // quadratic terms  
      for (unsigned int iBixelk=0; iBixelk<the_DoseInfluenceMatrix_->get_nBixels(); iBixelk++)
      {
	  qjk_file.read((char*) &temp_float, sizeof(float));
	  QuadraticTerms_[iBixelj][iBixelk] += temp_float;
      }
  }

  qjk_file.close();
  qjk_file.clear();
  
  std::cout << "Done" << endl;

}

void ExpectedQuadraticObjectiveTensor::write_contributions(string qjk_file_root)
{

  unsigned int temp_int;
  float temp_float;
  string qjk_file_name;
  FILE *fOutput;

  // check for correct size of variables
  if (   (sizeof(float) != 4)
      || (sizeof(int)   != 4)
      || (sizeof(unsigned int)   != 4)
      || (sizeof(short) != 2) ) {
    cout << "Can not write data: wrong byte size";
    exit (-1);
  }

  // Derive file name
  qjk_file_name = qjk_file_root + "_" + the_voi_->get_name() + ".qjk";

  fOutput = fopen(qjk_file_name.c_str(),"wb");
  if (fOutput == NULL) {
      cout << "Can't open file: " << qjk_file_name << endl;
      exit(-1);
  }

  cout << "Saving Qjk data to file: " << qjk_file_name << endl;

  // write the header
  temp_int = the_DoseInfluenceMatrix_->get_nBixels();
  fwrite(&temp_int,sizeof(int),1,fOutput);

  // bixel loop
  for (unsigned int iBixelj=0; iBixelj<the_DoseInfluenceMatrix_->get_nBixels(); iBixelj++)
  {
      // bixel number
      temp_int = iBixelj;
      fwrite(&temp_int,sizeof(int),1,fOutput);

      // linear terms
      temp_float = get_LinearTerm(iBixelj);
      fwrite(&temp_float,sizeof(float),1,fOutput);

      // quadratic terms  
      for (unsigned int iBixelk=0; iBixelk<the_DoseInfluenceMatrix_->get_nBixels(); iBixelk++)
      {
	  temp_float = get_QuadraticTerm(iBixelj,iBixelk);
	  fwrite(&temp_float,sizeof(float),1,fOutput);
//	  fprintf(fOutput,"%f ",temp_float);
      }
//    fprintf(fOutput,"\n");
  }

  // close file
  fclose(fOutput);

  cout << "Done" << endl;
}

/**
 * calculate objective based on dose vector. Does not take expectation
 */
double ExpectedQuadraticObjectiveTensor::calculate_dose_objective(
    const DoseVector & the_dose)
{
    double objective = 0;
    size_t nVoxels = the_voi_->get_nVoxels();
    double adjusted_weight = weight_ / nVoxels;

    // loop over all voxels in this VOI
    for(unsigned int iVoxel=0; iVoxel<nVoxels; iVoxel++) {
	
	unsigned int voxelNo = the_voi_->get_voxel(iVoxel);

	objective += adjusted_weight * (the_dose.get_voxel_dose(voxelNo)-desired_dose_)
	    * (the_dose.get_voxel_dose(voxelNo)-desired_dose_);
    }

    return objective;
}


/**
 * function is not implemented
 *
 */
double ExpectedQuadraticObjectiveTensor::calculate_objective_and_gradient_and_Hv(
    const BixelVector & beam_weights,
    DoseInfluenceMatrix & Dij,
    BixelVectorDirection & gradient,
    float gradient_multiplier,
    const vector<float> & v,
    vector<float> & Hv,
        bool use_voxel_sampling,
        double &estimated_ssvo)
{
    cout << "called function which is not applicable for ExpectedQuadraticObjectiveTensor" << endl;
    exit(-1);
} 	    

/**
 * Prints a description of the objective on the given stream
 *
 * @param o The output stream to write to
 */
void ExpectedQuadraticObjectiveTensor::printOn(std::ostream& o) const
{
  o << "OBJ " << get_objNo() << ": "; 
  o << "VOI " << the_voi_->get_voiNo() << "(" << the_voi_->get_name() << ")";
  o << "\tprescribed dose = ";
  o << desired_dose_;
  o << "\tweight " << weight_;
  o << "\t(exp. quad. obj.)";
}


