/**
 * @file: VoxelDoseConstraint.cpp
 * VoxelDoseConstraint Class implementation.
 *
 */

#include "VoxelDoseConstraint.hpp"
#include "DoseParser.hpp"


/**
 * Constructor
 */
UniformDoseConstraint::UniformDoseConstraint(Voi*  the_voi,
											 unsigned int consNo,
											 bool is_max_constraint,
											 bool is_min_constraint,
											 float max_dose,
											 float min_dose,
											 float initial_penalty)
  : VoxelDoseConstraint(the_voi,consNo,is_max_constraint,is_min_constraint,initial_penalty),
	uniform_max_dose_(max_dose),
	uniform_min_dose_(min_dose)
{
}


/**
 * Destructor: default
 */
UniformDoseConstraint::~UniformDoseConstraint()
{
}

/**
 * Prints a description of the constraint on the given stream
 *
 * @param o The output stream to write to
 */
void UniformDoseConstraint::printOn(std::ostream& o) const
{
  if(is_max_constraint_) {
    o << "CONS " << consNo_ << ": "; 
    o << "VOI " << the_voi_->get_voiNo() << "(" << the_voi_->get_name() << ")";
    o << "\tmax dose < " << uniform_max_dose_ << endl;
  }
  if(is_min_constraint_ && uniform_min_dose_ > 0) {
    o << "CONS " << consNo_ << ": "; 
    o << "VOI " << the_voi_->get_voiNo() << "(" << the_voi_->get_name() << ")";
    o << "\tmin dose > " << uniform_min_dose_ << endl;
  }
}


/**
 * set upper dose limit
 */
void UniformDoseConstraint::set_upper_bound()
{
  for(unsigned int i=0; i<get_nVoxels(); i++)
	{
	  max_dose_[i] = uniform_max_dose_;
	}
}

/**
 * set lower dose limit
 */
void UniformDoseConstraint::set_lower_bound()
{
  for(unsigned int i=0; i<get_nVoxels(); i++)
	{
	  min_dose_[i] = uniform_min_dose_;
	}
}


/**
 * Constructor
 */
VaryingDoseConstraint::VaryingDoseConstraint(Voi*  the_voi,
											 unsigned int consNo,
											 bool is_max_constraint,
											 bool is_min_constraint,
											 unsigned int nTotalVoxels,
											 string max_dose_file_name,
											 string min_dose_file_name,
											 float initial_penalty)
  : VoxelDoseConstraint(the_voi,consNo,is_max_constraint,is_min_constraint,initial_penalty),
	max_dose_file_name_(max_dose_file_name),
	min_dose_file_name_(min_dose_file_name)
{
  // read dose from file
  if(is_max_constraint) {
	DoseParser max_dose_parser(max_dose_file_name,nTotalVoxels);
	varying_max_dose_ = max_dose_parser.get_dose();
  }

  if(is_min_constraint) {
	DoseParser min_dose_parser(min_dose_file_name,nTotalVoxels);
	varying_min_dose_ = min_dose_parser.get_dose();
  }
}


/**
 * Destructor: default
 */
VaryingDoseConstraint::~VaryingDoseConstraint()
{
}

/**
 * Prints a description of the constraint on the given stream
 *
 * @param o The output stream to write to
 */
void VaryingDoseConstraint::printOn(std::ostream& o) const
{
  if(is_max_constraint_) {
    o << "CONS " << consNo_ << ": "; 
    o << "VOI " << the_voi_->get_voiNo() << "(" << the_voi_->get_name() << ")";
    o << "\tmaximum voxel dose from file: " << max_dose_file_name_ << endl;
  }
  if(is_min_constraint_) {
    o << "CONS " << consNo_ << ": "; 
    o << "VOI " << the_voi_->get_voiNo() << "(" << the_voi_->get_name() << ")";
    o << "\tminimum voxel dose from file: " << min_dose_file_name_ << endl;
  }
}

/**
 * set upper dose limit
 */
void VaryingDoseConstraint::set_upper_bound()
{
  for(unsigned int i=0; i<get_nVoxels(); i++)
	{
	  max_dose_[i] = varying_max_dose_.get_voxel_dose(the_voi_->get_voxel(i));
	}
}

/**
 * set lower dose limit
 */
void VaryingDoseConstraint::set_lower_bound()
{
  for(unsigned int i=0; i<get_nVoxels(); i++)
	{
	  min_dose_[i] = varying_min_dose_.get_voxel_dose(the_voi_->get_voxel(i));
	}
}




/**
 * Constructor: Initialize variables.  
 */
VoxelDoseConstraint::VoxelDoseConstraint(Voi*  the_voi,
										 unsigned int consNo,
										 bool is_max_constraint,
										 bool is_min_constraint,
										 float initial_penalty)
: Constraint(consNo),
  dose_contribution_norm_(),
  the_voi_(the_voi),
  is_max_constraint_(is_max_constraint),
  is_min_constraint_(is_min_constraint),
  max_dose_(get_nVoxels(),100),
  min_dose_(get_nVoxels(),0),
  penalty_(),
  initial_penalty_(initial_penalty/(double)get_nVoxels()),
  lagrange_(),
  in_active_set_(get_nVoxels(),true)
{
  dose_contribution_norm_ = vector<float>(get_nVoxels(),0);
  lagrange_ = vector<float>(get_nVoxels(),0);
  penalty_ = vector<float>(get_nVoxels(),initial_penalty/(double)get_nVoxels());
  reset_active_set();
}

/**
 * Destructor: default.  Not responsible for deleting the Voi
 */
VoxelDoseConstraint::~VoxelDoseConstraint()
{
}


/**
 * Initialize the objective: calculate norm of dose contribution vectors
 */
void VoxelDoseConstraint::initialize(DoseInfluenceMatrix& Dij)
{
  for(unsigned int iVoxel=0; iVoxel<get_nVoxels(); iVoxel++)
    {
      unsigned int voxelNo = the_voi_->get_voxel(iVoxel);
      for(DoseInfluenceMatrix::iterator iter = Dij.begin_voxel(voxelNo);
	                                       iter.get_voxelNo() == voxelNo; iter++) {
	dose_contribution_norm_[iVoxel] += (iter.get_influence()) * (iter.get_influence()); 
      }
    }

  // set maximum and/or minimum dose
  if(is_max_constraint_) {
	set_upper_bound();
  }
  if(is_min_constraint_) {
	set_lower_bound();
  }

  is_initialized_ = true;
  verbose_=false;
}





/**
 * get lagrange multipliers as pairs <voxelNo,value>
 */
vector<pair<unsigned int,float> > VoxelDoseConstraint::get_lagrange_multipliers()
{
  cout << "get lagrange multipliers for constraint " << get_consNo() << endl;

  vector<pair<unsigned int,float> > data;

  for(unsigned int i=0; i<lagrange_.size(); i++) {
    unsigned int voxelNo = the_voi_->get_voxel(i);
    data.push_back(pair<unsigned int,float>(voxelNo,lagrange_[i]));
  }
  return data;
}



/**
 * update the lagrange multipliers
 */
void VoxelDoseConstraint::update_lagrange_multipliers(const BixelVector & beam_weights,
						 DoseInfluenceMatrix & Dij)
{
  if(verbose_) {
    cout << "updating lagrange multipliers for constraint " << get_consNo() << endl;
  }  

  for(unsigned int iVoxel=0; iVoxel<get_nVoxels(); iVoxel++)
    {
      unsigned int voxelNo = the_voi_->get_voxel(iVoxel);
      float dose_buf = Dij.calculate_voxel_dose(voxelNo, beam_weights);
      float lag_buf = lagrange_[iVoxel];
      float penalty = penalty_[iVoxel];

      // if constraint satisfied
      float temp_lagrange = 0;

      // upper bound active
      if(is_max_constraint_) {
		double cons = dose_buf-max_dose_[iVoxel];
		if(-1*lag_buf/penalty < cons) {
		  temp_lagrange = lag_buf + penalty*cons;
		}
      }

      // lower bound active
      if(is_min_constraint_) {
		double cons = dose_buf-min_dose_[iVoxel];
		if(-1*lag_buf/penalty > cons) {
		  temp_lagrange = lag_buf + penalty*cons;
		}
      }

      lagrange_[iVoxel] = temp_lagrange;
    }
}


/**
 * update the penalty factor
 */
void VoxelDoseConstraint::update_penalty(const BixelVector & beam_weights,
									DoseInfluenceMatrix & Dij, 
									float tol, float multiplier)
{
    size_t nVoxels = the_voi_->get_nVoxels();
    for(size_t iVoxel=0; iVoxel<nVoxels; iVoxel++) {
	  
	  // Find the voxelNo of the voxel we are looking at
	  unsigned int voxelNo = the_voi_->get_voxel(iVoxel);
	  
	  // Calculate dose to voxel
	  float dose_buf = Dij.calculate_voxel_dose(voxelNo, beam_weights);

	  // upper bound active
	  if(is_max_constraint_) {
		double cons = dose_buf-max_dose_[iVoxel];
		if(cons > tol*max_dose_[iVoxel]) { // constraint violated by more than the tolerance
		  penalty_[iVoxel] *= multiplier;
		}
	  }
	  
	  // lower bound active
	  if(is_min_constraint_) {
		double cons = dose_buf-min_dose_[iVoxel];
		if(cons < -1*tol*min_dose_[iVoxel]) {
		  penalty_[iVoxel] *= multiplier;
		}
	  }
	}
}

/**
 * Calculate contribution of constraint to augmented Lagrangian
 */
double VoxelDoseConstraint::calculate_aug_lagrangian(
        const BixelVector & beam_weights,
        DoseInfluenceMatrix & Dij,
		double & constraint,
		double & merit)
{
  if(verbose_) {
    cout << "calc augmented lagragian for constraint " << get_consNo() << endl;
  }

    double objective = 0;
	constraint = 0;
	merit = 0;
    double voxel_lagrange = 0;
    double voxel_cons = 0;
	
    // loop over all voxels in this VOI and look up dose deviation
    size_t nVoxels = the_voi_->get_nVoxels();
    for(size_t iVoxel=0; iVoxel<nVoxels; iVoxel++) {
	  
	  // Find the voxelNo of the voxel we are looking at
	  unsigned int voxelNo = the_voi_->get_voxel(iVoxel);
	  
	  // Calculate dose to voxel
	  float dose_buf = Dij.calculate_voxel_dose(voxelNo, beam_weights);
	  float lag_buf = lagrange_[iVoxel];
	  float penalty = penalty_[iVoxel];

	  // unconstrained minimum of penalty function
	  voxel_lagrange = -0.5*lag_buf*lag_buf/penalty;
	  voxel_cons=0;	  

	  // upper bound active
	  if(is_max_constraint_) {
		double cons = dose_buf-max_dose_[iVoxel];
		if(-1*lag_buf/penalty < cons) {
		  voxel_lagrange = lag_buf*cons + 0.5*penalty*cons*cons;
		}
		if(cons > 0) {
		  voxel_cons = cons;
		}
	  }
	  
	  // lower bound active
	  if(is_min_constraint_) {
		double cons = dose_buf-min_dose_[iVoxel];
		if(-1*lag_buf/penalty > cons) {
		  voxel_lagrange = lag_buf*cons + 0.5*penalty*cons*cons;
		}
		if(cons < 0) {
		  voxel_cons = -1*cons;
		}
	  }
	  
	  // lagrange function
	  objective += voxel_lagrange;

	  // merit function returns the quadratic penalty term with the initial penalty factor
	  merit += voxel_cons*voxel_cons;

	  // returns the maximum constraint violation for any voxel in the VOI
	  if(voxel_cons > constraint) {
		constraint = voxel_cons;
	  }
    }
	
	merit *= 0.5*initial_penalty_;
    return objective; 
}



/**
 * Calculate contribution of constraint to augmented Lagrangian and its gradient
 */
double VoxelDoseConstraint::calculate_aug_lagrangian_and_gradient(
        const BixelVector & beam_weights,
        DoseInfluenceMatrix & Dij,
        BixelVectorDirection & gradient,
        float gradient_multiplier)
{
  if(verbose_) {
    cout << "calc gradient of augmented lagragian for constraint " << get_consNo() << endl;
  }

    assert(beam_weights.get_nBixels() == gradient.get_nBixels());

    double objective = 0;
    double voxel_lagrange = 0;

    // loop over all voxels in this VOI and look up dose deviation
    size_t nVoxels = the_voi_->get_nVoxels();
    for(size_t iVoxel=0; iVoxel<nVoxels; iVoxel++) {

        // Find the voxelNo of the voxel we are looking at
        unsigned int voxelNo = the_voi_->get_voxel(iVoxel);

        // Calculate dose to voxel
        float dose_buf = Dij.calculate_voxel_dose(voxelNo, beam_weights);
		float lag_buf = lagrange_[iVoxel];
		float penalty = penalty_[iVoxel];

	// unconstrained minimum of penalty function
	voxel_lagrange = -0.5*lag_buf*lag_buf/penalty;

	// upper bound active
	if(is_max_constraint_) {
	  double cons = dose_buf-max_dose_[iVoxel];
	  if(-1*lag_buf/penalty < cons) {
	    voxel_lagrange = lag_buf*cons + 0.5*penalty*cons*cons;

	    double factor = lag_buf + penalty * cons;
	    Dij.calculate_voxel_dose_gradient(voxelNo,
					      gradient,
					      factor * gradient_multiplier);
	  }
	}

	// lower bound active
	if(is_min_constraint_) {
	  double cons = dose_buf-min_dose_[iVoxel];
	  if(-1*lag_buf/penalty > cons) {
	    voxel_lagrange = lag_buf*cons + 0.5*penalty*cons*cons;

	    double factor = lag_buf + penalty * cons;
	    Dij.calculate_voxel_dose_gradient(voxelNo,
					      gradient,
					      factor * gradient_multiplier);
	  }
	}

	objective += voxel_lagrange;
    }
    return objective;
}

        

/**
 * sequentially project onto voxel constraints
 */
unsigned int VoxelDoseConstraint::project_onto(BixelVector & beam_weights,
					  DoseInfluenceMatrix & Dij)
{
  unsigned int nProjections = 0;

  // cout << "projecting onto constraint " << get_consNo() << ": ";

  for(unsigned int iVoxel=0; iVoxel<get_nVoxels(); iVoxel++)
    {
      if(is_active(iVoxel)) 
	{
	  // Find the voxelNo of the voxel we are looking at
	  unsigned int voxelNo = the_voi_->get_voxel(iVoxel);
	  
	  // Calculate dose to voxel
	  float dose = Dij.calculate_voxel_dose(voxelNo, beam_weights);
	  float max_dose = max_dose_[iVoxel];
	  float min_dose = min_dose_[iVoxel];

	  // need to distinguish four cases
	  float factor;
	  bool project = false;
	  if(dose>max_dose){
	    project = true;
	    if(dose-max_dose<0.5*(max_dose-min_dose)) {
	      factor = -2*(dose-max_dose); 
	    }
	    else {
	      factor = -1*(dose-max_dose) - 0.5*(max_dose-min_dose); 
	    }
	  } 
	  if (dose<min_dose){
	    project = true;
	    if(min_dose-dose<0.5*(max_dose-min_dose)) {
	      factor = 2*(min_dose-dose);
	    }
	    else {
	      factor = (min_dose-dose) + 0.5*(max_dose-min_dose);
	    }
	  }

	  if(project) {
	    // divide by the norm of the dij column
	    factor = factor / dose_contribution_norm_[iVoxel];

	    // do the actual weight change
	    for(DoseInfluenceMatrix::iterator iter = Dij.begin_voxel(voxelNo);
		                                     iter.get_voxelNo() == voxelNo; iter++) {
	      beam_weights[iter.get_bixelNo()] += factor * iter.get_influence();
	    }
	    // increment projection counter
	    nProjections++;
	  }
	  else {
	    // constraint satisfied, no projection needed
	    // remove voxel from active set
	    in_active_set_[iVoxel] = false;
	  }
	}
    }
  //  cout << nProjections << " projections" << endl;
  return nProjections;
}

/**
 * returns total number of constraints for commercial solver interface
 */
unsigned int VoxelDoseConstraint::get_nGeneric_constraints() const
{
    return(get_nVoxels());
}

/**
 * prepare constraints for commercial solver interface
 */
void VoxelDoseConstraint::add_to_optimization_data_set(GenericOptimizationData & data, DoseInfluenceMatrix & Dij) const
{
  cout << "Creating generic constraint set for CONS " << get_consNo() << endl;

  for (unsigned int iVoxel=0; iVoxel<get_nVoxels(); iVoxel++) {

    // Find the voxelNo of the voxel we are looking at
    unsigned int voxelNo = the_voi_->get_voxel(iVoxel);
 
    // get number of nonzero dij elements
    unsigned int nEntries = Dij.get_nEntries_per_voxel(voxelNo);

    // add to number of nonzero coefficients
    data.nNonZeros_ += nEntries;

    // initialize next constraint
    data.constraints_[data.nConstraints_until_now_].initialize(nEntries);

    // the coefficients are just the Dij elements
    unsigned int iEntry = 0;
    for(DoseInfluenceMatrix::iterator iter = Dij.begin_voxel(voxelNo);
	iter.get_voxelNo() == voxelNo; iter++) {
	  data.constraints_[data.nConstraints_until_now_].index_[iEntry] = iter.get_bixelNo();
	  data.constraints_[data.nConstraints_until_now_].value_[iEntry] = iter.get_influence();
	  iEntry++;
    }

    // bounds
    if(is_max_constraint_) {
      data.constraints_[data.nConstraints_until_now_].upper_bound_ = max_dose_[iVoxel];
    }
    else {
      data.constraints_[data.nConstraints_until_now_].upper_bound_ = OPT4D_INFINITY;
    }

    if(is_min_constraint_) {
      data.constraints_[data.nConstraints_until_now_].lower_bound_ = min_dose_[iVoxel];
    }
    else {
      data.constraints_[data.nConstraints_until_now_].lower_bound_ = -OPT4D_INFINITY;
    }

    // constraint type
    if(is_max_constraint_ && is_min_constraint_) {
	  if(abs(max_dose_[iVoxel]-min_dose_[iVoxel]) < OPT4D_EPSILON) {
		data.constraints_[data.nConstraints_until_now_].type_ = EQUALITY;
	  }
	  else {
		data.constraints_[data.nConstraints_until_now_].type_ = TWOSIDED;
	  }
    }
    else if(is_max_constraint_ && !is_min_constraint_) {
      data.constraints_[data.nConstraints_until_now_].type_ = UPPERBOUND;
    }
    else if(!is_max_constraint_ && is_min_constraint_) {
      data.constraints_[data.nConstraints_until_now_].type_ = LOWERBOUND;
    }
    else {
	  cout << "WARNING: CONS " << get_consNo() << " has neither max nor min dose." << endl; 
	  data.constraints_[data.nConstraints_until_now_].type_ = FREE; 
    }

    // increment constrait counter
    data.nConstraints_until_now_ += 1;

  }
}
  

  




