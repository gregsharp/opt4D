/**
 * @file FastMeanObjective.hpp
 * FastMeanObjective Class header
 *
 * $ Id: $
 */

#ifndef FASTMEANOBJECTIVE_HPP
#define FASTMEANOBJECTIVE_HPP

#include "Objective.hpp"
#include "Voi.hpp"
#include <cmath>

/**
 * Class FastMeanObjective is an objective.
 */
class FastMeanObjective : public Objective
{

    public:

        // Constructor
        FastMeanObjective(
                Voi*  the_voi,
                unsigned int objNo,
                float desired_dose,
                bool  is_max_constraint,
                float weight = 1);

        // Virtual destructor
        virtual ~FastMeanObjective();


        // Initialize with the Dij
        virtual void initialize(DoseInfluenceMatrix& Dij);

        // Objective-specific actions
        void set_desired_dose(float desired_dose);
        float get_desired_dose()const;
        size_t get_voiNo() const;

        //
        // Calculate or estimate the objective
        //
        virtual double calculate_objective(
                const BixelVector & beam_weights,
                DoseInfluenceMatrix & Dij,
                bool use_voxel_sampling,
                double &estimated_ssvo);
        virtual double calculate_objective_and_gradient(
                const BixelVector & beam_weights,
                DoseInfluenceMatrix & Dij,
                BixelVectorDirection & gradient,
                float gradient_multiplier,
                bool use_voxel_sampling,
                double &estimated_ssvo);
        virtual double calculate_objective_and_gradient_and_Hv(
                const BixelVector & beam_weights,
                DoseInfluenceMatrix & Dij,
                BixelVectorDirection & gradient,
                float gradient_multiplier,
                const vector<float> & v,
                vector<float> & Hv,
                bool use_voxel_sampling,
                double &estimated_ssvo);

        // Calculate the objective based on just the dose vector provided
        // Returns zero if the objective does not suppor dose objectives
        virtual double calculate_dose_objective(
                const DoseVector & the_dose
                );


        //
        // incorporating uncertainty
        //

        /// Does the objective support RandomScenario?
        virtual bool supports_RandomScenario() const {return false;};

        /// Find out how many voxels were in objective
        virtual unsigned int get_nVoxels() const;

        /// Prints a description of the objective
        virtual void printOn(std::ostream& o) const;

    protected:

    private:
        // Default constructor not allowed
        FastMeanObjective();

        // Calculate mean dose
        float calculate_mean_dose(const BixelVector & beam_weights) const;

        Voi *the_voi_;

        float desired_dose_;
        bool is_max_constraint_;

        vector<float> mean_dose_contribution_;

};


/*
 * Inline functions
 */

inline
void FastMeanObjective::set_desired_dose(float desired_dose)
{
  desired_dose_ = desired_dose;
}

inline
float FastMeanObjective::get_desired_dose()const
{
  return desired_dose_;
}

inline
size_t FastMeanObjective::get_voiNo() const
{
  return the_voi_->get_voiNo();
}


#endif
