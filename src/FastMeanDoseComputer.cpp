/**
 * @file: FastMeanDoseComputer.cpp
 * FastMeanDoseComputer Class implementation.
 *
 */

#include "FastMeanDoseComputer.hpp"

/**
 * Constructor
 */
FastMeanDoseComputer::FastMeanDoseComputer()
  : mean_dose_contribution_()
  , norm_(0)
  , is_initialized_(false)
{
}

FastMeanDoseComputer::~FastMeanDoseComputer()
{
}

/**
 * Initialize: calculate mean dose contributions of beamlets
 */
void FastMeanDoseComputer::initialize(Voi*  the_voi, 
				      DoseInfluenceMatrix& Dij)
{
  // Initialize the mean_dose_contribution_ vector
  unsigned int nBixels = Dij.get_nBixels();
  mean_dose_contribution_.resize(nBixels);
  fill(mean_dose_contribution_.begin(),mean_dose_contribution_.end(), 0);

  // Get the total dose_contribution from the Dij
  unsigned int nVoxels = the_voi->get_nVoxels();
  for(unsigned int iVoxel=0; iVoxel<nVoxels; iVoxel++) {
    unsigned int voxelNo = the_voi->get_voxel(iVoxel);
    for(DoseInfluenceMatrix::iterator iter = Dij.begin_voxel(voxelNo);
	iter.get_voxelNo() == voxelNo; iter++) {
      mean_dose_contribution_[iter.get_bixelNo()] += iter.get_influence();
    }
  }

  // Scale the dose contributions to the mean
  for(unsigned int iBixel=0; iBixel<nBixels; iBixel++) {
    mean_dose_contribution_[iBixel] /= nVoxels;
  }

  // calculate norm
  norm_ = 0;
  for(unsigned int iBixel=0; iBixel<nBixels; iBixel++) {
    norm_ += mean_dose_contribution_[iBixel]*mean_dose_contribution_[iBixel];
  }

  is_initialized_ = true;
}


/**
 * Calculate the mean dose from a vector of beam weights
 */
float FastMeanDoseComputer::calculate_mean_dose(
        const BixelVector & beam_weights) const
{
  assert(is_initialized_);
  assert(beam_weights.get_nBixels() == mean_dose_contribution_.size());

  unsigned int nBixels = mean_dose_contribution_.size();
  float dose_buf = 0;
  for(unsigned int iBixel = 0; iBixel < nBixels; iBixel++) {
    dose_buf += beam_weights[iBixel] * mean_dose_contribution_[iBixel];
  }
  return dose_buf;
}
