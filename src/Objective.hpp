/**
 * @file Objective.hpp
 * Objective Class header
 */

#ifndef OBJECTIVE_HPP
#define OBJECTIVE_HPP

#include <iostream>

#include "BixelVector.hpp"
#include "DoseVector.hpp"
#include "DoseInfluenceMatrix.hpp"
#include "DoseDeliveryModel.hpp"
#include "GenericConstraint.hpp"

/**
 * Class Objective is a pure virtual class that is inherited to build 
 * objectives.
 */
class Objective
{

    public:

        // Virtual destructor
        virtual ~Objective();

         // Initialize the objective with any information it needs
        virtual void initialize(DoseInfluenceMatrix& Dij) {};

        // Print description of objective
        friend std::ostream& operator<< (
               std::ostream& o, const Objective& objective);

        // Virtual printOn statement
        virtual void printOn(std::ostream& o) const = 0;

        // which optimization method is supported 
        virtual bool supports_gradient() const {return false;}; // gradient can be calculated
        virtual bool supports_meta_objective() const {return false;}; // supports metaobjective and stochastic approach
        virtual bool supports_voxel_sampling() const {return false;}; // voxel sampling is supported
        virtual bool supports_external_solver() const {return false;}; // interface to external solver is implemented
        virtual bool supports_calculation_via_dose_vector() const {return false;}; // objective can be calculated by supplying a dose vector

        //
        // Calculate or estimate the objective
        //
        virtual double calculate_objective(
                const BixelVector & beam_weights,
                DoseInfluenceMatrix & Dij,
                bool use_voxel_sampling,
                double &estimated_ssvo) = 0;

        // Calculate the objective based on just the dose vector provided
        // Returns zero if the objective does not support dose objectives
        virtual double calculate_dose_objective(
                const DoseVector & the_dose) = 0;


        //
        // Calculate gradient
        //
        virtual double calculate_objective_and_gradient(
                const BixelVector & beam_weights,
                DoseInfluenceMatrix & Dij,
                BixelVectorDirection & gradient,
                float gradient_multiplier,
                bool use_voxel_sampling,
                double &estimated_ssvo);

        //
        // Calculate objective, gradient, and hessian times vector
        //
        virtual double calculate_objective_and_gradient_and_Hv(
                const BixelVector & beam_weights,
                DoseInfluenceMatrix & Dij,
                BixelVectorDirection & gradient,
                float gradient_multiplier,
                const vector<float> & v,
                vector<float> & Hv,
                bool use_voxel_sampling,
                double &estimated_ssvo);

        // Calculate the gradient of the objective with respect to the dose vector
        virtual double calculate_dose_objective_and_gradient(
				const DoseVector & the_dose,
				DoseVector & dose_gradient);


        //
        // incorporating uncertainty
        //

        // Calculate the objective for one random scenario
        virtual double calculate_scenario_objective(
                const BixelVector & beam_weights,
                DoseInfluenceMatrix & Dij,
                const DoseDeliveryModel &uncertainty_model,
                const RandomScenario &random_scenario,
                bool use_voxel_sampling,
                double &estimated_ssvo
                );

        // Calculate the objective and gradient for one random scenario
        virtual double calculate_scenario_objective_and_gradient(
                const BixelVector & beam_weights,
                DoseInfluenceMatrix & dij,
                BixelVectorDirection & gradient, 
                float gradient_multiplier,
                const DoseDeliveryModel &uncertainty_model,
                const RandomScenario &random_scenario,
                bool use_voxel_sampling,
                double &estimated_ssvo
                );

        // Calculate the objective and gradient and Hv for one random scenario
        virtual double calculate_scenario_objective_and_gradient_and_Hv(
                const BixelVector & beam_weights,
                DoseInfluenceMatrix & dij,
                BixelVectorDirection & gradient, 
                float gradient_multiplier,
                const vector<float> & v,
                vector<float> & Hv,
                const DoseDeliveryModel &uncertainty_model,
                const RandomScenario &random_scenario,
                bool use_voxel_sampling,
                double &estimated_ssvo
                );

        //
        // external solver interface
        //

        /// returns total number of constraints
        virtual unsigned int get_nGeneric_constraints() const;

        /// returns total number of auxiliary variables
        virtual unsigned int get_nAuxiliary_variables() const;

        /// extract data for commercial solver
        virtual void add_to_optimization_data_set(GenericOptimizationData & data, 
						  DoseInfluenceMatrix & Dij) const;


        //
        // other functions
        //

        // get the number of the objective
        unsigned int get_objNo() const;

        // set weight of the objective
        void set_weight(const float weight);

        /// Find out how many voxels were in objective if appropriate, otherwise 0
        virtual unsigned int get_nVoxels() const;

        /// Find out voxel sampling rate if appropriate, otherwise 1
        virtual float get_sampling_fraction() const;

        /// Set voxel sampling rate if appropriate, otherwise do nothing
        virtual void set_sampling_fraction(float sampling_fraction);

    protected:
        // Constructor for derived classes only
        Objective(unsigned int objNo, float weight);

        float weight_;
        bool is_initialized_;
        unsigned int objNo_;  

    private:
        /// Default constructor not allowed
        Objective();
};

inline
unsigned int Objective::get_objNo() const
{
  return objNo_;
}


inline
void Objective::set_weight(const float weight)
{
  weight_ = weight;
}

inline
std::ostream& operator<< (std::ostream& o, const Objective& objective)
{
  objective.printOn(o);
  return o;
}



#endif
