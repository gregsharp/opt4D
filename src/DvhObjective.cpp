#include "DvhObjective.hpp"
#include "SortableTriple.hpp"



/*
template<class T_v1, class T_v2, class T_v3>
bool compvalue1( const SortableTriple<T_v1,T_v2,T_v3> & lhs, 
				 const SortableTriple<T_v1,T_v2,T_v3> & rhs)
{
  return lhs.value1_ < rhs.value1_;
}
*/

bool compvalue1( const SortableTriple<float,float,unsigned int> & lhs, 
				 const SortableTriple<float,float,unsigned int> & rhs)
{
  return lhs.value1_ < rhs.value1_;
}

bool compvalue2( const SortableTriple<float,float,unsigned int> & lhs, 
				 const SortableTriple<float,float,unsigned int> & rhs)
{
  return lhs.value2_ < rhs.value2_;
}

bool compvalue3( const SortableTriple<float,float,unsigned int> & lhs, 
				 const SortableTriple<float,float,unsigned int> & rhs)
{
  return lhs.value3_ < rhs.value3_;
}

bool compvolume( const DvhObjective::DvhPoint & lhs, 
				 const DvhObjective::DvhPoint & rhs)
{
  return lhs.volume_ < rhs.volume_;
}

bool compdose( const DvhObjective::DvhPoint & lhs, 
			   const DvhObjective::DvhPoint & rhs)
{
  return lhs.dose_ < rhs.dose_;
}



DvhObjective::DvhPoint::DvhPoint(float volume,float dose,float weight)
  : volume_(volume)
  , dose_(dose)
  , weight_(weight)
{
}


DvhObjective::DvhInputParser::DvhInputParser(string input)
  : dvh_points_()
{
    if(!input.empty()) 
	  {
		std::istringstream iss(input);

		float volume,dose,weight;

	    while(!(iss >> volume >> dose >> weight).fail()) 
		  {
			dvh_points_.push_back(DvhObjective::DvhPoint(volume,dose,weight));
		  }
	  }
}



/**
 * Constructor
 */
DvhObjective::DvhObjective(Voi*  the_voi,
						   unsigned int objNo,
						   bool is_max_constraint_,
						   bool is_min_constraint_,
						   vector<DvhPoint> max_dvh_points,
						   vector<DvhPoint> min_dvh_points,
						   float weight,
						   float sampling_fraction,
						   bool sample_with_replacement)
: NonSeparableObjective(the_voi, objNo, weight,sampling_fraction, sample_with_replacement)
, is_max_constraint_(is_max_constraint_)
, is_min_constraint_(is_min_constraint_)
, nMax_dvh_points_(0)
, nMin_dvh_points_(0)
, max_dvh_points_(max_dvh_points)
, min_dvh_points_(min_dvh_points)
{
  // sort dvh points
  std::sort(max_dvh_points_.begin(),max_dvh_points_.end(),compdose);
  std::sort(min_dvh_points_.begin(),min_dvh_points_.end(),compvolume);

  nMax_dvh_points_ = max_dvh_points_.size();
  nMin_dvh_points_ = min_dvh_points_.size();
}

/**
 * Destructor: default.
 */
DvhObjective::~DvhObjective()
{
}

/**
 * Prints a description of the objective on the given stream
 *
 * @param o The output stream to write to
 */
void DvhObjective::printOn(std::ostream& o) const
{
    o << "OBJ " << objNo_ << ": ";
    o << "VOI " << the_voi_->get_voiNo() << "(" << the_voi_->get_name() << ")";
    if(is_max_constraint_) {
	  o << "\tmax Dvh points: " << nMax_dvh_points_ << "\n";
	  for(unsigned int i=0; i<nMax_dvh_points_; i++) {
		o << max_dvh_points_[i] << endl;
	  }
    }
    if(is_min_constraint_) {
	  o << "\tmin Dvh points: " << nMin_dvh_points_ << "\n";
	  for(unsigned int i=0; i<nMin_dvh_points_; i++) {
		o << min_dvh_points_[i] << endl;
	  }
    }
}

/// Write out scenario to stream
std::ostream& operator<< (std::ostream& o, const DvhObjective::DvhPoint& rhs) 
{
  o << "(volume " << rhs.volume_ << ", dose " << rhs.dose_ << ", weight " << rhs.weight_ << ")";
}


/**
 * get voxel dependent dose limits and penalties
 */
void DvhObjective::get_maxdose_and_weight(float rank, float & doselimit, float & weight) const
{
	
	// voxels are ordered with increasing dose (voxel 0 lowest dose)
	// dvh points are ordered with increasing dose (first dvh point has lowest dose value)
	
  unsigned int current = 0;

  while(current < nMax_dvh_points_)
	{
	  if(rank >= (100.0-max_dvh_points_[current].volume_)/100.0)
		{
		  current++;
		}
	  else 
		{
		  break;
		}
	}

  if(current == nMax_dvh_points_) // beyond last DVH point - no overdose penalty
	{
	  doselimit = 0; // value will be irrelevant
	  weight = 0;
	}
  else 
	{
	  doselimit = max_dvh_points_[current].dose_;
	  weight = max_dvh_points_[current].weight_;
	}
}

/**
 * get voxel dependent dose limits and penalties
 */
void DvhObjective::get_mindose_and_weight(float rank, float & doselimit, float & weight) const
{
	
	// voxels are ordered with increasing dose (voxel 0 lowest dose)
	// dvh points are ordered with increasing volume (first dvh point has lowest volume value)
	
  unsigned int current = 0;

  while(current < nMin_dvh_points_)
	{
	  if(rank < (100.0-min_dvh_points_[current].volume_)/100.0)
		{
		  current++;
		}
	  else 
		{
		  break;
		}
	}

  if(current == nMin_dvh_points_) // beyond last DVH point - no overdose penalty
	{
	  doselimit = 0; // value will be irrelevant
	  weight = 0;
	}
  else 
	{
	  doselimit = min_dvh_points_[current].dose_;
	  weight = min_dvh_points_[current].weight_;
	}
}

/*
void DvhObjective::get_maxdose_and_weight(nVoxels,doselimit,weight)
{
	
	// voxels are ordered with increasing dose (voxel 0 lowest dose)
	// dvh points are ordered with increasing dose (first dvh point has dose value)
	
	doselimit.resize(nVoxels,0);
	weight.resize(nVoxels,0);
	
	// start with the dvh point that applies to the largest volume
	current = 0;
	
	// start with the low dose voxels
	for (iVoxel=0; iVoxel<nVoxels; iVoxel++) {
		
		// go to the next dvh point if volume threshold is met
		if (iVoxel >= nVoxels*(100.0-dvh_points[current].volume_)/100.0) {
			if (current == (nMaxDvhPoints_ - 1)) {
				break;
			}
			else {
				current++;
			}
		}
		
		// set dose value and weight
		doselimit[iVoxel] = dvh_points[current].dose_;
		weight[iVoxel] = dvh_points[current].weight_;
				
	}
}


void DvhObjective::get_mindose_and_weight(nVoxels,doselimit,weight)
{
	
	// voxels are ordered with increasing dose (voxel 0 lowest dose)
	// dvh points are ordered with increasing dose (first dvh point has lowest volume value)
	
	doselimit.resize(nVoxels,0);
	weight.resize(nVoxels,0);
	
	// start with the dvh point that applies to the largest volume
	current = 0;
	
	// start with the high dose voxels
	for (iVoxel=nVoxels-1; iVoxel>0; iVoxel=iVoxel-1) {
		
		// go to the next dvh point if volume threshold is met
		if (iVoxel < nVoxels*dvh_points[current].volume_/100.0) {
			if (current == (nMaxDvhPoints_ - 1)) {
				break;
				// we are at the last dvh point
				// skip remailing voxels and leave weight at zero
			}
			else {
				current++;
			}
		}
		
		// set dose value and weight
		doselimit[iVoxel] = dvh_points[current].dose_;
		weight[iVoxel] = dvh_points[current].weight_;
				
	}
}
*/



/**
 * Calculate the objective from some voxels with the given doses.
 *
 * @param dose The vector of voxel doses.
 */
double DvhObjective::calculate_objective(const vector<float> & dose) const
{
    unsigned int nVoxels = dose.size();
    double dVoxels = static_cast<double>(nVoxels);
	
    // Skip if there are no voxels
    if(nVoxels == 0) {
        return 0;
    }
	
	// make a copy of the dose vector which is sortable
	vector<SortableTriple<float,float,unsigned int> > dosevector;
	dosevector.resize(nVoxels);
	     
	for (unsigned int i=0; i<nVoxels; i++) {
		dosevector[i] =  SortableTriple<float,float,unsigned int>(dose[i],0,i);
	}
	
	// sort the dose values
	std::sort(dosevector.begin(),dosevector.end());
	
	float doselimit;
	float penalty;	
	float tmp;

	double objective = 0;
	
	if (is_max_constraint_) {
		
		for (unsigned int i=0; i<nVoxels; i++) {
		    get_maxdose_and_weight(i/dVoxels,doselimit,penalty);
			tmp = dosevector[i].value1_ - doselimit;
			if (tmp>0) {
				objective += penalty*tmp*tmp;
			}
		}
	}
	
	if (is_min_constraint_) {
		
		for (unsigned int i=0; i<nVoxels; i++) {
		    get_mindose_and_weight(i/dVoxels,doselimit,penalty);
			tmp = doselimit - dosevector[i].value1_;
			if (tmp>0) {
				objective += penalty*tmp*tmp;
			}
		}
	}
		
    return weight_*objective/dVoxels;
}

/**
 * Calculate the objective and partial derivative of the voxel contribution to
 * the objective from one voxel with the given dose.
 *
 * @param dose The dose of the voxels in the objective.
 * @param d_obj_by_dose The vector to hold the returned partial derivatives
 */
double DvhObjective::calculate_objective_and_d(const vector<float> & dose,
											   vector<float> & d_obj_by_dose) const
{
	unsigned int nVoxels = dose.size();
	double dVoxels = static_cast<double>(nVoxels);
	
	// Skip if there are no voxels
	if(nVoxels == 0) {
		return 0;
	}
	
	// make a copy of the dose vector which is sortable
	vector<SortableTriple<float,float,unsigned int> > dosevector;
	dosevector.resize(nVoxels);
	
	for (unsigned int i=0; i<nVoxels; i++) {
		dosevector[i] =  SortableTriple<float,float,unsigned int>(dose[i],0,i);
	}
	
	// sort the dose values
	std::sort(dosevector.begin(),dosevector.end());
	
	float doselimit;
	float penalty;	
	float tmp;

	double objective = 0;
	
	if (is_max_constraint_) {
		
		for (unsigned int i=0; i<nVoxels; i++) {
		    get_maxdose_and_weight(i/dVoxels,doselimit,penalty);
			tmp = dosevector[i].value1_ - doselimit;
			if (tmp>0) {
				objective += penalty*tmp*tmp;
				dosevector[i].value2_ += weight_*2*penalty*tmp/dVoxels;
			}
			//cout << i << " \t" << dosevector[i].value3_ << " \t" << dosevector[i].value1_ << " \t" << doselimit << " \t" << dosevector[i].value2_ << endl;
		}
	}
	
	if (is_min_constraint_) {
		
		for (unsigned int i=0; i<nVoxels; i++) {
		    get_mindose_and_weight(i/dVoxels,doselimit,penalty);
			tmp = doselimit - dosevector[i].value1_;
			if (tmp>0) {
				objective += penalty*tmp*tmp;
				dosevector[i].value2_ += (-1)*weight_*2*penalty*tmp/dVoxels;
			}
			//cout << i << " \t" << dosevector[i].value3_ << " \t" << dosevector[i].value1_ << " \t" << doselimit << " \t" << dosevector[i].value2_ << endl;
		}
	}
		    
	// now we have to bring the voxel list back into the original order
	std::sort(dosevector.begin(),dosevector.end(),compvalue3);
	d_obj_by_dose.resize(nVoxels,0);
	
	//cout<< endl;

	for (unsigned int i=0; i<nVoxels; i++) {
		d_obj_by_dose[i] =  dosevector[i].value2_;
		//cout << i << " \t" << dosevector[i].value3_ << " \t" << dosevector[i].value1_  << " \t" << dosevector[i].value2_ << endl;

	}
	
	return weight_*objective/dVoxels;
}


/**
 * Calculate the objective and first and second partial derivatives of the
 * voxel contribution to the objective with the given dose.
 *
 * @param dose The dose of the voxels in the objective.
 * @param d_obj_by_dose The vector to hold the returned partial derivatives
 * @param d2_obj_by_dose The vector to hold the returned partial derivatives
 */
double DvhObjective::calculate_objective_and_d2(const vector<float> & dose,
												vector<float> & d_obj_by_dose,
												vector<float> & d2_obj_by_dose) const
{
	std::cerr << "DvhObjective::calculate_objective_and_d2"
	<< "\nis not implemented yet." << std::endl;
	exit(-1);
}



