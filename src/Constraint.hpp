/**
 * @file Constraint.hpp
 * Constraint Class header
 *
 */

#ifndef CONSTRAINT_HPP
#define CONSTRAINT_HPP

#include <iostream>
#include <cmath>
#include <map>

#include "BixelVector.hpp"
#include "DoseInfluenceMatrix.hpp"
#include "Objective.hpp"
#include "Voi.hpp"
#include "GenericConstraint.hpp"

/**
 * Class Constraint implements a base class for all hard constraints
 *
 * Constraints are implemented through a 
 * - quadratic penalty function in an augmented Lagrangian method,
 * - projection methods
 * - interface to external solver
 */
class Constraint
{

public:

  // Virtual destructor
  virtual ~Constraint();
  
  
  //
  // general functions
  //
  
  /// get corresponding VOI
  unsigned int get_consNo() const;
  
  /// Print a description of the constraint
  virtual void printOn(std::ostream& o) const = 0;  

  // Print description of objective
  friend std::ostream& operator<< (std::ostream& o, const Constraint& constraint);

  /// initialize the Constraint
  virtual void initialize(DoseInfluenceMatrix& Dij);

  /// set upper bound of a constraint
  virtual void set_upper_bound(float value);

  /// set upper bound of a constraint
  virtual float get_upper_bound() const;

  /// set upper bound of a constraint
  virtual void set_lower_bound(float value);

  /// set upper bound of a constraint
  virtual float get_lower_bound() const;

  /// returns the maximum of two numbers
  float max(float value_1, float value_2) const;

  /// returns the minimum of two numbers
  float min(float value_1, float value_2) const;

  /// returns the minimum of three numbers
  float min(float value_1, float value_2, float value_3) const;


  //
  // functions for augmented lagrangian approach
  //
  
  virtual bool supports_augmented_lagrangian() {return(false);};
  virtual bool supports_calculation_via_dose_vector() const {return false;}; // augmented lagrangian can be calculated by supplying a dose vector

  virtual double calculate_dose_aug_lagrangian(const DoseVector & the_dose);
  virtual double calculate_dose_aug_lagrangian_and_gradient(const DoseVector & the_dose, DoseVector & dose_gradient);

  /// calculate the augmented lagrangian function
  virtual double calculate_aug_lagrangian(
                const BixelVector & beam_weights,
                DoseInfluenceMatrix & Dij);

  /// calculate the augmented lagrangian function
  virtual double calculate_aug_lagrangian(
                const BixelVector & beam_weights,
                DoseInfluenceMatrix & Dij,
				double & constraint,
				double & merit);

  /// calculate the augmented lagrangian function and its gradient
  virtual double calculate_aug_lagrangian_and_gradient(
                const BixelVector & beam_weights,
                DoseInfluenceMatrix & Dij,
                BixelVectorDirection & gradient,
                float gradient_multiplier);

   /// update lagrange parameter
  virtual void update_lagrange_multipliers(const BixelVector & beam_weights,
										   DoseInfluenceMatrix & Dij);

  /// update lagrange parameter
  virtual void update_penalty(const BixelVector & beam_weights,
							  DoseInfluenceMatrix & Dij, 
							  float tol, 
							  float multiplier);

  /// get lagrange multipliers as pairs <voxelNo,value>
  virtual vector<pair<unsigned int,float> > get_lagrange_multipliers();


  //
  // functions for projection method
  //
  
  virtual bool supports_projection_solver() const {return(false);};
  virtual bool supports_projection_optimizer() const {return(false);};

  /// sequentially project onto the constraints
  virtual unsigned int project_onto(BixelVector & beam_weights,
				    DoseInfluenceMatrix & Dij);

  /// ckeck if active set is empty
  virtual bool is_active_set_empty() const;

  /// put all constraints back into the active set
  virtual void reset_active_set();


  //
  // external solver interface
  //
  
  virtual bool supports_external_solver() const {return(false);};

  /// return number of geeric constraints
  virtual unsigned int get_nGeneric_constraints() const;

  /// generate generic constraints for external solver
  virtual void add_to_optimization_data_set(GenericOptimizationData & data, 
					    DoseInfluenceMatrix & Dij) const;


protected:

  // Constructor for dirived classes only
  Constraint(unsigned int ConsNo);
  
  /// the number of the constraint
  unsigned int consNo_;

  /// if constraint has been initialized
  bool is_initialized_;

  /// output information on screen is true
  bool verbose_;

private:

  // Default constructor not allowed
  Constraint();
};

/*
 * Inline functions
 */


inline
unsigned int Constraint::get_consNo() const
{
  return consNo_;
}

inline
void Constraint::initialize(DoseInfluenceMatrix& Dij)
{
  // do nothing
}

inline
std::ostream& operator<< (std::ostream& o, const Constraint& constraint)
{
  constraint.printOn(o);
  return o;
}

inline
float Constraint::max(float value_1, float value_2) const
{
  if (value_1 > value_2){
    return value_1;
  }
  else {
    return value_2;
  }
}

inline
float Constraint::min(float value_1, float value_2) const
{
  if (value_1 < value_2){
    return value_1;
  }
  else {
    return value_2;
  }
}

inline
float Constraint::min(float value_1, float value_2, float value_3) const
{
  if (value_1 < value_2){
    return min(value_1,value_3);
  }
  else {
    return min(value_2,value_3);
  }
}


 

#endif
