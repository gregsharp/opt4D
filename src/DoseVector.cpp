
#include <iostream>
#include <vector>
#include <cstdio>
#include <cstdlib>

#include "DoseVector.hpp"

/**
 * @file DoseVector.cpp
 * DoseVector class functions.
 */


/**
 * Constructor: Creates empty voxel grid of desired size (default 0)
 */
DoseVector::DoseVector(size_t nVoxels)
  : nVoxels_(nVoxels)
  , dose_(nVoxels, 0)
{
}

/**
 * Scale dose for all voxels by scale_factor
 *
 * @param scale_factor the number to multiply all voxels by
 */
void DoseVector::scale_dose(float scale_factor)
{
    for(size_t i=0; i<nVoxels_; i++)
        dose_[i] *= scale_factor;

    max_dose_ *= scale_factor;
}


/**
 * Reset dose for all voxels to zero
 */
void DoseVector::reset_dose()
{
  fill(dose_.begin(), dose_.end(), 0);
  max_dose_ = 0;
}


/**
 * Write dose distribution to a file
 *
 * @param doseFile name of file to write out
 */
void DoseVector::write_dose(string doseFile)const
{
    FILE *fOutput=NULL;

    fOutput = fopen(doseFile.c_str(),"wb");
    if (fOutput == NULL) {
      std::cerr << "Can't open file: " << doseFile << std::endl;
        exit(-1);
    }

    for (size_t i=0; i<nVoxels_; i++) {
        fwrite(&dose_[i],sizeof(float),1,fOutput);
    }

    fclose(fOutput);
}


/**
 * Find the maximum dose of any voxel in the grid.
 */
float DoseVector::find_max_dose()
{
  // determine max dose of any voxel in dose cube
  max_dose_ = 0.;
  for(size_t iVoxel=0; iVoxel<nVoxels_; iVoxel++) {
    if(dose_[iVoxel] > max_dose_)
      max_dose_ = dose_[iVoxel];
  }

  return max_dose_;
}

