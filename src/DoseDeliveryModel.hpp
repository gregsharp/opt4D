#ifndef DOSEDELIVERYMODEL_HPP
#define DOSEDELIVERYMODEL_HPP

#include <vector>
using std::vector;
#include <string>
using std::string;
#include <algorithm>

// Predefine class before including others
class DoseDeliveryModel;
class StandardDoseModel;
class AuxiliaryBasedSystematicErrorModel;
class RandomScenario;
class RigidDoseShift;

#include "DoseVector.hpp"
#include "BixelVector.hpp"
#include "DoseInfluenceMatrix.hpp"
#include "Dvh.hpp"
#include "VirtualBixelTrafoMatrix.hpp"
#include "TxtFileParser.hpp"
#include "GaussianSetupError.hpp"


/**
 * This holds all information about a rigid dose shift
 */
class RigidDoseShift
{

    public:
        /// constructor, creates an empty shift (same as nominal shift)
        RigidDoseShift()
            : weight_(1),
            subscript_shift_(0,0,0),
            auxiliaryNo_(0),
            use_bixelNos_(false),
            bixelNos_(),
            use_bixel_auxiliaryNos_(false),
            bixel_auxiliaryNos_(),
            use_random_rounding_(false),
            shift_(0,0,0) {};

        /// Lexicographic comparison operator
        bool operator<(const RigidDoseShift& rhs) const;

        /// Swap contents with another RigidDoseShift
        void swap(RigidDoseShift & rhs);

        /// Merge with another shift
        void merge(const RigidDoseShift & rhs);

        /// set bixel_auxiliaryNos_
        void set_bixel_auxiliaryNos(vector<unsigned int> & bixel_auxiliaryNos);

        /// Weight of this shift
        float weight_;

        //
        // Sortable data (in order of sorting)
        //

        /// shift based uncertainty modelling: subscript shifts
        Voxel_Shift_Subscripts subscript_shift_;

        /// auxiliary number for shift
        unsigned int auxiliaryNo_;

        //
        // Beamlets that shift applies to
        //

        /// True if only some bixels are included in shift
        bool use_bixelNos_;

        /// bixel numbers that correspond to the shift
        vector<unsigned int> bixelNos_;

        /// True if each bixel listed in bixelNos_ can have a different
        /// auxiliary number
        bool use_bixel_auxiliaryNos_;

        /// The auxiliary number for each bixel listed in bixelNos_
        vector<unsigned int> bixel_auxiliaryNos_;

        //
        // Random Rounding
        //

        /// Whether to use random rounding
        bool use_random_rounding_;

        /// shift in patient coordinates (to use for random rounding)
        /// Randomly chosen during merge
        Shift_in_Patient_Coord shift_;

        /// Write out scenario to stream
        friend std::ostream& operator<< (std::ostream& o, const RigidDoseShift& rhs);
};


/// Swap contents of two RigidDoseShift objects
inline
void swap(RigidDoseShift & lhs, RigidDoseShift & rhs) { lhs.swap(rhs); };


/**
 * This holds all information about a random scenario
 */
class RandomScenario
{

    public:
        /// constructor, creates an empty scenario
        RandomScenario()
            : rigid_shifts_(),
            auxiliaryNos_(),
            range_shifts_(),
            bixelNos_(),
            Pdf_(),
            auxiliary_dij_lut_(),
            virtual_bixel_trafo_() ,
	    virtual_bixel_patient_shifts_(),
	    use_single_instance_(false),
	    instanceNo_(0) {};

        /// Round shifts
        void round_shifts(const Geometry& geo);

        /// Linearly interpolate shifts
        void linearly_interpolate_shifts(const Geometry& geo);

        /// Sort the shifts and merge identical shifts
        void merge_shifts();

        //
        // Public data
        //

        /// shift based uncertainty modelling
        vector<RigidDoseShift> rigid_shifts_;

        /// auxiliary based uncertainty modelling: auxiliary number for each
        /// beamlet
        vector<unsigned int> auxiliaryNos_;

        /// range error
        vector<float> range_shifts_;


        /// shift based uncertainty modelling: bixel numbers that correspond to a shift
        vector<vector<unsigned int> > bixelNos_;

        /// Respiratory motion model: Pdf realization
        vector<float> Pdf_;
        /// Respiratory motion model: corresponding auxiliary Dij indices
        vector<unsigned int> auxiliary_dij_lut_;

        /// transformation from physical to virtual beamlets
        mutable TrafoMatrix virtual_bixel_trafo_;

        /// Vector to hold shifts used for virtual beamlet classes
        vector <Shift_in_Patient_Coord> virtual_bixel_patient_shifts_;

        /// Write out scenario to stream
        friend std::ostream& operator<< (std::ostream& o, const RandomScenario& rhs);

        /// an instance number for instance based models
        bool use_single_instance_;
        unsigned int instanceNo_;
};



// ---------------------------------------------------------------------
// DoseDeliveryModel base class
// ---------------------------------------------------------------------

/**
 * DoseDeliveryModel is an abstract base class for different types of dose
 * delivery models. Derived classes, for example, include the StandardDoseModel
 * which handles a standard IMRT case with no uncertainty or motion, or the
 * RangeUncertaintyModel which handles range uncertainties in IMPT.
 *
 * A DoseDeliveryModel is incorporated into the fluence map optimization
 * either via a special objective (e.g. ExpectedQuadraticObjectiveIntegrate)
 * or a Meta-objective which contains a vector of primary objectives
 * (e.g. ExpectedValueMetaObjective). If none of those objectives is specified,
 * the fluence map optimization is performed by ignoring the DoseDeliveryModel
 * and utilizing the nominal Dij matrix only. The DoseDeliveryModel is however
 * accounted for during the output of results.
 */
class DoseDeliveryModel
{

  public:
    /// constructor
    DoseDeliveryModel(const Geometry *the_geometry);

    // Virtual destructor
    virtual ~DoseDeliveryModel();

    /// returns type of uncertainty model
    virtual std::string get_uncertainty_model() const = 0;

    /// returns number of instances
    virtual unsigned int get_nInstances() const;

    /// generate a random sample scenario
    virtual void generate_random_scenario(RandomScenario &random_scenario) const = 0;

    /// generate the nominal scenario
    virtual void generate_nominal_scenario(RandomScenario &random_scenario)
        const = 0;

    /// calculate voxel dose for a scenario
    virtual float calculate_voxel_dose(unsigned int voxelNo,
				       const BixelVector &bixel_grid,
				       DoseInfluenceMatrix &dij,
				       const RandomScenario &random_scenario) const = 0;

    /// calculate gradient contribution of the voxel for a scenario
    virtual void calculate_voxel_dose_gradient(
            unsigned int voxelNo,
 	    const BixelVector &bixel_grid,
            DoseInfluenceMatrix &dij,
            const RandomScenario &random_scenario,
            BixelVectorDirection &gradient,
            float gradient_multiplier) const = 0;

    /// calculate the expected dose in a voxel
    virtual float calculate_expected_voxel_dose(unsigned int voxelNo,
						const BixelVector &bixel_grid,
						DoseInfluenceMatrix &dij) const;

    /// calculate the variance of the dose
    virtual float calculate_voxel_variance(unsigned int voxelNo,
					   const BixelVector &bixel_grid,
					   DoseInfluenceMatrix &dij) const;

    /// True if calculate_voxel_variance is supported by an uncertainty model
    virtual bool supports_calculate_voxel_variance() const;

    /// calculate gradient contribution of the voxel variance
    virtual float calculate_quadratic_objective(
            unsigned int voxelNo,
            float prescribed_dose,
            const BixelVector &bixel_grid,
            DoseInfluenceMatrix &dij,
            BixelVectorDirection &gradient,
            float gradient_multiplier,
            double adjusted_weight) const;

    /// calculate bixel pair contributions to quadratic objective
    virtual void calculate_bixel_pair_contributions_to_quadratic_objective(
	unsigned int voxelNo,
	vector< vector<double> > &QuadraticTerms,
	vector<double> &LinearTerms,
	vector<float> &Temp_DijRow,
	vector<float> &Temp_LinearTerms) const;

    /// calculate dose for all voxels
    void calculate_dose(DoseVector &dose_vector,
			const BixelVector &bixel_grid,
			DoseInfluenceMatrix &dij,
			const RandomScenario &random_scenario) const;

    /// calculate expected dose for all voxels
    void calculate_expected_dose(DoseVector &dose_vector,
				 const BixelVector &bixel_grid,
				 DoseInfluenceMatrix &dij) const;

    /// calculate expected dose for all voxels
    void calculate_nominal_dose(DoseVector &dose_vector,
				const BixelVector &bixel_grid,
				DoseInfluenceMatrix &dij) const;

    /// calculate variance for all voxels
    void calculate_variance(DoseVector &variance,
			    const BixelVector &bixel_grid,
			    DoseInfluenceMatrix &dij) const;

    /// calculate instance dose
    virtual void calculate_instance_dose(DoseVector &dose_vector,
					 const BixelVector &bixel_grid,
					 DoseInfluenceMatrix &dij,
					 unsigned int iInstance) const;
    /// True if calculate_variance is supported by an uncertainty model
    bool supports_calculate_variance() const;

    /// True if the objective can be calculated exactly by an uncertainty model
    // virtual bool supports_calculate_exact_objective() const {return false;};
    virtual bool has_discrete_scenarios() const {return false;};

    /// True if the objective can be calculated exactly by an uncertainty model
    virtual bool supports_calculate_quadratic_objective_integrate() const {return false;};

    /// True if the objective can be calculated exactly by an uncertainty model
    virtual bool supports_calculate_quadratic_objective_tensor() const {return false;};

    /// gives gaussian random number
    float get_normal_distributed_random_number(float width) const;

    /// gives gaussian random number from a truncated distribution
    float get_truncated_normal_distributed_random_number(float width, float cutoff = 2) const;

    /// gives a 3D Gaussian setup error in a 2sigma elipse
    Shift_in_Patient_Coord get_truncated_gaussian_setup_error(float sigmaX, float sigmaY, float sigmaZ, float cutoff = 2) const;

    /// write results
    virtual void write_dose(BixelVector &bixel_grid,
			    DoseInfluenceMatrix &dij,
			    string file_prefix,
			    Dvh dvh) const;

    /// write dose for scenarios
    void write_scenario_dose(BixelVector &bixel_grid,
			     DoseInfluenceMatrix &dij,
			     string file_prefix,
			     Dvh dvh) const;

    /// store a dose evaluation scenario
    virtual bool set_dose_evaluation_scenario(map<string,string> scenario_map);

    /// get number of dose evaluation scenarios
    unsigned int get_nDose_eval_scenarios() const {return nDose_eval_scenarios_;};

    /// get a dose evaluation scenario
    RandomScenario get_dose_eval_scenario(unsigned int scenarioNo) const
	{return (dose_eval_scenarios_.at(scenarioNo));};

    /// get a random dose evaluation scenario
    RandomScenario get_random_dose_eval_scenario() const;

    /// get probability for dose evaluation scenario
    float get_dose_eval_pdf(unsigned int scenarioNo) const
	{return dose_eval_pdf_.at(scenarioNo);};

    /// normalize the PDF to one
    void normalize_dose_eval_pdf();

    class options;

  protected:

    /// geometry information
    const Geometry* the_geometry_;

    /// number of dose evaluation scenarios
    unsigned int nDose_eval_scenarios_;

    /// scenarios for which the dose distribution is computed in the end
    vector<RandomScenario> dose_eval_scenarios_;

    /// probability associated with a dose evaluation scenario
    vector<float> dose_eval_pdf_;

  private:
    /// Make sure default constructor is never used
    DoseDeliveryModel();

};


/*
 * Inline functions
 */


// Write out a pair to a stream
template <class T1, class T2>
std::ostream& operator<<(
        std::ostream& o,
        const std::pair<T1,T2>& rhs)
{
    o << "(" << rhs.first() << ", " << rhs.second() << ")";
    return o;
};


inline
unsigned int DoseDeliveryModel::get_nInstances() const
{
    return 1;
}

inline
float DoseDeliveryModel::calculate_voxel_variance(unsigned int voxelNo,
						 const BixelVector &bixel_grid,
						 DoseInfluenceMatrix &dij) const
{
    cout << "WARNING: variance calculation not implemented for "
	 << get_uncertainty_model() << endl;
    return(0);
}

/**
 * True if calculate_voxel_variance is supported by an uncertainty model
 */
inline
bool DoseDeliveryModel::supports_calculate_voxel_variance() const
{
    return false;
}

inline
float DoseDeliveryModel::calculate_expected_voxel_dose(unsigned int voxelNo,
						      const BixelVector &bixel_grid,
						      DoseInfluenceMatrix &dij) const
{
    cout << "WARNING: expected dose calculation not implemented for "
	 << get_uncertainty_model() << endl;
    return(0);
}

inline
float DoseDeliveryModel::calculate_quadratic_objective(
    unsigned int voxelNo,
    float prescribed_dose,
    const BixelVector &bixel_grid,
    DoseInfluenceMatrix &dij,
    BixelVectorDirection &gradient,
    float gradient_multiplier,
    double adjusted_weight) const
{
    cout << "ERROR: calculate_quadratic_objective not implemented for "
	 << get_uncertainty_model() << endl;
    exit(-1);
}

inline
void DoseDeliveryModel::calculate_bixel_pair_contributions_to_quadratic_objective(
    unsigned int voxelNo,
    vector< vector<double> > &QuadraticTerms,
    vector<double> &LinearTerms,
    vector<float> &Temp_DijRow,
    vector<float> &Temp_LinearTerms) const
{
    cout << "ERROR: calculate_bixel_pair_contributions_to_quadratic_objective not implemented for "
	 << get_uncertainty_model() << endl;
    exit(-1);
}

/// True if calculate_variance is supported by an uncertainty model
inline bool DoseDeliveryModel::supports_calculate_variance() const
{
    return supports_calculate_voxel_variance();
}

inline void DoseDeliveryModel::calculate_instance_dose(
    DoseVector &dose_vector,
    const BixelVector &bixel_grid,
    DoseInfluenceMatrix &dij,
    unsigned int iInstance) const
{
    cout << "WARNING: calculate_instance_dose not applicable for "
	 << get_uncertainty_model() << endl;

    // clear dose vector
    dose_vector.reset_dose();
}

inline bool DoseDeliveryModel::set_dose_evaluation_scenario(map<string,string> scenario_map)
{
    cout << "WARNING: set_dose_evaluation_scenario(map<string,string>) not implemented for "
	 << get_uncertainty_model() << endl;

    return(false);
}

inline void DoseDeliveryModel::write_dose(BixelVector &bixel_grid,
					  DoseInfluenceMatrix &dij,
					  string file_prefix,
					  Dvh dvh) const
{
    // do nothing
}


// ---------------------------------------------------------------------
// standard dose model
// ---------------------------------------------------------------------

/**
 * This Instance of a DoseDeliveryModel represents the standard case,
 * i.e. there is neither motion nor uncertainty
 */
class StandardDoseModel : public DoseDeliveryModel
{

  public:
    /// constructor
    StandardDoseModel(const Geometry *the_geometry);

    // Virtual destructor
    virtual ~StandardDoseModel();

    /// returns type of uncertainty model
    virtual std::string get_uncertainty_model() const {
	return("StandardDoseModel"); }

    /// generate a random sample scenario
    virtual void generate_random_scenario(RandomScenario &random_scenario) const;

    /// generate the nominal scenario
    virtual void generate_nominal_scenario(
            RandomScenario &random_scenario) const;

    /// calculate voxel dose for a scenario
    virtual float calculate_voxel_dose(unsigned int voxelNo,
				       const BixelVector &bixel_grid,
				       DoseInfluenceMatrix &dij,
				       const RandomScenario &random_scenario) const;

    /// calculate gradient contribution of the voxel for a scenario
    virtual void calculate_voxel_dose_gradient(
            unsigned int voxelNo,
 	    const BixelVector &bixel_grid,
            DoseInfluenceMatrix &dij,
            const RandomScenario &random_scenario,
            BixelVectorDirection &gradient,
            float gradient_multiplier) const;

};

// ---------------------------------------------------------------------
// base class for auxiliary based models for systematic errors
// ---------------------------------------------------------------------

/**
 * AuxiliaryBasedSystematicErrorModel is an abstract base class for
 * uncertainty models which incorporate any kind of systematic error and
 * model dose distributions for occuring errors using auxiliary Dij matrices
 */
class AuxiliaryBasedSystematicErrorModel : public DoseDeliveryModel
{

  public:
    /// constructor
    AuxiliaryBasedSystematicErrorModel(const Geometry *the_geometry, unsigned int nInstances=1);

    // Virtual destructor
    virtual ~AuxiliaryBasedSystematicErrorModel();

    unsigned int get_nInstances() const;
    float get_Pdf(unsigned int iInstance) const;
    unsigned int get_AuxiliaryDijNo(unsigned int iInstance) const;

    /// calculate voxel dose for a scenario
    float calculate_voxel_dose(unsigned int voxelNo,
			       const BixelVector &bixel_grid,
			       DoseInfluenceMatrix &dij,
			       const RandomScenario &random_scenario) const;

    /// calculate gradient contribution of the voxel for a scenario
    void calculate_voxel_dose_gradient(
            unsigned int voxelNo,
 	    const BixelVector &bixel_grid,
            DoseInfluenceMatrix &dij,
            const RandomScenario &random_scenario,
            BixelVectorDirection &gradient,
            float gradient_multiplier) const;

  protected:
    void set_Pdf(unsigned int iInstance, float value);
    void set_AuxiliaryDijNo(unsigned int iInstance, int value);

  private:
    /// Make sure default constructor is never used
    AuxiliaryBasedSystematicErrorModel();

    /// number of instances
    unsigned int nInstances_;

    /// probability for each instance
    vector<float> pdf_;

    /// look up table for the assignment of instanceNo and AuxiliaryDijNo
    vector<unsigned int> auxiliary_dij_lut_;
};

inline
unsigned int AuxiliaryBasedSystematicErrorModel::get_nInstances() const
{
    return nInstances_;
}

inline
unsigned int AuxiliaryBasedSystematicErrorModel::get_AuxiliaryDijNo(unsigned int iInstance) const
{
    return auxiliary_dij_lut_.at(iInstance);
}

inline
float AuxiliaryBasedSystematicErrorModel::get_Pdf(unsigned int iInstance) const
{
    return pdf_.at(iInstance);
}

inline
void AuxiliaryBasedSystematicErrorModel::set_AuxiliaryDijNo(unsigned int iInstance, int value)
{
    auxiliary_dij_lut_.at(iInstance) = value;
}

inline
void AuxiliaryBasedSystematicErrorModel::set_Pdf(unsigned int iInstance, float value)
{
    pdf_.at(iInstance) = value;
}



// ---------------------------------------------------------------------
// base class for shift based models for systematic errors
// ---------------------------------------------------------------------

/**
 * ShiftBasedErrorModel is an abstract base class for
 * uncertainty models which incorporate any kind of systematic error and
 * model dose distributions for occuring errors by shifting voxels.  Only
 * changes the rigid_shifts_ vector in the RandomScenario.
 */
class ShiftBasedErrorModel : public DoseDeliveryModel
{

  public:
    /// constructor
    ShiftBasedErrorModel(const Geometry *the_geometry);

    /// destructor
    virtual ~ShiftBasedErrorModel();

    /// generate nominal scenario
    virtual void generate_nominal_scenario(
            RandomScenario &random_scenario) const;

    /// calculate voxel dose for a scenario
    float calculate_voxel_dose(unsigned int voxelNo,
            const BixelVector &bixel_grid,
            DoseInfluenceMatrix &dij,
            const RandomScenario &random_scenario) const;

    /// calculate gradient contribution of the voxel for a scenario
    void calculate_voxel_dose_gradient(
            unsigned int voxelNo,
 	    const BixelVector &bixel_grid,
            DoseInfluenceMatrix &dij,
            const RandomScenario &random_scenario,
            BixelVectorDirection &gradient,
            float gradient_multiplier) const;

    /// write dose to a file
    virtual void write_dose(
            BixelVector &bixel_grid,
            DoseInfluenceMatrix &dij,
            string file_prefix,
            Dvh dvh) const;


  private:
    /// Make sure default constructor is never used
    ShiftBasedErrorModel();

};



/**
 * holds parameters to describe the uncertainty
 */
class DoseDeliveryModel::options
{
  public:

    /// constructor
    options(const TxtFileParser* pln_parser);

    /// set surface normal vectors
    void set_surface_normals(const TxtFileParser* pln_parser, const Geometry* the_geometry);

    /// the dose delivery model to be created
    string dose_delivery_model_;

    // -------------------- range uncertainty --------------------------

    /// beamlet correlation model
    string range_correlation_model_;

    /// the range uncertainty (sigma of a Gaussian, same for each bixel)
    float range_sigma_;

    /// file name to read range uncertainty (sigma of a Gaussian) for each beamlet
    string range_sigma_file_root_;

    // for auxiliary based model
    unsigned int range_nRanges_; // number of auxiliaries/range shifts
    float range_max_dev_; // maximum range deviation in multiples of the distribution width
    bool range_readDijs_; // whether to read precalculated auxiliaries
    bool range_writeDijs_; // whether to write generated auxiliaries
    string range_auxDij_root_; // file root for written Dijs

    // virtual bixel based
    float range_dWEL_; // distance of energy layers in water-equivalent range


    // -------------------- setup uncertainty --------------------------

    // Various options
    GaussianSetupError::options gaussian_setup_options_;

    // systematic error
    // float setup_sigmaX_sys_;
    // float setup_sigmaY_sys_;
    // float setup_sigmaZ_sys_;

    // random error
    // float setup_sigmaX_rand_;
    // float setup_sigmaY_rand_;
    // float setup_sigmaZ_rand_;

    // Cut-off to use when taking samples
    // float setup_cutoff_;

    //
    // shift based modelling
    //

    /// switch on randomized rounding
    bool setup_use_randomized_rounding_;

    /// switch on linear interpolation
    bool setup_use_linear_interpolation_;

    /// switch on merging of matching shifts
    bool setup_merge_shifts_;

    /// switch on improved static dose cloud approximation
    bool setup_use_improved_sdca_;

    /// normal vectors on patient surface for each beam
    vector<Shift_in_Patient_Coord> setup_surface_normals_;


    // -------------------- pdf variation model --------------------------

    /// file name containing the pdf
    string pdf_file_name_;

    /// number of Pdf variation modes
    unsigned int pdf_nModes_;


    // --------------------- miscellaneous ------------------------------

    /// number of fractions
    unsigned int nFractions_; // number of fractions

    // --------------------- BED ------------------------------

    /// standard fraction dose
    float standard_fraction_dose_; 

    /// scale factor for LET in simple RBE model
    float LET_scalefactor_;

    /// scale factor for physical in simple RBE model
    float physD_scalefactor_;

    /// file roots for virtual bixel Dij
    string virtual_bixel_dij_root_;
    string virtual_bixel_stf_root_;
};




#endif
