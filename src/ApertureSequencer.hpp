/**
 * @file ApertureSequencer.hpp
 * ApertureSequencer class.
 * <pre>
 * Ver
 * </pre>
 */

#ifndef APERTURESEQUENCER_HPP
#define APERTURESEQUENCER_HPP

// Predefine class before including others
class ApertureSequencer;

#include <exception>
#include <fstream>
#include <iostream>
using std::cerr;
#include <string>
#include "Plan.hpp"

/// All types of sequencers that may be used to convert bixel map to apertures
/// When adding a new sequencer update print and use_sequencer() functions
enum SequencerTypes {
    VMAT_DUMMY
};

/// Print type of aperture sequencer
inline std::ostream& operator<< ( std::ostream& o,  const SequencerTypes seq )
{
    switch ( seq ) {
    case VMAT_DUMMY:
        return o << "VMAT_DUMMY";
    default:
        throw std::logic_error("Invalid enum value.");
    }
};

class ApertureSequencer {
    public:

        ApertureSequencer(Plan* thisplan);
        virtual ~ApertureSequencer();

        /**
         * ApertureSequencer options
         *
         * Make sure you update the print_on function when adding or changing options.
         */
        class options {
            public:
                options() : seqType_(), machine_file_(""),  optMlc_(true),
                            optJaw_(true),  nFields_(1),  cpPerBeam_(1) { };

                /// Print options
                friend std::ostream& operator<< ( std::ostream& o, const ApertureSequencer::options& opt );

                /// Type of sequencer to use to convert bixel map to apertures
                enum SequencerTypes seqType_;

                /// Filename of machine model file describing machine capabilities and constraints
                std::string machine_file_;

                /// optimize MLC leaf position directly
                bool optMlc_;

                /// optimize collimator jaw position directly
                bool optJaw_;

                /// Fixed number of fields or arcs in plan
                unsigned int nFields_;

                /// Number of segments per field or arc control points per dij beam
                unsigned int cpPerBeam_;
        };

        void set_options( ApertureSequencer::options options);
        ApertureSequencer::options get_options();

        /// Sequence the plan apertures.
        void use_sequencer();

        class exception;

    private:

        /// Sequence the plan using the vmat dummy method
        void use_vmat_dummy_sequencer();

        /// The plan to be sequenced
        Plan* the_plan_;

        /// Options for sequencing
        ApertureSequencer::options options_;
};


/**
 * Exception thrown when something goes wrong with the sequencer.  Simply send to cout to read.
 */
class ApertureSequencer::exception {
public:
    exception(std::string comment) : comment_(comment) {};

    // Print description of exception
    friend std::ostream& operator<< (std::ostream& o, const ApertureSequencer::exception& rhs);

private:
    /// Private default constructor to ensure that it is never called
    exception();

    /// The comment for the objective
    std::string comment_;
};


/**
 * Print the reason that the exception was called
 */
inline std::ostream& operator<< (std::ostream& o, const ApertureSequencer::exception& rhs) {
    o << rhs.comment_;
    return o;
};


/**
 * Set ApertureSequencer options
 * @param options The options to use
 */
inline void ApertureSequencer::set_options( ApertureSequencer::options options ) {
    options_ = options;
};


/**
 * Get ApertureSequencer options
 * @returns A copy of the current ApertureSequencer options
 */
inline ApertureSequencer::options ApertureSequencer::get_options() {
    return options_;
};

#endif // APERTURESEQUENCER_HPP
