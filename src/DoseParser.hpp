/**
 * @file DoseParser.hpp
 * Header for DoseParser class.
 */

#ifndef DOSEPARSER_HPP
#define DOSEPARSER_HPP

#include <vector>
using std::vector;

#include <string>
using std::string;

#include <iostream>

#include "DoseVector.hpp"

class DoseParser;

/**
 * The DoseParser class reads dose distribution from a file
 * The dose distribution is a binary file with no header
 */
class DoseParser
{
    public:

        // Constructor (Read the file)
        DoseParser(const string & file_name,
		   unsigned int nVoxels);

        // Read a file
        void read_file(const string & file_name,
		       unsigned int nVoxels);

        // get dose
        DoseVector get_dose();

    private:

        float read_binary_float(std::istream & input);
        
	/// the dose
	DoseVector dose_;
};


/**
 * Read a binary little-endian float from a stream
 */
inline
float DoseParser::read_binary_float(std::istream & input)
{
#if defined(__BIG_ENDIAN__)
    bool big_endian = true;
#else
    bool big_endian = false;
#endif

    if(big_endian) {
        char SS[4], ST[4];
        input.read(SS, 4);
        ST[0] = SS[3];  ST[1] = SS[2];  ST[2] = SS[1];  ST[3] = SS[0];
        return *( (float *) ST);
    } else {
        float temp;

        input.read((char*) &temp, sizeof(float));

        return temp;
    }
}

#endif
