/**
 * @file NonSeparableObjective.hpp
 * NonSeparableObjective Class header
 *
 * $Id: NonSeparableObjective.hpp,v 1.5 2008/04/14 20:42:13 bmartin Exp $
 */

#ifndef NONSEPARABLEOROBJECTIVE_HPP
#define NONSEPARABLEOROBJECTIVE_HPP

#include "Objective.hpp"
#include "Voi.hpp"
#include <cmath>

/**
 * Class NonSeparableObjective is a virtual class that is inherited to build 
 * objectives.
 */
class NonSeparableObjective : public Objective
{

    public:

        // Constructor
        NonSeparableObjective(
                Voi*  the_voi,
                unsigned int objNo,
                float weight,
                float sampling_fraction,
                bool sample_with_replacement
                );

        // Virtual destructor
        virtual ~NonSeparableObjective();


        //
	// NonSeparableObjective specific functions
        //
       
        /// Find out how many voxels were in objective
        virtual unsigned int get_nVoxels() const;

        /// Find out voxel sampling rate
        virtual float get_sampling_fraction() const;

        /// Find out if objective uses sampling
        virtual bool uses_sampling() const;

        /// Find out if objective supports meta objectives
        virtual bool supports_meta_objective() const;

        virtual void set_sampling_fraction(const float sampling_fraction);

        size_t get_voiNo() const;

        //
        // Functions that must be re-implemented in children
        // 
        virtual double calculate_objective(
                const vector<float> & dose) const = 0;
        virtual double calculate_objective_and_d(
                const vector<float> & dose,
	        vector<float> & d_obj_by_dose) const = 0;
        virtual double calculate_objective_and_d2(
                const vector<float> & dose,
	        vector<float> & d_obj_by_dose,
	        vector<float> & d2_obj_by_dose) const = 0;

        /// Print a description of the objective
        virtual void printOn(std::ostream& o) const = 0;
        
        //
        // Functions reimplemented from Objective
        //

        virtual void initialize(DoseInfluenceMatrix& Dij);

        virtual double calculate_objective(
                const BixelVector & beam_weights,
                DoseInfluenceMatrix & Dij,
                bool use_voxel_sampling,
                double &estimated_ssvo
                );
        virtual double calculate_objective_and_gradient(
                const BixelVector & beam_weights,
                DoseInfluenceMatrix & Dij,
                BixelVectorDirection & gradient,
                float gradient_multiplier,
                bool use_voxel_sampling,
                double &estimated_ssvo
                );

        // Calculate the objective based on just the dose vector provided
        // Returns zero if the objective does not suppor dose objectives
        virtual double calculate_dose_objective(
                const DoseVector & the_dose
                );

        // incorporating uncertainty

        /// Does the objective support RandomScenario?
        virtual bool supports_RandomScenario() const;

        // Calculate the objective for one random scenario
        virtual double calculate_scenario_objective(
                const BixelVector & beam_weights,
                DoseInfluenceMatrix & Dij,
                const DoseDeliveryModel &uncertainty_model,
                const RandomScenario &random_scenario,
                bool use_voxel_sampling,
                double &estimated_ssvo
                );

        // Calculate the objective and gradient for one random scenario
        virtual double calculate_scenario_objective_and_gradient(
                const BixelVector & beam_weights,
                DoseInfluenceMatrix & dij,
                BixelVectorDirection & gradient, 
                float gradient_multiplier,
                const DoseDeliveryModel &uncertainty_model,
                const RandomScenario &random_scenario,
                bool use_voxel_sampling,
                double &estimated_ssvo
                );


    protected:
        Voi *the_voi_;
        float sampling_fraction_;
        bool sample_with_replacement_;

    private:
        // Default constructor not allowed
        NonSeparableObjective();

        /// Calculate dose and store the results in temp_dose_
        void calculate_temp_dose(
                const BixelVector & beam_weights,
                DoseInfluenceMatrix & Dij,
                const vector<unsigned int>& voxelNos);

        /// Calculate dose for the given scenario and store in temp_dose_
        void calculate_scenario_temp_dose(
                const BixelVector & beam_weights,
                DoseInfluenceMatrix & dij,
                const DoseDeliveryModel & dose_model,
                const RandomScenario & random_scenario,
                const vector<unsigned int>& voxelNos);

        /// Temporary vector to hold dose
        vector<unsigned int> temp_voxelNos_;
        vector<float> temp_dose_;
        vector<float> temp_d_obj_by_dose_;
        vector<float> temp_d2_obj_by_dose_;
};

/*
 * Inline functions
 */


inline
size_t NonSeparableObjective::get_voiNo() const
{
  return the_voi_->get_voiNo();
}


/**
 *  Find out if objective uses sampling
 */
inline
bool NonSeparableObjective::uses_sampling() const
{
  return true;
}

inline
bool NonSeparableObjective::supports_meta_objective() const
{
  return true;
}
 

/// Does the objective support RandomScenario?
inline
bool NonSeparableObjective::supports_RandomScenario() const
{
    return true;
}


#endif
