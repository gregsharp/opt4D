/**
 * @file: OptppInterface.cpp
 * OptppInterface Class implementation.
 *
 */

#include "OptppInterface.hpp"

//using namespace::OPTPP;

#ifndef HAVE_OPTPP

// Opt++ not available

OptppInterface::OptppInterface(Plan* the_plan, Optimization::options* options)
    : the_plan_(the_plan)
    , options_(options)
{
}

OptppInterface::~OptppInterface()
{
}

void OptppInterface::optimize()
{
    cout << "Function call_optpp() was called but macro HAVE_OPTPP was not defined at compilation" << endl;
    cout << "Not optimizing the plan" << endl;
}

#else

// Opt++ available

#include "OptBCQNewton.h"
#include "OptBaQNewton.h"
#include "OptLBFGS.h"
#include "OptCG.h"
#include "OptQNIPS.h"
#include "BoundConstraint.h"

/*
 * declare global variable pointing to the instance of OptppInterface
 * created in the function call_optpp() in Optimization.cpp.
 * This is needed by the wrapper function objective_wrapper below.
 */
extern OptppInterface* global_optpp; 

/*
 * function to get the objective function value and the gradient
 */
void objective_wrapper(int mode, 
		       int nVar,                      // number of optimization variables
		       const ColumnVector& x,         // the optimization variables
		       real& optpp_objective,         // objective value
		       ColumnVector& optpp_gradient,  // gradient
		       int& result_type)
{
  // now calculate objective and gradient using the OptppInterface member function
  global_optpp->get_objective_and_gradient(mode, nVar, x, optpp_objective, optpp_gradient, result_type);
};

/*
 * function to initialize
 */
void initialize_wrapper(int nVar,                     
			ColumnVector& x)
{
  // now calculate objective and gradient using the OptppInterface member function
  global_optpp->initialize(nVar, x);
};



/*
 * Constructor
 */
OptppInterface::OptppInterface(Plan* the_plan, Optimization::options* options)
    : the_plan_(the_plan)
    , options_(options)
    , nVar_(the_plan->get_nParameters()) // number of optimization variables
    , gradient_(the_plan->get_nParameters())
    , parameter_vector_()
    , multi_objective_()
    , problem_(NULL)
    , constraints_()
{
  cout << "OptppInterface constructor called" << endl;

  // make constraints
  //make_constraints();

  // copy Plan's ParameterVector
  parameter_vector_ = the_plan_->get_parameter_vector();
  
  // create the problem
  //problem_ = new NLF1(nVar_, objective_wrapper, initialize_wrapper, constraints_);
  problem_ = new NLF1(nVar_, objective_wrapper, initialize_wrapper);

  cout << "created problem object" << endl;

  // create the optimizer object
  init_optimizer();

  cout << "done initializing" << endl;
}

OptppInterface::~OptppInterface()
{
  cout << "calling destructor of OptppInterface" << endl;
  delete problem_;
  delete optimizer_;
  delete constraints_;
  cout << "done" << endl;
}


/*
 * make constraints
 */
void OptppInterface::make_constraints()
{
  // make bound constraints
  ColumnVector lowerbound = get_lower_bounds();
  ColumnVector upperbound = get_upper_bounds(); 

  // make bound constraints
  OPTPP::Constraint bounds = new BoundConstraint(nVar_, lowerbound);
  //OPTPP::Constraint bounds = new BoundConstraint(nVar_, lowerbound, upperbound);

  // merge all constraints into one object
  constraints_ = new CompoundConstraint(bounds);
}


/*
 * get lower variable bounds from the plan's ParameterVector
 */
ColumnVector OptppInterface::get_lower_bounds()
{
  // copy the plan's current parameter vector
  synchronize_parameter_vector();

  ColumnVector bound(nVar_);

  for(unsigned int i=0; i<nVar_; i++) {
    bound(i+1) = parameter_vector_->get_lowBound(i);
  }

  return(bound);
}

/*
 * get upper variable bounds from the plan's ParameterVector
 */
ColumnVector OptppInterface::get_upper_bounds()
{
  // copy the plan's current parameter vector
  synchronize_parameter_vector();

  ColumnVector bound(nVar_);

  for(unsigned int i=0; i<nVar_; i++) {
    bound(i+1) = parameter_vector_->get_highBound(i);
  }

  return(bound);
}

/*
 * synchronize local copy of the parameter vector with the plans parameter vector
 */
void OptppInterface::synchronize_parameter_vector()
{
  parameter_vector_ = the_plan_->get_parameter_vector();
}

/*
 * copy the opt++ ColumnVector variables to an opt4D ParameterVector
 */
void OptppInterface::set_parameter_vector(const ColumnVector& x)
{
  for(unsigned int i=0; i<nVar_; i++) {
    parameter_vector_->set_value(i,x(i+1));
  }
} 

/*
 * function to get the objective function value and the gradient
 */
void OptppInterface::get_objective_and_gradient(int mode, 
						int nVar,                      // number of optimization variables
						const ColumnVector& x,         // the optimization variables
						real& optpp_objective,         // objective value
						ColumnVector& optpp_gradient,  // gradient
						int& result_type)
{
  // first convert the opt++ variable vector to a opt4D parameter vector
  set_parameter_vector(x);

  // clear gradient
  gradient_.clear_values();
  
  // get objective and gradient
  double objective = the_plan_->calculate_objective_and_gradient(*the_plan_->get_dose_influence_matrix(), 
																 *parameter_vector_,
																 gradient_,
																 multi_objective_);    
  
  // get objective value
  if (mode & NLPFunction) { // bit wise AND, NLPFunction=1
    optpp_objective = objective;
    cout << "getting objective: " << optpp_objective << endl;
    result_type = NLPFunction;
  }

  // get gradient
  if (mode & NLPGradient) { // bit wise AND, NLPGradient=2
    cout << "evaluating gradient" << endl;
    set_optpp_gradient(optpp_gradient);
    result_type = NLPGradient;
  }
}

/*
 * set the internal opt++ gradient to the one obtained from opt4D
 */
void OptppInterface::set_optpp_gradient(ColumnVector& optpp_gradient) const
{
  for(unsigned int i=0; i<nVar_; i++) {
    optpp_gradient(i+1) = gradient_.get_value(i);
  }
} 


/*
 * init function: initializes variables
 */
void OptppInterface::initialize(int nVar, ColumnVector& x)
{
  synchronize_parameter_vector();

  for(unsigned int i=0; i<nVar; i++) {
    x(i+1) = parameter_vector_->get_value(i);
  }
}

/*
 * build optimizer
 */
void OptppInterface::init_optimizer()
{

  // build the optimizer object
  optimizer_ = new OptLBFGS(problem_); 
  //optimizer_ = new OptCG(problem_); 
  //optimizer_ = new OptQNIPS(problem_); 
  //optimizer_ = new OptBCQNewton(problem_); 
  //optimizer_ = new OptBaQNewton(problem_); 

  // set output file
  string filename = options_->out_file_prefix_ + "optpp.out";
  optimizer_->setOutputFile(filename.c_str(), 0);

  // set options
  optimizer_->setMaxIter(options_->max_steps_);
  //objfcn.setSearchStrategy(LineSearch);
  //objfcn.setMaxBacktrackIter(20);
  //objfcn.setLSTol(1e-6);
  //objfcn.setMaxFeval(10);

  // activate debugging output
  problem_->setDebug();
  //optimizer_->setDebug();
}  

/*
 * optimize the plan
 */
void OptppInterface::optimize()
{
  // reset problem or optimizer
  //problem_->reset();
  //optimizer_->reset();

  // optimize
  optimizer_->optimize();
  
  // print optimization info
  optimizer_->printStatus("Solution");
  
  // get the solution
  ColumnVector x = problem_->getXc();
  set_parameter_vector(x);
  for(unsigned int i=0; i<nVar_; i++) {
    the_plan_->set_parameter_vector(i,x(i+1));
  }

  optimizer_->cleanup();      
}  


#endif
