
/**
 * @file Plan.hpp
 * @version 0.9.4
 *
 * $Id: Plan.hpp,v 1.76 2008/04/14 15:29:29 bmartin Exp $
 */

#ifndef PLAN_HPP
#define PLAN_HPP

#include <vector>
#include <string>
#include <iostream>

class Plan;

#include "Voi.hpp"
#include "ParameterVectorDirection.hpp"
#include "DoseVector.hpp"
#include "DoseInfluenceMatrix.hpp"
#include "Objective.hpp"
#include "MetaObjective.hpp"
#include "Constraint.hpp"
#include "RangeUncertaintyModel.hpp"
#include "DoseDeliveryModel.hpp"
#include "RespiratoryMotionModel.hpp"
#include "VirtualBixelModel.hpp"
#include "BiologicallyEquivalentDoseModel.hpp"
#include "TxtFileParser.hpp"
#include "Dvh.hpp"
#include "ApertureSequencer.hpp"


const string DOSE_FILE_SUFFIX = "dose.dat";
const string DVH_FILE_SUFFIX = "DVH.dat";

/**
 * Class Plan initializes and stores a treatment plan.
 * The plan is initialized by either reading the KonRad files or constructing a phantom plan.
 * <p>
 * The treatment plan consists of:
 * <ul>
 * <li>The name of the plan
 * <li>Several volume of information classes.
 * <li>One voxel grid class.
 * <li>One bixel grid class per beam within each instance.
 * <li>One dose calculation class per beam within each instance.
 * </ul>
 * <p>
 * <b>Multiple Instances</b> One of the strengths of opt4D is in the way it allows the use of multiple instances of the
 * plan to be combined.  Typically, this is used to combine information about multiple breathing phases.  The
 * contributions of all instances are added when calculating the dose, though finding the dose for individual instances
 * is possible.
 * <p>
 * The overall algorithm used to work like this:
 * <ol>
 *   <li> Initialize by loading Konrad files and setting initial beam weights to zero.
 *   <li> Compute the dose from the beam weights
 *   <li> Compute the DVH for each VOI
 *   <li> Calculate Objective
 *   <li> Calculate first and second partial derivative of objective with respect to the dose in each voxel.
 *   <li> Quit if objective is satisfactory.
 *   <li> Find new beam weights:
 *   <ol>
 *     <li>Calculate the first and second derivative of the objective with respect to each beam weight
 *     <li>Update the beam weights based on a scaled gradient method.
 *   </ol>
 *   <li> Go back to step 2.
 * </ol>
  */
class Plan
{
public:
	class options;

	// Construct a plan based on prescription file
	Plan( std::string plan_file,  Plan::options options );

	// Broken Constructor for phantom plan
	Plan( unsigned int nInstances,  unsigned int nBeams );

	// Destructor
	~Plan();

	// Access functions
	unsigned int get_nInstances()const
	{
	    return nInstances_;
	};
	unsigned int get_nBeams()const
	{
	    return nBeams_;
	};
	std::string get_name()const
	{
	    return plan_file_root_;
	};

	const DoseInfluenceMatrix* get_dose_influence_matrix()const;
	DoseInfluenceMatrix* get_dose_influence_matrix();
        const Geometry* get_geometry() const {return &geometry_; };

	/*
	 * Functions about Volumes of Interest
	 */
	unsigned int get_nVois()const;
	const Voi* get_voi( const unsigned int voiNo )const;
	Voi* get_voi( const unsigned int voiNo );
	bool is_target(unsigned int voiNo )const;

	void print_voi_info(std::ostream& o )const;

	void calculate_voi_stats( const DoseVector & dose );
	void print_voi_stats( std::ostream& o );
	void print_voi_stats( const DoseVector & dose, std::ostream& o );

	void resample_vois();
	void resample_vois_with_replacement();
	void resample_voi(const unsigned int voiNo);
	void resample_voi_with_replacement( const unsigned int voiNo );

	/*
	 * Functions about bixel intensities
	 */
	void load_parameter_files( string bwf_file_root );
	void write_parameter_files( string bwf_file_root ) const;

	unsigned int get_nParameters() const ;
        unsigned int get_nBixels() const ;

	std::auto_ptr<ParameterVector> get_parameter_vector() const;
    ParameterVector* get_pointer_to_parameter_vector();
	void set_parameter_vector( const ParameterVector& rhs );
    void set_parameter_vector(const unsigned int index, const float value);
	void swap_parameter_vector( ParameterVector& rhs );

	float get_intensity( const unsigned int j )const;
	float get_intensity( const unsigned int instanceNo,  const unsigned int beamletNo )const;
	void  scale_intensities(const double scale_factor);
	void  set_zero_intensities();
	void  set_unit_intensities();
    void add_noise_to_intensities(float scale_factor); 
	void  backup_intensities();
	void  restore_intensities();
	void  extract_beam( const unsigned int beamNo,  ParameterVector &pv ) const;
	float get_max_intensity()const;
	void  add_intensity_step( const ParameterVectorDirection& d,  const double step_size );
	double try_intensity_step( const ParameterVectorDirection& d,  const double step_size );
        void project_on_parameter_bounds();

	bool is_static_beam()const;
	bool is_dynamic_beam()const;

    /// Get geometry information
    inline Geometry const * get_Geometry() const {
        return &geometry_;
    }

	/*
	 * Functions about Voxels and geometry
	 */
	unsigned int get_nVoxels()const ;
	unsigned int get_nNonair_voxels()const;

	unsigned int get_nAuxiliaries()const;

	/*
	 * Calculating the dose
	 */

	// Calculating the nominal dose
	void calculate_dose( DoseVector & dose ) const;
	void calculate_dose( DoseVector & dose,  const ParameterVector&pv ) const;
	std::auto_ptr<DoseVector> calculate_dose() const;

	// Calculating special doses
	void calculate_beam_dose( DoseVector & dose,  const unsigned int beamNo ) const;
	void calculate_instance_dose( DoseVector & dose,  const unsigned int instanceNo ) const;
	void calculate_auxiliary_dose( DoseVector & dose,  const unsigned int auxiliaryNo ) const;
	void calculate_random_scenario_dose( DoseVector & dose) const;
	void calculate_random_scenario_dose( DoseVector & dose,  const RandomScenario & scenario ) const;

	// Write the nominal dose and dvh to a file
	void write_dose( string file_prefix,  bool CT_dose_flag=false );

	// Write other doses to file
	void write_beam_dose( string file_prefix );
	void write_auxiliary_dose( string file_prefix );
	void write_instance_dose( string file_prefix );
	void write_scenario_dose( string file_prefix );
	void write_expected_dose( string file_prefix,  int nScenarios );

	// Write the variance and variance volume history to a file if appropriate
	void write_variance( string file_prefix );

    /// calculate objective statistics
    void write_objective_samples( string file_prefix );

    /// set dose evaluation scenarios
    void set_dose_evaluation_scenarios( const TxtFileParser* txtFileParser ) const;

	/*
	 * Calculating the DVH
	 */

	// Calculating the nominal DVH
	Dvh get_empty_Dvh() const;
	void calculate_DVH( Dvh & dvh ) const;
	std::auto_ptr<Dvh> calculate_DVH() const;


	void write_DVH( string DVHfile );
	void append_DVH( string DVHfile );

    //
    // Dealing with objectives
    //

	unsigned int get_nPlain_objectives() const;
	unsigned int get_nMeta_objectives() const;
	unsigned int get_total_n_objectives() const;

	void print_objective_info( std::ostream& o ) const;
    double calculate_dose_objective( const DoseVector & the_dose,  vector<double> & multi_objective ) const;
    double calculate_dose_objective_and_gradient( const DoseVector & the_dose,  DoseVector & dose_gradient,  vector<double> & multi_objective ) const;
	double calculate_objective( DoseInfluenceMatrix & dij, const ParameterVector & beam_weights,  vector<double> & multi_objective,
                vector<double> & estimated_ssvo,  bool use_voxel_sampling, bool use_scenario_sampling,
		const vector<unsigned int>& instance_samples ) const;

	// double calculate_voi_objective(const unsigned int voiNo) const;
    double calculate_objective_and_gradient( DoseInfluenceMatrix & dij, 
										   const ParameterVector & pv,  
										   ParameterVectorDirection & gradient,  
										   vector<double> & multi_objective, 
										   vector<double> & estimated_ssvo, 
										   bool use_voxel_sampling,
										   bool use_scenario_sampling,
										   const vector<unsigned int> & instance_samples) const;
    double calculate_objective_and_gradient( DoseInfluenceMatrix & dij, 
										   const ParameterVector & pv,  
										   ParameterVectorDirection & gradient,  
										   vector<double> & multi_objective) const;


	double calculate_objective_and_gradient_and_Hv( vector<double> & multi_objective,  vector<double> & estimated_ssvo,
                ParameterVectorDirection & gradient,  const std::vector<float> & v,  std::vector<float> & Hv,
                bool sample_voxels,  bool sample_scenarios, const vector<unsigned int>& instance_samples ) const;
	double calculate_objective_and_gradient_and_Hv( const ParameterVector & beam_weights,
                vector<double> & multi_objective,  vector<double> & estimated_ssvo,  ParameterVectorDirection & gradient,
                const std::vector<float> & v,  std::vector<float> & Hv,  bool sample_voxels, bool sample_scenarios,
                const vector<unsigned int>& instance_samples ) const;

    /// Calculate objectives via a dose vector
    double calculate_objective_and_gradient_via_dose( DoseInfluenceMatrix & dij,
													  const BixelVector & beamweights,
													  BixelVectorDirection & gradient,
													  vector<double> & multi_objective) const; 
    double calculate_objective_via_dose( DoseInfluenceMatrix & dij,
										 const BixelVector & beamweights,
										 vector<double> & multi_objective) const; 

    /// Calculate estimate of true objective by taking many instance samples.
    double estimate_true_objective( DoseInfluenceMatrix & dij, const ParameterVector & beam_weights,  vector<double> & multi_objective,
                vector<double> & estimated_ssvo,  vector<double> & estimated_variance,
                bool use_voxel_sampling_for_objectives,  bool use_voxel_sampling_for_metaobjectives,
                int max_samples,  int min_samples,  float max_uncertainty ) const;

    /// Get the sampling fraction for each objective
	vector<float> get_objective_sampling_fractions() const;
	void set_objective_sampling_fractions(const vector<float> & sampling_fractions);
	vector<unsigned int> get_objective_nVoxels() const;
	vector<bool> get_objective_supports_sampling() const;

    //
    // Dealing with constraints
    //

    /// get number of constraints
    unsigned int get_nConstraints() const {return constraints_.size();};
    void print_constraint_info( std::ostream& o ) const;
    float get_upper_constraint_bound(unsigned int consNo) const;
    float get_lower_constraint_bound(unsigned int consNo) const;
    void set_upper_constraint_bound(unsigned int consNo, float value);
    void set_lower_constraint_bound(unsigned int consNo, float value);

    // ckeck if an optimization method is supported by all constraints
    bool supports_augmented_lagrangian() const;
    bool supports_projection_solver() const;
    bool supports_projection_optimizer(unsigned int consNo) const;
    bool supports_calculation_via_dose_vector() const;

    /// calculate constraint contributions to augmented lagrangian
    double calculate_dose_aug_lagrangian( const DoseVector & the_dose ) const;
    double calculate_dose_aug_lagrangian_and_gradient( const DoseVector & the_dose,  DoseVector & dose_gradient ) const;
    double calculate_aug_lagrangian_and_gradient(DoseInfluenceMatrix & dij, const BixelVector & bv, BixelVectorDirection & bvd) const;
    double calculate_aug_lagrangian(DoseInfluenceMatrix & dij, const BixelVector & bv) const;
    double calculate_merit_function( DoseInfluenceMatrix & dij, const ParameterVector & pv,double & lagrangian,  
									 double & objective, vector<double> & multi_objective,
									 double & constraint, vector<double> & multi_constraint) const;


    /// update lagrange multipliers and penalty factors
    void update_lagrange_multipliers(DoseInfluenceMatrix & dij, const ParameterVector & beam_weights);
    void update_penalty(DoseInfluenceMatrix & dij, const ParameterVector & beam_weights, float tol, float multiplier);
    void update_penalty(float tol, float multiplier);

    ///  write lagrange multipliers to a dose cube
    void write_lagrange_multipliers(string file_prefix);

    /// project onto constraints
    unsigned int project_onto_constraints();
    unsigned int project_onto_constraints(ParameterVector & pv);

    /// put all constraints back into the active set
    void reset_active_set();

    /// check if active sets in all constraints are empty
    bool is_active_set_empty() const;

    //
    // External Solver interface
    //

    bool supports_external_solver() const;
    unsigned int get_nGeneric_constraints() const;
    unsigned int get_nAuxiliary_variables() const;
    void get_generic_optimization_data(GenericOptimizationData & data);

	/**
	 * Plan options
	 */
	class options
	{
      public:
        options()
            : param_numbers_(),  param_values_(),  use_uniform_sampling_(false),  uniform_sampling_fraction_(1),
			  use_uniform_samples_per_obj_(false),  samples_per_obj_(1), static_beam_(false), read_beamlets_first_(false),
			  read_auxiliary_dij_(false), transpose_when_reading_Dij_(true),  dvh_bin_size_(0.1), nObjective_samples_(100),
			  min_beam_weight_(0) {};

		/// Numbers of optional paramaters passed on command line
		std::vector<int> param_numbers_;
		/// Values of optional paramaters passed on command line
		std::vector<std::string> param_values_;

		/// Whether to override the sampling rates from the plan file with uniform ones
		bool use_uniform_sampling_;

		/// Sampling rate to use if use_uniform_sampling_ is true
		float uniform_sampling_fraction_;

		/// Whether to override the sampling rates from the plan file with uniform samples per objective
		bool use_uniform_samples_per_obj_;

                /// Samples to use if use_uniform_samples_per_obj_ is true
		float samples_per_obj_;

		/// Whether or not the beam is static
		bool static_beam_;

		/// Whether or not bixel vector is initialized before Dij
		bool read_beamlets_first_;

	        /// whether to read multiple dij files as auxiliaries
	        bool read_auxiliary_dij_;

		/// transpose the Dij matrix when reading
		bool transpose_when_reading_Dij_;

		/// The size of bins in the Dose Volume Histogram in Gy
		float dvh_bin_size_;

		/// number of bins to generate histogram of objective values (for uncertainty applications)
	    unsigned int nObjective_samples_;

	    /// minimum beam weight for bixels
	    float min_beam_weight_;

	};

	/// Access the options
	const Plan::options & get_options() const;

    /// Allow the aperture sequencer class direct access to parameter_vector_ data
    friend class ApertureSequencer;

private:

	unsigned int read_voi_file(std::string voiFileName );
	unsigned int read_vv_file(std::string voiFileName );

        //void read_bwf_files( const std::string bwf_file_root );
        //void read_bwf_and_stf_files( const std::string bwf_file_root,  const std::string stf_file_root );

	void set_voi_parameters();
	void create_objectives();
	void create_meta_objectives();
	void create_constraints();

    /// create the DoseInfluenceMatrix and the ParameterVector
    void create_dij_and_parameters( string dij_file_root,  string bwf_file_root,  string stf_file_root );

    /// create the dose delivery model, the DoseInfluenceMatrix, the ParameterVector and the Geometry
    void create_dose_delivery_model( string dij_file_root,  string bwf_file_root,  string stf_file_root );

    /// create a standard dose delivery model without uncertainties
    void create_pdf_variation_model();
    /// model with a seperate fluence map per instance

	bool file_exists( std::string file_name );

    //
    // Private data members
    // ^^^^^^^^^^^^^^^^^^^^
    //

	/// The pln file parser
	std::auto_ptr<const TxtFileParser> txtFileParser_;

	/// Root file name for Konrad files
	std::string plan_file_root_;
	std::string plan_title_;

	/// A short description stored with the plan.
	std::string plan_description_;

	/// plan properties
	unsigned int nInstances_, nBeams_;
	unsigned int nNonair_voxels_;

    /// Holds information about the Volumes of Interest. Also used to calculate the EUD and other statistics.
	std::vector< Voi* > vois_;

    /// The number of VOIs.  May be larger than vois_.size() since somevois may be skipped.
    /// unsigned int nVois_;

	/// The bixel grid holds the beam weights for all beams.  It is indexed on the instance number and beam number.
	std::auto_ptr<ParameterVector> parameter_vector_;

	/// The backup bixel grids.
	std::auto_ptr<ParameterVector> backup_parameter_vector_;

	/// The dose influence matrix class is used to calculate the dose delivered to the plan.
	// std::vector< DoseInfluenceMatrix > dijs_;
	std::auto_ptr<DoseInfluenceMatrix> dij_;

	/// A scale factor to be applied to each instance
	std::vector< float > instance_scale_factor_;

        /// The objectives to be applied
	std::vector< Objective* > objectives_;

        /// The meta-objectives to be applied
	std::vector< MetaObjective* > meta_objectives_;

        /// The constraints to be applied
	std::vector< Constraint* > constraints_;

	/// Geometric information
	Geometry geometry_;

	/// Options
	Plan::options options_;

	/// The dose delivery model, describing motion and uncertainty
	std::auto_ptr<DoseDeliveryModel> dose_delivery_model_;
};


// -----------------------------------
// Inline Functions
// -----------------------------------

/**
 * Get Beamlet intensity from beamlet index
 *
 * @param j the beamlet index
 */
inline float Plan::get_intensity(const unsigned int j)const
{
    // TODO (dualta#2#): Need to restore this after get_intensity is restored to ParameterVector class.
    //return parameter_vector_->get_intensity(j);
    return 0;
}

inline const DoseInfluenceMatrix*  Plan::get_dose_influence_matrix()const
{
    return dij_.get();
}

inline DoseInfluenceMatrix*  Plan::get_dose_influence_matrix()
{
    return dij_.get();
}


inline bool Plan::is_static_beam()const {
    return options_.static_beam_;
}

inline bool Plan::is_dynamic_beam()const {
    return !is_static_beam();
}

inline unsigned int Plan::get_nParameters()const
{
    return parameter_vector_->get_nParameters();
}

inline unsigned int Plan::get_nBixels()const
{
    return parameter_vector_->get_nBixels();
}

inline unsigned int Plan::get_nAuxiliaries()const
{
    return dij_->get_nAuxiliaries();
}


inline unsigned int Plan::get_nPlain_objectives() const
{
    return objectives_.size();
}


inline unsigned int Plan::get_nMeta_objectives() const
{
    return meta_objectives_.size();
}


/**
 * Get total number of objectives, including those in meta-objectives
 */
inline unsigned int Plan::get_total_n_objectives() const
{
    unsigned int n = objectives_.size();
    for(unsigned int i = 0; i < meta_objectives_.size(); i++) {
        n += meta_objectives_[i]->get_nObjectives();
    }
    return n;
}

inline std::auto_ptr<ParameterVector> Plan::get_parameter_vector() const
{
    return parameter_vector_->get_copy();
}

inline ParameterVector* Plan::get_pointer_to_parameter_vector() 
{
    return parameter_vector_.get();
}

inline const Voi* Plan::get_voi( const unsigned int voiNo )const
{
    return vois_[voiNo];
}

inline Voi* Plan::get_voi( const unsigned int voiNo )
{
    return vois_[voiNo];
}


/**
 * Get a const reference to the options.
 */
inline const Plan::options & Plan::get_options() const
{
    return options_;
}


/**
 * Clear the intensities of all beamlets except the given beam in the supplied parameter_vector.
 *
 * @param beamNo The number of the beam to retain within the parameter_vector
 * @param pv    The ParameterVector to change.
 */
inline void Plan::extract_beam(const unsigned int beamNo, ParameterVector &pv) const
{
    unsigned int nParameters = pv.get_nParameters();
    for( unsigned int ip=0; ip<nParameters; ip++ ) {
        if( geometry_.get_beamNo(ip) != beamNo ) {
            // set parameter to not active
            pv.set_isActive( ip, false );
        }
    }
}


#endif
