/**
 * @file AbsoluteErrorObjective.hpp
 * AbsoluteErrorObjective Class header
 *
 */

#ifndef ABSOLUTEERROROBJECTIVE_HPP
#define ABSOLUTEERROROBJECTIVE_HPP

#include "Objective.hpp"
#include "Voi.hpp"
#include <cmath>

/**
 * Class AbsoluteErrorObjective implements an objective to maximize 
 * the absolute deviation from a min dose or a max dose
 * (alse called the Ramp Objective)
 *
 * Supports: external solver interface
 */
class AbsoluteErrorObjective : public Objective
{

public:

  /// Constructor
  AbsoluteErrorObjective(Voi*  the_voi,
			 unsigned int objNo,
			 float weight,
			 bool is_max_constraint,
			 float max_dose,
			 float weight_over,
			 bool  is_min_constraint = false,
			 float min_dose = 0,
			 float weight_under = 1);

  /// default destructor
  ~AbsoluteErrorObjective();

  /// calculate objective
  double calculate_objective(const BixelVector & beam_weights,
			     DoseInfluenceMatrix & Dij,
			     bool use_voxel_sampling,
			     double &estimated_ssvo);
  
  /// calculate objective from dose vector
  double calculate_dose_objective(const DoseVector & the_dose);

  /// Find out how many voxels are in this objective
  unsigned int get_nVoxels() const;
  
  /// Print a description of the objective
  void printOn(std::ostream& o) const;

  /// initialize the objective
  void initialize(DoseInfluenceMatrix& Dij);

  //
  // specific functions for commercial solver interface
  //

  bool supports_external_solver() const {return true;};
  
  /// returns total number of constraints
  unsigned int get_nGeneric_constraints() const;

  /// returns total number of auxiliary variables
  unsigned int get_nAuxiliary_variables() const;

  /// extract data for commercial solver
  void add_to_optimization_data_set(GenericOptimizationData & data, 
				    DoseInfluenceMatrix & Dij) const;




private:

  /// default constructor not allowed
  AbsoluteErrorObjective();

  /// the corresponding VOI
  Voi *the_voi_;

  /// True if this is a maximum dose constraint
  bool is_max_constraint_;
  
  /// Maximum dose in Gy
  float max_dose_;
  
  /// Penalty for being over the max dose (multiplied by the weight)
  float weight_over_;
  
  /// True if this is a minimum dose constraint
  bool is_min_constraint_;
  
  /// Minimum dose in Gy
  float min_dose_;
  
  /// Penalty for being under the min dose (multiplied by the weight)
  float weight_under_;
};

inline
unsigned int AbsoluteErrorObjective::get_nVoxels() const
{
  return(the_voi_->get_nVoxels());
}


#endif
