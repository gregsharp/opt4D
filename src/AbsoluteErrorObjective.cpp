/**
 * @file: AbsoluteErrorObjective.cpp
 * AbsoluteErrorObjective Class implementation.
 *
 */


#include "AbsoluteErrorObjective.hpp"

/**
 * Constructor
 */
AbsoluteErrorObjective::AbsoluteErrorObjective(Voi*  the_voi,
					       unsigned int objNo,
					       float weight,
					       bool  is_max_constraint,
					       float max_dose,
					       float weight_over,
					       bool  is_min_constraint,
					       float min_dose,
					       float weight_under)
  : Objective(objNo, weight),
    the_voi_(the_voi),
    is_max_constraint_(is_max_constraint),
    max_dose_(max_dose),
    weight_over_(weight_over),
    is_min_constraint_(is_min_constraint),
    min_dose_(min_dose),
    weight_under_(weight_under)
{
}

/**
 * Destructor
 */
AbsoluteErrorObjective::~AbsoluteErrorObjective()
{
}

/**
 * initialize
 */
void AbsoluteErrorObjective::initialize(DoseInfluenceMatrix& Dij)
{
    is_initialized_ = true;
}
  
/**
 * Prints a description of the objective on the given stream
 *
 * @param o The output stream to write to
 */
void AbsoluteErrorObjective::printOn(std::ostream& o) const
{
  if(is_max_constraint_) {
    o << "OBJ " << objNo_ << ": "; 
    o << "VOI " << the_voi_->get_voiNo() << "(" << the_voi_->get_name() << ")";
    o << "\tmax dose < " << max_dose_ << " (absolute error)" << endl;
  }
  if(is_min_constraint_) {
    o << "OBJ " << objNo_ << ": "; 
    o << "VOI " << the_voi_->get_voiNo() << "(" << the_voi_->get_name() << ")";
    o << "\tmin dose > " << min_dose_ << " (absolute error)" << endl;
  }
}

/**
 * calculate objective
 */
double AbsoluteErrorObjective::calculate_objective(
    const BixelVector & beam_weights,
    DoseInfluenceMatrix & Dij,
    bool use_voxel_sampling,
    double &estimated_ssvo)
{
  float dose;
  double obj = 0;
  for (unsigned int iVoxel=0; iVoxel<get_nVoxels(); iVoxel++) {
    unsigned int voxelNo = the_voi_->get_voxel(iVoxel);
    dose = Dij.calculate_voxel_dose(voxelNo, beam_weights);

    if(is_max_constraint_ && dose>max_dose_) {
      obj += weight_over_*(dose-max_dose_);
    }
    if(is_min_constraint_ && dose<min_dose_) {
      obj += weight_under_*(min_dose_-dose);
    }
  }
  obj /= get_nVoxels();
  return(weight_*obj);
}


/**
 * calculate objective from dose vector
 */
double AbsoluteErrorObjective::calculate_dose_objective(const DoseVector & the_dose)
{
  double obj = 0;
  for (unsigned int iVoxel=0; iVoxel<get_nVoxels(); iVoxel++) {
    unsigned int voxelNo = the_voi_->get_voxel(iVoxel);
    if(is_max_constraint_ && the_dose[voxelNo]>max_dose_) {
      obj += weight_over_*(the_dose[voxelNo]-max_dose_);
    }
    if(is_min_constraint_ && the_dose[voxelNo]<min_dose_) {
      obj += weight_under_*(min_dose_-the_dose[voxelNo]);
    }
  }
  obj /= get_nVoxels();
  return(weight_*obj);
}

/**
 * returns total number of constraints
 */
unsigned int AbsoluteErrorObjective::get_nGeneric_constraints() const
{
  unsigned int nConstraints = 0;
  if(is_max_constraint_) {
    nConstraints += get_nVoxels();
  }
  if(is_min_constraint_) {
    nConstraints += get_nVoxels();
  }
  return nConstraints;
}

/**
 * returns total number of auxiliary variables
 */
unsigned int AbsoluteErrorObjective::get_nAuxiliary_variables() const
{
  unsigned int nVar = 0;
  if(is_max_constraint_) {
    nVar += get_nVoxels();
  }
  if(is_min_constraint_) {
    nVar += get_nVoxels();
  }
  return nVar;
}


/**
 * prepare constraints for commercial solver interface
 */
void AbsoluteErrorObjective::add_to_optimization_data_set(GenericOptimizationData & data, 
							  DoseInfluenceMatrix & Dij) const
{
  cout << "Creating generic optimization data for OBJ " << get_objNo() << endl;

  // add constraints
  for (unsigned int iVoxel=0; iVoxel<get_nVoxels(); iVoxel++) {

    // Find the voxelNo of the voxel we are looking at
    unsigned int voxelNo = the_voi_->get_voxel(iVoxel);
 
    // get number of nonzero dij elements
    unsigned int nEntries = Dij.get_nEntries_per_voxel(voxelNo);

    if(is_max_constraint_) {

      // initialize next constraint
      data.constraints_[data.nConstraints_until_now_].initialize(nEntries+1);

      // add to number of nonzero coefficients
      data.nNonZeros_ += (nEntries+1);

      // index to the new auxiliary variable
      unsigned int iAux = data.nVarInt_ + data.nAuxiliaries_until_now_;

      // bounds for auxiliary variable
      data.upper_var_bound_[iAux] = OPT4D_INFINITY;
      data.lower_var_bound_[iAux] = 0;
      data.var_bound_type_[iAux] = LOWERBOUND;

      // objective function coefficient
      data.obj_coeffs_[iAux] += weight_*weight_over_; 

      // set constraints
      unsigned int iEntry = 0;
      for(DoseInfluenceMatrix::iterator iter = Dij.begin_voxel(voxelNo);
	  iter.get_voxelNo() == voxelNo; iter++) {
	data.constraints_[data.nConstraints_until_now_].index_[iEntry] = iter.get_bixelNo();
	data.constraints_[data.nConstraints_until_now_].value_[iEntry] = iter.get_influence();
	iEntry++;
      }

      // coefficient for auxiliary variable
      data.constraints_[data.nConstraints_until_now_].index_[iEntry] = iAux; 
      data.constraints_[data.nConstraints_until_now_].value_[iEntry] = -1.0;

      // bounds
      data.constraints_[data.nConstraints_until_now_].upper_bound_ = max_dose_;
      data.constraints_[data.nConstraints_until_now_].lower_bound_ = -OPT4D_INFINITY;
      data.constraints_[data.nConstraints_until_now_].type_ = UPPERBOUND;

      // increment counter
      data.nConstraints_until_now_ += 1;
      data.nAuxiliaries_until_now_ += 1;
    }

    if(is_min_constraint_) {

      // initialize next constraint
      data.constraints_[data.nConstraints_until_now_].initialize(nEntries+1);

      // add to number of nonzero coefficients
      data.nNonZeros_ += (nEntries+1);

      // index to the new auxiliary variable
      unsigned int iAux = data.nVarInt_ + data.nAuxiliaries_until_now_;

      // bounds for auxiliary variable
      data.upper_var_bound_[iAux] = OPT4D_INFINITY;
      data.lower_var_bound_[iAux] = 0;
      data.var_bound_type_[iAux] = LOWERBOUND;

      // objective function coefficient
      data.obj_coeffs_[iAux] += weight_*weight_under_; 

      // set constraints
      unsigned int iEntry = 0;
      for(DoseInfluenceMatrix::iterator iter = Dij.begin_voxel(voxelNo);
	  iter.get_voxelNo() == voxelNo; iter++) {
	data.constraints_[data.nConstraints_until_now_].index_[iEntry] = iter.get_bixelNo();
	data.constraints_[data.nConstraints_until_now_].value_[iEntry] = iter.get_influence();
	iEntry++;
      }

      // coefficient for auxiliary variable
      data.constraints_[data.nConstraints_until_now_].index_[iEntry] = iAux; 
      data.constraints_[data.nConstraints_until_now_].value_[iEntry] = 1.0;

      // bounds
      data.constraints_[data.nConstraints_until_now_].upper_bound_ = OPT4D_INFINITY;
      data.constraints_[data.nConstraints_until_now_].lower_bound_ = min_dose_;
      data.constraints_[data.nConstraints_until_now_].type_ = LOWERBOUND;

      // increment counter
      data.nConstraints_until_now_ += 1;
      data.nAuxiliaries_until_now_ += 1;
    }
  }
}
