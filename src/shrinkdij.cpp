/**
 * @file shrinkdij.cpp
 * The main file for shrinkdij.
 *
 * Uses importance sampling to shrink a dij file
 *
 * $Id: shrinkdij.cpp,v 1.6 2007/09/26 21:18:07 bmartin Exp $
 */

/**
 * \par Welcome to shrinkdij.
 *
 * shrinkdij is a utility to perform importance sampling on a dij file.  Type 
 * shrinkdij --help for usage information.
 */

#include <iostream>
using std::cout;
using std::cerr;
using std::endl;

#include <iomanip>
#include <exception>
#include <fstream>
#include <limits>
#include <tclap/CmdLine.h>
#include <string>
using std::string;

#include "DijFileParser.hpp"

void shrink_file(
        string in_file_name,
        string out_file_name,
        float threshold_fraction);

/**
 * Main function to interpret command line options and run program.
 */
int main(int argc, char* argv[])
{

    // Wrap everything in a try block to catch TCLAP exceptions.
    try {

        /*
         * Defining the command line syntax
         * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
         *
         * We define the command line object, and add various switches and 
         * value arguments to it.  The CmdLine object parses the argv array 
         * based on the Arg objects that it contains.
         */

        // Define the command line object and insert a message
        // that describes the program. This is printed last in the help text.  
        // The second argument is the delimiter (usually space) and the last 
        // one is the version number.   
        TCLAP::CmdLine cmd("shrinkdij dij file sampling tool",
                ' ', "0.1");

        /*
         * TCLAP syntax
         * ^^^^^^^^^^^^
         *          
         * TCLAP reads two kinds of command line parameters, value arguments 
         * and flags.  Value argments are used to pass data to the program 
         * (such as file names).  Flags are used to pass true or false 
         * information to the program.
         *
         * SwitchArg
         * ---------
         * Switches are used to tell the program something true or false, but 
         * must be false by default, so the default behavior comes from not 
         * using any flags at all.
         *
         * SwitchArg constructor:
         *
         * TCLAP::SwitchArg::SwitchArg  ( const std::string &   flag,
         *                                const std::string &   name,
         *                                const std::string &   desc,
         *                                CmdLineInterface &    parser)
         *
         * Parameters:
         *     flag   - The one character flag that identifies this argument
         *              on the command line.
         *     name   - A one word name for the argument. Can be used as a
         *              long flag on the command line.
         *     desc   - A description of what the argument is for or does.
         *     parser - A CmdLine parser object to add this Arg to
         *
         *
         * ValueArg
         * --------
         * Value arguments can read any type data into a variable that you 
         * create.  Most arguments use this format.
         *
         * ValueArg constructor:
         *
         * template<class T>
         * TCLAP::ValueArg< T >::ValueArg (const std::string &  flag,
         *                                 const std::string &  name,
         *                                 const std::string &  desc,
         *                                 bool                 req,
         *                                 T                    value
         *                                 const std::string &  typeDesc,
         *                                 CmdLineInterface &   parser)
         *
         * Parameters:
         *     flag     - The one character flag that identifies this argument
         *                on the command line.
         *     name     - A one word name for the argument. Can be used as a
         *                long flag on the command line.
         *     desc     - A description of what the argument is for or does.
         *     req      - Whether the argument is required on the command line.
         *     value    - The default value assigned to this argument if it is
         *                not present on the command line.
         *     typeDesc - A short, human readable description of the type that
         *                this object expects. This is used in the generation 
         *                of the USAGE statement. The goal is to be helpful to 
         *                the end user of the program.
         *      parser  - A CmdLine parser object to add this Arg to
         *
         * UnlabledValueArg
         * ----------------
         *
         * The plan file name is read by an UnlabledValueArg, so any argument 
         * that is not processed by something else will be assumed to be the 
         * plan file name.  Don't add any other UnlabledValueArg arguments!!!
         *
         * UnlabledValueArg constructor:
         * 
         * template<class T>
         * TCLAP::ValueArg< T >::UnlabledValueArg (
         *                                 const std::string &  name,
         *                                 const std::string &  desc,
         *                                 bool                 req,
         *                                 T                    value
         *                                 const std::string &  typeDesc,
         *                                 CmdLineInterface &   parser)
         *
         *
         * MultiArg
         * --------
         *
         * If several values may be passed in for one argument, use a MultiArg, 
         * which returns a vector of values instead of a single one.
         *
         * MultiArg syntax:
         *
         * template<class T>
         * TCLAP::MultiArg< T >::MultiArg (const std::string &  flag,
         *                                 const std::string &  name,
         *                                 const std::string &  desc,
         *                                 bool                 req,
         *                                 const std::string &  typeDesc,
         *                                 CmdLineInterface &   parser)
         *
         * UnlabeldMultiArg
         * ----------------
         *
         *  Syntax:
         *
         *  template<class T>
         *  TCLAP::UnlabledMultiArg< T >::UnlabeledMultiArg (
         *              const std::string & name,
         *              const std::string & desc,
         *              bool                req,
         *              const std::string & typeDesc,
         *              CmdLineInterface &  parser,
         *              bool                ignoreable=false)
 	 *
         */

        /*
         * Add General options to cmd
         */

        // Input file prefix in case of processing a group of files
        TCLAP::ValueArg<std::string> in_file_prefix_arg("i",
                "infile_prefix","Input file prefix",
                false, "","dij file prefix",cmd);

        // Output file prefix in case of processing a group of files
        TCLAP::ValueArg<std::string> out_file_prefix_arg("o",
                "outfile_prefix","Output file prefix (defaults to shrunk)",
                false, "", "dij file prefix",cmd);

        // Random seed
        TCLAP::ValueArg<int> random_seed_arg(
                "r", "random_seed","Random seed to use",false, 0,"integer",cmd);

        TCLAP::ValueArg<float> threshold_fraction_arg("t",
                "threshold",
                "Fraction of the largest entry to set threshold at (default 0)",
                true,0,"float in [0,1]",cmd);

        TCLAP::UnlabeledMultiArg<std::string> in_file_names_arg(
                "infile","Input file names",false,"dij file names",cmd);

        // Parse the command line
        cmd.parse(argc, argv);

        string out_file_prefix = out_file_prefix_arg.getValue();
        float threshold_fraction = threshold_fraction_arg.getValue();


        // Initialize random number generator
        if(random_seed_arg.isSet()) {
            // Specific random seed passed in
            srand(random_seed_arg.getValue());
        } else {
            // No seed passed in, so make it random based on the time
            srand(time(0));
        }

        if(in_file_prefix_arg.isSet()) {
            // In file prefix is supplied, so convert any files that can be 
            // found

            string in_file_prefix = in_file_prefix_arg.getValue();

            // Find out file prefix
            string out_file_prefix;
            if(out_file_prefix_arg.isSet()) {
                out_file_prefix = out_file_prefix_arg.getValue();
            } else {
                out_file_prefix = "shrunk_" + in_file_prefix;
            }

            unsigned int instanceNo = 0;
            unsigned int beamNo = 0;
            while(true) {

                // Find file names
                string in_file_name;
                string out_file_name;
                char cbeam[10];
                sprintf(cbeam,"%d",beamNo + 1);
                if(instanceNo == 0) {
                    in_file_name = in_file_prefix
                        + "_" + string(cbeam) + ".dij";
                    out_file_name = out_file_prefix
                        + "_" + string(cbeam) + ".dij";
                } else {
                    char cinstance[10];
                    sprintf(cinstance,"%d",instanceNo);
                    in_file_name = in_file_prefix
                        + "_" + string(cinstance) 
                        + "_" + string(cbeam) + ".dij";
                    out_file_name = out_file_prefix
                        + "_" + string(cinstance) 
                        + "_" + string(cbeam) + ".dij";
                }

                try {
                    shrink_file( in_file_name, out_file_name,
                            threshold_fraction);
		    beamNo ++;
                }
                catch (DijFileParser::exception & e) {
                    if(beamNo == 0) {
                        // We're done since we're at an instance with no beams
                        break;
                    } else {
                        // We found at least one beam before failing, so go to 
                        // next instance
                        beamNo = 0;
                        instanceNo++;
                    }
                }
            }
        }

        if(in_file_names_arg.getValue().size() > 0) {
            // Input files are supplied, so convert them

            // Find out file prefix
            string out_file_prefix;
            if(out_file_prefix_arg.isSet()) {
                out_file_prefix = out_file_prefix_arg.getValue();
            } else {
                out_file_prefix = "shrunk_";
            }

            vector<string>::const_iterator iter;
            for(iter = in_file_names_arg.getValue().begin();
                    iter != in_file_names_arg.getValue().end();
                    iter++) {
                try {
                    shrink_file( *iter, out_file_prefix + *iter,
                            threshold_fraction);
                }
                catch (DijFileParser::exception & e) {
                    cerr << "Error while shrinking file: " << *iter << "\n";

                }
            }
        }

    }
    catch (TCLAP::ArgException &e) { // catch any exceptions
        std::cerr << "error: " << e.error() << " for arg " << e.argId()
            << std::endl;
    }
    catch (std::exception & e) { // Catch other exceptions
        std::cerr << "Error caught in shrinkdij.cpp, main(): " << e.what()
                                                              << std::endl;
    }

    return(0);
}


/**
 * Shrink a dij file with the given name and threshold.  Throws an exception if 
 * the input file does not exist or the output cannot be written to.
 *
 * @param in_file_name The file to read
 * @param out_file_name The file to write
 * @param threshold_fraction The fraction of the largest value to use as a 
 * threshold
 */
void shrink_file(
        string in_file_name,
        string out_file_name,
        float threshold_fraction)
{
    short max_value = 0;

    int num_beamlets = 0;
    int nInput_elements = 0;
    int nOutput_elements = 0;

    DijFileParser::header head;
    DijFileParser::bixel bixel;
    DijFileParser::bixel scratch_bixel;

    // find max_value in file
    {
        DijFileParser in_file(in_file_name);

        head = in_file.get_header();

        num_beamlets = head.npb_;

        for(int iBixel = 0; iBixel < head.npb_; iBixel++) {
            in_file.read_next_bixel(bixel);
            nInput_elements += bixel.nVox_;
            for(vector<short>::const_iterator iter =
                    bixel.value_.begin();
                    iter != bixel.value_.end(); iter++) {
                if(*iter > max_value) {
                    max_value = *iter;
                }
            }
        }
        // File closes as in_file goes out of scope here
    }

    // Find threshold
    short threshold = short(float(max_value) * threshold_fraction);

    //
    // Write out_file
    //

    // Open infile for reading
    DijFileParser in_file(in_file_name);

    head = in_file.get_header();

    // Open outfile for writing
    DijFileParser out_file(out_file_name, head);

    // Process each bixel
    for(int iBixel = 0; iBixel < head.npb_; iBixel++) {

        // Read bixel
        in_file.read_next_bixel(bixel);

        // Do sampling
        for(vector<short>::iterator iter =
                bixel.value_.begin();
                iter != bixel.value_.end(); iter++) {
            if(*iter < threshold) {
                if((float(*iter) / float(threshold)) >
                        float(std::rand()) / float(RAND_MAX)) {
                    // Include entry
                    *iter = threshold;
                } else {
                    // Exclude entry
                    *iter = 0;
                }
            }
        }

        // Eliminate zeros
        scratch_bixel = bixel;
        scratch_bixel.voxelNo_.resize(0);
        scratch_bixel.value_.resize(0);

        vector<int>::const_iterator voxelNo_iter =
            bixel.voxelNo_.begin();
        vector<short>::const_iterator value_iter =
            bixel.value_.begin();
        while(value_iter != bixel.value_.end()) {
            if(*value_iter != 0) {
                // Include entry
                scratch_bixel.voxelNo_.push_back(*voxelNo_iter);
                scratch_bixel.value_.push_back(*value_iter);
            }
            voxelNo_iter++;
            value_iter++;
        }
        scratch_bixel.nVox_ = int(scratch_bixel.voxelNo_.size());
        nOutput_elements += scratch_bixel.nVox_;

        // Write out bixel
        out_file.write_next_bixel(scratch_bixel);
    }

    // Completed successfully, so report success
    cout << "Read " << in_file_name << " with "
        << num_beamlets << " beamlets and "
        << nInput_elements << " entries"
        << " max entry = " << max_value << "\n";

    cout << "Wrote " << out_file_name << " with "
        << num_beamlets << " beamlets and "
        << nOutput_elements << " entries"
        << " threshold = " << threshold << "\n";

}
  
