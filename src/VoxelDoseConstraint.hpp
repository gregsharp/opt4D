/**
 * @file DoseConstraint.hpp
 * VoxelDoseConstraint Class header
 *
 */

#ifndef VOXELDOSECONSTRAINT_HPP
#define VOXELDOSECONSTRAINT_HPP

class VoxelDoseConstraint;
class UniformDoseConstraint;
class VaryingDoseConstraint;

#include "Constraint.hpp"
#include "Voi.hpp"
#include <cmath>




/**
 * Class VoxelDoseConstraint implements a hard maximum or minimum dose constraint for all voxels in an ROI.
 */
class VoxelDoseConstraint : public Constraint
{

public:
  
  // Constructor
  VoxelDoseConstraint(Voi*  the_voi,
		 unsigned int consNo,
		 bool is_max_constraint,
		 bool is_min_constraint,
		 float initial_penalty);
  
  // destructor
  ~VoxelDoseConstraint();
  
  
  /// Find out how many voxels are in this constraint
  unsigned int get_nVoxels() const;
  
  /// get corresponding VOI
  size_t get_voiNo() const;
  
  /// Print a description of the constraint
  virtual void printOn(std::ostream& o) const = 0;

  /// set upper bound of a constraint
  virtual void set_upper_bound() = 0;

  /// set lower bound of a constraint
  virtual void set_lower_bound() = 0;

  /// initialize the Constraint
  void initialize(DoseInfluenceMatrix& Dij);

  /// if voxel is in active set
  bool is_active(unsigned int iVoxel) const;

  
  //
  // specific functions for augmented lagrangian approach
  //
  
  bool supports_augmented_lagrangian() const {return(true);};

  /// calculate the augmented lagrangian function
  double calculate_aug_lagrangian(
                const BixelVector & beam_weights,
                DoseInfluenceMatrix & Dij,
				double & constraint,
				double & merit);

  /// calculate the augmented lagrangian function and its gradient
  double calculate_aug_lagrangian_and_gradient(
                const BixelVector & beam_weights,
                DoseInfluenceMatrix & Dij,
                BixelVectorDirection & gradient,
                float gradient_multiplier);

  /// update lagrange parameter
  void update_lagrange_multipliers(const BixelVector & beam_weights,
				   DoseInfluenceMatrix & Dij);

  /// get lagrange multipliers as pairs <voxelNo,value>
  vector<pair<unsigned int,float> > get_lagrange_multipliers();

  /// update penalty parameter
  void update_penalty(const BixelVector & beam_weights,
					  DoseInfluenceMatrix & Dij, 
					  float tol, 
					  float multiplier);

  //
  // specific functions for projection method
  //
  
  bool supports_projection_solver() const {return(true);};

  /// sequentially project onto (beyond) the constraints
  unsigned int project_onto(BixelVector & beam_weights,
			    DoseInfluenceMatrix & Dij);

  /// puts all voxels into the active set
  void reset_active_set();

  /// ckeck if active set is empty
  bool is_active_set_empty() const;


  //
  // specific functions for commercial solver interface
  //
  
  bool supports_external_solver() const {return(true);};

  /// return number of geeric constraints
  unsigned int get_nGeneric_constraints() const;

  /// generate generic constraints for external solver
  void add_to_optimization_data_set(GenericOptimizationData & data, 
				    DoseInfluenceMatrix & Dij) const;


protected:

  /// the corresponding VOI
  Voi *the_voi_;

  /// is max dose constraint
  bool is_max_constraint_;

  /// is min dose constraint
  bool is_min_constraint_;

  /// maximum tolerance dose
  vector<float> max_dose_;

  /// minimum tolerance dose
  vector<float> min_dose_;


private:

  // Default constructor not allowed
  VoxelDoseConstraint();

  /// holds for every voxel the norm of the vector containing its dose contributions
  vector<float> dose_contribution_norm_;

  /// penalty factor
  vector<float> penalty_;
  
  /// initial penalty factor
  float initial_penalty_;

  /// lagrange multiplier
  vector<float> lagrange_;

  /// defines the active set of constraints
  vector<bool> in_active_set_;

};

/*
 * Inline functions
 */


inline
size_t VoxelDoseConstraint::get_voiNo() const
{
  return the_voi_->get_voiNo();
}

inline
unsigned int VoxelDoseConstraint::get_nVoxels() const
{
  return the_voi_->get_nVoxels();
}

inline
bool VoxelDoseConstraint::is_active(unsigned int iVoxel) const
{
  return in_active_set_[iVoxel];
}

inline
void VoxelDoseConstraint::reset_active_set()
{
  fill(in_active_set_.begin(),in_active_set_.end(),true);
}

inline
bool VoxelDoseConstraint::is_active_set_empty() const
{
  for(unsigned int i=0; i<in_active_set_.size(); i++) {
    if(in_active_set_[i]) {
      return false;
    }
  }
  return true;
}


class UniformDoseConstraint : public VoxelDoseConstraint
{

public:

  /// Constructor
  UniformDoseConstraint(Voi*  the_voi,
						unsigned int consNo,
						bool is_max_constraint,
						bool is_min_constraint,
						float max_dose,
						float min_dose,
						float initial_penalty);

  /// destructor
  ~UniformDoseConstraint();

  /// Print a description of the constraint
  void printOn(std::ostream& o) const;

  /// set upper bound of a constraint
  void set_upper_bound();

  /// set lower bound of a constraint
  void set_lower_bound();

private:

  float uniform_max_dose_;
  float uniform_min_dose_;
};


class VaryingDoseConstraint : public VoxelDoseConstraint
{

public:

  /// Constructor
  VaryingDoseConstraint(Voi*  the_voi,
						unsigned int consNo,
						bool is_max_constraint,
						bool is_min_constraint,
						unsigned int nTotalVoxels,
						string max_dose_file_name,
						string min_dose_file_name,
						float initial_penalty);

  /// destructor
  ~VaryingDoseConstraint();

  /// Print a description of the constraint
  void printOn(std::ostream& o) const;

  /// set upper bound of a constraint
  void set_upper_bound();

  /// set lower bound of a constraint
  void set_lower_bound();

private:

  string max_dose_file_name_;
  string min_dose_file_name_;

  DoseVector varying_max_dose_;
  DoseVector varying_min_dose_;

};

#endif
