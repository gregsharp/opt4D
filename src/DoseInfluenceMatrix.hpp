/**
 * @file DoseInfluenceMatrix.hpp
 * DoseInfluenceMatrix class header
 *
 * $Id: DoseInfluenceMatrix.hpp,v 1.33 2008/05/13 19:19:52 unkelbac Exp $
 */

#ifndef DOSEINFLENCEMATRIX_HPP
#define DOSEINFLENCEMATRIX_HPP

class DoseInfluenceMatrix;

#include <vector>
#include <string>
#include <algorithm>

#include "DoseVector.hpp"
#include "BixelVector.hpp"
#include "Geometry.hpp"



/**
 * Class DoseInfluenceMatrix holds pre-calculated dose matrix and calculates 
 * dose from a given set of bixel intensities.
 *
 * To access the entries of the DoseInfluenceMatrix, it is neccessary to use an 
 * iterator.
 *
 * The class can store auxiliary Dij matrices (i.e. additional influence values 
 * for the same voxel-bixel pairs) in the member variable auxiliary_influence_
 * When accessing the influence values from outside the class via get_influence(auxiliaryNo)
 * and set_influence(auxiliaryNo), the index auxiliaryNo=0 points to the original Dij and 
 * auxiliaryNo=1 points to the first auxiliary.
 */
class DoseInfluenceMatrix
{

  public:
    // Default Copy constructor is sufficient

    // Default Destructor is declared virtual
    virtual ~DoseInfluenceMatrix();

    class iterator;    
    class node;

    /// Calculates dose and stores it in the voxel grid
    void dose_forward (
        DoseVector &dose_vector,        // output: dose vector
        const BixelVector &bixel_grid,   // input: intensity vector
	unsigned int auxiliaryNo=0
        )const ;

    /// Calculates dose to one voxel
    float calculate_voxel_dose (
        unsigned int voxelNo,
        const BixelVector &bixel_grid,         // input: intensity vector
	unsigned int auxiliaryNo=0
        );

    /// Calculates dose to one voxel for only the included bixelNos
    float calculate_voxel_dose (
        unsigned int voxelNo,
        const BixelVector &bixel_grid,         // input: intensity vector
	unsigned int auxiliaryNo,
        const vector<unsigned int> &bixelNos
        );

    /// Calculates dose to one voxel for only the included bixelNos with
    /// associated auxiliaries
    float calculate_voxel_dose (
            unsigned int voxelNo,
            const BixelVector &bixel_grid,
            const vector<unsigned int> &bixelNos,
            const vector<unsigned int> &bixel_instanceNos);

    /// Calculates the derivative of the dose in a voxel with respect to the bixel vector
    void calculate_voxel_dose_gradient (
                unsigned int voxelNo,
                BixelVectorDirection &gradient,
                float gradient_multiplier,
                unsigned int auxiliaryNo=0
                );

    /// Calculates the derivative of the dose in a voxel with respect to the
    /// bixel vector using only the specified bixels
    void calculate_voxel_dose_gradient (
        unsigned int voxelNo,
	BixelVectorDirection &gradient,
        float gradient_multiplier,
	unsigned int auxiliaryNo,
        const vector<unsigned int> &bixelNos
        );

    /// Calculates the derivative of the dose in a voxel with respect to the
    /// bixel vector using only the specified bixels with suplied auxiliary
    /// number per bixel
    void calculate_voxel_dose_gradient (
        unsigned int voxelNo,
	BixelVectorDirection &gradient,
        float gradient_multiplier,
        const vector<unsigned int> &bixelNos,
        const vector<unsigned int> &bixel_auxiliaryNos
        );

    /// Calculates dose to one voxel delivered by the one beam direction.
    float calculate_voxel_dose_per_beam (
        unsigned int voxelNo,
        const BixelVector &bixel_grid,         // input: intensity vector
	unsigned int beamNo,
	const Geometry &geometry,
	unsigned int auxiliaryNo=0        
	);

    /// copy the dose distribution of a single beamlet to a dose vector 
    void copy_pencil_beam_to_dose_vector(unsigned int bixelNo, 
					 DoseVector &dose_vector,
					 unsigned int auxiliaryNo=0);

    DoseInfluenceMatrix::iterator begin()const;
    DoseInfluenceMatrix::iterator begin_voxel(unsigned int voxelNo);
    DoseInfluenceMatrix::iterator begin_bixel(unsigned int bixelNo);
    
    unsigned int get_nVoxels()const;
    unsigned int get_nBixels()const;
    unsigned int get_nEntries()const;
    unsigned int get_nAuxiliaries()const;
    unsigned int get_nEntries_per_voxel(unsigned int voxelNo);

    void transpose();
    void ensure_transposed();
    void ensure_not_transposed();
    bool is_transposed()const;

    bool is_sorted()const;
    void ensure_sorted();
    bool confirm_sorted();
    void sort_Dij();

    DoseInfluenceMatrix operator+(const DoseInfluenceMatrix& rhs) const;
    DoseInfluenceMatrix& operator*=(const float scale_factor);

    // Functions used for importance sampling

    // Get the maximum entry intensity
    float get_max_influence() const;

    // Do importance sampling
    void importance_sample(float threshold);

  protected:
    // Protected default constructor
    DoseInfluenceMatrix(bool is_transposed=false);

    // Class used to adjust influence of Dij entries
    class non_const_iterator;

    DoseInfluenceMatrix::non_const_iterator non_const_begin();
    DoseInfluenceMatrix::non_const_iterator non_const_begin_voxel(unsigned int voxelNo);
    DoseInfluenceMatrix::non_const_iterator non_const_begin_bixel(unsigned int bixelNo);

    void set_nBixels(unsigned int nBixels);
    void set_nVoxels(unsigned int nVoxels);
    void set_nAuxiliaries(unsigned int nAuxiliaries);

    void reserve_Dij(unsigned int nDij);

    void set_nEntries_per_bixel(const vector<unsigned int> &nEntries);
    void set_nEntries_per_voxel(const vector<unsigned int> &nEntries);
    const vector<unsigned int> & get_nEntries_per_bixel() const;

    /// add an bixel with no entries, add blank space for nElements
    void add_empty_bixel(unsigned int nElements);

    /// allocate memory for auxiliary influence vector
    void create_auxiliary_Dij();

    /// read auxiliary Dij matrices
    void read_auxiliary_Dijs(std::string adij_file_name, int file_id_number);

    /// write auxiliary Dij matrices
    void write_auxiliary_Dijs(std::string adij_file_name, int file_id_number);

    /// write nominal Dij
    virtual void write_dij_files(string dij_file_root);

    /// set one Dij element
    void add_Dij_element(unsigned int bixelNo, unsigned int voxelNo, float dose);
    void add_Dij_element(unsigned int bixelNo, unsigned int voxelNo, 
			 float dose, vector<float> auxiliary_dose);



    /// add blank space to the matrix
    void add_blank_space(const vector<unsigned int> &nEntries);

    /// add a vector of new Dij elements to a bixel
    void add_Dij_elements_to_bixel(const unsigned int bixelNo, vector<unsigned int> &voxelNos, vector<float> &influence);

    /// add a vector of new Dij elements to a voxel
    void add_Dij_elements_to_voxel(const unsigned int voxelNo, vector<unsigned int> &bixelNos, vector<float> &influence);

    /// If entries are sorted
    bool is_sorted_;

  private:
    /// remove all blank space from the matrix
    void remove_blank_space();

    /// remove entries with zero intensity but leave blank space
    void remove_zero_entries();

    /// Total number of bixels in beams
    unsigned int nBixels_;

    /// Total number of voxels in dose grid
    unsigned int nVoxels_;

    /// Total number of entries in Dij matrix (including blank space)
    unsigned int nEntries_;

    /// Total number of auxiliary Dij matrices (the original Dij does not count)
    unsigned int nAuxiliaries_;

    /// If the matrix is transposed
    bool is_transposed_;

    /// Array holding influence
    vector<float> influence_;

    /// additional influence values for motion/uncertainty applications
    vector< vector<float> > auxiliary_influence_;

    /// Array holding voxelNo if not transposed or bixelNo if transposed
    vector<unsigned int> voxelNo_or_bixelNo_;

    /// Array holding first entry in each bixel if not transposed.  The last 
    /// entry is the nEntries_ and the size is nBixels_+1.
    vector<unsigned int> bixel_start_index_;
    vector<unsigned int> bixel_nEntries_;

    /// Array holding first entry in each voxel if transposed. The last
    /// entry is the nEntries_ and the size is nVoxels_+1.
    vector<unsigned int> voxel_start_index_;
    vector<unsigned int> voxel_nEntries_;

    /// Array containing the norm of the vectors of dose contributions for every voxel
    vector<float> norm_;

    friend class DoseInfluenceMatrix::iterator;
    friend class DoseInfluenceMatrix::non_const_iterator;
    friend class AuxiliaryRangeModel;
};


/**
 * Class DoseInfluenceMatrix::iterator
 *
 * Used to Iterate over the entries in the DoseInfluenceMatrix
 * 
 */
class DoseInfluenceMatrix::iterator
{

  public:
    /// Initialize to the start of the DoseInfluenceMatrix
    iterator(const DoseInfluenceMatrix& rhs);

    /// Initialize at either a row or a column of the DoseInfluenceMatrix.
    /// Initialized to the end of the matrix if there are no entries in the
    /// row.
    iterator(const DoseInfluenceMatrix& the_DoseInfluenceMatrix,
             const unsigned int bixelNo_or_voxelNo);

    // Operator overloading
    DoseInfluenceMatrix::iterator& operator++ ();
    void operator++ (int);

    unsigned int get_bixelNo()const;
    unsigned int get_voxelNo()const;
    float get_influence()const;
    float get_influence(unsigned int auxiliaryNo)const;

    bool not_at_end();
    bool at_end();

  protected:
    /// The DoseInfluenceMatrix being iterated over
    const DoseInfluenceMatrix *the_DoseInfluenceMatrix_;

    /// The pointed at voxelNo_
    unsigned int voxelNo_;

    /// The pointed at bixelNo_
    unsigned int bixelNo_;
    
    /// The entry number
    unsigned int entryNo_;

  private:
    /// Default constructor is private to prevent use by other classes
    iterator();

};

/**
 * Class DoseInfluenceMatrix::non_const_iterator
 *
 * Used to Iterate over and change the entries in the DoseInfluenceMatrix.
 * Only available to classes derived from DoseInfluenceMatrix.
 * Identical to DoseInfluenceMatrix::iterator except for the const-ness of the 
 * dose influence matrix and the availability of set_influence()
 */
class DoseInfluenceMatrix::non_const_iterator
{

  public:
    /// Initialize to the start of the DoseInfluenceMatrix
    non_const_iterator(DoseInfluenceMatrix& rhs);

    /// Initialize at either a row or a column of the DoseInfluenceMatrix.
    /// Initialized to the end of the matrix if there are no entries in the
    /// row.
    non_const_iterator(DoseInfluenceMatrix& the_DoseInfluenceMatrix,
		       const unsigned int bixelNo_or_voxelNo);

    // Operator overloading
    DoseInfluenceMatrix::non_const_iterator& operator++ ();
    void operator++ (int);

    unsigned int get_bixelNo()const;
    unsigned int get_voxelNo()const;
    float get_influence()const;
    float get_influence(unsigned int auxiliaryNo)const;

    bool not_at_end();
    bool at_end();

    /// Set the influence to the current entry
    //void set_influence(float influence);
    void set_influence(float influence, unsigned int auxiliaryNo=0);

  protected:
    /// The DoseInfluenceMatrix being iterated over
    DoseInfluenceMatrix *the_DoseInfluenceMatrix_;

    /// The pointed at voxelNo_
    unsigned int voxelNo_;

    /// The pointed at bixelNo_
    unsigned int bixelNo_;
    
    /// The entry number
    unsigned int entryNo_;

  private:
    /// Default constructor is private to prevent use by other classes
    non_const_iterator();

};

/**
 * Class DoseInfluenceMatrix::node
 *
 * Used to hold voxel/bixel index plus influence and auxiliary influence values
 */
class DoseInfluenceMatrix::node
{
  public:

    // constructor
    node(unsigned int index, float influence, vector<float> auxiliary_influence)
	: index_(index)
	, influence_(influence)
	, auxiliary_influence_(auxiliary_influence.size())
	{
	    auxiliary_influence_ = auxiliary_influence;
	};

    node()
	: index_(0)
	, influence_(0)
	, auxiliary_influence_()
	{
	};

    // operator overloding
    inline bool operator<(const DoseInfluenceMatrix::node rhs) const {
	return (index_ < rhs.index_); };

    // attributes
    unsigned int index_;
    float influence_;
    vector<float> auxiliary_influence_;
};



// -------------------------------
// Inline functions
// -------------------------------

inline
bool DoseInfluenceMatrix::is_transposed()const
{
  return is_transposed_;
}

inline
bool DoseInfluenceMatrix::is_sorted()const
{
  return is_sorted_;
}

inline
void DoseInfluenceMatrix::ensure_transposed()
{
  if(!is_transposed_)
    transpose();
}

inline
void DoseInfluenceMatrix::ensure_not_transposed()
{
  if(is_transposed_)
    transpose();
}


inline
void DoseInfluenceMatrix::ensure_sorted()
{
    if(!is_sorted_) {
	sort_Dij();
    }
}

inline
unsigned int DoseInfluenceMatrix::get_nBixels()const
{
  return nBixels_;
}

inline
unsigned int DoseInfluenceMatrix::get_nVoxels()const
{
  return nVoxels_;
}


inline
unsigned int DoseInfluenceMatrix::get_nEntries()const
{
  return nEntries_;
}

inline
unsigned int DoseInfluenceMatrix::get_nAuxiliaries()const
{
  return nAuxiliaries_;
}


/**
 * Calculates dose to one voxel.  This will transpose the matrix if neccessary, 
 * so it cannot be done to a const DoseInfluenceMatrix.
 */
inline
float DoseInfluenceMatrix::calculate_voxel_dose (
        unsigned int voxelNo,
        const BixelVector &bixel_grid,
	unsigned int auxiliaryNo)
{
  float dose_buf = 0;

  if (auxiliaryNo==0) {
      for(DoseInfluenceMatrix::iterator iter = begin_voxel(voxelNo);
	  iter.get_voxelNo() == voxelNo; iter++) {
	  dose_buf += bixel_grid[iter.get_bixelNo()] * iter.get_influence();
      }
  }
  else {
      for(DoseInfluenceMatrix::iterator iter = begin_voxel(voxelNo);
	  iter.get_voxelNo() == voxelNo; iter++) {
	  dose_buf += bixel_grid[iter.get_bixelNo()] * iter.get_influence(auxiliaryNo);
      }
  }

  return dose_buf;
}


/**
 * Calculates dose to one voxel.  This will transpose the matrix if neccessary, 
 * so it cannot be done to a const DoseInfluenceMatrix.  Requires the dose 
 * influence matrix entries to be sorted.
 *
 * Only uses the bixels listed in bixelNos.
 */
inline
float DoseInfluenceMatrix::calculate_voxel_dose (
        unsigned int voxelNo,
        const BixelVector &bixel_grid,
	unsigned int auxiliaryNo,
        const vector<unsigned int> &bixelNos)
{
    ensure_sorted();

    float dose_buf = 0;

    DoseInfluenceMatrix::iterator dij_iter = begin_voxel(voxelNo);
    vector<unsigned int>::const_iterator bixel_iter = bixelNos.begin();

    if (auxiliaryNo==0) {
        while(dij_iter.get_voxelNo() == voxelNo) {
            // Search through bixelNos to first equal to or reater then the 
            // current dij entry bixel number
            while(bixel_iter != bixelNos.end()
                    && (*bixel_iter < dij_iter.get_bixelNo())) {
                ++bixel_iter;
            }

            // Break if we're out of included bixeNos
            if(bixel_iter == bixelNos.end()) {
                break;
            }

            // Add to dose if current bixel is in list
            if(*bixel_iter == dij_iter.get_bixelNo()) {
                dose_buf += bixel_grid[dij_iter.get_bixelNo()]
                    * dij_iter.get_influence();
            }
            dij_iter++;
        }
    }
    else {
        while(dij_iter.get_voxelNo() == voxelNo) {
            // Search through bixelNos to first equal to or reater then the 
            // current dij entry bixel number
            while(bixel_iter != bixelNos.end()
                    && (*bixel_iter < dij_iter.get_bixelNo())) {
                ++bixel_iter;
            }

            // Break if we're out of included bixeNos
            if(bixel_iter == bixelNos.end()) {
                break;
            }

            // Add to dose if current bixel is in list
            if(*bixel_iter == dij_iter.get_bixelNo()) {
                dose_buf += bixel_grid[dij_iter.get_bixelNo()]
                    * dij_iter.get_influence(auxiliaryNo);
            }
            dij_iter++;
        }
    }
    return dose_buf;
}


/**
 * Calculates dose to one voxel.  This will transpose the matrix if neccessary, 
 * so it cannot be done to a const DoseInfluenceMatrix.  Requires the dose 
 * influence matrix entries to be sorted.
 *
 * Only uses the bixels listed in bixelNos. Uses the corresponding entry in 
 * bixel_instanceNos to choose the instance associated with each bixel.
 */
inline
float DoseInfluenceMatrix::calculate_voxel_dose (
        unsigned int voxelNo,
        const BixelVector &bixel_grid,
        const vector<unsigned int> &bixelNos,
        const vector<unsigned int> &bixel_instanceNos)
{
    ensure_sorted();

    assert(bixelNos.size() == bixel_instanceNos.size());

    float dose_buf = 0;

    DoseInfluenceMatrix::iterator dij_iter = begin_voxel(voxelNo);
    vector<unsigned int>::const_iterator bixel_iter = bixelNos.begin();
    vector<unsigned int>::const_iterator aux_iter = bixel_instanceNos.begin();

    while(dij_iter.get_voxelNo() == voxelNo) {
        // Search through bixelNos to first equal to or reater then the 
        // current dij entry bixel number
        while(bixel_iter != bixelNos.end()
                && (*bixel_iter < dij_iter.get_bixelNo())) {
            ++bixel_iter;
            ++aux_iter;
        }

        // Break if we're out of included bixeNos
        if(bixel_iter == bixelNos.end()) {
            break;
        }

        // Add to dose if current bixel is in list
        if(*bixel_iter == dij_iter.get_bixelNo()) {
            dose_buf += bixel_grid[dij_iter.get_bixelNo()]
                * dij_iter.get_influence(*aux_iter);
        }
        dij_iter++;
    }
    return dose_buf;
}


/**
 * Calculates the derivative of the dose in a voxel 
 * with respect to the bixel vector
 */
inline
void DoseInfluenceMatrix::calculate_voxel_dose_gradient (
        unsigned int voxelNo,
        BixelVectorDirection &gradient,
        float gradient_multiplier,
        unsigned int auxiliaryNo)
{
    ensure_sorted();

    // Convert to float
    float d = gradient_multiplier;

    if (auxiliaryNo==0) {
	for(DoseInfluenceMatrix::iterator iter = begin_voxel(voxelNo);
	    iter.get_voxelNo() == voxelNo; iter++) {
	    gradient[iter.get_bixelNo()] += d * iter.get_influence();
	}
    }
    else {
	for(DoseInfluenceMatrix::iterator iter = begin_voxel(voxelNo);
	    iter.get_voxelNo() == voxelNo; iter++) {
	    gradient[iter.get_bixelNo()] += d * iter.get_influence(auxiliaryNo);
	}
    }
}


/**
 * Calculates the derivative of the dose in a voxel with respect to the bixel 
 * vector using only the specified bixels.
 */
inline
void DoseInfluenceMatrix::calculate_voxel_dose_gradient (
        unsigned int voxelNo,
        BixelVectorDirection &gradient,
        float gradient_multiplier,
        unsigned int auxiliaryNo,
        const vector<unsigned int> &bixelNos)
{
    DoseInfluenceMatrix::iterator iter = begin_voxel(voxelNo);
    vector<unsigned int>::const_iterator bixel_iter = bixelNos.begin();

    if (auxiliaryNo==0) {
        while(iter.get_voxelNo() == voxelNo) {
            // Search through bixelNos to first equal to or reater then the 
            // current dij entry bixel number
            while(bixel_iter != bixelNos.end()
                    && (*bixel_iter < iter.get_bixelNo())) {
                ++bixel_iter;
            }

            // Break if we're out of included bixeNos
            if(bixel_iter == bixelNos.end()) {
                break;
            }

            // Add to dose if current bixel is in list
            if(*bixel_iter == iter.get_bixelNo()) {
                gradient[iter.get_bixelNo()] += gradient_multiplier
                    * iter.get_influence();
            }
            iter++;
        }
    } else {
        while(iter.get_voxelNo() == voxelNo) {
            // Search through bixelNos to first equal to or reater then the 
            // current dij entry bixel number
            while(bixel_iter != bixelNos.end()
                    && (*bixel_iter < iter.get_bixelNo())) {
                ++bixel_iter;
            }

            // Break if we're out of included bixeNos
            if(bixel_iter == bixelNos.end()) {
                break;
            }

            // Add to dose if current bixel is in list
            if(*bixel_iter == iter.get_bixelNo()) {
                gradient[iter.get_bixelNo()] += gradient_multiplier
                    * iter.get_influence(auxiliaryNo);
            }
            iter++;
        }
    }
}


/**
 * Calculates the derivative of the dose in a voxel with respect to the bixel 
 * vector using only the specified bixels and using the specified auxiliary 
 * numbers for each bixel.
 */
inline
void DoseInfluenceMatrix::calculate_voxel_dose_gradient (
        unsigned int voxelNo,
        BixelVectorDirection &gradient,
        float gradient_multiplier,
        const vector<unsigned int> &bixelNos,
        const vector<unsigned int> &bixel_auxiliaryNos
        )
{
    DoseInfluenceMatrix::iterator dij_iter = begin_voxel(voxelNo);
    vector<unsigned int>::const_iterator bixel_iter = bixelNos.begin();
    vector<unsigned int>::const_iterator aux_iter = bixel_auxiliaryNos.begin();
    assert(bixelNos.size() == bixel_auxiliaryNos.size());

    while(dij_iter.get_voxelNo() == voxelNo) {
        // Search through bixelNos to first equal to or reater then the 
        // current dij entry bixel number
        while(bixel_iter != bixelNos.end()
                && (*bixel_iter < dij_iter.get_bixelNo())) {
            ++bixel_iter;
            ++aux_iter;
        }

        // Break if we're out of included bixeNos
        if(bixel_iter == bixelNos.end()) {
            break;
        }

        // Add to dose if current bixel is in list
        if(*bixel_iter == dij_iter.get_bixelNo()) {
            gradient[dij_iter.get_bixelNo()] += gradient_multiplier
                * dij_iter.get_influence(*aux_iter);
        }
        dij_iter++;
    }
}


/**
 * Calculates dose to one voxel delivered by the one beam direction.  
 * This will transpose the matrix if neccessary, 
 * so it cannot be done to a const DoseInfluenceMatrix.
 */
inline
float DoseInfluenceMatrix::calculate_voxel_dose_per_beam (
        unsigned int voxelNo,
        const BixelVector &bixel_grid,
	unsigned int beamNo,
	const Geometry &geometry,
	unsigned int auxiliaryNo)
{
  float dose_buf = 0;
  for(DoseInfluenceMatrix::iterator iter = begin_voxel(voxelNo);
      iter.get_voxelNo() == voxelNo; iter++) {
    if(geometry.get_beamNo(iter.get_bixelNo()) == beamNo) {
      dose_buf += bixel_grid[iter.get_bixelNo()] * iter.get_influence(auxiliaryNo);
    }
  }
  return dose_buf;
}

inline
void DoseInfluenceMatrix::set_nAuxiliaries(unsigned int nAuxiliaries)
{
  nAuxiliaries_ = nAuxiliaries;
}

inline
void DoseInfluenceMatrix::reserve_Dij(unsigned int nDij)
{
  influence_.reserve(nDij);
  voxelNo_or_bixelNo_.reserve(nDij);
}


/**
 * Insert a Dij entry 
 */
inline
void DoseInfluenceMatrix::add_Dij_element(
    unsigned int bixelNo, unsigned int voxelNo, float dose)
{
  if(!is_transposed_) {
    assert(bixel_start_index_[bixelNo] + bixel_nEntries_[bixelNo] < bixel_start_index_[bixelNo+1]);

    // Push back entry
    ++nEntries_;
    unsigned int entryNo = bixel_start_index_[bixelNo] 
      + bixel_nEntries_[bixelNo];
    influence_[entryNo] = dose;
    voxelNo_or_bixelNo_[entryNo] = voxelNo;
    ++bixel_nEntries_[bixelNo];
  }
  else {
    assert(voxel_start_index_[voxelNo] + voxel_nEntries_[voxelNo] < voxel_start_index_[voxelNo+1]);

    // Push back entry
    ++nEntries_;
    unsigned int entryNo = voxel_start_index_[voxelNo] 
      + voxel_nEntries_[voxelNo];
    influence_[entryNo] = dose;
    voxelNo_or_bixelNo_[entryNo] = bixelNo;
    ++voxel_nEntries_[voxelNo];
  }
}

/**
 * reset a Dij entry 
 */
inline
void DoseInfluenceMatrix::add_Dij_element(
    unsigned int bixelNo, unsigned int voxelNo, float dose, vector<float> auxiliary_dose)
{
  assert(auxiliary_dose.size() == nAuxiliaries_);

  if(!is_transposed_) {
    assert(bixel_start_index_[bixelNo] + bixel_nEntries_[bixelNo] < bixel_start_index_[bixelNo+1]);

    // Push back entry
    ++nEntries_;
    unsigned int entryNo = bixel_start_index_[bixelNo] 
      + bixel_nEntries_[bixelNo];
    influence_[entryNo] = dose;
    for(unsigned int i=0;i<nAuxiliaries_;i++) {
	auxiliary_influence_[i][entryNo] = auxiliary_dose[i];
    }
    voxelNo_or_bixelNo_[entryNo] = voxelNo;
    ++bixel_nEntries_[bixelNo];
  }
  else {
    assert(voxel_start_index_[voxelNo] + voxel_nEntries_[voxelNo] < voxel_start_index_[voxelNo+1]);

    // Push back entry
    ++nEntries_;
    unsigned int entryNo = voxel_start_index_[voxelNo] 
      + voxel_nEntries_[voxelNo];
    influence_[entryNo] = dose;
    for(unsigned int i=0;i<nAuxiliaries_;i++) {
	auxiliary_influence_[i][entryNo] = auxiliary_dose[i];
    }
    voxelNo_or_bixelNo_[entryNo] = bixelNo;
    ++voxel_nEntries_[voxelNo];
  }
}


// --------------------------------------------------
// Inline functions for DoseInfluenceMatrix::iterator
// --------------------------------------------------
inline
DoseInfluenceMatrix::iterator& DoseInfluenceMatrix::iterator::operator++ ()
{
  entryNo_++;
  if(entryNo_ >= the_DoseInfluenceMatrix_->nEntries_) {
    // At end, so set voxelNo_ and bixelNo_ larger than any possible entry
    voxelNo_ = the_DoseInfluenceMatrix_->nVoxels_;
    bixelNo_ = the_DoseInfluenceMatrix_->nBixels_;
    return *this;
  }
  
  if(!the_DoseInfluenceMatrix_->is_transposed_)     // Not Transposed
  {
    // check for blank space
    if(entryNo_ >= the_DoseInfluenceMatrix_->bixel_start_index_[bixelNo_] + the_DoseInfluenceMatrix_->bixel_nEntries_[bixelNo_])
    {
	bixelNo_++;
	entryNo_ = the_DoseInfluenceMatrix_->bixel_start_index_[bixelNo_];

	// check for missing bixels
	while(entryNo_==the_DoseInfluenceMatrix_->bixel_start_index_[bixelNo_+1]) {
	    bixelNo_++;
	}
    }
    // set voxelNo
    voxelNo_ = the_DoseInfluenceMatrix_->voxelNo_or_bixelNo_[entryNo_];
  }
  else      // Transposed
  {
    // check for blank space
    if(entryNo_ >= the_DoseInfluenceMatrix_->voxel_start_index_[voxelNo_] + the_DoseInfluenceMatrix_->voxel_nEntries_[voxelNo_])
    {
	voxelNo_++;
	entryNo_ = the_DoseInfluenceMatrix_->voxel_start_index_[voxelNo_];
	
	// check for missing voxels
	while(entryNo_==the_DoseInfluenceMatrix_->voxel_start_index_[voxelNo_+1]) {
	    voxelNo_++;
	}
    }
    // set bixelNo
    bixelNo_ = the_DoseInfluenceMatrix_->voxelNo_or_bixelNo_[entryNo_];
  }  

  return *this;
}


inline
void DoseInfluenceMatrix::iterator::operator++ (int)
{
  ++(*this);
}

inline
unsigned int DoseInfluenceMatrix::iterator::get_bixelNo()const
{
  return bixelNo_;
}

inline
unsigned int DoseInfluenceMatrix::iterator::get_voxelNo()const
{
  return voxelNo_;
} 

inline
float DoseInfluenceMatrix::iterator::get_influence()const
{
  return the_DoseInfluenceMatrix_->influence_[entryNo_];
} 

inline
float DoseInfluenceMatrix::iterator::get_influence(unsigned int auxiliaryNo)const
{
  if (auxiliaryNo==0) {
    return the_DoseInfluenceMatrix_->influence_[entryNo_];
  }
  else {
    return the_DoseInfluenceMatrix_->auxiliary_influence_[auxiliaryNo-1][entryNo_];
  }
} 

inline
bool DoseInfluenceMatrix::iterator::at_end()
{
  return (entryNo_ >= the_DoseInfluenceMatrix_->nEntries_);
}
  
inline
bool DoseInfluenceMatrix::iterator::not_at_end()
{
  return !at_end();
}


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Inline functions for DoseInfluenceMatrix::non_const_iterator        *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
inline
DoseInfluenceMatrix::non_const_iterator& DoseInfluenceMatrix::non_const_iterator::operator++ ()
{
  entryNo_++;
  if(entryNo_ >= the_DoseInfluenceMatrix_->nEntries_) {
    // At end, so set voxelNo_ and bixelNo_ larger than any possible entry
    voxelNo_ = the_DoseInfluenceMatrix_->nVoxels_;
    bixelNo_ = the_DoseInfluenceMatrix_->nBixels_;
    return *this;
  }
  
  if(!the_DoseInfluenceMatrix_->is_transposed_)     // Not Transposed
  {
    // check for blank space
    if(entryNo_ >= the_DoseInfluenceMatrix_->bixel_start_index_[bixelNo_] + the_DoseInfluenceMatrix_->bixel_nEntries_[bixelNo_])
    {
	bixelNo_++;
	entryNo_ = the_DoseInfluenceMatrix_->bixel_start_index_[bixelNo_];

	// check for missing bixels
	while(entryNo_==the_DoseInfluenceMatrix_->bixel_start_index_[bixelNo_+1]) {
	    bixelNo_++;
	}
    }
    // set voxelNo
    voxelNo_ = the_DoseInfluenceMatrix_->voxelNo_or_bixelNo_[entryNo_];
  }
  else      // Transposed
  {
    // check for blank space
    if(entryNo_ >= the_DoseInfluenceMatrix_->voxel_start_index_[voxelNo_] + the_DoseInfluenceMatrix_->voxel_nEntries_[voxelNo_])
    {
	voxelNo_++;
	entryNo_ = the_DoseInfluenceMatrix_->voxel_start_index_[voxelNo_];

	// check for missing bixels
	while(entryNo_==the_DoseInfluenceMatrix_->voxel_start_index_[voxelNo_+1]) {
	    voxelNo_++;
	}
    }
    // set bixelNo
    bixelNo_ = the_DoseInfluenceMatrix_->voxelNo_or_bixelNo_[entryNo_];
  }  
  return *this;
}

inline
void DoseInfluenceMatrix::non_const_iterator::operator++ (int)
{
  ++(*this);
}

inline
unsigned int DoseInfluenceMatrix::non_const_iterator::get_bixelNo()const
{
  return bixelNo_;
}

inline
unsigned int DoseInfluenceMatrix::non_const_iterator::get_voxelNo()const
{
  return voxelNo_;
} 

inline
float DoseInfluenceMatrix::non_const_iterator::get_influence()const
{
  return the_DoseInfluenceMatrix_->influence_[entryNo_];
} 

inline
float DoseInfluenceMatrix::non_const_iterator::get_influence(unsigned int auxiliaryNo)const
{
  if (auxiliaryNo==0) {
    return the_DoseInfluenceMatrix_->influence_[entryNo_];
  }
  else {
    return the_DoseInfluenceMatrix_->auxiliary_influence_[auxiliaryNo-1][entryNo_];
  }
} 

inline
bool DoseInfluenceMatrix::non_const_iterator::at_end()
{
  return (entryNo_ >= the_DoseInfluenceMatrix_->nEntries_);
}
  
inline
bool DoseInfluenceMatrix::non_const_iterator::not_at_end()
{
  return !at_end();
}

inline
void DoseInfluenceMatrix::non_const_iterator::set_influence(float influence, unsigned int auxiliaryNo)
{
  if (auxiliaryNo==0) {
    the_DoseInfluenceMatrix_->influence_[entryNo_] = influence;
  }
  else {
    the_DoseInfluenceMatrix_->auxiliary_influence_[auxiliaryNo-1][entryNo_] = influence;
  }
} 


#endif
