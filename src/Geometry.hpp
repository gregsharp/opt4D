/**
 * @file Geometry.hpp
 * Header for Geometry class
 *
 * $Id: Geometry.hpp,v 1.31 2008/05/30 14:27:41 unkelbac Exp $
 */

#ifndef GEOMETRY_HPP
#define GEOMETRY_HPP

#include <vector>
using std::vector;
#include <string>
using std::string;
#include <map>
using std::map;

#include <cmath>
#include <assert.h>
#include <iostream>
#include <stdexcept>
#include <cstdlib>

static const float PI=3.1415927;

class BixelIndex;
class Bixel_Subscripts;
class Shift_in_Patient_Coord;
class Shift_in_Collimator_Coord;
class Point_in_Patient_Coord;
class Point_in_Table_Coord;
class Point_in_Gantry_Coord;
class Point_in_Room_Coord;
class Point_in_Collimator_Coord;
class Voxel_Shift_Subscripts;
class Voxel_Subscripts;
class Geometry;
class CTscanAttributes;

#include "Voi.hpp"
#include "DoseVector.hpp"

/// Round a number up or down
int round(const float);

/// Randomly round a number up or down to preserve expected value
int randomly_round(const float);

/**
 * Compare two floats using an absolute tolerance
 */
inline
bool comp_float_abs(float a, float b, float epsilon)
{
    if(fabs(a-b) < epsilon) {
	return true;
    }
    return false;
}

/**
 * Holds bixel index which is not identical to the bixelNo
 *
 * Use the Geometry class to convert to another coordinate system.
 */
class BixelIndex
{
  public:
    BixelIndex(const unsigned int rhs=0) { index_ = rhs; };

    /// Index of the voxel
    unsigned int index_;
};

/**
 * Holds subscripts for a bixel shift
 *
 * iX_ is the lateral component
 * iY_ is the lateral component
 * iE_ is the energy layer
 *
 * Use the Geometry class to convert to another coordinate system.
 */
class Bixel_Shift_Subscripts
{
  public:
    Bixel_Shift_Subscripts(int iX=0, int iY=0, int iE=0)
    { iX_=iX; iY_=iY; iE_ = iE; };

    // operator overloding
    /// addition
    inline Bixel_Shift_Subscripts operator + (const Bixel_Shift_Subscripts& rhs)
	{ return ( Bixel_Shift_Subscripts(iX_+rhs.iX_, iY_+rhs.iY_, iE_+rhs.iE_) ); };

    /// print shift
    friend std::ostream& operator<< (std::ostream& o, const Bixel_Shift_Subscripts& rhs);

    /// Subscripts of the voxel
    int iX_, iY_, iE_;
};

/**
 * Holds subscripts for a bixel
 *
 * iX_ is the lateral component
 * iY_ is the lateral component
 * iE_ is the energy layer
 *
 * Use the Geometry class to convert to another coordinate system.
 */
class Bixel_Subscripts
{
  public:
    Bixel_Subscripts(int iX=0, int iY=0, int iE=0)
    { iX_=iX; iY_=iY; iE_ = iE; };

    // operator overloding
    /// addition
    inline Bixel_Subscripts operator + (const Bixel_Shift_Subscripts& rhs)
	{ return ( Bixel_Subscripts(iX_+rhs.iX_, iY_+rhs.iY_, iE_+rhs.iE_) ); };

    /// print subscripts
    friend std::ostream& operator<< (std::ostream& o, const Bixel_Subscripts& rhs);

    /// Subscripts of the bixel
    int iX_, iY_, iE_;
};

/**
 * Holds position of a bixel
 *
 * x_ is the lateral component
 * y_ is the lateral component
 * E_ is the energy
 *
 * Use the Geometry class to convert to another coordinate system.
 */
class Bixel_Position
{
  public:
    Bixel_Position(float x=0, float y=0, float E=0)
	{ x_=x; y_=y; E_=E; };

    // operator overloding
    inline bool operator==(const Bixel_Position& rhs) const {
      float epsilon = 0.001;
      return(comp_float_abs(x_,rhs.x_,epsilon)
	     && comp_float_abs(y_,rhs.y_,epsilon)
	     && comp_float_abs(E_,rhs.E_,epsilon));
    }

    /// print position
    friend std::ostream& operator<< (std::ostream& o, const Bixel_Position& rhs);

    ///  x,y position and energy
    float x_, y_, E_;
};

/**
 * Holds voxelNo
 *
 * Use the Geometry class to convert to another coordinate system.
 */
class VoxelIndex
{
  public:
    VoxelIndex(const unsigned int rhs) { index_ = rhs; };

    /// Index of the voxel
    unsigned int index_;
};

/**
 * Holds shift in the patient's coordinate system.
 *
 * x is the lateral component
 * y is the axial component
 * z is the anterior-posterior component
 *
 * Use the Geometry class to convert to another coordinate system.
 */
class Shift_in_Patient_Coord
{
  public:
    Shift_in_Patient_Coord(float x=0.f, float y=0.f, float z=0.f)
    { x_=x; y_=y; z_=z; };

    // operator overloding
    /// vector addition
    inline Shift_in_Patient_Coord& operator+= (const Shift_in_Patient_Coord& rhs)
	{ x_+=rhs.x_; y_+=rhs.y_; z_+=rhs.z_; return(*this); };

    /// vector addition
    inline Shift_in_Patient_Coord operator+ (const Shift_in_Patient_Coord& rhs) const
	{ return ( Shift_in_Patient_Coord(x_+rhs.x_, y_+rhs.y_, z_+rhs.z_) ); };

    /// vector subtraction
    inline Shift_in_Patient_Coord& operator-= (const Shift_in_Patient_Coord& rhs)
	{ x_-=rhs.x_; y_-=rhs.y_; z_-=rhs.z_; return(*this); };

    /// vector subtraction
    inline Shift_in_Patient_Coord operator- (const Shift_in_Patient_Coord& rhs) const
	{ return ( Shift_in_Patient_Coord(x_-rhs.x_, y_-rhs.y_, z_-rhs.z_) ); };

    /// scalar product
    inline float operator* (const Shift_in_Patient_Coord& rhs) const
	{ return (x_*rhs.x_ + y_*rhs.y_ + z_*rhs.z_); };

    /// multiplication with scalar
    inline Shift_in_Patient_Coord& operator*= (const float rhs)
	{ x_*=rhs; y_*=rhs; z_*=rhs; return *this; };

    /// multiplication with scalar
    inline Shift_in_Patient_Coord operator* (const float& rhs) const
	{ return ( Shift_in_Patient_Coord(x_*rhs, y_*rhs, z_*rhs) ); };

    /// Lexicographic comparison
    bool operator<(
            const Shift_in_Patient_Coord& rhs) const ;

    /// print shift
    friend std::ostream& operator<< (std::ostream& o, const Shift_in_Patient_Coord& rhs);

    /// shift in mm
    float x_, y_, z_;
};

/**
 * Holds point in the patient's coordinate system.
 *
 * x is the lateral component
 * y is the axial component
 * z is the anterior-posterior component
 *
 * Use the Geometry class to convert to another coordinate system.
 */
class Point_in_Patient_Coord
{
  public:
    Point_in_Patient_Coord(float x=0.f, float y=0.f, float z=0.f)
    { x_=x; y_=y; z_=z; };

    // operator overloding
    /// vector addition
    inline Point_in_Patient_Coord operator+ (const Shift_in_Patient_Coord& rhs) const
	{ return ( Point_in_Patient_Coord(x_+rhs.x_, y_+rhs.y_, z_+rhs.z_) ); };

    /// vector addition
    inline Point_in_Patient_Coord& operator+= (const Point_in_Patient_Coord& rhs)
	{ x_+=rhs.x_; y_+=rhs.y_; z_+=rhs.z_; return(*this); };

    /// vector addition
    inline Point_in_Patient_Coord operator+ (const Point_in_Patient_Coord& rhs) const
	{ return ( Point_in_Patient_Coord(x_+rhs.x_, y_+rhs.y_, z_+rhs.z_) ); };

    /// Less than comparison
    bool operator<(const Point_in_Patient_Coord& rhs) const;

    /// Position of point in mm
    float x_, y_, z_;

};

/**
 * Holds the voxel subscripts shifts corresponding to a physical shift
 * in the Patient Coordinate System
 *
 * iX_ is the lateral component
 * iY_ is the axial component
 * iZ_ is the anterior-posterior component
 *
 * Use the Geometry class to convert to another coordinate system.
 */
class Voxel_Shift_Subscripts
{
  public:
    Voxel_Shift_Subscripts(int iX=0, int iY=0, int iZ=0)
    { iX_=iX; iY_=iY; iZ_ = iZ; };

    /// Lexicographic comparison operator
    bool operator<(const Voxel_Shift_Subscripts& rhs) const;

    /// print shift
    friend std::ostream& operator<< (std::ostream& o, const Voxel_Shift_Subscripts& rhs);

    /// Subscripts of the voxel
    int iX_, iY_, iZ_;
};

/**
 * Holds subscripts for a voxel
 *
 * iX_ is the lateral component
 * iY_ is the axial component
 * iZ_ is the anterior-posterior component
 *
 * Use the Geometry class to convert to another coordinate system.
 */
class Voxel_Subscripts
{
  public:
    Voxel_Subscripts(int iX=0, int iY=0, int iZ=0)
    { iX_=iX; iY_=iY; iZ_ = iZ; };

    // operator overloding
    /// addition
    inline Voxel_Subscripts operator + (const Voxel_Subscripts& rhs)
	{ return ( Voxel_Subscripts(iX_+rhs.iX_, iY_+rhs.iY_, iZ_+rhs.iZ_) ); };

    inline Voxel_Subscripts operator + (const Voxel_Shift_Subscripts& rhs)
	{ return ( Voxel_Subscripts(iX_+rhs.iX_, iY_+rhs.iY_, iZ_+rhs.iZ_) ); };

    /// Subscripts of the voxel
    int iX_, iY_, iZ_;
};

/**
 * Holds point in collimator coords
 *
 * x_ is lateral component
 * y_ is lateral component
 * depth_ is component in beam direction
 *
 * Use the Geometry class to convert to another coordinate system.
 */
class Point_in_Collimator_Coord
{
  public:
    Point_in_Collimator_Coord(float x=0.f, float y=0.f, float depth=0.f)
    { x_=x; y_=y; depth_=depth; };

    // operator overloding
    /// vector addition
    inline Point_in_Collimator_Coord operator + (const Point_in_Collimator_Coord& rhs)
	{ return ( Point_in_Collimator_Coord(x_+rhs.x_, y_+rhs.y_, depth_+rhs.depth_) ); };

    /// Position of point in mm
    float x_, y_, depth_;
};

/**
 * Holds shift in collimator coords
 *
 * x_ is lateral component
 * y_ is lateral component
 * depth_ is component in beam direction
 */
class Shift_in_Collimator_Coord
{
  public:
    Shift_in_Collimator_Coord(float x=0.f, float y=0.f, float depth=0.f)
    { x_=x; y_=y; depth_=depth; };

    // operator overloding
    /// vector addition
    inline Shift_in_Collimator_Coord operator + (const Shift_in_Collimator_Coord& rhs)
	{ return ( Shift_in_Collimator_Coord(x_+rhs.x_, y_+rhs.y_, depth_+rhs.depth_) ); };

    /// multiplication with scalar
    inline Shift_in_Collimator_Coord operator* (const float& rhs)
	{ return ( Shift_in_Collimator_Coord(x_*rhs, y_*rhs, depth_*rhs) ); };

    /// Position of point in mm
    float x_, y_, depth_;
};

class Point_in_Gantry_Coord
{
  public:
    Point_in_Gantry_Coord(float x=0.f, float y=0.f, float depth=0.f)
    { x_=x; y_=y; depth_=depth; };

    // operator overloding
    /// vector addition
    inline Point_in_Gantry_Coord operator + (const Point_in_Gantry_Coord& rhs)
	{ return ( Point_in_Gantry_Coord(x_+rhs.x_, y_+rhs.y_, depth_+rhs.depth_) ); };

    /// Position of point in mm
    float x_, y_, depth_;
};

class Point_in_Table_Coord
{
  public:
    Point_in_Table_Coord(float x=0.f, float y=0.f, float z=0.f)
    { x_=x; y_=y; z_=z; };

    // operator overloding
    /// vector addition
    inline Point_in_Table_Coord operator + (const Point_in_Table_Coord& rhs)
	{ return ( Point_in_Table_Coord(x_+rhs.x_, y_+rhs.y_, z_+rhs.z_) ); };

    /// Position of point in mm
    float x_, y_, z_;
};

class Point_in_Room_Coord
{
  public:
    Point_in_Room_Coord(float x=0.f, float y=0.f, float z=0.f)
    { x_=x; y_=y; z_=z; };

    // operator overloding
    /// vector addition
    inline Point_in_Room_Coord operator + (const Point_in_Room_Coord& rhs)
	{ return ( Point_in_Room_Coord(x_+rhs.x_, y_+rhs.y_, z_+rhs.z_) ); };

    /// Position of point in mm
    float x_, y_, z_;
};

/**
 * Point in CT coordinates
 */
class Point_in_CT_Coord
{
  public:
    Point_in_CT_Coord(float x=0.f, float y=0.f, float z=0.f)
    { x_=x; y_=y; z_=z; };

    /// Position of point in mm
    float x_, y_, z_;
};

/**
 * Subscripts of a CT voxel
 */
class CT_Voxel_Subscripts
{
  public:
    CT_Voxel_Subscripts(int iX=0, int iY=0, int iZ=0)
    { iX_=iX; iY_=iY; iZ_ = iZ; };

    /// Subscripts of the voxel
    int iX_, iY_, iZ_;
};

/**
 * Holds CT voxel index
 */
class CTVoxelIndex
{
  public:
    CTVoxelIndex(const unsigned int rhs) { index_ = rhs; };

    /// Index of the voxel
    unsigned int index_;
};


/**
 * This class holds size and resolution of the original CT scan.
 */
class CTscanAttributes
{

  public:

    /// constructor
    CTscanAttributes(Geometry *the_geometry,string ctatts_file_name);

    /// constructor
    CTscanAttributes();

    /// find out whether attributes are successfully set
    bool is_initialized() const {return is_initialized_;};

    /// returns an interpolated dose cube which has the size of the original CT
    DoseVector interpolate_dose_cube(const DoseVector &dose_vector) const;

    /// Convert from CT voxel subscript to a point in CT coordinates
    Point_in_CT_Coord convert_to_Point_in_CT_Coord(const CT_Voxel_Subscripts rhs) const;

    /// Convert from voxelNo to voxel subscripts
    CT_Voxel_Subscripts convert_to_CT_Voxel_Subscripts(const CTVoxelIndex rhs) const;

    /// Convert from CT voxel index to a point in CT coordinates
    Point_in_CT_Coord convert_to_Point_in_CT_Coord(const CTVoxelIndex rhs) const;

    /// Convert from point in CT coordinates to point in patient coordinates
    Point_in_Patient_Coord convert_to_Point_in_Patient_Coord(const Point_in_CT_Coord rhs) const;

  private:

    /// pointer to geometry class
    Geometry *the_geometry_;

    /// find out whether attributes are successfully set
    bool is_initialized_;

    /// slice thickness
    float slice_distance_;

    /// voxel size
    float pixel_size_;

    /// number of slices
    unsigned int nSlices_;

    /// number of voxels per direction
    unsigned int nDimension_;

    /// isocenter location x
    float isoX_;

    /// isocenter location y
    float isoY_;

    /// isocenter location z
    float isoZ_;
};



/**
 * Preforms geometry calculations for an IMRT or IMPT plan.
 */
class Geometry {
  public:
    /// Constructors which reads .dif file
    Geometry(string dif_file_name, unsigned int nBeams = 0, unsigned int nBixels = 0);

    /// default constructor
    Geometry(unsigned int nBeams = 0, unsigned int nBixels = 0);

    /// copy constructor which takes voxel information only and clears beam information
    Geometry(const Geometry & rhs, unsigned int nBeams, unsigned int nBixels);

    inline bool is_initialized() const {return is_initialized_;};
    inline void set_initialized(bool status) {is_initialized_=status;};
    inline bool bixel_grid_is_valid() const {return bixel_grid_is_valid_;};

    // --------------------------------------
    // Accessors for information about voxels
    // --------------------------------------
    void set_voxel_size(float dx, float dy, float dz);
    void set_nVoxels(unsigned int nX, unsigned int nY, unsigned int nZ);
    void set_isocenter_in_voxel_coordinates(float isox, float isoy, float isoz);

    unsigned int get_nVoxels() const { return nX_ * nY_ * nZ_; };
    float  get_voxel_dx() const { return voxel_dx_; };
    float  get_voxel_dy() const { return voxel_dy_; };
    float  get_voxel_dz() const { return voxel_dz_; };
    unsigned int get_voxel_nx() const { return nX_; };
    unsigned int get_voxel_ny() const { return nY_; };
    unsigned int get_voxel_nz() const { return nZ_; };
    bool is_inside(const Voxel_Subscripts subscripts) const;
    bool is_air(const unsigned int voxelNo) const;
    bool is_air(const VoxelIndex voxelNo) const;
    bool is_valid_voxel(const Voxel_Subscripts subscripts) const;
    void load_air_voxels(vector<Voi *> vois);
    void load_ct_attributes(string ctatts_file_name);

    // -------------------------------------
    // Accessors for information about beams
    // -------------------------------------
    void set_nBeams(unsigned int nBeams);
    void set_gantry_angle(const std::vector<float> & gantry_angle);
    void set_table_angle(const std::vector<float> & table_angle);
    void set_collimator_angle(const std::vector<float> & collimator_angle);
    void set_bixel_grid_size(
        const std::vector<float> & bixel_grid_dx,
        const std::vector<float> & bixel_grid_dy);
    void set_beam_SAD(const std::vector<float> & beam_SAD );
    void set_beam_dWEL(const std::vector<float> & beam_dWEL );
    void set_beam_starting_bixelNo(
	const std::vector<unsigned int> & beam_starting_bixelNo);
    void set_nBixels_in_beam(
	const std::vector<unsigned int> & nBixels_in_beam);

    const unsigned int get_nBeams() const;
    const std::vector<float> & get_gantry_angle() const;
    const std::vector<float> & get_collimator_angle() const;
    const std::vector<float> & get_table_angle() const;
    const std::vector<float> & get_bixel_grid_dx() const;
    const std::vector<float> & get_bixel_grid_dy() const;
    float get_beam_dWEL(unsigned int beamNo) const;
    unsigned int get_beam_starting_bixelNo(unsigned int beamNo) const;
    unsigned int get_nBixels_in_beam(unsigned int beamNo) const;
    std::vector<unsigned int> get_beam_starting_bixelNo() const;
    std::vector<unsigned int> get_nBixels_in_beam() const;
    unsigned int get_bixel_grid_nX(unsigned int beamNo) const;
    unsigned int get_bixel_grid_nY(unsigned int beamNo) const;
    float get_bixel_minX(unsigned int beamNo) const;
    float get_bixel_maxX(unsigned int beamNo) const;
    float get_bixel_minY(unsigned int beamNo) const;
    float get_bixel_maxY(unsigned int beamNo) const;
    const std::vector<float> &  get_bixel_minX() const;
    const std::vector<float> &  get_bixel_maxX() const;
    const std::vector<float> &  get_bixel_minY() const;
    const std::vector<float> &  get_bixel_maxY() const;

    Point_in_Patient_Coord get_isocenter(unsigned int BeamNo) const;


    // --------------------------------------
    // Accessors for information about bixels
    // --------------------------------------
    const unsigned int get_nBixels() const;
    void set_nBixels(unsigned int nBixels);
    void set_bixel_beamNo(
        const std::vector<unsigned int> & bixel_beamNo);
    void set_bixel_position(
        const std::vector<Bixel_Position> & bixel_position);
    void set_physical_bixel(unsigned int bixelNo, bool value);

    Bixel_Position get_bixel_position(unsigned int bixelNo) const;
    Bixel_Subscripts get_bixel_subscript(unsigned int bixelNo) const;
    float get_bixel_energy(unsigned int bixelNo) const;
    float get_bixel_position_x(unsigned int bixelNo) const;
    float get_bixel_position_y(unsigned int bixelNo) const;

    void set_isocenters();

    unsigned int get_beamNo(unsigned int bixelNo) const;

    unsigned int get_bixelNo(
	const BixelIndex rhs, unsigned int beamNo) const;

    unsigned int get_bixelNo(
	const Bixel_Subscripts rhs, unsigned int beamNo) const;

    unsigned int get_beam_bixel_nPoints(unsigned int beamNo) const;
    bool is_physical_bixel(unsigned int bixelNo) const;
    bool is_valid_bixel(Bixel_Subscripts subscripts, unsigned int beamNo) const;
    bool is_inside(Bixel_Subscripts subscripts, unsigned int beamNo) const;
    void assert_valid_bixel(Bixel_Subscripts subscripts, unsigned int beamNo) const;
    void assert_bixel_inside(Bixel_Subscripts subscripts, unsigned int beamNo) const;


    // ---------------------------------------
    // initialize the bixel grid for all beams
    // ---------------------------------------

    /// initialize the beamlet grid
    void initialize_bixel_grid();

    /// initialize the energy-subscript lookup table
    void initialize_energy_table();

    /// initialize bixel grid size
    void initialize_bixel_grid_size();

    /// set bixel subscripts
    void set_bixel_subscripts_and_index();

    /// set bixelNo in bixel grid
    bool set_bixelNo_in_bixel_grid();

    // -----------------------------------------------
    // Conversion between different coordinate systems
    // -----------------------------------------------

    // Convert from point in patient to voxel subscripts
    Voxel_Subscripts convert_to_Voxel_Subscripts(
        const Point_in_Patient_Coord rhs) const;

    // Convert from voxel subscript to a point in patient
    Point_in_Patient_Coord convert_to_Point_in_Patient_Coord(const Voxel_Subscripts rhs) const;

    // Convert from voxel subscripts to voxelNo
    VoxelIndex convert_to_VoxelIndex(const Voxel_Subscripts rhs) const;

    // Convert from voxelNo to voxel subscripts
    Voxel_Subscripts convert_to_Voxel_Subscripts(const VoxelIndex rhs) const;

    // Convert from point in patient to a voxelNo
    VoxelIndex convert_to_VoxelIndex(const Point_in_Patient_Coord rhs) const;

    // Convert from a voxelNo to a point in patient
    Point_in_Patient_Coord convert_to_Point_in_Patient_Coord(
            const VoxelIndex rhs) const;

    // Convert from subsrcipt shift to a in patient
    Shift_in_Patient_Coord convert_to_Shift_in_Patient_Coord(
            const Voxel_Shift_Subscripts rhs) const;

    // Convert from shift in patient to voxel subscripts shifts
    Voxel_Shift_Subscripts convert_to_Shift_Subscripts(
        const Shift_in_Patient_Coord rhs) const;

    // Convert from shift in patient to voxel subscripts shifts
    void linearly_interpolate_to_Shift_Subscripts(
        const Shift_in_Patient_Coord rhs,
        const float weight,
        vector<std::pair<Voxel_Shift_Subscripts, float> >& result ) const;

    // Convert from shift in patient to voxel subscripts shifts
    Voxel_Shift_Subscripts randomly_convert_to_Shift_Subscripts(
        const Shift_in_Patient_Coord rhs) const;

    // Convert from bixel subscripts to bixel index
    BixelIndex convert_to_BixelIndex(
	const Bixel_Subscripts rhs, const unsigned int beamNo) const;

    // Convert from bixel index to bixel subscripts
    Bixel_Subscripts convert_to_Bixel_Subscripts(
	const BixelIndex rhs, const unsigned int beamNo) const;

    // Convert from bixel number to bixel index
    BixelIndex convert_to_BixelIndex(
	const unsigned int bixelNo) const;

    // Convert from bixel number to bixel subscripts
    Bixel_Subscripts convert_to_Bixel_Subscripts(
	const unsigned int bixelNo) const;

    // Convert from a shift in collimator coordinates to bixel shift subscripts
    Bixel_Shift_Subscripts convert_to_Lateral_Bixel_Shift_Subscripts(
	const Shift_in_Collimator_Coord rhs, const unsigned int beamNo) const;

    // Convert a range shift (in WER) to bixel shift subscripts
    Bixel_Shift_Subscripts convert_to_Axial_Bixel_Shift_Subscripts(
	const float range_shift, const unsigned int beamNo) const;

    /// convert bixel position to bixel subscripts
    Bixel_Subscripts convert_to_Bixel_Subscripts(Bixel_Position rhs, unsigned int beamNo) const;

    // --------------------------
    // Coordinate transformations
    // --------------------------

    /// Transformation into gantry coordinate system
    Point_in_Gantry_Coord transform_to_Gantry_Coord(
	    const Point_in_Room_Coord rhs, const int BeamNo) const;
    Point_in_Gantry_Coord transform_to_Gantry_Coord(
	    const Point_in_Patient_Coord rhs, const int BeamNo) const;
    Point_in_Gantry_Coord transform_to_Gantry_Coord(
	    const Point_in_Collimator_Coord rhs, const int BeamNo) const;

    /// Transformation into patient coordinate system
    Point_in_Patient_Coord transform_to_Patient_Coord(
	    const Point_in_Table_Coord rhs, const int BeamNo)const ;
    Point_in_Patient_Coord transform_to_Patient_Coord(
	    const Point_in_Gantry_Coord rhs, const int BeamNo) const;

    /// Transformation into room coordinate system
    Point_in_Room_Coord transform_to_Room_Coord(
	    const Point_in_Table_Coord rhs, const int BeamNo) const;
    Point_in_Room_Coord transform_to_Room_Coord(
	    const Point_in_Gantry_Coord rhs, const int BeamNo) const;

    /// Transformation into table coordinate system
    Point_in_Table_Coord transform_to_Table_Coord(
	const Point_in_Patient_Coord rhs, const int BeamNo) const;
    Point_in_Table_Coord transform_to_Table_Coord(
	const Point_in_Room_Coord rhs, const int BeamNo) const;

    /// Transformation into collimator coordinate system
    Point_in_Collimator_Coord transform_to_Collimator_Coord(
	const Point_in_Gantry_Coord rhs, const int BeamNo) const;

    /// Transformation of shifts into patient coordinate system
    Shift_in_Patient_Coord transform_to_Shift_in_Patient_Coord(
	const Point_in_Table_Coord rhs, const int BeamNo)const ;
    Shift_in_Patient_Coord transform_to_Shift_in_Patient_Coord(
	const Point_in_Gantry_Coord rhs, const int BeamNo) const;
    Shift_in_Collimator_Coord transform_to_Shift_in_Collimator_Coord(
	const Shift_in_Patient_Coord rhs, const int BeamNo) const;


    // -------------------------
    // Other Geometric Functions
    // -------------------------

    /// get dose to a point in the patient by interpolation using the 8 adjacent voxels
    float get_dose_by_trilinear_interpolation(
	    const DoseVector &dose_vector, const Point_in_Patient_Coord point) const;

    // -------------------------
    // Tools
    // -------------------------

    /// print out a dose cube on the planning CT
    void write_dose_on_planning_CT(const DoseVector &dose_vector, string file_name) const;

    /// Compare two floats using an absolute tolerance
    bool comp_float_abs(float a, float b, float epsilon) const;


    // -------------------------
    // Legacy
    // -------------------------
    /*
    // Calculate Radiological Path Length. This is purely a dummy
    // implementation.
    float calculate_RPL(const unsigned int beamNo,
        const unsigned int j, const float z)const;

    /// Transformation from table into gantry coordinate system
    void rot_table_gantry(const unsigned int beamNo, float* xx, float* yy, float* zz) const;

    /// Transformation from parallel into cone beam coordinate system
    void trans_par_cone(const unsigned int beamNo, float* xx, float* yy, float* zz) const;
    */

  private:

    bool is_initialized_;
    bool bixel_grid_is_valid_;

    /// information about planning CT
    CTscanAttributes ctatts_;

    // ----------------------------
    // Information about voxel grid
    // ----------------------------

    /// Number of voxels in each dimension
    unsigned int nX_, nY_, nZ_;

    /// Voxel size in mm
    float voxel_dx_, voxel_dy_, voxel_dz_;

    /// isocenter voxel in dose information file
    float iso_x_, iso_y_, iso_z_;

    /// tells if a voxel is air
    std::vector<bool> is_air_;

    // ---------------------------
    // Information about each beam
    // ---------------------------

    /// Total number of beams
    unsigned int nBeams_;

    /// number of bixels in each beam
    std::vector<unsigned int> nBixels_in_beam_;

    /// Starting bixelNo for each beam.
    std::vector<unsigned int> beam_starting_bixelNo_;

    /// The angle of the gantry for each beam in degrees
    std::vector<float> beam_gantry_angle_;

    /// The angle of the table for each beam in degrees
    std::vector<float> beam_table_angle_;

    /// The angle of the colimator for each beam in degrees
    std::vector<float> beam_collimator_angle_;

    /// The SAD for each beam
    std::vector<float> beam_sad_;

    /// The location of the isocenter for each beam with respect to the origin of the patient coordinate system
    std::vector<Point_in_Patient_Coord> isocenter_;

    // ------------------------------
    // Information about each beamlet
    // ------------------------------

    /// The total number of bixels
    unsigned int nBixels_;

    /// The beamNo for each bixel
    std::vector<unsigned int> bixel_beamNo_;

    /// bixel index in the grid for each beamlet
    std::vector<BixelIndex> bixel_index_;

    /// bixel grid subscripts for each bixel
    std::vector<Bixel_Subscripts> bixel_subscripts_;

    /// find out whether it's a physical beamlet, not a virtual
    std::vector<bool> bixel_is_physical_;

    /// bixel position (x,y,energy)
    std::vector<Bixel_Position> bixel_position_;

    // ------------------------------
    // Information about bixel grid
    // ------------------------------

    /// total number of bixel grid points in each beam
    std::vector<unsigned int> beam_bixel_nPoints_;

    /// size of bixel grid in x divided by 2 minus 1
    std::vector<unsigned int> beam_bixel_nX_;

    /// size of bixel grid in y divided by 2 minus 1
    std::vector<unsigned int> beam_bixel_nY_;

    /// size of bixel grid: number of energy layers
    std::vector<unsigned int> beam_bixel_nE_;

    /// bixel grid resolution for each beam in x
    std::vector<float> beam_bixel_dx_;

    /// bixel grid resolution for each beam in y
    std::vector<float> beam_bixel_dy_;

    /// minimum x bixel positions for each beam
    std::vector<float> beam_bixel_minX_;

    /// maximum x bixel positions for each beam
    std::vector<float> beam_bixel_maxX_;

    /// minimum y bixel positions for each beam
    std::vector<float> beam_bixel_minY_;

    /// maximum y bixel positions for each beam
    std::vector<float> beam_bixel_maxY_;

    /// bixel grid resolution in depth:
    /// distance of bragg peaks in water equivalent depth
    std::vector<float> beam_bixel_dwel_;

    /// contains the bixelNo for each bixel grid point in each beam
    std::vector<std::vector<unsigned int> > beam_grid_bixelNo_;

    /// tells whether there is a bixel at this grid point in a beam
    std::vector<std::vector<bool> > beam_grid_is_bixel_;

    /// lookup table for energy layer to subscript conversion
    std::vector<std::vector<float> > beam_energy_table_;

};

/********************************
 *      Inline Functions        *
 ********************************/


/**
 * Round a number up or down
 *
 * For example, 1.25 rounds to 1 while 1.5 or 1.75 round to 2.
 *
 * @param rhs the number to round
 */
inline
int round(const float rhs)
{
    return static_cast<int>(floor(rhs+0.5f));
}


/**
 * Randomly round a number up or down to preserve expected value
 *
 * For example, 1.25 has a 75% chance of being rounded to 1 and a 25% chance of
 * being rounded to 2.
 *
 * @param rhs the number to round
 */
inline
int randomly_round(const float rhs)
{
    int iPart = static_cast<int>(floor(rhs));
    float fpart = rhs - floor(rhs);
    int threshold = static_cast<int>(
            fpart * static_cast<float>(RAND_MAX));
    if(std::rand() > threshold) {
        // Round down
        return iPart;
    } else {
        // Round up
        return iPart + 1;
    }
}



/**
 * Less than comparison.  Performs a lexicographic comparison
 */
inline
bool Point_in_Patient_Coord::operator<(
        const Point_in_Patient_Coord& rhs ///< the thing to compare against
        ) const
{
    if(x_ < rhs.x_) {
        return true;
    } else if(rhs.x_ < x_) {
        return false;
    } else {
        if(y_ < rhs.y_) {
            return true;
        } else if(rhs.y_ < y_) {
            return false;
        } else {
            return z_ < rhs.z_;
        }
    }
}


/**
 * Less than comparison.  Performs a lexicographic comparison
 */
inline
bool Shift_in_Patient_Coord::operator<(
        const Shift_in_Patient_Coord& rhs ///< the thing to compare against
        ) const
{
    if(x_ < rhs.x_) {
        return true;
    } else if(rhs.x_ < x_) {
        return false;
    }

    if(y_ < rhs.y_) {
        return true;
    } else if(rhs.y_ < y_) {
        return false;
    }

    return z_ < rhs.z_;
}


/**
 * Less than comparison.  Performs a lexicographic comparison
 */
inline
bool Voxel_Shift_Subscripts::operator<(
        const Voxel_Shift_Subscripts& rhs ///< the thing to compare against
        ) const
{
    if(iX_ < rhs.iX_) {
        return true;
    } else if(rhs.iX_ < iX_) {
        return false;
    } else {
        if(iY_ < rhs.iY_) {
            return true;
        } else if(rhs.iY_ < iY_) {
            return false;
        } else {
            return iZ_ < rhs.iZ_;
        }
    }
}


/**
 * Convert from point in patient to a subscript
 */
inline
Voxel_Subscripts Geometry::convert_to_Voxel_Subscripts(
    const Point_in_Patient_Coord rhs) const
{
  Voxel_Subscripts temp;
  temp.iX_ = (int) (rhs.x_ / voxel_dx_);
  temp.iY_ = (int) (rhs.y_ / voxel_dy_);
  temp.iZ_ = (int) (rhs.z_ / voxel_dz_);
  return temp;
}


/**
 * Convert from shift in patient to a subscript shift
 */
inline
Voxel_Shift_Subscripts Geometry::convert_to_Shift_Subscripts(
    const Shift_in_Patient_Coord rhs) const
{
  Voxel_Shift_Subscripts temp;
  temp.iX_ = roundf(rhs.x_ / voxel_dx_);
  temp.iY_ = roundf(rhs.y_ / voxel_dy_);
  temp.iZ_ = roundf(rhs.z_ / voxel_dz_);
  return temp;
}


/**
 * Convert from shift in patient to a subscript shift with random rounding
 */
inline
Voxel_Shift_Subscripts Geometry::randomly_convert_to_Shift_Subscripts(
    const Shift_in_Patient_Coord rhs) const
{
  Voxel_Shift_Subscripts temp;
  temp.iX_ = randomly_round(rhs.x_ / voxel_dx_);
  temp.iY_ = randomly_round(rhs.y_ / voxel_dy_);
  temp.iZ_ = randomly_round(rhs.z_ / voxel_dz_);
  return temp;
}


/**
 * Convert from shift in patient to voxel subscripts shifts
 * Adds results to end of result vector.  Doesn't add shifts with zero weight
 */
inline
void Geometry::linearly_interpolate_to_Shift_Subscripts(
        const Shift_in_Patient_Coord rhs,
        const float weight,
        vector<std::pair<Voxel_Shift_Subscripts, float> >& result ) const
{
    int iX_low = static_cast<int>(floor(rhs.x_));
    int iX_high = iX_low + 1;
    float weight_x_high = rhs.x_ - floor(rhs.x_);
    float weight_x_low = 1.0-weight_x_high;

    int iY_low = static_cast<int>(floor(rhs.y_));
    int iY_high = iY_low + 1;
    float weight_y_high = rhs.y_ - floor(rhs.y_);
    float weight_y_low = 1-weight_y_high;

    int iZ_low = static_cast<int>(floor(rhs.z_));
    int iZ_high = iZ_low + 1;
    float weight_z_high = rhs.z_ - floor(rhs.z_);
    float weight_z_low = 1-weight_z_high;

    float temp_weight = weight*weight_x_low * weight_y_low * weight_z_low;
    if(temp_weight > 0.0) {
        result.push_back(std::pair<Voxel_Shift_Subscripts, float>(
                    Voxel_Shift_Subscripts(iX_low, iY_low, iZ_low),
                    temp_weight));
    }

    temp_weight = weight*weight_x_low * weight_y_low * weight_z_high;
    if(temp_weight > 0.0) {
        result.push_back(std::pair<Voxel_Shift_Subscripts, float>(
                    Voxel_Shift_Subscripts(iX_low, iY_low, iZ_high),
                    temp_weight));
    }

    temp_weight = weight*weight_x_low * weight_y_high * weight_z_low;
    if(temp_weight > 0.0) {
        result.push_back(std::pair<Voxel_Shift_Subscripts, float>(
                    Voxel_Shift_Subscripts(iX_low, iY_high, iZ_low),
                    temp_weight));
    }

    temp_weight = weight*weight_x_low * weight_y_high * weight_z_high;
    if(temp_weight > 0.0) {
        result.push_back(std::pair<Voxel_Shift_Subscripts, float>(
                    Voxel_Shift_Subscripts(iX_low, iY_high, iZ_high),
                    temp_weight));
    }

    temp_weight = weight*weight_x_high * weight_y_low * weight_z_low;
    if(temp_weight > 0.0) {
        result.push_back(std::pair<Voxel_Shift_Subscripts, float>(
                    Voxel_Shift_Subscripts(iX_high, iY_low, iZ_low),
                    temp_weight));
    }

    temp_weight = weight*weight_x_high * weight_y_low * weight_z_high;
    if(temp_weight > 0.0) {
        result.push_back(std::pair<Voxel_Shift_Subscripts, float>(
                    Voxel_Shift_Subscripts(iX_high, iY_low, iZ_high),
                    temp_weight));
    }

    temp_weight = weight*weight_x_high * weight_y_high * weight_z_low;
    if(temp_weight > 0.0) {
        result.push_back(std::pair<Voxel_Shift_Subscripts, float>(
                    Voxel_Shift_Subscripts(iX_high, iY_high, iZ_low),
                    temp_weight));
    }

    temp_weight = weight*weight_x_high * weight_y_high * weight_z_high;
    if(temp_weight > 0.0) {
        result.push_back(std::pair<Voxel_Shift_Subscripts, float>(
                    Voxel_Shift_Subscripts(iX_high, iY_high, iZ_high),
                    temp_weight));
    }
}


/**
 * Convert from subsrcipt shift to a in patient
 */
inline
Shift_in_Patient_Coord Geometry::convert_to_Shift_in_Patient_Coord(
    const Voxel_Shift_Subscripts rhs) const
{
    return Shift_in_Patient_Coord (
            rhs.iX_* voxel_dx_,
            rhs.iY_* voxel_dy_,
            rhs.iZ_* voxel_dz_);
}



/**
 * Convert from voxel subscripts to a point in patient
 */
inline
Point_in_Patient_Coord Geometry::convert_to_Point_in_Patient_Coord(
        const Voxel_Subscripts rhs) const
{
    Point_in_Patient_Coord temp;
    temp.x_ = rhs.iX_ * voxel_dx_ + 0.5f;
    temp.y_ = rhs.iY_ * voxel_dy_ + 0.5f;
    temp.z_ = rhs.iZ_ * voxel_dz_ + 0.5f;
    return temp;
}

/**
 * Convert from voxel subscripts to voxelNo
 */
inline
VoxelIndex Geometry::convert_to_VoxelIndex(
    const Voxel_Subscripts rhs) const
{
  return  VoxelIndex(nX_ * (nY_ * rhs.iZ_ + rhs.iY_) + rhs.iX_);
}

/**
 * Convert from voxelNo to voxel subscripts
 */
inline
Voxel_Subscripts Geometry::convert_to_Voxel_Subscripts(
    const VoxelIndex rhs) const
{
  Voxel_Subscripts temp;
  temp.iX_ = rhs.index_ % nX_;
  temp.iY_ = (rhs.index_ / nX_) % nY_;
  temp.iZ_ = (rhs.index_ / nX_) / nY_;
  return  temp;
}

/**
 * Convert from point in patient to a voxelNo
 */
inline
VoxelIndex Geometry::convert_to_VoxelIndex(const Point_in_Patient_Coord rhs) const
{
  Voxel_Subscripts temp = convert_to_Voxel_Subscripts(rhs);
  return convert_to_VoxelIndex(temp);
}

/**
 * Convert from a voxelNo to a point in patient
 */
inline
Point_in_Patient_Coord Geometry::convert_to_Point_in_Patient_Coord(const VoxelIndex rhs) const
{
  Voxel_Subscripts temp = convert_to_Voxel_Subscripts(rhs);
  return convert_to_Point_in_Patient_Coord(temp);
}

/**
 * Set voxel size for dose grid.
 *
 * @param dx Size of voxels in lateral direction in mm
 * @param dy Size of voxels in axial direction in mm
 * @param dz Size of voxels in anterior-posterior direction in mm
 */
inline
void Geometry::set_voxel_size(float dx, float dy, float dz)
{
  voxel_dx_ = dx;
  voxel_dy_ = dy;
  voxel_dz_ = dz;
}

/**
 * Set voxel size for dose grid.
 *
 * @param nX Number of voxels in lateral direction in mm
 * @param nY Number of voxels in axial direction in mm
 * @param nZ Number of voxels in anterior-posterior direction in mm
 */
inline
void Geometry::set_nVoxels(unsigned int nX, unsigned int nY, unsigned int nZ)
{
  nX_ = nX;
  nY_ = nY;
  nZ_ = nZ;
}

inline
void Geometry::set_isocenter_in_voxel_coordinates(float isox, float isoy, float isoz)
{
    iso_x_ = isox;
    iso_y_ = isoy;
    iso_z_ = isoz;
}

inline
bool Geometry::is_inside(const Voxel_Subscripts subscripts) const
{
    if (subscripts.iX_ < 0 ||
	subscripts.iY_ < 0 ||
	subscripts.iZ_ < 0 ||
	subscripts.iX_ >= (int)get_voxel_nx() ||
	subscripts.iY_ >= (int)get_voxel_ny() ||
	subscripts.iZ_ >= (int)get_voxel_nz() )
    {
	return false;
    }
    return true;
}

inline
bool Geometry::is_air(const unsigned int voxelNo) const
{
    return is_air_.at(voxelNo);
}

/**
 * check if voxel is air
 */
inline
bool Geometry::is_air(const VoxelIndex voxelNo) const
{
    return is_air_.at(voxelNo.index_);
}

/**
 * check if subscripts correspond to a non-air voxel
 */
inline
bool Geometry::is_valid_voxel(const Voxel_Subscripts subscripts) const
{
    // check if subscripts correspond to a grid point in the voxel grid
    if(!is_inside(subscripts)) {
	return false;
    }

    // check if voxel is air
    if(is_air(convert_to_VoxelIndex(subscripts))) {
	return false;
    }

    return true;
}

inline
void Geometry::set_gantry_angle(const std::vector<float> & gantry_angle)
{
  beam_gantry_angle_ = gantry_angle;
}

inline
void Geometry::set_table_angle(const std::vector<float> & table_angle)
{
  beam_table_angle_ = table_angle;
}

inline
void Geometry::set_collimator_angle(const std::vector<float> & collimator_angle)
{
  beam_collimator_angle_ = collimator_angle;
}

/**
 * Set beam startinf bixelNo
 */
inline
void Geometry::set_beam_starting_bixelNo(
    const std::vector<unsigned int> & beam_starting_bixelNo)
{
  assert(beam_starting_bixelNo.size() == nBeams_);
  beam_starting_bixelNo_ = beam_starting_bixelNo;
}

/**
 * Set number of bixels in beams
 */
inline
void Geometry::set_nBixels_in_beam(
    const std::vector<unsigned int> & nBixels_in_beam)
{
  assert(nBixels_in_beam.size() == nBeams_);
  nBixels_in_beam_ = nBixels_in_beam;
}

/**
 * Set the size for the bixels in each beam
 */
inline
void Geometry::set_bixel_grid_size(
    const std::vector<float> & bixel_grid_dx,
    const std::vector<float> & bixel_grid_dy)
{
  assert(bixel_grid_dx.size() == nBeams_);
  beam_bixel_dx_ = bixel_grid_dx;
  beam_bixel_dy_ = bixel_grid_dy;
}



/**
 * Set the beam number for each bixel.
 */
inline
void Geometry::set_bixel_beamNo(
    const std::vector<unsigned int> & bixel_beamNo)
{
  assert(bixel_beamNo.size() == nBixels_);
  bixel_beamNo_ = bixel_beamNo;
}

/**
 * Set position (x, y, energy) for each bixel.
 */
inline
void Geometry::set_bixel_position(
    const std::vector<Bixel_Position> & bixel_position)
{
  assert(bixel_position.size() == nBixels_);
  bixel_position_ = bixel_position;
}

/**
 * Set a bixel as physical.
 */
inline
void Geometry::set_physical_bixel(unsigned int bixelNo, bool value)
{
    bixel_is_physical_.at(bixelNo) = value;
}

/**
 * Set the SAD for each beam
 */
inline
void Geometry::set_beam_SAD(const std::vector<float> & beam_SAD )
{
  beam_sad_ = beam_SAD;
}

/**
 * Set the axial distance of IMPT spots for each beam
 */
inline
void Geometry::set_beam_dWEL(const std::vector<float> & beam_dWEL )
{
  beam_bixel_dwel_ = beam_dWEL;
}

/**
 * Get the number of beams
 */
inline
const unsigned int Geometry::get_nBeams() const
{
    return nBeams_;
}

/**
 * Get the number of bixels
 */
inline
const unsigned int Geometry::get_nBixels() const
{
    return nBixels_;
}

/**
 * Get number of bixels in a beam
 */
inline
unsigned int Geometry::get_nBixels_in_beam(unsigned int beamNo) const
{
    return nBixels_in_beam_.at(beamNo);
}

/**
 * Get number of bixels in a beam
 */
inline
std::vector<unsigned int> Geometry::get_nBixels_in_beam() const
{
    return nBixels_in_beam_;
}

/**
 * Get beam starting bixelNo for a beam
 */
inline
unsigned int Geometry::get_beam_starting_bixelNo(unsigned int beamNo) const
{
    return(beam_starting_bixelNo_.at(beamNo));
}

/**
 * Get beam starting bixelNo for a beam
 */
inline
std::vector<unsigned int> Geometry::get_beam_starting_bixelNo() const
{
    return beam_starting_bixelNo_;
}

/**
 * Get the gantry angle for each beam
 */
inline
const std::vector<float> & Geometry::get_gantry_angle() const
{
  return beam_gantry_angle_;
}

/**
 * Get the collimator angle for each beam
 */
inline
const std::vector<float> & Geometry::get_collimator_angle() const
{
  return beam_collimator_angle_;
}

/**
 * Get the table angle for each beam
 */
inline
const std::vector<float> & Geometry::get_table_angle() const
{
  return beam_table_angle_;
}

/**
 * Get the bixel grid size in the x direction for each beam
 */
inline
const std::vector<float> & Geometry::get_bixel_grid_dx() const
{
  return beam_bixel_dx_;
}

/**
 * Get the bixel grid size in the y direction for each beam
 */
inline
const std::vector<float> & Geometry::get_bixel_grid_dy() const
{
  return beam_bixel_dy_;
}

/**
 * get the axial distance of IMPT spots
 */
inline
float Geometry::get_beam_dWEL(unsigned int beamNo) const
{
  return beam_bixel_dwel_.at(beamNo);
}

/**
 * get bixel grid size in X
 */
inline
unsigned int Geometry::get_bixel_grid_nX(unsigned int beamNo) const
{
  return beam_bixel_nX_.at(beamNo);
}

/**
 * get bixel grid size in Y
 */
inline
unsigned int Geometry::get_bixel_grid_nY(unsigned int beamNo) const
{
  return beam_bixel_nY_.at(beamNo);
}


/**
 * Get the x axis position of the bixel with minimum x position in a given beam.
 */
inline
float Geometry::get_bixel_minX(unsigned int beamNo) const {
    return beam_bixel_minX_.at(beamNo);
}


/**
 * Get the x axis position of the bixel with maximum x position in a given beam.
 */
inline
float Geometry::get_bixel_maxX(unsigned int beamNo) const {
    return beam_bixel_maxX_.at(beamNo);
}


/**
 * Get the y axis position of the bixel with minimum y position in a given beam.
 */
inline
float Geometry::get_bixel_minY(unsigned int beamNo) const {
    return beam_bixel_minY_.at(beamNo);
}


/**
 * Get the y axis position of the bixel with maximum y position in a given beam.
 */
inline
float Geometry::get_bixel_maxY(unsigned int beamNo) const {
    return beam_bixel_maxY_.at(beamNo);
}


/**
 * Get the x axis position of the bixels with minimum x position in each beam.
 */
inline
const std::vector<float> &  Geometry::get_bixel_minX() const {
    return beam_bixel_minX_;
}


/**
 * Get the x axis position of the bixels with maximum x position in each beam.
 */
inline
const std::vector<float> &  Geometry::get_bixel_maxX() const {
    return beam_bixel_maxX_;
}


/**
 * Get the y axis position of the bixels with minimum y position in each beam.
 */
inline
const std::vector<float> &  Geometry::get_bixel_minY() const {
    return beam_bixel_minY_;
}


/**
 * Get the y axis position of the bixels with maximum y position in each beam.
 */
inline
const std::vector<float> &  Geometry::get_bixel_maxY() const {
    return beam_bixel_maxY_;
}


/**
 * Get the energy for a bixel
 */
inline
float Geometry::get_bixel_energy(unsigned int bixelNo) const
{
  return bixel_position_[bixelNo].E_;
}

/**
 * Get x position of a bixel
 */
inline
float Geometry::get_bixel_position_x(unsigned int bixelNo) const
{
  return bixel_position_[bixelNo].x_;
}

/**
 * Get y position of a bixel
 */
inline
float Geometry::get_bixel_position_y(unsigned int bixelNo) const
{
  return bixel_position_[bixelNo].y_;
}

/**
 * Get position of a bixel
 */
inline
Bixel_Position Geometry::get_bixel_position(unsigned int bixelNo) const
{
  return bixel_position_[bixelNo];
}

/**
 * Get subscripts of a bixel
 */
inline
Bixel_Subscripts Geometry::get_bixel_subscript(unsigned int bixelNo) const
{
  return bixel_subscripts_[bixelNo];
}

/**
 * Get beam number for a bixel
 */
inline
unsigned int Geometry::get_beamNo(unsigned int bixelNo) const
{
  return bixel_beamNo_[bixelNo];
}

/**
 * Get isocenter location for specified beam
 */
inline
Point_in_Patient_Coord Geometry::get_isocenter(unsigned int BeamNo) const
{
    return isocenter_[BeamNo];
}

/**
 * find out if bixel is physical or only virtual
 */
inline
bool Geometry::is_physical_bixel(unsigned int bixelNo) const
{
    return bixel_is_physical_[bixelNo];
}

/**
 * find out if subscripts correspond to a valid bixel grid point
 */
inline
bool Geometry::is_inside(Bixel_Subscripts subscripts, unsigned int beamNo) const
{
    // check whether it is inside the grid
    if (subscripts.iX_ < 0 ||
	subscripts.iY_ < 0 ||
	subscripts.iE_ < 0 ||
	subscripts.iX_ >= (int) (1+2*beam_bixel_nX_[beamNo]) ||
	subscripts.iY_ >= (int) (1+2*beam_bixel_nY_[beamNo]) ||
	subscripts.iE_ >= (int) beam_bixel_nE_[beamNo] )
    {
	return false;
    }

    return true;
}

/**
 * find out if subscripts correspond to a valid bixel grid point
 * and the grid point hosts a bixel
 */
inline
bool Geometry::is_valid_bixel(Bixel_Subscripts subscripts, unsigned int beamNo) const
{
    // check whether it is inside the grid
    if(is_inside(subscripts,beamNo)) {
      // check whether there is a bixel
      return beam_grid_is_bixel_[beamNo][(convert_to_BixelIndex(subscripts,beamNo)).index_];
    }
    else {
      return false;
    }
}

/**
 * throw exception if bixel subscripts not valid
 */
inline
void Geometry::assert_valid_bixel(Bixel_Subscripts subscripts, unsigned int beamNo) const
{
  if(!is_valid_bixel(subscripts,beamNo)) {
    cout << "bixel " << subscripts << " in beam " << beamNo;
    cout << " corresponds to invalid bixel subscripts!" << endl;
    throw(std::logic_error("invalid bixel subscript"));
  }
}

/**
 * throw exception if bixel subscripts are outside bixel grid
 */
inline
void Geometry::assert_bixel_inside(Bixel_Subscripts subscripts, unsigned int beamNo) const
{
  if(!is_inside(subscripts,beamNo)) {
    cout << "bixel " << subscripts << " in beam " << beamNo;
    cout << " corresponds to invalid bixel subscripts!" << endl;
    throw(std::logic_error("invalid bixel subscript"));
  }
}

/**
 * get total number of points in the bixel grid
 */
inline
unsigned int Geometry::get_beam_bixel_nPoints(unsigned int beamNo) const
{
    return beam_bixel_nPoints_.at(beamNo);
}

/**
 * Convert from bixel subscripts to bixel index
 */
inline
BixelIndex Geometry::convert_to_BixelIndex(
    const Bixel_Subscripts rhs, const unsigned int beamNo) const
{
  return  BixelIndex((1+2*beam_bixel_nX_[beamNo]) *
		     ((1+2*beam_bixel_nY_[beamNo]) * rhs.iE_ + rhs.iY_) + rhs.iX_);
}

/**
 * Convert from bixel index to bixel subscripts
 */
inline
Bixel_Subscripts Geometry::convert_to_Bixel_Subscripts(
    const BixelIndex rhs, const unsigned int beamNo) const
{
  Bixel_Subscripts temp;
  temp.iX_ = rhs.index_ % (1+2*beam_bixel_nX_[beamNo]);
  temp.iY_ = (rhs.index_ / (1+2*beam_bixel_nX_[beamNo])) % (1+2*beam_bixel_nY_[beamNo]);
  temp.iE_ = (rhs.index_ / (1+2*beam_bixel_nX_[beamNo])) / (1+2*beam_bixel_nY_[beamNo]);
  return  temp;
}

/**
 * Convert from bixel number to bixel index
 */
inline
BixelIndex Geometry::convert_to_BixelIndex(
    const unsigned int bixelNo) const
{
    return BixelIndex(bixel_index_[bixelNo]);
}

/**
 * Convert from bixel number to bixel subscripts
 */
inline
Bixel_Subscripts Geometry::convert_to_Bixel_Subscripts(
    const unsigned int bixelNo) const
{
    BixelIndex temp = convert_to_BixelIndex(bixelNo);
    return convert_to_Bixel_Subscripts(temp, bixel_beamNo_[bixelNo]);
}

/**
 * Convert from bixel index to bixel number
 */
inline
unsigned int Geometry::get_bixelNo(
    const BixelIndex rhs, const unsigned int beamNo) const
{
    return beam_grid_bixelNo_[beamNo][rhs.index_];
}

/**
 * Convert from bixel subscripts to bixel number
 */
inline
unsigned int Geometry::get_bixelNo(
    const Bixel_Subscripts rhs, const unsigned int beamNo) const
{
    BixelIndex temp = convert_to_BixelIndex(rhs,beamNo);
    return beam_grid_bixelNo_[beamNo][temp.index_];
}

/**
 * Convert the lateral component of a shift in Collimator coordinates
 * to bixel shift subscripts
 */
inline
Bixel_Shift_Subscripts Geometry::convert_to_Lateral_Bixel_Shift_Subscripts(
    const Shift_in_Collimator_Coord rhs, const unsigned int beamNo) const
{
    Bixel_Shift_Subscripts temp;
    if(rhs.x_>0) {
	temp.iX_ = (int) (rhs.x_ / beam_bixel_dx_[beamNo] + 0.5);
    }
    else {
	temp.iX_ = (int) (rhs.x_ / beam_bixel_dx_[beamNo] - 0.5);
    }
    if(rhs.y_>0) {
	temp.iY_ = (int) (rhs.y_ / beam_bixel_dy_[beamNo] + 0.5);
    }
    else {
	temp.iY_ = (int) (rhs.y_ / beam_bixel_dy_[beamNo] - 0.5);
    }
    // apply no shift in energy layer
    temp.iE_ = 0;

    return temp;
}

/**
 * Convert a range shift (in WER) to bixel shift subscripts
 */
inline
Bixel_Shift_Subscripts Geometry::convert_to_Axial_Bixel_Shift_Subscripts(
    const float range_shift, const unsigned int beamNo) const
{
    Bixel_Shift_Subscripts temp;
    temp.iX_ = 0;
    temp.iY_ = 0;

    if(range_shift>0) {
	temp.iE_ = (int) (range_shift / beam_bixel_dwel_[beamNo] + 0.5);
    }
    else {
	temp.iE_ = (int) (range_shift / beam_bixel_dwel_[beamNo] - 0.5);
    }

    return temp;
}

/**
 * Compare two floats using an absolute tolerance
 */
inline
bool Geometry::comp_float_abs(float a, float b, float epsilon) const
{
    if(fabs(a-b) < epsilon) {
	return true;
    }
    return false;
}


/**
 * Convert from CT voxel subscript to a point in CT coordinates
 */
inline
Point_in_CT_Coord CTscanAttributes::convert_to_Point_in_CT_Coord(const CT_Voxel_Subscripts rhs) const
{
    Point_in_CT_Coord temp;
    temp.x_ = rhs.iX_ * pixel_size_ + 0.5f;
    temp.y_ = rhs.iY_ * pixel_size_ + 0.5f;
    temp.z_ = rhs.iZ_ * slice_distance_ + 0.5f;
    return temp;
}

/**
 * Convert from voxelNo to voxel subscripts
 */
inline
CT_Voxel_Subscripts CTscanAttributes::convert_to_CT_Voxel_Subscripts(const CTVoxelIndex rhs) const
{
    CT_Voxel_Subscripts temp;
    temp.iX_ = rhs.index_ % nDimension_;
    temp.iY_ = (rhs.index_ / nDimension_) % nDimension_;
    temp.iZ_ = (rhs.index_ / nDimension_) / nDimension_;
    return  temp;
}

/**
 * Convert from CT voxel index to a point in CT coordinates
 */
inline
Point_in_CT_Coord CTscanAttributes::convert_to_Point_in_CT_Coord(const CTVoxelIndex rhs) const
{
    return( convert_to_Point_in_CT_Coord(
		convert_to_CT_Voxel_Subscripts(rhs)));
}

#endif
