/**
 * @file Optimization.cpp
 * Optimization Class implementation.
 *
 * $Id: Optimization.cpp,v 1.91 2008/06/03 16:23:34 bmartin Exp $
 *
 * @version 0.9.4
 * <pre>
 * ver 0.9.4   BM      Jul 15, 2004    Many changes.
 * ver 0.9.2   AT,TB   Apr 05, 2004    negative beamlet weights are now
 *                                       initialized just before dose_calc
 *                                       (to allow for "dij averaging")
 * ver 0.9.0   TB      Mar 02, 2004    moved optimization functions from
 *                                       DoseVector to optimization
 * ver 0.8.10  AT      Feb 27, 2004    minor fixes, changes, cleanup
 * ver 0.8.8   TB      Feb 15, 2004    added objective function history
 * ver 0.8.7   AT      Feb 04, 2004    added deform_Dij
 * ver 0.5     TB      Jan 04, 2004    creation
 * TB: Thomas Bortfeld
 * BM: Ben Martin
 * Massachusetts General Hospital, Department of Radiation Oncology
 * </pre>
 */

#include <fstream>
#include <iostream>
using std::cerr;
#include <vector>
using std::vector;
#include <queue>
using std::priority_queue;
#include <set>
using std::set;
#include <cmath>
#include <limits>
#include <algorithm>
using std::fill;

#include "Optimization.hpp"
#include "MosekInterface.hpp"
#include "OptppInterface.hpp"

/**
 * Constructor: initialize variables.
 *
 * @param thisplan The plan to be optimized.
 */
Optimization::Optimization(Plan* thisplan, string log_file_name, Optimization::options options)
  : the_plan_(thisplan), 
	objective_(0.0f), multi_objective_(), 
	options_(options), log_(log_file_name), timer_(), 
	min_sampling_fractions_(), initial_sampling_fractions_(), 
	last_ssvo_(), smoothed_ssvo_(),
	bwf_file_counter_(0), dose_file_counter_(0), dvh_file_counter_(0),
	daoSeq_(thisplan) {

    // Reserve space in arrays
    multi_objective_.reserve(the_plan_->get_nVois());

}


/**
 * Destructor.
 */
//Optimization::~Optimization() { }





/**
 * Pull current events off a priority queue
 *
 * @param p_queue   The queue to read events from
 * @param trigger   The value to compare with the event trigger
 * @param event_set The set that new events will be added to
 * @returns         Number of events harvested
 */
int Optimization::harvest_events( priority_queue<Optimization::event> & p_queue, double trigger,
    set<OptimizationEventType> & event_set ) const {
    int count = 0;

    // Get current actions from p_queue
    while (!p_queue.empty() && p_queue.top().trigger_ <= trigger) {
        // The top action on the queue is current so read it off
        Optimization::event tmp_event = p_queue.top();
        p_queue.pop();

        // Add event to event_set
        event_set.insert(tmp_event.type_);
        count++;

        // Add new event to queue in case of recurrence
        if (tmp_event.recurrence_ > 0) {
            while (tmp_event.trigger_ <= trigger) {
                tmp_event.trigger_ += tmp_event.recurrence_;
            }
            p_queue.push(tmp_event);
        }
    }
    return count;
}




/**
 * Call the aperture sequencer class to translate the bixel vector into a set of aperture parameters.
 */
void Optimization::call_aperture_sequencer() {
  /*
    std::cout << "Sequencing bixel vector ... " << std::endl;
    // Make sure sequencer options are up to date
    daoSeq_.set_options( options_.sequencer_options_ );
    // Start the sequencer
    daoSeq_.use_sequencer();
    // Clear gradient and resize for new parameter vector
    gradient_ = ParameterVectorDirection( *the_plan_->get_parameter_vector() );

    std::cout << "Sequencing bixel vector ... done" << std::endl;
  */
};


/*
 * Optimization::options functions
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 */

/**
 * Print options to stream
 */
std::ostream& operator<< (std::ostream& o, const Optimization::options& opt) {
    for ( unsigned int opT=0; opT<opt.optimization_type_.size(); opT++ ) {
        o << "optimization_type_[" << opT << "] = " << opt.optimization_type_[ opT ] << "\n";
    }
    o << "out_file_prefix_ = " << opt.out_file_prefix_ << "\n";
    o << "step_events_ = " << opt.step_events_ << "\n";
    o << "time_events_ = " << opt.time_events_ << "\n";
    o << "final_events_ = " << opt.final_events_ << "\n";
    o << "step_size_multiplier_ = " << opt.step_size_multiplier_ << "\n";
    o << "use_hessian_ = " << opt.use_hessian_ << "\n";
    o << "use_alternate_linesearch_ = "
        << opt.use_alternate_linesearch_ << "\n";
    o << "verbosity_ = " << opt.verbosity_ << "\n";
    o << "linesearch_steps_ = " << opt.linesearch_steps_ << "\n";
    o << "use_optimal_steps_ = " << opt.use_optimal_steps_ << "\n";
    o << "use_voxel_sampling_ = " << opt.use_voxel_sampling_ << "\n";
    o << "nScenario_samples_per_step_ = " << opt.nScenario_samples_per_step_ << "\n";
    o << "calculate_true_objective_ = "
        << opt.calculate_true_objective_ << "\n";
    o << "use_diminishing_step_sizes_ = "
        << opt.use_diminishing_step_sizes_ << "\n";
    o << "momentum_gradient_smoothing_exponent_ = "
        << opt.momentum_gradient_smoothing_exponent_ << "\n";
    o << "smd_smoothing_coefficient_ = "
        << opt.smd_smoothing_coefficient_ << "\n";
    o << "smd_meta_step_size_ = " << opt.smd_meta_step_size_ << "\n";
    o << "delta_bar_delta_options_ = {\n"
        << opt.delta_bar_delta_options_ << "}\n";
    o << "min_steps_ = " << opt.min_steps_ << "\n";
    o << "max_steps_ = " << opt.max_steps_ << "\n";
    o << "max_time_ = " << opt.max_time_ << "\n";
    o << "stopping_fraction_ = " << opt.stopping_fraction_ << "\n";
    o << "perform direct aperture optimization = " << opt.daoOpt_ << "\n";
    if ( opt.daoOpt_ ) o << opt.sequencer_options_;
    o << "use_adaptive_sampling_ = " << opt.use_adaptive_sampling_ << "\n";
    o << "as_update_period_ = " << opt.as_update_period_ << "\n";
    o << "as_sampling_fraction_ = " << opt.as_sampling_fraction_ << "\n";
    o << "as_min_sampling_fraction_ = " << opt.as_min_sampling_fraction_ << "\n";
    o << "as_smoothing_factor_ = " << opt.as_smoothing_factor_ << "\n";
    o << "calculate_constrained_gradient_ = "
        << opt.calculate_constrained_gradient_ << "\n";
    o << "calculate_feasible_gradient_ = "
        << opt.calculate_feasible_gradient_ << "\n";
    return o;
}


/**
 * Print vector of OptimizationEventType to stream
 */
std::ostream& operator<< ( std::ostream& o, const vector<OptimizationEventType> & rhs ) {
    o << "{";
    bool add_comma = false;
    vector<OptimizationEventType>::const_iterator theIterator;
    for ( theIterator = rhs.begin(); theIterator != rhs.end(); theIterator++ ) {
        if (add_comma) {
            o << ", ";
        } else {
            add_comma = true;
        }
        o << *theIterator;
    }
    o << "}";
    return o;
}


/**
 * Print set of OptimizationEventType to stream
 */
std::ostream& operator<< ( std::ostream& o, const set<OptimizationEventType> & rhs ) {
    o << "(";
    bool add_comma = false;
    set<OptimizationEventType>::const_iterator theIterator;
    for ( theIterator = rhs.begin(); theIterator != rhs.end(); theIterator++ ) {
        if (add_comma) {
            o << ", ";
        } else {
            add_comma = true;
        }
        o << *theIterator;
    }
    o << ")";
    return o;
}

//
// Optimization::event functions
// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
//

/**
 * Input from a stream
 */
std::istream& operator>> (std::istream& i, Optimization::event& rhs) {
    i >> rhs.type_;

    // Discard comma if needed
    if (i.peek() == ',') {
        i.ignore(1);
    }

    i >> rhs.trigger_;

    // Discard comma if needed
    if (i.peek() == ',') {
        i.ignore(1);
    }

    i >> rhs.recurrence_;

    return i;
}


/**
 * Write description of event out to stream
 */
std::ostream& operator<< (std::ostream& o, const Optimization::event& rhs) {
    return o << rhs.type_ << " trigger: " << rhs.trigger_ << " recurrence: " << rhs.recurrence_;
}


/**
 * Print vector of Optimization::event to stream
 */
std::ostream& operator<< ( std::ostream& o, const vector<Optimization::event> & rhs ) {
    o << "{";
    bool add_comma = false;
    vector<Optimization::event>::const_iterator theIterator;
    for ( theIterator = rhs.begin(); theIterator != rhs.end(); theIterator++ ) {
        if (add_comma) {
            o << ", ";
        } else {
            add_comma = true;
        }
        o << *theIterator;
    }
    o << "}";
    return o;
}

//
// OptimizationEventType functions
// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
//

/// Read OptimizationEventType from a stream
std::istream& operator>> (std::istream& i, OptimizationEventType & event_type) {
    // Read the first word from stream
    string word = "";
    char c;
    while (i.peek() != EOF && i >> c) {
        if ((c >= 'A' && c <= 'Z') || c == '_') {
            word += c;
        }  else {
            // Invalid character, so put it back on the stream
            i.putback(c);
            break;
        }
    }

    // Check if the strings from any of the events matches
    for (OptimizationEventType iEvent = start_OptimizationEventType; iEvent != end_OptimizationEventType; ++iEvent) {
        std::ostringstream s;
        s << iEvent;
        if (s.str() == word) {
            event_type = iEvent;
            return i;
        }
    }

    // We didn't find a match, so throw an error
    string error_message = "Failed reading OptimizationEventType: " + word;
    throw std::ios_base::failure(error_message.c_str());
}


/*
 * Optimization::log functions
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^
 */


/**
 * Constructor.  Opens file and writes header.
 */
Optimization::log::log(string file_name, int initial_stepNo)
        : out_file_(file_name.c_str()),  stepNo_(initial_stepNo),  current_step_attributes_(),
        current_step_contents_(),  opt4d_hist_open_(false),  header_open_(false),  optimization_open_(false) {

  cout << endl << " constructor Optimization::log::log() called" << endl << endl;

    // Write file header
    out_file_ << "<?xml version=\"1.0\"?>\n";

    // Write comment telling that this was made by opt4D
    out_file_ << "<!-- Optimization history written by opt4D -->\n";

    // Write root element start
    out_file_ << "<OPT4D_HIST>\n";
    opt4d_hist_open_ = true;
}


/**
 * Destructor.  Finishes writing file.
 */
Optimization::log::~log() {
    //
    // Close open tags to make sure file is valid
    //

  cout << endl << " destructor Optimization::log::~log() called" << endl << endl;

    try {
        if (header_open_) {
            // Write optimization element end
            out_file_ << "</header>\n";
            header_open_ = false;
        }

        if (optimization_open_) {
            // Write optimization element end
            out_file_ << "</optimization>\n";
            optimization_open_ = false;
        }

        if (opt4d_hist_open_) {
            // Write root element end
            out_file_ << "</OPT4D_HIST>\n";
            opt4d_hist_open_ = false;
        }
    } catch (...) {
        // Do nothing, since we're in a destructor
        std::cerr << "Warning: exception in Optimization::log destructor.\n";
    }

    // Do nothing to close file, let the destructor do that
}


/**
 * Finish the step and supply the time
 */
void Optimization::log::finish_step(double time) {
    // Store time and stepNo
    current_step_attributes_ << " time=\"" << time << "\"";
    current_step_attributes_ << " stepNo=\"" << stepNo_ << "\"";

    // Make sure we are in an optimization
    ensure_optimization_open();

    // Write out step
    out_file_ << "<step " << current_step_attributes_.str();
    if (current_step_contents_.str().empty()) {
        // Simple element so close
        out_file_ << " />\n";
    } else {
        // step contains other xml elements
        out_file_ << ">\n" << current_step_contents_.str() << "\n</step>\n";
    }

    // Clear the current step streams and empty them
    current_step_attributes_.clear();
    current_step_attributes_.str("");
    current_step_contents_.clear();
    current_step_contents_.str("");

    // Increment to the next step
    stepNo_++;
}


/**
 * Make sure we are in an optimization block
 */
void Optimization::log::ensure_optimization_open() {
    // Make sure we are in an optimization
    if (!optimization_open_) {
        if (header_open_) {
            out_file_ << "</header>\n";
            header_open_ = false;
        }
        out_file_ << "<optimization>\n";
        optimization_open_ = true;
    }
}


/**
 * Reset history to empty
 */
void Optimization::log::new_optimization(int stepNo) {
    // Close header if needed
    if (header_open_) {
        out_file_ << "</header>\n";
        header_open_ = false;
    }

    // Check if we are already in an optimization and close if needed
    if (optimization_open_) {
        out_file_ << "</optimization>\n";
        optimization_open_ = false;
    }

    // Start new optimization element
    out_file_ << "<optimization>\n";
    optimization_open_ = true;

    // Update to the new stepNo
    stepNo_ = stepNo;

    // Clear the current step streams and empty them
    current_step_attributes_.clear();
    current_step_attributes_.str("");
    current_step_contents_.clear();
    current_step_contents_.str("");
}




/**
 * Initialize adaptive sampling.
 *
 * Set a uniform sampling rate for all objectives.
 */
void Optimization::init_adaptive_sampling() {

    unsigned int nObjectives = the_plan_->get_total_n_objectives();

    // Verify that this is the first time this function has been run
    if (!min_sampling_fractions_.empty()) {
        throw std::logic_error( "Attempted to initialize adaptive sampling twice without resetting");
    }

    // Read current sampling fraction
    initial_sampling_fractions_ = the_plan_->get_objective_sampling_fractions();

    // Check if minimum sampling fraction is supplied
    if (options_.as_min_sampling_fraction_ > 0) {
        min_sampling_fractions_.resize(nObjectives);
        fill(min_sampling_fractions_.begin(), min_sampling_fractions_.end(), options_.as_min_sampling_fraction_);
    } else {
        // Use current sampling fraction to be the minimum
        min_sampling_fractions_ = initial_sampling_fractions_;
    }

    // Initialize smoothed_ssvo_ to empty
    smoothed_ssvo_.resize(nObjectives);
    fill(smoothed_ssvo_.begin(), smoothed_ssvo_.end(), 0);

    // Set the sampling rate to be uniform since we don't have any more
    // information.
    vector<float> sampling_fractions(nObjectives, options_.as_sampling_fraction_);

    // Apply sampling fractions
    the_plan_->set_objective_sampling_fractions(sampling_fractions);

    std::cout << "Initializing adaptive sampling.  Sampling fraction: " << options_.as_sampling_fraction_ << "\n";
}


/**
 * Adjust the sampling rate for each objective.  The overall sampling fraction
 * will be within +/- 5% of the desired sampling fraction.
 *
 * Works by binary search, adjusting the parameter lambda in the following formula:
 *
 * Uses the formula that the optimal sampling rate
 * p* = (lambda / (1-lambda) * (mean_squared_voxel_objective))^(1/2)
 *
 * If p* is less than the minimum or more than one, it will be corrected.
 *
 */
void Optimization::adjust_sampling_rate() {

    unsigned int nObjectives = the_plan_->get_total_n_objectives();

    // Find out if this is the first time this function has been run
    if (min_sampling_fractions_.size() != nObjectives) {
        // This is the first time the function has been run, so set a uniform
        // sampling rate for all objectives.
        init_adaptive_sampling();
        return;
    }

    // Read the estimated sum of squared voxel-objective for each objective
    assert(smoothed_ssvo_.size() == nObjectives);
    for (unsigned int ob = 0; ob < nObjectives; ob++) {
        smoothed_ssvo_[ob] *= options_.as_smoothing_factor_;
        smoothed_ssvo_[ob] += (1-options_.as_smoothing_factor_) * last_ssvo_[ob];
    }

    // Read number of voxels for each objective
	vector<unsigned int> nVoxels = the_plan_->get_objective_nVoxels();

    // Read whether each objective supports sampling
    vector<bool> uses_sampling = the_plan_->get_objective_supports_sampling();

    unsigned int total_voxels = 0;
    for (unsigned int vo = 0; vo < nVoxels.size(); vo++) {
        if (uses_sampling[vo]) {
            total_voxels += nVoxels[vo];
        }
    }

    // Bounds on mu
    double mu_upper_bound = 0;
    double mu_lower_bound = -1;

    // Calculate minimum and maximum mu for each objective with nVoxels > 0
    vector<double> mu_min(nObjectives,0);
    vector<double> mu_max(nObjectives,0);
    vector<double> mu_boundaries;
    mu_boundaries.reserve(nObjectives*2);

    for (unsigned int ob = 0; ob < nObjectives; ob++) {
        if (nVoxels[ob] == 0 || !uses_sampling[ob]) {
            continue;
        }
        mu_min[ob] = smoothed_ssvo_[ob] / nVoxels[ob];
        mu_max[ob] = mu_min[ob] / ( min_sampling_fractions_[ob] * min_sampling_fractions_[ob] );
        mu_boundaries.push_back( mu_min[ob] );
        mu_boundaries.push_back( mu_max[ob] );
        if (mu_lower_bound < 0 || mu_min[ob] < mu_lower_bound) {
            mu_lower_bound = mu_min[ob];
        } if (mu_max[ob] > mu_upper_bound) {
            mu_upper_bound = mu_max[ob];
        }
    }

    // std::cout << "mu_upper_bound= " << mu_upper_bound << " "
    //   << "mu_lower_bound= " << mu_lower_bound << "\n";

    float desired_nSamples = total_voxels * options_.as_sampling_fraction_;

    // Narrow the bounds to only one active interval
    for ( unsigned int ii = 0; ii < mu_boundaries.size(); ii++ ) {
        // Check if mu_boundaries[i] is an upper or lower bound
        double mu = mu_boundaries[ii];
        if ( mu < mu_upper_bound && mu > mu_lower_bound ) {
            // Find out sampling rate at mu
            float nSamples = 0;
            for ( unsigned int ob = 0; ob < nObjectives; ob++ ) {
                if (nVoxels[ob] == 0 || !uses_sampling[ob]) {
                    continue;
                }
                if (mu <= mu_min[ob]) {
                    nSamples += nVoxels[ob];
                }
                else if (mu >= mu_max[ob]) {
                    nSamples += min_sampling_fractions_[ob] * nVoxels[ob];
                } else {
                    nSamples += sqrt(smoothed_ssvo_[ob] / (nVoxels[ob] * mu)) * nVoxels[ob];
                }
            }
            // std::cout << "mu: " << mu << " fraction: " << nSamples / total_voxels
            //   << " desired nSamples: " << desired_nSamples / total_voxels << "\n";

            if (nSamples < desired_nSamples) {
                mu_upper_bound = mu;
            } else {
                mu_lower_bound = mu;
            }

            // std::cout << "mu_upper_bound= " << mu_upper_bound << " "
            //   << "mu_lower_bound= " << mu_lower_bound << "\n";
        }
    }

    // Find the optimal mu for the current active set
    double guess_mu = (mu_upper_bound + mu_lower_bound)/2;
    double optimal_mu_nominator = 0;
    double optimal_mu_denominator = desired_nSamples;
    for ( unsigned int ob = 0; ob < nObjectives; ob++ ) {
        if ( nVoxels[ob] == 0 || !uses_sampling[ob] ) {
            continue;
        }
        // Figure out which set k is in
        if (guess_mu >= mu_max[ob]) {
            // p_k is pinned to the minimum
            optimal_mu_denominator -= min_sampling_fractions_[ob] * nVoxels[ob];
        } else if (guess_mu <= mu_min[ob]) {
            // p_k is pinned to 1
            optimal_mu_denominator -= nVoxels[ob];
        } else {
            // p_k is in the active set
            optimal_mu_nominator += sqrt(smoothed_ssvo_[ob] * nVoxels[ob]);
        }
    }
    double optimal_mu;
    bool no_active_set = false;
    if (optimal_mu_nominator > 0) {
        optimal_mu = optimal_mu_nominator / optimal_mu_denominator;
        optimal_mu *= optimal_mu;
    } else {
        // No objectives are in the active set, so the calculation of optimal_mu
        // is not accurate.  Keep the old one.
        no_active_set = true;
        optimal_mu = guess_mu;
    }


    // Find the sampling rate for each objective, given mu
    vector<float> sampling_fractions(nObjectives,1);
    for ( unsigned int ob = 0; ob < nObjectives; ob++ ) {
        if (nVoxels[ob] == 0 || !uses_sampling[ob]) {
            continue;
        }
        // Figure out which set k is in
        if (optimal_mu >= mu_max[ob]) {
            // p_k is pinned to the minimum
            sampling_fractions[ob] = min_sampling_fractions_[ob];
        } else if (optimal_mu <= mu_min[ob]) {
            // p_k is pinned to 1
            sampling_fractions[ob] = 1;
        } else {
            // p_K is in the active set
            sampling_fractions[ob] = sqrt(smoothed_ssvo_[ob] / (nVoxels[ob] * optimal_mu));
        }
    }

    if (no_active_set) {
        // Find out how many voxels are in the objectives that are minimally
        // sampled
        float expected_nSamples = 0;
        float voxel_count = 0;
        for ( unsigned int ob = 0; ob < nObjectives; ob++ ) {
            if (uses_sampling[ob]) {
                expected_nSamples += sampling_fractions[ob] * nVoxels[ob];
                if (sampling_fractions[ob] < 1) {
                    voxel_count += nVoxels[ob];
                }
            }
        }
        float fraction_to_add = (desired_nSamples-expected_nSamples)/voxel_count;
        for ( unsigned int ob = 0; ob < nObjectives; ob++ ) {
            if (sampling_fractions[ob] < 1 && uses_sampling[ob]) {
                sampling_fractions[ob] += fraction_to_add;
            }
        }
    }

    // Apply sampling fractions
    the_plan_->set_objective_sampling_fractions(sampling_fractions);


    // Figure out how many samples to expect
    // float expected_nSamples = 0;
    // for(unsigned int k = 0; k < nObjectives; k++) {
    // expected_nSamples += sampling_fractions[k] * nVoxels[k];
    // }
    // std::cout << "Sampling adaptation mu: " << optimal_mu
    // << " Fraction Sampled: " << expected_nSamples / total_voxels <<  "\n";

    // std::cout << "k\tp_k\tmu_min\tmu_max\n" ;
    // for(unsigned int k = 0; k < nObjectives ; k++) {
    //   std::cout << (k+1) << "\t" << sampling_fractions[k] << "\t" << mu_min[k]
    //   << "\t" << mu_max[k] << "\n";
    // }
    // std::cout << "\n";
}


/**
 * Clears all adaptive sampling data
 */
void Optimization::reset_adaptive_sampling() {
    unsigned int nObjectives = the_plan_->get_total_n_objectives();

    // Restore initial sampling fractino if initial_sampling_fraction_ has the
    // same number of objectives as the plan
    if (initial_sampling_fractions_.size() == nObjectives) {
        the_plan_->set_objective_sampling_fractions( initial_sampling_fractions_ );
    }

    // Empty out the adaptive sampling related vectors
    initial_sampling_fractions_.resize(0);
    min_sampling_fractions_.resize(0);
    smoothed_ssvo_.resize(0);
}

