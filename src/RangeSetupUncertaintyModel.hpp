#ifndef RANGESETUPUNCERTAINTYMODEL_HPP
#define RANGESETUPUNCERTAINTYMODEL_HPP

#include <vector>
using std::vector;
#include <string>
using std::string;

// Predefine class before including others
class RangeSetupUncertaintyModel;


#include "Geometry.hpp"
#include "DoseDeliveryModel.hpp"
#include "RangeUncertaintyModel.hpp"
#include "SetupUncertaintyModel.hpp"
#include "GaussianSetupError.hpp"



// ---------------------------------------------------------------------
// Range and Setup uncertainty
// ---------------------------------------------------------------------


/**
 * Uncertainty model which handles both range and setup uncertainty
 */
class RangeSetupUncertaintyModel : public ShiftBasedErrorModel
{
  public:

    RangeSetupUncertaintyModel(
            DoseInfluenceMatrix &Dij, 
            const Geometry *Geom, 
            const DoseDeliveryModel::options *options);

    /// returns type of uncertainty model
    virtual std::string get_uncertainty_model() const {
	return("RangeSetupUncertaintyModel");}

    /// generate a random sample scenario 
    virtual void generate_random_scenario(
            RandomScenario &random_scenario) const;

    /// store a dose evaluation scenario
    bool set_dose_evaluation_scenario(map<string,string> scenario_map);

   /*
    /// calculate voxel dose for a scenario
    virtual float calculate_voxel_dose(
            unsigned int voxelNo,	
            const BixelVector &bixel_grid,                  
            DoseInfluenceMatrix &dij,
            const RandomScenario &random_scenario) const;
    */

    /*
    /// calculate gradient contribution of the voxel for a scenario
    virtual void calculate_voxel_dose_gradient(
            unsigned int voxelNo,	
            DoseInfluenceMatrix &dij,
            const RandomScenario &random_scenario,
            BixelVectorDirection &gradient,
            float gradient_multiplier) const;
    */

  private:

    /// Range uncertainty model
    std::auto_ptr<GeneralAuxiliaryRangeModel> the_range_uncertainty_;

    /// Setup uncertainty model
    std::auto_ptr<SetupUncertaintyModel> the_setup_uncertainty_;

    /// Put shifts in to RandomScenario
    void set_shifts_in_scenario(
            RandomScenario &random_scenario,
            vector<Shift_in_Patient_Coord> daily_shifts) const;

    /// transform setup shift to effective voxel shift
    Shift_in_Patient_Coord transform_setup_error_to_voxel_shift(
	Shift_in_Patient_Coord setup_shift, unsigned int iBeam) const;

    /// correct for shift of the static dose cloud in beam direction 
    bool use_shift_correction_;

    /// use randomized rounding for every voxel
    bool use_randomized_rounding_;

    /// use linear interpolation for shifts
    bool use_linear_interpolation_;

    /// switch on merging of matching shifts
    bool merge_shifts_;

    /// Setup error model:
    GaussianSetupError setup_model_;

    /// normal vector on the patient surface for each beam direction 
    vector<Shift_in_Patient_Coord> surface_normal_;

};


#endif
