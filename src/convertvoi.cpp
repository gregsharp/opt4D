/**
 * @file convertvoi.cpp
 * The main file for convertvoi.
 *
 * converts voxel numbering from CT coordinate system to opt4D system
 *
 */

/**
 * \par Welcome to convertvoi
 *
 * convertdij is a utility to perform convert a voi matrix in CT coordinate system to opt4D system. 
 * This flips the y axis and then interchanges y and z axis.  Type convertvoi --help for usage information.
 */

#include <iostream>
using std::cout;
using std::cerr;
using std::endl;

#include <iomanip>
#include <exception>
#include <fstream>
#include <limits>
#include <tclap/CmdLine.h>
#include <string>
#include <stdexcept>

using std::string;

#include "DijFileParser.hpp"
#include "Geometry.hpp"
#include "DoseVector.hpp"

int convert_voxel_index(int voxelNo, int nx, int ny, int nz); 

/**
 * Main function
 */
int main(int argc, char* argv[])
{

    // Wrap everything in a try block to catch TCLAP exceptions.
    try {

        /*
         * Defining the command line syntax
         * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
         *
         * We define the command line object, and add various switches and 
         * value arguments to it.  The CmdLine object parses the argv array 
         * based on the Arg objects that it contains.
         */

        // Define the command line object and insert a message
        // that describes the program. This is printed last in the help text.  
        // The second argument is the delimiter (usually space) and the last 
        // one is the version number.   
        TCLAP::CmdLine cmd("convertdij dij file sampling tool",
                ' ', "0.1");

        /*
         * TCLAP syntax
         * ^^^^^^^^^^^^
         *          
         * TCLAP reads two kinds of command line parameters, value arguments 
         * and flags.  Value argments are used to pass data to the program 
         * (such as file names).  Flags are used to pass true or false 
         * information to the program.
         *
         * SwitchArg
         * ---------
         * Switches are used to tell the program something true or false, but 
         * must be false by default, so the default behavior comes from not 
         * using any flags at all.
         *
         * SwitchArg constructor:
         *
         * TCLAP::SwitchArg::SwitchArg  ( const std::string &   flag,
         *                                const std::string &   name,
         *                                const std::string &   desc,
         *                                CmdLineInterface &    parser)
         *
         * Parameters:
         *     flag   - The one character flag that identifies this argument
         *              on the command line.
         *     name   - A one word name for the argument. Can be used as a
         *              long flag on the command line.
         *     desc   - A description of what the argument is for or does.
         *     parser - A CmdLine parser object to add this Arg to
         *
         *
         * ValueArg
         * --------
         * Value arguments can read any type data into a variable that you 
         * create.  Most arguments use this format.
         *
         * ValueArg constructor:
         *
         * template<class T>
         * TCLAP::ValueArg< T >::ValueArg (const std::string &  flag,
         *                                 const std::string &  name,
         *                                 const std::string &  desc,
         *                                 bool                 req,
         *                                 T                    value
         *                                 const std::string &  typeDesc,
         *                                 CmdLineInterface &   parser)
         *
         * Parameters:
         *     flag     - The one character flag that identifies this argument
         *                on the command line.
         *     name     - A one word name for the argument. Can be used as a
         *                long flag on the command line.
         *     desc     - A description of what the argument is for or does.
         *     req      - Whether the argument is required on the command line.
         *     value    - The default value assigned to this argument if it is
         *                not present on the command line.
         *     typeDesc - A short, human readable description of the type that
         *                this object expects. This is used in the generation 
         *                of the USAGE statement. The goal is to be helpful to 
         *                the end user of the program.
         *      parser  - A CmdLine parser object to add this Arg to
         *
         * UnlabledValueArg
         * ----------------
         *
         * The plan file name is read by an UnlabledValueArg, so any argument 
         * that is not processed by something else will be assumed to be the 
         * plan file name.  Don't add any other UnlabledValueArg arguments!!!
         *
         * UnlabledValueArg constructor:
         * 
         * template<class T>
         * TCLAP::ValueArg< T >::UnlabledValueArg (
         *                                 const std::string &  name,
         *                                 const std::string &  desc,
         *                                 bool                 req,
         *                                 T                    value
         *                                 const std::string &  typeDesc,
         *                                 CmdLineInterface &   parser)
         *
         *
         * MultiArg
         * --------
         *
         * If several values may be passed in for one argument, use a MultiArg, 
         * which returns a vector of values instead of a single one.
         *
         * MultiArg syntax:
         *
         * template<class T>
         * TCLAP::MultiArg< T >::MultiArg (const std::string &  flag,
         *                                 const std::string &  name,
         *                                 const std::string &  desc,
         *                                 bool                 req,
         *                                 const std::string &  typeDesc,
         *                                 CmdLineInterface &   parser)
         *
         * UnlabeldMultiArg
         * ----------------
         *
         *  Syntax:
         *
         *  template<class T>
         *  TCLAP::UnlabledMultiArg< T >::UnlabeledMultiArg (
         *              const std::string & name,
         *              const std::string & desc,
         *              bool                req,
         *              const std::string & typeDesc,
         *              CmdLineInterface &  parser,
         *              bool                ignoreable=false)
 	 *
         */

        /*
         * Add General options to cmd
         */

        // Input file prefix in case of processing a group of files
        TCLAP::ValueArg<std::string> in_file_name_arg("i",
                "infile_name","Input file name",
                false, "","voi file name",cmd);

        // Output file prefix in case of processing a group of files
        TCLAP::ValueArg<std::string> out_file_name_arg("o",
                "outfile_name","Output file name (defaults to converted_)",
                false, "", "voi file name",cmd);

        // Input file prefix in case of processing a group of files
        TCLAP::ValueArg<std::string> dif_file_name_arg("d",
                "dif_file_name","dif file name",
                false, "","dif file name",cmd);

        // Parse the command line
        cmd.parse(argc, argv);

	// check if required arguments are provided
	if(!in_file_name_arg.isSet() || !dif_file_name_arg.isSet()) {
	    cout << "you have to provide the dif file name and the input voi file name" << endl;
	    throw(std::runtime_error("input arguments missing"));
	}

        string in_file_name = in_file_name_arg.getValue();
        string dif_file_name = dif_file_name_arg.getValue();
        string out_file_name;

	if(out_file_name_arg.isSet()) {
	    out_file_name = out_file_name_arg.getValue();
	}
	else {
	    out_file_name = "converted_" + in_file_name;
	}

	// create geometry class to read dif file
	Geometry geometry = Geometry(dif_file_name);

	std::ifstream infile;
	std::ofstream outfile;

	char voiNo;
	int tempvoxel;
	vector<char> tempvoi(geometry.get_nVoxels(),0);

	// Attempt to open the file
	infile.open(in_file_name.c_str(), std::ios::in | std::ios::binary);
	if (!infile.is_open()) {
	    cout << "Can't open file: " << in_file_name << endl;
	    throw(std::runtime_error("cannot open file"));
	}

	outfile.open(out_file_name.c_str(), std::ios::out | std::ios::binary);
	if (!outfile.is_open()) {
	    cout << "Can't open file: " << out_file_name << endl;
	    throw(std::runtime_error("cannot open file"));
	}

	cout << "Reading VOI data from file: " << in_file_name << endl;

	// read the voi file and convert voxel indices
	for (int iVoxel=0; iVoxel<geometry.get_nVoxels(); iVoxel++) {

	    unsigned int oldvoxelNo = iVoxel;

	    // Read the next value from file
	    infile.get(voiNo);

	    // convert voxel number
	    unsigned int newvoxelNo = convert_voxel_index(iVoxel, 
							  geometry.get_voxel_nx(), 
							  geometry.get_voxel_ny(), 
							  geometry.get_voxel_nz());
	    // save voi index for this voxel
	    tempvoi[newvoxelNo] = voiNo; 
	}

	//write new file
	for (int iVoxel=0; iVoxel<geometry.get_nVoxels(); iVoxel++) {
	    outfile.write((char*) &(tempvoi[iVoxel]), sizeof(char));
	}

	// close files
	infile.close();
	outfile.close();
    }
    catch (TCLAP::ArgException &e) { // catch any exceptions
        std::cerr << "error: " << e.error() << " for arg " << e.argId()
            << std::endl;
    }
    catch (std::exception & e) { // Catch other exceptions
        std::cerr << "Error caught in convertvoi.cpp, main(): " << e.what()
                                                              << std::endl;
    }

    return(0);
}
  
int convert_voxel_index(int voxelNo, int nx, int ny, int nz) 
{
    int tempcx,tempcy,tempcz;
    int new_voxelNo;

    tempcx = voxelNo % nx;
    tempcy = (voxelNo/nx) / nz;
    tempcz = (nz-1) - ( (voxelNo/nx) % nz );

    new_voxelNo = tempcx+tempcy*nx+tempcz*nx*ny;

    assert(new_voxelNo<nx*ny*nz);
    assert(new_voxelNo>=0);

    return (new_voxelNo);
}

