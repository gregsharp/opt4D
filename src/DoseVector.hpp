#ifndef DOSEVECTOR_HPP
#define DOSEVECTOR_HPP

#include <vector>
using std::vector;
#include <string>
using std::string;

class DoseVector;

/**
 * Class DoseVector handles a vector of voxels.
 */
class DoseVector {

public:
  // Constructors

  /// Default constructor
  DoseVector (size_t nVoxels = 0);

  // Accessor to dose
  float& operator[] (const unsigned i);
  float  operator[] (const unsigned i)const;
  void  operator+= (const DoseVector &rhs);

  // add two dose cubes
  // DoseVector& operator+= (const DoseVector& rhs);

  void reset_dose();

  void scale_dose(float scale_factor);
  void set_voxel_dose(int j, float value);
  inline float get_voxel_dose(const int i)const { return dose_[i]; };
  void add_dose(const int i, const float dose_inc);
  float find_max_dose();
  inline unsigned int get_nVoxels()const { return(nVoxels_); };
  void write_dose(string dose_file)const;

private:
  /// number of voxels
  size_t nVoxels_;

  /// voxel dose
  vector<float> dose_;                

  /// maximum dose delivered
  float max_dose_;
};

inline
float& DoseVector::operator[] (const unsigned i) 
{ 
  return dose_.at(i);
}

inline
float DoseVector::operator[] (const unsigned i) const
{ 
  return dose_.at(i);
}

inline
void  DoseVector::operator+= (const DoseVector &rhs)
{
  for(unsigned int i=0;i<nVoxels_;i++){
    dose_.at(i) += rhs.dose_.at(i);
  }
}

/**
 * Set dose at voxels
 *
 * @param j     the voxel number
 * @param value the value to set voxel to
 */
inline
void DoseVector::set_voxel_dose(int j, float value)
{
  dose_.at(j)=value;
}

inline
void DoseVector::add_dose(const int i, const float dose_inc)
{
  dose_.at(i) += dose_inc;
}

#endif
