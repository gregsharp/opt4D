/**
 * @file: SquaredMeanObjective.cpp
 * SquaredMeanObjective Class implementation.
 *
 * $Id: SquaredMeanObjective.cpp,v 1.2 2008/03/13 22:06:17 bmartin Exp $
 */

#include "SquaredMeanObjective.hpp"

/**
 * Constructor: Initialize variables.  By default, there are no minimum dose 
 * constraints.
 *
 * @param the_voi The Volume of intrestest that the objective acts on
 * @param objNo The number of this objective
 * @param weight The overall weight for this objective
 * @param is_max_constraint True if this is a maximum dose constraint
 * @param max_dose Maximum dose if this is a max constraint
 * @param weight_over Penalty for overdosing
 * @param is_min_constraint True if this is a minimum dose constraint
 * @param min_dose Minimum dose if this is a min constraint
 * @param weight_under Penalty for underdosing
 */
SquaredMeanObjective::SquaredMeanObjective(
      Voi*  the_voi,
      unsigned int objNo,
      float weight,
      float sampling_fraction,
      bool sample_with_replacement,
      bool  is_max_constraint,
      float max_dose,
      float weight_over,
      bool  is_min_constraint,
      float min_dose,
      float weight_under)
: NonSeparableObjective(the_voi, objNo, weight,
       sampling_fraction, sample_with_replacement),
    is_max_constraint_(is_max_constraint),
    max_dose_(max_dose),
    weight_over_(weight_over),
    is_min_constraint_(is_min_constraint),
    min_dose_(min_dose),
    weight_under_(weight_under)
{
}

/**
 * Destructor: default.
 */
SquaredMeanObjective::~SquaredMeanObjective()
{
}


/**
 * Prints a description of the objective on the given stream
 *
 * @param o The output stream to write to
 */
void SquaredMeanObjective::printOn(std::ostream& o) const
{
    o << "OBJ " << objNo_ << ": "; 
    o << "VOI " << the_voi_->get_voiNo() << "(" << the_voi_->get_name() << ")";
    if(is_max_constraint_) {
	o << "\tmax mean dose < " << max_dose_;
	if(weight_over_ != 1.0) {
	    o << "\tweight " << weight_over_;
	}
    }
    if(is_min_constraint_) {
	o << "\tmin mean dose > " << min_dose_;
	if(weight_under_ != 1.0) {
	    o << "\tweight " << weight_under_;
	}
    }
    if(weight_ != 1.0) {
	o << "\tweight " << weight_;
    }

    // o << "\tsampling fraction " << sampling_fraction_;
}


