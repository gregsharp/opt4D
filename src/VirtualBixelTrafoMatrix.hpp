#ifndef VIRTUALBIXELTRAFOMATRIX_HPP
#define VIRTUALBIXELTRAFOMATRIX_HPP

#include <vector>
using std::vector;
#include <string>
using std::string;
#include <assert.h>


/**
 * This class implements a sparse matrix which is used to map
 * physical beamlets (those that are actually used for treatments)
 * to virtual beamlets (all beamlets which have an entry in the DoseInfluenceMatrix)
 */
class TrafoMatrix
{
  public:

    /// constructor: create an empty Trafo matrix
    TrafoMatrix();

    // Destructor
    ~TrafoMatrix();

    class node;
    class iterator;

    TrafoMatrix::iterator begin() const;
    TrafoMatrix::iterator begin_virtual(unsigned int bixelNo);
    TrafoMatrix::iterator begin_physical(unsigned int bixelNo);

    /// clear entries of the trafo matrix and set number of bixels
    void reset(unsigned int nPhysical_bixels, unsigned int nVirtual_bixels, bool transpose);

    unsigned int index_to_physicalNo(unsigned long int index) const;
    unsigned int index_to_virtualNo(unsigned long int index) const;

    /// convert number of physical bixel and virtual bixel to a combined index
    unsigned long int subscripts_to_index(const unsigned int physicalNo,
					  const unsigned int virtualNo) const;
    void transpose();
    void ensure_transposed() {if(!is_transposed_) transpose();};
    void ensure_not_transposed() {if(is_transposed_) transpose();};

    /// sort entries according to their indix
    void sort_entries();

    unsigned int get_start_index(unsigned int bixelNo) const;

    /// add a new element to the matrix
    void push_back_element(unsigned int physicalNo, unsigned int virtualNo, float value);

    /// make sure transformation is ready for use
    void initialize();

    /// check if transformation is ready for use
    bool is_initialized() const {return is_initialized_;};

    /// delete zero entries
    void delete_zero_entries();

    /// unite entries for the same virtual/physical bixel combination
    void unite_entries();

    /// tells if a bixel has an entry in the travo matrix
    bool bixel_used(unsigned int bixelNo) const;

  private:

    void set_start_index();

    /// whether matrix is transposed
    /// (sorted for physical beamlets: not transposed)
    /// (sorted for virtual beamlets: transposed)
    bool is_transposed_;

    /// Total number of physical bixels
    unsigned int nPhysical_bixels_;

    /// Total number of virtual bixels
    unsigned int nVirtual_bixels_;

    /// Total number of entries in Trafo matrix
    unsigned int nEntries_;

    /// start index for virtual/physical bixels
    vector<unsigned int> start_index_;

    /// number of virtual bixels per physical bixel (or vice versa)
    vector<unsigned int> nVirtualOrPhysical_;

    /// check whether elements are sorted and start index is set
    bool is_initialized_;

    /// Array holding sparce transformation matrix
    vector<TrafoMatrix::node> Trafo_;
};

/**
 * Class TrafoMatrix::node
 */
class TrafoMatrix::node
{
  public:
    node(unsigned long int index=0, float value=0)
      : index_(index), value_(value) {};

    friend bool operator< (
	const TrafoMatrix::node& lhs,
       	const TrafoMatrix::node& rhs) {
      return lhs.index_ < rhs.index_;
    };

    unsigned long int index_;
    float value_;
};

/**
 * Class TrafoMatrix::iterator
 */
class TrafoMatrix::iterator
{

  public:
    iterator(const TrafoMatrix& rhs);
    iterator(const TrafoMatrix& the_TrafoMatrix,
             unsigned int bixelNo);

    // Operator overloading
    TrafoMatrix::iterator& operator++ ();
    void operator++ (int);

    unsigned int get_virtualNo()const;
    unsigned int get_physicalNo()const;
    float get_value()const;

    bool not_at_end();
    bool at_end();

  private:
    iterator();

    const TrafoMatrix *the_TrafoMatrix_;

    unsigned int physicalNo_;
    unsigned int virtualNo_;
    unsigned int entryNo_;

};

/* --------------------- inline functions -------------------------- */

inline
void TrafoMatrix::reset(unsigned int nPhysical_bixels, unsigned int nVirtual_bixels, bool transpose)
{
    nPhysical_bixels_ = nPhysical_bixels;
    nVirtual_bixels_ = nVirtual_bixels;
    is_transposed_ = transpose;

    // resize
    if(!is_transposed_) {
	start_index_.resize(nPhysical_bixels_,0);
	nVirtualOrPhysical_.resize(nPhysical_bixels_,0);
    }
    else {
	start_index_.resize(nVirtual_bixels_,0);
	nVirtualOrPhysical_.resize(nVirtual_bixels_,0);
    }

    Trafo_.reserve(nPhysical_bixels_);

    // zero out values
    fill(nVirtualOrPhysical_.begin(), nVirtualOrPhysical_.end(), 0);
    fill(start_index_.begin(), start_index_.end(), 0);
    nEntries_ = 0;
    Trafo_.resize(0);
    is_initialized_ = false;
}

/**
 * Insert an entry
 */
inline
void TrafoMatrix::push_back_element(unsigned int physicalNo, unsigned int virtualNo, float value)
{
  unsigned long int index = subscripts_to_index(physicalNo, virtualNo);

  TrafoMatrix::node temp(index, value);
  Trafo_.push_back(temp);

  ++nEntries_;
  if(!is_transposed_) {
      ++(nVirtualOrPhysical_[physicalNo]);
  }
  else {
      ++(nVirtualOrPhysical_[virtualNo]);
  }
}

inline
unsigned int TrafoMatrix::get_start_index(unsigned int bixelNo) const
{
    assert(is_initialized_);
    return(start_index_[bixelNo]);
}

inline
unsigned int TrafoMatrix::index_to_physicalNo(unsigned long int index) const
{
    if(!is_transposed_) {
	return(index / nVirtual_bixels_);
    }
    else {
	return(index % nPhysical_bixels_);
    }
}

inline
unsigned int TrafoMatrix::index_to_virtualNo(unsigned long int index) const
{
    if(!is_transposed_) {
	return(index % nVirtual_bixels_);
    }
    else {
	return(index / nPhysical_bixels_);
    }
}

inline
unsigned long int TrafoMatrix::subscripts_to_index(
    const unsigned int physicalNo, const unsigned int virtualNo) const
{
  if(!is_transposed_) {
    return ((unsigned long int)nVirtual_bixels_ * (unsigned long int)physicalNo + (unsigned long int)virtualNo);
  }
  else {
    return ((unsigned long int)nPhysical_bixels_ * (unsigned long int)virtualNo + (unsigned long int)physicalNo);
  }
}

inline
bool TrafoMatrix::bixel_used(unsigned int bixelNo) const
{
    if(nVirtualOrPhysical_[bixelNo] == 0) {
	return false;
    }
    return true;
}

inline
TrafoMatrix::iterator TrafoMatrix::begin() const
{
    return TrafoMatrix::iterator(*this);
}

inline
TrafoMatrix::iterator TrafoMatrix::begin_physical(unsigned int bixelNo)
{
    ensure_not_transposed();
    return TrafoMatrix::iterator(*this,bixelNo);
}

inline
TrafoMatrix::iterator TrafoMatrix::begin_virtual(unsigned int bixelNo)
{
    ensure_transposed();
    return TrafoMatrix::iterator(*this,bixelNo);
}


/* --------------------- iterator inline functions -------------------------- */

inline
TrafoMatrix::iterator::iterator(
    const TrafoMatrix& rhs)
    : the_TrafoMatrix_(&rhs)
    , physicalNo_(0)
    , virtualNo_(0)
    , entryNo_(0)
{
    physicalNo_ = the_TrafoMatrix_->index_to_physicalNo(
	the_TrafoMatrix_->Trafo_[entryNo_].index_);
    virtualNo_ = the_TrafoMatrix_->index_to_virtualNo(
	the_TrafoMatrix_->Trafo_[entryNo_].index_);
}

inline
TrafoMatrix::iterator::iterator(
    const TrafoMatrix& trafo,
    unsigned int bixelNo)
    : the_TrafoMatrix_(&trafo)
    , physicalNo_(0)
    , virtualNo_(0)
    , entryNo_(0)
{
    entryNo_ = the_TrafoMatrix_->get_start_index(bixelNo);
    physicalNo_ = the_TrafoMatrix_->index_to_physicalNo(
	the_TrafoMatrix_->Trafo_[entryNo_].index_);
    virtualNo_ = the_TrafoMatrix_->index_to_virtualNo(
	the_TrafoMatrix_->Trafo_[entryNo_].index_);
}

inline
TrafoMatrix::iterator& TrafoMatrix::iterator::operator++ ()
{
    entryNo_++;
    if(entryNo_ >= the_TrafoMatrix_->nEntries_) {
	// At end
	physicalNo_ = the_TrafoMatrix_->nPhysical_bixels_;
	virtualNo_ = the_TrafoMatrix_->nVirtual_bixels_;
	return *this;
    }

    physicalNo_ = the_TrafoMatrix_->index_to_physicalNo(
	the_TrafoMatrix_->Trafo_[entryNo_].index_);
    virtualNo_ = the_TrafoMatrix_->index_to_virtualNo(
	the_TrafoMatrix_->Trafo_[entryNo_].index_);

    return *this;
}

inline
void TrafoMatrix::iterator::operator++ (int)
{
    ++(*this);
}

inline
unsigned int TrafoMatrix::iterator::get_physicalNo()const
{
  return physicalNo_;
}

inline
unsigned int TrafoMatrix::iterator::get_virtualNo()const
{
  return virtualNo_;
}

inline
float TrafoMatrix::iterator::get_value()const
{
  return the_TrafoMatrix_->Trafo_[entryNo_].value_;
}

inline
bool TrafoMatrix::iterator::at_end()
{
    return (entryNo_ >= the_TrafoMatrix_->nEntries_);
}

inline
bool TrafoMatrix::iterator::not_at_end()
{
    return (!at_end());
}



#endif
