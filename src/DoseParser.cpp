
/**
 * @file DoseParser.cpp
 *
 * Function definitions for DoseParser class.
 */

#include <fstream>
#include <iomanip>
#include <stdexcept>
#include <memory>

#include "DoseParser.hpp"

/**
 * Constructor to read from a file
 *
 * @param file_name The file to read
 * @param nVoxels check for the correct size of dose vector
*/
DoseParser::DoseParser(const string & file_name,
		       unsigned int nVoxels)
  : dose_(nVoxels)
{
  read_file(file_name,nVoxels);
}


/**
 * get the dose
 */
DoseVector DoseParser::get_dose()
{
  return(dose_);
}


/**
 * Read a file
 */
void DoseParser::read_file(const string & file_name,
			   unsigned int nVoxels)
{
  std::cout << "DoseParser reading file: " << file_name << std::endl;

  /// Stream to read from
  std::auto_ptr<std::ifstream> input_file;

  // Open file
  input_file.reset(new std::ifstream(file_name.c_str(), std::ios::binary));

  if(!input_file->good()) {
    string temp = "Can't open file: " + file_name;
    std::cout << temp << std::endl;
    throw(std::runtime_error(temp));
  }

  std::cout << "Reading Dose data from file: " << file_name << std::endl;

  // Read each voxel from file
  unsigned int iVoxel = 0;
  while(!input_file->eof() && iVoxel<nVoxels) {
    dose_.set_voxel_dose(iVoxel,read_binary_float(*input_file));
    iVoxel++;
  }

  // check number of voxels
  if(iVoxel != nVoxels){
    string temp = "Size of dose vector to read is not correct";
    std::cout << temp << std::endl;
    throw(std::runtime_error(temp));
  }
}



