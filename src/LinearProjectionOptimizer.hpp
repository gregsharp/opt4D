/**
 * @file LinearProjectionOptimizer.hpp
 * LinearProjectionOptimizer Class Header
 */

#ifndef LINEARPROJECTIONOPTIMIZER_HPP
#define LINEARPROJECTIONOPTIMIZER_HPP

class LinearProjectionOptimizer;


#include "Optimization.hpp"
#include "Plan.hpp"



/**
 * 
 * ExternalOptimizer class is used to call an external solver
 */
class LinearProjectionOptimizer : public Optimization
{
public:

  /// constructor
  LinearProjectionOptimizer(Plan* thisplan, string log_file_name, Optimization::options options);

  /// optimize plan
  void optimize();

  void initialize();

private:

  // perform iterative tightening of a constraint
  void linear_projection_solver();

  // solve feasibility problem
  bool linear_feasibility_solver();
};


#endif
