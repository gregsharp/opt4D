/**
 * @file DirectParameterVector.cpp
 * DirectParameterVector functions
 */

#include "DirectParameterVector.hpp"
#include "TxtFileParser.hpp"
#include <limits>
#include <memory>
#include <cstdlib>
#include <cmath>
#include <fstream>
#include <iomanip>
#include <ios>

// Parameter difference threshold, relative to step size
#define ZERO_DIFF_THRESHOLD 0.1
#define TMP_BIX_SIZE_FACTOR 0.25
#define ALLOWED_BEAMANGLE_DISCREPANCY 30
#define PI 3.14159265

#define DEBUG_MESSAGES 1
#define WRITE_DEBUG_MOTION_FILE 0

/**
 * Constructor
 *
 * @param dao_file_name	The DAO setup filename
 */
DirectParameterVector::DirectParameterVector( const BixelVector & bixMap, const string &dao_file_name, Geometry const * geometry ){

    /// The dao file parser
    std::auto_ptr<const TxtFileParser> txtFileParser_;

    vector< string > required_attributes;
    vector< string > optional_attributes;
    map< string, string > default_values;
    vector< string > multi_attributes;
    map< string, vector< string > > required_sub_attributes;
    map< string, vector< string > > optional_sub_attributes;
    map< string, map< string, string > > default_sub_values;
    populateParserVectors( required_attributes, optional_attributes, default_values, multi_attributes,
                           required_sub_attributes, optional_sub_attributes, default_sub_values );

    // Read the dao file
    txtFileParser_.reset( new TxtFileParser( dao_file_name, required_attributes, optional_attributes, default_values,
            multi_attributes, required_sub_attributes, optional_sub_attributes, default_sub_values ) );

    bool optimizeEnergy = false;
    if ( txtFileParser_->is_set_multi_attribute( "BeamEnergy", "Optimize", 0) )
        optimizeEnergy = txtFileParser_->get_num_multi_attribute<bool>( "BeamEnergy", "Optimize", 0);
    bool optimizeDoseRate = false;
    if ( txtFileParser_->is_set_multi_attribute( "DoseRate", "Optimize", 0) )
        optimizeDoseRate = txtFileParser_->get_num_multi_attribute<bool>( "DoseRate", "Optimize", 0);
    bool optimizeGantry = false;
    if ( txtFileParser_->is_set_multi_attribute( "Gantry", "Optimize", 0) )
        optimizeGantry = txtFileParser_->get_num_multi_attribute<bool>( "Gantry", "Optimize", 0);
    bool optimizeCollimator = false;
    if ( txtFileParser_->is_set_multi_attribute( "Collimator", "Optimize", 0) )
        optimizeCollimator = txtFileParser_->get_num_multi_attribute<bool>( "Collimator", "Optimize", 0);
    bool optimizeCouch = false;
    if ( txtFileParser_->is_set_multi_attribute( "Couch", "Optimize", 0) )
        optimizeCouch = txtFileParser_->get_num_multi_attribute<bool>( "Couch", "Optimize", 0);
    bool optimizeJaws = false;
    if ( txtFileParser_->is_set_multi_attribute( "Jaws", "Optimize", 0) )
        optimizeJaws = txtFileParser_->get_num_multi_attribute<bool>( "Jaws", "Optimize", 0);
    bool optimizeMLC = false;
    if ( txtFileParser_->is_set_multi_attribute( "MLC", "Optimize", 0) )
        optimizeMLC = txtFileParser_->get_num_multi_attribute<bool>( "MLC", "Optimize", 0);

    bool optimizeEnergyChanges = false;
    if ( txtFileParser_->is_set_multi_attribute( "BeamEnergy", "ChangesBetweenCP", 0) )
        optimizeEnergyChanges = txtFileParser_->get_num_multi_attribute<bool>( "BeamEnergy", "ChangesBetweenCP", 0);
    bool optimizeDoseRateChanges = false;
    if ( txtFileParser_->is_set_multi_attribute( "DoseRate", "ChangesBetweenCP", 0) )
        optimizeDoseRateChanges = txtFileParser_->get_num_multi_attribute<bool>( "DoseRate", "ChangesBetweenCP", 0);
    bool optimizeGantryChanges = false;
    if ( txtFileParser_->is_set_multi_attribute( "Gantry", "ChangesBetweenCP", 0) )
        optimizeGantryChanges = txtFileParser_->get_num_multi_attribute<bool>( "Gantry", "ChangesBetweenCP", 0);
    bool optimizeCollimatorChanges = false;
    if ( txtFileParser_->is_set_multi_attribute( "Collimator", "ChangesBetweenCP", 0) )
        optimizeCollimatorChanges = txtFileParser_->get_num_multi_attribute<bool>( "Collimator", "ChangesBetweenCP", 0);
    bool optimizeCouchChanges = false;
    if ( txtFileParser_->is_set_multi_attribute( "Couch", "ChangesBetweenCP", 0) )
        optimizeCouchChanges = txtFileParser_->get_num_multi_attribute<bool>( "Couch", "ChangesBetweenCP", 0);
    bool optimizeJawsChanges = false;
    if ( txtFileParser_->is_set_multi_attribute( "Jaws", "ChangesBetweenCP", 0) )
        optimizeJawsChanges = txtFileParser_->get_num_multi_attribute<bool>( "Jaws", "ChangesBetweenCP", 0);
    bool optimizeMLCChanges = false;
    if ( txtFileParser_->is_set_multi_attribute( "MLC", "ChangesBetweenCP", 0) )
        optimizeMLCChanges = txtFileParser_->get_num_multi_attribute<bool>( "MLC", "ChangesBetweenCP", 0);

    // Set up the treatment machine model
    if ( ! txtFileParser_->is_set_attribute( "MachineFile" ) ) {
        cerr << "No Machine file listed in DAO file" << endl;
        assert(0);
    }
    machine_.reset( new DeliveryMachineModel( txtFileParser_->get_attribute( "MachineFile" ) ) );

    /// Get number of fields and number of control points per field
    unsigned int nFields = 0;
    vector<unsigned int> cpNums;

    if ( txtFileParser_->is_set_attribute( "NumFields" ) )
        nFields = txtFileParser_->get_num_attribute<unsigned int>( "NumFields" );

    int cpNumEntries = 0;
    unsigned int cpEntry;
	for ( unsigned int fd=0; fd<nFields; fd++ )
        if ( txtFileParser_->is_set_multi_attribute( "Field", "NumSegments", fd) ) {
            cpNumEntries++;
            cpEntry = txtFileParser_->get_num_multi_attribute<unsigned int>("Field", "NumSegments", fd);
        }
    if (cpNumEntries == 1) {
		for ( unsigned int fd=0; fd<nFields; fd++ ) cpNums.push_back( cpEntry );
    } else {
		for ( unsigned int fd=0; fd<nFields; fd++ ) {
            if ( txtFileParser_->is_set_multi_attribute( "Field", "NumSegments", fd) ) {
                cpNums.push_back( txtFileParser_->get_num_multi_attribute<unsigned int>("Field", "NumSegments", fd) );
            } else cpNums.push_back( 0 );
        }
    }

    // Call other constructor to finish constructing object.
    DirectParameterVector( bixMap, *machine_, geometry, cpNums, optimizeMLC, optimizeMLCChanges, optimizeJaws,
                           optimizeJawsChanges, optimizeEnergy, optimizeEnergyChanges,
                           optimizeGantry, optimizeGantryChanges, optimizeCollimator,
                           optimizeCollimatorChanges, optimizeCouch, optimizeCouchChanges,
                           optimizeDoseRate, optimizeDoseRateChanges );
}


/**
 * Constructor.
 *
 * @param dao_file_name	The DAO setup filename
 * @param mpf_file_name	The filename of the delivery machine model.
 * @param nFields       The number of fields in the plan.
 * @param cpPerField    Array of numbers of control points in each field.
 */
DirectParameterVector::DirectParameterVector( const BixelVector & bixMap,
		const string &dao_file_name, const string & mpf_file_name, Geometry const * geometry, const vector<unsigned int> &cpPerField ) {

    /// The dao file parser
    std::auto_ptr<const TxtFileParser> txtFileParser_;

    vector< string > required_attributes;
    vector< string > optional_attributes;
    map< string, string > default_values;
    vector< string > multi_attributes;
    map< string, vector< string > > required_sub_attributes;
    map< string, vector< string > > optional_sub_attributes;
    map< string, map< string, string > > default_sub_values;
    populateParserVectors( required_attributes, optional_attributes, default_values, multi_attributes,
                           required_sub_attributes, optional_sub_attributes, default_sub_values );

    // Read the dao file
    txtFileParser_.reset( new TxtFileParser( dao_file_name, required_attributes, optional_attributes, default_values,
            multi_attributes, required_sub_attributes, optional_sub_attributes, default_sub_values ) );

    bool optimizeEnergy = false;
    if ( txtFileParser_->is_set_multi_attribute( "BeamEnergy", "Optimize", 0) )
        optimizeEnergy = txtFileParser_->get_num_multi_attribute<bool>( "BeamEnergy", "Optimize", 0);
    bool optimizeDoseRate = false;
    if ( txtFileParser_->is_set_multi_attribute( "DoseRate", "Optimize", 0) )
        optimizeDoseRate = txtFileParser_->get_num_multi_attribute<bool>( "DoseRate", "Optimize", 0);
    bool optimizeGantry = false;
    if ( txtFileParser_->is_set_multi_attribute( "Gantry", "Optimize", 0) )
        optimizeGantry = txtFileParser_->get_num_multi_attribute<bool>( "Gantry", "Optimize", 0);
    bool optimizeCollimator = false;
    if ( txtFileParser_->is_set_multi_attribute( "Collimator", "Optimize", 0) )
        optimizeCollimator = txtFileParser_->get_num_multi_attribute<bool>( "Collimator", "Optimize", 0);
    bool optimizeCouch = false;
    if ( txtFileParser_->is_set_multi_attribute( "Couch", "Optimize", 0) )
        optimizeCouch = txtFileParser_->get_num_multi_attribute<bool>( "Couch", "Optimize", 0);
    bool optimizeJaws = false;
    if ( txtFileParser_->is_set_multi_attribute( "Jaws", "Optimize", 0) )
        optimizeJaws = txtFileParser_->get_num_multi_attribute<bool>( "Jaws", "Optimize", 0);
    bool optimizeMLC = false;
    if ( txtFileParser_->is_set_multi_attribute( "MLC", "Optimize", 0) )
        optimizeMLC = txtFileParser_->get_num_multi_attribute<bool>( "MLC", "Optimize", 0);

    bool optimizeEnergyChanges = false;
    if ( txtFileParser_->is_set_multi_attribute( "BeamEnergy", "ChangesBetweenCP", 0) )
        optimizeEnergyChanges = txtFileParser_->get_num_multi_attribute<bool>( "BeamEnergy", "ChangesBetweenCP", 0);
    bool optimizeDoseRateChanges = false;
    if ( txtFileParser_->is_set_multi_attribute( "DoseRate", "ChangesBetweenCP", 0) )
        optimizeDoseRateChanges = txtFileParser_->get_num_multi_attribute<bool>( "DoseRate", "ChangesBetweenCP", 0);
    bool optimizeGantryChanges = false;
    if ( txtFileParser_->is_set_multi_attribute( "Gantry", "ChangesBetweenCP", 0) )
        optimizeGantryChanges = txtFileParser_->get_num_multi_attribute<bool>( "Gantry", "ChangesBetweenCP", 0);
    bool optimizeCollimatorChanges = false;
    if ( txtFileParser_->is_set_multi_attribute( "Collimator", "ChangesBetweenCP", 0) )
        optimizeCollimatorChanges = txtFileParser_->get_num_multi_attribute<bool>( "Collimator", "ChangesBetweenCP", 0);
    bool optimizeCouchChanges = false;
    if ( txtFileParser_->is_set_multi_attribute( "Couch", "ChangesBetweenCP", 0) )
        optimizeCouchChanges = txtFileParser_->get_num_multi_attribute<bool>( "Couch", "ChangesBetweenCP", 0);
    bool optimizeJawsChanges = false;
    if ( txtFileParser_->is_set_multi_attribute( "Jaws", "ChangesBetweenCP", 0) )
        optimizeJawsChanges = txtFileParser_->get_num_multi_attribute<bool>( "Jaws", "ChangesBetweenCP", 0);
    bool optimizeMLCChanges = false;
    if ( txtFileParser_->is_set_multi_attribute( "MLC", "ChangesBetweenCP", 0) )
        optimizeMLCChanges = txtFileParser_->get_num_multi_attribute<bool>( "MLC", "ChangesBetweenCP", 0);

    // Set up the treatment machine model
    machine_.reset( new DeliveryMachineModel( mpf_file_name ) );

    // Call other constructor to finish constructing object.
    DirectParameterVector( bixMap, *machine_, geometry, cpPerField, optimizeMLC, optimizeMLCChanges, optimizeJaws,
                           optimizeJawsChanges, optimizeEnergy, optimizeEnergyChanges,
                           optimizeGantry, optimizeGantryChanges, optimizeCollimator,
                           optimizeCollimatorChanges, optimizeCouch, optimizeCouchChanges,
                           optimizeDoseRate, optimizeDoseRateChanges );
}


/**
 * Constructor.
 *
 * @param dao_file_name	The DAO setup filename
 * @param mpf_file_name	The filename of the delivery machine model.
 * @param nFields       The number of fields in the plan.
 * @param cpPerField    The number of control points in every field.
 */
DirectParameterVector::DirectParameterVector( const BixelVector & bixMap, const string &dao_file_name, const string & mpf_file_name,
                                              Geometry const * geometry, unsigned int nFields, unsigned int cpPerField ) {
    vector<unsigned int> cpNums(nFields,cpPerField);
	DirectParameterVector( bixMap, dao_file_name, mpf_file_name, geometry, cpNums );
}


/**
 * Constructor.
 * Initializes set of parameters sufficent to define the treatment plan.
 *
 * @param machine       The number of fields in the plan.
 * @param cpPerField    The number of control points in each field.
 * @param includeLeafPosition		Include leaf position parameters for each field.
 * @param includeLeafPositionChanges Include leaf position parameters for each control point.
 * @param includeJawPosition		Include jaw position parameters for each field.
 * @param includeJawPositionChanges Include jaw position parameters for each control point.
 * @param includeEnergy				Include beam energy parameters for each field.
 * @param includeEnergyChanges		Include beam energy parameters for each control point.
 * @param includeGantryAngle		Include gantry angle parameters for each field.
 * @param includeGantryAngleChanges Include gantry angle parameters for each control point.
 * @param includeCollAngle			Include collimator angle parameters for each field.
 * @param includeCollAngleChanges	Include collimator angle parameters for each control point.
 * @param includeCouchAngle			Include couch angle parameters for each field.
 * @param includeCouchAngleChanges	Include couch angle parameters for each control point.
 * @param includeDoseRate			Include dose rate parameters for each field.
 * @param includeDoseRateChanges	Include dose rate parameters for each control point.
 */
DirectParameterVector::DirectParameterVector( const BixelVector & bixMap, DeliveryMachineModel & machine,
        Geometry const * geometry, const vector<unsigned int> &cpPerField, bool includeLeafPosition,
        bool includeLeafPositionChanges, bool includeJawPosition, bool includeJawPositionChanges,
        bool includeEnergy, bool includeEnergyChanges, bool includeGantryAngle, bool includeGantryAngleChanges,
        bool includeCollAngle, bool includeCollAngleChanges, bool includeCouchAngle, bool includeCouchAngleChanges,
        bool includeDoseRate, bool includeDoseRateChanges )
    : bixMap_( new BixelVector( bixMap ) ), machine_( new DeliveryMachineModel(machine) ), geometry_( geometry ),
    doseNormFactor_(1.0f), isWeightingsAbsolute_(true), nf_( cpPerField.size() ), ncp_( cpPerField.size(), 0 ) {

    // --- Check Input Parameters for validity --- //

    // Cannot optimize parameter changes without including the parameter
    if ( !includeLeafPosition ) includeLeafPositionChanges = false;
    if ( !includeJawPosition ) includeJawPositionChanges = false;
    if ( !includeEnergy ) includeEnergyChanges = false;
    if ( !includeGantryAngle ) includeGantryAngleChanges = false;
    if ( !includeCollAngle ) includeCollAngleChanges = false;
    if ( !includeCouchAngle ) includeCouchAngleChanges = false;
    if ( !includeDoseRate ) includeDoseRateChanges = false;

    // Override selections where they clash with the machine capabilities.
    if ( machine_->getMlc(0u)->getNumValues() < 2 && includeLeafPosition ) {
        cerr << "Warning: MLC optimisation property clashes with machine file." << endl;
        cerr << "Rejecting optimization parameter selection." << endl;
        includeLeafPosition = false;
        includeLeafPositionChanges = false;
    }
    if ( !machine_->getMlc(0u)->getCanChangeDuringDelivery() && includeLeafPositionChanges ) {
        includeLeafPositionChanges = false;
    }

    // Override selections where they clash with the machine capabilities.
    if ( machine_->getJaws(0u)->getNumValues() < 2 && includeJawPosition ) {
        cerr << "Warning: Jaw optimisation property clashes with machine file." << endl;
        cerr << "Rejecting optimization parameter selection." << endl;
        includeJawPosition = false;
        includeJawPositionChanges = false;
    }
    if ( !machine_->getJaws(0u)->getCanChangeDuringDelivery() && includeJawPositionChanges ) {
        includeJawPositionChanges = false;
    }

    // Override selections where they clash with the machine capabilities.
    if ( machine_->getBeamEnergy()->getNumValues() < 2 && includeEnergy ) {
        cerr << "Warning: Beam energy optimisation property clashes with machine file." << endl;
        cerr << "Rejecting optimization parameter selection." << endl;
        includeEnergy = false;
        includeEnergyChanges = false;
    }
    if ( !machine_->getBeamEnergy()->getCanChangeDuringDelivery() && includeEnergyChanges ) {
        includeEnergyChanges = false;
    }

    // Override selections where they clash with the machine capabilities.
    if ( machine_->getGantry()->getNumValues() < 2 && includeGantryAngle ) {
        cerr << "Warning: Gantry optimisation property clashes with machine file." << endl;
        cerr << "Rejecting optimization parameter selection." << endl;
        includeGantryAngle = false;
        includeGantryAngleChanges = false;
    }
    if ( !machine_->getGantry()->getCanChangeDuringDelivery() && includeGantryAngleChanges ) {
        includeGantryAngleChanges = false;
    }

    // Override selections where they clash with the machine capabilities.
    if ( machine_->getCollimator()->getNumValues() < 2 && includeCollAngle ) {
        cerr << "Warning: Collimator optimisation property clashes with machine file." << endl;
        cerr << "Rejecting optimization parameter selection." << endl;
        includeCollAngle = false;
        includeCollAngleChanges = false;
    }
    if ( !machine_->getCollimator()->getCanChangeDuringDelivery() && includeCollAngleChanges ) {
        includeCollAngleChanges = false;
    }

    // Override selections where they clash with the machine capabilities.
    if ( machine_->getCouch()->getNumValues() < 2 && includeCouchAngle ) {
        cerr << "Warning: Couch optimisation property clashes with machine file." << endl;
        cerr << "Rejecting optimizationparameter selection." << endl;
        includeCouchAngle = false;
        includeCouchAngleChanges = false;
    }
    if ( !machine_->getCouch()->getCanChangeDuringDelivery() && includeCouchAngleChanges ) {
        includeCouchAngleChanges = false;
    }

    // Override selections where they clash with the machine capabilities.
    if ( machine_->getDoseRate()->getNumValues() <= 1 && includeDoseRate ) {
        cerr << "Warning: Dose rate optimisation property clashes with machine file." << endl;
        cerr << "Rejecting optimization parameter selection." << endl;
        includeDoseRate = false;
        includeDoseRateChanges = false;
    }
    if ( !machine_->getDoseRate()->getCanChangeDuringDelivery() && includeDoseRateChanges ) {
        includeDoseRateChanges = false;
    }

    nf_ = cpPerField.size();

    isConstantDoseRateInFd_.assign(nf_,true);
    isConstantGantryInFd_.assign(nf_,true);
    isConstantCollInFd_.assign(nf_,true);
    isConstantCouchInFd_.assign(nf_,true);
    isConstantXJawInFd_.assign(nf_,true);
    isConstantYJawInFd_.assign(nf_,true);
    isConstantXLeafInFd_.assign(nf_,true);
    isConstantYLeafInFd_.assign(nf_,true);

    if ( includeEnergyChanges ) {
        isConstantEnergyInFd_.assign(nf_,false);
    } else {
        isConstantEnergyInFd_.assign(nf_,true);
    }
    if ( includeDoseRateChanges ) {
        isConstantDoseRateInFd_.assign(nf_,false);
    } else {
        isConstantDoseRateInFd_.assign(nf_,true);
    }
    if ( includeGantryAngleChanges ) {
        isConstantGantryInFd_.assign(nf_,false);
    } else {
        isConstantGantryInFd_.assign(nf_,true);
    }
    if ( includeCollAngleChanges ) {
        isConstantCollInFd_.assign(nf_,false);
    } else {
        isConstantCollInFd_.assign(nf_,true);
    }
    if ( includeCouchAngleChanges ) {
        isConstantCouchInFd_.assign(nf_,false);
    } else {
        isConstantCouchInFd_.assign(nf_,true);
    }
    if ( includeJawPositionChanges ) {
        if ( machine_->getIsXJaws() ) isConstantXJawInFd_.assign( nf_, false );
        if ( machine_->getIsYJaws() ) isConstantYJawInFd_.assign( nf_, false );
    } else {
        if ( machine_->getIsXJaws() ) isConstantXJawInFd_.assign( nf_, true );
        if ( machine_->getIsYJaws() ) isConstantYJawInFd_.assign( nf_, true );
    }
    if ( includeLeafPositionChanges ) {
        if ( machine_->getIsXMlc() ) isConstantXLeafInFd_.assign( nf_, false );
        if ( machine_->getIsYMlc() ) isConstantYLeafInFd_.assign( nf_, false );
    } else {
        if ( machine_->getIsXMlc() ) isConstantXLeafInFd_.assign( nf_, true );
        if ( machine_->getIsYMlc() ) isConstantYLeafInFd_.assign( nf_, true );
    }

    // Count total number of control points
    ncp_.reserve(cpPerField.size());
    ncpTot_ = 0;
    for ( unsigned int fd=0; fd<nf_; fd++ ) {
        ncp_[fd] = cpPerField[fd];
        ncpTot_ += ncp_[fd];
    }

    initializeCP_metaData(includeLeafPosition, includeJawPosition, includeEnergy, includeGantryAngle,
                          includeCollAngle, includeCouchAngle, includeDoseRate );

    // Count number of parameters required
    nParams_ = 0;
    for ( unsigned int cpi=0; cpi<ncpTot_; cpi++ ) nParams_ += paramPerCP_[cpi];

    // Deal with per parameter vectors
    reserve_nParams( nParams_ );

    step_size_.assign( nParams_, 0.0f );
    isActive_.assign( nParams_, true );
    isDiscreteVariable_.assign( nParams_, true );
    numberOfValidValues_.assign( nParams_, 0 );
    type_.assign( nParams_, Weight );
    absLowBound_.assign( nParams_, 0.0f );
    absHighBound_.assign( nParams_, 0.0f );
    lowBound_.assign( nParams_, 0.0f );
    highBound_.assign( nParams_, 0.0f );
    value_.assign( nParams_, 0.0f );

    // Set up parameters
    for ( unsigned int cpi=0; cpi<ncpTot_; cpi++ )
        initializeCPParams(cpi);

    // Default weights can be problematic with arcing so override to make a possible set
    for ( unsigned int fd=0; fd<nf_; fd++ ) {
        if ( is_impossible_ArcWeights(fd) ) {
            resetLimits_ArcWeights(fd);
            if ( !isConstantGantryInFd_[fd] ) {
                /*unsigned int ip1 = get_PInd_Gantry( get_absCPindex(fd,0), 0 );
                unsigned int ip2 = get_PInd_Gantry( get_absCPindex(fd,ncp_[fd]/2), 0 );
                cout << "Debug 1 : " << value_[ip1] << " : " << lowBound_[ip1] << " : " << highBound_[ip1] << endl;
                cout << "Debug 2 : " << value_[ip2] << " : " << lowBound_[ip2] << " : " << highBound_[ip2] << endl;*/
                for ( unsigned int cp=0; cp<ncp_[fd]; cp++ ) {
                    unsigned int ip0 = get_PInd_Gantry( get_absCPindex(fd,cp), 0 );
                    value_[ip0] = ( lowBound_[ip0] + highBound_[ip0] ) / 2;
                }
                //cout << "Debug 3 : " << value_[ip1] << " : " << lowBound_[ip1] << " : " << highBound_[ip1] << endl;
                //cout << "Debug 4 : " << value_[ip2] << " : " << lowBound_[ip2] << " : " << highBound_[ip2] << endl;
            }
            if ( !isConstantCollInFd_[fd] ) {
                for ( unsigned int cp=0; cp<ncp_[fd]; cp++ ) {
                    unsigned int ip0 = get_PInd_Collimator( get_absCPindex(fd,cp), 0 );
                    value_[ip0] = ( lowBound_[ip0] + highBound_[ip0] ) / 2;
                }
            }
            if ( !isConstantCouchInFd_[fd] ) {
                for ( unsigned int cp=0; cp<ncp_[fd]; cp++ ) {
                    unsigned int ip0 = get_PInd_Couch( get_absCPindex(fd,cp), 0 );
                    value_[ip0] = ( lowBound_[ip0] + highBound_[ip0] ) / 2;
                }
            }
        }
    }

    // finally set low and high bounds (those that depend on the current set of values).
    //makePossible();
    resetLimits();
}


/**
 * Copy Constructor.
 */
DirectParameterVector::DirectParameterVector( const DirectParameterVector & dpv ) : ParameterVector( dpv ) {

    doseNormFactor_ = dpv.doseNormFactor_;
    nf_ = dpv.nf_;
    ncpTot_ = dpv.ncpTot_;

    isWeightingsAbsolute_ = dpv.isWeightingsAbsolute_;
    isConstantEnergyInFd_ = dpv.isConstantEnergyInFd_;
    isConstantDoseRateInFd_ = dpv.isConstantDoseRateInFd_;
    isConstantGantryInFd_ = dpv.isConstantGantryInFd_;
    isConstantCollInFd_ = dpv.isConstantCollInFd_;
    isConstantCouchInFd_ = dpv.isConstantCouchInFd_;
    isConstantXJawInFd_ = dpv.isConstantXJawInFd_;
    isConstantYJawInFd_ = dpv.isConstantYJawInFd_;
    isConstantXLeafInFd_ = dpv.isConstantXLeafInFd_;
    isConstantYLeafInFd_ = dpv.isConstantYLeafInFd_;

    ncp_ = dpv.ncp_;
    paramPerCP_ = dpv.paramPerCP_;
    nWeightingParams_ = dpv.nWeightingParams_;
    nBeamEnergyParams_ = dpv.nBeamEnergyParams_;
    nDoseRateParams_ = dpv.nDoseRateParams_;
    nGantryParams_ = dpv.nGantryParams_;
    nCollimatorParams_ = dpv.nCollimatorParams_;
    nCouchParams_ = dpv.nCouchParams_;
    nXJawParams_ = dpv.nXJawParams_;
    nYJawParams_ = dpv.nYJawParams_;
    nXLeafParams_ = dpv.nXLeafParams_;
    nYLeafParams_ = dpv.nYLeafParams_;

    type_ = dpv.type_;
    absHighBound_ = dpv.absHighBound_;
    absLowBound_ = dpv.absLowBound_;
    isDiscreteVariable_ = dpv.isDiscreteVariable_;
    numberOfValidValues_ = dpv.numberOfValidValues_;
    validValues_ = dpv.validValues_;

    geometry_ = dpv.geometry_;
    bixMap_ = new BixelVector( *dpv.bixMap_ );
    machine_.reset( new DeliveryMachineModel(*dpv.machine_));
}

/// Set a parameter with given set of constraints
void DirectParameterVector::setParam( unsigned int pInd, int nValidValues, float minVal, float maxVal,
                                      float stepSize, ParameterType pType ) {

    step_size_[pInd] = stepSize;
    isActive_[pInd] = true;
    if (stepSize > 0.0f)
        isDiscreteVariable_[ pInd ] = true;
    else
        isDiscreteVariable_[ pInd ] = false;
    numberOfValidValues_[ pInd ] = nValidValues;
    type_[ pInd ] = pType;
    absLowBound_[pInd] = minVal;
    absHighBound_[pInd] = maxVal;
    // temporarily set local bounds as equal to global bounds.
    lowBound_[pInd] = minVal;
    highBound_[pInd] = maxVal;

    switch ( pType ) {
    case YLeaf:
    case XLeaf:
    case YJaw:
    case XJaw:
        value_[ pInd ] = get_RoundToValidValue( pInd, ( get_absLowBound(pInd) + get_absHighBound(pInd) ) * 0.5f );
        break;
    case Couch:
    case Collimator:
    case Gantry:
        value_[ pInd ] = 0.0f;
        break;
    case DoseRate:
        value_[ pInd ] = get_RoundToValidValue( pInd, machine_->getDoseRate()->getDefaultValue() );
        break;
    case BeamEnergy:
        value_[ pInd ] = get_RoundToValidValue( pInd, machine_->getBeamEnergy()->getDefaultIndex() );
        break;
    case Weight:
        value_[ pInd ] = 1.0f;
    };
}

/// Set a parameter with given set of constraints
void DirectParameterVector::setParam( unsigned int pInd, int nValidValues, float minVal, float maxVal,
                                    float stepSize, const std::vector<float> & validValForPn , ParameterType pType) {

    setParam( pInd, nValidValues, minVal, maxVal, stepSize, pType );
    isDiscreteVariable_[ pInd ] = true;
    validValues_.insert( pair<unsigned int, std::vector<float> >(pInd,validValForPn) );
    value_[ pInd ] = get_RoundToValidValue( pInd, ( get_absLowBound(pInd) + get_absHighBound(pInd) ) * 0.5f );
}


/**
 * Initializes a set of control points and the component parameters for a particular field.
 */
void DirectParameterVector::initializeCP_metaData(bool includeLeafPosition, bool includeJawPosition, bool includeEnergy,
                                                  bool includeGantryAngle, bool includeCollAngle, bool includeCouchAngle,
                                                  bool includeDoseRate ) {

    // Populate per control point variables for each parameter type
    nWeightingParams_.reserve( ncpTot_ + nf_ );
    nBeamEnergyParams_.reserve( ncpTot_ );
    nDoseRateParams_.reserve( ncpTot_ );
    nGantryParams_.reserve( ncpTot_ );
    nCollimatorParams_.reserve( ncpTot_ );
    nCouchParams_.reserve( ncpTot_ );
    nXJawParams_.reserve( ncpTot_ );
    nYJawParams_.reserve( ncpTot_ );
    nXLeafParams_.reserve( ncpTot_ );
    nYLeafParams_.reserve( ncpTot_ );

    for ( unsigned int fd=0; fd<nf_; fd++ ) {
        nWeightingParams_.push_back( 1 );
        if ( includeEnergy ) {
            nBeamEnergyParams_.push_back( 1 );
        } else {
            nBeamEnergyParams_.push_back( 0 );
        }
        if ( includeDoseRate ) {
            nDoseRateParams_.push_back( 1 );
        } else {
            nDoseRateParams_.push_back( 0 );
        }
        if ( includeGantryAngle ) {
            nGantryParams_.push_back( 1 );
        } else {
            nGantryParams_.push_back( 0 );
        }
        if ( includeCollAngle ) {
            nCollimatorParams_.push_back( 1 );
        } else {
            nCollimatorParams_.push_back( 0 );
        }
        if ( includeCouchAngle ) {
            nCouchParams_.push_back( 1 );
        } else {
            nCouchParams_.push_back( 0 );
        }
        if ( includeJawPosition ) {
            if ( machine_->getIsXJaws() ) {
                nXJawParams_.push_back( machine_->getNXJaws() );
            } else {
                nXJawParams_.push_back( 0 );
            }
            if ( machine_->getIsYJaws() ) {
                nYJawParams_.push_back( machine_->getNYJaws() );
            } else {
                nYJawParams_.push_back( 0 );
            }
        } else {
            nXJawParams_.push_back( 0 );
            nYJawParams_.push_back( 0 );
        }
        if ( includeLeafPosition ) {
            if ( machine_->getIsXMlc() ) {
                nXLeafParams_.push_back( machine_->getNXLeaves() );
            } else {
                nXLeafParams_.push_back( 0 );
            }
            if ( machine_->getIsYMlc() ) {
                nYLeafParams_.push_back( machine_->getNYLeaves() );
            } else {
                nYLeafParams_.push_back( 0 );
            }
        } else {
            nXLeafParams_.push_back( 0 );
            nYLeafParams_.push_back( 0 );
        }

        isConstantDoseRateInFd_[fd];
        isConstantGantryInFd_[fd];
        isConstantCollInFd_[fd];
        isConstantCouchInFd_[fd];
        isConstantXJawInFd_[fd];
        isConstantYJawInFd_[fd];
        isConstantXLeafInFd_[fd];
        isConstantYLeafInFd_[fd];

		for ( unsigned int cpi=1; cpi<ncp_[fd]; cpi++ ) {
            nWeightingParams_.push_back( 1 );
            if ( !isConstantEnergyInFd_[fd] ) {
                nBeamEnergyParams_.push_back( 1 );
            } else {
                nBeamEnergyParams_.push_back( 0 );
            }
            if ( !isConstantDoseRateInFd_[fd] ) {
                nDoseRateParams_.push_back( 1 );
            } else {
                nDoseRateParams_.push_back( 0 );
            }
            if ( !isConstantGantryInFd_[fd] ) {
                nGantryParams_.push_back( 1 );
            } else {
                nGantryParams_.push_back( 0 );
            }
            if ( !isConstantCollInFd_[fd] ) {
                nCollimatorParams_.push_back( 1 );
            } else {
                nCollimatorParams_.push_back( 0 );
            }
            if ( !isConstantCouchInFd_[fd] ) {
                nCouchParams_.push_back( 1 );
            } else {
                nCouchParams_.push_back( 0 );
            }
            if ( !isConstantXJawInFd_[fd] ) {
                nXJawParams_.push_back( machine_->getNXJaws() );
            } else {
                nXJawParams_.push_back( 0 );
            }
            if ( !isConstantYJawInFd_[fd] ) {
                nYJawParams_.push_back( machine_->getNYJaws() );
            } else {
                nYJawParams_.push_back( 0 );
            }
            if ( !isConstantXLeafInFd_[fd] ) {
                nXLeafParams_.push_back( machine_->getNXLeaves() );
            } else {
                nXLeafParams_.push_back( 0 );
            }
            if ( !isConstantYLeafInFd_[fd] ) {
                nYLeafParams_.push_back( machine_->getNYLeaves() );
            } else {
                nYLeafParams_.push_back( 0 );
            }
        }
    }

    // Count number of parameters required
    paramPerCP_.clear();
    paramPerCP_.reserve( ncpTot_ );
    for ( unsigned int cpi=0; cpi<ncpTot_; cpi++ ) {
	    paramPerCP_.push_back( nWeightingParams_[cpi] );
        paramPerCP_[cpi] += nBeamEnergyParams_[cpi];
        paramPerCP_[cpi] += nDoseRateParams_[cpi];
        paramPerCP_[cpi] += nGantryParams_[cpi];
        paramPerCP_[cpi] += nCollimatorParams_[cpi];
        paramPerCP_[cpi] += nCouchParams_[cpi];
        paramPerCP_[cpi] += nXJawParams_[cpi];
        paramPerCP_[cpi] += nYJawParams_[cpi];
        paramPerCP_[cpi] += nXLeafParams_[cpi];
        paramPerCP_[cpi] += nYLeafParams_[cpi];
    }
}


/**
 * Initializes a set of parameters describing a control point.
 * Based on the nParams field for each parameter type.
 */
void DirectParameterVector::initializeCPParams( unsigned int cpi ) {

    //  ------------------------  Weighting parameters ------------------------- //
    assert( nWeightingParams_[cpi] == 1 );
	setParam( get_PInd_Initial(cpi, Weight), numeric_limits<int>::infinity(), 0.0f, numeric_limits<float>::infinity(),
               machine_->get_cpTimeRes(), Weight );

    //  ------------------------  Beam energy parameters ------------------------- //
	assert( nBeamEnergyParams_[cpi] <= 1 );
	if ( nBeamEnergyParams_[cpi] == 1 ) {
        if ( machine_->getBeamEnergy()->getIsContinuouslyVariable() ) {
            setParam( get_PInd_Initial(cpi, BeamEnergy), machine_->getBeamEnergy()->getNumValues(),
                    machine_->getBeamEnergy()->getMinValue(), machine_->getBeamEnergy()->getMaxValue(),
                    machine_->getBeamEnergy()->getStepSize(), BeamEnergy );
        } else {
            setParam( get_PInd_Initial(cpi, BeamEnergy), machine_->getBeamEnergy()->getNumValues(),
                    machine_->getBeamEnergy()->getMinValue(), machine_->getBeamEnergy()->getMaxValue(),
                    -1.0f, machine_->getBeamEnergy()->getAllValues(), BeamEnergy);
        }
	}
    //  ------------------------  Dose Rate parameters ------------------------- //
	assert( nDoseRateParams_[cpi] <= 1 );
    if ( nDoseRateParams_[cpi] == 1 ) {
        if ( machine_->getDoseRate()->getIsContinuouslyVariable() ) {
            setParam( get_PInd_Initial(cpi, DoseRate), machine_->getDoseRate()->getNumValues(),
                    machine_->getDoseRate()->getMinValue(), machine_->getDoseRate()->getMaxValue(),
                    machine_->getDoseRate()->getStepSize(), DoseRate );
        } else {
            setParam( get_PInd_Initial(cpi, DoseRate), machine_->getDoseRate()->getNumValues(),
                    machine_->getDoseRate()->getMinValue(), machine_->getDoseRate()->getMaxValue(),
                    -1.0f, machine_->getDoseRate()->getAllValues(), DoseRate);
        }
    }
    //  ------------------------  Gantry Angle parameters ------------------------- //
	assert( nGantryParams_[cpi] <= 1 );
    if ( nGantryParams_[cpi] == 1 ) {
        if ( machine_->getGantry()->getIsContinuouslyVariable() ) {
            setParam( get_PInd_Initial(cpi, Gantry), machine_->getGantry()->getNumValues(),
                    machine_->getGantry()->getMinValue(), machine_->getGantry()->getMaxValue(),
                    machine_->getGantry()->getStepSize(), Gantry );
        } else {
            setParam( get_PInd_Initial(cpi, Gantry), machine_->getGantry()->getNumValues(),
                    machine_->getGantry()->getMinValue(), machine_->getGantry()->getMaxValue(),
                    -1.0f, machine_->getGantry()->getAllValues(), Gantry);
        }
    }
    //  ------------------------  Collimator Angle parameters ------------------------- //
	assert( nCollimatorParams_[cpi] <= 1 );
    if ( nCollimatorParams_[cpi] == 1 ) {
        if ( machine_->getCollimator()->getIsContinuouslyVariable() ) {
            setParam( get_PInd_Initial(cpi, Collimator), machine_->getCollimator()->getNumValues(),
                    machine_->getCollimator()->getMinValue(), machine_->getCollimator()->getMaxValue(),
                    machine_->getCollimator()->getStepSize(), Collimator );
        } else {
            setParam( get_PInd_Initial(cpi, Collimator), machine_->getCollimator()->getNumValues(),
                    machine_->getCollimator()->getMinValue(), machine_->getCollimator()->getMaxValue(),
                    -1.0f, machine_->getCollimator()->getAllValues(), Collimator);
        }
    }
    //  ------------------------  Couch Angle parameters ------------------------- //
	assert( nCouchParams_[cpi] <= 1 );
    if ( nCouchParams_[cpi] == 1 ) {
        if ( machine_->getCouch()->getIsContinuouslyVariable() ) {
            setParam( get_PInd_Initial(cpi, Couch), machine_->getCouch()->getNumValues(),
                    machine_->getCouch()->getMinValue(), machine_->getCouch()->getMaxValue(),
                    machine_->getCouch()->getStepSize(), Couch );
        } else {
            setParam( get_PInd_Initial(cpi, Couch), machine_->getCouch()->getNumValues(),
                    machine_->getCouch()->getMinValue(), machine_->getCouch()->getMaxValue(),
                    -1.0f, machine_->getCouch()->getAllValues(), Couch);
        }
    }

    unsigned int pInd = get_PIndCP(cpi);
	float cpCollAngle = get_Collimator(cpi) - get_dijCollimatorAngle(pInd);
    float cosHT = cos( cpCollAngle * PI / 180.0f );
    float sinHT = sin( cpCollAngle * PI / 180.0f );
    float xVert[] = {   get_minXAx( pInd ) * cosHT - get_minYAx( pInd ) * sinHT,
                        get_maxXAx( pInd ) * cosHT - get_minYAx( pInd ) * sinHT,
                        get_maxXAx( pInd ) * cosHT - get_maxYAx( pInd ) * sinHT,
                        get_minXAx( pInd ) * cosHT - get_maxYAx( pInd ) * sinHT     };
    float yVert[] = {   get_minXAx( pInd ) * sinHT + get_minYAx( pInd ) * cosHT,
                        get_maxXAx( pInd ) * sinHT + get_minYAx( pInd ) * cosHT,
                        get_maxXAx( pInd ) * sinHT + get_maxYAx( pInd ) * cosHT,
                        get_minXAx( pInd ) * sinHT + get_maxYAx( pInd ) * cosHT     };

    float minX = numeric_limits<float>::infinity();
    float maxX = -numeric_limits<float>::infinity();
    float minY = numeric_limits<float>::infinity();
    float maxY = -numeric_limits<float>::infinity();
    int indMinX, indMaxX, indMinY, indMaxY;
    for ( int cc=0; cc<4; cc++ ) {
        if ( xVert[cc] < minX ) {
            minX = xVert[cc];
            indMinX = cc;
        }
        if ( xVert[cc] > maxX ) {
            maxX = xVert[cc];
            indMaxX = cc;
        }
        if ( yVert[cc] < minY ) {
            minY = yVert[cc];
            indMinY = cc;
        }
        if ( yVert[cc] > maxY ) {
            maxY = yVert[cc];
            indMaxY = cc;
        }
    }

    //  ------------------------  X Jaw parameters ------------------------- //
	assert( nXJawParams_[cpi] <= machine_->getNXJaws() );
	if ( nXJawParams_[cpi] > 0 ) {
	    pInd = get_PInd_Initial(cpi, XJaw);
        for ( unsigned int jw=0; jw<nXJawParams_[cpi]; jw++ ) {
            if ( machine_->getXJaw(jw)->getIsContinuouslyVariable() ) {
                setParam( pInd++, machine_->getXJaw(jw)->getNumValues(),
                    max(minX, machine_->getXJaw(jw)->getMinValue()), min(maxX, machine_->getXJaw(jw)->getMaxValue()),
                    machine_->getXJaw(jw)->getStepSize(), XJaw );
            } else {
                setParam( pInd++, machine_->getXJaw(jw)->getNumValues(),
                    max(minX, machine_->getXJaw(jw)->getMinValue()), min(maxX, machine_->getXJaw(jw)->getMaxValue()),
                   -1.0f, machine_->getXJaw(jw)->getAllValues(), XJaw);
            }
        }
        pInd = get_PInd_Initial(cpi, XJaw);
        value_[ pInd ] = max( minX, machine_->getXJaw(0)->getMinValue() );
        value_[ pInd+1 ] = min( maxX, machine_->getXJaw(1)->getMaxValue() );
    }
    //  ------------------------  Y Jaw parameters ------------------------- //
	assert( nYJawParams_[cpi] <= machine_->getNYJaws() );
	if ( nYJawParams_[cpi] > 0 ) {
        pInd = get_PInd_Initial(cpi, YJaw);
        for ( unsigned int jw=0; jw<nYJawParams_[cpi]; jw++ ) {
            if ( machine_->getYJaw(jw)->getIsContinuouslyVariable() ) {
                setParam( pInd++, machine_->getYJaw(jw)->getNumValues(),
                    max(minY, machine_->getYJaw(jw)->getMinValue()), min(maxY, machine_->getYJaw(jw)->getMaxValue()),
                    machine_->getYJaw(jw)->getStepSize(), YJaw );
            } else {
                setParam( pInd++, machine_->getYJaw(jw)->getNumValues(),
                    max(minY, machine_->getYJaw(jw)->getMinValue()), min(maxY, machine_->getYJaw(jw)->getMaxValue()),
                    -1.0f, machine_->getYJaw(jw)->getAllValues(), YJaw);
            }
        }
	    pInd = get_PInd_Initial(cpi, YJaw);
        value_[ pInd ] = max( minY, machine_->getYJaw(0)->getMinValue() );
        value_[ pInd+1 ]  = min( maxY, machine_->getYJaw(1)->getMaxValue() );
    }
    //  ------------------------  X Leaf parameters ------------------------- //
	assert( nXLeafParams_[cpi] <= machine_->getNXLeaves() );
	if ( nXLeafParams_[cpi] > 0 ) {
        pInd = get_PInd_Initial(cpi, XLeaf);

        // Find vertices of bixel map which define bixel map bounds for each leaf
        float x1, y1, x2, y2, x3, y3, x4, y4;
        x1 = xVert[ indMinX ];   y1 = yVert[ indMinX ];
        x3 = xVert[ indMaxX ];   y3 = yVert[ indMaxX ];

        unsigned int indMinNxt = indMinX+1 > 3 ? 0 : indMinX+1;
        unsigned int indMinPrv = indMinX-1 < 0 ? 3 : indMinX-1;
        unsigned int indMaxNxt = indMaxX+1 > 3 ? 0 : indMaxX+1;
        unsigned int indMaxPrv = indMaxX-1 < 0 ? 3 : indMaxX-1;
        for ( unsigned int lf=0; lf<nXLeafParams_[cpi]; lf++ ) {
            float yL = machine_->getXLeaf(lf)->getPosition();

            minX = machine_->getXLeaf(lf)->getMinValue();
            maxX = machine_->getXLeaf(lf)->getMaxValue();
            bool insideBixels = false;
            if ( (yL > yVert[indMinY]) && (yL < yVert[indMaxY]) ) {
                insideBixels = true;
                unsigned int indVert2, indVert4;
                if ( yL > y1 ) indVert2 = yVert[indMinNxt] > yVert[indMinPrv] ? indMinNxt : indMinPrv;
                else indVert2 = yVert[indMinNxt] < yVert[indMinPrv] ? indMinNxt : indMinPrv;
                x2 = xVert[ indVert2 ];     y2 = yVert[ indVert2 ];
                if ( yL > y1 ) indVert4 = yVert[indMaxNxt] > yVert[indMaxPrv] ? indMaxNxt : indMaxPrv;
                else indVert4 = yVert[indMaxNxt] < yVert[indMaxPrv] ? indMaxNxt : indMaxPrv;
                x4 = xVert[ indVert4 ];     y4 = yVert[ indVert4 ];

                // Find point on bixel map bounds corresponding to leaf travel line and find limits
                minX = max( minX, ( (x2-x1) / (y2-y1) ) * (yL-y1) + x2);
                maxX = min( maxX, ( (x4-x3) / (y4-y3) ) * (yL-y3) + x4);

                assert( minX < maxX );
            }

            if ( machine_->getXLeaf(lf)->getIsContinuouslyVariable() ) {
                setParam( pInd, machine_->getXLeaf(lf)->getNumValues(), minX, maxX,
                         machine_->getXLeaf(lf)->getStepSize(), XLeaf );
            } else {
                setParam( pInd, machine_->getXLeaf(lf)->getNumValues(), minX, maxX, -1.0f,
                         machine_->getXLeaf(lf)->getAllValues(), XLeaf);
            }

            isActive_[pInd] = insideBixels;
            if ( insideBixels ) {
                if ( lf < nXLeafParams_[cpi]/2 ) value_[ pInd ] = max( minX , machine_->getXLeaf(lf)->getMinValue() );
                else value_[ pInd ] = min( maxX, machine_->getXLeaf(lf)->getMaxValue() );
            } else {
                if ( lf < nXLeafParams_[cpi]/2 ) value_[ pInd ] = -(machine_->getXLeaf(lf)->getMinGapOpposing() / 2);
                else value_[ pInd ] = +(machine_->getXLeaf(lf)->getMinGapOpposing() / 2);
            }

            pInd++;
        }
        /*cout << endl;
        if ( cpi == 0 || cpi == 1 || cpi == 2 ) {
            for (unsigned int pp=0; pp<nXLeafParams_[cpi]; pp++) {
                if ( pp == 0 || pp == 19 || pp == 40 || pp == 59 ) {
                    int pLf = get_PInd_XLeaf(cpi, pp);
                    cout << pp << " : " << machine_->getXLeaf(pp)->getPosition() << " : " << value_[pLf] << " : " << isActive_[pLf] << " : " << absLowBound_[pLf] << " : " << absHighBound_[pLf] << endl;
                }
            }
        }*/
	}
    //  ------------------------  Y Leaf parameters ------------------------- //
	assert( nYLeafParams_[cpi] <= machine_->getNYLeaves() );
	if ( nYLeafParams_[cpi] > 0 ) {
        pInd = get_PInd_Initial(cpi, YLeaf);
        // Find vertices of bixel map which define bixel map bounds for each leaf
        float x1, y1, x2, y2, x3, y3, x4, y4;
        x1 = xVert[ indMinY ];   y1 = yVert[ indMinY ];
        x3 = xVert[ indMaxY ];   y3 = yVert[ indMaxY ];

        unsigned int indMinNxt = indMinY+1 > 3 ? 0 : indMinY+1;
        unsigned int indMinPrv = indMinY-1 < 0 ? 3 : indMinY-1;
        unsigned int indMaxNxt = indMaxY+1 > 3 ? 0 : indMaxY+1;
        unsigned int indMaxPrv = indMaxY-1 < 0 ? 3 : indMaxY-1;
        for ( unsigned int lf=0; lf<nYLeafParams_[cpi]; lf++ ) {
            float xL = machine_->getYLeaf(lf)->getPosition();

            minY = machine_->getYLeaf(lf)->getMinValue();
            maxY = machine_->getYLeaf(lf)->getMaxValue();
            bool insideBixels = false;
            if ( (xL > xVert[indMinX]) && (xL < xVert[indMaxX]) ) {
                insideBixels = true;
                unsigned int indVert2, indVert4;
                if ( xL > x1 ) indVert2 = xVert[indMinNxt] > xVert[indMinPrv] ? indMinNxt : indMinPrv;
                else indVert2 = xVert[indMinNxt] < xVert[indMinPrv] ? indMinNxt : indMinPrv;
                x2 = xVert[ indVert2 ];     y2 = yVert[ indVert2 ];
                if ( xL > x1 ) indVert4 = xVert[indMaxNxt] > xVert[indMaxPrv] ? indMaxNxt : indMaxPrv;
                else indVert4 = xVert[indMaxNxt] < xVert[indMaxPrv] ? indMaxNxt : indMaxPrv;
                x4 = xVert[ indVert4 ];     y4 = yVert[ indVert4 ];

                // Find point on bixel map bounds corresponding to leaf travel line and find limits
                minY = max( minY, ( (y2-y1) / (x2-x1) ) * (xL-x1) + y2);
                maxY = min( maxY, ( (y2-y3) / (x2-x3) ) * (xL-x3) + y2);

                assert( minY < maxY );
            }
            if ( machine_->getYLeaf(lf)->getIsContinuouslyVariable() ) {
                setParam( pInd, machine_->getYLeaf(lf)->getNumValues(), minY, maxY,
                        machine_->getYLeaf(lf)->getStepSize(), YLeaf );
            } else {
                setParam( pInd, machine_->getYLeaf(lf)->getNumValues(), minY, maxY, -1.0f,
                         machine_->getYLeaf(lf)->getAllValues(), YLeaf);
            }

            isActive_[pInd] = insideBixels;
            if ( insideBixels ) {
                if ( lf < nYLeafParams_[cpi]/2 ) value_[ pInd ] = max( minY , machine_->getYLeaf(lf)->getMinValue() );
                else value_[ pInd ] = min( maxY, machine_->getYLeaf(lf)->getMaxValue() );
            } else {
                if ( lf < nYLeafParams_[cpi]/2 ) value_[ pInd ] = -(machine_->getYLeaf(lf)->getMinGapOpposing() / 2);
                else value_[ pInd ] = +(machine_->getYLeaf(lf)->getMinGapOpposing() / 2);
            }
            pInd++;
        }
	}
}


/// Get the parameter set which describes one field of the plan.
DirectParameterVector * DirectParameterVector::copy_field( int fdCopy ) const {
    DirectParameterVector * dpv = new DirectParameterVector(*this);

    for ( int fd = nf_-1; fd>=0; --fd ) {
        if ( fd == fdCopy ) continue;
        dpv->remove_field( fd );
    }

    return dpv;
}


/// Get the parameter set which describes one control point of the plan.
DirectParameterVector * DirectParameterVector::copy_cp( int cpCopy ) const {
    DirectParameterVector * dpv = new DirectParameterVector(*this);

    for ( int cp = ncpTot_-1; cp>=0; --cp ) {
        if ( cp == cpCopy ) continue;
        dpv->remove_cp( cp );
    }

    return dpv;
}


/// Get the parameter set which describes one control point of one field.
DirectParameterVector * DirectParameterVector::copy_cp( int fdCopy, int cpCopy ) const {
    DirectParameterVector * dpv = new DirectParameterVector(*this);

    for ( int fd=nf_-1; fd>=0; --fd ) {
        if ( fd == fdCopy ) continue;
        dpv->remove_field( fd );
    }
    for ( int cp=ncp_[fdCopy]-1; cp>=0; --cp ) {
        if ( cp == cpCopy ) continue;
        dpv->remove_cp( cp );
    }
    return dpv;
}


/// Add a new field to the parameter set after a given field index.
void DirectParameterVector::add_field(unsigned int fdInd, const DirectParameterVector & fields) {
    assert( fdInd <= nf_ );

    // Create a working copy of the current parameter vector
    auto_ptr<DirectParameterVector> tmp;
    tmp.reset( new DirectParameterVector(*this) );

    size_t cpInd, pInd;
    if (fdInd < (nf_-1)) {
        // Find the corresponding control point index to the field insertion point
        cpInd = tmp->get_absCPindex( fdInd + 1 , 0 ) - 1;
        // Find the corresponding parameter index to the field insertion point
        pInd = tmp->get_PIndField( fdInd + 1 ) - 1;
    } else {
        // Set to last control point
        cpInd = ncpTot_ - 1;
        // Set to last parameter
        pInd = nParams_ - 1;
    }

    nf_ += fields.nf_;
    ncpTot_ += fields.ncpTot_;

    // Resize vectors
    ncp_.reserve( nf_ );
    nParams_ = tmp->get_nParameters() + fields.get_nParameters();
    reserve_nParams(nParams_);

    isConstantEnergyInFd_.reserve( nf_ );
    isConstantDoseRateInFd_.reserve( nf_ );
    isConstantGantryInFd_.reserve( nf_ );
    isConstantCollInFd_.reserve( nf_ );
    isConstantCouchInFd_.reserve( nf_ );
    isConstantXJawInFd_.reserve( nf_ );
    isConstantYJawInFd_.reserve( nf_ );
    isConstantXLeafInFd_.reserve( nf_ );
    isConstantYLeafInFd_.reserve( nf_ );

    nWeightingParams_.reserve( ncpTot_ );
    nBeamEnergyParams_.reserve( ncpTot_ );
    nDoseRateParams_.reserve( ncpTot_ );
    nGantryParams_.reserve( ncpTot_ );
    nCollimatorParams_.reserve( ncpTot_ );
    nCouchParams_.reserve( ncpTot_ );
    nXJawParams_.reserve( ncpTot_ );
    nYJawParams_.reserve( ncpTot_ );
    nXLeafParams_.reserve( ncpTot_ );
    nYLeafParams_.reserve( ncpTot_ );

    // Repopulate per field arrays
    unsigned int fdIndNew = 0;
    for ( unsigned int fd=0; fd<=fdInd; fd++ ) {
        ncp_[fdIndNew] = tmp->ncp_[fd];
        isConstantEnergyInFd_[fdIndNew] = tmp->isConstantEnergyInFd_[fd];
        isConstantDoseRateInFd_[fdIndNew] = tmp->isConstantDoseRateInFd_[fd];
        isConstantGantryInFd_[fdIndNew] = tmp->isConstantGantryInFd_[fd];
        isConstantCollInFd_[fdIndNew] = tmp->isConstantCollInFd_[fd];
        isConstantCouchInFd_[fdIndNew] = tmp->isConstantCouchInFd_[fd];
        isConstantXJawInFd_[fdIndNew] = tmp->isConstantXJawInFd_[fd];
        isConstantYJawInFd_[fdIndNew] = tmp->isConstantYJawInFd_[fd];
        isConstantXLeafInFd_[fdIndNew] = tmp->isConstantXLeafInFd_[fd];
        isConstantYLeafInFd_[fdIndNew] = tmp->isConstantYLeafInFd_[fd];
        fdIndNew++;
    }
    for ( unsigned int fd=0; fd<fields.nf_; fd++ ) {
        ncp_[fdIndNew++] = fields.ncp_[fd];
        isConstantEnergyInFd_[fdIndNew] = fields.isConstantEnergyInFd_[fd];
        isConstantDoseRateInFd_[fdIndNew] = fields.isConstantDoseRateInFd_[fd];
        isConstantGantryInFd_[fdIndNew] = fields.isConstantGantryInFd_[fd];
        isConstantCollInFd_[fdIndNew] = fields.isConstantCollInFd_[fd];
        isConstantCouchInFd_[fdIndNew] = fields.isConstantCouchInFd_[fd];
        isConstantXJawInFd_[fdIndNew] = fields.isConstantXJawInFd_[fd];
        isConstantYJawInFd_[fdIndNew] = fields.isConstantYJawInFd_[fd];
        isConstantXLeafInFd_[fdIndNew] = fields.isConstantXLeafInFd_[fd];
        isConstantYLeafInFd_[fdIndNew] = fields.isConstantYLeafInFd_[fd];
        fdIndNew++;
    }
    for ( unsigned int fd=fdInd+1; fd<tmp->nf_; fd++ ) {
        ncp_[fdIndNew++] = tmp->ncp_[fd];
        isConstantEnergyInFd_[fdIndNew] = tmp->isConstantEnergyInFd_[fd];
        isConstantDoseRateInFd_[fdIndNew] = tmp->isConstantDoseRateInFd_[fd];
        isConstantGantryInFd_[fdIndNew] = tmp->isConstantGantryInFd_[fd];
        isConstantCollInFd_[fdIndNew] = tmp->isConstantCollInFd_[fd];
        isConstantCouchInFd_[fdIndNew] = tmp->isConstantCouchInFd_[fd];
        isConstantXJawInFd_[fdIndNew] = tmp->isConstantXJawInFd_[fd];
        isConstantYJawInFd_[fdIndNew] = tmp->isConstantYJawInFd_[fd];
        isConstantXLeafInFd_[fdIndNew] = tmp->isConstantXLeafInFd_[fd];
        isConstantYLeafInFd_[fdIndNew] = tmp->isConstantYLeafInFd_[fd];
        fdIndNew++;
    }

    // Repopulate per control point arrays
    unsigned int cpIndNew = 0;
    for ( unsigned int cp=0; cp<=cpInd; cp++ ) {
        paramPerCP_[cpIndNew] = tmp->paramPerCP_[cp];
        nWeightingParams_[cpIndNew] = tmp->nWeightingParams_[cp];
        nBeamEnergyParams_[cpIndNew] = tmp->nBeamEnergyParams_[cp];
        nDoseRateParams_[cpIndNew] = tmp->nDoseRateParams_[cp];
        nGantryParams_[cpIndNew] = tmp->nGantryParams_[cp];
        nCollimatorParams_[cpIndNew] = tmp->nCollimatorParams_[cp];
        nCouchParams_[cpIndNew] = tmp->nCouchParams_[cp];
        nXJawParams_[cpIndNew] = tmp->nXJawParams_[cp];
        nYJawParams_[cpIndNew] = tmp->nYJawParams_[cp];
        nXLeafParams_[cpIndNew] = tmp->nXLeafParams_[cp];
        nYLeafParams_[cpIndNew] = tmp->nYLeafParams_[cp];
        cpIndNew++;
    }
    for ( unsigned int cp=0; cp<fields.ncpTot_; cp++ ) {
        paramPerCP_[cpIndNew] = fields.paramPerCP_[cp];
        nWeightingParams_[cpIndNew] = fields.nWeightingParams_[cp];
        nBeamEnergyParams_[cpIndNew] = fields.nBeamEnergyParams_[cp];
        nDoseRateParams_[cpIndNew] = fields.nDoseRateParams_[cp];
        nGantryParams_[cpIndNew] = fields.nGantryParams_[cp];
        nCollimatorParams_[cpIndNew] = fields.nCollimatorParams_[cp];
        nCouchParams_[cpIndNew] = fields.nCouchParams_[cp];
        nXJawParams_[cpIndNew] = fields.nXJawParams_[cp];
        nYJawParams_[cpIndNew] = fields.nYJawParams_[cp];
        nXLeafParams_[cpIndNew] = fields.nXLeafParams_[cp];
        nYLeafParams_[cpIndNew] = fields.nYLeafParams_[cp];
        cpIndNew++;
    }
    for ( unsigned int cp=cpInd+1; cp<tmp->ncpTot_; cp++ ) {
        paramPerCP_[cpIndNew] = tmp->paramPerCP_[cp];
        nWeightingParams_[cpIndNew] = tmp->nWeightingParams_[cp];
        nBeamEnergyParams_[cpIndNew] = tmp->nBeamEnergyParams_[cp];
        nDoseRateParams_[cpIndNew] = tmp->nDoseRateParams_[cp];
        nGantryParams_[cpIndNew] = tmp->nGantryParams_[cp];
        nCollimatorParams_[cpIndNew] = tmp->nCollimatorParams_[cp];
        nCouchParams_[cpIndNew] = tmp->nCouchParams_[cp];
        nXJawParams_[cpIndNew] = tmp->nXJawParams_[cp];
        nYJawParams_[cpIndNew] = tmp->nYJawParams_[cp];
        nXLeafParams_[cpIndNew] = tmp->nXLeafParams_[cp];
        nYLeafParams_[cpIndNew] = tmp->nYLeafParams_[cp];
        cpIndNew++;
    }

    // Repopulate per parameter arrays
    unsigned int pIndNew = 0;
    for ( unsigned int pp=0; pp<=pInd; pp++ ) {
        value_[pIndNew] = tmp->value_[pp];
        lowBound_[pIndNew] = tmp->lowBound_[pp];
        highBound_[pIndNew] = tmp->highBound_[pp];
        isActive_[pIndNew] = tmp->isActive_[pp];
        step_size_[pIndNew] = tmp->step_size_[pp];
        absHighBound_[pIndNew] = tmp->absHighBound_[pp];
        absLowBound_[pIndNew] = tmp->absLowBound_[pp];
        isDiscreteVariable_[pIndNew] = tmp->isDiscreteVariable_[pp];
        numberOfValidValues_[pIndNew] = tmp->numberOfValidValues_[pp];
        type_[pIndNew] = tmp->type_[pp];
        if ( tmp->validValues_.find(pp) != tmp->validValues_.end() )
            validValues_[pIndNew] = tmp->validValues_.find(pp)->second;
        pIndNew++;
    }
    for ( unsigned int pp=0; pp<fields.nParams_; pp++ ) {
        value_[pIndNew] = fields.value_[pp];
        lowBound_[pIndNew] = fields.lowBound_[pp];
        highBound_[pIndNew] = fields.highBound_[pp];
        isActive_[pIndNew] = fields.isActive_[pp];
        step_size_[pIndNew] = fields.step_size_[pp];
        absHighBound_[pIndNew] = fields.absHighBound_[pp];
        absLowBound_[pIndNew] = fields.absLowBound_[pp];
        isDiscreteVariable_[pIndNew] = fields.isDiscreteVariable_[pp];
        numberOfValidValues_[pIndNew] = fields.numberOfValidValues_[pp];
        type_[pIndNew] = fields.type_[pp];
        if ( fields.validValues_.find(pp) != fields.validValues_.end() )
            validValues_[pIndNew] = fields.validValues_.find(pp)->second;
        pIndNew++;
    }
    for ( unsigned int pp=pInd+1; pp<tmp->ncpTot_; pp++ ) {
        value_[pIndNew] = tmp->value_[pp];
        lowBound_[pIndNew] = tmp->lowBound_[pp];
        highBound_[pIndNew] = tmp->highBound_[pp];
        isActive_[pIndNew] = tmp->isActive_[pp];
        step_size_[pIndNew] = tmp->step_size_[pp];
        absHighBound_[pIndNew] = tmp->absHighBound_[pp];
        absLowBound_[pIndNew] = tmp->absLowBound_[pp];
        isDiscreteVariable_[pIndNew] = tmp->isDiscreteVariable_[pp];
        numberOfValidValues_[pIndNew] = tmp->numberOfValidValues_[pp];
        type_[pIndNew] = tmp->type_[pp];
        if ( tmp->validValues_.find(pp) != tmp->validValues_.end() )
            validValues_[pIndNew] = tmp->validValues_.find(pp)->second;
        pIndNew++;
    }

    resetLimits();
}

/// Add a new control point into the parameter set after a given control point index.
void DirectParameterVector::add_cp(unsigned int cpInd, const DirectParameterVector & cpPoints) {
    assert( cpInd < ncpTot_ );
    assert( cpPoints.nf_ < 1 );

    // Create a working copy of the current parameter vector
    auto_ptr<DirectParameterVector> tmp( new DirectParameterVector(*this) );
    auto_ptr<DirectParameterVector> cpTmp( new DirectParameterVector(cpPoints) );

    // Find the corresponding parameter index to the field insertion point
    size_t curFd = tmp->get_Fieldindex( cpInd );
    size_t pInd = ( cpInd < ( ncpTot_ - 1 ) ) ? tmp->get_PIndCP( cpInd + 1 ) - 1 : nParams_ - 1;

    // Remove field weighting parameter if present
    for ( unsigned int cp=0; cp<cpTmp->ncpTot_; cp++ )
        if ( cpTmp->nWeightingParams_[cp] > 1 )
            cpTmp->remove_param( cpTmp->get_PInd( cp, Weight, 0 ) );

    ncpTot_ += cpTmp->ncpTot_;
    ncp_[curFd] += cpTmp->ncpTot_;

    // Resize vectors
    nParams_ = tmp->get_nParameters() + cpTmp->get_nParameters();
    reserve_nParams( get_nParameters() );
    nWeightingParams_.reserve( ncpTot_ );
    nBeamEnergyParams_.reserve( ncpTot_ );
    nDoseRateParams_.reserve( ncpTot_ );
    nGantryParams_.reserve( ncpTot_ );
    nCollimatorParams_.reserve( ncpTot_ );
    nCouchParams_.reserve( ncpTot_ );
    nXJawParams_.reserve( ncpTot_ );
    nYJawParams_.reserve( ncpTot_ );
    nXLeafParams_.reserve( ncpTot_ );
    nYLeafParams_.reserve( ncpTot_ );

    // Repopulate per control point arrays
    unsigned int cpIndNew = 0;
    for ( unsigned int cp=0; cp<=cpInd; cp++ ) {
        paramPerCP_[cpIndNew] = tmp->paramPerCP_[cp];
        nWeightingParams_[cpIndNew] = tmp->nWeightingParams_[cp];
        nBeamEnergyParams_[cpIndNew] = tmp->nBeamEnergyParams_[cp];
        nDoseRateParams_[cpIndNew] = tmp->nDoseRateParams_[cp];
        nGantryParams_[cpIndNew] = tmp->nGantryParams_[cp];
        nCollimatorParams_[cpIndNew] = tmp->nCollimatorParams_[cp];
        nCouchParams_[cpIndNew] = tmp->nCouchParams_[cp];
        nXJawParams_[cpIndNew] = tmp->nXJawParams_[cp];
        nYJawParams_[cpIndNew] = tmp->nYJawParams_[cp];
        nXLeafParams_[cpIndNew] = tmp->nXLeafParams_[cp];
        nYLeafParams_[cpIndNew] = tmp->nYLeafParams_[cp];
        cpIndNew++;
    }
    for ( unsigned int cp=0; cp<cpTmp->ncpTot_; cp++ ) {
        paramPerCP_[cpIndNew] = cpTmp->paramPerCP_[cp];
        nWeightingParams_[cpIndNew] = cpTmp->nWeightingParams_[cp];
        nBeamEnergyParams_[cpIndNew] = cpTmp->nBeamEnergyParams_[cp];
        nDoseRateParams_[cpIndNew] = cpTmp->nDoseRateParams_[cp];
        nGantryParams_[cpIndNew] = cpTmp->nGantryParams_[cp];
        nCollimatorParams_[cpIndNew] = cpTmp->nCollimatorParams_[cp];
        nCouchParams_[cpIndNew] = cpTmp->nCouchParams_[cp];
        nXJawParams_[cpIndNew] = cpTmp->nXJawParams_[cp];
        nYJawParams_[cpIndNew] = cpTmp->nYJawParams_[cp];
        nXLeafParams_[cpIndNew] = cpTmp->nXLeafParams_[cp];
        nYLeafParams_[cpIndNew] = cpTmp->nYLeafParams_[cp];
        cpIndNew++;
    }
    for ( unsigned int cp=cpInd+1; cp<tmp->ncpTot_; cp++ ) {
        paramPerCP_[cpIndNew] = tmp->paramPerCP_[cp];
        nWeightingParams_[cpIndNew] = tmp->nWeightingParams_[cp];
        nBeamEnergyParams_[cpIndNew] = tmp->nBeamEnergyParams_[cp];
        nDoseRateParams_[cpIndNew] = tmp->nDoseRateParams_[cp];
        nGantryParams_[cpIndNew] = tmp->nGantryParams_[cp];
        nCollimatorParams_[cpIndNew] = tmp->nCollimatorParams_[cp];
        nCouchParams_[cpIndNew] = tmp->nCouchParams_[cp];
        nXJawParams_[cpIndNew] = tmp->nXJawParams_[cp];
        nYJawParams_[cpIndNew] = tmp->nYJawParams_[cp];
        nXLeafParams_[cpIndNew] = tmp->nXLeafParams_[cp];
        nYLeafParams_[cpIndNew] = tmp->nYLeafParams_[cp];
        cpIndNew++;
    }

    // Repopulate per parameter arrays
    unsigned int pIndNew = 0;
    for ( unsigned int pp=0; pp<=pInd; pp++ ) {
        value_[pIndNew] = tmp->value_[pp];
        lowBound_[pIndNew] = tmp->lowBound_[pp];
        highBound_[pIndNew] = tmp->highBound_[pp];
        isActive_[pIndNew] = tmp->isActive_[pp];
        step_size_[pIndNew] = tmp->step_size_[pp];
        absHighBound_[pIndNew] = tmp->absHighBound_[pp];
        absLowBound_[pIndNew] = tmp->absLowBound_[pp];
        isDiscreteVariable_[pIndNew] = tmp->isDiscreteVariable_[pp];
        numberOfValidValues_[pIndNew] = tmp->numberOfValidValues_[pp];
        type_[pIndNew] = tmp->type_[pp];
        if ( tmp->validValues_.find(pp) != tmp->validValues_.end() )
            validValues_[pIndNew] = tmp->validValues_.find(pp)->second;
        pIndNew++;
    }
    for ( unsigned int pp=0; pp<cpTmp->nParams_; pp++ ) {
        value_[pIndNew] = cpTmp->value_[pp];
        lowBound_[pIndNew] = cpTmp->lowBound_[pp];
        highBound_[pIndNew] = cpTmp->highBound_[pp];
        isActive_[pIndNew] = cpTmp->isActive_[pp];
        step_size_[pIndNew] = cpTmp->step_size_[pp];
        absHighBound_[pIndNew] = cpTmp->absHighBound_[pp];
        absLowBound_[pIndNew] = cpTmp->absLowBound_[pp];
        isDiscreteVariable_[pIndNew] = cpTmp->isDiscreteVariable_[pp];
        numberOfValidValues_[pIndNew] = cpTmp->numberOfValidValues_[pp];
        type_[pIndNew] = cpTmp->type_[pp];
        if ( cpTmp->validValues_.find(pp) != cpTmp->validValues_.end() )
            validValues_[pIndNew] = cpTmp->validValues_.find(pp)->second;
        pIndNew++;
    }
    for ( unsigned int pp=pInd+1; pp<tmp->nParams_; pp++ ) {
        value_[pIndNew] = tmp->value_[pp];
        lowBound_[pIndNew] = tmp->lowBound_[pp];
        highBound_[pIndNew] = tmp->highBound_[pp];
        isActive_[pIndNew] = tmp->isActive_[pp];
        step_size_[pIndNew] = tmp->step_size_[pp];
        absHighBound_[pIndNew] = tmp->absHighBound_[pp];
        absLowBound_[pIndNew] = tmp->absLowBound_[pp];
        isDiscreteVariable_[pIndNew] = tmp->isDiscreteVariable_[pp];
        numberOfValidValues_[pIndNew] = tmp->numberOfValidValues_[pp];
        type_[pIndNew] = tmp->type_[pp];
        if ( tmp->validValues_.find(pp) != tmp->validValues_.end() )
            validValues_[pIndNew] = tmp->validValues_.find(pp)->second;
        pIndNew++;
    }
    resetLimits();
}

/// Add a new control point into the parameter set for a given field after a given control point index.
void DirectParameterVector::add_cp(unsigned int fd, unsigned int cpi, const DirectParameterVector & cpPoints) {
    add_cp( get_absCPindex( fd, cpi ), cpPoints );
}

/// Remove a field from the parameter set.
void DirectParameterVector::remove_field(unsigned int fdInd) {
    assert( fdInd < nf_ );

    size_t fromCpInd = get_absCPindex( fdInd, 0 );
    size_t toCpind = fromCpInd + ncp_[fdInd];
    size_t fromPInd = get_PIndField( fdInd );
    size_t toPInd = (fdInd<nf_-1) ? get_PIndField( fdInd + 1 ) - 1 : nParams_;

    --nf_;
    isConstantEnergyInFd_.erase( isConstantEnergyInFd_.begin() + fdInd );
    isConstantDoseRateInFd_.erase( isConstantDoseRateInFd_.begin() + fdInd );
    isConstantGantryInFd_.erase( isConstantGantryInFd_.begin() + fdInd );
    isConstantCollInFd_.erase( isConstantCollInFd_.begin() + fdInd );
    isConstantCouchInFd_.erase( isConstantCouchInFd_.begin() + fdInd );
    isConstantXJawInFd_.erase( isConstantXJawInFd_.begin() + fdInd );
    isConstantYJawInFd_.erase( isConstantYJawInFd_.begin() + fdInd );
    isConstantXLeafInFd_.erase( isConstantXLeafInFd_.begin() + fdInd );
    isConstantYLeafInFd_.erase( isConstantYLeafInFd_.begin() + fdInd );

    // Delete control point records for deleted field.
    for ( unsigned int pp=fromPInd; pp<=toPInd; pp++ ) {
        value_.erase( value_.begin() + pp );
        lowBound_.erase( lowBound_.begin() + pp );
        highBound_.erase( highBound_.begin() + pp );
        isActive_.erase( isActive_.begin() + pp );
        step_size_.erase( step_size_.begin() + pp );
        absHighBound_.erase( absHighBound_.begin() + pp );
        absLowBound_.erase( absLowBound_.begin() + pp );
        isDiscreteVariable_.erase( isDiscreteVariable_.begin() + pp );
        numberOfValidValues_.erase( numberOfValidValues_.begin() + pp );
        type_.erase( type_.begin() + pp );
        validValues_.erase( pp );
        --nParams_;
    }

    // Delete control point records for deleted field.
    for ( unsigned int cp=fromCpInd; cp<=toCpind; cp++ ) {
        paramPerCP_.erase( paramPerCP_.begin() + cp );
        nWeightingParams_.erase( nWeightingParams_.begin() + cp );
        nBeamEnergyParams_.erase( nBeamEnergyParams_.begin() + cp );
        nDoseRateParams_.erase( nDoseRateParams_.begin() + cp );
        nGantryParams_.erase( nGantryParams_.begin() + cp );
        nCollimatorParams_.erase( nCollimatorParams_.begin() + cp );
        nCouchParams_.erase( nCouchParams_.begin() + cp );
        nXJawParams_.erase( nXJawParams_.begin() + cp );
        nYJawParams_.erase( nYJawParams_.begin() + cp );
        nXLeafParams_.erase( nXLeafParams_.begin() + cp );
        nYLeafParams_.erase( nYLeafParams_.begin() + cp );
    }

    ncpTot_ -= ncp_[fdInd];
    ncp_.erase( ncp_.begin() + fdInd );

    resetLimits();
}

/// Remove a control point from the parameter set.
void DirectParameterVector::remove_cp(unsigned int cpInd) {
    assert( cpInd < ncpTot_ );

    size_t fromPInd = get_PIndCP( cpInd );
    size_t toPInd = ( cpInd < ncpTot_-1 ) ? get_PIndCP( cpInd + 1) - 1 : nParams_;

    // Delete control point records for deleted field.
    for ( unsigned int pp=fromPInd; pp<=toPInd; pp++ ) {
        value_.erase( value_.begin() + pp );
        lowBound_.erase( lowBound_.begin() + pp );
        highBound_.erase( highBound_.begin() + pp );
        isActive_.erase( isActive_.begin() + pp );
        step_size_.erase( step_size_.begin() + pp );
        absHighBound_.erase( absHighBound_.begin() + pp );
        absLowBound_.erase( absLowBound_.begin() + pp );
        isDiscreteVariable_.erase( isDiscreteVariable_.begin() + pp );
        numberOfValidValues_.erase( numberOfValidValues_.begin() + pp );
        type_.erase( type_.begin() + pp );
        validValues_.erase( pp );
    }

    nParams_ -= paramPerCP_[cpInd];

    paramPerCP_.erase( paramPerCP_.begin() + cpInd );
    nWeightingParams_.erase( nWeightingParams_.begin() + cpInd );
    nBeamEnergyParams_.erase( nBeamEnergyParams_.begin() + cpInd );
    nDoseRateParams_.erase( nDoseRateParams_.begin() + cpInd );
    nGantryParams_.erase( nGantryParams_.begin() + cpInd);
    nCollimatorParams_.erase( nCollimatorParams_.begin() + cpInd );
    nCouchParams_.erase( nCouchParams_.begin() + cpInd );
    nXJawParams_.erase( nXJawParams_.begin() + cpInd );
    nYJawParams_.erase( nYJawParams_.begin() + cpInd );
    nXLeafParams_.erase( nXLeafParams_.begin() + cpInd );
    nYLeafParams_.erase( nYLeafParams_.begin() + cpInd );

    --ncpTot_;
    --ncp_[cpInd];

    resetLimits();
}

/// Remove a control point from the parameter set for a given field.
void DirectParameterVector::remove_cp( unsigned int fdInd, unsigned int cpInd ) {
    remove_cp( get_absCPindex( fdInd, cpInd ) );
}

/**
 * Populate per-parameter vectors with default values, limits and properties for a given parsmeter index.
 * N.B. The parameter should already exist in the vectors and have the parameter type already set.
 */
 void DirectParameterVector::putDefaults( unsigned int pInd ) {

    size_t cpi = paramIndToCPInd(pInd);

    switch ( type_[pInd] ) {
    case Weight:
        setParam(pInd, numeric_limits<int>::infinity(), 0.0f, numeric_limits<float>::infinity(),
               machine_->get_cpTimeRes(), Weight );
        set_value(pInd, 1.0f);
        break;
    case BeamEnergy:
        if ( machine_->getBeamEnergy()->getIsContinuouslyVariable() ) {
            setParam( pInd, machine_->getBeamEnergy()->getNumValues(),
                    machine_->getBeamEnergy()->getMinValue(), machine_->getBeamEnergy()->getMaxValue(),
                    machine_->getBeamEnergy()->getStepSize(), BeamEnergy );
        } else {
            setParam( pInd, machine_->getBeamEnergy()->getNumValues(),
                    machine_->getBeamEnergy()->getMinValue(), machine_->getBeamEnergy()->getMaxValue(),
                    -1.0f, machine_->getBeamEnergy()->getAllValues(), BeamEnergy);
        }
        break;
    case DoseRate:
        if ( machine_->getDoseRate()->getIsContinuouslyVariable() ) {
            setParam( pInd, machine_->getDoseRate()->getNumValues(),
                    machine_->getDoseRate()->getMinValue(), machine_->getDoseRate()->getMaxValue(),
                    machine_->getDoseRate()->getStepSize(), DoseRate );
        } else {
            setParam( pInd, machine_->getDoseRate()->getNumValues(),
                    machine_->getDoseRate()->getMinValue(), machine_->getDoseRate()->getMaxValue(),
                    -1.0f, machine_->getDoseRate()->getAllValues(), DoseRate);
        }
        break;
    case Gantry:
        if ( machine_->getGantry()->getIsContinuouslyVariable() ) {
            setParam( pInd, machine_->getGantry()->getNumValues(),
                    machine_->getGantry()->getMinValue(), machine_->getGantry()->getMaxValue(),
                    machine_->getGantry()->getStepSize(), Gantry );
        } else {
            setParam( pInd, machine_->getGantry()->getNumValues(),
                    machine_->getGantry()->getMinValue(), machine_->getGantry()->getMaxValue(),
                    -1.0f, machine_->getGantry()->getAllValues(), Gantry);
        }
        break;
    case Collimator:
        if ( machine_->getCollimator()->getIsContinuouslyVariable() ) {
            setParam( pInd, machine_->getCollimator()->getNumValues(),
                    machine_->getCollimator()->getMinValue(), machine_->getCollimator()->getMaxValue(),
                    machine_->getCollimator()->getStepSize(), Collimator );
        } else {
            setParam( pInd, machine_->getCollimator()->getNumValues(),
                    machine_->getCollimator()->getMinValue(), machine_->getCollimator()->getMaxValue(),
                    -1.0f, machine_->getCollimator()->getAllValues(), Collimator);
        }
        break;
    case Couch:
        if ( machine_->getCouch()->getIsContinuouslyVariable() ) {
            setParam( pInd, machine_->getCouch()->getNumValues(),
                    machine_->getCouch()->getMinValue(), machine_->getCouch()->getMaxValue(),
                    machine_->getCouch()->getStepSize(), Couch );
        } else {
            setParam( pInd, machine_->getCouch()->getNumValues(),
                    machine_->getCouch()->getMinValue(), machine_->getCouch()->getMaxValue(),
                    -1.0f, machine_->getCouch()->getAllValues(), Couch);
        }
        break;
    case XJaw: {
        float cpCollAngle = get_Collimator(cpi) - get_dijCollimatorAngle(pInd);
        float cosHT = cos( cpCollAngle * PI / 180.0f );
        float sinHT = sin( cpCollAngle * PI / 180.0f );
        float minX = get_minXAx( pInd ) * cosHT - get_minYAx( pInd ) * sinHT;
        float maxX = get_maxXAx( pInd ) * cosHT - get_maxYAx( pInd ) * sinHT;

        int jw = pInd - get_PInd(cpi, XJaw);
        if ( machine_->getXJaw(jw)->getIsContinuouslyVariable() ) {
            setParam( pInd, machine_->getXJaw(jw)->getNumValues(),
                    max(minX, machine_->getXJaw(jw)->getMinValue() ), min(maxX, machine_->getXJaw(jw)->getMaxValue() ),
                    machine_->getXJaw(jw)->getStepSize(), XJaw );
        } else {
            setParam( pInd, machine_->getXJaw(jw)->getNumValues(),
                    max(minX, machine_->getXJaw(jw)->getMinValue() ), min(maxX, machine_->getXJaw(jw)->getMaxValue() ),
                    -1.0f, machine_->getXJaw(jw)->getAllValues(), XJaw);
        }
        break;
    } case YJaw: {
        float cpCollAngle = get_Collimator(cpi) - get_dijCollimatorAngle(pInd);
        float cosHT = cos( cpCollAngle * PI / 180.0f );
        float sinHT = sin( cpCollAngle * PI / 180.0f );
        float minY = get_minXAx( pInd ) * sinHT + get_minYAx( pInd ) * cosHT;
        float maxY = get_maxXAx( pInd ) * sinHT + get_maxYAx( pInd ) * cosHT;

        int jw = pInd - get_PInd(cpi, YJaw);
        if ( machine_->getYJaw(jw)->getIsContinuouslyVariable() ) {
            setParam( pInd, machine_->getYJaw(jw)->getNumValues(),
                    max(minY, machine_->getYJaw(jw)->getMinValue() ), min(maxY, machine_->getYJaw(jw)->getMaxValue() ),
                    machine_->getYJaw(jw)->getStepSize(), YJaw );
        } else {
            setParam( pInd, machine_->getYJaw(jw)->getNumValues(),
                    max(minY, machine_->getYJaw(jw)->getMinValue() ), min(maxY, machine_->getYJaw(jw)->getMaxValue() ),
                    -1.0f, machine_->getYJaw(jw)->getAllValues(), YJaw);
        }
        break;
    } case XLeaf: {
        float cpCollAngle = get_Collimator(cpi) - get_dijCollimatorAngle(pInd);
        float cosHT = cos( cpCollAngle * PI / 180.0f );
        float sinHT = sin( cpCollAngle * PI / 180.0f );
        float minX = get_minXAx( pInd ) * cosHT - get_minYAx( pInd ) * sinHT;
        float maxX = get_maxXAx( pInd ) * cosHT - get_maxYAx( pInd ) * sinHT;

        int lf = pInd - get_PInd(cpi, XLeaf);
        if ( machine_->getXLeaf(lf)->getIsContinuouslyVariable() ) {
            setParam( pInd, machine_->getXLeaf(lf)->getNumValues(),
                    max( minX, machine_->getXLeaf(lf)->getMinValue() ), min( maxX, machine_->getXLeaf(lf)->getMaxValue() ),
                    machine_->getXLeaf(lf)->getStepSize(), XLeaf );
        } else {
            setParam( pInd, machine_->getXLeaf(lf)->getNumValues(),
                    max( minX, machine_->getXLeaf(lf)->getMinValue() ), min( maxX, machine_->getXLeaf(lf)->getMaxValue() ),
                    -1.0f, machine_->getXLeaf(lf)->getAllValues(), XLeaf);
        }
        break;
    } case YLeaf: {
        float cpCollAngle = get_Collimator(cpi) - get_dijCollimatorAngle(pInd);
        float cosHT = cos( cpCollAngle * PI / 180.0f );
        float sinHT = sin( cpCollAngle * PI / 180.0f );
        float minY = get_minXAx( pInd ) * sinHT + get_minYAx( pInd ) * cosHT;
        float maxY = get_maxXAx( pInd ) * sinHT + get_maxYAx( pInd ) * cosHT;

        int lf = pInd - get_PInd(cpi, YLeaf);
        if ( machine_->getYLeaf(lf)->getIsContinuouslyVariable() ) {
            setParam( pInd, machine_->getXLeaf(lf)->getNumValues(),
                    max( minY, machine_->getYLeaf(lf)->getMinValue() ), min( maxY, machine_->getYLeaf(lf)->getMaxValue() ),
                    machine_->getYLeaf(lf)->getStepSize(), YLeaf );
        } else {
            setParam( pInd, machine_->getYLeaf(lf)->getNumValues(),
                    max( minY, machine_->getYLeaf(lf)->getMinValue() ), min( maxY, machine_->getYLeaf(lf)->getMaxValue() ),
                    -1.0f, machine_->getYLeaf(lf)->getAllValues(), YLeaf);
        }
        break;
    } default:
        cerr << "DirectParameterVector.insert_param() Error unhandled parameter type" << endl;
        assert(0);
    }
 }

/**
 * Remove a single parameter from a control point.
 *
 * Warning casual deletion of parameters will break the interpretion of the parameter set.
 * Only safe to use in very specific conditions.
 * E.g. deleting a field weighting term to change a zeroth control point to a normal control point.
 */
void DirectParameterVector::remove_param( unsigned int pInd ) {

    size_t cpInd = paramIndToCPInd(pInd);
    --paramPerCP_[cpInd];
    --nParams_;
    if ( type_[pInd] == Weight ) {
        --nWeightingParams_[cpInd];
    } else if ( type_[pInd] == BeamEnergy ) {
        --nBeamEnergyParams_[cpInd];
    } else if ( type_[pInd] == DoseRate ) {
        --nDoseRateParams_[cpInd];
    } else if ( type_[pInd] == Gantry ) {
        --nGantryParams_[cpInd];
    } else if ( type_[pInd] == Collimator ) {
        --nCollimatorParams_[cpInd];
    } else if ( type_[pInd] == Couch ) {
        --nCouchParams_[cpInd];
    } else if ( type_[pInd] == XJaw ) {
        --nXJawParams_[cpInd];
    } else if ( type_[pInd] == YJaw ) {
        --nXJawParams_[cpInd];
    } else if ( type_[pInd] == XLeaf ) {
        --nXLeafParams_[cpInd];
    } else if ( type_[pInd] == YLeaf ) {
        --nYLeafParams_[cpInd];
    } else {
        cerr << "DirectParameterVector.remove_param() Error unhandled parameter type" << endl;
        assert(0);
    }

    value_.erase( value_.begin() + pInd );
    lowBound_.erase( lowBound_.begin() + pInd );
    highBound_.erase( highBound_.begin() + pInd );
    isActive_.erase( isActive_.begin() + pInd );
    step_size_.erase( step_size_.begin() + pInd );
    absHighBound_.erase( absHighBound_.begin() + pInd );
    absLowBound_.erase( absLowBound_.begin() + pInd );
    isDiscreteVariable_.erase( isDiscreteVariable_.begin() + pInd );
    numberOfValidValues_.erase( numberOfValidValues_.begin() + pInd );
    type_.erase( type_.begin() + pInd );
    validValues_.erase( pInd );
}

/**
 * Insert a single parameter into a control point immediately before the input parameter index.
 *
 * Warning casual insertion of parameters will break the interpretion of the parameter set.
 * Only safe to use in very specific conditions.
 * E.g. Inserting a field weighting term to change a standard control point to a field weighting
 * control point.
 */
void DirectParameterVector::insert_param( unsigned int pInd, ParameterType pType ) {

    size_t cpInd = paramIndToCPInd(pInd);
    ++paramPerCP_[cpInd];
    ++nParams_;

    // Insert extra element into per-parameter vectors
    value_.insert( value_.begin() + pInd, 0.0f );
    lowBound_.insert( lowBound_.begin() + pInd, 0.0f );
    highBound_.insert( highBound_.begin() + pInd, 0.0f );
    isActive_.insert( isActive_.begin() + pInd, true );
    step_size_.insert( step_size_.begin() + pInd, 0.0f );
    absHighBound_.insert( absHighBound_.begin() + pInd, 0.0f );
    absLowBound_.insert( absLowBound_.begin() + pInd, 0.0f );
    isDiscreteVariable_.insert( isDiscreteVariable_.begin() + pInd, true );
    numberOfValidValues_.insert( numberOfValidValues_.begin() + pInd, 0 );
    type_.insert( type_.begin() + pInd, pType );

    // Set type and parameter numbering
    switch ( pType ) {
    case Weight:
        type_[pInd] = Weight;
        ++nWeightingParams_[cpInd];
        break;
    case BeamEnergy:
        type_[pInd] = BeamEnergy;
        ++nBeamEnergyParams_[cpInd];
        break;
    case DoseRate:
        type_[pInd] = DoseRate;
        ++nDoseRateParams_[cpInd];
        break;
    case Gantry:
        type_[pInd] = Gantry;
        ++nGantryParams_[cpInd];
        break;
    case Collimator:
        type_[pInd] = Collimator;
        ++nCollimatorParams_[cpInd];
        break;
    case Couch:
        type_[pInd] = Couch;
        ++nCouchParams_[cpInd];
        break;
    case XJaw:
        type_[pInd] = XJaw;
        ++nXJawParams_[cpInd];
        break;
    case YJaw:
        type_[pInd] = YJaw;
        ++nYJawParams_[cpInd];
        break;
    case XLeaf:
        type_[pInd] = XLeaf;
        ++nXLeafParams_[cpInd];
        break;
    case YLeaf:
        type_[pInd] = YLeaf;
        ++nYLeafParams_[cpInd];
        break;
    default:
        cerr << "DirectParameterVector.insert_param() Error unhandled parameter type" << endl;
        assert(0);
    }

    // Populate parameter with default properties.
    putDefaults( pInd );
}

/**
 * Insert a single parameter into a control point immediately before the input parameter index and
 * of the same type.
 */
void DirectParameterVector::insert_param( unsigned int pInd ) {
    insert_param( pInd, type_[pInd] );
}

/**
 * Insert a single parameter into a control point immediately before the input parameter index and
 * of the same type.
 */
void DirectParameterVector::copy_param( unsigned int pIndFrom, unsigned int pIndTo ) {
    insert_param( pIndTo, type_[pIndFrom] );

    value_[pIndTo] = value_[pIndFrom];
    lowBound_[pIndTo] = lowBound_[pIndFrom];
    highBound_[pIndTo] = highBound_[pIndFrom];
    isActive_[pIndTo] = isActive_[pIndFrom];
    step_size_[pIndTo] = step_size_[pIndFrom];
    absHighBound_[pIndTo] = absHighBound_[pIndFrom];
    absLowBound_[pIndTo] = absLowBound_[pIndFrom];
    isDiscreteVariable_[pIndTo] = isDiscreteVariable_[pIndFrom];
    numberOfValidValues_[pIndTo] = numberOfValidValues_[pIndFrom];
    type_[pIndTo] = type_[pIndFrom];
    validValues_[pIndTo] = validValues_[pIndFrom];
}

/// Combine control points from two fields into a single field.
void DirectParameterVector::combine_fields( unsigned int fd1, unsigned int fd2 ) {

    // Sort input field indices into ascending order
    if ( fd1 > fd2 ) {
        unsigned int tmpfd = fd1;
        fd1 = fd2;
        fd2 = tmpfd;
    }

    // Check if fields are adjacent in absolute control point index order otherwise resort
    size_t cp1 = get_absCPindex( fd1, 0 );
    if ( ! get_absCPindex( fd2, 0 ) == cp1 + ncp_[fd1] ) {
        DirectParameterVector * field2 = copy_field( fd2 );
        remove_field( fd2 );
        add_field( fd1, *field2 );
        fd2 = fd1 + 1;
        delete field2;
    }

    // Remove field weightings
    bool tmpWeightingStatus = isWeightingsAbsolute_;
    set_WeightingCPRelative();
    if ( get_nParamOfType( get_absCPindex(fd2, 0), Weight) > 1 )
        remove_param( get_PIndField(fd2) );

    ncp_[fd1] += ncp_[fd2];
    ncp_.erase( ncp_.begin() + fd2 );

    // Restore original control point weighting system
    if ( tmpWeightingStatus ) set_WeightingCPAbsolute();

    resetLimits();
}

/// Split a field into two fields at a given control point.
void DirectParameterVector::split_field( unsigned int fd, unsigned int cpInd ) {
    // Get a copy of the field of interest
    DirectParameterVector * tmpField = copy_field( fd );

    // Delete control points after the split point
    for ( unsigned int cp=cpInd; cp<ncp_[fd]; cp++ )
        remove_cp( fd, cp );

    // If required copy field weighting from first control point to new field first control point.
    if ( get_nParamOfType( get_absCPindex(fd, 0), Weight) > 1 )
        tmpField->copy_param( 0, get_PIndCP( cpInd ) );

    // In temporary field delete control points before the split point
    for ( unsigned int cp=0; cp<cpInd; cp++ )
        tmpField->remove_cp( 0, cp );

    add_field(fd, *tmpField);

    delete tmpField;
}


/**
 * Get the dij beam number for a given parameter
 */
unsigned int DirectParameterVector::get_beamNo(const int &pInd) const {
    unsigned int dijBeamInd;
    unsigned int cpInd = paramIndToCPInd(pInd);

    vector<float> dijGantryAngles = geometry_->get_gantry_angle();
    vector<float> dijCouchAngles = geometry_->get_table_angle();

    float cpGantryAngle = fmod( get_Gantry( cpInd ) + 360.0f, 360.0f );
    float cpCouchAngle = get_Couch( cpInd );

    float minAngleDiffSq = 259200.0f; // = 360^2 + 360^2
    for ( unsigned int bm=0;  bm<dijGantryAngles.size(); ++bm ) {
        float angleDiffSq = ( dijGantryAngles[bm] - cpGantryAngle ) * ( dijGantryAngles[bm] - cpGantryAngle ) +
                            ( dijCouchAngles[bm] - cpCouchAngle ) * ( dijCouchAngles[bm] - cpCouchAngle );
        if ( angleDiffSq < minAngleDiffSq ) {
            minAngleDiffSq = angleDiffSq;
            dijBeamInd = bm;
        }
    }

    assert( sqrt( minAngleDiffSq ) < ALLOWED_BEAMANGLE_DISCREPANCY );

    return dijBeamInd;
}


/**
 * Get the gantry angle of the dij map for the beam corresponding to a given parameter
 */
float DirectParameterVector::get_dijGantryAngle(const int &pInd) const {
    unsigned int dijBeamInd;
    unsigned int cpInd = paramIndToCPInd(pInd);

    vector<float> dijGantryAngles = geometry_->get_gantry_angle();
    vector<float> dijCouchAngles = geometry_->get_table_angle();

    float cpGantryAngle = get_Gantry( cpInd );
    float cpCouchAngle = get_Couch( cpInd );

    float minAngleDiffSq = 259200.0f; // = 360^2 + 360^2
    for ( unsigned int bm=0;  bm<dijGantryAngles.size(); ++bm ) {
        float angleDiffSq = ( dijGantryAngles[bm] - cpGantryAngle ) * ( dijGantryAngles[bm] - cpGantryAngle ) +
                            ( dijCouchAngles[bm] - cpCouchAngle ) * ( dijCouchAngles[bm] - cpCouchAngle );
        if ( angleDiffSq < minAngleDiffSq ) {
            minAngleDiffSq = angleDiffSq;
            dijBeamInd = bm;
        }
    }
    assert( sqrt( minAngleDiffSq ) < ALLOWED_BEAMANGLE_DISCREPANCY );

    return dijGantryAngles[ dijBeamInd ];
}


/**
 * Get the collimator angle of the dij map for the beam corresponding to a given parameter
 */
float DirectParameterVector::get_dijCollimatorAngle(const int &pInd) const {
    vector<float> dijCollimatorAngles = geometry_->get_collimator_angle();
    return dijCollimatorAngles[ get_beamNo(pInd) ];
}


/**
 * Get the couch angle of the dij map for the beam corresponding to a given parameter
 */
float DirectParameterVector::get_dijCouchAngle(const int &pInd) const {
    unsigned int dijBeamInd;
    unsigned int cpInd = paramIndToCPInd(pInd);

    vector<float> dijGantryAngles = geometry_->get_gantry_angle();
    vector<float> dijCouchAngles = geometry_->get_table_angle();

    float cpGantryAngle = get_Gantry( cpInd );
    float cpCouchAngle = get_Couch( cpInd );

    float minAngleDiffSq = 259200.0f; // = 360^2 + 360^2
    for ( unsigned int bm=0;  bm<dijGantryAngles.size(); ++bm ) {
        float angleDiffSq = ( dijGantryAngles[bm] - cpGantryAngle ) * ( dijGantryAngles[bm] - cpGantryAngle ) +
                            ( dijCouchAngles[bm] - cpCouchAngle ) * ( dijCouchAngles[bm] - cpCouchAngle );
        if ( angleDiffSq < minAngleDiffSq ) {
            minAngleDiffSq = angleDiffSq;
            dijBeamInd = bm;
        }
    }
    assert( sqrt( minAngleDiffSq ) < ALLOWED_BEAMANGLE_DISCREPANCY );

    return dijCouchAngles[ dijBeamInd ];
}


/**
 * Translate the ParameterVector into a BixelVector
 * Warning calling function should free returned object.
 */
BixelVector * DirectParameterVector::translateToBixelVector() const {

    if ( is_impossible() ) throw DirectParameterVector_exception();

    // Populate bixel values from control points
    bixMap_->clear_values();
    for (unsigned int cpInd = 0; cpInd<ncpTot_; ++cpInd) {
        translateControlPointToBixelVector( cpInd, bixMap_ );
    }
    // Create a new object from the internal cached object to return
    BixelVector * bixMapTmp = new BixelVector(*bixMap_);
    return bixMapTmp;
};


/**
 * Translate the gradient in terms of bixel parameters to the gradient in terms of
 * the parameters used to describe plan.
 */
ParameterVectorDirection DirectParameterVector::translateBixelGradient( BixelVectorDirection &bvd ) const {

    // Initialise blank parameter vector direction to store new gradient.
    ParameterVectorDirection pvd = ParameterVectorDirection(*this);
    pvd.clear_values();
    pvd.setVectorType(DIRECT_PVD_TYPE);

    // Make a list of the correspondance of control points and dij beam numbers
    multimap<unsigned int, unsigned int> cpDijBeamInds;
    vector<float> dijCollAngles = geometry_->get_collimator_angle();
    vector<float> dijXBixSz = geometry_->get_bixel_grid_dx();
    vector<float> dijYBixSz = geometry_->get_bixel_grid_dy();

    vector<unsigned int> uniqueDijInds;
    for (unsigned int cpInd = 0; cpInd<ncpTot_; ++cpInd) {
        unsigned int dijBeamInd = get_beamNo( get_PIndCP(cpInd) );
        cpDijBeamInds.insert( pair<unsigned int, unsigned int>(dijBeamInd ,cpInd) );
        bool alreadyPresent = false;
        for (vector<unsigned int>::iterator it = uniqueDijInds.begin(); it < uniqueDijInds.end(); ++it)
            if ( dijBeamInd == *it ) {
                alreadyPresent = true;
                break;
            }
        if (!alreadyPresent) uniqueDijInds.push_back(dijBeamInd);
    }

    // For each Dij beam index used in the plan
    BixelVector * bixVecCP = new BixelVector(*bixMap_);
    for ( vector<unsigned int>::iterator it = uniqueDijInds.begin(); it < uniqueDijInds.end(); ++it ) {
        unsigned int dijBeamInd = *it;
        float cpXBixSz = dijXBixSz[ dijBeamInd ];
        float cpYBixSz = dijXBixSz[ dijBeamInd ];

        unsigned int firstBix = geometry_->get_beam_starting_bixelNo( dijBeamInd );
        unsigned int lastBix = firstBix + geometry_->get_nBixels_in_beam( dijBeamInd );

        float bixMapSum = 0.0f;
        for ( unsigned int bxNum = firstBix; bxNum < lastBix; ++bxNum ) {
            bixMapSum += bixMap_->get_value(bxNum);
        }
        // If this angle contributes no fluence then skip to next angle
        if ( bixMapSum == 0 ) continue;

        // Find all the control points using this dij beam index
        pair< multimap<unsigned int, unsigned int>::iterator, multimap<unsigned int, unsigned int>::iterator > cpIts;
        cpIts = cpDijBeamInds.equal_range(dijBeamInd);
        unsigned int cpInd = cpIts.first->second;
        float cpCollAngle = get_Collimator( cpInd ) - dijCollAngles[ dijBeamInd ];

        for ( multimap<unsigned int, unsigned int>::iterator cpIt = cpIts.first;  cpIt != cpIts.second; ++cpIt ) {
            bixVecCP->clear_values();
            translateControlPointToBixelVector( cpInd, bixVecCP );

            float cpMapSum = 0.0f;
            float cpGradSum = 0.0f;
            int cpMapArea = 0;
            for ( unsigned int bxNum = firstBix; bxNum < lastBix; ++bxNum ) {
                if ( bixVecCP->get_value(bxNum) > 0 ) {
                    cpMapSum += bixVecCP->get_value(bxNum);
                    cpGradSum += bvd[bxNum];
                    cpMapArea++;
                }
            }

            int pInd = get_PInd( cpInd, Weight ) + get_nParamOfType( cpInd, Weight ) - 1;
            pvd[ pInd ] = cpGradSum / cpMapArea; // Control point weighting gradient

            // The relative fluence contribution of each control point affects the the gradient for leaf and jaw positions.
            float cpContribToFlu = ( cpMapSum / bixMapSum );

            assert(!isnan(cpContribToFlu));

            //if (cpIt == cpIts.first) cout << endl << "Got here AA " << endl;

            // Jaw Parameters
            if ( get_nParamOfType( cpInd, XJaw ) > 0 )
                populateGradientForXJaws( cpInd, bvd, pvd, dijBeamInd, cpContribToFlu, cpCollAngle, cpXBixSz, cpYBixSz );
                //if (cpIt == cpIts.first) cout << endl << "Got here BB " << endl;

            if ( get_nParamOfType( cpInd, YJaw ) > 0 )
                populateGradientForYJaws( cpInd, bvd, pvd, dijBeamInd, cpContribToFlu, cpCollAngle, cpXBixSz, cpYBixSz );
                //if (cpIt == cpIts.first) cout << endl << "Got here CC " << endl;


            // Leaf Parameters
            if ( get_nParamOfType( cpInd, XLeaf ) > 0 )
                populateGradientForXLeaves( cpInd, bvd, pvd, dijBeamInd, cpContribToFlu, cpCollAngle, cpXBixSz, cpYBixSz );
                //if (cpIt == cpIts.first) cout << endl << "Got here DD " << endl;

            if ( get_nParamOfType( cpInd, YLeaf ) > 0 )
                populateGradientForYLeaves( cpInd, bvd, pvd, dijBeamInd, cpContribToFlu, cpCollAngle, cpXBixSz, cpYBixSz );
                //if (cpIt == cpIts.first) cout << endl << "Got here EE " << endl;

            cpInd++;
        }
    }
    delete bixVecCP;

    assert(!isnan(pvd.get_value(22)));

    // Set field gradient as mean gradient of all control points within the field.
    // Field term no longer present
    /*int cpInd = 0;
    for ( unsigned int fd=0; fd<nf_; fd++) {
        int pIndFd = get_PInd( cpInd, Weight );
        float cpGradSum = 0.0f;
        for (unsigned int cp=0; cp<ncp_[fd]; cp++) {
            int pInd = get_PInd( cpInd, Weight ) + get_nParamOfType( cpInd, Weight ) - 1;
            cpGradSum += pvd[ pInd ];
            cpInd++;
        }
        pvd[ pIndFd ] = cpGradSum / ncp_[fd];
    }*/
    return pvd;
};


/**
 * Translate the hessian in terms of bixel parameters to the hessian in terms of
 * the parameters used to describe plan.
 */
vector<float> DirectParameterVector::translateBixelHessian( vector<float> & hv ) const {
    cerr << "DirectParameterVector.translateBixelHessian(): Error this function does not exist." << endl;
    assert(0);
};


/**
 * Translate the bixel gradient terms onto gradient for parameters within a given control point.
 */
void DirectParameterVector::translateControlPointToBixelVector( unsigned int absCpInd, BixelVector * bixVect ) const {
    // For this single control point make a bixel vector of bixel intensities

    // Get information about dij bixel grid
    vector<float> dijXBixSz = geometry_->get_bixel_grid_dx();
    vector<float> dijYBixSz = geometry_->get_bixel_grid_dy();

    // Find beam index in dij data that best corresponds to this control point.
    unsigned int dijBeamInd = get_beamNo( get_PIndCP(absCpInd) );
    float cpWeight = get_Weight(absCpInd);

    // Create a set of fluence profiles (one for each leaf pair) with at least the same resolution
    // as the equivalent direction in the dij bixels matrices
    float tmpBixSz = min(dijXBixSz[dijBeamInd], dijYBixSz[dijBeamInd]) * TMP_BIX_SIZE_FACTOR;

    // Get Min and Max relevant positions
    float minPX = floor( ( get_minXAx( get_PIndCP(absCpInd) ) ) / tmpBixSz ) * tmpBixSz - 1;
    float maxPX = ceil( ( get_maxXAx( get_PIndCP(absCpInd) ) ) / tmpBixSz ) * tmpBixSz + 1;
    float minPY = floor( ( get_minYAx( get_PIndCP(absCpInd) ) ) / tmpBixSz ) * tmpBixSz - 1;
    float maxPY = ceil( ( get_maxYAx( get_PIndCP(absCpInd) ) ) / tmpBixSz ) * tmpBixSz + 1;

    // Create temporary fluence grid
    int dimX = (int) (( maxPX - minPX ) / tmpBixSz + 1.5f); // range inclusive both sides so +1 rounding +0.5.
    int dimY = (int) (( maxPY - minPY ) / tmpBixSz + 1.5f);

    vector< vector<float> > * fluProfilesX;
    vector< vector<float> > * fluProfilesY;
    vector<float> * fluOffsetX;
    vector<float> * fluOffsetY;
    vector< vector<float> >::iterator fluRow;
    vector<float>::iterator fluBix;

    if ( machine_->getIsXMlc() ) {    // ---------- X Leaves And Jaws ---------- //
        float jaw1, jaw2;
        int jwPosInd1, jwPosInd2;
        if ( machine_->getIsXJaws() ) {
            jaw1 = get_XJaw(absCpInd, 0);
            jaw2 = get_XJaw(absCpInd, 1);

            // Work out over which bixels the jaws are currently situated.
            jwPosInd1 = (int) ( ( ( jaw1 - minPX ) / tmpBixSz ) + 0.5f );
            jwPosInd2 = (int) ( ( ( jaw2 - minPX ) / tmpBixSz ) + 0.5f );
        }
        vector<float> leafBnk1, leafBnk2;
        get_XLeaves( absCpInd, leafBnk1, 0 );
        get_XLeaves( absCpInd, leafBnk2, 1 );

        fluProfilesX = new vector< vector<float> >( leafBnk1.size(), vector<float>(dimX, 1.0f ) );
        fluOffsetX = new vector<float>(leafBnk1.size(),0.0f);

        int nLeafPairs = machine_->getNXLeaves() / 2;
        fluRow = fluProfilesX->begin();

        for ( unsigned int lf=0; lf < leafBnk1.size(); ++lf ) {
            fluOffsetX->at(lf) = machine_->getXLeaf(lf)->getPosition();

            // Work out over which bixels the leaves are currently situated.
            int lfPosInd1 = (int) ( ( ( leafBnk1[lf] - minPX ) / tmpBixSz ) + 0.5f );
            int lfPosInd2 = (int) ( ( ( leafBnk2[lf] - minPX ) / tmpBixSz ) + 0.5f );

            // Leakage outside leaf positions
            for ( int bx = 0; bx < lfPosInd1; ++bx )
                fluRow->at(bx) *= machine_->getXLeaf(lf)->getTransmission();
            for ( int bx = ( lfPosInd2 + 1 ); bx < dimX; ++bx )
                fluRow->at(bx) *= machine_->getXLeaf(lf+nLeafPairs)->getTransmission();

            // Leakage outside jaw positions
            for ( int bx = 0; bx < jwPosInd1; ++bx )
                fluRow->at(bx) *= machine_->getXJaw(0)->getTransmission();
            for ( int bx = ( jwPosInd2 + 1 ); bx < dimX; ++bx )
                fluRow->at(bx) *= machine_->getXJaw(1)->getTransmission();

            // Assign partial fluence at bixel beneath leaf 1
            fluRow->at(lfPosInd1) *= ( ( leafBnk1[lf] - minPX ) / tmpBixSz - lfPosInd1 + 0.5f )
                                        * (1-machine_->getXLeaf(lf)->getTransmission());

            // Assign partial fluence at bixel beneath leaf 2
            fluRow->at(lfPosInd2) *= ( ( minPX - leafBnk2[lf] ) / tmpBixSz + lfPosInd2 + 0.5f )
                                        * (1-machine_->getXLeaf(lf+nLeafPairs)->getTransmission());

            if ( jwPosInd1!=lfPosInd1 && jwPosInd1!=lfPosInd2 && jwPosInd2!=lfPosInd1 && jwPosInd2!=lfPosInd2 ) {
                // Assign partial fluence at bixel beneath jaw 1
                fluRow->at(jwPosInd1) *= ( ( jaw1 - minPX ) / tmpBixSz - jwPosInd1 + 0.5f ) *
                                        ( 1 - machine_->getXJaw(0)->getTransmission() );
                // Assign partial fluence at bixel beneath jaw 2
                fluRow->at(jwPosInd2) *= ( ( minPX - jaw2 ) / tmpBixSz + jwPosInd2 + 0.5f ) *
                                        ( 1 - machine_->getXJaw(1)->getTransmission() );
            }

            ++fluRow;
        }
    } else if ( machine_->getIsXJaws() ) {                      // --------------  X Jaws -------------- //

        float jaw1 = get_XJaw(absCpInd, 0);
        float jaw2 = get_XJaw(absCpInd, 1);

        // Work out over which bixels the jaws are currently situated.
        int jwPosInd1 = (int) ( ( ( jaw1 - minPX ) / tmpBixSz ) + 0.5f );
        int jwPosInd2 = (int) ( ( ( jaw2 - minPX ) / tmpBixSz ) + 0.5f );

        fluProfilesX = new vector< vector<float> >( 1, vector<float>(dimX, 1.0f ) );
        fluOffsetX = new vector<float>(1,0.0f);

        for ( fluRow = fluProfilesX->begin(); fluRow != fluProfilesX->end(); ++fluRow ) {
            // Assign leakage outside jaw positions
            for ( int bx = 0; bx < jwPosInd1; ++bx )
                fluRow->at(bx) *= machine_->getXJaw(0)->getTransmission();
            for ( int bx = ( jwPosInd2 + 1 ); bx < dimX; ++bx )
                fluRow->at(bx) *= machine_->getXJaw(1)->getTransmission();

            // Assign partial fluence at jaw position bixels
            fluRow->at(jwPosInd1) *= ( ( jaw1 - minPX ) / tmpBixSz - jwPosInd1 + 0.5f )
                                    * (1-machine_->getXJaw(0)->getTransmission());
            fluRow->at(jwPosInd2) *= ( ( minPX - jaw2 ) / tmpBixSz + jwPosInd2 + 0.5f )
                                    * (1-machine_->getXJaw(1)->getTransmission());
        }
    } else {
        fluProfilesX = new vector< vector<float> >( 1, vector<float>(dimX, 1.0f ) );
        fluOffsetX = new vector<float>(1,0.0f);
    }

    /*if ( absCpInd == 0 ) { // Debugging ...
        cout.precision(3);
        cout << endl << "Debug Fluence X 1 " << endl;
        for ( int bx = 0; bx < dimX; ++bx ) {
            cout << " : " << fixed << bx * tmpBixSz + minPX;
        }
        cout << endl;
        for (vector< vector<float> >::iterator fluIt=fluProfilesX->begin(); fluIt!=fluProfilesX->end(); ++fluIt) {
            unsigned int fluNum = fluIt-fluProfilesX->begin();
            //if ( fluNum == 0 || fluNum == 20 ) {
                cout << fixed << fluOffsetX->at(fluNum);
                for ( vector<float>::iterator fluBixIt=fluIt->begin(); fluBixIt!=fluIt->end(); ++fluBixIt) {
                    cout << " : " << fixed << *fluBixIt;
                }
                cout << endl;
            //}
        }
        cout << endl;
    }*/

    if ( machine_->getIsYMlc() ) {                       // -------------- Y Leaves ------------- //
        float jaw1, jaw2;
        int jwPosInd1, jwPosInd2;
        if ( machine_->getIsXJaws() ) {
            jaw1 = get_YJaw(absCpInd, 0);
            jaw2 = get_YJaw(absCpInd, 1);

            // Work out over which bixels the jaws are currently situated.
            jwPosInd1 = (int) ( ( ( jaw1 - minPY ) / tmpBixSz ) + 0.5f );
            jwPosInd2 = (int) ( ( ( jaw2 - minPY ) / tmpBixSz ) + 0.5f );
        }
        vector<float> leafBnk1, leafBnk2;
        get_YLeaves( absCpInd, leafBnk1, 0 );
        get_YLeaves( absCpInd, leafBnk2, 1 );

        fluProfilesY = new vector< vector<float> >( leafBnk1.size(), vector<float>(dimY, 1.0f ) );
        fluOffsetY = new vector<float>(leafBnk1.size(),0.0f);

        int nLeafPairs = machine_->getNYLeaves();
        fluRow = fluProfilesY->begin();
        for ( unsigned int lf=0; lf < leafBnk1.size(); ++lf ) {
            fluOffsetY->at(lf) = machine_->getYLeaf(lf)->getPosition();

            // Work out over which bixels the leaves are currently situated.
            int lfPosInd1 = (int) ( ( ( leafBnk1[lf] - minPY ) / tmpBixSz ) + 0.5f );
            int lfPosInd2 = (int) ( ( ( leafBnk2[lf] - minPY ) / tmpBixSz ) + 0.5f );

            // Assign leakage outside leaf positions
            for ( int bx = 0; bx < lfPosInd1; ++bx )
                fluRow->at(bx) *= machine_->getYLeaf(lf)->getTransmission();
            for ( int bx = ( lfPosInd2 + 1 ); bx < dimY; ++bx )
                fluRow->at(bx) *= machine_->getYLeaf(lf+nLeafPairs)->getTransmission();

            // Assign partial fluence at leaf position bixels
            fluRow->at(lfPosInd1) *= ( ( leafBnk1[lf] - minPY ) / tmpBixSz - lfPosInd1 + 0.5f )
                                    * (1-machine_->getYLeaf(lf)->getTransmission());
            fluRow->at(lfPosInd2) *= ( ( minPY - leafBnk2[lf] ) / tmpBixSz + lfPosInd2 + 0.5f )
                                    * (1-machine_->getYLeaf(lf+nLeafPairs)->getTransmission());

            if ( jwPosInd1!=lfPosInd1 && jwPosInd1!=lfPosInd2 && jwPosInd2!=lfPosInd1 && jwPosInd2!=lfPosInd2 ) {
                // Assign partial fluence at bixel beneath jaw 1
                fluRow->at(jwPosInd1)*=((jaw1-minPX)/tmpBixSz-jwPosInd1+0.5f)*(1-machine_->getXJaw(0)->getTransmission());
                // Assign partial fluence at bixel beneath jaw 2
                fluRow->at(jwPosInd2)*=((minPX-jaw2)/tmpBixSz+jwPosInd2+0.5f)*(1-machine_->getXJaw(1)->getTransmission());
            }

            ++fluRow;
        }
    } else if ( machine_->getIsYJaws() ) {                          // --------------  Y Jaws -------------- //
        float jaw1 = get_YJaw(absCpInd, 0);
        float jaw2 = get_YJaw(absCpInd, 1);

        // Work out over which bixels the jaws are currently situated.
        int jwPosInd1 = (int) ( ( ( jaw1 - minPY ) / tmpBixSz ) + 0.5f );
        int jwPosInd2 = (int) ( ( ( jaw2 - minPY ) / tmpBixSz ) + 0.5f );

        fluProfilesY = new vector< vector<float> >( 1, vector<float>(dimY, 1.0f ) );
        fluOffsetY = new vector<float>(1,0.0f);

        for ( fluRow = fluProfilesY->begin(); fluRow != fluProfilesY->end(); ++fluRow ) {
            // Assign leakage outside jaw positions
            for ( int bx = 0; bx < jwPosInd1; ++bx )
                fluRow->at(bx) *= machine_->getYJaw(0)->getTransmission();
            for ( int bx = ( jwPosInd2 + 1 ); bx < dimY; ++bx )
                fluRow->at(bx) *= machine_->getYJaw(1)->getTransmission();

            // Assign partial fluence at jaw position bixels
            fluRow->at(jwPosInd1) *= ( ( jaw1 - minPY ) / tmpBixSz - jwPosInd1 + 0.5f )
                                    * (1-machine_->getYJaw(0)->getTransmission());
            fluRow->at(jwPosInd2) *= ( ( minPY - jaw2 ) / tmpBixSz + jwPosInd2 + 0.5f )
                                    * (1-machine_->getYJaw(1)->getTransmission());
        }
    } else {
        // Valgrind reports error here: Address 0x50394C8 is 0 bytes after a block of size 312 alloc'd
        fluProfilesY = new vector< vector<float> >( 1, vector<float>(dimY, 1.0f ) );
        fluOffsetY = new vector<float>(1,0.0f);
    }

    /*if ( absCpInd == 0 ) {  // Debugging ...
        cout.precision(3);
        cout << endl << "Debug Fluence Y 2 " << endl;
        for ( int by = 0; by < dimY; ++by ) {
            cout << " : " << fixed << by * tmpBixSz + minPY;
        }
        cout << endl;
        for (vector< vector<float> >::iterator fluIt=fluProfilesY->begin(); fluIt!=fluProfilesY->end(); ++fluIt) {
            unsigned int fluNum = fluIt-fluProfilesY->begin();
            if ( fluNum == 0 || fluNum == 20 ) {
                cout << fixed << fluOffsetY->at(fluNum);
                for ( vector<float>::iterator fluBixIt=fluIt->begin(); fluBixIt!=fluIt->end(); ++fluBixIt) {
                    cout << " : " << fixed << *fluBixIt;
                }
                cout << endl;
            }
        }
        cout << endl;
    }*/

    // --------------  Map fluence profiles onto temporary bixel grid -------------- //
    if ( *fluOffsetX->begin() > *(fluOffsetX->end()-1) ) {
        reverse( fluOffsetX->begin(), fluOffsetX->end() );
        reverse( fluProfilesX->begin(), fluProfilesX->end() );
    }
    if ( *fluOffsetY->begin() > *(fluOffsetY->end()-1) ) {
        reverse( fluOffsetY->begin(), fluOffsetY->end() );
        reverse( fluProfilesY->begin(), fluProfilesY->end() );
    }

    vector< vector<float> > * bixMap = new vector< vector<float> >( dimY, vector<float>(dimX, 1.0f ) );
    for ( fluRow = bixMap->begin(); fluRow != bixMap->end(); ++fluRow ) {

        unsigned int fluBixIndY = ( fluRow - bixMap->begin() );
        float bixPosY = fluBixIndY * tmpBixSz + minPY;

        // Seach through profiles and find first x profile inside bixel map
        vector<vector<float> >::iterator fluRowX = fluProfilesX->begin();
        unsigned int fli = 0;
        if ( fluOffsetX->size() > 1 ) {
            float profileWidth = fluOffsetX->at( fli + 1 ) - fluOffsetX->at( fli );
            while ( bixPosY > ( fluOffsetX->at( fli ) + profileWidth / 2.0f ) ) {
                ++fluRowX;
                ++fli;
                if ( fluOffsetX->size() > fli ) {
                    profileWidth = fluOffsetX->at( fli + 1 ) - fluOffsetX->at( fli );
                } else break;
            }
        }

        vector<float>::iterator fluBixX = fluRowX->begin();

        for ( fluBix = fluRow->begin(); fluBix != fluRow->end(); ++fluBix ) {
            unsigned int fluBixIndX = ( fluBix - fluRow->begin() );
            float bixPosX = fluBixIndX * tmpBixSz + minPX;

            // Seach through profiles and find first y profile inside bixel map
            unsigned int flj = 0;
            vector<vector<float> >::iterator fluRowY = fluProfilesY->begin();
            if ( fluOffsetY->size() > 1 ) {
                float profileWidth = fluOffsetY->at( fli + 1 ) - fluOffsetY->at( fli );
                while ( bixPosX > ( fluOffsetY->at( fli ) + profileWidth / 2.0f ) ) {
                    ++fluRowY;
                    ++fli;
                    if ( fluOffsetY->size() > fli ) {
                        profileWidth = fluOffsetY->at( fli + 1 ) - fluOffsetY->at( fli );
                    } else break;
                }
            }

            vector<float>::iterator fluBixY = fluRowY->begin() + (fluRow - bixMap->begin());

            /*if ( absCpInd == 0 ) {
                unsigned int px = fluBix - fluRow->begin();
                unsigned int py = fluRow - bixMap->begin();
                //if ( ( px == 19 || px == 66 ) && ( py == 10 || py == 19 || py == 66 ) ) {
                if ( ( px == 19 ) ) {
                    cout << " ind1 = [" << px << "," << py << "] ind2 = [" << fluBixIndX << "," << fluBixIndY << "] ind3 = [" << fli << "," << flj << "] pos = (" << bixPosX << "," << bixPosY << ") ";
                    cout << " xProfile " << fli << " at (" << (fluBixX-fluRowX->begin())*tmpBixSz+minPX << "," << fluOffsetX->at(fli) << ") = " <<  *fluBixX;
                    cout << " yProfile " << flj << flush;
                    cout << " at (" << fluOffsetY->at(flj) << "," << (fluBixY-fluRowY->begin())*tmpBixSz+minPY << ") = " <<  *fluBixY;
                    cout << endl;
                }
            }*/

            *fluBix *= *fluBixX * *fluBixY;

            ++fluBixX;
            ++flj;
        }
        ++fli;
    }
    delete fluProfilesX;
    delete fluProfilesY;
    delete fluOffsetX;
    delete fluOffsetY;

    /*if ( absCpInd == 0 ) { // Debugging ...
        cout << endl << "High resolution bixel map collimator reference : " << endl;
        for ( int xi=0; xi < dimX; ++xi ) {
            cout << " : " << fixed << xi * tmpBixSz + minPX;
        }
        for ( int yj=0; yj < dimY; ++yj ) {
            cout << endl << fixed << yj * tmpBixSz + minPY << " : ";
            for ( int xi=0; xi < dimX; ++xi ) {
                cout << bixMap->at(yj).at(xi) << " : " << flush;
            }
        }
        cout << endl << "Done" << endl;
    }*/

    // ------ Rotate fluence map with head twist and map to bixel map resolution ------ //

    // Subtract dij collimator angle from control point collimator angle before rotating control point.
    vector<float> dijCollAngles = geometry_->get_collimator_angle();
    float cpCollAngle = get_Collimator( absCpInd ) - dijCollAngles[dijBeamInd];

    float cosHT = cos( cpCollAngle * PI / 180.0f );
    float sinHT = sin( cpCollAngle * PI / 180.0f );

    float tmpVal;
    float newMinX = numeric_limits<float>::infinity();
    float newMinY = numeric_limits<float>::infinity();
    float newMaxX = -numeric_limits<float>::infinity();
    float newMaxY = -numeric_limits<float>::infinity();
    // Inverse angular transformation i.e angle sign reversed so sin terms sign changed.
    tmpVal=minPX*cosHT+minPY*sinHT;  newMinX=tmpVal<newMinX?tmpVal:newMinX;  newMaxX=tmpVal>newMaxX?tmpVal:newMaxX;
    tmpVal=maxPX*cosHT+minPY*sinHT;  newMinX=tmpVal<newMinX?tmpVal:newMinX;  newMaxX=tmpVal>newMaxX?tmpVal:newMaxX;
    tmpVal=minPX*cosHT+maxPY*sinHT;  newMinX=tmpVal<newMinX?tmpVal:newMinX;  newMaxX=tmpVal>newMaxX?tmpVal:newMaxX;
    tmpVal=maxPX*cosHT+maxPY*sinHT;  newMinX=tmpVal<newMinX?tmpVal:newMinX;  newMaxX=tmpVal>newMaxX?tmpVal:newMaxX;
    tmpVal=minPY*cosHT-minPX*sinHT;  newMinY=tmpVal<newMinY?tmpVal:newMinY;  newMaxY=tmpVal>newMaxY?tmpVal:newMaxY;
    tmpVal=maxPY*cosHT-minPX*sinHT;  newMinY=tmpVal<newMinY?tmpVal:newMinY;  newMaxY=tmpVal>newMaxY?tmpVal:newMaxY;
    tmpVal=minPY*cosHT-maxPX*sinHT;  newMinY=tmpVal<newMinY?tmpVal:newMinY;  newMaxY=tmpVal>newMaxY?tmpVal:newMaxY;
    tmpVal=maxPY*cosHT-maxPX*sinHT;  newMinY=tmpVal<newMinY?tmpVal:newMinY;  newMaxY=tmpVal>newMaxY?tmpVal:newMaxY;

    int rotDimX = (int) ceil( newMaxX - newMinX );
    int rotDimY = (int) ceil( newMaxY - newMinY );
    vector< vector< float > > * rotBixMap = new vector< vector< float > >( rotDimY, vector<float>(rotDimX, 0.0f ) );

    vector<float> xBixPos, yBixPos;
    xBixPos.reserve(rotDimX);
    yBixPos.reserve(rotDimY);
    for ( int bi = 0; bi < rotDimX; bi++ ) xBixPos.push_back( bi * tmpBixSz + newMinX );
    for ( int bi = 0; bi < rotDimY; bi++ ) yBixPos.push_back( bi * tmpBixSz + newMinY );

    int yj = 0;
    for ( fluRow = rotBixMap->begin(); fluRow != rotBixMap->end(); ++fluRow ) {
        int xi = 0;
        for ( fluBix = fluRow->begin(); fluBix != fluRow->end(); ++fluBix ) {
            float xRotBixPos = xBixPos[xi] * cosHT - yBixPos[yj] * sinHT;
            float yRotBixPos = xBixPos[xi] * sinHT + yBixPos[yj] * cosHT;

            int xRotBixInd = (int) ( ( ( xRotBixPos - newMinX ) / tmpBixSz ) - 0.5f );
            int yRotBixInd = (int) ( ( ( yRotBixPos - newMinY ) / tmpBixSz ) - 0.5f );

            float bixWtX1 = ( xRotBixPos - xRotBixInd * tmpBixSz - newMinX ) / tmpBixSz;
            float bixWtY1 = ( yRotBixPos - yRotBixInd * tmpBixSz - newMinY  ) / tmpBixSz;

            if ( yRotBixInd >= 0 && xRotBixInd >= 0 && yRotBixInd < dimY && xRotBixInd < dimX )
                *fluBix += bixMap->at(yRotBixInd).at(xRotBixInd) * ( (1-bixWtX1) * (1-bixWtY1) );
            if ( (yRotBixInd+1) >= 0 && xRotBixInd >= 0 && (yRotBixInd+1) < dimY && xRotBixInd < dimX )
                *fluBix += bixMap->at(yRotBixInd+1).at(xRotBixInd) * ( (1-bixWtX1) * bixWtY1 );
            if ( yRotBixInd >= 0 && (xRotBixInd+1) >= 0 && yRotBixInd < dimY && (xRotBixInd+1) < dimX )
                *fluBix += bixMap->at(yRotBixInd).at(xRotBixInd+1) * ( bixWtX1 * (1-bixWtY1) );
            if ( (yRotBixInd+1) >= 0 && (xRotBixInd+1) >= 0 && (yRotBixInd+1) < dimY && (xRotBixInd+1) < dimX )
                *fluBix += bixMap->at(yRotBixInd+1).at(xRotBixInd+1) * ( bixWtX1 * bixWtY1 );
            xi++;
        }
        yj++;
    }
    delete bixMap;

    /*if ( absCpInd == 0 ) { // Debugging ...
        cout << endl << "High resolution rotated bixel map dij reference : " << endl;
        for ( int xi=0; xi < rotDimX; ++xi ) {
            cout << " : " << fixed << xi * tmpBixSz + minPX;
        }
        for ( int yj=0; yj < rotDimY; ++yj ) {
            cout << endl << fixed << yj * tmpBixSz + minPY << " : ";
            for ( int xi=0; xi < rotDimX; ++xi ) {
                cout << rotbixMap->at(yj).at(xi) << " : " << flush;
            }
        }
        cout << endl << "Done" << endl;
    }*/

    // Prepare moving average filter to reduce high res map to low res map.
    // Size is next odd integer from desired size so 3.1 -> 5 etc.
    float desiredXFiltSize = dijXBixSz[dijBeamInd] / tmpBixSz; // Desired filter size in x
    int halfXFiltSz = (int) ceil( (desiredXFiltSize - 1) / 2 );
    int xFiltSz = halfXFiltSz * 2 + 1;

    float desiredYFiltSize = dijYBixSz[dijBeamInd] / tmpBixSz; // Desired filter size in y
    int halfYFiltSz = (int) ceil( (desiredYFiltSize - 1) / 2 );
    int yFiltSz = halfYFiltSz * 2 + 1;

    vector<float> * filtX = new vector<float>(xFiltSz,1.0f);
    *(filtX->begin()) = 1.0f - ( ( ((float) xFiltSz) - desiredXFiltSize ) / 2.0f );
    *(filtX->end()-1) = 1.0f - ( ( ((float) xFiltSz) - desiredXFiltSize ) / 2.0f );

    vector<float> * filtY = new vector<float>(yFiltSz,1.0f);
    *(filtY->begin()) = 1.0f - ( ( ((float) yFiltSz) - desiredYFiltSize ) / 2.0f );
    *(filtY->end()-1) = 1.0f - ( ( ((float) yFiltSz) - desiredYFiltSize ) / 2.0f );

    // Normalise filters, sum should be desiredFiltSize
    for ( vector<float>::iterator filtIt = filtX->begin(); filtIt != filtX->end(); filtIt++ )
        *filtIt /= desiredXFiltSize;
    for ( vector<float>::iterator filtIt = filtY->begin(); filtIt != filtY->end(); filtIt++ )
        *filtIt /= desiredYFiltSize;

    // Calculate moving average along each row
    // Work from rotated (rot) matrix to temporary matrix
    vector<vector<float> > * tmpBixMap = new vector<vector<float> >(rotDimY,vector<float>(rotDimX,0.0f));
    vector<vector<float> >::iterator fluRow_Rot = rotBixMap->begin();
    vector<vector<float> >::iterator fluRow_MvAv = tmpBixMap->begin();
    for ( yj=0; yj < rotDimY; ++yj ) {
        // Calculate with partial filter at start
        for ( int xi = 0; xi <= halfXFiltSz; xi++ ) {
            vector<float>::iterator mvAvBix = fluRow_MvAv->begin() + xi;
            vector<float>::iterator rotBix = fluRow_Rot->begin();
            int flStart = halfXFiltSz + 1 - xi;
            for ( int fl = flStart; fl < xFiltSz; ++fl ) {
                *mvAvBix += *rotBix * filtX->at(fl);
                rotBix += 1;
            }
        }
        // Calculate with full filter in middle
        for ( int xi = halfXFiltSz + 1; xi <= rotDimX - halfXFiltSz - 1; xi++ ) {
            vector<float>::iterator mvAvBix = fluRow_MvAv->begin() + xi;
            vector<float>::iterator rotBix = fluRow_Rot->begin() + xi - halfXFiltSz;
            for ( int fl = 0; fl < xFiltSz; ++fl ) {
                *mvAvBix += *rotBix * filtX->at(fl);
                rotBix += 1;
            }
        }
        // Calculate with full filter at end
        for ( int xi = rotDimX - halfXFiltSz; xi < rotDimX; xi++ ) {
            vector<float>::iterator mvAvBix = fluRow_MvAv->begin() + xi;
            vector<float>::iterator rotBix = fluRow_Rot->begin() + xi - halfXFiltSz;
            int flEnd = xFiltSz + rotDimX - halfXFiltSz - 1 - xi;
            for ( int fl = 0; fl < flEnd; ++fl ) {
                *mvAvBix += *rotBix * filtX->at(fl);
                rotBix += 1;
            }
        }
        ++fluRow_Rot;
        ++fluRow_MvAv;
    }

    // Calculate moving average along each column
    // Work from temporary matrix back to rot matrix
    rotBixMap->assign(rotDimY,vector<float>(rotDimX,0.0f));
    for ( int xi=0; xi < rotDimX; ++xi ) {

        fluRow_MvAv = rotBixMap->begin();

        // Calculate with partial filter at start
        for ( int yj = 0; yj <= halfYFiltSz; yj++ ) {
            vector<float>::iterator mvAvBix = (fluRow_MvAv + yj)->begin() + xi;
            fluRow_Rot = tmpBixMap->begin();
            vector<float>::iterator rotBix = fluRow_Rot->begin();
            int flStart = halfYFiltSz + 1 - yj;
            for ( int fl = flStart; fl < xFiltSz; ++fl ) {
                *mvAvBix += *rotBix * filtY->at(fl);
                rotBix = (fluRow_Rot++)->begin() + xi;
            }
        }

        // Calculate with full filter in middle
        for ( int yj = halfYFiltSz + 1; yj <= rotDimY - halfYFiltSz - 1; yj++ ) {
            vector<float>::iterator mvAvBix = (fluRow_MvAv + yj)->begin() + xi;
            fluRow_Rot = tmpBixMap->begin() + yj - halfYFiltSz;
            vector<float>::iterator rotBix = fluRow_Rot->begin() + xi;
            for ( int fl = 0; fl < xFiltSz; ++fl ) {
                *mvAvBix += *rotBix * filtY->at(fl);
                rotBix = (fluRow_Rot++)->begin() + xi;  // Valgrind reports error here.
            }
        }

        // Calculate with full filter at end
        for ( int yj = rotDimY - halfYFiltSz; yj < rotDimY; yj++ ) {
            vector<float>::iterator mvAvBix = (fluRow_MvAv + yj)->begin() + xi;
            fluRow_Rot = tmpBixMap->begin() + yj - halfYFiltSz;
            vector<float>::iterator rotBix = fluRow_Rot->begin() + xi;
            int flEnd = xFiltSz + rotDimX - halfXFiltSz - 1 - yj;
            for ( int fl = 0; fl < flEnd; ++fl ) {
                *mvAvBix += *rotBix * filtY->at(fl);
                rotBix = (fluRow_Rot++)->begin() + xi; // Valgrind reports error here.
            }
        }
    }
    delete filtX;
    delete filtY;
    delete tmpBixMap;

    /*if ( absCpInd == 0 ) { // Debugging ...
        cout << endl << "Filtered high resolution rotated bixel map with moving average : " << endl;
        for ( int xi=0; xi < rotDimX; ++xi ) {
            cout << " : " << fixed << xi * tmpBixSz + minPX;
        }
        for ( int yj=0; yj < rotDimY; ++yj ) {
            cout << endl << fixed << yj * tmpBixSz + minPY << " : ";
            for ( int xi=0; xi < rotDimX; ++xi ) {
                cout << rotbixMap->at(yj).at(xi) << " : " << flush;
            }
        }
        cout << endl << "Done" << endl;
    }*/

    // Downsample rotated matrix to actual fluence matrix
    unsigned int firstBix = geometry_->get_beam_starting_bixelNo( dijBeamInd );
    unsigned int lastBix = firstBix + geometry_->get_nBixels_in_beam( dijBeamInd );
    for ( unsigned int bixNum = firstBix; bixNum < lastBix; ++bixNum ) {
        //cout << bixNum << " " << flush;
        float xPos = geometry_->get_bixel_position_x( bixNum );
        float yPos = geometry_->get_bixel_position_y( bixNum );

        float xInd = ( xPos - minPX ) / tmpBixSz;
        float yInd = ( yPos - minPY ) / tmpBixSz;
        int xInd_flr = (int)floor(xInd);
        int xInd_ceil = (int)ceil(xInd);
        int yInd_flr = (int)floor(yInd);
        int yInd_ceil = (int)ceil(yInd);

        float valInterp = 0.0f;
        if ( xInd > 0 ) {
            if ( yInd > 0 )
                valInterp += ( (xInd-(float)xInd_flr) ) * ( ((float)yInd_ceil-yInd) ) *
                             rotBixMap->at(yInd_flr).at(xInd_ceil);
            if ( yInd < rotDimY )
                valInterp += ( (xInd-(float)xInd_flr) ) * ( (yInd-(float)yInd_flr) ) *
                             rotBixMap->at(yInd_ceil).at(xInd_ceil);
        }
        if ( xInd < rotDimX ) {
            if ( yInd > 0 )
                valInterp += ( ((float)xInd_ceil-xInd) ) * ( ((float)yInd_ceil-yInd) ) *
                             rotBixMap->at(yInd_flr).at(xInd_flr);
            if ( yInd < rotDimY )
                valInterp += ( ((float)xInd_ceil-xInd) ) * ( (yInd-(float)yInd_flr) ) *
                             rotBixMap->at(yInd_ceil).at(xInd_flr);
        }

        // Apply control point weighting to interpolated value
        bixVect->add_value(bixNum, valInterp * cpWeight);
    }

    /*if ( absCpInd == 0 ) { // Debugging ...
        cout << endl << "Down sampled bixel map dij reference and resolution : " << endl;
        unsigned int pInd = 0;
        for ( int xi=0; xi < 20; ++xi )
            cout << " : " << fixed << geometry_->get_bixel_position_x( pInd+xi );
        for ( int yj=0; yj < 20; ++yj ) {
            cout << endl << fixed << geometry_->get_bixel_position_y( pInd ) << " : ";
            for ( int xi=0; xi < 20; ++xi ) {
                cout << bixVect->get_value(pInd++) << " : " << flush;
            }
        }
        cout << endl << "Done" << endl;
    }*/

    delete rotBixMap;
}


/**
 * Translate the bixel gradient terms onto Jaw position gradient terms for a given control point.
 */
void DirectParameterVector::populateGradientForXJaws( unsigned int cpInd, BixelVectorDirection &bvd, ParameterVectorDirection &pvd,
                                                     unsigned int dijBeamInd, float cpContribToFlu, float cpCollAngle,
                                                     float cpXBixSz, float cpYBixSz ) const {

    float xjawPos, jawVertX0, jawVertY0, jawVertX1, jawVertY1;

    float cosHT = cos( cpCollAngle * PI / 180.0f );
    float sinHT = sin( cpCollAngle * PI / 180.0f );

    float minYAx = get_minYAx( get_PIndCP(cpInd) );
    float maxYAx = get_maxYAx( get_PIndCP(cpInd) );

    xjawPos = get_XJaw(cpInd, 0);
    jawVertX0 = xjawPos * cosHT - minYAx * sinHT;
    jawVertY0 = xjawPos * sinHT + minYAx * cosHT;
    jawVertX1 = xjawPos * cosHT - maxYAx * sinHT;
    jawVertY1 = xjawPos * sinHT + maxYAx * cosHT;

    int pInd = get_PInd( cpInd, XJaw, 0 );
    pvd[ pInd ] = (-1) * findGradientAlongLine( bvd, jawVertX0, jawVertY0, jawVertX1, jawVertY1, dijBeamInd,
                                               cpXBixSz, cpYBixSz ) * cpContribToFlu;

    xjawPos = get_XJaw(cpInd, 1);
    jawVertX0 = xjawPos * cosHT - minYAx * sinHT;
    jawVertY0 = xjawPos * sinHT + minYAx * cosHT;
    jawVertX1 = xjawPos * cosHT - maxYAx * sinHT;
    jawVertY1 = xjawPos * sinHT + maxYAx * cosHT;

    pInd = get_PInd( cpInd, XJaw, 1 );
    pvd[ pInd ] = findGradientAlongLine( bvd, jawVertX0, jawVertY0, jawVertX1, jawVertY1, dijBeamInd,
                                               cpXBixSz, cpYBixSz ) * cpContribToFlu;
}

/**
 * Translate the bixel gradient terms onto Jaw position gradient terms for a given control point.
 */
void DirectParameterVector::populateGradientForYJaws( unsigned int cpInd, BixelVectorDirection &bvd, ParameterVectorDirection &pvd,
                                                     unsigned int dijBeamInd, float cpContribToFlu, float cpCollAngle,
                                                     float cpXBixSz, float cpYBixSz ) const {

    float yjawPos, jawVertX0, jawVertY0, jawVertX1, jawVertY1;

    float cosHT = cos( cpCollAngle * PI / 180.0f );
    float sinHT = sin( cpCollAngle * PI / 180.0f );

    float minXAx = get_minXAx( get_PIndCP(cpInd) );
    float maxXAx = get_maxXAx( get_PIndCP(cpInd) );

    yjawPos = get_YJaw(cpInd, 0);
    jawVertX0 = minXAx * cosHT - yjawPos * sinHT;
    jawVertY0 = minXAx * sinHT + yjawPos * cosHT;
    jawVertX1 = maxXAx * cosHT - yjawPos * sinHT;
    jawVertY1 = maxXAx * sinHT + yjawPos * cosHT;

    int pInd = get_PInd( cpInd, YJaw, 0 );
    pvd[ pInd ] = (-1) * findGradientAlongLine( bvd, jawVertX0, jawVertY0, jawVertX1, jawVertY1, dijBeamInd,
                                               cpXBixSz, cpYBixSz ) * cpContribToFlu;

    yjawPos = get_YJaw(cpInd, 1);
    jawVertX0 = minXAx * cosHT - yjawPos * sinHT;
    jawVertY0 = minXAx * sinHT + yjawPos * cosHT;
    jawVertX1 = maxXAx * cosHT - yjawPos * sinHT;
    jawVertY1 = maxXAx * sinHT + yjawPos * cosHT;

    pInd = get_PInd( cpInd, YJaw, 1 );
    pvd[ pInd ] = findGradientAlongLine( bvd, jawVertX0, jawVertY0, jawVertX1, jawVertY1, dijBeamInd,
                                               cpXBixSz, cpYBixSz ) * cpContribToFlu;
}


/**
 * Translate the bixel gradient terms onto leaf position gradient terms for a given control point.
 */
void DirectParameterVector::populateGradientForXLeaves( unsigned int cpInd, BixelVectorDirection &bvd, ParameterVectorDirection &pvd,
                                                     unsigned int dijBeamInd, float cpContribToFlu, float cpCollAngle,
                                                     float cpXBixSz, float cpYBixSz ) const {

    // Calculate position of leaf vertices
    int nLeafPairs = (int) machine_->getNXLeaves() / 2;

    vector<float> leafBnk, leafBnk_RotX, leafBnk_RotY;
    vector<float> leafX_OnAxY( nLeafPairs, 0.0f );
    vector<float> leafWidth( nLeafPairs, 0.0f );
    int lf;
    if ( get_nParamOfType( cpInd, XLeaf ) > 0 )
        for ( lf=0; lf<nLeafPairs; lf++ ) {
            leafWidth[lf] = machine_->getXLeaf(lf)->getWidth();
            leafX_OnAxY[lf] = machine_->getXLeaf(lf)->getPosition() - ( leafWidth[lf] / 2.0f );
        }

    leafBnk_RotX.reserve( nLeafPairs * 2 ); // Two tip vertices for each leaf in one bank
    leafBnk_RotY.reserve( nLeafPairs * 2 );
    float cosHT = cos( cpCollAngle * PI / 180.0f );
    float sinHT = sin( cpCollAngle * PI / 180.0f );

    // First bank of leaves
    get_XLeaves( cpInd, leafBnk, 0 );
    for ( lf=0; lf<nLeafPairs; lf++ ) {
        leafBnk_RotX[lf*2] = leafBnk[lf] * cosHT - leafX_OnAxY[lf] * sinHT;
        leafBnk_RotY[lf*2] = leafBnk[lf] * sinHT + leafX_OnAxY[lf] * cosHT;
        leafBnk_RotX[lf*2+1] = leafBnk[lf] * cosHT - ( leafX_OnAxY[lf] + leafWidth[lf] ) * sinHT;
        leafBnk_RotY[lf*2+1] = leafBnk[lf] * sinHT + ( leafX_OnAxY[lf] + leafWidth[lf] ) * cosHT;
    }

    // Trace bixels along leaf tip edge
    for ( lf=0; lf<nLeafPairs; lf++ ) {
        // Increase in leaf position decreases bixel weighting so sign of gradient is changed
        int pInd = get_PInd( cpInd, XLeaf, lf );
        pvd[ pInd ] = (-1) * findGradientAlongLine( bvd, leafBnk_RotX[lf*2], leafBnk_RotY[lf*2], leafBnk_RotX[lf*2+1],
                                            leafBnk_RotY[lf*2+1], dijBeamInd, cpXBixSz, cpYBixSz ) * cpContribToFlu;
    }

    // Repeat for second bank of leaves
    get_XLeaves( cpInd, leafBnk, 1 );
    for ( int lf=0; lf<nLeafPairs; lf++ ) {
        leafBnk_RotX[lf*2] = leafBnk[lf] * cosHT - leafX_OnAxY[lf] * sinHT;
        leafBnk_RotY[lf*2] = leafBnk[lf] * sinHT + leafX_OnAxY[lf] * cosHT;
        leafBnk_RotX[lf*2+1] = leafBnk[lf] * cosHT - ( leafX_OnAxY[lf] + leafWidth[lf] ) * sinHT;
        leafBnk_RotY[lf*2+1] = leafBnk[lf] * sinHT + ( leafX_OnAxY[lf] + leafWidth[lf] ) * cosHT;
    }

    // Trace bixels along leaf tip edge
    for ( lf=0; lf<nLeafPairs; lf++ ) {
        int pInd = get_PInd( cpInd, XLeaf, lf ) + nLeafPairs;
        pvd[ pInd ] = findGradientAlongLine( bvd, leafBnk_RotX[lf*2], leafBnk_RotY[lf*2], leafBnk_RotX[lf*2+1],
                                            leafBnk_RotY[lf*2+1], dijBeamInd, cpXBixSz, cpYBixSz ) * cpContribToFlu;
    }
}


/**
 * Translate the bixel gradient terms onto leaf position gradient terms for a given control point.
 */
void DirectParameterVector::populateGradientForYLeaves( unsigned int cpInd, BixelVectorDirection &bvd, ParameterVectorDirection &pvd,
                                                     unsigned int dijBeamInd, float cpContribToFlu, float cpCollAngle,
                                                     float cpXBixSz, float cpYBixSz ) const {

    // Calculate position of leaf vertices
    int nLeafPairs = (int) machine_->getNYLeaves() / 2;

    vector<float> leafBnk, leafBnk_RotX, leafBnk_RotY;
    vector<float> leafY_OnAxX( nLeafPairs, 0.0f );
    vector<float> leafWidth( nLeafPairs, 0.0f );
    int lf;
    if ( get_nParamOfType( cpInd, YLeaf ) > 0 )
        for ( lf=0; lf<nLeafPairs; lf++ ) {
            leafWidth[lf] = machine_->getYLeaf(lf)->getWidth();
            leafY_OnAxX[lf] = machine_->getYLeaf(lf)->getPosition() - ( leafWidth[lf] / 2.0f );
        }

    leafBnk_RotX.reserve( nLeafPairs * 2 ); // Two tip vertices for each leaf in one bank
    leafBnk_RotY.reserve( nLeafPairs * 2 );
    float cosHT = cos( cpCollAngle * PI / 180.0f );
    float sinHT = sin( cpCollAngle * PI / 180.0f );

    // First bank of leaves
    get_YLeaves( cpInd, leafBnk, 0 );
    for ( lf=0; lf<nLeafPairs; lf++ ) {
        leafBnk_RotX[lf*2] = leafY_OnAxX[lf] * cosHT - leafBnk[lf] * sinHT;
        leafBnk_RotY[lf*2] = leafY_OnAxX[lf] * sinHT + leafBnk[lf] * cosHT;
        leafBnk_RotX[lf*2+1] = ( leafY_OnAxX[lf] + leafWidth[lf] ) * cosHT - leafBnk[lf] * sinHT;
        leafBnk_RotY[lf*2+1] = ( leafY_OnAxX[lf] + leafWidth[lf] ) * sinHT + leafBnk[lf] * cosHT;
    }

    // Trace bixels along leaf tip edge
    for ( lf=0; lf<nLeafPairs; lf++ ) {
        // Increase in leaf position decreases bixel weighting so sign of gradient is changed
        int pInd = get_PInd( cpInd, YLeaf, lf );
        pvd[ pInd ] = (-1) * findGradientAlongLine( bvd, leafBnk_RotX[lf*2], leafBnk_RotY[lf*2], leafBnk_RotX[lf*2+1],
                                            leafBnk_RotY[lf*2+1], dijBeamInd, cpXBixSz, cpYBixSz ) * cpContribToFlu;
    }

    // Repeat for second bank of leaves
    get_YLeaves( cpInd, leafBnk, 1 );
    for ( lf=0; lf<nLeafPairs; lf++ ) {
        leafBnk_RotX[lf*2] = leafY_OnAxX[lf] * cosHT - leafBnk[lf] * sinHT;
        leafBnk_RotY[lf*2] = leafY_OnAxX[lf] * sinHT + leafBnk[lf] * cosHT;
        leafBnk_RotX[lf*2+1] = ( leafY_OnAxX[lf] + leafWidth[lf] ) * cosHT - leafBnk[lf] * sinHT;
        leafBnk_RotY[lf*2+1] = ( leafY_OnAxX[lf] + leafWidth[lf] ) * sinHT + leafBnk[lf] * cosHT;
    }
    // Trace bixels along leaf tip edge
    for ( lf=0; lf<nLeafPairs; lf++ ) {
        int pInd = get_PInd( cpInd, YLeaf, lf ) + nLeafPairs;
        pvd[ pInd ] = findGradientAlongLine( bvd, leafBnk_RotX[lf*2], leafBnk_RotY[lf*2], leafBnk_RotX[lf*2+1],
                                            leafBnk_RotY[lf*2+1], dijBeamInd, cpXBixSz, cpYBixSz ) * cpContribToFlu;
    }
}


/**
 * Find the mean gradient encountered along a line between two points in a bixel map.
 */
float DirectParameterVector::findGradientAlongLine( BixelVectorDirection &bvd, float startPosX, float startPosY,
                                                  float endPosX, float endPosY, unsigned int dijBeamInd,
                                                  float cpXBixSz, float cpYBixSz ) const {

    float lfGradSum = 0.0f;

    if ( endPosX < startPosX ) {
        float tmp;  // Swop start and end points
        tmp = startPosX;    startPosX = endPosX;        endPosX = tmp;
        tmp = startPosY;    startPosY = endPosY;        endPosY = tmp;
    }

    float lineGradient = ( endPosY - startPosY ) / ( endPosX - startPosX );

    // If start or end position is outside bixel map then project onto bixel grid
    float minX = geometry_->get_bixel_minX( dijBeamInd );
    float maxX = geometry_->get_bixel_maxX( dijBeamInd );
    float minY = geometry_->get_bixel_minY( dijBeamInd );
    float maxY = geometry_->get_bixel_maxY( dijBeamInd );

    if ( startPosY < minY ) {
        startPosX = ( minY - startPosY ) / lineGradient + startPosX;
        startPosY = minY;
    }
    if ( startPosY > maxY ) {
        startPosX = ( maxY - startPosY ) / lineGradient + startPosX;
        startPosY = maxY;
    }
    if ( startPosX < minX ) {
        startPosY = ( minX - startPosX ) * lineGradient + startPosY;
        startPosX = minX;
    }
    if ( startPosX > maxX ) {
        startPosY = ( maxX - startPosX ) * lineGradient + startPosY;
        startPosX = maxX;
    }

    if ( endPosY < minY ) {
        endPosX = ( minY - endPosY ) / lineGradient + endPosX;
        endPosY = minY;
    }
    if ( endPosY > maxY ) {
        endPosX = ( maxY - endPosY ) / lineGradient + endPosX;
        endPosY = maxY;
    }
    if ( endPosX < minX ) {
        endPosY = ( minX - endPosX ) * lineGradient + endPosY;
        endPosX = minX;
    }
    if ( endPosX > maxX ) {
        endPosY = ( maxX - endPosX ) * lineGradient + endPosY;
        endPosX = maxX;
    }

    // If projected line length is zero (i.e. start and end were outside bixel grid on same side) then return zero.
    if ( startPosX == endPosX && startPosY == endPosY ) return 0.0f;

    // To exclude invalid bixels the line length must be accumulated inside the do loop.
    float lineLength = 0.0f;

    // Find bixel under the start and end points of the line
    Bixel_Subscripts startBix = geometry_->convert_to_Bixel_Subscripts( Bixel_Position(startPosX,startPosY,0.0f), dijBeamInd );
    Bixel_Subscripts endBix = geometry_->convert_to_Bixel_Subscripts( Bixel_Position(endPosX,endPosY,0.0f), dijBeamInd );

    startBix.iE_ = 0;
    endBix.iE_ = 0;
    Bixel_Subscripts curBix = startBix;

    unsigned int bixNum = geometry_->get_bixelNo( curBix, dijBeamInd );

    // If start and end point is in the same bixel then return gradient of that bixel
    if ( curBix.iX_ == endBix.iX_ && curBix.iY_ == endBix.iY_ ) return bvd[ bixNum ];

    // Otherwise trace the line along the bixels and record the gradient of each bixel traversed
    // weighted by the length of the line segment over that bixel.
    float intersectX, intersectY, inBixLength;
    float lastIntersectLength = 0.0f;

    // Querrying position from geometry class goes wrong for invalid bixels
    // float testPosX = geometry_->get_bixel_position_x(bixNum) + cpXBixSz / 2;
    float startBixCenterX = (int)((startPosX - minX) / cpXBixSz + 0.5) * cpXBixSz + minX;
    float startBixCenterY = (int)((startPosY - minY) / cpYBixSz + 0.5) * cpYBixSz + minY;

    float testPosX = startBixCenterX + cpXBixSz / 2.0f;

    if ( endPosY > startPosY ) {

        float testPosY = startBixCenterY + cpYBixSz / 2.0f;
        intersectX = startPosX;
        intersectY = startPosY;

        bool tipReached = false;
        do {
            if ( curBix.iX_ == endBix.iX_ && curBix.iY_ == endBix.iY_ ) tipReached = true;

            bixNum = geometry_->get_bixelNo( curBix, dijBeamInd );

            if ( lineGradient == 0 ) {
                // Line is horizontal move along x
                intersectX = testPosX;
                curBix.iX_++;
                testPosX += cpXBixSz;
            } else if ( std::isinf(lineGradient) ) {
                // Line is vertical move along y
                intersectY = testPosY;
                curBix.iY_++;
                testPosY += cpYBixSz;
            } else {
                // Line is at an angle compute intersection with bixel boundary
                intersectY = ( testPosX - startPosX ) * lineGradient + startPosY;
                if ( intersectY > testPosY ) {
                    // Line intersects x bixel boundary
                    intersectY = testPosY < endPosY ? testPosY : endPosY;
                    intersectX = ( intersectY - startPosY ) / lineGradient + startPosX;
                    curBix.iX_++;
                    testPosX += cpXBixSz;
                } else {
                    // Line intersects y bixel boundary
                    intersectX = testPosX < endPosX ? testPosX : endPosX;
                    intersectY = ( intersectX - startPosX ) * lineGradient + startPosY;
                    curBix.iY_++;
                    testPosY += cpYBixSz;
                }
            }

            // compute length of line inside current bixel
            float intersectLength = sqrt( ( intersectY - startPosY ) * ( intersectY - startPosY ) +
                                        ( intersectX - startPosX ) * ( intersectX - startPosX ) );

            // Add gradient contribution weighting by line length in bixel
            if ( geometry_->is_valid_bixel( curBix, dijBeamInd ) ){
                inBixLength = ( intersectLength - lastIntersectLength );
                lfGradSum += bvd[ bixNum ] * inBixLength;
                lineLength += inBixLength;
            }
            lastIntersectLength = intersectLength;

        } while (!tipReached);

    } else {
        float testPosY = startBixCenterY - cpYBixSz / 2.0f;


        bool tipReached = false;
        do {
            if ( curBix.iX_ == endBix.iX_ && curBix.iY_ == endBix.iY_ ) tipReached = true;

            unsigned int bixNum = geometry_->get_bixelNo( curBix, dijBeamInd );

            if ( lineGradient == 0 ) {
                // Line is horizontal move along x
                intersectX = testPosX;
                curBix.iX_++;
                testPosX += cpXBixSz;
            } else if ( std::isinf(lineGradient) ) {
                // Line is vertical move along y
                intersectY = testPosY;
                curBix.iY_--;
                testPosY -= cpYBixSz;
            } else {
                // Line is at an angle compute intersection with bixel boundary
                intersectY = ( testPosX - startPosX ) * lineGradient + startPosY;
                if ( intersectY > testPosY ) {
                    // Line intersects x bixel boundary
                    intersectY = testPosY > endPosY ? testPosY : endPosY;
                    intersectX = ( intersectY - startPosY ) / lineGradient + startPosX;
                    curBix.iX_++;
                    testPosX += cpXBixSz;
                } else {
                    // Line intersects y bixel boundary
                    intersectX = testPosX < endPosX ? testPosX : endPosX;
                    intersectY = ( intersectX - startPosX ) * lineGradient + startPosY;
                    curBix.iY_--;
                    testPosY -= cpYBixSz;
                }
            }

            // compute length of line inside current bixel
            float intersectLength = sqrt( (intersectY-startPosY) * (intersectY-startPosY) +
                                               (intersectX-startPosX) * (intersectX-startPosX) );

            // Add gradient contribution weighting by line length in bixel
            if ( geometry_->is_valid_bixel( curBix, dijBeamInd ) ){
                inBixLength = ( intersectLength - lastIntersectLength );
                lfGradSum += bvd[ bixNum ] * inBixLength;
                lineLength += inBixLength;
            }
            lastIntersectLength = intersectLength;

        } while (!tipReached);
    }

    float gradient = lineLength > 0 ? ( lfGradSum / lineLength ) : 0.0f;
    assert(!isnan(gradient));

    return gradient;
}


/**
 * Test if parameter vectors are equal.
 * Test will return false if there are different number of parameters or if there are any differences in :
 * values, lower bounds OR high bounds.
 * I.E. The values in the parameters can be the same but the test will return false if the bounds are different.
 */
bool DirectParameterVector::operator==( const DirectParameterVector& rhs ) const {
    return ( ParameterVector::operator==( rhs ) && ( absHighBound_ == rhs.absHighBound_ ) &&
            ( absLowBound_ == rhs.absLowBound_ ) && ( nf_ == rhs.nf_ ) && ( ncp_ == rhs.ncp_ ) &&
            ( paramPerCP_ == rhs.paramPerCP_ ) );
}


/**
 * Reserve space for a ParameterVector with nParams elements.
 * Requests that the capacity of the allocated storage space for the elements of the vector container be at least
 * enough to hold n elements.
 *
 * @param nParams    The number of elements that the vector is required to store without reallocation.
 */
void DirectParameterVector::reserve_nParams( const unsigned int nParams ) {
    ParameterVector::reserve_nParams( nParams );

    type_.reserve( nParams_ );
    absLowBound_.reserve( nParams );
    absHighBound_.reserve( nParams );
    isDiscreteVariable_.reserve( nParams );
    numberOfValidValues_.reserve( nParams );

    //validValues_ = dpv.validValues_;
    //validSpeeds_ = dpv.validSpeeds_;
}


/**
 * Adds a new element at the end of the vector, after its current last element.
 * The content of this new element is initialized with the set value, lowBound, highBound and isActive values.
 *
 * @param value     The value for the new ParameterVector element.
 * @param lowBound  The lower Bound for the new ParameterVector element.
 * @param lowBound  The lower Bound for the new ParameterVector element.
 * @param isActive  The activity flag for the new ParameterVector element.
 * @param step_size The step size by which the parameter value should be incremented.
 * @param absLowBound	The smallest valid value when considered independantly of other parameters.
 * @param absHighBound	The largest valid value when considered independantly of other parameters.
  */
void DirectParameterVector::push_back_parameter( const float &value, const float &lowBound, const float &highBound,
        const bool &isActive, const float &step_size, const float &absLowBound, const float &absHighBound ) {

    cerr << "Cannot add single parameters to vector only control points" << endl;
    throw DirectParameterVector_exception();

    /*
    ParameterVector::push_back_parameter( value, lowBound, highBound, isActive, step_size );

    absLowBound_.push_back( absLowBound );
    absHighBound_.push_back( absHighBound );
    // Assumed quantities
    isDiscreteVariable_.push_back( true );
    numberOfValidValues_.reserve( (unsigned int)((absHighBound-absLowBound)/step_size) );
    */
}


/**
 * Swap the contents of two ParameterVectors.
 *
 * @param rhs The ParameterVector to swap with the current one
 */
void DirectParameterVector::swap( DirectParameterVector & rhs ) {

    ParameterVector::swap( rhs );

    absLowBound_.swap( rhs.absLowBound_ );
    absHighBound_.swap( rhs.absHighBound_ );
    isDiscreteVariable_.swap( rhs.isDiscreteVariable_ );
    numberOfValidValues_.swap( rhs.numberOfValidValues_ );
    validValues_.swap( rhs.validValues_ );
    type_.swap( rhs.type_ );

    isConstantEnergyInFd_.swap( rhs.isConstantEnergyInFd_ );
    isConstantDoseRateInFd_.swap( rhs.isConstantDoseRateInFd_ );
    isConstantGantryInFd_.swap( rhs.isConstantGantryInFd_ );
    isConstantCollInFd_.swap( rhs.isConstantCollInFd_ );
    isConstantCouchInFd_.swap( rhs.isConstantCouchInFd_ );
    isConstantXJawInFd_.swap( rhs.isConstantXJawInFd_ );
    isConstantYJawInFd_.swap( rhs.isConstantYJawInFd_ );
    isConstantXLeafInFd_.swap( rhs.isConstantXLeafInFd_ );
    isConstantYLeafInFd_.swap( rhs.isConstantYLeafInFd_ );

    ncp_.swap( rhs.ncp_ );
    paramPerCP_.swap( rhs.paramPerCP_ );
    nWeightingParams_.swap( rhs.nWeightingParams_ );
    nBeamEnergyParams_.swap( rhs.nBeamEnergyParams_ );
    nDoseRateParams_.swap( rhs.nDoseRateParams_ );
    nGantryParams_.swap( rhs.nGantryParams_ );
    nCollimatorParams_.swap( rhs.nCollimatorParams_ );
    nCouchParams_.swap( rhs.nCouchParams_ );
    nXJawParams_.swap( rhs.nXJawParams_ );
    nYJawParams_.swap( rhs.nYJawParams_ );
    nXLeafParams_.swap( rhs.nXLeafParams_ );
    nYLeafParams_.swap( rhs.nYLeafParams_ );

    DeliveryMachineModel * tmpMachine = machine_.release();
    machine_ = rhs.machine_;
    rhs.machine_.reset( tmpMachine ) ;

    BixelVector * tmpBixMap = rhs.bixMap_;
    rhs.bixMap_ = bixMap_;
    bixMap_ = tmpBixMap;

    Geometry const * tmpGeometry = rhs.geometry_;
    rhs.geometry_ = geometry_;
    geometry_ = tmpGeometry;

    double dtmp = doseNormFactor_;
    doseNormFactor_ = rhs.doseNormFactor_;
    rhs.doseNormFactor_ = dtmp;

    bool btmp = isWeightingsAbsolute_;
    isWeightingsAbsolute_ = rhs.isWeightingsAbsolute_;
    rhs.isWeightingsAbsolute_ = btmp;

    unsigned int utmp = nf_;
    nf_ = rhs.nf_;
    rhs.nf_ = utmp;

    utmp = ncpTot_;
    ncpTot_ = rhs.ncpTot_;
    rhs.ncpTot_ = utmp;
}


/**
 * Find out if the parameter set is physically imposible.
 * Check for parameter values which are lower than lower bound or higher than higher bound, then check against
 * specific delivery constraints.
 *
 * N.B. Currently only verifies delivery constraints relevant to leaf and jaw positions.
 *
 * @returns true if the parameter vector is impossible to deliver.
 */
bool DirectParameterVector::is_impossible() const {

    if ( ParameterVector::is_impossible() ) {
        if ( DEBUG_MESSAGES ) {
            for ( unsigned int ip = 0; ip < value_.size(); ip++ ) {
                if ( value_[ip] < lowBound_[ip] ) {
                    cout << type_[ip] << " parameter " << ip << " in cp " << paramIndToCPInd(ip)
                        << ", is too low value = " << value_[ip] << " Limits = [ "
                        << lowBound_[ip] << " , " << highBound_[ip] << " ] : ";
                    if ( type_[ip-1] == type_[ip] )
                        cout << type_[ip-1] << " pair " << ip-1 << " value = " << value_[ip-1] << " : ";
                    if ( type_[ip+1] == type_[ip] )
                        cout << type_[ip+1] << " pair " << ip+1 << " value = " << value_[ip+1] << " : ";

                    unsigned int ip2;
                    ip2 = get_PInd(paramIndToCPInd(ip)-1,type_[ip],ip-get_PInd(paramIndToCPInd(ip),type_[ip],0));
                    if ( ip2 > 0 )
                        cout << type_[ip2] << " previous cp " << ip2 << " value = " << value_[ip2] << " : ";

                    ip2 = get_PInd(paramIndToCPInd(ip)+1,type_[ip],ip-get_PInd(paramIndToCPInd(ip),type_[ip],0));
                    if ( ip2 < nParams_ )
                        cout << type_[ip2] << " next cp " << ip2 << " value = " << value_[ip2] << " : ";

                    cout << endl;
                }
                if ( value_[ip] > highBound_[ip] ) {
                    cout << type_[ip] << " parameter " << ip << " in cp " << paramIndToCPInd(ip)
                        << ", is too high value = " << value_[ip] << " Limits = [ "
                        << lowBound_[ip] << " , " << highBound_[ip] << " ] : ";
                    if ( type_[ip-1] == type_[ip] )
                        cout << type_[ip-1] << "pair " << ip-1 << " value = " << value_[ip-1] << " : ";
                    if ( type_[ip+1] == type_[ip] )
                        cout << type_[ip+1] << "pair " << ip+1 << " value = " << value_[ip+1] << " : ";

                    unsigned int ip2;
                    ip2 = get_PInd(paramIndToCPInd(ip)-1,type_[ip],ip-get_PInd(paramIndToCPInd(ip),type_[ip],0));
                    if ( ip2 > 0 )
                        cout << type_[ip2] << " previous cp " << ip2 << " value = " << value_[ip2] << " : ";

                    ip2 = get_PInd(paramIndToCPInd(ip)+1,type_[ip],ip-get_PInd(paramIndToCPInd(ip),type_[ip],0));
                    if ( ip2 < nParams_ )
                        cout << type_[ip2] << " next cp " << ip2 << " value = " << value_[ip2] << " : ";

                    cout << endl;
                }
            }
        }
        return true;
    }

    // Check that discrete parameters are set to an allowed value.
    float relTol = 0.01; // 1% tolerance limit
	for ( unsigned int pn=0; pn<get_nParameters(); pn++ ) {
        if (isDiscreteVariable_[pn]) {
            float curValue = get_value(pn);
            float absTol = std::max( relTol, relTol * curValue ); // When curValue = 0 set tolerance to positive number
            if ( std::abs( curValue - get_RoundToValidValue( pn, curValue ) ) > absTol ) {
                if ( DEBUG_MESSAGES ) {
                    cout << "Value not allowed for discrete " << type_[pn] << " parameter " << pn << " = " << curValue
                        << " closest = " << get_RoundToValidValue( pn, curValue ) << " tol = " << absTol
                        << " at abs cp " << paramIndToCPInd(pn) << " or field "
                        << get_Fieldindex(paramIndToCPInd(pn)) << " and cp "
                        << paramIndToCPInd(pn)-get_absCPindex(get_Fieldindex(paramIndToCPInd(pn)),0) << endl;
                }
                return true;
            }
        }
    }

    int cpi=0;
    for ( unsigned int fd = 0; fd < nf_; fd++ ) {

        // Get a time axis for checking dynamic parameters
        std::vector<float> cpTime ( ncp_[fd], 0 );
        for ( unsigned int cp = 0; cp < ncp_[fd]; cp++ ) {
            // if dose rate is not set then assume that we can set the
            // dose rate to minimum positive value if required.
            if ( nDoseRateParams_[cpi+cp] > 0 ) {
                cpTime[cp] = doseNormFactor_ * get_Weight(cpi+cp) / get_DoseRate(cpi+cp);
            } else {
                if ( machine_->getDoseRate()->getMinValue() > 0 ) {
                    cpTime[cp] = doseNormFactor_ * get_Weight(cpi+cp) / machine_->getDoseRate()->getMinValue();
                } else {
                    cpTime[cp] = std::numeric_limits<float>::max() / ncp_[fd];
                }
            }
        }

        if ( is_impossible_ArcWeights( fd ) ) {
            if ( DEBUG_MESSAGES ) cout << "Arc Weights Problem" << endl;
            return true;
        }
        if ( nXJawParams_[cpi] > 0 ) {
            if ( is_impossible_XJaw(fd, cpTime) ) {
                if ( DEBUG_MESSAGES ) cout << "X Jaw Problem" << endl;
                return true;
            }
        }
        if ( nYJawParams_[cpi] > 0 ) {
            if ( is_impossible_YJaw(fd, cpTime) ) {
                if ( DEBUG_MESSAGES ) cout << "Y Jaw Problem" << endl;
                return true;
            }
        }
        if ( nXLeafParams_[cpi] > 0 ) {
            if ( is_impossible_XMlc(fd, cpTime) ) {
                if ( DEBUG_MESSAGES ) cout << "X MLC Problem" << endl;
                return true;
            }
        }
        if ( nYLeafParams_[cpi] > 0 ) {
            if ( is_impossible_YMlc(fd, cpTime) ) {
                if ( DEBUG_MESSAGES ) cout << "Y MLC Problem" << endl;
                return true;
            }
        }
        cpi += ncp_[fd];
    }

    // If we made it this far, no entries are impossible
    return false;
}


/**
 * Find out if the control point weighting parameters are physically imposible to deliver.
 * Check against the limitations on gantry, collimator and couch rotation speed and on doserate.
 * N.B. Do not consider leaf or jaw motion.
 *
 * @returns true if the parameter vector is impossible to deliver.
 */
bool DirectParameterVector::is_impossible_ArcWeights( unsigned int fd ) const {

    vector<vector<float> > weightLim = findArcWeightLimits( fd );

    unsigned int cpi = get_absCPindex( fd, 0 );
    for ( unsigned int cp = 0; cp < ncp_[fd]; cp++ ) {
        unsigned int ip0 = get_PInd(cpi+cp, Weight);
        if ( value_[ip0] < weightLim[0][cp] ) {
            if ( DEBUG_MESSAGES ) cout << " Weight term " << ip0 << " " << value_[ip0] << " ( " << weightLim[0][cp] << " , " << weightLim[1][cp] << " )" << endl;
            return true;
        }
        if ( value_[ip0] > weightLim[1][cp] ) {
            if ( DEBUG_MESSAGES ) cout << " Weight term " << ip0 << " " << value_[ip0] << " ( " << weightLim[0][cp] << " , " << weightLim[1][cp] << " )" << endl;
            return true;
        }
    }
    return false;
}


/**
 * Map control point weightings into the allowed region given a certain gantry collimator and or couch motion.
 */
void DirectParameterVector::makePossible_ArcWeights( unsigned int fd ) {

    vector<vector<float> > weightLim = findArcWeightLimits( fd );

    unsigned int cpi = get_absCPindex( fd, 0 );
    for ( unsigned int cp = 0; cp < ncp_[fd]; cp++ ) {

        unsigned int ip0 = get_PInd(cpi+cp, Weight);

        // If weight is too low then reset weight
        if ( value_[ip0] < weightLim[0][cp] ) {
            value_[ip0] = weightLim[0][cp];
        }

        // If weight is too high then reset weight
        if ( value_[ip0] > weightLim[1][cp] ) {
            value_[ip0] = weightLim[1][cp];
        }

        // Check that weight is a valid increment of the weight step size
        if ( step_size_[ip0] > 0 ) {
            value_[ip0] = get_absLowBound(ip0) +
                ceil( ( value_[ip0] - get_absLowBound(ip0) ) / get_step_size(ip0) - 0.5f ) * get_step_size(ip0);
            value_[ip0] += ( value_[ip0] < weightLim[0][cp] ) ? get_step_size(ip0) : 0.0f;
        }
    }
}


/**
 * Calculate a 2xN vector of minimum and maximum weightings for each arc control point.
 *
 * @returns 2xN vector of floats representing minimum and maximum weightings for each arc control point.
 */
vector<vector<float> > DirectParameterVector::findArcWeightLimits( unsigned int fd ) const {

    unsigned int cpi = get_absCPindex( fd, 0 );

    vector<float> minWeight( ncp_[fd], 0.0f );
    vector<float> maxWeight( ncp_[fd], numeric_limits<float>::infinity() );

    vector<float> doseRateMin(ncp_[fd], 0.0f);
    vector<float> doseRateMax(ncp_[fd], 0.0f);
    for ( unsigned int cp = 0; cp < ncp_[fd]; cp++ ) {
        if ( nDoseRateParams_[cpi+cp] > 0 ) {
            doseRateMin[cpi+cp] = get_DoseRate(cpi+cp);
            doseRateMax[cpi+cp] = doseRateMin[cpi+cp];
        } else {
            doseRateMin[cpi+cp] = machine_->getDoseRate()->getMinValue();
            doseRateMax[cpi+cp] = machine_->getDoseRate()->getMaxValue();
        }
    }

    // Gantry angle constraint
    if ( ! isConstantGantryInFd_[fd] ) {
        vector<float> gantryAngle(ncp_[fd], 0.0f);
        for ( unsigned int cp = 0; cp < ncp_[fd]; cp++ )
            gantryAngle[cp] = get_Gantry(cpi+cp);

        float maxSpd = machine_->getGantry()->getMaxSpeed();
        float minSpd = machine_->getGantry()->getMinSpeed();

        float diffNxt = abs( gantryAngle[1] - gantryAngle[0] );

        minWeight[0] = max( minWeight[0], ( diffNxt / 2.0f ) / maxSpd * doseRateMin[0] );
        //maxWeight[0] = min( maxWeight[0], ( diffNxt / 2.0f ) * minSpd / doseRateMax[0] );

        float diffPrv = diffNxt;
        for ( unsigned int cp = 1; cp < (ncp_[fd]-1); cp++ ) {
            diffNxt = abs( gantryAngle[cp+1] - gantryAngle[cp] );
            float diffTot = diffNxt / 2.0f + diffPrv / 2.0f;

            minWeight[cp] = max( minWeight[cp], diffTot / maxSpd * doseRateMin[cp] );
            if ( gantryAngle[cp+1] != gantryAngle[0] && gantryAngle[cp] != gantryAngle[ncp_[fd]-1] ) {
                maxWeight[cp] = min( maxWeight[cp], diffTot / minSpd * doseRateMax[cp] );
            }
            diffPrv = diffNxt;
        }
        minWeight[ncp_[fd]-1] = max( minWeight[ncp_[fd]-1], ( diffPrv / 2.0f ) / maxSpd * doseRateMin[ncp_[fd]-1] );
        //maxWeight[ncp_[fd]-1] = min( maxWeight[ncp_[fd]-1], ( diffPrv / 2.0f ) * minSpd / doseRateMax[ncp_[fd]-1] );
        for ( unsigned int cp = 1; cp < (ncp_[fd]-1); cp++ ) {
        }
    }

    // Collimator angle constraint
    if ( ! isConstantCollInFd_[fd] ) {
        vector<float> collAngle(ncp_[fd], 0.0f);
        for ( unsigned int cp = 0; cp < ncp_[fd]; cp++ )
            collAngle[cp] = get_Collimator(cpi+cp);

        float maxSpd = machine_->getCollimator()->getMaxSpeed();
        float minSpd = machine_->getCollimator()->getMinSpeed();

        float diffNxt = abs( collAngle[1] - collAngle[0] );
        minWeight[0] = max( minWeight[0], ( diffNxt / 2.0f ) / maxSpd * doseRateMin[0] );
        //maxWeight[0] = min( maxWeight[0], ( diffNxt / 2.0f ) / minSpd * doseRateMax[0] );
        float diffPrv = diffNxt;
        for ( unsigned int cp = 1; cp < (ncp_[fd]-1); cp++ ) {
            diffNxt = abs( collAngle[cp+1] - collAngle[cp] );
            float diffTot = diffNxt / 2.0f + diffPrv / 2.0f;

            minWeight[cp] = max( minWeight[cp], diffTot / maxSpd * doseRateMin[cp] );
            if ( collAngle[cp+1] != collAngle[0] && collAngle[cp] != collAngle[ncp_[fd]-1] ) {
                maxWeight[cp] = min( maxWeight[cp], diffTot / minSpd * doseRateMax[cp] );
            }
            diffPrv = diffNxt;
        }
        minWeight[ncp_[fd]-1] = max( minWeight[ncp_[fd]-1], ( diffPrv / 2.0f ) / maxSpd * doseRateMin[ncp_[fd]-1] );
        //maxWeight[ncp_[fd]-1] = min( maxWeight[ncp_[fd]-1], ( diffPrv / 2.0f ) / minSpd * doseRateMax[ncp_[fd]-1] );
    }

    // Couch angle constraint
    if ( ! isConstantCouchInFd_[fd] ) {
        vector<float> couchAngle(ncp_[fd], 0.0f);
        for ( unsigned int cp = 0; cp < ncp_[fd]; cp++ )
            couchAngle[cp] = get_Couch(cpi+cp);

        float maxSpd = machine_->getCouch()->getMaxSpeed();
        float minSpd = machine_->getCouch()->getMinSpeed();

        float diffNxt = abs( couchAngle[1] - couchAngle[0] );
        minWeight[0] = max( minWeight[0], ( diffNxt / 2.0f ) / maxSpd * doseRateMin[0] );
        //maxWeight[0] = min( maxWeight[0], ( diffNxt / 2.0f ) / minSpd * doseRateMax[0] );
        float diffPrv = diffNxt;
        for ( unsigned int cp = 1; cp < (ncp_[fd]-1); cp++ ) {
            diffNxt = abs( couchAngle[cp+1] - couchAngle[cp] );
            float diffTot = diffNxt / 2.0f + diffPrv / 2.0f;

            minWeight[cp] = max( minWeight[cp], diffTot / maxSpd * doseRateMin[cp] );
            if ( couchAngle[cp+1] != couchAngle[0] && couchAngle[cp] != couchAngle[ncp_[fd]-1] ) {
                maxWeight[cp] = min( maxWeight[cp], diffTot / minSpd * doseRateMax[cp] );
            }
            diffPrv = diffNxt;
        }
        minWeight[ncp_[fd]-1] = max( minWeight[ncp_[fd]-1], ( diffPrv / 2.0f ) / maxSpd * doseRateMin[ncp_[fd]-1] );
        //maxWeight[ncp_[fd]-1] = min( maxWeight[ncp_[fd]-1], ( diffPrv / 2.0f ) / minSpd * doseRateMax[ncp_[fd]-1] );
    }

    vector<vector<float> > weightLim(2,vector<float>(ncp_[fd],0.0f));
    for ( unsigned int cp = 0; cp < ncp_[fd]; cp++ ) {
        if ( doseRateMin[cp] > 0 ) weightLim[0][cp] = minWeight[cp];
        weightLim[1][cp] = maxWeight[cp];
    }

    return weightLim;
}


/**
 * Find out if the x jaw parameters are physically imposible to deliver.
 * Check against the minimum gap and maximum motion constraints.
 *
 * @returns true if the parameter vector is impossible to deliver.
 */
bool DirectParameterVector::is_impossible_XJaw( unsigned int fd, std::vector<float> cpTime ) const {

    unsigned int ip0, ip1;
    unsigned int cpi = get_absCPindex( fd, 0 );

    // Minimum and maximum positions
    for ( unsigned int cp = 0; cp < ncp_[fd]; cp++ ) {
        if ( nXJawParams_[cpi+cp] > 0 ) {
            ip0 = get_PInd(cpi+cp, XJaw);

            float cpCollAngle = get_Collimator(cpi+cp) - get_dijCollimatorAngle(ip0);
            float cosHT = cos( cpCollAngle * PI / 180.0f );
            float sinHT = sin( cpCollAngle * PI / 180.0f );
            float minX = get_minXAx( ip0 ) * cosHT - get_minYAx( ip0 ) * sinHT;
            float maxX = get_maxXAx( ip0 ) * cosHT - get_maxYAx( ip0 ) * sinHT;

            if ( get_value(ip0) < get_absLowBound(ip0) ) {
                if ( DEBUG_MESSAGES ) {
                    cout << "Param " << ip0 << " Jaw 0 at cp " << cp << " value " << get_value(ip0)
                         << " is too low [" << minX << "," << get_absLowBound(ip0)
                         << "," << get_lowBound(ip0) << "]" << " : ";
                }
                return true;
            }
            if ( get_value(ip0) > get_absHighBound(ip0) ) {
                if ( DEBUG_MESSAGES ) {
                    cout << "Param " << ip0 << " Jaw 0 at cp " << cp << " value " << get_value(ip0)
                         << " is too high [" << maxX << "," << get_absHighBound(ip0)
                         << "," << get_highBound(ip0) << "]" << " : " << get_maxXAx( ip0 )
                         << " : " << get_maxYAx( ip0 ) << " : ";
                }
                return true;
            }
            if ( get_value(ip0+1) < get_absLowBound(ip0+1) ) {
                if ( DEBUG_MESSAGES ) {
                    cout << "Param " << ip0+1 << " Jaw 1 at cp " << cp << " value " << get_value(ip0+1)
                         << " is too low [" << minX << "," << get_absLowBound(ip0+1)
                         << "," << get_lowBound(ip0+1) << "]" << " : ";
                }
                return true;
            }
            if ( get_value(ip0+1) > get_absHighBound(ip0+1) ) {
                if ( DEBUG_MESSAGES ) {
                    cout << "Param " << ip0+1 << " Jaw 1 at cp " << cp << " value " << get_value(ip0+1)
                         << " is too high [" << maxX << "," << get_absHighBound(ip0+1)
                         << "," << get_highBound(ip0+1) << "]" << " : " << get_maxXAx( ip0 )
                         << " : " << get_maxYAx( ip0 ) << " : ";
                }
                return true;
            }

            // Parameter outside fluence map is now a warning instead of returning true
            /*if ( get_value(ip0+1) > maxX || get_value(ip0+1) < minX
                    || get_value(ip0) > maxX || get_value(ip0) < minX ) {
                cerr << "WARNING Dij/Bixel matrix is too small at control point " << cp
                     << " dij index " << get_beamNo(ip0) << endl;
                //return true;
            }*/
        }
    }

    // Minimum gap constraint between opposing jaws
    float minGap = machine_->getXJaw(0)->getMinGapOpposing();
    for ( unsigned int cp = 0; cp < ncp_[fd]; cp++ ) {
        if ( nXJawParams_[cpi+cp] > 0 ) {
            ip0 = get_PInd(cpi+cp, XJaw);
            ip1 = ip0+1;
            if ( get_value(ip1) - get_value(ip0) < minGap ) {
                if ( DEBUG_MESSAGES )
                    cout << "Jaw Gap at cp " << cp << " value0 " << get_value(ip0)
                        << " value1 " << get_value(ip1) << " : ";
                return true;
            }
        }
    }

    // Motion constraint
    ip0 = get_PInd(cpi, XJaw);
    if ( get_canChangeBetweenCP(ip0) ) {
        float velLimt = get_maxSpeed( get_PInd(0, XJaw) );
        float distAccFactor = velLimt * velLimt /
                ( get_maxAcceleration( get_PInd(0, XJaw) ) * get_maxAcceleration( get_PInd(0, XJaw) ));

        for ( unsigned int cp = 1; cp < ncp_[fd]; cp++ ) {
            if ( nXJawParams_[cpi+cp] > 0 ) {
				float distlimit = velLimt * cpTime[cp] - distAccFactor;
                ip1 = get_PInd(cpi+cp, XJaw);
                if ( std::abs(get_value(ip1) - get_value(ip0)) > distlimit ) {
                    if ( DEBUG_MESSAGES )
                        cout << "Jaw excessive motion. Value at cp " << cp << " = " << get_value(ip0)
                            << ". Value at cp " << cp-1 << " = " << get_value(ip1) << " : ";
                    return true;
                }
                ip0 = ip1;
            }
        }
    } else {
        for ( unsigned int cp = 1; cp < ncp_[fd]; cp++ ) {
            if ( nXJawParams_[cpi+cp] > 0 ) {
                ip1 = get_PInd(cpi+cp, XJaw);
                if ( std::abs( get_value(ip1) - get_value(ip0) )
                                        > get_step_size(ip0) * ZERO_DIFF_THRESHOLD ) {
                    if ( DEBUG_MESSAGES )
                        cout << "Jaw motion forbidden. Value at cp " << cp << " = " << get_value(ip0)
                            << ". Value at cp " << cp-1 << " = " << get_value(ip1) << " : ";
                    return true;
                }
            }
        }
    }

    // If we made it this far, no entries are impossible
    return false;
}


/**
 * Find out if the y jaw parameters are physically imposible to deliver.
 * Check against the minimum gap and maximum motion constraints.
 *
 * @returns true if the parameter vector is impossible to deliver.
 */
bool DirectParameterVector::is_impossible_YJaw(unsigned int fd, std::vector<float> cpTime) const {

    unsigned int ip0, ip1;
    unsigned int cpi = get_absCPindex( fd, 0 );

    // Minimum and maximum positions
    for ( unsigned int cp = 0; cp < ncp_[fd]; cp++ ) {
        if ( nYJawParams_[cpi+cp] > 0 ) {
            ip0 = get_PInd(cpi+cp, YJaw);
            float cpCollAngle = get_Collimator(cpi+cp) - get_dijCollimatorAngle(ip0);
            float cosHT = cos( cpCollAngle * PI / 180.0f );
            float sinHT = sin( cpCollAngle * PI / 180.0f );
            float minY = get_minXAx( ip0 ) * sinHT + get_minYAx( ip0 ) * cosHT;
            float maxY = get_maxXAx( ip0 ) * sinHT + get_maxYAx( ip0 ) * cosHT;
            if ( get_value(ip0) < get_absLowBound(ip0) ) {
                if ( DEBUG_MESSAGES )
                    cout << "Param " << ip0 << " Jaw 0 at cp " << cp << " value " << get_value(ip0)
                         << " is too low [" << minY << "," << get_absLowBound(ip0)
                         << "," << get_lowBound(ip0) << "]" << " : ";
                return true;
            }
            if ( get_value(ip0) > get_absHighBound(ip0) ) {
                if ( DEBUG_MESSAGES )
                    cout << "Param " << ip0 << " Jaw 0 at cp " << cp << " value " << get_value(ip0)
                         << " is too high [" << maxY << "," << get_absHighBound(ip0)
                         << "," << get_highBound(ip0) << "]" << " : ";
                return true;
            }
            if ( get_value(ip0+1) < get_absLowBound(ip0+1) ) {
                if ( DEBUG_MESSAGES )
                    cout << "Param " << ip0+1 << " Jaw 0 at cp " << cp << " value " << get_value(ip0+1)
                         << " is too low [" << minY << "," << get_absLowBound(ip0+1)
                         << "," << get_lowBound(ip0+1) << "]" << " : ";
                return true;
            }
            if ( get_value(ip0+1) > get_absHighBound(ip0+1) ) {
                if ( DEBUG_MESSAGES )
                    cout << "Param " << ip0+1 << " Jaw 0 at cp " << cp << " value " << get_value(ip0+1)
                         << " is too high [" << maxY << "," << get_absHighBound(ip0+1)
                         << "," << get_highBound(ip0+1) << "]" << " : ";
                return true;
            }

            // Parameter outside fluence map is now a warning instead of returning true
            /*if ( get_value(ip0+1) > maxY || get_value(ip0+1) < minY
                    || get_value(ip0) > maxY || get_value(ip0) < minY ) {
                cerr << "WARNING Dij/Bixel matrix is too small at control point " << cp
                     << " dij index " << get_beamNo(ip0) << endl;
                //return true;
            }*/
        }
    }

    // Minimum gap constraint between opposing jaws
    float minGap = machine_->getYJaw(0)->getMinGapOpposing();
    for ( unsigned int cp = 0; cp < ncp_[fd]; cp++ ) {
        if ( nYJawParams_[cpi+cp] > 0 ) {
            ip0 = get_PInd(cpi+cp, YJaw);
            ip1 = ip0+1;
            if ( get_value(ip1) - get_value(ip0) < minGap ) {
                if ( DEBUG_MESSAGES ) cout << "Jaw Gap at cp " << cp << " value0 " << get_value(ip0) << " value1 " << get_value(ip1) << " : ";
                return true;
            }
        }
    }

    // Motion constraint
    ip0 = get_PInd(cpi, YJaw);
    if ( get_canChangeBetweenCP(ip0) ) {
        float velLimt = get_maxSpeed( get_PInd(0, YJaw) );
        float distAccFactor = velLimt * velLimt /
                ( get_maxAcceleration( get_PInd(0, YJaw) ) * get_maxAcceleration( get_PInd(0, YJaw) ));

        for ( unsigned int cp = 1; cp < ncp_[fd]; cp++ ) {
            if (nYJawParams_[cpi+cp] > 0 ) {
				float distlimit = velLimt * cpTime[cp] - distAccFactor;
                ip1 = get_PInd(cpi+cp, YJaw);
                if ( std::abs(get_value(ip1) - get_value(ip0)) > distlimit ) {
                    if ( DEBUG_MESSAGES ) cout << "Jaw excessive motion. Value at cp " << cp << " = " << get_value(ip0) << ". Value at cp " << cp-1 << " = " << get_value(ip1) << " : ";
                    return true;
                }
                ip0 = ip1;
            }
        }
    } else {
        for ( unsigned int cp = 1; cp < ncp_[fd]; cp++ ) {
            if ( nYJawParams_[cpi+cp] > 0 ) {
                ip1 = get_PInd(cpi+cp, YJaw);
                if ( std::abs( get_value(ip1) - get_value(ip0) )
                                        > get_step_size(ip0) * ZERO_DIFF_THRESHOLD ) {
                    if ( DEBUG_MESSAGES ) cout << "Jaw motion forbidden. Value at cp " << cp << " = " << get_value(ip0) << ". Value at cp " << cp-1 << " = " << get_value(ip1) << " : ";
                    return true;
                }
            }
        }
    }

    // If we made it this far, no entries are impossible
    return false;
}


/**
 * Find out if the x mlc parameters are physically imposible to deliver.
 * Check against the constraints on :
 *      the minimum gap between opposing leaves in the same track;
 *      the minimum gap between opposing leaves in adjacent tracks;
 *      the maximum extension or maximum range of minimum leaf position to maximum leaf position in a bank, (i.e. leaf carriage constraints)
 *      and the maximum motion between control points.
 *
 * @returns true if the parameter vector is impossible to deliver.
 */
bool DirectParameterVector::is_impossible_XMlc(unsigned int fd, std::vector<float> cpTime) const {

    unsigned int ip0, ip1;
    unsigned int cpi = get_absCPindex( fd, 0 );
    int nLeaves = ( nXLeafParams_[cpi] / 2 );

    // Minimum and maximum positions
    for ( unsigned int cp = 0; cp < ncp_[fd]; cp++ ) {
        if ( nXLeafParams_[cpi+cp] > 0 ) {
            ip0 = get_PInd(cpi+cp, XLeaf);
            float cpCollAngle = get_Collimator(cpi+cp) - get_dijCollimatorAngle(ip0);
            float cosHT = cos( cpCollAngle * PI / 180.0f );
            float sinHT = sin( cpCollAngle * PI / 180.0f );
            float minX = get_minXAx( ip0 ) * cosHT - get_minYAx( ip0 ) * sinHT;
            float maxX = get_maxXAx( ip0 ) * cosHT - get_maxYAx( ip0 ) * sinHT;
            for ( unsigned int lf=0; lf<nXLeafParams_[cpi+cp]; lf++ ) {
                if ( get_value(ip0+lf) < get_absLowBound(ip0+lf) ) {
                    if ( DEBUG_MESSAGES ) cout << "Leaf Pair " << lf << " : " << get_value(ip0+lf) << " [ " << get_absLowBound(ip0+lf) << " , " << get_absHighBound(ip0+lf) << " ] or [ " << minX << " , " << maxX << " ]" << " : ";
                    return true;
                }
                if ( get_value(ip0+lf) > get_absHighBound(ip0+lf) ) {
                    if ( DEBUG_MESSAGES ) cout << "Leaf Pair " << lf << " : " << get_value(ip0+lf) << " [ " << get_absLowBound(ip0+lf) << " , " << get_absHighBound(ip0+lf) << " ] or [ " << minX << " , " << maxX << " ]"  << " : ";
                    return true;
                }

                // Parameter outside fluence map is now a warning instead of returning true
                /*if ( get_value(ip0+lf) > maxX || get_value(ip0+lf) < minX ) {
                    cerr << "WARNING Dij/Bixel matrix is too small at control point " << cp
                         << " dij index " << get_beamNo(ip0) << endl;
                    //return true;
                }*/
            }
        }
    }

    // Minimum gap constraint between opposing leaves
    std::vector<float> minGap ( nLeaves, 0.0f );
    for ( int lf=0; lf<nLeaves; lf++ ) {
        minGap[lf] = machine_->getXLeafMinGapOpposing(lf);
    }

    for ( unsigned int cp = 0; cp < ncp_[fd]; cp++ ) {
        if ( nXLeafParams_[cpi+cp] > 0 ) {
            ip0 = get_PInd(cpi+cp, XLeaf);
            ip1 = ip0+nLeaves;
            for ( int lf=0; lf<nLeaves; lf++ ) {
                if ( get_value(ip1+lf) - get_value(ip0+lf) < minGap[lf] ) {
                    if ( DEBUG_MESSAGES ) cout << "Leaf Pair " << lf << " : " << get_value(ip0+lf) << " , " << get_value(ip1+lf) << " : ";
                    return true;
                }
            }
        }
    }

    // Interdigitation constraints
    for ( int lf=0; lf<(nLeaves-1); lf++ ) {
        minGap[lf] = machine_->getXLeafMinGapAdjacent(lf);
    }
    for ( unsigned int cp = 0; cp < ncp_[fd]; cp++ ) {
        if ( nXLeafParams_[cpi+cp] > 0 ) {
            ip0 = get_PInd(cpi+cp, XLeaf);
            ip1 = ip0+nLeaves;
            for ( int lf=0; lf<(nLeaves-1); lf++ ) {
                if ( ! machine_->xLeafCanInterdigitate( lf ) ) {
                    if ( get_value(ip1+lf+1) - get_value(ip0+lf) < minGap[lf] ) {
                        if ( DEBUG_MESSAGES ) cout << "Leaf " << lf << " = " << get_value(ip0+lf) << " Leaf " << lf+1+nLeaves << " = " << get_value(ip1+lf+1)
                            << " Gap = " << get_value(ip1+lf+1) - get_value(ip0+lf) << " MinGap = " << minGap[lf] << " : ";
                        return true;
                    }
                }
            }
        }
    }
    for ( int lf=0; lf<(nLeaves-1); lf++ ) {
        minGap[lf] = machine_->getXLeafMinGapAdjacent(lf+nLeaves);
    }
    for ( unsigned int cp = 0; cp < ncp_[fd]; cp++ ) {
        if ( nXLeafParams_[cpi+cp] > 0 ) {
            ip0 = get_PInd(cpi+cp, XLeaf);
            ip1 = ip0+nLeaves;
            for ( int lf=0; lf<(nLeaves-1); lf++ ) {
                if ( ! machine_->xLeafCanInterdigitate( lf ) ) {
                    if ( get_value(ip1+lf) - get_value(ip0+lf+1) < minGap[lf] ) {
                        if ( DEBUG_MESSAGES ) cout << "Leaf " << lf << " = " << get_value(ip0+lf+1) << " Leaf " << lf+nLeaves << " = " << get_value(ip1+lf)
                            << " Gap = " << get_value(ip1+lf) - get_value(ip0+lf+1) << " MinGap = " << minGap[lf] << " : ";
                        return true;
                    }
                }
            }
        }
    }

    // Leaf Carriage Constraints
    // Evaluate minimum of first bank and maximum of second bank
    float minLeaf = numeric_limits<float>::infinity();
    for ( int lf=0; lf<nLeaves; lf++ ) {
        for ( unsigned int cp = 0; cp < ncp_[fd]; cp++ ) {
            minLeaf = std::min( minLeaf, get_value( get_PInd(cpi+cp, XLeaf,lf) ) );
        }
    }
    float maxLeaf = -numeric_limits<float>::infinity();
    for ( int lf=0; lf<nLeaves; lf++ ) {
        for ( unsigned int cp = 0; cp < ncp_[fd]; cp++ ) {
            maxLeaf = std::max( maxLeaf, get_value( get_PInd(cpi+cp, XLeaf, lf+nLeaves ) ) );
        }
    }

    for ( int lf=0; lf<nLeaves; lf++ ) {
        if ( ! machine_->getXLeafLength(lf) ) {
            float maxLen = machine_->getXLeafMinGapAdjacent(lf);
            for ( unsigned int cp = 0; cp < ncp_[fd]; cp++ ) {
                if ( nXLeafParams_[cpi+cp] > 0 ) {
                    ip0 = get_PInd(cpi+cp, XLeaf);
                    ip1 = ip0 + nLeaves;
                    if ( get_value(ip0+lf) - minLeaf > maxLen ) {
                        if ( DEBUG_MESSAGES ) cout << "Leaf " << lf << " : " << get_value(ip0+lf) << " , Min Leaf = " << minLeaf
                            << " , Diff = " << get_value(ip0+lf) - minLeaf << " , Max Diff = " << maxLen << " : ";
                        return true;
                    }
                    if ( maxLeaf - get_value(ip1+lf) > maxLen ) {
                        if ( DEBUG_MESSAGES ) cout << "Leaf " << lf << " : " << get_value(ip1+lf) << " , Max Leaf = " << maxLeaf
                            << " , Diff = " << maxLeaf - get_value(ip1+lf) << " , Max Diff = " << maxLen << " : ";
                        return true;
                    }
                }
            }
        }
    }

    // Motion constraints
    ip0 = get_PInd(cpi, XLeaf);
    std::vector<float> velLimt (nLeaves, 0.0f);
    std::vector<float> distAccFactor (nLeaves, 0.0f);
    for ( int lf=0; lf<nLeaves; lf++ ) {
        velLimt[lf] = get_maxSpeed( ip0 + lf );
        distAccFactor[lf] = velLimt[lf] * velLimt[lf] / ( get_maxAcceleration( ip0 + lf ) * get_maxAcceleration( ip0 + lf ));
    }

    if ( get_canChangeBetweenCP(ip0) ) {
        for ( int lf=0; lf<nLeaves; lf++ ) {
            ip0 = get_PInd(cpi, XLeaf) + lf;
            for ( unsigned int cp = 1; cp < ncp_[fd]; cp++ ) {
                if ( nXLeafParams_[cpi+cp] > 0 ) {
					float distlimit = velLimt[lf] * cpTime[cp] - distAccFactor[lf];
					ip1 = get_PInd(cpi+cp, XLeaf) + lf;
                    if ( std::abs(get_value(ip1) - get_value(ip0)) > distlimit ) {
                        if ( DEBUG_MESSAGES ) cout << "Leaf " << lf << " : " << get_value(ip0) << " , " << cp << " -> "<< get_value(ip1) << " limit = " << distlimit << " : ";
                        return true;
                    }
                    ip0 = ip1;
                }
            }
        }
    }

    // If we made it this far, no entries are impossible
    return false;
}


/**
 * Find out if the y mlc parameters are physically imposible to deliver.
 * Check against the constraints on :
 *      the minimum gap between opposing leaves in the same track;
 *      the minimum gap between opposing leaves in adjacent tracks;
 *      the maximum extension or maximum range of minimum leaf position to maximum leaf position in a bank, (i.e. leaf carriage constraints)
 *      and the maximum motion between control points.
 *
 * @returns true if the parameter vector is impossible to deliver.
 */
bool DirectParameterVector::is_impossible_YMlc(unsigned int fd, std::vector<float> cpTime) const {

    unsigned int ip0, ip1;
    unsigned int cpi = get_absCPindex( fd, 0 );
    int nLeaves = ( nYLeafParams_[cpi] / 2 );

    // Minimum and maximum positions
    for ( unsigned int cp = 0; cp < ncp_[fd]; cp++ ) {
        if ( nYLeafParams_[cpi+cp] > 0 ) {
            ip0 = get_PInd(cpi+cp, YLeaf);
            float cpCollAngle = get_Collimator(cpi+cp) - get_dijCollimatorAngle(ip0);
            float cosHT = cos( cpCollAngle * PI / 180.0f );
            float sinHT = sin( cpCollAngle * PI / 180.0f );
            float minY = get_minXAx( ip0 ) * sinHT + get_minYAx( ip0 ) * cosHT;
            float maxY = get_maxXAx( ip0 ) * sinHT + get_maxYAx( ip0 ) * cosHT;
            for ( unsigned int lf=0; lf<nYLeafParams_[cpi+cp]; lf++ ) {
                if ( get_value(ip0+lf) < get_absLowBound(ip0+lf) ) {
                    if ( DEBUG_MESSAGES ) cout << "Leaf Pair " << lf << " : " << get_value(ip0+lf) << " [ " << get_absLowBound(ip0+lf) << " , " << get_absHighBound(ip0+lf) << " ] or [ " << minY << " , " << maxY << " ]"  << " : ";
                    return true;
                }
                if ( get_value(ip0+lf) > get_absHighBound(ip0+lf) ) {
                    if ( DEBUG_MESSAGES ) cout << "Leaf Pair " << lf << " : " << get_value(ip0+lf) << " [ " << get_absLowBound(ip0+lf) << " , " << get_absHighBound(ip0+lf) << " ] or [ " << minY << " , " << maxY << " ]" << " : ";
                    return true;
                }
                // Parameter outside fluence map is now a warning instead of returning true
                /*if ( get_value(ip0+lf) > maxY || get_value(ip0+lf) < minY ) {
                    cerr << "WARNING Dij/Bixel matrix is too small at control point " << cp
                         << " dij index " << get_beamNo(ip0) << endl;
                    //return true;
                }*/
            }
        }
    }

    // Minimum gap constraint between opposing leaves
    for ( int lf=0; lf<nLeaves; lf++ ) {
        float minGap = machine_->getYLeafMinGapOpposing(lf);
        for ( unsigned int cp = 0; cp < ncp_[fd]; cp++ ) {
            if ( nYLeafParams_[cpi+cp] > 0 ) {
                ip0 = get_PInd(cpi+cp, YLeaf);
                ip1 = ip0+nLeaves;
                if ( get_value(ip1+lf) - get_value(ip0+lf) < minGap ) {
                    if ( DEBUG_MESSAGES ) cout << "Leaf Pair " << lf << " : " << get_value(ip0+lf) << " , " << get_value(ip1+lf) << " : ";
                    return true;
                }
            }
        }
    }

    // Interdigitation constraints
    for ( int lf=0; lf<(nLeaves-1); lf++ ) {
        if ( ! machine_->yLeafCanInterdigitate( lf ) ) {
            float minGap = machine_->getYLeafMinGapAdjacent(lf);
            for ( unsigned int cp = 0; cp < ncp_[fd]; cp++ ) {
                ip0 = get_PInd(cpi+cp, YLeaf);
                ip1 = ip0+nLeaves;
                if ( get_value(ip1+lf+1) - get_value(ip0+lf) < minGap ) {
                    return true;
                }
            }
            minGap = machine_->getYLeafMinGapAdjacent(lf+nLeaves);
            for ( unsigned int cp = 0; cp < ncp_[fd]; cp++ ) {
                ip0 = get_PInd(cpi+cp, YLeaf);
                ip1 = ip0+nLeaves;
                if ( get_value(ip1+lf) - get_value(ip0+lf+1) < minGap ) {
                    return true;
                }
            }
        }
    }

    // Leaf Carriage Constraints
    // Evaluate minimum of first bank and maximum of second bank
    float minLeaf = numeric_limits<float>::infinity();
    for ( int lf=0; lf<nLeaves; lf++ ) {
        for ( unsigned int cp = 0; cp < ncp_[fd]; cp++ ) {
            minLeaf = std::min( minLeaf, get_value( get_PInd(cpi+cp, YLeaf, lf) ) );
        }
    }
    float maxLeaf = -(numeric_limits<float>::infinity());
    for ( int lf=0; lf<nLeaves; lf++ ) {
        for ( unsigned int cp = 0; cp < ncp_[fd]; cp++ ) {
            maxLeaf = std::max( maxLeaf, get_value( get_PInd(cpi+cp, YLeaf, lf+nLeaves) ) );
        }
    }

    for ( int lf=0; lf<nLeaves; lf++ ) {
        if ( ! machine_->getYLeafLength(lf) ) {
            float maxLen = machine_->getYLeafMinGapAdjacent(lf);
            for ( unsigned int cp = 0; cp < ncp_[fd]; cp++ ) {
                ip0 = get_PInd(cpi+cp, YLeaf);
                ip1 = ip0 + nLeaves;
                if ( get_value(ip0+lf) - minLeaf > maxLen ) {
                    return true;
                }
                if ( maxLeaf - get_value(ip1+lf) > maxLen ) {
                    return true;
                }
            }
        }
    }

    // Motion constraints
    if ( get_canChangeBetweenCP(ip0) ) {
        for ( int lf=0; lf<nLeaves; lf++ ) {
            ip0 = get_PInd(cpi, YLeaf, lf);
            float velLimt = get_maxSpeed( ip0 );
            float distAccFactor = velLimt * velLimt / ( get_maxAcceleration( ip0 ) * get_maxAcceleration( ip0 ));

            for ( unsigned int cp = 1; cp < ncp_[fd]; cp++ ) {
				float distlimit = velLimt * cpTime[cp] - distAccFactor;
				ip1 = get_PInd(cpi+cp, YLeaf, lf);
                if ( std::abs(get_value(ip1) - get_value(ip0)) > distlimit ) {
                    return true;
                }
                ip0 = ip1;
            }
        }
    }

    // If we made it this far, no entries are impossible
    return false;
}


/**
 * Reset the current parameter limits.
 * Assumes that current set is deliverable otherwise throws error.
 *
 * Currently only strictly handles leaf and jaw bounds other bounds are assumed to be unchanged during optimization.
 * This should be changed as other parameters are added to the optimization.
 */
void DirectParameterVector::resetLimits() {

    /*cout << endl << "old params = [ ";
    for ( unsigned int pp=0; pp<nParams_; pp++ )
        cout << get_value(pp) << ", ";
    cout << "\b\b ]" << endl << "old lowBound = [ ";
    for ( unsigned int pp=0; pp<nParams_; pp++ )
        cout << get_lowBound(pp) << ", ";
    cout << "\b\b ]" << endl << "new highBound = [ ";
    for ( unsigned int pp=0; pp<nParams_; pp++ )
        cout << get_highBound(pp) << ", ";
    cout << "\b\b ]" << endl;*/

    /// Run component deliverability tests without testing actual parameter limits
    bool parameterSetIsImpossible = false;
    int cpi=0;
    for ( unsigned int fd = 0; fd < nf_; fd++ ) {
        if ( isArc(fd) )
            if ( is_impossible_ArcWeights(fd) ) {
                cerr << "Problem with control point weights : ";
                parameterSetIsImpossible=true;
                break;
            }

        std::vector<float> cpTime ( ncp_[fd], 0 );
        for ( unsigned int cp = 0; cp < ncp_[fd]; cp++ ) {
            if ( nDoseRateParams_[cpi+cp] > 0 ) {
                cpTime[cp] = doseNormFactor_ * get_Weight(cpi+cp) / get_DoseRate(cpi+cp);
            } else {
                if ( machine_->getDoseRate()->getMinValue() > 0 ) {
                    cpTime[cp] = doseNormFactor_ * get_Weight(cpi+cp) / machine_->getDoseRate()->getMinValue();
                } else {
                    cpTime[cp] = cp * ( numeric_limits<float>::max() / ncp_[fd] );
                }
            }
        }

        if ( nXJawParams_[cpi] > 0 ) {
            if ( is_impossible_XJaw(fd, cpTime) ) {
                cerr << "Problem with X Jaws : ";
                parameterSetIsImpossible=true;
                break;
            }
        }

        if ( nYJawParams_[cpi] > 0 ) {
            if ( is_impossible_YJaw(fd, cpTime) ) {
                cerr << "Problem with Y Jaws : ";
                parameterSetIsImpossible=true;
                break;
            }
        }

        if ( nXLeafParams_[cpi] > 0 ) {
            if ( is_impossible_XMlc(fd, cpTime) ) {
                cerr << "Problem with X MLC : ";
                parameterSetIsImpossible=true;
                break;
            }
        }

        if ( nYLeafParams_[cpi] > 0 ) {
            if ( is_impossible_YMlc(fd, cpTime) ) {
                cerr << "Problem with Y MLC : ";
                parameterSetIsImpossible=true;
                break;
            }
        }

        cpi += ncp_[fd];
    }

    if ( parameterSetIsImpossible ) {
        cerr << "Parameter set not possible to deliver" << endl;
        throw DirectParameterVector_exception();
    }

    cpi=0;
    for ( unsigned int fd = 0; fd < nf_; fd++ ) {
        if ( isArc(fd) ) {
            resetLimits_ArcWeights(fd);
        } else {
            // For standard fields set limits on weighting terms to be the same as absolute weightings.
            for ( unsigned int cp = 0; cp < ncp_[fd]; cp++ ) {
                unsigned int ip0 = get_PInd(cpi+cp, Weight);
                lowBound_[ip0] = absLowBound_[ip0];
                highBound_[ip0] = absHighBound_[ip0];
                value_[ip0] = value_[ip0] < lowBound_[ip0] ? lowBound_[ip0] : value_[ip0];
                value_[ip0] = value_[ip0] > highBound_[ip0] ? highBound_[ip0] : value_[ip0];
            }
        }

        std::vector<float> cpTime ( ncp_[fd], 0 );
        for ( unsigned int cp = 0; cp < ncp_[fd]; cp++ ) {
            // If dose rate is not set then assume that we can set the
            // dose rate to minimum positive value if required.
            if ( nDoseRateParams_[cpi+cp] > 0 ) {
                cpTime[cp] = doseNormFactor_ * get_Weight(cpi+cp) / get_DoseRate(cpi+cp);
            } else {
                if ( machine_->getDoseRate()->getMinValue() > 0 ) {
                    cpTime[cp] = doseNormFactor_ * get_Weight(cpi+cp) / machine_->getDoseRate()->getMinValue();
                } else {
                    cpTime[cp] = std::numeric_limits<float>::max() / ncp_[fd];
                }
            }
        }

        if ( nXJawParams_[cpi] > 0 )
            resetLimits_XJaw(fd, cpTime);

        if ( nYJawParams_[cpi] > 0 )
            resetLimits_YJaw(fd, cpTime);

        if ( nXLeafParams_[cpi] > 0 )
            resetLimits_XMlc(fd, cpTime);

        if ( nYLeafParams_[cpi] > 0 )
            resetLimits_YMlc(fd, cpTime);

        cpi += ncp_[fd];
    }

    /*cout << endl << "old params = [ ";
    for ( unsigned int pp=0; pp<nParams_; pp++ )
        cout << get_value(pp) << ", ";
    cout << "\b\b ]" << endl << "old lowBound = [ ";
    for ( unsigned int pp=0; pp<nParams_; pp++ )
        cout << get_lowBound(pp) << ", ";
    cout << "\b\b ]" << endl << "new highBound = [ ";
    for ( unsigned int pp=0; pp<nParams_; pp++ )
        cout << get_highBound(pp) << ", ";
    cout << "\b\b ]" << endl;*/

}

/**
 * Calculate the new upper and lower bounds on the weight parameter based on the current parameter values.
 */
void DirectParameterVector::resetLimits_ArcWeights(unsigned int fd) {

    vector<vector<float> > weightLim = findArcWeightLimits( fd );

    unsigned int cpi = get_absCPindex( fd, 0 );
    for ( unsigned int cp = 0; cp < ncp_[fd]; cp++ ) {
        unsigned int ip0 = get_PInd(cpi+cp, Weight);
        lowBound_[ip0] = max( absLowBound_[ip0], weightLim[0][cp] );
        highBound_[ip0] = min( absHighBound_[ip0], weightLim[1][cp] );

        assert( highBound_[ip0] > lowBound_[ip0] );
        value_[ip0] = value_[ip0] < lowBound_[ip0] ? lowBound_[ip0] : value_[ip0];
        value_[ip0] = value_[ip0] > highBound_[ip0] ? highBound_[ip0] : value_[ip0];
    }
}


/**
 * Calculate the new upper and lower bounds on the position of the x jaw based on the current parameter values.
 */
void DirectParameterVector::resetLimits_XJaw(unsigned int fd, vector<float> cpTime) {

    unsigned int ip0, ip1;
    unsigned int cpi = get_absCPindex( fd, 0 );

    float minGap = machine_->getXJaw(0)->getMinGapOpposing();
    for ( unsigned int cp = 0; cp < ncp_[fd]; cp++ ) {
        if ( nXJawParams_[cpi+cp] > 0 ) {
            ip0 = get_PInd(cpi+cp, XJaw);         ip1 = ip0+1;

            float cpCollAngle = get_Collimator(cpi+cp) - get_dijCollimatorAngle(ip0);
            float cosHT = cos( cpCollAngle * PI / 180.0f );
            float sinHT = sin( cpCollAngle * PI / 180.0f );
            float minX = get_minXAx( ip0 ) * cosHT - get_minYAx( ip0 ) * sinHT;
            float maxX = get_maxXAx( ip0 ) * cosHT - get_maxYAx( ip0 ) * sinHT;

            // Minimum and maximum positions
            lowBound_[ ip0 ] = std::max( get_absLowBound(ip0), minX );
            highBound_[ ip0 ] = std::min( get_absHighBound(ip0), maxX );
            lowBound_[ ip1 ] = std::max( get_absLowBound(ip1), minX );
            highBound_[ ip1 ] = std::min( get_absHighBound(ip1), maxX );

            // Set the minimum gap constraint
            highBound_[ ip0 ] = std::min( get_highBound(ip0), get_value(ip1) - minGap );
            lowBound_[ ip1 ] = std::max( get_lowBound(ip1), get_value(ip0) + minGap );
        }
    }

    // Motion constraint
    ip0 = get_PInd(cpi, XJaw);
    if ( get_canChangeBetweenCP(ip0) ) {
        float velLimt = get_maxSpeed( get_PInd(0, XJaw) );
        float distAccFactor = velLimt * velLimt /
                ( get_maxAcceleration( get_PInd(0, XJaw) ) * get_maxAcceleration( get_PInd(0, XJaw) ));

        for ( unsigned int cp = 1; cp < ncp_[fd]; cp++ ) {
            if ( nXJawParams_[cpi+cp] > 0 ) {
				float distlimit = velLimt * cpTime[cp] - distAccFactor;

                ip1 = get_PInd(cpi+cp, XJaw);
                // From one control point to next
                highBound_[ ip1 ] = std::min( get_highBound(ip1), get_value(ip0) + distlimit );
                lowBound_[ ip1 ] = std::max( get_lowBound(ip1), get_value(ip0) - distlimit );
                // From one control point to previous
                highBound_[ ip0 ] = std::min( get_highBound(ip0), get_value(ip1) + distlimit );
                lowBound_[ ip0 ] = std::max( get_lowBound(ip0), get_value(ip1) - distlimit );

                // Repeat for opposing jaw
                // From one control point to next
                highBound_[ ip1+1 ] = std::min( get_highBound(ip1+1), get_value(ip0+1) + distlimit );
                lowBound_[ ip1+1 ] = std::max( get_lowBound(ip1+1), get_value(ip0+1) - distlimit );
                // From one control point to previous
                highBound_[ ip0+1 ] = std::min( get_highBound(ip0+1), get_value(ip1+1) + distlimit );
                lowBound_[ ip0+1 ] = std::max( get_lowBound(ip0+1), get_value(ip1+1) - distlimit );

                ip0 = ip1;
            }
        }
    }

    for ( unsigned int cp = 0; cp < ncp_[fd]; cp++ ) {
        if ( nXJawParams_[cpi+cp] > 0 ) {
            ip0 = get_PInd(cpi+cp, XJaw);         ip1 = ip0+1;
            value_[ip0] = value_[ip0] < lowBound_[ip0] ? lowBound_[ip0] : value_[ip0];
            value_[ip0] = value_[ip0] > highBound_[ip0] ? highBound_[ip0] : value_[ip0];
            value_[ip1] = value_[ip1] < lowBound_[ip1] ? lowBound_[ip1] : value_[ip1];
            value_[ip1] = value_[ip1] > highBound_[ip1] ? highBound_[ip1] : value_[ip1];
        }
    }
}


/**
 * Calculate the new upper and lower bounds on the position of the y jaw based on the current parameter values.
 */
void DirectParameterVector::resetLimits_YJaw(unsigned int fd, vector<float> cpTime) {

    unsigned int ip0, ip1;
    unsigned int cpi = get_absCPindex( fd, 0 );

    float minGap = machine_->getYJaw(0)->getMinGapOpposing();
    for ( unsigned int cp = 0; cp < ncp_[fd]; cp++ ) {
        if ( nYJawParams_[cpi+cp] > 0 ) {
            ip0 = get_PInd(cpi+cp, YJaw);         ip1 = ip0+1;

            float cpCollAngle = get_Collimator(cpi+cp) - get_dijCollimatorAngle(ip0);
            float cosHT = cos( cpCollAngle * PI / 180.0f );
            float sinHT = sin( cpCollAngle * PI / 180.0f );
            float minY = get_minXAx( ip0 ) * sinHT + get_minYAx( ip0 ) * cosHT;
            float maxY = get_maxXAx( ip0 ) * sinHT + get_maxYAx( ip0 ) * cosHT;

            // Minimum and maximum positions
            lowBound_[ ip0 ] = std::max( get_absLowBound(ip0), minY );
            highBound_[ ip0 ] = std::min( get_absHighBound(ip0), maxY );
            lowBound_[ ip1 ] = std::max( get_absLowBound(ip1), minY );
            highBound_[ ip1 ] = std::min( get_absHighBound(ip1), maxY );

            // Set the minimum gap constraint
            highBound_[ ip0 ] = std::min( get_highBound(ip0), get_value(ip1) - minGap );
            lowBound_[ ip1 ] = std::max( get_lowBound(ip1), get_value(ip0) + minGap );
        }
    }

    // Motion constraint
    ip0 = get_PInd(cpi, YJaw);
    if ( get_canChangeBetweenCP(ip0) ) {
        float velLimt = get_maxSpeed( get_PInd(0, YJaw) );
        float distAccFactor = velLimt * velLimt /
                ( get_maxAcceleration( get_PInd(0, YJaw) ) * get_maxAcceleration( get_PInd(0, YJaw) ));
        if ( nYJawParams_[cpi] > 0 ) {
            for ( unsigned int cp = 1; cp < ncp_[fd]; cp++ ) {
				float distlimit = velLimt * cpTime[cp] - distAccFactor;
				ip1 = get_PInd(cpi+cp, YJaw);
                // From one control point to next
                highBound_[ ip1 ] = std::min( get_highBound(ip1), get_value(ip0) + distlimit );
                lowBound_[ ip1 ] = std::max( get_lowBound(ip1), get_value(ip0) - distlimit );
                // From one control point to previous
                highBound_[ ip0 ] = std::min( get_highBound(ip0), get_value(ip1) + distlimit );
                lowBound_[ ip0 ] = std::max( get_lowBound(ip0), get_value(ip1) - distlimit );

                // Repeat for opposing jaw
                // From one control point to next
                highBound_[ ip1+1 ] = std::min( get_highBound(ip1+1), get_value(ip0+1) + distlimit );
                lowBound_[ ip1+1 ] = std::max( get_lowBound(ip1+1), get_value(ip0+1) - distlimit );
                // From one control point to previous
                highBound_[ ip0+1 ] = std::min( get_highBound(ip0+1), get_value(ip1+1) + distlimit );
                lowBound_[ ip0+1 ] = std::max( get_lowBound(ip0+1), get_value(ip1+1) - distlimit );

                ip0 = ip1;
            }
        }
    }

    ip0 = get_PInd(cpi, YJaw);
    for ( unsigned int cp = 0; cp < ncp_[fd]; cp++ ) {
        if ( nYJawParams_[cpi+cp] > 0 ) {
            ip0 = get_PInd(cpi+cp, YJaw);         ip1 = ip0+1;
            value_[ip0] = value_[ip0] < lowBound_[ip0] ? lowBound_[ip0] : value_[ip0];
            value_[ip0] = value_[ip0] > highBound_[ip0] ? highBound_[ip0] : value_[ip0];
            value_[ip1] = value_[ip1] < lowBound_[ip1] ? lowBound_[ip1] : value_[ip1];
            value_[ip1] = value_[ip1] > highBound_[ip1] ? highBound_[ip1] : value_[ip1];
        }
    }
}


/**
 * Calculate the new upper and lower bounds on the positions of the x mlc leaves based on the current parameter values.
 */
void DirectParameterVector::resetLimits_XMlc(unsigned int fd, vector<float> cpTime) {

    unsigned int ip0, ip1;
    unsigned int cpi = get_absCPindex( fd, 0 );
    int nLeaves = ( nXLeafParams_[cpi] / 2 );

    // Minimum gap constraint between opposing leaves
    std::vector<float> minGap ( nLeaves, 0.0f );
    for ( int lf=0; lf<nLeaves; lf++ ) {
        minGap[lf] = machine_->getXLeafMinGapOpposing(lf);
    }

    for ( unsigned int cp = 0; cp < ncp_[fd]; cp++ ) {
        if ( nXLeafParams_[cpi+cp] > 0 ) {
            ip0 = get_PInd(cpi+cp, XLeaf);    ip1 = ip0+nLeaves;

            float cpCollAngle = get_Collimator(cpi+cp) - get_dijCollimatorAngle(ip0);
            float cosHT = cos( cpCollAngle * PI / 180.0f );
            float sinHT = sin( cpCollAngle * PI / 180.0f );
            float minX = get_minXAx( ip0 ) * cosHT - get_minYAx( ip0 ) * sinHT;
            float maxX = get_maxXAx( ip0 ) * cosHT - get_maxYAx( ip0 ) * sinHT;

            for ( int lf=0; lf<nLeaves; lf++ ) {
                // Minimum and maximum positions
                lowBound_[ip0+lf] = std::max( get_absLowBound(ip0+lf), minX );
                highBound_[ip0+lf] = std::min( get_absHighBound(ip0+lf), maxX );
                lowBound_[ip1+lf] = std::max( get_absLowBound(ip1+lf), minX );
                highBound_[ip1+lf] = std::min( get_absHighBound(ip1+lf), maxX );

                // Set the minimum gap constraint
                highBound_[ip0+lf] = std::min( get_highBound(ip0+lf), get_value(ip1) - minGap[lf] );
                lowBound_[ip1+lf] = std::max( get_lowBound(ip1+lf), get_value(ip0) + minGap[lf] );
            }
        }
    }

    // Motion constraint
    ip0 = get_PInd(cpi, XLeaf);
    if ( get_canChangeBetweenCP(ip0) ) {
        float velLimt = get_maxSpeed( get_PInd(0, XLeaf) );
        float distAccFactor = velLimt * velLimt /
                ( get_maxAcceleration( get_PInd(0, XLeaf) ) * get_maxAcceleration( get_PInd(0, XLeaf) ));

        for ( unsigned int cp = 1; cp < ncp_[fd]; cp++ ) {
			float distlimit = velLimt * cpTime[cp] - distAccFactor;

            ip1 = get_PInd(cpi+cp, XLeaf);
            // From one control point to next
            highBound_[ ip1 ] = std::min( get_highBound(ip1), get_value(ip0) + distlimit );
            lowBound_[ ip1 ] = std::max( get_lowBound(ip1), get_value(ip0) - distlimit );
            // From one control point to previous
            highBound_[ ip0 ] = std::min( get_highBound(ip0), get_value(ip1) + distlimit );
            lowBound_[ ip0 ] = std::max( get_lowBound(ip0), get_value(ip1) - distlimit );

            ip0 = ip1;
        }
    }

    for ( unsigned int cp = 0; cp < ncp_[fd]; cp++ ) {
        if ( nXLeafParams_[cpi+cp] > 0 ) {
            ip0 = get_PInd(cpi+cp, XLeaf);         ip1 = ip0+1;
            for ( int lf=0; lf<nLeaves*2; lf++ ) {
                value_[ip0+lf] = value_[ip0+lf] < lowBound_[ip0+lf] ? lowBound_[ip0+lf] : value_[ip0+lf];
                value_[ip0+lf] = value_[ip0+lf] > highBound_[ip0+lf] ? highBound_[ip0+lf] : value_[ip0+lf];
                value_[ip1+lf] = value_[ip1+lf] < lowBound_[ip1+lf] ? lowBound_[ip1+lf] : value_[ip1+lf];
                value_[ip1+lf] = value_[ip1+lf] > highBound_[ip1+lf] ? highBound_[ip1+lf] : value_[ip1+lf];
            }
        }
    }
}


/**
 * Calculate the new upper and lower bounds on the positions of the y mlc leaves based on the current parameter values.
 */
void DirectParameterVector::resetLimits_YMlc(unsigned int fd, vector<float> cpTime) {

    unsigned int ip0, ip1;
    unsigned int cpi = get_absCPindex( fd, 0 );
    int nLeaves = ( nYLeafParams_[cpi] / 2 );

    // Minimum gap constraint between opposing leaves
    std::vector<float> minGap ( nLeaves, 0.0f );
    for ( int lf=0; lf<nLeaves; lf++ ) {
        minGap[lf] = machine_->getYLeafMinGapOpposing(lf);
    }

    for ( unsigned int cp = 0; cp < ncp_[fd]; cp++ ) {
        if ( nYLeafParams_[cpi+cp] > 0 ) {
            ip0 = get_PInd(cpi+cp, YLeaf);    ip1 = ip0+nLeaves;

            float cpCollAngle = get_Collimator(cpi+cp) - get_dijCollimatorAngle(ip0);
            float cosHT = cos( cpCollAngle * PI / 180.0f );
            float sinHT = sin( cpCollAngle * PI / 180.0f );
            float minY = get_minXAx( ip0 ) * sinHT + get_minYAx( ip0 ) * cosHT;
            float maxY = get_maxXAx( ip0 ) * sinHT + get_maxYAx( ip0 ) * cosHT;

            for ( int lf=0; lf<nLeaves; lf++ ) {
                // Minimum and maximum positions
                lowBound_[ ip0+lf ] = std::max( get_absLowBound(ip0+lf), minY );
                highBound_[ ip0+lf ] = std::min( get_absHighBound(ip0+lf), maxY );
                lowBound_[ ip1+lf ] = std::max( get_absLowBound(ip1+lf), minY );
                highBound_[ ip1+lf ] = std::min( get_absHighBound(ip1+lf), maxY );

                // Set the minimum gap constraint
                highBound_[ ip0+lf ] = std::min( get_highBound(ip0+lf), get_value(ip1) - minGap[lf] );
                lowBound_[ ip1+lf ] = std::max( get_lowBound(ip1+lf), get_value(ip0) + minGap[lf] );
            }
        }
    }

    // Motion constraint
    ip0 = get_PInd(cpi, YLeaf);
    if ( get_canChangeBetweenCP(ip0) ) {
        float velLimt = get_maxSpeed( get_PInd(0, YLeaf) );
        float distAccFactor = velLimt * velLimt /
                ( get_maxAcceleration( get_PInd(0, YLeaf) ) * get_maxAcceleration( get_PInd(0, YLeaf) ));

        for ( unsigned int cp = 1; cp < ncp_[fd]; cp++ ) {
			float distlimit = velLimt * cpTime[cp] - distAccFactor;

            ip1 = get_PInd(cpi+cp, YLeaf);
            // From one control point to next
            highBound_[ ip1 ] = std::min( get_highBound(ip1), get_value(ip0) + distlimit );
            lowBound_[ ip1 ] = std::max( get_lowBound(ip1), get_value(ip0) - distlimit );
            // From one control point to previous
            highBound_[ ip0 ] = std::min( get_highBound(ip0), get_value(ip1) + distlimit );
            lowBound_[ ip0 ] = std::max( get_lowBound(ip0), get_value(ip1) - distlimit );

            ip0 = ip1;
        }
    }

    for ( unsigned int cp = 0; cp < ncp_[fd]; cp++ ) {
        if ( nYLeafParams_[cpi+cp] > 0 ) {
            ip0 = get_PInd(cpi+cp, YLeaf);         ip1 = ip0+1;
            for ( int lf=0; lf<nLeaves*2; lf++ ) {
                value_[ip0+lf] = value_[ip0+lf] < lowBound_[ip0+lf] ? lowBound_[ip0+lf] : value_[ip0+lf];
                value_[ip0+lf] = value_[ip0+lf] > highBound_[ip0+lf] ? highBound_[ip0+lf] : value_[ip0+lf];
                value_[ip1+lf] = value_[ip1+lf] < lowBound_[ip1+lf] ? lowBound_[ip1+lf] : value_[ip1+lf];
                value_[ip1+lf] = value_[ip1+lf] > highBound_[ip1+lf] ? highBound_[ip1+lf] : value_[ip1+lf];
            }
        }
    }
}

/**
 * populate vectors of required and optional fields in configuration file.
 */
void DirectParameterVector::populateParserVectors( vector< string > & required_attributes,
                                              vector< string > & optional_attributes,
                                              map< string, string > & default_values,
                                              vector< string > & multi_attributes,
                                              map< string, vector< string > > & required_sub_attributes,
                                              map< string, vector< string > > & optional_sub_attributes,
                                              map< string, map< string, string > > &default_sub_values ) {

    required_attributes.push_back( "dao_file_version" );

    optional_attributes.push_back( "Machine" );
    default_values[ "Machine" ] = "";
    optional_attributes.push_back( "MachineFile" );
    default_values[ "MachineFile" ] = "";
    optional_attributes.push_back( "NumFields" );
    default_values[ "NumFields" ] = "0";

    multi_attributes.push_back( "BeamEnergy" );
    optional_sub_attributes[ "BeamEnergy" ].push_back( "Optimize" );
    default_sub_values[ "BeamEnergy" ][ "Optimize" ] = "0";
    optional_sub_attributes[ "BeamEnergy" ].push_back( "ChangesBetweenCP" );
    default_sub_values[ "BeamEnergy" ][ "ChangesBetweenCP" ] = "0";

    multi_attributes.push_back( "DoseRate" );
    optional_sub_attributes[ "DoseRate" ].push_back( "Optimize" );
    default_sub_values[ "DoseRate" ][ "Optimize" ] = "0";
    optional_sub_attributes[ "DoseRate" ].push_back( "ChangesBetweenCP" );
    default_sub_values[ "DoseRate" ][ "ChangesBetweenCP" ] = "0";

    multi_attributes.push_back( "Gantry" );
    optional_sub_attributes[ "Gantry" ].push_back( "Optimize" );
    default_sub_values[ "Gantry" ][ "Optimize" ] = "0";
    optional_sub_attributes[ "Gantry" ].push_back( "ChangesBetweenCP" );
    default_sub_values[ "Gantry" ][ "ChangesBetweenCP" ] = "0";

    multi_attributes.push_back( "Collimator" );
    optional_sub_attributes[ "Collimator" ].push_back( "Optimize" );
    default_sub_values[ "Collimator" ][ "Optimize" ] = "0";
    optional_sub_attributes[ "Collimator" ].push_back( "ChangesBetweenCP" );
    default_sub_values[ "Collimator" ][ "ChangesBetweenCP" ] = "0";

    multi_attributes.push_back( "Couch" );
    optional_sub_attributes[ "Couch" ].push_back( "Optimize" );
    default_sub_values[ "Couch" ][ "Optimize" ] = "0";
    optional_sub_attributes[ "Couch" ].push_back( "ChangesBetweenCP" );
    default_sub_values[ "Couch" ][ "ChangesBetweenCP" ] = "0";

    multi_attributes.push_back( "Jaws" );
    optional_sub_attributes[ "Jaws" ].push_back( "Optimize" );
    default_sub_values[ "Jaws" ][ "Optimize" ] = "0";
    optional_sub_attributes[ "Jaws" ].push_back( "ChangesBetweenCP" );
    default_sub_values[ "Jaws" ][ "ChangesBetweenCP" ] = "0";

    multi_attributes.push_back( "MLC" );
    optional_sub_attributes[ "MLC" ].push_back( "Optimize" );
    default_sub_values[ "MLC" ][ "Optimize" ] = "0";
    optional_sub_attributes[ "MLC" ].push_back( "ChangesBetweenCP" );
    default_sub_values[ "MLC" ][ "ChangesBetweenCP" ] = "0";

    multi_attributes.push_back( "Field" );
    optional_sub_attributes[ "Field" ].push_back( "NumSegments" );
    default_sub_values[ "Field" ][ "NumSegments" ] = "0";
}


/**
 * Recalculate the lower and upper bounds for each parameter by considering the current set of parameter values
 * resetting the parameter values as necessary to ensure compliance with the delivery constraints.
 */
void DirectParameterVector::makePossible( bool largeApertures ) {

    unsigned int ip0, ip1;

    // Make weights possible
    for ( unsigned int fd = 0; fd < nf_; fd++ ) {
        makePossible_ArcWeights( fd );
    }

    // Look at each control point in turn and make sure jaw positions in each control point are deliverable.
    for ( unsigned int cp = 0; cp < ncpTot_; cp++ ) {
        if ( nXJawParams_[cp] > 0 ) {
            ip0 = get_PInd(cp, XJaw);         ip1 = ip0+1;

            float cpCollAngle = get_Collimator(cp) - get_dijCollimatorAngle(ip0);
            float cosHT = cos( cpCollAngle * PI / 180.0f );
            float sinHT = sin( cpCollAngle * PI / 180.0f );
            float minX = get_minXAx( ip0 ) * cosHT - get_minYAx( ip0 ) * sinHT;
            float maxX = get_maxXAx( ip0 ) * cosHT - get_maxYAx( ip0 ) * sinHT;

            float minXAx = get_minXAx(ip0);
            float maxXAx = get_maxXAx(ip0);

            lowBound_[ ip0 ] = std::max( get_absLowBound(ip0), minXAx );
            highBound_[ ip0 ] = std::min( get_absHighBound(ip0), maxXAx );
            lowBound_[ ip1 ] = std::max( get_absLowBound(ip1), minXAx );
            highBound_[ ip1 ] = std::min( get_absHighBound(ip1), maxXAx );

            float halfMinGap = machine_->getXJaw(0)->getMinGapOpposing() * 0.5f;
            float gapCenter = ( get_value(ip0) + get_value(ip1) ) * 0.5f;
            if ( lowBound_[ ip0 ] > gapCenter - halfMinGap ) gapCenter = lowBound_[ ip0 ] + halfMinGap;
            if ( highBound_[ ip1 ] < gapCenter + halfMinGap ) gapCenter = highBound_[ ip1 ] - halfMinGap;
            highBound_[ ip0 ] = std::min( get_highBound(ip0), gapCenter - halfMinGap );
            lowBound_[ ip1 ] = std::max( get_lowBound(ip1), gapCenter + halfMinGap );

            value_[ ip0 ] = get_RoundToValidValue(ip0, value_[ip0]);
            value_[ ip1 ] = get_RoundToValidValue(ip1, value_[ip1]);
            /*value_[ ip0 ] = std::min( get_value(ip0), get_highBound(ip0) );
            value_[ ip0 ] = std::max( get_value(ip0), get_lowBound(ip0) );
            value_[ ip1 ] = std::min( get_value(ip1), get_highBound(ip1) );
            value_[ ip1 ] = std::max( get_value(ip1), get_lowBound(ip1) );*/
        }
    }

    for ( unsigned int cp = 0; cp < ncpTot_; cp++ ) {
        if ( nYJawParams_[cp] > 0 ) {
            ip0 = get_PInd(cp, YJaw);         ip1 = ip0+1;

            float minYAx = get_minYAx(ip0);
            float maxYAx = get_maxYAx(ip0);

            lowBound_[ ip0 ] = std::max( get_absLowBound(ip0), minYAx );
            highBound_[ ip0 ] = std::min( get_absHighBound(ip0), maxYAx );
            lowBound_[ ip1 ] = std::max( get_absLowBound(ip1), minYAx );
            highBound_[ ip1 ] = std::min( get_absHighBound(ip1), maxYAx);

            float halfMinGap = machine_->getYJaw(0)->getMinGapOpposing() * 0.5f;
            float gapCenter = ( get_value(ip0) + get_value(ip1) ) * 0.5f;
            if ( lowBound_[ ip0 ] > gapCenter - halfMinGap ) gapCenter = lowBound_[ ip0 ] + halfMinGap;
            if ( highBound_[ ip1 ] < gapCenter + halfMinGap ) gapCenter = highBound_[ ip1 ] - halfMinGap;
            highBound_[ ip0 ] = std::min( get_highBound(ip0), gapCenter - halfMinGap );
            lowBound_[ ip1 ] = std::max( get_lowBound(ip1), gapCenter + halfMinGap );

            value_[ ip0 ] = get_RoundToValidValue(ip0, value_[ip0]);
            value_[ ip1 ] = get_RoundToValidValue(ip1, value_[ip1]);
            /*value_[ ip0 ] = std::min( get_value(ip0), get_highBound(ip0) );
            value_[ ip0 ] = std::max( get_value(ip0), get_lowBound(ip0) );
            value_[ ip1 ] = std::min( get_value(ip1), get_highBound(ip1) );
            value_[ ip1 ] = std::max( get_value(ip1), get_lowBound(ip1) );*/
        }
    }

    // --- MLC --- //
    // Reset parameter limits to defaults
    for ( unsigned int cp = 0; cp < ncpTot_; cp++ ) {
        if ( nXLeafParams_[cp] > 0 ) {
            int nLeaves = ( nXLeafParams_[cp] / 2 );
            ip0 = get_PInd(cp,XLeaf);
            ip1 = ip0 + nLeaves;

            float minXAx = get_minXAx(ip0);
            float maxXAx = get_maxXAx(ip0);
            for (int lf=0; lf<nLeaves; lf++) {
                lowBound_[ ip0 ] = std::max( get_absLowBound(ip0), minXAx );
                highBound_[ ip0 ] = std::min( get_absHighBound(ip0), maxXAx );
                lowBound_[ ip1 ] = std::max( get_absLowBound(ip1), minXAx );
                highBound_[ ip1 ] = std::min( get_absHighBound(ip1), maxXAx );
                ++ip0;  ++ip1;
            }
        }
        if ( nYLeafParams_[cp] > 0 ) {
            int nLeaves = (nYLeafParams_[cp] / 2);
            ip0 = get_PInd(cp, YLeaf);
            ip1 = ip0 + nLeaves;

            float minYAx = get_minYAx(ip0);
            float maxYAx = get_maxYAx(ip0);

            for (int lf=0; lf<nLeaves; lf++) {
                lowBound_[ ip0 ] = std::max( get_absLowBound(ip0), minYAx );
                highBound_[ip0 ] = std::min( get_absHighBound(ip0), maxYAx );
                lowBound_[ ip1 ] = std::max( get_absLowBound(ip1), minYAx );
                highBound_[ ip1 ] = std::min( get_absHighBound(ip1), maxYAx );
                ++ip0;  ++ip1;
            }
        }
        makePossible_minGap( cp );
        makePossible_interdigitate( cp );
   }

    // Look at the control points in combination and ensure that the parameter values do not change too much
    // between control points.
    for ( unsigned int fd = 0; fd < nf_; fd++ ) {
        makePossible_carriage( fd, !largeApertures );
    }

    vector<float> cpTime ( ncpTot_, 0.0f );
    int cpi=0;
    for ( unsigned int fd = 0; fd < nf_; fd++ ) {
        for ( unsigned int cp = 0; cp < ncp_[fd]; cp++ ) {
            // If dose rate is not set then assume that we can set the
            // dose rate to minimum positive value if required.
            if ( nDoseRateParams_[cpi+cp] > 0 ) {
                cpTime[cp] = doseNormFactor_ * get_Weight(cpi+cp) / get_DoseRate(cpi+cp);
            } else {
                if ( machine_->getDoseRate()->getMinValue() > 0 ) {
                    cpTime[cp] = doseNormFactor_ * get_Weight(cpi+cp) / machine_->getDoseRate()->getMinValue();
                } else {
                    cpTime[cp] = std::numeric_limits<float>::max() / ncp_[fd];
                }
            }
        }
    }

    if ( nXJawParams_[0] > 0 ) {
        int cpNum = 0;
        float velLimt = get_maxSpeed( get_PInd(0, XJaw) );
        float distAccFactor = velLimt * velLimt /
                ( get_maxAcceleration( get_PInd(0, XJaw) ) * get_maxAcceleration( get_PInd(0, XJaw) ));

        std::vector<unsigned int> jawParams;
        for ( unsigned int fd = 0; fd < nf_; fd++ ) {
            jawParams.assign( ncp_[fd], 0 );
            std::vector<float> allowedDist( ncp_[fd], 0.0f );
            std::copy( cpTime.begin() + cpNum, cpTime.begin() + cpNum + ncp_[fd], allowedDist.begin() );
            cpNum += ncp_[fd];
            for ( unsigned int cp = 0; cp < ncp_[fd]; cp++ ) {
                jawParams[cp] = get_PInd(get_absCPindex(fd, cp), XJaw);
                allowedDist[cp] = velLimt * allowedDist[cp] - distAccFactor;
            }
            makePossible_motion( jawParams, allowedDist, largeApertures, true, false );

            // Assume allowed distances are the same for the opposing jaw
            for ( unsigned int cp = 0; cp < ncp_[fd]; cp++ ) ++jawParams[cp];

            makePossible_motion( jawParams, allowedDist, !largeApertures, false, true );
        }
    }

    if ( nYJawParams_[0] > 0 ) {
        int cpNum = 0;
        float velLimt = get_maxSpeed( get_PInd(0, YJaw) );
        float distAccFactor = velLimt * velLimt /
                ( get_maxAcceleration( get_PInd(0, YJaw) ) * get_maxAcceleration( get_PInd(0, YJaw) ) );

        std::vector<unsigned int> jawParams;
        for ( unsigned int fd = 0; fd < nf_; fd++ ) {
            jawParams.assign( ncp_[fd], 0 );
            std::vector<float> allowedDist( ncp_[fd], 0.0f );
            std::copy( cpTime.begin() + cpNum, cpTime.begin() + cpNum + ncp_[fd], allowedDist.begin() );
            cpNum += ncp_[fd];

            for ( unsigned int cp = 0; cp < ncp_[fd]; cp++ ) {
                jawParams[cp] = get_PInd(get_absCPindex(fd, cp), YJaw);
                allowedDist[cp] = velLimt * allowedDist[cp] - distAccFactor;
            }
            makePossible_motion( jawParams, allowedDist, largeApertures, true, false );

            // Assume allowed distances are the same for the opposing jaw
            for ( unsigned int cp = 0; cp < ncp_[fd]; cp++ ) ++jawParams[cp];
            makePossible_motion( jawParams, allowedDist, !largeApertures, false, true );
        }
    }

    if ( nXLeafParams_[0] > 0 ) {
        int nLeafPairs = (nXLeafParams_[0]/2);

        std::vector<float> velLimt( nLeafPairs, 0.0f );
        std::vector<float> distAccFactor( nLeafPairs, 0.0f );
        int ip0 = get_PInd(0, XLeaf);
        for (int lf=0; lf<nLeafPairs; lf++) {
            velLimt[lf] = get_maxSpeed( ip0 );
            distAccFactor[lf] = velLimt[lf] * velLimt[lf] /
                    ( get_maxAcceleration( ip0 + lf ) * get_maxAcceleration( ip0 + lf ) );
        }
        for ( int lf=0; lf<nLeafPairs; lf++ ) {
            std::vector<unsigned int> lfParams;
            int cpNum = 0;
            for ( unsigned int fd = 0; fd < nf_; fd++ ) {
                bool leafActive = false;
                for ( unsigned int cp = 0; cp < ncp_[fd]; cp++ )
                    leafActive = (leafActive || get_isActive(get_PInd(get_absCPindex(fd, cp), XLeaf, lf)));
                if (!leafActive) continue;

                lfParams.assign( ncp_[fd], 0 );
                std::vector<float> allowedDist( ncp_[fd], 0.0f );
                std::copy( cpTime.begin() + cpNum, cpTime.begin() + cpNum + ncp_[fd], allowedDist.begin() );
                cpNum += ncp_[fd];

                for ( unsigned int cp = 0; cp < ncp_[fd]; cp++ ) {
                    lfParams[cp] = get_PInd(get_absCPindex(fd, cp), XLeaf, lf);
                    allowedDist[cp] = velLimt[lf] * allowedDist[cp] - distAccFactor[lf];
                    assert(allowedDist[cp]>0);
                }

                makePossible_motion( lfParams, allowedDist, largeApertures, true, false );
                // Assume allowed distances are the same for the opposing leaf
                for ( unsigned int cp = 0; cp < ncp_[fd]; cp++ ) lfParams[cp] += nLeafPairs;
                makePossible_motion( lfParams, allowedDist, !largeApertures, false, true );
            }
        }
    }

    if ( nYLeafParams_[0] > 0 ) {
        int nLeafPairs = (nYLeafParams_[0]/2);

        std::vector<float> velLimt( nLeafPairs, 0.0f );
        std::vector<float> distAccFactor( nLeafPairs, 0.0f );
        int ip0 = get_PInd(0, YLeaf);
        for (int lf=0; lf<nLeafPairs; lf++) {
            velLimt[lf] = get_maxSpeed( ip0 );
            distAccFactor[lf] = velLimt[lf] * velLimt[lf] /
                    ( get_maxAcceleration( ip0 + lf ) * get_maxAcceleration( ip0 + lf ) );
        }

        for ( int lf=0; lf<nLeafPairs; lf++ ) {
            int cpNum = 0;
            std::vector<unsigned int> lfParams;
            for ( unsigned int fd = 0; fd < nf_; fd++ ) {
                bool leafActive = false;
                for ( unsigned int cp = 0; cp < ncp_[fd]; cp++ )
                    leafActive = (leafActive || get_isActive(get_PInd(get_absCPindex(fd, cp), YLeaf, lf)));
                if (!leafActive) continue;

                lfParams.assign( ncp_[fd], 0 );
                std::vector<float> allowedDist( ncp_[fd], 0.0f );
                std::copy( cpTime.begin() + cpNum, cpTime.begin() + cpNum + ncp_[fd], allowedDist.begin() );
                cpNum += ncp_[fd];

                for ( unsigned int cp = 0; cp < ncp_[fd]; cp++ ) {
                    lfParams[cp] = get_PInd( get_absCPindex(fd, cp), YLeaf, lf);
                    allowedDist[cp] = velLimt[lf] * allowedDist[cp] - distAccFactor[lf];
                    assert(allowedDist[cp]>0);
                }

                makePossible_motion( lfParams, allowedDist, largeApertures, true, false );

                // Assume allowed distances are the same for the opposing leaf
                for ( unsigned int cp = 0; cp < ncp_[fd]; cp++ ) lfParams[cp] += nLeafPairs;
                makePossible_motion( lfParams, allowedDist, !largeApertures, false, true );
            }
        }
    }

    assert ( ! is_impossible() );
}
void DirectParameterVector::makePossible() {
    makePossible( true );
}


/**
 * For those parameters which change between control points in a constrained way. Transform the values such that
 * the value changes obey the motion constraints.
 *
 * @param pInds Parameter indices for points in each control point
 * @param dx    Allowed displacement between control points
 * @param prioritiseMin Prioritise points with minimum value (true-default), or prioritise maximum value points (false)
 */
void DirectParameterVector::makePossible_motion( vector<unsigned int> pInds,  vector<float> dx, bool prioritiseMin=true, bool overrideMin=false, bool overrideMax=false ) {

    // Count the number of active inactive and total points in array.
	unsigned int numPts = pInds.size();
	unsigned int numActive = 0;
    for ( std::vector<unsigned int>::iterator itPInd=pInds.begin(); itPInd<pInds.end(); itPInd++ )
        numActive += get_isActive( *itPInd ) ? 1 : 0;
	unsigned int numInActive = numPts - numActive;

    // Record points and positions
    vector<float> pos( numActive, 0 );                  // positions
    vector<float> minPos( numActive, 0 );               // positions
    vector<float> maxPos( numActive, 0 );               // positions
	vector<unsigned int> indActive( numActive, 0 );     // index of active point
    vector<float> posActive( numActive, 0 );            // position of active point
	vector<unsigned int> indInActive( numInActive, 0 ); // index of inactive point
    vector<float> posInActive( numInActive, 0 );        // position of inactive point

	unsigned int aa=0;
	unsigned int bb=0;
	unsigned int cc=0;
    for ( std::vector<unsigned int>::iterator itPInd=pInds.begin(); itPInd<pInds.end(); itPInd++ ) {
        pos[cc] = get_value( *itPInd );
        assert( !isnan(pos[cc]) );
        minPos[cc] = ( overrideMin ) ? get_absLowBound( *itPInd ) : get_lowBound( *itPInd );
        maxPos[cc] = ( overrideMax ) ? get_absHighBound( *itPInd ) : get_highBound( *itPInd );

        if ( get_isActive( *itPInd ) ) {
            indActive[aa] = itPInd-pInds.begin();
            posActive[aa] = pos[cc];
            aa++;
        } else {
            indInActive[bb] = itPInd-pInds.begin();
            posInActive[bb] = pos[cc];
            bb++;
        }
        cc++;
    }

    // Create arrays of positions sorted by magnitute for active and inactive points
    vector<float> srtPosActive ( numActive, 0 );
    copy( posActive.begin(), posActive.end(), srtPosActive.begin() );
    sort( srtPosActive.begin(), srtPosActive.end() );

    vector<float> srtPosInActive ( numInActive, 0 );
    copy( posInActive.begin(), posInActive.end(), srtPosInActive.begin() );
    sort( srtPosInActive.begin(), srtPosInActive.end() );

    // If we need to prioritise maximum points instead of minimum points then reverese sorting
    if ( !prioritiseMin ) {
        std::reverse( srtPosActive.begin(), srtPosActive.end() );
        std::reverse( srtPosInActive.begin(), srtPosInActive.end() );
    }

    // Make a list of the order in which points should be fixed.
    // start with highest active then lower active points then highest inactive, then lower inactive.
	vector< unsigned int > priority ( numPts, 0);
    vector< bool > prSet ( numActive, false );
    for ( aa=0; aa<numActive; aa++ )
        for ( cc=0; cc<numActive; cc++ )
            if ( ( !prSet[cc] ) && ( srtPosActive[aa] == posActive[cc] ) ) {
                priority[aa] = indActive[cc];
                prSet[cc] = true;
                break;
            }

    prSet.resize( numInActive );
    fill( prSet.begin(), prSet.end(), false );
    for (bb=0; bb<numInActive; bb++)
        for ( cc=0; cc<numInActive; cc++ )
            if ( (!prSet[cc]) && (srtPosInActive[bb] == posInActive[cc]) ) {
                priority[aa+bb] = indInActive[cc];
                prSet[cc] = true;
                break;
            }

    // Set vectors for working copies of position, minimum position and maximum position
    vector<float> newPos ( numActive, 0 );
    vector<float> newMinPos ( numActive, 0 );
    vector<float> newMaxPos ( numActive, 0 );

    vector<float> * localMinBkdStore;
    vector<float> * localMaxBkdStore;
    vector<float> * localMinFwdStore;
    vector<float> * localMaxFwdStore;
    std::ofstream debugfile;
    unsigned int debugPInd = 3;
    if ( WRITE_DEBUG_MOTION_FILE && pInds[0]==debugPInd ) {
        localMinBkdStore = new vector<float> ( numActive, 0 );
        localMaxBkdStore = new vector<float> ( numActive, 0 );
        localMinFwdStore = new vector<float> ( numActive, 0 );
        localMaxFwdStore = new vector<float> ( numActive, 0 );
        debugfile.open("/home/dualta/Debug_makePossibleMotion", ios::trunc|ios::out );
        cout << "Writing debug data to /home/dualta/Debug_makePossibleMotion" << endl;
        debugfile << "loopCount : curInd : prvInd : nxtInd : : newPos : ";
        for ( cc=1; cc<numPts; cc++ ) debugfile << cc << " : "; debugfile << " : dx : ";
        for ( cc=1; cc<numPts; cc++ ) debugfile << cc << " : "; debugfile << " : newMinPos : ";
        for ( cc=1; cc<numPts; cc++ ) debugfile << cc << " : "; debugfile << " : newMaxPos : ";
        for ( cc=1; cc<numPts; cc++ ) debugfile << cc << " : "; debugfile << " : localMinBkd : ";
        for ( cc=1; cc<numPts; cc++ ) debugfile << cc << " : "; debugfile << " : localMaxBkd : ";
        for ( cc=1; cc<numPts; cc++ ) debugfile << cc << " : "; debugfile << " : localMinFwd : ";
        for ( cc=1; cc<numPts; cc++ ) debugfile << cc << " : "; debugfile << " : localMaxFwd : ";
        for ( cc=1; cc<numPts; cc++ ) debugfile << cc << " : "; debugfile << " : ";
        debugfile << "\n";
    }

    // Flag indicating if parameter is fixed or not
    vector<bool> isSet (numPts, false);
	unsigned int loopCount = 0;
    bool priorityChanged;
    do {

		priorityChanged = false;
        loopCount++;
        assert( loopCount < 100  );

        // Reset the values of parameters which are unset
		for ( cc=0; cc<numPts; cc++)
            if (!isSet[cc]) {
                newMaxPos[ cc ] = maxPos[ cc ];
                newMinPos[ cc ] = minPos[ cc ];
                newPos[ cc ] = pos[ cc ];
            }

        for ( vector<unsigned int>::iterator priorityInd = priority.begin(); priorityInd < priority.end(); ++priorityInd ) {
			int curInd = *priorityInd; // Index of current high priority point

            // Find neighbouring points already fixed previously
			int prvInd;
			bool foundPrvInd = false;
			for ( prvInd=curInd-1; prvInd>=0; prvInd-- )
                if ( isSet[prvInd] ) {
                    foundPrvInd = true;
                    break;
                }

            bool foundNxtInd = false;
			int nxtInd;
			for ( nxtInd=curInd+1; nxtInd<(int)numPts; ++nxtInd )
			    if ( isSet[nxtInd] ) {
                    foundNxtInd = true;
                    break;
                }

            // If no fixed point was found then fix this point and continue to next iteration
            if ( !(foundPrvInd || foundNxtInd) ) {
                isSet[ curInd ] = true;
                continue;
            }

            if ( WRITE_DEBUG_MOTION_FILE && pInds[0]==debugPInd ) {
                debugfile << loopCount << " : " << curInd << " : " << prvInd << " : " << nxtInd << " : : ";
                for ( cc=0; cc<numPts; cc++ ) debugfile << newPos[ cc ] << " : "; debugfile << " : ";
                for ( cc=0; cc<numPts; cc++ ) debugfile << dx[ cc ] << " : "; debugfile << " : ";
                for ( cc=0; cc<numPts; cc++ ) debugfile << newMinPos[ cc ] << " : "; debugfile << " : ";
                for ( cc=0; cc<numPts; cc++ ) debugfile << newMaxPos[ cc ] << " : "; debugfile << " : ";
                for ( cc=0; cc<numPts; cc++ ) debugfile << localMinBkdStore->at(cc) << " : "; debugfile << " : ";
                for ( cc=0; cc<numPts; cc++ ) debugfile << localMaxBkdStore->at(cc) << " : "; debugfile << " : ";
                for ( cc=0; cc<numPts; cc++ ) debugfile << localMinFwdStore->at(cc) << " : "; debugfile << " : ";
                for ( cc=0; cc<numPts; cc++ ) debugfile << localMaxFwdStore->at(cc) << " : "; debugfile << " : ";
                debugfile << "\n";
            }

            // Set default limits as +/- infinity i.e. unlimited
            float localMinBkd = -numeric_limits<float>::infinity();
            float localMaxBkd = numeric_limits<float>::infinity();
            float localMinFwd = -numeric_limits<float>::infinity();
            float localMaxFwd = numeric_limits<float>::infinity();

            if ( foundPrvInd ) {
                localMinBkd = newPos[prvInd];
                localMaxBkd = newPos[prvInd];
                for ( int dd=prvInd+1; dd<=curInd; dd++ ) {
                    unsigned int ip = pInds[dd];
                    float proposedLocalMin = localMinBkd - dx[ dd - 1 ];
                    float proposedLocalMax = localMaxBkd + dx[ dd - 1 ];
                    if ( get_step_size(ip) > 0 ) {
                        // Make sure local values are valid discrete steps
                        if ( newMinPos[ dd ] > localMinBkd - dx[dd-1] ) {
                            localMinBkd = get_absLowBound( ip ) + get_step_size(ip) *
                                ceil( ( newMinPos[ dd ] - get_absLowBound(ip) ) / get_step_size(ip) - 0.5f );
                            localMinBkd += localMinBkd < newMinPos[ dd ] ? get_step_size(ip) : 0.0f;
                        } else {
                            localMinBkd = get_absLowBound( ip ) + get_step_size(ip) *
                                ceil( ( proposedLocalMin - get_absLowBound(ip) ) / get_step_size(ip) - 0.5f );
                            localMinBkd += localMinBkd < proposedLocalMin ? get_step_size(ip) : 0.0f;
                        }
                        if ( newMaxPos[ dd ] < proposedLocalMax ) {
                            localMaxBkd = get_absLowBound( ip ) + get_step_size(ip) *
                                ceil( ( newMaxPos[ dd ] - get_absLowBound(ip) ) / get_step_size(ip) - 0.5f );
                            localMaxBkd -= localMaxBkd > newMaxPos[ dd ] ? get_step_size(ip) : 0.0f;
                        } else {
                            localMaxBkd = get_absLowBound( ip ) + get_step_size(ip) *
                                ceil( ( proposedLocalMax - get_absLowBound(ip) ) / get_step_size(ip) - 0.5f );
                            localMaxBkd -= localMaxBkd > proposedLocalMax ? get_step_size(ip) : 0.0f;
                        }
                    } else {
                        localMinBkd = std::max( newMinPos[dd], proposedLocalMin );
                        localMaxBkd = std::min( newMaxPos[dd], proposedLocalMax );
                    }

                    if ( WRITE_DEBUG_MOTION_FILE && pInds[0]==debugPInd ) {
                        localMinBkdStore->at(dd) = localMinBkd;
                        localMaxBkdStore->at(dd) = localMaxBkd;
                    }

                    // If required position range is outside acceptible range for this parameter then promote and rerun
                    if ( newMaxPos[dd] < localMinBkd || newMinPos[dd] > localMaxBkd ) {
                        priorityChanged = escalatePriority( dd, priority, isSet, priorityInd );
                        if ( priorityChanged ) break;
                    }

                    if ( localMaxBkd - localMinBkd <= -ZERO_DIFF_THRESHOLD ) {
                        cout << "Debugging ::: [" << localMinBkd << "," << localMaxBkd << "] " << pInds[0] << endl;
                    }

                    assert( localMaxBkd - localMinBkd > -ZERO_DIFF_THRESHOLD );
                }
            }
            if ( priorityChanged ) break;

            if ( foundNxtInd ) {
                localMinFwd = newPos[nxtInd];
                localMaxFwd = newPos[nxtInd];

                //if ( loopCount == 1 ) cout << endl << " Fwd ";
                for ( int dd=nxtInd-1; dd>=curInd; dd-- ) {
                    unsigned int ip = pInds[dd];
                    float proposedLocalMin = localMinFwd - dx[ dd + 1 ];
                    float proposedLocalMax = localMaxFwd + dx[ dd + 1 ];
                    if ( get_step_size(ip) > 0 ) {
                        // Make sure local values are valid discrete steps
                        if ( newMinPos[ dd ] > proposedLocalMin ) {
                            localMinFwd = get_absLowBound( ip ) + get_step_size(ip) *
                                ceil( ( newMinPos[ dd ] - get_absLowBound(ip) ) / get_step_size(ip) - 0.5f );
                            localMinFwd += localMinFwd < newMinPos[ dd ] ? get_step_size(ip) : 0.0f;
                        } else {
                            localMinFwd = get_absLowBound( ip ) + get_step_size(ip) *
                                ceil( ( proposedLocalMin - get_absLowBound(ip) ) / get_step_size(ip) - 0.5f );
                            localMinFwd += localMinFwd < proposedLocalMin ? get_step_size(ip) : 0.0f;
                        }
                        if ( newMaxPos[ dd ] < proposedLocalMax ) {
                            localMaxFwd = get_absLowBound( ip ) + get_step_size(ip) *
                                ceil( ( newMaxPos[ dd ] - get_absLowBound(ip) ) / get_step_size(ip) - 0.5f );
                            localMaxFwd -= localMaxFwd > newMaxPos[ dd ] ? get_step_size(ip) : 0.0f;
                        } else {
                            localMaxFwd = get_absLowBound( ip ) + get_step_size(ip) *
                                ceil( ( proposedLocalMax - get_absLowBound(ip) ) / get_step_size(ip) - 0.5f );
                            localMaxFwd -= localMaxFwd > proposedLocalMax ? get_step_size(ip) : 0.0f;
                        }
                    } else {
                        localMinFwd = std::max( newMinPos[dd], proposedLocalMin );
                        localMaxFwd = std::min( newMaxPos[dd], proposedLocalMax );
                    }

                    if ( WRITE_DEBUG_MOTION_FILE && pInds[0]==debugPInd ) {
                        localMinBkdStore->at(dd) = localMinBkd;
                        localMaxBkdStore->at(dd) = localMaxBkd;
                    }

                    // If required position range is outside acceptible range for this parameter then promote and rerun
                    if ( newMaxPos[dd] < localMinFwd || newMinPos[dd] > localMaxFwd ) {
                        priorityChanged = escalatePriority( dd, priority, isSet, priorityInd );
                        if ( priorityChanged ) break;
                    }
                    assert( localMaxFwd - localMinFwd > -ZERO_DIFF_THRESHOLD );
                }
            }
            if ( priorityChanged ) break;

            // Try Setting the current point as close as possible to the desired position
            float localMin = std::max( localMinBkd , localMinFwd );
            float localMax = std::min( localMaxBkd , localMaxFwd );

            if ( newMaxPos[curInd] < localMin || newMinPos[curInd] > localMax ) {
                // If required position range is outside acceptible range for this parameter then promote and rerun
                priorityChanged = escalatePriority( curInd, priority, isSet );
            } else {
                newMaxPos[ curInd ] = std::min( newMaxPos[ curInd ], localMax );
                newMinPos[ curInd ] = std::max( newMinPos[ curInd ], localMin );
                unsigned int ip = pInds[curInd];
                if ( get_step_size(ip) > 0 ) {
                    if ( newPos[ curInd ] < newMinPos[ curInd ] ) {
                        newPos[ curInd ] = get_absLowBound( ip ) + get_step_size(ip) *
                            ceil( ( newMinPos[ curInd ] - get_absLowBound(ip) ) / get_step_size(ip) - 0.5f );
                    } else if ( newPos[ curInd ] > newMaxPos[ curInd ] ) {
                        newPos[ curInd ] = get_absLowBound( ip ) + get_step_size(ip) *
                            ceil( ( newMaxPos[ curInd ] - get_absLowBound(ip) ) / get_step_size(ip) - 0.5f );
                    }
                    newPos[ curInd ] += newPos[ curInd ] < newMinPos[ curInd ] ? get_step_size(ip) : 0.0f;
                    newPos[ curInd ] -= newPos[ curInd ] > newMaxPos[ curInd ] ? get_step_size(ip) : 0.0f;
                    if ( isnan( newPos[ curInd ] ) ) {
                        cout << "NaN value : " << newPos[ curInd ] << " at " << curInd << " ["
                             << newMinPos[ curInd ] << "," << newMaxPos[ curInd ] << "] "
                             << ip << " : " << get_absLowBound(ip) << " : " << get_step_size(ip) << endl;
                    }
                } else {
                    newPos[ curInd ] = std::min( newMaxPos[ curInd ], newPos[ curInd ] );
                    newPos[ curInd ] = std::max( newMinPos[ curInd ], newPos[ curInd ] );
                }

                isSet[ curInd ] = true;
            }
        }
    } while ( priorityChanged );

    if ( WRITE_DEBUG_MOTION_FILE && pInds[0]==debugPInd ) {
        delete localMinBkdStore;
        delete localMaxBkdStore;
        delete localMinFwdStore;
        delete localMaxFwdStore;
        debugfile.close();
    }

    // Update parameters with new values and bounds.
    cc = 0;
    for ( std::vector<unsigned int>::iterator itPInd=pInds.begin(); itPInd<pInds.end(); itPInd++ ) {
        lowBound_[ *itPInd ] = newMinPos[ cc ];
        highBound_[ *itPInd ] = newMaxPos[ cc ];
        value_[ *itPInd ] = get_RoundToValidValue(*itPInd,  newPos[ cc ]);
        ++cc;
    }

}


/**
 * Escalate priority of a problem point to cause it to be fixed before other points which are
 * hopefully less constrained.
 *
 * @param pt            The index of the problem point
 * @param priority      A vector of point indexes in priority order
 * @param isSet         A vector indicating if the parameter has been fixed or not.
 * @param priorityIt    The priority position of the current point before which the problem point
 *						will be placed
 *                      (optional default=current priority position)
 */
bool DirectParameterVector::escalatePriority( unsigned int pt, vector<unsigned int> & priority, vector<bool> & isSet,
		vector<unsigned int>::iterator & priorityIt ) {

    if ( pt != *priority.begin() ) {

        // Remove problem point from priority list
        priority.erase( find(priority.begin(), priority.end(), pt) );

        // Put point back into priority list before current high priority point
        priority.insert(priorityIt, pt);

        // Unset all the parameters which are now of lower priority than than the current point
		vector<unsigned int>::iterator priorityItTmp;
        for ( priorityItTmp = priorityIt-1; priorityItTmp < priority.end(); priorityItTmp++ ) {
            isSet[*priorityItTmp] = false;
        }

        return true;

    } else {

        // If point still fails or if we start cycling between two or more
        // points which need to be top priority then give up.
        cerr << "DirectParameterVector::makePossible_motion(): "
                << "Failed to solve restricted point by setting priorities. "
                << "Cannot make deliverable field" << std::endl;
        throw DirectParameterVector_exception();
    }
}
bool DirectParameterVector::escalatePriority( unsigned int pt, vector<unsigned int> & priority, vector<bool> & isSet) {
	vector<unsigned int>::iterator it = find(priority.begin(), priority.end(), pt);
    return escalatePriority( pt, priority, isSet, it );
}

/**
 * Transform the parameter values encoding leaf positions into ones obeying the minimum leaf gap constraint
 * between opposing leaves in the same track
 */
void DirectParameterVector::makePossible_minGap(unsigned int cp) {

    if ( nXLeafParams_[cp] > 0 ) {
        int nLeaves = (nXLeafParams_[cp]/2);

        // Manage leaf and its opposing match
        unsigned int ip0 = get_PInd(cp, XLeaf);
        unsigned int ip1 = ip0 + nLeaves;
        float halfMinGap = machine_->getXLeaf(0)->getMinGapOpposing() * 0.5f;
        for (int lf=0; lf<nLeaves; lf++) {
            float gapCenter = ( get_value(ip0) + get_value(ip1) ) * 0.5f;
            if ( lowBound_[ ip0 ] > gapCenter - halfMinGap ) gapCenter = lowBound_[ ip0 ] + halfMinGap;
            if ( highBound_[ ip1 ] < gapCenter + halfMinGap ) gapCenter = highBound_[ ip1 ] - halfMinGap;
            highBound_[ip0] = std::min( get_highBound(ip0), gapCenter - halfMinGap );
            lowBound_[ip1] = std::max( get_lowBound(ip1), gapCenter + halfMinGap );

            value_[ ip0 ] = get_RoundToValidValue(ip0, value_[ip0]);
            value_[ ip1 ] = get_RoundToValidValue(ip1, value_[ip1]);
            /*value_[ ip0 ] = std::min( get_value(ip0), get_highBound(ip0) );
            value_[ ip0 ] = std::max( get_value(ip0), get_lowBound(ip0) );
            value_[ ip1 ] = std::min( get_value(ip1), get_highBound(ip1) );
            value_[ ip1 ] = std::max( get_value(ip1), get_lowBound(ip1) );*/

            ++ip0;  ++ip1;
        }
    }

    if ( nYLeafParams_[cp] > 0 ) {
        int nLeaves = (nYLeafParams_[cp]/2);

        // Manage leaf and its opposing match
        unsigned int ip0 = get_PInd(cp, YLeaf);
        unsigned int ip1 = ip0 + nLeaves;
        float halfMinGap = machine_->getYLeaf(0)->getMinGapOpposing() * 0.5f;
        for (int lf=0; lf<nLeaves; lf++) {
            float gapCenter = ( get_value(ip0) + get_value(ip1) ) * 0.5f;
            if ( lowBound_[ ip0 ] > gapCenter - halfMinGap ) gapCenter = lowBound_[ ip0 ] + halfMinGap;
            if ( highBound_[ ip1 ] > gapCenter + halfMinGap ) gapCenter = highBound_[ ip1 ] - halfMinGap;
            highBound_[ip0] = std::min( get_highBound(ip0), gapCenter - halfMinGap );
            lowBound_[ip1] = std::max( get_lowBound(ip1), gapCenter + halfMinGap );

            value_[ ip0 ] = get_RoundToValidValue(ip0, value_[ip0]);
            value_[ ip1 ] = get_RoundToValidValue(ip1, value_[ip1]);
            /*value_[ ip0 ] = std::min( get_value(ip0), get_highBound(ip0) );
            value_[ ip0 ] = std::max( get_value(ip0), get_lowBound(ip0) );
            value_[ ip1 ] = std::min( get_value(ip1), get_highBound(ip1) );
            value_[ ip1 ] = std::max( get_value(ip1), get_lowBound(ip1) );*/

            ++ip0;  ++ip1;
        }
    }
}


/**
 * Transform the parameter values encoding leaf positions into ones obeying the minimum leaf gap constraint
 * between opposing leaves in adjacent tracks.
 */
void DirectParameterVector::makePossible_interdigitate(unsigned int cp) {
    if ( nXLeafParams_[cp] > 0 ) {
        int nLeaves = (nXLeafParams_[cp]/2);
        float halfMinGap = machine_->getXLeaf(1)->getOppositeAboveMinGap() * 0.5f;

        // Assume all leaves have the same constraint and just check the first one.
        if ( ! machine_->xLeafCanInterdigitate(0) ) {
            unsigned int ip0 = get_PInd(cp, XLeaf);
            unsigned int ip1 = ip0 + nLeaves;

            // Four leaf gaps must be considered. n = number of leaf pairs
            // Gap0 between leaf i and leaf i+n+1
            // Gap1 between leaf i+1 and leaf i+n
            // Gap3 between leaf i and leaf i+n-1
            // Gap4 between leaf i-1 and leaf i+n-1
            float gapCenter0 = ( get_value(ip0) + get_value(ip1+1) ) * 0.5f;
            float gapCenter1 = ( get_value(ip0+1) + get_value(ip1) ) * 0.5f;
            float gapCenter2, gapCenter3; // First leaf has no leaf pair above it (i-1, i+n-1)

            if ( lowBound_[ ip0 ] > gapCenter0 - halfMinGap ) gapCenter0 = lowBound_[ ip0 ] + halfMinGap;
            if ( highBound_[ ip1 ] < gapCenter1 + halfMinGap ) gapCenter1 = highBound_[ ip1 ] - halfMinGap;
            highBound_[ip0] = std::min( get_highBound(ip0), gapCenter0 - halfMinGap );
            lowBound_[ip1] = std::max( get_lowBound(ip1), gapCenter1 + halfMinGap );
            for (int lf=1; lf<(nLeaves-1); lf++) {
                ++ip0;  ++ip1;
                gapCenter2 = gapCenter0; // Previous gaps below are now gaps above
                gapCenter3 = gapCenter1;
                gapCenter0 = ( get_value(ip0) + get_value(ip1+1) ) * 0.5f;
                gapCenter1 = ( get_value(ip0+1) + get_value(ip1) ) * 0.5f;

                if ( lowBound_[ ip0 ] > gapCenter0 - halfMinGap ) gapCenter0 = lowBound_[ ip0 ] + halfMinGap;
                if ( highBound_[ ip1 ] < gapCenter1 + halfMinGap ) gapCenter1 = highBound_[ ip1 ] - halfMinGap;
                if ( lowBound_[ ip0 ] > gapCenter2 - halfMinGap ) gapCenter2 = lowBound_[ ip0 ] + halfMinGap;
                if ( highBound_[ ip1 ] < gapCenter3 + halfMinGap ) gapCenter3 = highBound_[ ip1 ] - halfMinGap;

                highBound_[ip0] = std::min( get_highBound(ip0), gapCenter0 - halfMinGap );
                lowBound_[ip1] = std::max( get_lowBound(ip1), gapCenter1 + halfMinGap );
                highBound_[ip0] = std::min( get_highBound(ip0), gapCenter2 - halfMinGap );
                lowBound_[ip1] = std::min( get_lowBound(ip1), gapCenter3 + halfMinGap );
            }
            highBound_[ip0+1] = std::min( get_highBound(ip0+1), gapCenter0 - halfMinGap );
            lowBound_[ip1+1] = std::max( get_lowBound(ip1+1), gapCenter1 + halfMinGap );

            ip0 = get_PInd(cp, XLeaf);
            ip1 = ip0 + nLeaves;
            for (int lf=0; lf<nLeaves; lf++) {
                value_[ ip0 ] = get_RoundToValidValue(ip0, value_[ip0]);
                value_[ ip1 ] = get_RoundToValidValue(ip1, value_[ip1]);
                /*value_[ ip0 ] = std::min( get_value(ip0), get_highBound(ip0) );
                value_[ ip0 ] = std::max( get_value(ip0), get_lowBound(ip0) );
                value_[ ip1 ] = std::min( get_value(ip1), get_highBound(ip1) );
                value_[ ip1 ] = std::max( get_value(ip1), get_lowBound(ip1) );*/
                ++ip0;  ++ip1;
            }
        }
    }

    if ( nYLeafParams_[cp] > 0 ) {
        int nLeaves = (nYLeafParams_[cp]/2);
        float halfMinGap = machine_->getYLeaf(1)->getOppositeAboveMinGap() * 0.5f;
        // Assume all leaves have the same constraint and just check the first one.
        if ( ! machine_->yLeafCanInterdigitate(0) ) {
            unsigned int ip0 = get_PInd(cp, YLeaf);
            unsigned int ip1 = ip0 + nLeaves;

            // Four leaf gaps must be considered
            // gap0 between leaf i and leaf i+n+1
            // gap1 between leaf i+1 and leaf i+n
            // gap3 between leaf i and leaf i+n-1
            // gap4 between leaf i-1 and leaf i+n-1
            float gapCenter0 = ( get_value(ip0) + get_value(ip1+1) ) * 0.5f;
            float gapCenter1 = ( get_value(ip0+1) + get_value(ip1) ) * 0.5f;
            float gapCenter2, gapCenter3; // First leaf has no leaf pair above it (i-1, i+n-1)

            if ( lowBound_[ ip0 ] > gapCenter0 - halfMinGap ) gapCenter0 = lowBound_[ ip0 ] + halfMinGap;
            if ( highBound_[ ip1 ] < gapCenter1 + halfMinGap ) gapCenter1 = highBound_[ ip1 ] - halfMinGap;
            highBound_[ip0] = std::min( get_highBound(ip0), gapCenter0 - halfMinGap );
            lowBound_[ip1] = std::max( get_lowBound(ip1), gapCenter1 + halfMinGap );
            for (int lf=1; lf<(nLeaves-1); lf++) {
                ++ip0;  ++ip1;
                gapCenter2 = gapCenter0; // Previous gaps below are now gaps above
                gapCenter3 = gapCenter1;
                gapCenter0 = ( get_value(ip0) + get_value( ip1 + 1 ) ) * 0.5f;
                gapCenter1 = ( get_value(ip0+1) + get_value( ip1 ) ) * 0.5f;

                if ( lowBound_[ ip0 ] > gapCenter0 - halfMinGap ) gapCenter0 = lowBound_[ ip0 ] + halfMinGap;
                if ( highBound_[ ip1 ] < gapCenter1 + halfMinGap ) gapCenter1 = highBound_[ ip1 ] - halfMinGap;
                if ( lowBound_[ ip0 ] > gapCenter2 - halfMinGap ) gapCenter2 = lowBound_[ ip0 ] + halfMinGap;
                if ( highBound_[ ip1 ] < gapCenter3 + halfMinGap ) gapCenter3 = highBound_[ ip1 ] - halfMinGap;

                highBound_[ip0] = std::min( get_highBound( ip0 ), gapCenter0 - halfMinGap );
                lowBound_[ip1] = std::max( get_lowBound( ip1 ), gapCenter1 + halfMinGap );
                highBound_[ip0] = std::min( get_highBound( ip0 ), gapCenter2 - halfMinGap );
                lowBound_[ip1] = std::min( get_lowBound( ip1 ), gapCenter3 + halfMinGap );
            }
            highBound_[ip0+1] = std::min( get_highBound( ip0 + 1 ), gapCenter0 - halfMinGap );
            lowBound_[ip1+1] = std::max( get_lowBound( ip1 + 1 ), gapCenter1 + halfMinGap );

            ip0 = get_PInd(cp, YLeaf);
            ip1 = ip0 + nLeaves;
            for (int lf=0; lf<nLeaves; lf++) {
                value_[ ip0 ] = get_RoundToValidValue(ip0, value_[ip0]);
                value_[ ip1 ] = get_RoundToValidValue(ip1, value_[ip1]);
                /*value_[ ip0 ] = std::min( get_value(ip0), get_highBound(ip0) );
                value_[ ip0 ] = std::max( get_value(ip0), get_lowBound(ip0) );
                value_[ ip1 ] = std::min( get_value(ip1), get_highBound(ip1) );
                value_[ ip1 ] = std::max( get_value(ip1), get_lowBound(ip1) );*/
                ++ip0;  ++ip1;
            }
        }
    }
}


/**
 * Transform the parameter values encoding leaf positions into ones obeying the limited leaf length constraint or
 * maximum distance between least extended leaf and furthest extended leaf relevant for leaves on a carriage.
 */
void DirectParameterVector::makePossible_carriage(unsigned int fd, bool conserveExtended) {
    int cpi = get_absCPindex(fd, 0);
    if ( nXLeafParams_[cpi] > 0 ) {
        // Assume all leaves have the same constraint and just check the first one.
        if ( ! machine_->xLeafIsFullLength(0) ) {
            float leafLen = machine_->getXLeaf(0)->getLength();
            int nLeaves = (nXLeafParams_[cpi]/2);

            // Extract leaf parameter numbers and positions but ordered by position rather than index.
            multimap<float, int> leafPos;
			for ( unsigned int cp=0; cp<ncp_[fd]; cp++ ) {
                unsigned int ip0 = get_PInd(cpi+cp, XLeaf);
                for ( int lf = 0; lf < nLeaves; lf++ )
                    leafPos.insert ( pair<float, int>(get_value( ip0 + lf ), ip0 + lf) );
            }

            // Get iterator to leaf with the smallest and largest position.
            multimap<float, int>::iterator itLfFwd( leafPos.begin() );
            multimap<float, int>::iterator itLfBkd( --leafPos.end() );

            // Keep going until smallest and largest leaves obey constraint.
            bool violationDetected = true;
            int loopCount = 0;
            while (violationDetected) {
                violationDetected = false;
                ++loopCount;
                if ( itLfBkd->first - itLfFwd->first > leafLen ) {
                    violationDetected = true;

                    float lowLim = get_lowBound(itLfBkd->second) - leafLen;

                    if ( get_highBound(itLfFwd->second) > lowLim ) {
                        if (!conserveExtended) {
                            lowBound_[ itLfFwd->second ]= std::max( get_lowBound(itLfFwd->second), lowLim );
                            value_[ itLfFwd->second ] = std::max( itLfFwd->first, get_lowBound(itLfFwd->second) );
                            highBound_[ itLfBkd->second ]= get_value(itLfFwd->second) + leafLen;
                            value_[ itLfBkd->second ] = std::min( itLfBkd->first, get_highBound(itLfBkd->second) );
                        } else {
                            float highLim = get_highBound(itLfFwd->second) + leafLen;
                            highBound_[ itLfBkd->second ]= std::min( get_highBound(itLfFwd->second), highLim );
                            value_[ itLfBkd->second ] = std::min( itLfBkd->first, get_highBound(itLfBkd->second) );
                            lowBound_[ itLfFwd->second ]= get_value(itLfBkd->second) + leafLen;
                            value_[ itLfFwd->second ] = std::max( itLfFwd->first, get_lowBound(itLfFwd->second) );
                        }

                        unsigned int pn0 = itLfFwd->second;
                        unsigned int pn1 = itLfBkd->second;
                        leafPos.erase(itLfFwd);
                        leafPos.erase(itLfBkd);
                        leafPos.insert ( pair<float, int>(get_value( pn0 ), pn0) );
                        leafPos.insert ( pair<float, int>(get_value( pn1 ), pn1) );
                        itLfFwd = leafPos.begin();
                        itLfBkd = --leafPos.end();

                    } else {
                        cerr << "DirectParameterVector::makePossible_carriage(): "
                                << "Conflicting leaf position limits." << endl;
                        assert( 0 );
                    }
                }
                assert( loopCount < 100 );
            }

            // Now positions are fixed run a final check through the limits
            float minLimit = itLfBkd->first - leafLen;
            float maxLimit = itLfFwd->first + leafLen;
            for ( itLfFwd=leafPos.begin(); itLfFwd != leafPos.end(); ++itLfFwd ) {
				lowBound_[ itLfFwd->second ] = std::max( minLimit, get_lowBound( itLfFwd->second ) );
				highBound_[ itLfFwd->second ] = std::min( maxLimit, get_highBound( itLfFwd->second ) );
            	value_[ itLfFwd->second ] = get_RoundToValidValue(itLfFwd->second, value_[itLfFwd->second]);
            }

            // Repeat for other leaf bank

            // Extract leaf parameter numbers and positions but ordered by position rather than index.
            leafPos.clear();
			for ( unsigned int cp=0; cp<ncp_[fd]; cp++ ) {
                unsigned int ip0 = get_PInd(cpi+cp, XLeaf);
                for ( int lf = 0; lf < nLeaves; lf++ )
                    leafPos.insert ( pair<float, int>(get_value( ip0 + lf + nLeaves), ip0 + lf + nLeaves) );
            }

            // Get iterator to leaf with the smallest and largest position.
            itLfFwd = leafPos.begin();
            itLfBkd = --leafPos.end();

            // Keep going until smallest and largest leaves obey constraint.
            violationDetected = true;
            loopCount = 0;
            while (violationDetected) {
                violationDetected = false;
                ++loopCount;
                if ( itLfBkd->first - itLfFwd->first > leafLen ) {
                    violationDetected = true;

                    float lowLim = get_lowBound(itLfBkd->second) - leafLen;

                    if ( get_highBound(itLfFwd->second) > lowLim ) {
                        if (conserveExtended) {
                            lowBound_[ itLfFwd->second ] = std::max( get_lowBound(itLfFwd->second), lowLim );
                            value_[ itLfFwd->second ] = std::max( itLfFwd->first, get_lowBound(itLfFwd->second) );
                            highBound_[ itLfBkd->second ] = get_value(itLfFwd->second) + leafLen;
                            value_[ itLfBkd->second ] = std::min( itLfBkd->first, get_highBound(itLfBkd->second) );
                        } else {
                            float highLim = get_highBound(itLfFwd->second) + leafLen;
                            highBound_[ itLfBkd->second ] = std::min( get_highBound(itLfFwd->second), highLim );
                            value_[ itLfBkd->second ] = std::min( itLfBkd->first, get_highBound(itLfBkd->second) );
                            lowBound_[ itLfFwd->second ] = get_value(itLfBkd->second) + leafLen;
                            value_[ itLfFwd->second ] = std::max( itLfFwd->first, get_lowBound(itLfFwd->second) );
                        }

                        unsigned int pn0 = itLfFwd->second;
                        unsigned int pn1 = itLfBkd->second;
                        leafPos.erase(itLfFwd);
                        leafPos.erase(itLfBkd);
                        leafPos.insert ( pair<float, int>(get_value( pn0 ), pn0) );
                        leafPos.insert ( pair<float, int>(get_value( pn1 ), pn1) );
                        itLfFwd = leafPos.begin();
                        itLfBkd = --leafPos.end();

                    } else {
                        cerr << "DirectParameterVector::makePossible_carriage(): "
                                << "Conflicting leaf position limits." << endl;
                        assert( 0 );
                    }
                }
                assert( loopCount < 100 );
            }

            // Now positions are fixed run a final check through the limits
            minLimit = itLfBkd->first - leafLen;
            maxLimit = itLfFwd->first + leafLen;
            for ( itLfFwd=leafPos.begin(); itLfFwd != leafPos.end(); itLfFwd++ ) {
				lowBound_[ itLfFwd->second ] = std::max( minLimit, get_lowBound( itLfFwd->second ) );
				highBound_[ itLfFwd->second ] = std::min( maxLimit, get_highBound( itLfFwd->second ) );
				value_[ itLfFwd->second ] = get_RoundToValidValue(itLfFwd->second, value_[itLfFwd->second]);
            }
        }
    }

    if ( nYLeafParams_[cpi] > 0 ) {
        // Assume all leaves have the same constraint and just check the first one.
        if ( ! machine_->yLeafIsFullLength(0) ) {
            float leafLen = machine_->getYLeaf(0)->getLength();
            int nLeaves = (nYLeafParams_[cpi]/2);

            // Extract leaf parameter numbers and positions but ordered by position rather than index.
            multimap<float, int> leafPos;
			for ( unsigned int cp=0; cp<ncp_[fd]; cp++ ) {
                unsigned int ip0 = get_PInd(cpi+cp, YLeaf);
                for ( int lf = 0; lf < nLeaves; lf++ )
                    leafPos.insert ( pair<float, int>(get_value( ip0 + lf ), ip0 + lf) );
            }

            // Get iterator to leaf with the smallest and largest position.
            std::multimap<float, int>::iterator itLfFwd = leafPos.begin();
            std::multimap<float, int>::iterator itLfBkd = --leafPos.end();

            // Keep going until smallest and largest leaves obey constraint.
            bool violationDetected = true;
            int loopCount = 0;
            while (violationDetected) {
                violationDetected = false;
                ++loopCount;
                if ( itLfBkd->first - itLfFwd->first > leafLen ) {
                    violationDetected = true;

                    float lowLim = get_lowBound(itLfBkd->second) - leafLen;

                    if ( get_highBound(itLfFwd->second) > lowLim ) {
                        if (!conserveExtended) {
                            lowBound_[ itLfFwd->second ] = std::max( get_lowBound(itLfFwd->second), lowLim );
                            value_[ itLfFwd->second ] = std::max( itLfFwd->first, get_lowBound(itLfFwd->second) );
                            highBound_[ itLfBkd->second ] = get_value(itLfFwd->second) + leafLen;
                            value_[ itLfBkd->second ] = std::min( itLfBkd->first, get_highBound(itLfBkd->second) );
                        } else {
                            float highLim = get_highBound(itLfFwd->second) + leafLen;
                            highBound_[ itLfBkd->second ] = std::min( get_highBound(itLfFwd->second), highLim );
                            value_[ itLfBkd->second ] = std::min( itLfBkd->first, get_highBound(itLfBkd->second) );
                            lowBound_[ itLfFwd->second ] = get_value(itLfBkd->second) + leafLen;
                            value_[ itLfFwd->second ] = std::max( itLfFwd->first, get_lowBound(itLfFwd->second) );
                        }

                        unsigned int pn0 = itLfFwd->second;
                        unsigned int pn1 = itLfBkd->second;
                        leafPos.erase(itLfFwd);
                        leafPos.erase(itLfBkd);
                        leafPos.insert ( pair<float, int>(get_value( pn0 ), pn0) );
                        leafPos.insert ( pair<float, int>(get_value( pn1 ), pn1) );
                        itLfFwd = leafPos.begin();
                        itLfBkd = --leafPos.end();

                    } else {
                        cerr << "DirectParameterVector::makePossible_carriage(): "
                                << "Conflicting leaf position limits." << endl;
                        assert( 0 );
                    }
                }
                assert( loopCount < 100 );
            }

            // Now positions are fixed run a final check through the limits
            float minLimit = itLfBkd->first - leafLen;
            float maxLimit = itLfFwd->first + leafLen;
            for ( itLfFwd=leafPos.begin(); itLfFwd != leafPos.end(); itLfFwd++ ) {
				lowBound_[ itLfFwd->second ] = std::max( minLimit, get_lowBound( itLfFwd->second ) );
				highBound_[ itLfFwd->second ] = std::min( maxLimit, get_highBound( itLfFwd->second ) );
            	value_[ itLfFwd->second ] = get_RoundToValidValue(itLfFwd->second, value_[itLfFwd->second]);
            }

            // Repeat for other leaf bank

            // Extract leaf parameter numbers and positions but ordered by position rather than index.
            leafPos.clear();
			for ( unsigned int cp=0; cp<ncp_[fd]; cp++ ) {
                unsigned int ip0 = get_PInd(cpi+cp, YLeaf);
                for ( int lf = 0; lf < nLeaves; lf++ )
                    leafPos.insert ( pair<float, int>(get_value( ip0 + lf + nLeaves ), ip0 + lf + nLeaves) );
            }

            // Get iterator to leaf with the smallest and largest position.
            itLfFwd = leafPos.begin();
            itLfBkd = --leafPos.end();

            // Keep going until smallest and largest leaves obey constraint.
            violationDetected = true;
            loopCount = 0;
            while (violationDetected) {
                violationDetected = false;
                ++loopCount;
                if ( itLfBkd->first - itLfFwd->first > leafLen ) {
                    violationDetected = true;

                    float lowLim = get_lowBound(itLfBkd->second) - leafLen;

                    if ( get_highBound(itLfFwd->second) > lowLim ) {
                        if (conserveExtended) {
                            lowBound_[ itLfFwd->second ] = std::max( get_lowBound(itLfFwd->second), lowLim );
                            value_[ itLfFwd->second ] = std::max( itLfFwd->first, get_lowBound(itLfFwd->second) );
                            highBound_[ itLfBkd->second ] = get_value(itLfFwd->second) + leafLen;
                            value_[ itLfBkd->second ] = std::min( itLfBkd->first, get_highBound(itLfBkd->second) );
                        } else {
                            float highLim = get_highBound(itLfFwd->second) + leafLen;
                            highBound_[ itLfBkd->second ] = std::min( get_highBound(itLfFwd->second), highLim );
                            value_[ itLfBkd->second ] = std::min( itLfBkd->first, get_highBound(itLfBkd->second) );
                            lowBound_[ itLfFwd->second ] = get_value(itLfBkd->second) + leafLen;
                            value_[ itLfFwd->second ] = std::max( itLfFwd->first, get_lowBound(itLfFwd->second) );
                        }

                        unsigned int pn0 = itLfFwd->second;
                        unsigned int pn1 = itLfBkd->second;
                        leafPos.erase(itLfFwd);
                        leafPos.erase(itLfBkd);
                        leafPos.insert ( pair<float, int>(get_value( pn0 ), pn0) );
                        leafPos.insert ( pair<float, int>(get_value( pn1 ), pn1) );
                        itLfFwd = leafPos.begin();
                        itLfBkd = --leafPos.end();

                    } else {
                        cerr << "DirectParameterVector::makePossible_carriage(): "
                                << "Conflicting leaf position limits." << endl;
                        assert( 0 );
                    }
                }
                assert( loopCount < 100 );
            }

            // Now positions are fixed run a final check through the limits
            minLimit = itLfBkd->first - leafLen;
            maxLimit = itLfFwd->first + leafLen;
            for ( itLfFwd=leafPos.begin(); itLfFwd != leafPos.end(); itLfFwd++ ) {
				lowBound_[ itLfFwd->second ] = std::max( minLimit, get_lowBound( itLfFwd->second ) );
				highBound_[ itLfFwd->second ] = std::min( maxLimit, get_highBound( itLfFwd->second ) );
            	value_[ itLfFwd->second ] = get_RoundToValidValue(itLfFwd->second, value_[itLfFwd->second]);
            }
        }
    }
}

/**
 * Add value to a parameter
 */
void DirectParameterVector::add_value( const int ip, const float value_inc ) {
    float desiredVal = value_[ip] + value_inc;
    cout << "Add value " << value_[ip] << " : " << value_inc << " : " << get_RoundToValidValue(ip, desiredVal) << endl;
    value_[ip] = get_RoundToValidValue(ip, desiredVal);
    resetLimits();
    if ( value_[ip] - desiredVal > step_size_[ip] * ZERO_DIFF_THRESHOLD ) {
        cerr << "Unable to add requested value to parameter " << ip << endl;
        throw DirectParameterVector_exception();
    }
};

/**
 * Add a set of parameter values to the current set of parameter values
 */
void DirectParameterVector::add_value( const ParameterVector &pv ) {
    assert(pv.get_nParameters() == nParams_);

	for ( unsigned int iP = 0; iP < nParams_; iP++ )
        value_[iP] += pv.get_value(iP);

    resetLimits();
};


/**
 * Add a set of parameter values to the current set of parameter values
 */
void DirectParameterVector::add_value( const ParameterVectorDirection &pvd ) {
    assert(pvd.get_nParameters() == nParams_);

	for ( unsigned int iP = 0; iP < nParams_; iP++ )
        value_[iP] += pvd.get_value(iP);

    resetLimits();
};

/**
 * Set current parameter values to those of input set
 */
void DirectParameterVector::set_value( const ParameterVector &pv ) {
    assert(pv.get_nParameters() == nParams_);

	for ( unsigned int iP = 0; iP < nParams_; iP++ )
        value_[iP] = pv.get_value(iP);

    resetLimits();
};

/**
 * Set current parameter values to those of input set
 */
void DirectParameterVector::set_value( const ParameterVectorDirection &pvd ) {
    assert(pvd.get_nParameters() == nParams_);

	for ( unsigned int iP = 0; iP < nParams_; iP++ )
        value_[iP] = pvd.get_value(iP);

    resetLimits();
};

/**
 * Add a set of parameter values multiplied by a scaling factor to the current set of parameter values
 */
void DirectParameterVector::add_scaled_parameter_vector( const ParameterVector &pv, const float scale_factor ) {
    assert(pv.get_nParameters() == nParams_);

	for ( unsigned int iP = 0; iP < nParams_; iP++ )
        value_[iP] += pv.get_value(iP) * scale_factor;

    resetLimits();
};

/**
 * Add a set of parameter values multiplied by a scaling factor to the current set of parameter values
 */
void DirectParameterVector::add_scaled_parameter_vector( const ParameterVector &pv, const double scale_factor ) {
    assert(pv.get_nParameters() == nParams_);

	for ( unsigned int iP = 0; iP < nParams_; iP++ )
        value_[iP] += pv.get_value(iP) * scale_factor;

    resetLimits();
};

/**
 * Add a set of parameter values multiplied by a scaling factor to the current set of parameter values
 */
void DirectParameterVector::add_scaled_parameter_vector_direction( const ParameterVectorDirection &pvd, const float scale_factor ) {
    assert(pvd.get_nParameters() == nParams_);

	for ( unsigned int iP = 0; iP < nParams_; iP++ )
        value_[iP] += pvd.get_value(iP) * scale_factor;

    resetLimits();
};

/**
 * Add a set of parameter values multiplied by a scaling factor to the current set of parameter values
 */
void DirectParameterVector::add_scaled_parameter_vector_direction( const ParameterVectorDirection &pvd, const double scale_factor ) {
    assert(pvd.get_nParameters() == nParams_);

	for ( unsigned int iP = 0; iP < nParams_; iP++ )
        value_[iP] += pvd.get_value(iP) * scale_factor;

    resetLimits();
};

void DirectParameterVector::write_parameter_files( string parameters_file_root ) const {
    cout << "Writing files " << parameters_file_root << endl;
    char cCP[10];
    string param_file_name;
    for ( unsigned int cpi=0; cpi<ncpTot_; cpi++ ) {
        // convert beam# to string
        sprintf(cCP,"%d",(int)cpi+1);
        param_file_name = parameters_file_root + "_" + string(cCP) + ".dpf";
        write_parameter_file(param_file_name, cpi);
    }
};

void DirectParameterVector::write_parameter_file( string parameters_file_name, const unsigned int cpNo ) const {

    std::ofstream ofile(parameters_file_name.c_str());

    // Header
    unsigned int fdInd = get_Fieldindex(cpNo);
    unsigned int p0 = get_PIndCP(cpNo);

    ofile << "#Field Number: " << fdInd << "\n";
    if ( isArc( fdInd ) ) {
        ofile << "#Field Type: Arc\n";
    } else {
        ofile << "#Field Type: Static Gantry\n";
    }
    ofile << "#Abs Control Point Number: " << cpNo << "\n";
    ofile << "#Control Point Index Within Field: " << cpNo - get_absCPindex( fdInd, 0 ) << "\n";
    ofile << "#First Control Point Within Field: " << get_absCPindex( fdInd, 0 ) << "\n";
    ofile << "#Last Control Point Within Field: " << ncp_[fdInd] << "\n";
    ofile << "#Number of parameters within control point: " << paramPerCP_[cpNo] << "\n";
    ofile << "#First parameters index for control point: " << p0 << "\n";

    ofile << "#Delivery Machine Model: " << machine_->getMachinename() << "\n";
    ofile << "#Dij Beam Index: " << get_beamNo(p0) << "\n";

    // Add data to header if no parameters in this control point directly encode this information
    if ( get_nParam_BeamEnergy(cpNo) == 0 ) ofile << "#Beam Energy: " << get_BeamEnergy(cpNo) << "\n";
    if ( get_nParam_Gantry(cpNo) == 0 ) ofile << "#Gantry Angle: " << get_Gantry(cpNo) << " [degrees]\n";
    if ( get_nParam_Collimator(cpNo) == 0 ) ofile << "#Collimator Angle: " << get_Collimator(cpNo) << " [degrees]\n";
    if ( get_nParam_Couch(cpNo) == 0 ) ofile << "#Couch Angle: " << get_Couch(cpNo) << " [degrees]\n";
    if ( get_nParam_DoseRate(cpNo) == 0 ) ofile << "#Dose Rate: " << get_DoseRate(cpNo) * 60.0f << " [MU/min]\n";
    if ( machine_->getIsXJaws() ) {
        if ( get_nParam_XJaw(cpNo) == 0 ) {
            ofile << "#X Jaw 0: " << get_XJaw(cpNo,0) << " [mm]\n";
            ofile << "#X Jaw 1: " << get_XJaw(cpNo,1) << " [mm]\n";
        }
    }
    if ( machine_->getIsYJaws() ) {
        if ( get_nParam_YJaw(cpNo) == 0 ) {
            ofile << "#Y Jaw 0: " << get_YJaw(cpNo,0) << " [mm]\n";
            ofile << "#Y Jaw 1: " << get_YJaw(cpNo,1) << " [mm]\n";
        }
    }
    if ( machine_->getIsXMlc() )
        if ( get_nParam_XLeaf(cpNo) == 0 )
            for (unsigned int lf=0; lf<machine_->getNXJaws(); lf++ )
                ofile << "#X MLC Leaf " << lf << ": " << get_XLeaf(cpNo,lf) << " [mm]\n";

    if ( machine_->getIsYMlc() )
        if ( get_nParam_YLeaf(cpNo) == 0 )
            for (unsigned int lf=0; lf<machine_->getNYJaws(); lf++ )
                ofile << "#Y MLC Leaf " << lf << ": " << get_YLeaf(cpNo,lf) << " [mm]\n";


    ofile << setiosflags(std::ios::fixed | std::ios:: showpoint);
    ofile.precision(2);

    ofile << "#\tNum\tType\tVal\tlow\thigh\tisAct\tstep\tabsH\tabsL\tisCon\tnValid\tcanChgCP\t"
            << "canChgFd\tcanChgDir\tminSp\tmaxSp\tmaxAcc\tchgRqInt\tintTime\n";

    // Data directly encoded by control point parameters
    for (unsigned int pp = 0; pp<paramPerCP_[cpNo]; pp++) {
        unsigned int pInd = pp + p0;
        ofile << "\t" << pInd;
        ofile << "\t" << get_type( pInd );
        ofile << "\t" << fixed << value_[pInd];
        ofile << "\t" << fixed << lowBound_[pInd];
        ofile << "\t" << fixed << highBound_[pInd];
        ofile << "\t" << isActive_[pInd];
        ofile << "\t" << fixed << step_size_[pInd];
        ofile << "\t" << fixed << get_absHighBound( pInd );
        ofile << "\t" << fixed << get_absLowBound( pInd );
        ofile << "\t" << get_isDiscreteVariable( pInd );
        ofile << "\t" << get_numberOfValidValues( pInd );
        ofile << "\t" << get_canChangeBetweenCP( pInd );
        ofile << "\t" << get_canChangeBetweenFields( pInd );
        ofile << "\t" << get_canChangeDirection( pInd );
        ofile << "\t" << fixed << get_minSpeed( pInd );
        ofile << "\t" << fixed << get_maxSpeed( pInd );
        ofile << "\t" << fixed << get_maxAcceleration( pInd );
        ofile << "\t" << get_changeRequiresInterrupt( pInd );
        ofile << "\t" << fixed << get_interruptLatency( pInd ) << "\n";
    }

    ofile.close();
};
