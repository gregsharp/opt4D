#ifndef RESPIRATORYMOTIONMODEL_HPP
#define RESPIRATORYMOTIONMODEL_HPP

#include <vector>
using std::vector;
#include <string>
using std::string;
#include <iostream>

// Predefine class before including others
class MultiInstanceGatingModel;
class PdfVariationModel;

#include "BixelVector.hpp"
#include "DoseInfluenceMatrix.hpp"
#include "DoseDeliveryModel.hpp"



// ---------------------------------------------------------------------
// Dose delivery model for an uncertain motion Pdf
// ---------------------------------------------------------------------

/**
 * This dose delivery model assumes motion
 * that is modeled by a Pdf. The Pdf is however uncertain.
 * The uncertainty is modeled by a nominal Pdf that is perturbed
 * with a few linear independent modes which describe the variability
 * of the Pdf. Dose contributions from a discrete set of
 * instances are modeled by auxiliary Dij matrices.
 */
class PdfVariationModel : public DoseDeliveryModel
{

  public:
    /// constructor
    PdfVariationModel(const Geometry *the_geometry,
		      unsigned int nInstances,
		      vector<float> Pdf,
		      vector<unsigned int> auxiliaryNos,
		      unsigned int nModes,
		      vector<vector<float> > modes,
		      vector<float> sigma);

    PdfVariationModel(const Geometry *the_geometry,
		      string pdf_file_root,
		      unsigned int nInstances,
		      unsigned int nModes);

    // Virtual destructor
    virtual ~PdfVariationModel();

    /// get value of nominal PDF
    float get_nominal_pdf(unsigned int iInstance) const;

    /// get value of perturbation mode
    float get_mode(unsigned int iMode, unsigned int iInstance) const;

    /// get sigma for a mode
    float get_sigma(unsigned int iMode) const;

    /// get number of modes
    unsigned int get_nModes() const;

    /// get number of instances
    unsigned int get_nInstances() const;

    /// get auxiliary number for an instance
    unsigned int get_AuxiliaryDijNo(unsigned int iInstance) const;

    /// returns type of uncertainty model
    virtual std::string get_uncertainty_model() const;

    /// generate a random sample scenario
    virtual void generate_random_scenario(RandomScenario &random_scenario) const;

    /// generate the nominal scenario
    virtual void generate_nominal_scenario(RandomScenario &random_scenario) const;

    /// calculate voxel dose for a scenario
    virtual float calculate_voxel_dose(unsigned int voxelNo,
				       const BixelVector &bixel_grid,
				       DoseInfluenceMatrix &dij,
				       const RandomScenario &random_scenario) const;

    /// calculate gradient contribution of the voxel for a scenario
    virtual void calculate_voxel_dose_gradient(
            unsigned int voxelNo,
 	    const BixelVector &bixel_grid,
            DoseInfluenceMatrix &dij,
            const RandomScenario &random_scenario,
            BixelVectorDirection &gradient,
            float gradient_multiplier) const;

  protected:
    /// get motion Pdf
    virtual void generate_Pdf(RandomScenario &random_scenario) const;

  private:
    /// Make sure default constructor is never used
    PdfVariationModel();

    /// read Pdf and variation modes from file
    void read_pdf(string pdf_file_root);

    /// number of instances
    unsigned int nInstances_;

    /// number of Pdf perturbation modes
    unsigned int nModes_;

    /// look up table for the assignment of instanceNo and AuxiliaryDijNo
    vector<unsigned int> auxiliary_dij_lut_;

    /// probability for each instance
    vector<float> nominal_pdf_;

    /// Pdf perturbation modes
    vector<vector<float> > modes_;

    /// Amplitude of each mode
    vector<float> sigma_;

};

inline
std::string PdfVariationModel::get_uncertainty_model() const
{
    return "PdfVariationModel";
}

inline
float PdfVariationModel::get_nominal_pdf(unsigned int iInstance) const
{
    return( nominal_pdf_[iInstance] );
}

inline
float PdfVariationModel::get_mode(unsigned int iMode, unsigned int iInstance) const
{
    return( modes_[iMode][iInstance] );
}

inline
float PdfVariationModel::get_sigma(unsigned int iMode) const
{
    return( sigma_[iMode] );
}

inline
unsigned int PdfVariationModel::get_nInstances() const
{
    return nInstances_;
}

inline
unsigned int PdfVariationModel::get_nModes() const
{
    return nModes_;
}

inline
unsigned int PdfVariationModel::get_AuxiliaryDijNo(unsigned int iInstance) const
{
    return auxiliary_dij_lut_[iInstance];
}


// ---------------------------------------------------------------------
// Dose delivery model for a seperate fluence map per instance
// ---------------------------------------------------------------------

/**
 * This dose delivery model assumes multiple instances of geometry,
 * but for each instance a seperate fluence map is optimized.
 * There is no uncertainty in this model. Multiple instances
 * correspond to additional beams.
 */

class MultiInstanceGatingModel : public StandardDoseModel
{

  public:
    /// constructor
    MultiInstanceGatingModel(const Geometry *the_geometry,
			     unsigned int nInstances,
			     unsigned int nBeams_per_instance);

    // Virtual destructor
    virtual ~MultiInstanceGatingModel();

    /// get number of gating instances
    unsigned int get_nInstances() const;

    /// returns type of uncertainty model
    virtual std::string get_uncertainty_model() const {
	return("MultiInstanceGatingModel"); }

    /// generate a random sample scenario
    virtual void generate_random_scenario(RandomScenario &random_scenario) const {
	StandardDoseModel::generate_random_scenario(random_scenario); }

    /// generate the nominal scenario
    virtual void generate_nominal_scenario(RandomScenario &random_scenario) const {
	StandardDoseModel::generate_nominal_scenario(random_scenario); }

    /// calculate voxel dose for a scenario
    virtual float calculate_voxel_dose(unsigned int voxelNo,
				       const BixelVector &bixel_grid,
				       DoseInfluenceMatrix &dij,
				       const RandomScenario &random_scenario) const {
        return StandardDoseModel::calculate_voxel_dose(voxelNo, bixel_grid, dij, random_scenario);
    }


    /// calculate gradient contribution of the voxel for a scenario
    virtual void calculate_voxel_dose_gradient(
            unsigned int voxelNo,
 	    const BixelVector &bixel_grid,
            DoseInfluenceMatrix &dij,
            const RandomScenario &random_scenario,
            BixelVectorDirection &gradient,
            float gradient_multiplier) const {
        StandardDoseModel::calculate_voxel_dose_gradient(
                voxelNo,
		bixel_grid,
                dij,
                random_scenario,
                gradient,
                gradient_multiplier);
    }

    /// calculate instance dose
    virtual void calculate_instance_dose(DoseVector &dose_vector,
					 const BixelVector &bixel_grid,
					 DoseInfluenceMatrix &dij,
					 unsigned int iInstance) const;

  private:

    /// number of gating instances
    unsigned int nInstances_;

    /// instance number for each bixel
    vector<unsigned int> bixels_instanceNo_;
};

inline
unsigned int MultiInstanceGatingModel::get_nInstances() const
{
    return nInstances_;
}


#endif
