/**
 * @file ParameterVector.cpp
 * ParameterVector functions
 */

#include "ParameterVector.hpp"

using std::cout;
using std::endl;

/**
 * Constructor.
 * Initializes a parameter set with a given number of parameters.
 * By default, each parameter has zero value.
 *
 * @param nParams       The number of parameters in vector.
 * @param value         Float value for all parameters, (default=0).
 * @param lowBound      Float lower bound for valid values for all parameters, (default=0).
 * @param highBound     Float upper bound for valid values for all parameters, (default=infinite).
 * @param isActive      Boolean flag indicating if the parameter is contributing to the open aperature, (default=true).
 * @param step_size     The defualt value by which the value should be incremented or decremented.
 */
ParameterVector::ParameterVector( const unsigned int &nParams, const float &value, const float &lowBound,
	const float &highBound, const bool &isActive, const float &step_size )
	: nParams_( nParams ), value_( nParams, value ), lowBound_( nParams, lowBound ),
	highBound_( nParams, highBound ), isActive_( nParams, isActive ),
	step_size_( nParams, step_size ) { };


/**
 * Add a set of parameter values to the current set of parameter values
 */
void ParameterVector::add_value( const ParameterVector &pv ) {
    assert(pv.get_nParameters() == nParams_);

	for ( unsigned int iP = 0; iP < nParams_; iP++ ) {
        value_.at( iP ) += pv.value_.at( iP );
        //assert(value_[iP] >= lowBound_[iP] && value_[iP] <= highBound_[iP]);
    }
}


/**
 * Add a set of parameter values to the current set of parameter values
 */
void ParameterVector::add_value( const ParameterVectorDirection &pvd ) {
    assert(pvd.get_nParameters() == nParams_);

	for ( unsigned int iP = 0; iP < nParams_; iP++ ) {
        value_.at( iP ) += pvd.get_value( iP );
        //assert(value_[iP] >= lowBound_[iP] && value_[iP] <= highBound_[iP]);
    }
}


///**
// * Addition operator.
// * Assumes that the added ParameterVector has the same number of parameters.
// *
// * @param rhs The ParameterVector object to add.
// *
// * @return lhs The ParameterVector object formed by the sum of the lhs and this object.
// */
//ParameterVector ParameterVector::operator+=( const ParameterVector &rhs )
//{
//    assert(rhs.size() == nParams_);
//
//    for ( unsigned int iP = 0; iP < nParams_; iP++ )
//        value_.at( iP ) += rhs.value_.at( iP );
//
//    return *this;
//}
//
//
///**
// * Addition operator.
// * Assumes that the added ParameterVectorDirection has the same number of parameters.
// *
// * @param rhs The ParameterVectorDirection object to add.
// *
// * @return lhs The ParameterVector object formed by the sum of the lhs and this object.
// */
//ParameterVector ParameterVector::operator+=( const ParameterVectorDirection &rhs )
//{
//    assert(rhs_.size() == nParams_);
//
//    for ( unsigned int iP = 0; iP < nParams_; iP++ )
//        value_.at( iP ) += rhs.value_.at( iP );
//
//    return this;
//}
//
//
///**
// * Subtraction operator.
// * Assumes that the added ParameterVector has the same number of parameters.
// *
// * @param rhs The ParameterVector object to subtract.
// *
// * @return lhs The ParameterVector object formed by this object values - the lhs.
// */
//ParameterVector ParameterVector::operator-=( const ParameterVector &rhs )
//{
//    assert(rhs_.size() == nParams_);
//
//    for ( unsigned int iP = 0; iP < nParams_; iP++ )
//        value_.at( iP ) -= rhs.value_.at( iP );
//
//    return this;
//}
//
//
///**
// * Subtraction operator.
// * Assumes that the added ParameterVectorDirection has the same number of parameters.
// *
// * @param rhs The ParameterVectorDirection object to subtract.
// *
// * @return lhs The ParameterVector object formed by this object values - the lhs.
// */
//ParameterVector ParameterVector::operator-=( const ParameterVectorDirection &rhs )
//{
//    assert(rhs_.size() == nParams_);
//
//    for ( unsigned int iP = 0; iP < nParams_; iP++ )
//        value_.at( iP ) -= rhs.value_.at( iP );
//
//    return this;
//}
//
//
///**
// * Product operator.
// * Assumes that the added ParameterVector has the same number of parameters.
// *
// * @param rhs A vector of floats to multiply.
// *
// * @return lhs The ParameterVector object formed by the product of the lhs and this object.
// */
//ParameterVector ParameterVector::operator*=( const vector<float>& rhs )
//{
//    assert(rhs_.size() == nParams_);
//
//    for ( unsigned int iP = 0; iP < nParams_; iP++ )
//        value_.at( iP ) *= rhs[iP];
//
//    return this;
//}
//
//
///**
// * Product operator.
// * Assumes that the added ParameterVector has the same number of parameters.
// *
// * @param rhs A vector of doubles to multiply.
// *
// * @return lhs The ParameterVector object formed by the product of the lhs and this object.
// */
//ParameterVector ParameterVector::operator*=( const vector<double>& rhs )
//{
//    assert(rhs_.size() == nParams_);
//
//    for ( unsigned int iP = 0; iP < nParams_; iP++ )
//        value_.at( iP ) *= rhs[iP];
//
//    return this;
//}
//
//
///**
// * Product operator.
// * Assumes that the added ParameterVector has the same number of parameters.
// *
// * @param rhs The ParameterVector object to multiply.
// *
// * @return lhs The ParameterVector object formed by the product of the lhs and this object.
// */
//ParameterVector ParameterVector::operator*=( const ParameterVector &rhs )
//{
//    assert(rhs_.size() == nParams_);
//
//    for ( unsigned int iP = 0; iP < nParams_; iP++ )
//        value_.at( iP ) *= rhs.value_.at( iP );
//
//    return this;
//}
//
//
///**
// * Product operator.
// * Assumes that the added ParameterVector has the same number of parameters.
// *
// * @param rhs The ParameterVector object to multiply.
// *
// * @return lhs The ParameterVector object formed by the product of the lhs and this object.
// */
//ParameterVector ParameterVector::operator*=( const ParameterVectorDirection &rhs )
//{
//    assert(rhs_.size() == nParams_);
//
//    for ( unsigned int iP = 0; iP < nParams_; iP++ )
//        value_.at( iP ) *= rhs.value_.at( iP );
//
//    return this;
//}


/**
 * Add a scaled ParameterVector with the same number of parameters
 *
 * @param pv            The ParameterVector object to add.
 * @param scale_factor  The factor by which the parameter vector should be scaled.
 */
void ParameterVector::add_scaled_parameter_vector( const ParameterVector &pv, const double scale_factor ) {
    for ( unsigned int iP = 0; iP < nParams_; iP++ ) {
        value_[iP] += pv.get_value(iP) * scale_factor;
        //assert(value_[iP] >= lowBound_[iP] && value_[iP] <= highBound_[iP]);
    }
}


/**
 * Add a scaled ParameterVector with the same number of parameters
 *
 * @param pv            The ParameterVector object to add.
 * @param scale_factor  The factor by which the parameter vector should be scaled.
 */
void ParameterVector::add_scaled_parameter_vector( const ParameterVector &pv, const float scale_factor ) {
    for ( unsigned int iP = 0; iP < nParams_; iP++ ) {
        value_[iP] += pv.get_value(iP) * scale_factor;
        //assert(value_[iP] >= lowBound_[iP] && value_[iP] <= highBound_[iP]);
    }
}


/**
 * Add a scaled ParameterVector with the same number of parameters
 *
 * @param pvd            The ParameterVectorDirection object to add.
 * @param scale_factor  The factor by which the parameter vector should be scaled.
 */
void ParameterVector::add_scaled_parameter_vector_direction( const ParameterVectorDirection &pvd, const double scale_factor ) {
    for ( unsigned int iP = 0; iP < nParams_; iP++ ) {
        value_[iP] += pvd.get_value(iP) * scale_factor;
        //assert(value_[iP] >= lowBound_[iP] && value_[iP] <= highBound_[iP]);
    }
}


/**
 * Add a scaled ParameterVector with the same number of parameters
 *
 * @param pvd            The ParameterVectorDirection object to add.
 * @param scale_factor  The factor by which the parameter vector should be scaled.
 */
void ParameterVector::add_scaled_parameter_vector_direction( const ParameterVectorDirection &pvd, const float scale_factor ) {
    for ( unsigned int iP = 0; iP < nParams_; iP++ ) {
        value_[iP] += pvd.get_value(iP) * scale_factor;
        //assert(value_[iP] >= lowBound_[iP] && value_[iP] <= highBound_[iP]);
    }
}


/**
 * Scale all parameter values by a constant.
 *
 * @param scale_factor by which all values will be multiplied
 */
void ParameterVector::scale_values( const double scale_factor ) {
    for ( unsigned int iP = 0; iP < nParams_; iP++ ) {
        value_[iP] = value_[iP] * scale_factor;
        //assert(value_[iP] >= lowBound_[iP] && value_[iP] <= highBound_[iP]);
    }
}


/**
 * Scale all parameter values by a constant.
 *
 * @param scale_factor by which all values will be multiplied
 */
void ParameterVector::scale_values( const float scale_factor ) {
    for ( unsigned int iP = 0; iP < nParams_; iP++ ) {
        value_[iP] = value_[iP] * scale_factor;
        //assert(value_[iP] >= lowBound_[iP] && value_[iP] <= highBound_[iP]);
    }
}

/**
 * Translate the hessian in terms of bixel parameters to the hessian in terms of
 * the parameters used to describe plan.
 */
vector<float> ParameterVector::translateBixelHessian( vector<float> & hv ) const {
    cout << "Called ParameterVector::translateBixelHessian()" << endl;
    throw(std::runtime_error("Function not implemented in derived class"));
}
