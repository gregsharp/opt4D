#ifndef STRINGFUNCTIONS_HPP
#define STRINGFUNCTIONS_HPP

#include <iostream>
#include <sstream>

using namespace std;

    /*
     * Useful non-member functions
     */

/**
 * Remove initial or trailing white space
 */
inline string remove_whitespace( const std::string & str ) {
    std::string temp = str;

    // Remove Initial White space
    while( temp.length() > 0 && ((*(temp.begin()) == ' ') || (*(temp.begin()) == '\t')) ) {
        temp.erase(temp.begin());
    }

    // Remove Trailing White space
    while( temp.length() > 0 && ((*(temp.rbegin()) == ' ') || (*(temp.rbegin()) == '\t')) ) {
        temp.erase(temp.end()-1);
    }
    return temp;
};


/**
 * Read a number from a string.
 * Returns false if the string read fails.
 */
template <class T> bool from_string( T& t, const std::string& s, std::ios_base& (*f)(std::ios_base&) ) {
    std::istringstream iss(s);
    return !(iss >> f >> t).fail();
};


/**
 * Get the lowercase version of a string
 *
 * @param str The input string to convert
 */
inline string to_lower( const std::string & str ) {
    // Get a copy of the input string
    std::string temp_str = str;

    int upper_to_lower_shift = 'a' - 'A';
    for( std::string::iterator iter = temp_str.begin(); iter != temp_str.end(); iter++ ) {
        if( *iter >= 'A' && *iter <= 'Z' ) {
            *iter = *iter + upper_to_lower_shift;
        }
    }
    return temp_str;
};


/**
 * Check if a string starts with a matchstring.
 * Remove spaces and then compare start of string with match in a non case specific way.
 */
inline bool startsWith( const string & str1, const string & str2 ) {

    string originalStr = remove_whitespace( to_lower( str1 ) );
    string matchStr = remove_whitespace( to_lower( str2 ) );

    if ( originalStr.length() < matchStr.length() ) return false;

    return ( originalStr.substr( 0, matchStr.length() ) ).compare( matchStr ) == 0;
};


#endif // STRINGFUNCTIONS_HPP
