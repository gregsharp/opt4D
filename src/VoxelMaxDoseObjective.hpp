/**
 * @file VoxelMaxDoseObjective.hpp
 * VoxelMaxDoseObjective Class header
 *
 * $Id: MeanSquaredErrorObjective.hpp,v 1.26 2008/03/14 15:00:26 bmartin Exp $
 */

#ifndef VOXELMAXDOSEOBJECTIVE_HPP
#define VOXELMAXDOSEOBJECTIVE_HPP

#include "SeparableObjective.hpp"
#include "Voi.hpp"
#include <cmath>

/**
 * Class VoxelMaxDoseObjective is used to apply a voxel dependent maximum dose.
 * For every voxel there is a quadratic penalty for doses exceeding the 
 * voxel-dependent maximum dose
 */
class VoxelMaxDoseObjective : public SeparableObjective
{

    public:

        // Constructor
        VoxelMaxDoseObjective(
                Voi*  the_voi,
                unsigned int objNo,
		string dose_file_name,
		unsigned int nVoxels,
                float weight,
                float sampling_fraction,
                bool sample_with_replacement,
                bool  is_max_constraint,
                float weight_over,
                bool  is_min_constraint = false,
                float weight_under = 1);

        // Virtual destructor
        virtual ~VoxelMaxDoseObjective();

        //
        // VoxelMaxDoseObjective specific functions
        //
        float get_voxel_refdose(unsigned int voxelNo)const;

        //
        // Functions reimplemented from SeparableObjective
        //
        virtual double calculate_voxel_objective(
                float dose, unsigned int voxelNo, double nVoxels) const;
        virtual double calculate_dvoxel_objective_by_dose(
                float dose, unsigned int voxelNo, double nVoxels) const;
        virtual double calculate_d2voxel_objective_by_dose(
                float dose, unsigned int voxelNo, double nVoxels) const;

        //
        // Functions reimplemented from Objective
        //

        /// Prints a description of the objective
        virtual void printOn(std::ostream& o) const;

        /// Metaobjective support
        bool supports_meta_objective() const {return true;};

    private:
        // Default constructor not allowed
        VoxelMaxDoseObjective();

        /// Referenz dose distribution
        DoseVector reference_dose_;

        /// True if this is a maximum dose constraint
        bool is_max_constraint_;

        /// Penalty for being over the max dose (multiplied by the weight)
        float weight_over_;

        /// True if this is a minimum dose constraint
        bool is_min_constraint_;

        /// Penalty for being under the min dose (multiplied by the weight)
        float weight_under_;
};

/*
 * Inline functions
 */

inline
float VoxelMaxDoseObjective::get_voxel_refdose(unsigned int voxelNo) const
{
  return reference_dose_.get_voxel_dose(voxelNo);
}



/**
 * Calculate the contribution to the objective from one voxel with the given 
 * dose.
 *
 * @param dose The dose of the voxel.
 */
inline
double VoxelMaxDoseObjective::calculate_voxel_objective(
        float dose, unsigned int voxelNo, double nVoxels) const
{
  float refdose = get_voxel_refdose(voxelNo);

  if(is_max_constraint_ && (dose > refdose)) {
        return (dose - refdose) * (dose - refdose) 
            * weight_over_ * weight_ / nVoxels;
    }
    else if(is_min_constraint_ && (dose < refdose)) {
        return (dose - refdose) * (dose - refdose) 
            * weight_under_ * weight_ / nVoxels;
    }
    return 0;
}


/**
 * Calculate the partial derivative of the voxel contribution to the objective 
 * from one voxel with the given dose.
 *
 * @param dose The dose of the voxel.
 */
inline
double VoxelMaxDoseObjective::calculate_dvoxel_objective_by_dose(
        float dose, unsigned int voxelNo, double nVoxels) const
{
  float refdose = get_voxel_refdose(voxelNo);

    if(is_max_constraint_ && (dose > refdose)) {
        return  2 * (dose - refdose)
            * weight_over_ * weight_ / nVoxels;
    }
    else if(is_min_constraint_ && (dose < refdose)) {
        return 2 * (dose - refdose)
            * weight_under_ * weight_ / nVoxels;
    }
    return 0;
}


/**
 * Calculate the second partial derivative of the voxel contribution to the 
 * objective from one voxel with the given dose.
 *
 * @param dose The dose of the voxel.
 */
inline
double VoxelMaxDoseObjective::calculate_d2voxel_objective_by_dose(
        float dose, unsigned int voxelNo, double nVoxels) const
{
  float refdose = get_voxel_refdose(voxelNo);

    if(is_max_constraint_ && (dose > refdose)) {
        return  2 * weight_over_ * weight_ / nVoxels;
    }
    else if(is_min_constraint_ && (dose < refdose)) {
        return 2 * weight_under_ * weight_ / nVoxels;
    }
    return 0;
}

#endif
