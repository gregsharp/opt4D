/**
 * @file ExpectedQuadraticObjectiveIntegrate.hpp
 * ExpectedQuadraticObjectiveIntegrate class header
 *
 */

#ifndef EXPECTEDQUADRATICOBJECTIVEINTEGRATE_HPP
#define EXPECTEDQUADRATICOBJECTIVEINTEGRATE_HPP

class ExpectedQuadraticObjectiveIntegrate;

#include <vector>
using std::vector;
#include <string>
using std::string;

#include "DoseVector.hpp"
#include "BixelVector.hpp"
#include "DoseInfluenceMatrix.hpp"
#include "Objective.hpp"
#include "DoseDeliveryModel.hpp"



/**
 * This class is used to evaluate the expected value of the quadratic objective 
 * by integrating over different instances in each optimization step. Hence, it is useful
 * only if the number of instances is small. It can be evaluated by 
 * a sampling method.
 * 
 * The exact calculation of the gradient and the objective is implemented in
 * the uncertainty model class and may not be implemented for all models.
 *
 * The problem with this evaluation of the objective is as follows:  
 * For e.g. range uncertainty applications, we want to allow for 
 * different bixel correlation models (e.g. assuming that bixel ranges 
 * in different beams vary statistically independent. Therefore, it is not sufficient
 * to just sum over the instances.
 * Instead there are multiple functions to evaluate the gradient for 
 * different bixel correlation models. This is also the reason for using the
 * more restrictive exact dose constraint instead of the more general
 * MeanSquareErrorObjective.
 */
class ExpectedQuadraticObjectiveIntegrate : public Objective
{

public:
    /// constructor
    ExpectedQuadraticObjectiveIntegrate(Voi *the_voi, 
            unsigned int objNo,
            DoseDeliveryModel &Uncertainty,
            float desired_dose,
            float weight,
            float voxel_sampling
            );


    // Virtual destructor
    virtual ~ExpectedQuadraticObjectiveIntegrate();

    // initialize
    virtual void initialize(DoseInfluenceMatrix& Dij);

    //
    // Calculate or estimate the objective
    //
    virtual double calculate_objective(
	const BixelVector & beam_weights,
	DoseInfluenceMatrix & Dij,
	bool use_voxel_sampling,
	double &estimated_ssvo);
    virtual double calculate_objective_and_gradient(
	const BixelVector & beam_weights,
	DoseInfluenceMatrix & Dij,
	BixelVectorDirection & gradient,
        float gradient_multiplier,
	bool use_voxel_sampling,
	double &estimated_ssvo);
    virtual double calculate_objective_and_gradient_and_Hv(
	const BixelVector & beam_weights,
	DoseInfluenceMatrix & Dij,
	BixelVectorDirection & gradient,
        float gradient_multiplier,
	const vector<float> & v,
	vector<float> & Hv,
	bool use_voxel_sampling,
	double &estimated_ssvo);

    /// calculate objective based on dose vector. Does not take expectation
    virtual double calculate_dose_objective(const DoseVector & the_dose);

    /// Does the objective support RandomScenario?
    virtual bool supports_RandomScenario() const {return(false);};
    
    // print objective info
    void printOn(std::ostream& o) const;

    /// Find out if objective uses sampling
    virtual bool supports_voxel_sampling() const {return(true);};

  private:
    // Make sure default constructor is never used
    ExpectedQuadraticObjectiveIntegrate();

    /// pointer to the Volume of interest
    Voi *the_voi_;

    /// The uncertainty model
    DoseDeliveryModel *the_UncertaintyModel_;

    /// prescribed dose for targets, should be zero for OARs (does not make sence otherwise)
    float desired_dose_;

    /// voxel sampling
    float sampling_fraction_;
};


#endif
