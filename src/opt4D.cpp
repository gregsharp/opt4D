/**
 * @file opt4D.cpp
 * The main file for opt4D.
 *
 * Interprets command line arguments and performs optimization.
 *
 * $Id: opt4D.cpp,v 1.62 2008/03/13 22:06:17 bmartin Exp $
 */

/**
 * @mainpage
 *
 * \par Welcome to opt4D.
 *
 * opt4D is an optimization program for IMRT.
 * The "4D" in the name comes from the ability to optimize plans that account for a time-dependent patient geometry.
 * opt4D can handle a variety of DoseDeliveryModels in order to incorporate motion and uncertainty into the optimization
 * of an IMRT treatment plan. This includes range uncertainty in IMPT, setup errors, respiratory motion with a variable
 * Pdf.
 *
 * \par Running opt4D
 * If you wish to use opt4D, you can get command line arguments by typing \code opt4D --help \endcode at the command
 * line.  To use opt4D on a plan, you need at a minimum:
 * - A plan file (see pln_file_format.txt for information)
 * - .dij files for each beam
 * - .voi file
 * - .dif file (dimension information file)
 *
 * For example, a minimal 3 beam plan would include:
 * - dummy.pln
 * - dummy_1.dij
 * - dummy_2.dij
 * - dummy_3.dij
 * - dummy.voi
 * - dummy.dif
 *
 * Other files are used if available.
 * - .bwf Beam weight files
 * - .stf Beam steering files
 * - .pdf probability distribution file
 *
 * Some advanced features require the use of these additional files.
 *
 * \par How opt4D works
 * Opt4D has three main components.  There is the main function that handles command line arguments and organizes the
 * running of opt4D, contained in \c opt4D.cpp.  Next there is the Plan class which reads in and holds all of the
 * information needed by the optimization.  It also manages dose computation and objective or gradient calculation.
 * Finally, there is the Optimization class which contains the various optimization algorithms.
 *
 * \par The Plan class
 * Probably the most important part of opt4D is the plan class.  It holds all of the information about the plan,
 * including the dose influence matrix, the beam weights, the volumes of interest, the objectives and the dose delivery
 * model (i.e. the uncertainty and motion model).
 */

#include <iostream>
#include <iomanip>
#include <assert.h>
#include <exception>
#include <fstream>
#include <limits>
#include <tclap/CmdLine.h>
using TCLAP::CmdLine;
#include <string>
using std::string;

#include "Plan.hpp"
#include "Optimization.hpp"
#include "SteepestDescent.hpp"
#include "Dvh.hpp"
#include "AmplParser.hpp"
#include "GradientOptimizer.hpp"
#include "ExternalOptimizer.hpp"
#include "LinearProjectionOptimizer.hpp"
#include "ColumnGenerationDaoOptimizer.hpp"

using std::cout;
using std::endl;

void print_usage_message();
void print_usage_error_message();
void check_byte_size();


/**
 * Main function to interpret command line options and run program.
 */
int main(int argc, char* argv[]) {
    check_byte_size();

    // Wrap everything in a try block to catch TCLAP exceptions.
    try {
        /*
         * Defining the command line syntax
         * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
         *
         * We define the command line object, and add various switches and value arguments to it.
         * The CmdLine object parses the argv array based on the Arg objects that it contains.
         */

         /*
         * Define the command line object and insert a message that describes the program. This is printed last in
         * the help text.  The second argument is the delimiter (usually space) and the last one is the version number.
         */
         TCLAP::CmdLine cmd("Opt4D Radiation Therapy Optimization tool", ' ', "0.1");

         /*
         * TCLAP syntax
         * ^^^^^^^^^^^^
         *
         * TCLAP reads two kinds of command line parameters, value arguments and flags.  Value argments are used to
         * pass data to the program (such as file names). Flags are used to pass true or false information to the program.
         *
         * SwitchArg
         * ---------
         * Switches are used to tell the program something true or false, but must be false by default, so the default
         * behavior comes from not using any flags at all.
         *
         * SwitchArg constructor:
         *
         * TCLAP::SwitchArg::SwitchArg  ( const std::string &   flag,
         *                                const std::string &   name,
         *                                const std::string &   desc,
         *                                CmdLineInterface &    parser)
         *
         * Parameters:
         *     flag   - The one character flag that identifies this argument on the command line.
         *     name   - A one word name for the argument. Can be used as a long flag on the command line.
         *     desc   - A description of what the argument is for or does.
         *     parser - A CmdLine parser object to add this Arg to
         *
         *
         * ValueArg
         * --------
         * Value arguments can read any type data into a variable that you create.  Most arguments use this format.
         *
         * ValueArg constructor:
         *
         * template<class T>
         * TCLAP::ValueArg< T >::ValueArg (const std::string &  flag,
         *                                 const std::string &  name,
         *                                 const std::string &  desc,
         *                                 bool                 req,
         *                                 T                    value
         *                                 const std::string &  typeDesc,
         *                                 CmdLineInterface &   parser)
         *
         * Parameters:
         *     flag     - The one character flag that identifies this argument on the command line.
         *     name     - A one word name for the argument. Can be used as a long flag on the command line.
         *     desc     - A description of what the argument is for or does.
         *     req      - Whether the argument is required on the command line.
         *     value    - The default value assigned to this argument if it is not present on the command line.
         *     typeDesc - A short, human readable description of the type that this object expects. This is used in
         *                the generation of the USAGE statement. The goal is to be helpful to the end user of the program.
         *      parser  - A CmdLine parser object to add this Arg to
         *
         * UnlabledValueArg
         * ----------------
         *
         * The plan file name is read by an UnlabledValueArg, so any argument that is not processed by something else
         * will be assumed to be the plan file name.  Don't add any other UnlabledValueArg arguments!!!
         *
         * UnlabledValueArg constructor:
         *
         * template<class T>
         * TCLAP::ValueArg< T >::UnlabledValueArg (
         *                                 const std::string &  name,
         *                                 const std::string &  desc,
         *                                 bool                 req,
         *                                 T                    value
         *                                 const std::string &  typeDesc,
         *                                 CmdLineInterface &   parser)
         *
         *
         * MultiArg
         * --------
         *
         * If several values may be passed in for one argument, use a MultiArg, which returns a vector of values
         * instead of a single one.
         *
         * MultiArg syntax:
         *
         * template<class T>
         * TCLAP::MultiArg< T >::MultiArg (const std::string &  flag,
         *                                 const std::string &  name,
         *                                 const std::string &  desc,
         *                                 bool                 req,
         *                                 const std::string &  typeDesc,
         *                                 CmdLineInterface &   parser)
         *
         */

        // Add General options to cmd

        TCLAP::ValueArg<std::string> out_file_prefix_arg(
                "o", "out_file_prefix", "Prefix for written files",false, "", "string", cmd );
        TCLAP::SwitchArg discard_results_switch(
                "d","discard_results", "Don't write any files", cmd );
        TCLAP::SwitchArg write_log_switch(
                "l", "write_log", "Write log even if other results are not written", cmd );
        TCLAP::SwitchArg calculate_dose_switch(
                "c", "calculate_dose", "Just calculate the dose and quit", cmd );
        TCLAP::SwitchArg write_ampl_file_switch(
                "", "write_ampl_file", "Write voxels, bixels, Dij and vois to an AMPL .dat file and quit", cmd );
        TCLAP::SwitchArg write_dose_switch(
                "", "write_dose", "Write the final dose to a file", cmd);
        TCLAP::SwitchArg write_beam_dose_switch(
                "", "write_beam_dose", "Write the dose from each beam to a file", cmd );
        TCLAP::SwitchArg write_aux_dose_switch(
                "", "write_aux_dose", "Write the dose from each auxiliary to a file", cmd );
        TCLAP::SwitchArg write_inst_dose_switch(
                "", "write_inst_dose", "Write the dose for each instance to a file", cmd );
        TCLAP::SwitchArg write_ct_dose_switch(
                "", "write_ct_dose", "Write selected dose distributions in CT resolution to a file", cmd);
        TCLAP::SwitchArg write_obj_samples_switch(
                "", "write_obj_samples", "Write samples of the objective value to a file", cmd );
        TCLAP::ValueArg<int> random_seed_arg(
                "r", "random_seed","Random seed to use",false, 0, "integer", cmd );
        TCLAP::ValueArg<int> repeat_arg(
                "R", "repeat", "Number of times to repeat optimization", false, 1,"integer", cmd );
        TCLAP::ValueArg<string> initial_parameter_root_arg(
                "b", "initial_parameter_root", "Beam files to read initial parameters from", false, "", "string", cmd );
        TCLAP::SwitchArg zero_initial_fluence_arg(
                "0", "zero_initial_fluence", "Start optimization with zero fluence", cmd );
        TCLAP::SwitchArg unit_initial_fluence_arg(
                "", "unit_initial_fluence", "Start optimization with unit fluence", cmd );
        TCLAP::ValueArg<float> initial_fluence_noise_arg(
                "", "initial_fluence_noise", "Add noise to initial fluence", false, 1,
                "float in > 0", cmd);
        TCLAP::ValueArg<string> ampl_voi_list_arg(
                "", "ampl_voi_list", "list of voi numbers to include in ampl file", false, "", "string", cmd );
        TCLAP::ValueArg<string> ampl_voi_sampling_arg(
                "", "ampl_voi_sampling", "voxel sampling fraction for each VOI included in ampl file",
                false, "", "string", cmd);


        // Add Plan options to cmd

        TCLAP::MultiArg<std::string> replacement_string_args(
                "#", "replacement_string", "Replacement for string in plan file", false, "string", cmd );
        TCLAP::SwitchArg static_beam_switch(
                "", "static_beam", "Use the same beams weights for all instances", cmd );
        TCLAP::SwitchArg read_beamlets_first_switch(
                "", "read_beamlets_first", "Read the bixel vector prior to the Dij files", cmd );
        TCLAP::SwitchArg read_auxiliary_dij_switch(
                "", "read_auxiliary_dij", "Read multiple Dij files is auxiliaries", cmd );
        TCLAP::ValueArg<float> uniform_sampling_arg(
                "", "uniform_sampling", "Set the sampling fraction the same for every VOI", false, 1,
                "float in (0,1]", cmd);
        TCLAP::ValueArg<float> uniform_samples_per_obj_arg(
                "", "uniform_samples_per_obj",
                "Set the sampling fraction for every objective such that the given number of voxels are sampled in each",
                false, 1, "float > 0", cmd);
        TCLAP::SwitchArg dont_transpose_switch(
                "","dont_transpose", "Don't transpose the Dij matrix until necessary", cmd );
        TCLAP::ValueArg<int> nObjective_samples_arg(
                "", "nObjective_samples","number of objective value samples generated after the optimization",
                false, 100,"integer",cmd);


        // Add Optimization options to cmd
        TCLAP::ValueArg<unsigned int> solver_change_iter_arg(
                "", "solver_change_iter", "Iteration when we should switch solvers", false,
                std::numeric_limits<unsigned int>::max(), "index in [1,nIterations]", cmd );

        TCLAP::ValueArg<int> verbosity_arg(
                "V", "verbosity", "How verbose to be durint the optimization", false, 1, "integer", cmd );
        TCLAP::ValueArg<double> max_time_arg(
                "", "max_time", "Maximum time for optimization", false, 600, "seconds", cmd );
        TCLAP::ValueArg<int> max_steps_arg(
                "", "max_steps", "Maximum number of steps for optimization", false, 100, "integer", cmd );
        TCLAP::ValueArg<int> min_steps_arg(
                "", "min_steps", "Minimum number of steps for optimization", false, 2, "integer", cmd );
        TCLAP::ValueArg<double> stop_fract_arg(
                "", "stop_fract", "Minimum fractional improvement per step (0 to ignore) default 0", false, 0,
                "fraction", cmd );
        TCLAP::MultiArg<Optimization::event> add_step_event_arg(
                "", "add_step_event", "Add comma seperated event description.  Do not add any spaces",
                false, "EVENT,start stepNo,recurrence steps", cmd );
        TCLAP::MultiArg<Optimization::event> add_time_event_arg(
                "", "add_time_event", "Add comma seperated event description.  Do not add any spaces",
                false, "EVENT,start time,recurrence time", cmd );
        TCLAP::MultiArg<OptimizationEventType> add_final_event_arg(
                "", "add_final_event", "Add event type to the end of the optimization", false, "EVENT", cmd );
        TCLAP::ValueArg<double> step_size_multiplier_arg(
                "", "step_size_multiplier", "Multiplies the default stepsize", false, 1, "multiplier", cmd );
        TCLAP::SwitchArg optimal_steps_switch(
                "", "optimal_steps", "Use a line_search to find the optimal step size at each iteration", cmd );
        TCLAP::SwitchArg strong_wolfe_switch(
                "", "strong_wolfe", "find step size that satisfies the strong wolfe conditions", cmd );
        TCLAP::ValueArg<int> linesearch_steps_arg(
                "", "linesearch_steps", "How many steps to take in the linesearch (default 3)", false, 3,
                "integer", cmd );
        TCLAP::SwitchArg alternate_linesearch_switch(
                "", "alternate_linesearch", "Use a use cubic line search algorithrm (not reccomended)", cmd );
        TCLAP::SwitchArg diminishing_steps_switch(
                "", "diminishing_steps", "Use diminishing step size", cmd );
        TCLAP::SwitchArg write_parameter_history_switch(
                "", "write_parameter_history", "Write paramwter files on each step during optimization", cmd );
        TCLAP::SwitchArg write_dose_history_switch(
                "", "write_dose_history", "Write dose file on each step during optimization", cmd );
        TCLAP::SwitchArg write_dvh_history_switch(
                "", "write_dvh_history", "Write DVH file on each step during optimization", cmd );
        TCLAP::SwitchArg voxel_sampling_switch(
                "s", "voxel_sampling", "Use voxel sampling during optimization", cmd );
        TCLAP::SwitchArg scenario_sampling_switch(
                "", "scenario_sampling", "use scenario sampling for uncertainty handling", cmd );
        TCLAP::MultiArg<unsigned int> instances_per_step_arg( "I", "instances_per_step",
                "Number of Instance samples per optimization step.  Use once per meta-objective.",
                false, "int > 0", cmd );
        TCLAP::SwitchArg calc_true_obj_switch(
                "t", "calculate_true_objective", "Calculate the true objective at each step", cmd );
        TCLAP::SwitchArg calc_directional_derivative_switch(
                "", "calculate_directional_derivative", "Calculate the directional derivative at each step", cmd );

		// constraint handling options
        TCLAP::SwitchArg dont_project_on_bounds_switch(
                "", "dont_project_on_bounds", "Do not project on parameter bound constraints after each step", cmd );
        TCLAP::ValueArg<double> constraint_penalty_multiplier_arg(
                "", "constraint_penalty_multiplier", "Factor by which penalties for constraints are increased", false, 2.0, "float > 1", cmd );
        TCLAP::SwitchArg use_variable_transformation_switch(
                "", "use_variable_transformation", "handle positivity constraint via variable transformation", cmd );

        // Adaptive sampling options
        TCLAP::SwitchArg adaptive_sampling_switch(
                "a", "adaptive_sampling", "Use adaptive sampling during optimization", cmd );
        TCLAP::ValueArg<int> as_update_period_arg(
                "", "as_update_period", "Adaptive sampling update period", false, 1, "int > 0", cmd );
        TCLAP::ValueArg<float> as_sampling_fraction_arg(
                "", "as_sampling_fraction", "Adaptive sampling overall sampling fraction", false, 0.1,
                "float in (0,1]", cmd );
        TCLAP::ValueArg<float> as_min_sampling_fraction_arg(
                "", "as_min_sampling_fraction",
                "Adaptive sampling minimum sampling fraction.  0 implies same as in plan file.", false, 0,
                "float in [0,1]", cmd );
        TCLAP::ValueArg<float> as_smoothing_factor_arg(
                "", "as_smoothing_factor", "Adaptive sampling ssvo smoothing factor", false, 0.5,
                "float in [0,1)", cmd);

        // Options for steepest descent
        TCLAP::SwitchArg steepest_descent_switch(
                "", "steepest_descent", " optimize by steepest descent", cmd );
        // Options for diagonal-newton
        TCLAP::SwitchArg diagonal_newton_switch(
                "", "diagonal_newton", " optimize by diagonalized Newton descent", cmd );
        // Options for L-BFGS
        TCLAP::SwitchArg lbfgs_switch(
                "", "lbfgs", " optimize by limited memory BFGS quasi-newton method", cmd );
        TCLAP::ValueArg<unsigned int> lbfgs_m_arg(
                "", "lbfgs_m", "number of past steps to approximate the hessian from", false, 10, "integer > 0", cmd );
        // Options for conjugate gradient
        TCLAP::SwitchArg conjugate_gradient_switch(
                "", "conjugate_gradient", " optimize by conjugate gradient", cmd );
        // Options for delta-bar-delta
        TCLAP::SwitchArg delta_bar_delta_switch(
                "", "delta_bar_delta", " optimize by delta-bar-delta", cmd );
        TCLAP::ValueArg<float> dbd_r_arg(
                "", "dbd_r", "Delta-bar-delta learning rate increment", false, 0.1, "float > 0", cmd );
        TCLAP::ValueArg<float> dbd_phi_arg(
                "", "dbd_phi", "Delta-bar-delta learning_rate_decrement", false, 0.9, "float in [0,1)", cmd );
        TCLAP::ValueArg<double> dbd_theta_arg(
                "", "dbd_theta", "Delta-bar-delta gradient smoothing exponent", false, 0.9, "float in [0,1)", cmd );
        // Options for stochastic meta descent
        TCLAP::SwitchArg stochastic_meta_descent_switch(
                "", "stochastic_meta_descent", " optimize by stochastic-meta-descent", cmd );
        TCLAP::ValueArg<double> smd_smoothing_coefficient_arg(
                "", "smd_smoothing_coefficient", "SMD gradient smoothing exponent", false, 0.95, "float in [0,1)", cmd);
        TCLAP::ValueArg<double> smd_meta_step_size_arg(
                "", "smd_meta_step_size", "SMD meta step-size", false, 0.01, "float > 0", cmd );
        TCLAP::SwitchArg calc_constrained_grad_switch(
                "", "calculate_constrained_gradient", "Technical detail of SMD", cmd );
        TCLAP::SwitchArg calc_feasible_grad_switch(
                "", "calculate_feasible_gradient", "Technical detail of SMD", cmd );
        // Options for linear projection solver
        TCLAP::SwitchArg linear_projection_solver_switch(
                "", "linear_projection_solver", "optimize by projecting onto constraints", cmd );
        TCLAP::ValueArg<float> lps_epsilon_arg(
                "", "lps_epsilon", "Epsilon optimality value for linear projection solver",
                false, 0.1, "float > 0", cmd );
        TCLAP::ValueArg<unsigned int> lps_maxmin_arg(
                "", "lps_maxmin", "maximize the min dose for the constraint given", false, 1,
                "index in [1,nConstraints]", cmd );
        TCLAP::ValueArg<unsigned int> lps_minmax_arg(
                "", "lps_minmax", "minimize the max dose for the constraint given", false, 1,
                "index in [1,nConstraints]", cmd );
        TCLAP::SwitchArg mosek_switch( "", "mosek", "optimize by calling Mosek", cmd );


        TCLAP::SwitchArg optpp_switch(
                "", "optpp", "optimize using opt++ package", cmd );

        TCLAP::SwitchArg cg_dao_switch(
                "", "cg_dao", "column generation direct aperture optimization", cmd );
        TCLAP::ValueArg<unsigned int> max_apertures_arg(
				"", "max_apertures", "maximum number of apertures to be generated", false, 100, "int > 0", cmd);

        // Options for Direct Aperture Optimization
        TCLAP::SwitchArg dao_solver_switch(
                "", "direct_aperture", "optimize aperture parameters directly", cmd );
        TCLAP::ValueArg<unsigned int> dao_trans_iter_arg(
                "", "dao_translate_on", "Iteration when we should switch to direct aperture optimization", false, 1,
                "index in [1,nIterations]", cmd );
        TCLAP::ValueArg<std::string> dao_machine_file_arg( "", "dao_machine_file",
                "Filename of machine model file describing machine capabilities and constraints",
                false, "", "filename", cmd );
        TCLAP::SwitchArg dao_optMlc_arg( "", "dao_NoMlc", "optimize MLC leaf position directly", cmd );
        TCLAP::SwitchArg dao_optJaw_arg( "", "dao_NoJaw", "optimize collimator jaw position directly", cmd );

        // Options for Direct Aperture Optimization Sequencer
        TCLAP::SwitchArg dao_seq_dummyVmat_switch(
                "", "dao_seq_dummyVmat", "translate bixel maps to apertures with dummy vmat sequencer", cmd );
        TCLAP::ValueArg<unsigned int> dao_seq_nFields_arg(
                "", "dao_nFields", "Fixed number of fields or arcs in plan", false, 0, "[1,nDijBeams]", cmd );
        TCLAP::ValueArg<unsigned int> dao_seq_cpPerFd_arg(
                "", "dao_cpPerAngle", "Number of segments per field or arc control points per dij beam",
                false, 1, "[1,nDijBeams]", cmd );

        // Options for momentum
        TCLAP::SwitchArg momentum_switch( "", "momentum", "optimize by momentum", cmd );
        TCLAP::ValueArg<float> momentum_smoothing_factor_arg(
                "", "momentum_smoothing_factor", "Momentum gradient smoothing factor",
                false, 0.7, "float in [0,1)", cmd );


        // Estimate true objective options

        TCLAP::SwitchArg dont_estimate_sample_voxels_switch(
                "", "dont_estimate_sample_voxels", "Don't Sample voxels while estimating true objective.", cmd );
        TCLAP::ValueArg<int> estimate_max_samples_arg(
                "", "estimate_max_samples", "Max scenario samples to use when estimating true objective.", false,
                100, "int > 0", cmd );
        TCLAP::ValueArg<int> estimate_min_samples_arg(
                "", "estimate_min_samples", "Min scenario samples to use when estimating true objective.", false,
                10, "int > 0", cmd );
        TCLAP::ValueArg<float> estimate_max_uncertainty_arg(
                "", "estimate_max_uncertainty", "Max uncertainty to allow when estimating true objective.", false,
                0.01, "float in (0,1]", cmd );
        TCLAP::ValueArg<int> expected_dose_nScenarios_arg(
                "", "expected_dose_nScenarios", "Number of scenarios to calculate whan calculating the expected dose",
                false, 100, "int > 0", cmd );

        /*
         * Parse cmd
         */

        // The plan file name is the only unlabled argument. It must come last before parsing.
        TCLAP::UnlabeledValueArg<std::string> pln_file_arg( "file", "Plan file name", true, "", "pln file name", cmd );

        // Parse the command line
        cmd.parse(argc, argv);

        string out_file_prefix = out_file_prefix_arg.getValue();

        // Initialize random number generator
        if (random_seed_arg.isSet()) {
            // Specific random seed passed in
            srand(random_seed_arg.getValue());
        } else {
            // No seed passed in, so make it random based on the time
            srand(time(0));
        }

        /*
         * Set various plan options
         */

        // Start with default Plan options
        Plan::options plan_options;

        plan_options.transpose_when_reading_Dij_ = !dont_transpose_switch.getValue();

        plan_options.static_beam_ = static_beam_switch.getValue();

        plan_options.read_beamlets_first_ = read_beamlets_first_switch.getValue();

        plan_options.read_auxiliary_dij_ = read_auxiliary_dij_switch.getValue();

        if (uniform_sampling_arg.isSet()) {
            plan_options.use_uniform_sampling_ = true;
            plan_options.uniform_sampling_fraction_ = uniform_sampling_arg.getValue();
        } else {
            plan_options.use_uniform_sampling_ = false;
        }

        if (uniform_samples_per_obj_arg.isSet()) {
            plan_options.use_uniform_samples_per_obj_ = true;
            plan_options.samples_per_obj_ = uniform_samples_per_obj_arg.getValue();
        } else {
            plan_options.use_uniform_samples_per_obj_ = false;
        }

        if (nObjective_samples_arg.isSet()) {
            plan_options.nObjective_samples_ = nObjective_samples_arg.getValue();
        }

        // Replacement strings
        plan_options.param_values_ = replacement_string_args.getValue();
        for ( unsigned int i = 0; i < plan_options.param_values_.size(); i++ ) {
            plan_options.param_numbers_.push_back(i+1);
        }

        /*
         * Initialize the plan
         */
        std::auto_ptr<Plan> the_plan;

        cout << "Allocating plan" << std::endl;
        try {
            // Read plan
            the_plan.reset(new Plan(pln_file_arg.getValue(), plan_options));
        } catch (std::exception & e) {
            cout << "Standard exception: " << e.what() << std::endl;
            throw;
        }

        /*
         * Zero parameter intensities if needed
         */
        if (zero_initial_fluence_arg.isSet()) {
            the_plan->set_zero_intensities();
        }

        /*
         * unit parameter intensities if needed
         */
        if (unit_initial_fluence_arg.isSet()) {
            the_plan->set_unit_intensities();
        }

         /*
         * Read parameter files if needed
         */
        if (initial_parameter_root_arg.isSet()) {
            string parameter_root = initial_parameter_root_arg.getValue();
            the_plan->load_parameter_files(parameter_root);
        }

        /*
         * add noise
         */
        if (initial_fluence_noise_arg.isSet()) {
		  cout << "adding noise to initial fluence" << endl;
		  the_plan->add_noise_to_intensities(initial_fluence_noise_arg.getValue());
        }


       /*
         * Calculate dose or optimize plan
         */
        if (calculate_dose_switch.getValue()) {
            // Just calculate the dose
            // cout << "Max beamlet intensity = " << the_plan->get_max_intensity() << std::endl;

            // Write out files if desired
            if (!discard_results_switch.getValue()) {
                string temp_prefix = out_file_prefix;

                // Write out beam weights
                the_plan->write_parameter_files(temp_prefix + string("beam"));

                if (write_dose_switch.getValue()) {
                    // Write dose and DVH
                    the_plan->write_dose(temp_prefix,write_ct_dose_switch.getValue());
                } else {
                    // Just write the DVH
                    the_plan->write_DVH(temp_prefix + "DVH.dat");
                }

                // Write dose for specific scenarios
                the_plan->write_scenario_dose(temp_prefix);

                if (write_beam_dose_switch.getValue()) {
                    // Write dose from each beam
                    the_plan->write_beam_dose(temp_prefix);
                }

                if (write_aux_dose_switch.getValue()) {
                    // Write dose and dvh from each auxilary
                    the_plan->write_auxiliary_dose(temp_prefix);
                }

                if (write_inst_dose_switch.getValue()) {
                    // Write dose and dvh from each instance
                    the_plan->write_instance_dose(temp_prefix);
                }

                // write objective samples
                if (write_obj_samples_switch.getValue()) {
                    the_plan->write_objective_samples(temp_prefix);
                }

            }
        } else if (write_ampl_file_switch.getValue()) {
            // write voi, voxel, bixel, Dij information to a ampl dat file

            // result file prefix
            string temp_prefix = out_file_prefix;

            // get voi list if provided
            string ampl_voi_list = "";
            if (ampl_voi_list_arg.isSet()) {
                ampl_voi_list = ampl_voi_list_arg.getValue();
            }

            // check if voxel sampling is used
            bool use_voxel_sampling_in_ampl = false;
            if (voxel_sampling_switch.getValue()) {
                use_voxel_sampling_in_ampl = true;
            }

            // get voi sampling list if provided
            string ampl_voi_sampling = "";
            if (ampl_voi_sampling_arg.isSet()) {
                ampl_voi_sampling = ampl_voi_sampling_arg.getValue();
            }

            // create Ampl parser
            AmplParser ampl_parser(the_plan.get(),ampl_voi_list,use_voxel_sampling_in_ampl, ampl_voi_sampling);

            // write Ampl files
            ampl_parser.write_dat_file(out_file_prefix);
        } else {
            // Optimize plan

            /*
             * Set various Optimization options
             */

            // Start with default optimization options
            Optimization::options opt_options;

            // Override with the values passed in on the command line
            opt_options.verbosity_ = verbosity_arg.getValue();

            if (calc_directional_derivative_switch.getValue()) {
                opt_options.step_events_.push_back( Optimization::event( CALCULATE_DIRECTIONAL_DERIVATIVE, 0, 1 ) );
            }

            // Create events to write out files if desired
            if (!discard_results_switch.getValue()) {
                // Write out final parameter files
                opt_options.final_events_.push_back(WRITE_PARAMETER_FILES);

                // Write out final dvh file
                opt_options.final_events_.push_back(WRITE_DVH);

                if (write_dose_switch.getValue()) {
                    // Write final dose
                    if (write_ct_dose_switch.getValue()) {
                        // Write it to the ct grid
                        opt_options.final_events_.push_back(WRITE_CT_DOSE);
                    } else {
                        // Write it normally
                        opt_options.final_events_.push_back(WRITE_DOSE);
                    }
                }

                // Write dose from each beam
                if (write_beam_dose_switch.getValue()) {
                    opt_options.final_events_.push_back(WRITE_BEAM_DOSE);
                }

                // Write dose for specific scenarios
                opt_options.final_events_.push_back(WRITE_SCENARIO_DOSE);

                // Write dose and dvh from each auxilary
                if (write_aux_dose_switch.getValue()) {
                    opt_options.final_events_.push_back(WRITE_AUX_DOSE);
                }

                // Write dose and dvh from each instance
                if (write_inst_dose_switch.getValue()) {
                    opt_options.final_events_.push_back(WRITE_INST_DOSE);
                }

                // write variance
                opt_options.final_events_.push_back(WRITE_VARIANCE);

                // write objective samples
                if (write_obj_samples_switch.getValue()) {
                    opt_options.final_events_.push_back(WRITE_OBJ_SAMPLES);
                }

                // save parameter history
                if (write_parameter_history_switch.getValue()) {
                    // Create a recurring event
                    opt_options.step_events_.push_back( Optimization::event(WRITE_PARAMETER_FILES, 0, 1) );
                }

                // save dose history
                if (write_dose_history_switch.getValue()) {
                    // Create a recurring event
                    opt_options.step_events_.push_back( Optimization::event(WRITE_DOSE, 0, 1) );
                }

                // save dvh history
                if (write_dvh_history_switch.getValue()) {
                    // Create a recurring event
                    opt_options.step_events_.push_back(
                        Optimization::event(WRITE_DVH, 0, 1));
                }
            }

            // If two optimizers are specified add event to switch optimizer method

            // Count number of specified optimizer classes and record poistion in argument string
            unsigned int solverCount = 0;
            size_t solver_minArgPos = std::numeric_limits<std::size_t>::max();
            // One element for each of the 11 possible optimization types
            vector<size_t> solver_argPos(11,solver_minArgPos);
            if ( steepest_descent_switch.getValue() ) {
                ++solverCount;
                solver_argPos[0] = string(*argv).find_first_of("--steepest_descent");
                solver_minArgPos = std::min( solver_minArgPos, solver_argPos[0] );
            }
            if ( diagonal_newton_switch.getValue() ) {
                ++solverCount;
                solver_argPos[1] = string(*argv).find_first_of("--diagonal_newton");
                solver_minArgPos = std::min( solver_minArgPos, solver_argPos[1] );
            }
            if ( lbfgs_switch.getValue() ) {
                ++solverCount;
                solver_argPos[2] = string(*argv).find_first_of("--lbfgs");
                solver_minArgPos = std::min( solver_minArgPos, solver_argPos[2] );
            }
            if ( conjugate_gradient_switch.getValue() ) {
                ++solverCount;
                solver_argPos[3] = string(*argv).find_first_of("--conjugate_gradient");
                solver_minArgPos = std::min( solver_minArgPos, solver_argPos[3] );
            }
            if ( delta_bar_delta_switch.getValue() ) {
                ++solverCount;
                solver_argPos[4] = string(*argv).find_first_of("--delta_bar_delta");
                solver_minArgPos = std::min( solver_minArgPos, solver_argPos[4] );
            }
            if ( stochastic_meta_descent_switch.getValue() ) {
                ++solverCount;
                solver_argPos[5] = string(*argv).find_first_of("--stochastic_meta_descent");
                solver_minArgPos = std::min( solver_minArgPos, solver_argPos[5] );
            }
            if ( momentum_switch.getValue() ) {
                ++solverCount;
                solver_argPos[6] = string(*argv).find_first_of("--momentum");
                solver_minArgPos = std::min( solver_minArgPos, solver_argPos[6] );
            }
            if ( linear_projection_solver_switch.getValue() ) {
                ++solverCount;
                solver_argPos[7] = string(*argv).find_first_of("--linear_projection_solver");
                solver_minArgPos = std::min( solver_minArgPos, solver_argPos[7] );
            }
            if ( mosek_switch.getValue() ) {
                ++solverCount;
                solver_argPos[8] = string(*argv).find_first_of("--mosek");
                solver_minArgPos = std::min( solver_minArgPos, solver_argPos[8] );
            }
            if ( optpp_switch.getValue() ) {
                ++solverCount;
                solver_argPos[9] = string(*argv).find_first_of("--optpp");
                solver_minArgPos = std::min( solver_minArgPos, solver_argPos[9] );
            }
            if ( cg_dao_switch.getValue() ) {
                ++solverCount;
                solver_argPos[10] = string(*argv).find_first_of("--cg_dao");
                solver_minArgPos = std::min( solver_minArgPos, solver_argPos[10] );
            }
            solverCount += ( solverCount == 0 ) ? 1 : 0;
            assert( solverCount == 1 || solverCount == 2 );

            // Create a once off event to switch optimizer type
            if ( solverCount == 2 ) {
                unsigned int solver_change;
                if ( solver_change_iter_arg.isSet() ) {
                    solver_change = solver_change_iter_arg.getValue();
                } else if ( dao_trans_iter_arg.isSet() ) {
                    solver_change = dao_trans_iter_arg.getValue();
                } else {
                    solver_change = max_steps_arg.getValue() / 2;
                }
                opt_options.step_events_.push_back( Optimization::event(SWITCH_OPTIMIZER, solver_change, 0) );
            }

            // Create a once off event to convert the bixel map to apertures
            if ( dao_solver_switch.getValue() ) {
                unsigned int dao_translate;
                if ( dao_trans_iter_arg.isSet() ) {
                    dao_translate = dao_trans_iter_arg.getValue();
                } else if ( solver_change_iter_arg.isSet() ) {
                    dao_translate = solver_change_iter_arg.getValue();
                } else {
                    dao_translate = dao_trans_iter_arg.getValue();
                }
                opt_options.step_events_.push_back( Optimization::event(SEQUENCE_APERTURES, dao_translate, 0) );
            }

            // Stopping criteria options
            opt_options.max_time_ = max_time_arg.getValue();
            opt_options.max_steps_ = max_steps_arg.getValue();
            opt_options.min_steps_ = min_steps_arg.getValue();
            opt_options.stopping_fraction_ = stop_fract_arg.getValue();
            opt_options.max_apertures_ = max_apertures_arg.getValue();

            // Add events
            for (unsigned int i = 0; i < add_step_event_arg.getValue().size(); i++) {
                opt_options.step_events_.push_back( add_step_event_arg.getValue().at(i) );
            }

            for (unsigned int i = 0; i < add_time_event_arg.getValue().size(); i++) {
                opt_options.time_events_.push_back( add_time_event_arg.getValue().at(i) );
            }

            for (unsigned int i = 0; i < add_final_event_arg.getValue().size(); i++) {
                opt_options.final_events_.push_back( add_final_event_arg.getValue().at(i) );
            }

            // handling of constraints
            if(dont_project_on_bounds_switch.getValue()) {
              opt_options.project_on_bound_constraints_ = false;
            }
            if(use_variable_transformation_switch.getValue()) {
              opt_options.use_variable_transformation_ = true;
            }
			opt_options.constraint_penalty_multiplier_ = constraint_penalty_multiplier_arg.getValue();


            // Step size options
            opt_options.step_size_multiplier_ = step_size_multiplier_arg.getValue();
            opt_options.use_optimal_steps_ = optimal_steps_switch.getValue();
            opt_options.use_strong_wolfe_ = strong_wolfe_switch.getValue();
            opt_options.linesearch_steps_ = linesearch_steps_arg.getValue();
            opt_options.use_alternate_linesearch_ = alternate_linesearch_switch.getValue();
            opt_options.use_diminishing_step_sizes_ = diminishing_steps_switch.getValue();

            // Sampling options
            opt_options.use_voxel_sampling_ = voxel_sampling_switch.getValue();
            opt_options.use_scenario_sampling_ = scenario_sampling_switch.getValue();
            opt_options.nScenario_samples_per_step_ = instances_per_step_arg.getValue();
            opt_options.use_adaptive_sampling_ = adaptive_sampling_switch.getValue();
            opt_options.as_update_period_ = as_update_period_arg.getValue();
            opt_options.as_sampling_fraction_ = as_sampling_fraction_arg.getValue();
            opt_options.as_min_sampling_fraction_ = as_min_sampling_fraction_arg.getValue();
            opt_options.as_smoothing_factor_ = as_smoothing_factor_arg.getValue();
            opt_options.calculate_true_objective_ = calc_true_obj_switch.getValue();
            if (calc_true_obj_switch.getValue()) {
                opt_options.step_events_.push_back( Optimization::event(CALCULATE_TRUE_OBJECTIVE, 0, 1) );
            }

            opt_options.calculate_constrained_gradient_ = calc_constrained_grad_switch.getValue();

            opt_options.calculate_feasible_gradient_ = calc_feasible_grad_switch.getValue();

            // Steepest descent options
            {
                SteepestDescent::options options;
                options.step_size_ = 1;
                opt_options.steepest_descent_options_ = options;
            }

            //
            // Estimate true objective options
            //
            if (dont_estimate_sample_voxels_switch.isSet()) {
                opt_options.estimate_sample_voxels_ = !dont_estimate_sample_voxels_switch.getValue();
            }
            if (estimate_max_samples_arg.isSet()) {
                opt_options.estimate_max_samples_ = estimate_max_samples_arg.getValue();
            }
            if (estimate_min_samples_arg.isSet()) {
                opt_options.estimate_min_samples_ = estimate_min_samples_arg.getValue();
            }
            if (estimate_max_uncertainty_arg.isSet()) {
                opt_options.estimate_max_uncertainty_ = estimate_max_uncertainty_arg.getValue();
            }
            if (expected_dose_nScenarios_arg.isSet()) {
                opt_options.expected_dose_nScenarios_ = expected_dose_nScenarios_arg.getValue();
            }

            // Determine optimization type

            opt_options.optimization_type_.assign( solverCount, STEEPEST_DESCENT );
            if (steepest_descent_switch.getValue()) {
                if ( solver_argPos[0] == solver_minArgPos ) {
                    opt_options.optimization_type_[0] = STEEPEST_DESCENT;
                } else {
                    opt_options.optimization_type_[1] = STEEPEST_DESCENT;
                }
            }
            if (diagonal_newton_switch.getValue()) {
                if ( solver_argPos[1] == solver_minArgPos ) {
                    opt_options.optimization_type_[0] = DIAGONAL_NEWTON;
                } else {
                    opt_options.optimization_type_[1] = DIAGONAL_NEWTON;
                }
            }
            if (lbfgs_switch.getValue()) {
                if ( solver_argPos[2] == solver_minArgPos ) {
                    opt_options.optimization_type_[0] = LBFGS;
                } else {
                    opt_options.optimization_type_[1] = LBFGS;
                }
                opt_options.lbfgs_options_.nHistory_ = lbfgs_m_arg.getValue();
            }
            if (conjugate_gradient_switch.getValue()) {
                if ( solver_argPos[3] == solver_minArgPos ) {
                    opt_options.optimization_type_[0] = CONJUGATE_GRADIENT;
                } else {
                    opt_options.optimization_type_[1] = CONJUGATE_GRADIENT;
                }
            }
            if (delta_bar_delta_switch.getValue()) {
                if ( solver_argPos[4] == solver_minArgPos ) {
                    opt_options.optimization_type_[0] = DELTA_BAR_DELTA;
                } else {
                    opt_options.optimization_type_[1] = DELTA_BAR_DELTA;
                }
                opt_options.delta_bar_delta_options_.r_ = dbd_r_arg.getValue();
                opt_options.delta_bar_delta_options_.phi_ = dbd_phi_arg.getValue();
                opt_options.delta_bar_delta_options_.theta_ = dbd_theta_arg.getValue();
            }
            if (stochastic_meta_descent_switch.getValue()) {
                if ( solver_argPos[5] == solver_minArgPos ) {
                    opt_options.optimization_type_[0] = STOCHASTIC_META_DESCENT;
                } else {
                    opt_options.optimization_type_[1] = STOCHASTIC_META_DESCENT;
                }
                opt_options.smd_smoothing_coefficient_ = smd_smoothing_coefficient_arg.getValue();
                opt_options.smd_meta_step_size_ = smd_meta_step_size_arg.getValue();
            }
            if (momentum_switch.getValue()) {
                if ( solver_argPos[6] == solver_minArgPos ) {
                    opt_options.optimization_type_[0] = MOMENTUM;
                } else {
                    opt_options.optimization_type_[1] = MOMENTUM;
                }
                opt_options.momentum_gradient_smoothing_exponent_ = momentum_smoothing_factor_arg.getValue();
            }
            if (linear_projection_solver_switch.getValue()) {
                if ( solver_argPos[7] == solver_minArgPos ) {
                    opt_options.optimization_type_[0] = LINEAR_PROJECTION_SOLVER;
                } else {
                    opt_options.optimization_type_[1] = LINEAR_PROJECTION_SOLVER;
                }
                opt_options.lps_epsilon_ = lps_epsilon_arg.getValue();
                if(lps_maxmin_arg.isSet()) {
                  opt_options.lps_maxmin_ = true;
                  opt_options.lps_objNo_ = lps_maxmin_arg.getValue();
                }
                if(lps_minmax_arg.isSet()) {
                  opt_options.lps_minmax_ = true;
                  opt_options.lps_objNo_ = lps_minmax_arg.getValue();
                }
            }
            if (mosek_switch.getValue()) {
                if ( solver_argPos[8] == solver_minArgPos ) {
                    opt_options.optimization_type_[0] = MOSEK;
                } else {
                    opt_options.optimization_type_[1] = MOSEK;
                }
            }
            if (optpp_switch.getValue()) {
			  if ( solver_argPos[9] == solver_minArgPos ) {
                opt_options.optimization_type_[0] = OPT_PP;
			  } else {
				opt_options.optimization_type_[1] = OPT_PP;
			  }
			}
            if (cg_dao_switch.getValue()) {
			  if ( solver_argPos[10] == solver_minArgPos ) {
                opt_options.optimization_type_[0] = DAO;
			  } else {
				opt_options.optimization_type_[1] = DAO;
			  }
			}
			
            // Add direct aperture options if appropriate
            if ( dao_solver_switch.getValue() ) {
                opt_options.daoOpt_ = true;
                ApertureSequencer::options options;

                if ( dao_seq_dummyVmat_switch.getValue() ) {
                    options.seqType_ = VMAT_DUMMY;
                } else { // Add options for other sequencers here
                    options.seqType_ = VMAT_DUMMY;
                }

                options.machine_file_ = dao_machine_file_arg.getValue();
                options.optMlc_ = ! dao_optMlc_arg.getValue();
                options.optJaw_ = ! dao_optJaw_arg.getValue();
                options.nFields_ = dao_seq_nFields_arg.getValue();
                options.cpPerBeam_ = dao_seq_cpPerFd_arg.getValue();
                opt_options.sequencer_options_ = options;
            }

            // Figure out the log file name
            string log_file_name;
            if (discard_results_switch.getValue() && !write_log_switch.getValue()) {
                log_file_name = "/dev/null";
            } else {
                log_file_name = out_file_prefix + "history.xml";
            }

			opt_options.out_file_prefix_ = out_file_prefix;

            // Create the Optimization object
            std::auto_ptr<Optimization> planopt;

			switch ( opt_options.optimization_type_[0] ) 
			  {

			  // gradient type fluence map optimization
			  case STEEPEST_DESCENT:
			  case LBFGS:
			  case CONJUGATE_GRADIENT:
			  case DELTA_BAR_DELTA:
				planopt.reset(new GradientOptimizer(the_plan.get(), the_plan->get_dose_influence_matrix(), 
													the_plan->get_pointer_to_parameter_vector(), log_file_name, opt_options));
				break;
				
			  // projection solver fluence map optimization
			  case LINEAR_PROJECTION_SOLVER:
				planopt.reset(new LinearProjectionOptimizer(the_plan.get(), log_file_name, opt_options));
				
				break;

			  // external solver fluence map optimization
			  case MOSEK:
			  case OPT_PP:
				planopt.reset(new ExternalOptimizer(the_plan.get(), log_file_name, opt_options));
				
				break;

			  case DAO:
				planopt.reset(new ColumnGenerationDaoOptimizer(the_plan.get(), log_file_name, opt_options));

				break;

			default:
			  cout << "Optimization type " << opt_options.optimization_type_[0] << " is unknown" << endl;
			  throw( std::runtime_error( "unknown optimization type"));

			}

            // Find out how many times to run
            unsigned int repeat_count = repeat_arg.getValue();
            if (repeat_count < 1) {
                repeat_count = 1;
            }

            // Store initial beam weights
            std::auto_ptr<ParameterVector> initial_parameter_vector( the_plan->get_parameter_vector()->get_copy() );

            // Run optimization multiple times if needed
            for (unsigned int i_run = 0; i_run < repeat_count; i_run++) {
                // Add number for run to prefix if needed
                string temp_prefix = out_file_prefix;
                if (repeat_count > 1) {
                    char c_num[50];
                    sprintf(c_num, "%04u_", i_run+1);
                    temp_prefix += c_num;
                }
                opt_options.out_file_prefix_ = out_file_prefix;

                // Set optimization options
				cout << "setting optimization options" << endl;
                planopt->set_options(opt_options);

                if (i_run > 0) {
                    // Reset the plan and optimization to the initial state
                    the_plan->set_parameter_vector(*initial_parameter_vector);
                    planopt->reset_history();
                }

                try {
                    planopt->optimize();
                } catch (Optimization::exception) {
                    // The optimization failed, so regard that as an infinite objective value
                    cout << "Warning, optimization failed." << endl;
                }

                // Write out files if desired
                if (!discard_results_switch.getValue()) {
                    string temp_prefix = out_file_prefix;

                    std::auto_ptr<ParameterVector> tmpPv;

                    // Write out beam weights
                    the_plan->write_parameter_files(temp_prefix + string("beam"));

                    if (write_dose_switch.getValue()) {
                        // Write dose and DVH
                        the_plan->write_dose(temp_prefix,write_ct_dose_switch.getValue());
                    } else {
                        // Just write the DVH
                        the_plan->write_DVH(temp_prefix + "DVH.dat");
                    }

                    // Write dose for specific scenarios
                    the_plan->write_scenario_dose(temp_prefix);

                    if (write_beam_dose_switch.getValue()) {
                        // Write dose from each beam
                        the_plan->write_beam_dose(temp_prefix);
                    }

                    if (write_aux_dose_switch.getValue()) {
                        // Write dose and dvh from each auxilary
                        the_plan->write_auxiliary_dose(temp_prefix);
                    }

                    if (write_inst_dose_switch.getValue()) {
                        // Write dose and dvh from each instance
                        the_plan->write_instance_dose(temp_prefix);
                    }

                    // write variance
                    the_plan->write_variance(temp_prefix);

                    // write lagrange multipliers
                    //the_plan->write_lagrange_multipliers(temp_prefix);
                }
            }
        }

        // Print VOI information
        cout << "\n";
        the_plan->print_voi_stats(std::cout);

    } catch (TCLAP::ArgException &e)   // catch any exceptions
    {
        std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl;
    } /*catch (std::exception & e)   // Catch other exceptions
    {
        std::cerr << "Error caught in opt4D.cpp, main(): " << e.what() << std::endl;
        throw e;
    }*/

    return(0);
}


/**
 * Check to make sure the system uses the expected byte sizes for each type of variable.
 */
void check_byte_size() {
    if (sizeof(float) != 4 || sizeof(int) != 4 || sizeof(short) !=2 || sizeof(char) !=1 ) {
        cout << "Can not read the data: wrong byte size\n";
        cout << "sizeof(float): expect 4, found " << sizeof(float) << "\n";
        cout << "sizeof(int):   expect 4, found " << sizeof(int) << "\n";
        cout << "sizeof(short): expect 2, found " << sizeof(short) << "\n";
        cout << "sizeof(char):  expect 1, found " << sizeof(char) << "\n";
        exit (-1);
    }
}


/**
 * Print usage message for user.
 */
void print_usage_message() {
    cout << "'opt4D' is a package used for optimization of IMRT treatment plans." << "\n\n";
    cout << "Usage: opt4D [OPTIONS] [PLANFILE]" << "\n\n";

    cout << "Examples:\n"
        << "    opt4D plan.pln             "
        << "# Run a default optimization of a plan\n\n"
        << "    opt4D --out-file-prefix results/ plan.pln\n"
        << "                               "
        << "# Puts results from plan in folder \"results\"\n\n"
        << "    opt4D -c plan.pln          "
        << "# Calculate dose delivered by plan.\n"
        << "                               "
        << "# Does not optimize plan.\n"
        << "    opt4D -#1 0.1 plan.pln     "
        << "# Replace #1 in the plan file by 0.1\n\n";

    cout << "\nGeneral Options:\n"
        << "  -c, --calculate-dose          "
        << "  Calculates the dose delivered to plan\n"
        << "                                "
        << "  without performing any optimization\n"
        << "      --save-results            "
        << "  (default) write results to file\n"
        << "      --dont-save-results       "
        << "  don't write results to file\n"
        << "      --initial-parameter-root BWFROOT"
        << "  starts with beam weights from files\n"
        << "                                "
        << "  starting with BWFROOT\n"
        << "      --out-file-prefix PREFIX  "
        << "  adds PREFIX before saved file names. Useful\n"
        << "                                "
        << "  for putting saved files in another folder.\n"
        << "      --write-dose              "
        << "  write dose to file\n"
        << "      --dont-write-dose         "
        << "  (default) don't write dose to file\n"
        << "      --write-DVH-history       "
        << "  write DVH history to file\n"
        << "      --verbosity V             "
        << "  (default 1) Setting for how verbose to be.\n"
        << "                                "
        << "  Higher numbers produce more printouts during\n"
        << "                                "
        << "  optimization runs.\n"
        << "      --repeat REPETITIONS      "
        << "  Repeat the optimization REPETITIONS times\n"
        << "  -#PARAM PARAMETER             "
        << "  Replace parameter PARAM with PARAMETER in\n"
        << "                                "
        << "  the plan file.\n"
        << "      --help                    "
        << " Print this information\n";

    cout << "\nGeneric Optimization Options:\n"
        << "      --max-steps MAXSTEPS   "
        << " Maximum optimization steps (default 100)\n"
        << "      --min-steps MINSTEPS   "
        << " Minimum optimization steps (default 0)\n"
        << "      --max-time MAXTIME     "
        << " Maximum time for optimization in seconds\n"
        << "                             "
        << " (default 600)\n"
        << "      --stop-fract STOPFRACT "
        << " Stop when objective improves by less than\n"
        << "                             "
        << " STOPFRACT (default 0.002)\n"
        << "      --step-size-multiplier MULTIPLIER\n"
        << "                             "
        << " Multiplies the default stepsize by MULTIPLIER\n";

    cout << "\nChoosing an Optimization algorithm:\n"
        << "      --steepest-descent        "
        << "      --diagonal-newton         "
        << " optimize by diagnoalized Newton descent\n"
        << "      --conjugate-gradient      "
        << " optimize by conjugate gradient descent\n"
        << "      --delta-bar-delta         "
        << " optimize by delta-bar-delta\n"
        << "      --stochastic-meta-descent "
        << " optimize by stochastic-meta-descent\n";

    cout << "\nLine search parameters (for algorithms that allow it):\n"
        << "      --use-optimal-steps        "
        << " optimize the step size at each iteration\n"
        << "      --dont-use-optimal-steps   "
        << " (default) use fixed step size\n"
        << "      --linesearch-steps STEPS   "
        << " (default 3) number of iterations of linesearch\n"
        << "                                 "
        << " for computing optimal step size\n"
        << "      --use-alternate-linesearch "
        << "      --prevent-overstepping    ";

    cout << "\nSampling Options:\n"
        << "      --stochastic-optimization   "
        << "  Use voxels sampling during optimization\n"
        << "      --stochastic                "
        << "  same as --stochastic-optmization\n"
        << "      --uniform-sampling FRACTION "
        << "  Override VOI sampling to FRACTION\n"
        << "      --uniform-samples-per-voi N "
        << "  Override VOI sampling to use N per VOI\n"
        << "      --use-adaptive-sampling     "
        << "  adjust sampling rates on the fly\n"
        << "      --calculate-true-objective  "
        << "      --random-seed SEED          "
        << "  Use a particular seed (otherwise random)\n";

    cout << "\n Delta-Bar-Delta options:\n"
        << "  --dbd-learning-rate-increment R\n"
        << "  --dbd-learning-rate-decrement PHI\n"
        << "  --dbd-gradient-smoothing-exponent THETA\n";

    cout << "\n Stochastic Meta Descent options:\n"
        << "  --smd-smoothing-coefficient COEFFICIENT\n"
        << "  --smd-meta-step-size STEPSIZE\n";
    cout << "  --calculate-feasible-gradient\n"
        << "  --calculate-constrained-gradient\n";
    cout << "\nIf conflicting options are used, the last one takes precedent." << "\n\n";
}
