/**
 * @file DoseInfluenceMatrix.cpp
 * DoseInfluenceMatrix class implementation
 *
 * $Id: DoseInfluenceMatrix.cpp,v 1.30 2008/03/14 15:00:26 bmartin Exp $
 */

/* No longer up to date. Kept for historical reasons only.
 *
 * @version 0.9.4
 * <pre>
 * ver 0.9.4    BM      Jul 15, 2004    several changes.
 * ver 0.9.2    AT      Apr 05, 2004    beams from Dij are now matched to the
 *                                      steering file info
 * ver 0.9.0     TB      Mar 02, 2004   moved optimization functions from
 *                                      Dij_matrix to optimization;
 *                                      no abstraction Dij->dose_calc
 * ver 0.8.10    AT      Feb 27, 2004   minor fixes, changes, cleanup
 * ver 0.8.7     AT      Feb 05, 2004   added write_Dij_matrix (KonRad format),
 *                                      replace_Dij
 * ver 0.8       AT      Dec 05, 2003   implemented optimization with weights,
 *                                      readout of KonRad Dij-matrix
 * ver 0.7       TB      Oct 25, 2003   implemented aperture matrix
 * ver 0.6       TB      Sep 28, 2003   zero columns are disregarded
 * ver 0.5       TB      Sep 09, 2003   creation
 * AT: Alexei Trofimov
 * TB: Thomas Bortfeld
 * BM: Ben Martin
 * Massachusetts General Hospital, Department of Radiation Oncology
 * </pre>
 */

#include "DoseInfluenceMatrix.hpp"
#include "Geometry.hpp"
#include <exception>
#include <cstdio>

const unsigned int MAX_TRANSPOSE_MEMORY = 100000000;
const unsigned int UNDERFLOWWED_UINT = (unsigned int) -1;

/**
 * Default Constructor: Creates empty Dij matrix.
 */
DoseInfluenceMatrix::DoseInfluenceMatrix( bool is_transposed )
  : is_sorted_(false)
  , nBixels_(0)
  , nVoxels_(0)
  , nEntries_(0)
  , nAuxiliaries_(0)
  , is_transposed_(is_transposed)
  , influence_()
  , auxiliary_influence_()
  , voxelNo_or_bixelNo_()
  , bixel_start_index_()
  , voxel_start_index_()
{
}

/**
 * Default Destructor
 */
DoseInfluenceMatrix::~DoseInfluenceMatrix()
{
}


/**
 * Add two DoseInfluenceMatrix objects together to yeild a third.  This is very
 * inefficient if one is transposed and the other isn't, as an additional
 * matrix is created.
 */
DoseInfluenceMatrix DoseInfluenceMatrix::operator+(
    const DoseInfluenceMatrix& rhs) const
{
  // Temporarily shut this down
  assert(false);

}

/**
 * Set the number of Bixels.
 * This must be done before any entries are added
 * @param nBixels The number of bixels
 */
void DoseInfluenceMatrix::set_nBixels(unsigned int nBixels)
{
  assert(nEntries_ == 0);
  nBixels_ = nBixels;
  if(!is_transposed_) {
    bixel_start_index_.resize(nBixels+1);
    fill(bixel_start_index_.begin(), bixel_start_index_.end(), 0);
  }
}


/**
 * Set the number of voxels.
 * This must be done before any entries are added
 * @param nVoxels The number of voxels
 */
void DoseInfluenceMatrix::set_nVoxels(unsigned int nVoxels)
{
  assert(nEntries_ == 0);
  nVoxels_ = nVoxels;
  if(is_transposed_) {
    voxel_start_index_.resize(nVoxels+1);
    fill(voxel_start_index_.begin(), voxel_start_index_.end(), 0);
  }
}



/**
 * Scale the dose influence matrix by a constant
 *
 * @param scale_factor The factor to multiply the matrix by
 */
DoseInfluenceMatrix & DoseInfluenceMatrix::operator*=(
    const float scale_factor)
{
  for(unsigned int iEntry = 0; iEntry < influence_.size(); iEntry++) {
    influence_[iEntry] *= scale_factor;
  }
  return *this;
}


/**
 * Add Dij element, possible overwriting another or out of order
void DoseInfluenceMatrix::set_Dij(unsigned int bixelNo, unsigned int voxelNo, float dose)
{
  unsigned long int index = subscripts_to_index(bixelNo, voxelNo);

  if(nDijs_ == 1  || index > (*(Dij_.end()-1)).index_) {
    // Inserted in order
    ++nDijs_;
    Dij_.push_back(DoseInfluenceMatrix::node(index, dose));
  }
  else {
    // Inserted out of order
    vector<DoseInfluenceMatrix::node>::iterator iter = Dij_.end();

    // Find spot to insert entry
    while(iter != Dij_.begin() && ((*(iter - 1)).index_ >= index)) {
      --iter;
    }

    // iterators now point to insertion point
    if((*iter).index_ == index) {
      // Replacing an existing entry
      (*iter).influence_ = dose;
    }
    else {
      // Insert before iterator
      Dij_.insert(iter, DoseInfluenceMatrix::node(index,dose));
    }
  }
}
 */


/**
 * Add dose from bixels in bixel grid to a voxel grid.
 *
 * @param dose_vector pointer to voxel grid to store dose in
 * @param bixel_grid pointer to bixel grid
 * @param auxiliaryNo index to influence value to be used
 */
void DoseInfluenceMatrix::dose_forward(
    DoseVector &dose_vector,
    const BixelVector &bixel_grid,
    unsigned int auxiliaryNo)const
{
  // make sure auxiliary Dij exists
  if(auxiliaryNo > get_nAuxiliaries()) {
      cout << "cannot calculate dose for auxiliary Dij that does not exist:" << endl;
      cout << "auxiliaryNo = " << auxiliaryNo
	   << ", total number of auxiliaries = " << get_nAuxiliaries() << endl;
      exit(-1);
  }

  for(DoseInfluenceMatrix::iterator iter(this->begin());
      iter.not_at_end(); iter++) {
      dose_vector.add_dose(iter.get_voxelNo(),
			   bixel_grid.get_intensity(iter.get_bixelNo()) * iter.get_influence(auxiliaryNo));
  }
}


/**
 * Copy dose distribution of a pencil beam to a dose vector.
 *
 * @param bixelNo number of pencil beam to be considered
 * @param dose_vector pointer to voxel grid to store dose in
 * @param auxiliaryNo The auxiliary number to pull the dose from (optional,
 * default 0).
 */
void DoseInfluenceMatrix::copy_pencil_beam_to_dose_vector(
    unsigned int bixelNo,
    DoseVector &dose_vector,
    unsigned int auxiliaryNo)
{

    // make sure Dij is not transposed
    ensure_not_transposed();

    // zero dose
    dose_vector.reset_dose();

    // copy values
    for(DoseInfluenceMatrix::iterator iter = begin_bixel(bixelNo);
	iter.get_bixelNo() == bixelNo; iter++)
    {
	dose_vector.add_dose( iter.get_voxelNo() , iter.get_influence(auxiliaryNo) );
    }
}
/*void DoseInfluenceMatrix::copy_pencil_beam_to_dose_vector(
    unsigned int bixelNo,
    DoseVector &dose_vector,
    unsigned int auxiliaryNo)
{

    cout << endl << "WARNING: have to undo this!!!!!" << endl<<endl;

    // make sure Dij is not transposed
    ensure_not_transposed();

    // zero dose
    dose_vector.reset_dose();

    for(unsigned int i=0; i< get_nVoxels(); i++) {
	dose_vector.set_voxel_dose(i,1);
    }

    // copy values
    for(DoseInfluenceMatrix::iterator iter = begin_bixel(bixelNo);
	iter.get_bixelNo() == bixelNo; iter++)
    {
	dose_vector.set_voxel_dose( iter.get_voxelNo() , iter.get_influence(auxiliaryNo) );
    }
}*/


/**
 * Set the number of entries for each bixel.  Must be used before any entries
 * are loaded into the DoseInfluenceMatrix.  Only for non-transposed matrices.
 *
 * Puts the DoseInfluenceMatrix into a temporary invalid state.
 *
 * @param nEntries The number of entries in each bixel.
 */
void DoseInfluenceMatrix::set_nEntries_per_bixel(const vector<unsigned int> & nEntries)
{
  assert(nEntries_ == 0);
  assert(is_transposed_ == false);
  assert(nEntries.size() == nBixels_);

  // Set up bixel_start_index_
  bixel_start_index_[0] = 0;
  for(unsigned int iBixel = 0; iBixel < nBixels_; iBixel++) {
    bixel_start_index_[iBixel+1] = nEntries[iBixel] +
      bixel_start_index_[iBixel];
  }

  // Setup the bixel_nEntries_
  bixel_nEntries_.resize(nBixels_);
  fill(bixel_nEntries_.begin(), bixel_nEntries_.end(), 0);

  // Resize the data vectors
  influence_.resize(bixel_start_index_[nBixels_]);
  voxelNo_or_bixelNo_.resize(bixel_start_index_[nBixels_]);
}

/**
 * add an empty bixel
 * reserve space for nElements elements
 */
void DoseInfluenceMatrix::add_empty_bixel(unsigned int nElements)
{
  ensure_not_transposed();

  cout << "add empty bixel: nVoxels: " << get_nVoxels() << " nBixels: " << nBixels_+1 << endl;

  // increment number of bixels
  nBixels_++;

  // resize bixel start index
  bixel_start_index_.push_back(get_nEntries());
  bixel_nEntries_.push_back(0);

  // add blank space
  vector<unsigned int> tmp(nBixels_,0);
  tmp[nBixels_-1] = nElements;
  add_blank_space(tmp);
}

/**
 * Get the number of entries for each bixel.  Only for non-transposed matrices.
 *
 */
const vector<unsigned int> & DoseInfluenceMatrix::get_nEntries_per_bixel() const
{
  assert(is_transposed_ == false);
  assert(bixel_nEntries_.size() == nBixels_);

  return bixel_nEntries_;
}

/**
 * Get the number of entries for a given voxel.  Only for transposed matrices.
 *
 */
unsigned int DoseInfluenceMatrix::get_nEntries_per_voxel(unsigned int voxelNo)
{
  ensure_transposed();
  assert(voxel_nEntries_.size() == nVoxels_);

  return voxel_nEntries_[voxelNo];
}

/**
 * Set the number of entries for each voxel.  Must be used before any entries
 * are loaded into the DoseInfluenceMatrix.  Only for transposed matrices.
 *
 * Puts the DoseInfluenceMatrix into a temporary invalid state.
 *
 * @param nEntries The number of entries in each voxel.
 */
void DoseInfluenceMatrix::set_nEntries_per_voxel(const vector<unsigned int> & nEntries)
{
  assert(nEntries_ == 0);
  assert(is_transposed_ == true);
  assert(nEntries.size() == nVoxels_);

  // Set up voxel_start_index_
  voxel_start_index_[0] = 0;
  for(unsigned int iVoxel = 0; iVoxel < nVoxels_; iVoxel++) {
    voxel_start_index_[iVoxel+1] = nEntries[iVoxel] +
      voxel_start_index_[iVoxel];
  }

  // Set up the voxel_nEntries_
  voxel_nEntries_.resize(nVoxels_);
  fill(voxel_nEntries_.begin(), voxel_nEntries_.end(), 0);

  // Resize the data vectors
  influence_.resize(voxel_start_index_[nVoxels_]);
  voxelNo_or_bixelNo_.resize(voxel_start_index_[nVoxels_]);
}


/**
 * checks whether the Dij matrix is sorted
 */
bool DoseInfluenceMatrix::confirm_sorted()
{
    unsigned int iVoxel = get_nVoxels();
    unsigned int iBixel = get_nBixels();

    cout << "check whether Dij matrix is sorted" << endl;

    if(is_transposed()) {
	for(DoseInfluenceMatrix::iterator iter = begin();
	    iter.not_at_end(); iter++) {
	    if(iter.get_voxelNo() == iVoxel) {
		// bixel number must increase
		if(iter.get_bixelNo() <= iBixel) {
		    cout << "Dij matrix is NOT sorted" << endl;
		    return(false);
		}
	    }
	    iVoxel = iter.get_voxelNo();
	    iBixel = iter.get_bixelNo();
	}
    }
    else {
	for(DoseInfluenceMatrix::iterator iter = begin();
	    iter.not_at_end(); iter++) {
	    if(iter.get_bixelNo() == iBixel) {
		// voxel number must increase
		if(iter.get_voxelNo() <= iVoxel) {
		    cout << "Dij matrix is NOT sorted" << endl;
		    return(false);
		}
	    }
	    iVoxel = iter.get_voxelNo();
	    iBixel = iter.get_bixelNo();
	}
    }

    cout << "Dij matrix is sorted" << endl;
    return(true);
}


/**
 * Transpose Dij matrix.  Changes the order of accessing the elements of the
 * sparse matrix.  Try to do this as few times as possible since it is an
 * inherrently expensive action.  It uses temporary memory slightly
 * larger than the size of the Dij matrix or MAX_TRANSPOSE_MEMORY, whichever is
 * smaller.
 *
 * transposing the matrix will first remove potential blank space
 */
void DoseInfluenceMatrix::transpose()
{
  /*
   * Algorithm for Transposing Dose Influence Matrix
   *
   * Assuming that the following variables are available:
   *
   * unsigned int nBixels_, nVoxels_, nEntries;
   * vector<float> influence_;
   * vector<unsigned int> voxelNo_or_bixelNo_;
   * vector<unsigned int> bixel_start_index_;
   * vector<unsigned int> voxel_start_index_;
   *
   * bixel_start_index_ and voxel_start_index_ are size nBixels_+1 and
   * nVoxels_+1 respectively, such that bixel_start_index_[nBixels_] =
   * nEntries_.  This makes much of the following simpler.
   *
   * Block Sliding Algorithm:
   * For simplicity, the following description is for transposing the matrix.
   * To untranpose just replace all references to bixel with voxel and vice
   * versa.
   *
   * We transpose the matrix by pulling out blocks of voxels into temporary
   * vectors arranged in a transposed fashion.  We simultaneously slide the
   * remaining column-oriented data toward the end of the matrix while
   * preserving access to it in the normal fashion.  This technique is a good
   * trade-off between speed and memory usage.  Pulling out larger blocks of
   * memory at a time means that less sliding is required.
   *
   * We can then store the compressing the values of the existing orientation,
   * such that they can be accessed just as they normally would, but they are
   * pressed into the end of the vector.  Simultaneously, we build the new
   * system at the start of the influence_ and voxelNo_or_bixelNo_ vectors.
   *
   * To transpose from non-transposed to transposed (short version):
   * 1. build voxel_start_index_ based on voxelNo_or_bixelNo_
   * 2. for each block of voxelNos
   * 2.1. Figure out the maximum voxelNo that will fit in the block of memory
   * 2.2. Resize temp vectors to hold voxels to move
   * 2.3. Starting at the end of the vectors and working backwards, pull out
   *      each entry that is in the group of voxelNos and slide any others down
   *      (simultaneously sliding the elements of bixel_start_index_)
   * 2.4. Copy the entries into the appropriate part of the vector based on
   *      voxel_start_index_
   * 3. Update is_transposed_ and zero out bixel_start_index_
   */

  /*
   * Alternate algorithm using more memory but less time:
   *
   * 1. build voxel_start_index_ based on voxelNo_or_bixelNo_
   * 2. Attempt to allocate transposed_influence and transposed_bixelNo
   * 3. Allocate voxel_count to zero, size nVoxels
   * 4. For each entry in the sparse matrix:
   * 4.1. copy bixelNo and influence to appropriate spot in new matrices
   * 4.2. increment voxel_count
   * 5. Copy sparse matrix back into initial variables
   * 6. Discard temporary variables
   */

  std::cout << "Transposing Dose Influence Matrix...." << endl;

  // first remove blank space from the matrix
  remove_blank_space();

  is_sorted_ = false;

  if(!is_transposed_) {
     // Block Sliding algorithm

    assert(bixel_start_index_.size() == nBixels_+1);

    // The matrix is currently column oriented so we need to make it
    // row-oriented.

    // Populate voxel_start_index_
    // Start by zeroing it out
    voxel_start_index_.resize(nVoxels_+1);
    voxel_nEntries_.resize(nVoxels_);
    for(unsigned int i = 0; i < nVoxels_; i++) {
      voxel_nEntries_[i] = 0;
    }
    // Now count the number of occurences of each voxelNo
    for(unsigned int i = 0; i < voxelNo_or_bixelNo_.size(); i++) {
      voxel_nEntries_[voxelNo_or_bixelNo_[i]]++;
    }
    // Now sum up the results
    voxel_start_index_[0] = 0;
    for(unsigned int iVoxel = 0; iVoxel < nVoxels_; iVoxel++) {
      voxel_start_index_[iVoxel+1] = voxel_start_index_[iVoxel] + voxel_nEntries_[iVoxel];
    }
    assert(voxel_start_index_[nVoxels_] == nEntries_);

    // / / Now the main task / / //

    // Figure out how many entries you can hold within the maximum allowed
    // memory storage
    unsigned int nTempEntries = MAX_TRANSPOSE_MEMORY
      / (sizeof(int) + (get_nAuxiliaries()+1)*sizeof(float));
    if(nTempEntries > nEntries_) {
      nTempEntries = nEntries_;
    }

    // Create temporary vectors to hold blocks of entries
    vector<float> temp_influence;
    vector< vector<float> > temp_auxiliary_influence;
    vector<unsigned int> temp_bixelNo;
    try {
      temp_influence.reserve(nTempEntries);
      temp_bixelNo.reserve(nTempEntries);
      temp_auxiliary_influence.resize(get_nAuxiliaries());
      for(unsigned int i=0;i<get_nAuxiliaries();i++)
      {
	  temp_auxiliary_influence.at(i).reserve(nTempEntries);
      }
    } catch(...) {
      std::cerr << "Unable to reserve memory for transposing matrix.\n";
      throw;
    }

    // Counter for number of entries to each voxel
    vector<unsigned int> voxel_entry_counter(nVoxels_, 0);


    // Move entries by blocks of voxels
    unsigned int min_unmoved_voxelNo = 0;
    unsigned int nMoved_entries = 0;
    while(min_unmoved_voxelNo < nVoxels_ && bixel_start_index_[0] < nEntries_) {

      // First the maximum voxelNos to move on this iteration.
      // Make sure that at least one entry is moved.
      unsigned int nEntries_to_move = voxel_start_index_[min_unmoved_voxelNo+1]
        - voxel_start_index_[min_unmoved_voxelNo];
      min_unmoved_voxelNo++;
      while(min_unmoved_voxelNo < nVoxels_) {
        unsigned int nEntries_in_next_voxelNo =
          voxel_start_index_[min_unmoved_voxelNo+1]
          - voxel_start_index_[min_unmoved_voxelNo];
        if(nEntries_to_move + nEntries_in_next_voxelNo > nTempEntries) {
          // Already holds maximum number of entries
          break;
        }
        nEntries_to_move += nEntries_in_next_voxelNo;
        min_unmoved_voxelNo++;
      }

      // Pull out all entries with voxelNo < min_unmoved_voxelNo
      cout << "Moving " << nEntries_to_move << "/" << nTempEntries
        << " up to voxelNo: " << min_unmoved_voxelNo << "/" << nVoxels_ << "\n";

      // Resize temp vectors to hold entries for move
      temp_influence.resize(nEntries_to_move);
      temp_bixelNo.resize(nEntries_to_move);
      for(unsigned int i=0;i<get_nAuxiliaries();i++)
      {
	  temp_auxiliary_influence.at(i).resize(nEntries_to_move);
      }

      // Start with the last entry and work to the first.
      unsigned int bixelNo = nBixels_-1;
      unsigned int found_entries = 0;
      for(unsigned int iEntry = nEntries_;
          iEntry > bixel_start_index_[0];) {
        --iEntry; // To handle roll-over problem
        if(voxelNo_or_bixelNo_[iEntry] < min_unmoved_voxelNo) {
          // We are moving this entry

          // First Figure out what the bixel number is
          while(bixel_start_index_[bixelNo] > iEntry) {
            // We know that we haven't found the right one yet, so push this
            //  back.
            bixel_start_index_[bixelNo]+=found_entries;
            bixelNo--;
          }

          // We are now at the correct bixelNo, so store entry to temporary
          // vectors
          unsigned int temp_voxelNo = voxelNo_or_bixelNo_[iEntry];
          unsigned int temp_entryNo =
            voxel_start_index_[temp_voxelNo] +
            voxel_entry_counter[temp_voxelNo] - nMoved_entries;
          voxel_entry_counter[temp_voxelNo]++;

          temp_influence[temp_entryNo] = influence_[iEntry];
          temp_bixelNo[temp_entryNo] = bixelNo;
	  for(unsigned int i=0;i<get_nAuxiliaries();i++)
	  {
	    temp_auxiliary_influence[i][temp_entryNo] = auxiliary_influence_[i][iEntry];
	  }

          found_entries++;

          // Slide down bixelNo and any other bixelNos that have no entries
          while(bixelNo != UNDERFLOWWED_UINT
              && bixel_start_index_[bixelNo] == iEntry) {
            bixel_start_index_[bixelNo]+=found_entries;
            bixelNo--;
          }
        }
        else {
          // Entry is not being moved yet, so push back the entry
          influence_[iEntry+found_entries] = influence_[iEntry];
	  for(unsigned int i=0;i<get_nAuxiliaries();i++)
	  {
	    auxiliary_influence_[i][iEntry+found_entries] = auxiliary_influence_[i][iEntry];
	  }
          voxelNo_or_bixelNo_[iEntry+found_entries] =
            voxelNo_or_bixelNo_[iEntry];
        }
      }

      // Slide down the rest of the bixel_start_index_
      // Note: bixelNo will underflow after shifting bixelNo 0
      while(bixelNo != UNDERFLOWWED_UINT) {
        bixel_start_index_[bixelNo]+=found_entries;
        bixelNo--;
      }


      // We've now pulled out all of the entries for the voxels to be moved,
      // so we now need to copy the temp vectors back into place
      for(unsigned int iEntry = 0; iEntry < nEntries_to_move; iEntry++) {
        influence_[iEntry+nMoved_entries] = temp_influence[iEntry];
        voxelNo_or_bixelNo_[iEntry+nMoved_entries] = temp_bixelNo[iEntry];
	for(unsigned int i=0;i<get_nAuxiliaries();i++)
	{
	  auxiliary_influence_[i][iEntry+nMoved_entries] = temp_auxiliary_influence[i][iEntry];
	}

      }
      nMoved_entries += nEntries_to_move;
    }

    // Now just empty the bixel_start_index_ and we're done
    bixel_start_index_.resize(0);
    bixel_nEntries_.resize(0);
  }
  else {
     // Block Sliding algorithm

    assert(voxel_start_index_.size() == nVoxels_+1);

    // The matrix is currently row-oriented so we need to make it
    // column-oriented.

    // Populate bixel_start_index_
    // Start by zeroing it out
    bixel_start_index_.resize(nBixels_+1);
    bixel_nEntries_.resize(nBixels_);
    for(unsigned int i = 0; i < nBixels_; i++) {
      bixel_nEntries_[i] = 0;
    }
    // Now count the number of occurences of each bixelNo
    for(unsigned int i = 0; i < voxelNo_or_bixelNo_.size(); i++) {
      bixel_nEntries_[voxelNo_or_bixelNo_[i]]++;
    }
    // Now sum up the results
    bixel_start_index_[0] = 0;
    for(unsigned int iBixel = 0; iBixel < nBixels_; iBixel++) {
	bixel_start_index_[iBixel+1] = bixel_start_index_[iBixel] + bixel_nEntries_[iBixel];
    }
    assert(bixel_start_index_[nBixels_] == nEntries_);

    // / / Now the main task / / //

    // Figure out how many entries you can hold within the maximum allowed
    // memory storage
    unsigned int nTempEntries = MAX_TRANSPOSE_MEMORY
      / (sizeof(int) + (get_nAuxiliaries()+1)*sizeof(float));
    if(nTempEntries > nEntries_) {
      nTempEntries = nEntries_;
    }

    // Create temporary vectors to hold blocks of entries
    vector<float> temp_influence;
    vector< vector<float> > temp_auxiliary_influence;
    vector<unsigned int> temp_voxelNo;
    try {
      temp_influence.reserve(nTempEntries);
      temp_voxelNo.reserve(nTempEntries);
      temp_auxiliary_influence.resize(get_nAuxiliaries());
      for(unsigned int i=0;i<get_nAuxiliaries();i++)
      {
	  temp_auxiliary_influence.at(i).reserve(nTempEntries);
      }
    } catch(...) {
      std::cerr << "Unable to reserve memory for transposing matrix.\n";
      throw;
    }

    // Counter for number of entries to each bixel
    vector<unsigned int> bixel_entry_counter(nBixels_, 0);


    // Move entries by blocks of bixels
    unsigned int min_unmoved_bixelNo = 0;
    unsigned int nMoved_entries = 0;
    while(min_unmoved_bixelNo < nBixels_ && voxel_start_index_[0] < nEntries_) {

      // First the maximum bixelNos to move on this iteration.
      // Make sure that at least one entry is moved.
      unsigned int nEntries_to_move = bixel_start_index_[min_unmoved_bixelNo+1]
        - bixel_start_index_[min_unmoved_bixelNo];
      min_unmoved_bixelNo++;
      while(min_unmoved_bixelNo < nBixels_) {
        unsigned int nEntries_in_next_bixelNo =
          bixel_start_index_[min_unmoved_bixelNo+1]
          - bixel_start_index_[min_unmoved_bixelNo];
        if(nEntries_to_move + nEntries_in_next_bixelNo > nTempEntries) {
          // Already holds maximum number of entries
          break;
        }
        nEntries_to_move += nEntries_in_next_bixelNo;
        min_unmoved_bixelNo++;
      }

      // Pull out all entries with bixelNo < min_unmoved_bixelNo
      cout << "Moving " << nEntries_to_move << "/" << nTempEntries
        << " up to bixelNo: " << min_unmoved_bixelNo << "/" << nBixels_ << "\n";

      // Resize temp vectors to hold entries for move
      temp_influence.resize(nEntries_to_move);
      temp_voxelNo.resize(nEntries_to_move);
      for(unsigned int i=0;i<get_nAuxiliaries();i++)
      {
	temp_auxiliary_influence.at(i).resize(nEntries_to_move);
      }

      // Start with the last entry and work to the first.
      unsigned int voxelNo = nVoxels_-1;
      unsigned int found_entries = 0;
      for(unsigned int iEntry = nEntries_;
          iEntry > voxel_start_index_[0];) {
        --iEntry; // To handle roll-over problem
        if(voxelNo_or_bixelNo_[iEntry] < min_unmoved_bixelNo) {
          // We are moving this entry

          // First Figure out what the voxel number is
          while(voxel_start_index_[voxelNo] > iEntry) {
            // We know that we haven't found the right one yet, so push this
            //  back.
            voxel_start_index_[voxelNo]+=found_entries;
            voxelNo--;
          }

          // We are now at the correct voxelNo, so store entry to temporary
          // vectors
          unsigned int temp_bixelNo = voxelNo_or_bixelNo_[iEntry];
          unsigned int temp_entryNo =
            bixel_start_index_[temp_bixelNo] +
            bixel_entry_counter[temp_bixelNo] - nMoved_entries;
          bixel_entry_counter[temp_bixelNo]++;

          temp_influence[temp_entryNo] = influence_[iEntry];
          temp_voxelNo[temp_entryNo] = voxelNo;
	  for(unsigned int i=0;i<get_nAuxiliaries();i++)
	  {
	    temp_auxiliary_influence[i][temp_entryNo] = auxiliary_influence_[i][iEntry];
	  }

          found_entries++;

          // Slide down voxelNo and any other voxelNos that have no entries
          while(voxelNo != UNDERFLOWWED_UINT
              && voxel_start_index_[voxelNo] == iEntry) {
            voxel_start_index_[voxelNo]+=found_entries;
            voxelNo--;
          }
        }
        else {
          // Entry is not being moved yet, so push back the entry
          influence_[iEntry+found_entries] = influence_[iEntry];
	  for(unsigned int i=0;i<get_nAuxiliaries();i++)
	  {
	    auxiliary_influence_[i][iEntry+found_entries] = auxiliary_influence_[i][iEntry];
	  }
          voxelNo_or_bixelNo_[iEntry+found_entries] =
            voxelNo_or_bixelNo_[iEntry];
        }
      }

      // Slide down the rest of the voxel_start_index_
      // Note: voxelNo will underflow after shifting voxelNo 0
      while(voxelNo != UNDERFLOWWED_UINT) {
        voxel_start_index_[voxelNo]+=found_entries;
        voxelNo--;
      }

      // We've now pulled out all of the entries for the bixels to be moved,
      // so we now need to copy the temp vectors back into place
      for(unsigned int iEntry = 0; iEntry < nEntries_to_move; iEntry++) {
        influence_[iEntry+nMoved_entries] = temp_influence[iEntry];
        voxelNo_or_bixelNo_[iEntry+nMoved_entries] = temp_voxelNo[iEntry];
	for(unsigned int i=0;i<get_nAuxiliaries();i++)
	{
	  auxiliary_influence_[i][iEntry+nMoved_entries] = temp_auxiliary_influence[i][iEntry];
	}

      }
      nMoved_entries += nEntries_to_move;
    }

    // Now just empty the voxel_start_index_ and we're done
    voxel_start_index_.resize(0);
    voxel_nEntries_.resize(0);
  }

  // Update is_transposed_
  is_transposed_ = !is_transposed_;

  std::cout << "Done\n";
}


/**
 * sort Dij entries
 */
void DoseInfluenceMatrix::sort_Dij()
{
    vector<float> temp(get_nAuxiliaries());
    vector<DoseInfluenceMatrix::node> nodes;

    if(confirm_sorted()) {
	// nothing to do
    }
    else {
	// sort it
	cout << "sort Dij" << endl;

	if(!is_transposed()) {

	    // allocate vector of nodes to pull out all entries for a bixel
	    nodes.reserve(get_nVoxels());

	    // loop over all bixels
	    for (unsigned int iBixel=0;iBixel<get_nBixels();iBixel++) {

		// clear nodes
		nodes.resize(0);

		// copy values
		for(DoseInfluenceMatrix::iterator iter = begin_bixel(iBixel);
		    iter.get_bixelNo() == iBixel; iter++) {

		    for(unsigned int iAux=0;iAux<get_nAuxiliaries();iAux++) {
			temp[iAux] = iter.get_influence(iAux+1);
		    }

		    nodes.push_back(DoseInfluenceMatrix::node(iter.get_voxelNo(),
							      iter.get_influence(),
							      temp));
		}

		// sort it
		sort(nodes.begin(), nodes.end());

		// check
		assert(nodes.size() == bixel_nEntries_[iBixel]);

		// manipulate nEntries in order to use "add_dij_element" to restore entries
		bixel_nEntries_[iBixel] = 0;
		nEntries_ = nEntries_ - nodes.size();

		for(unsigned int iElement=0;iElement<nodes.size();iElement++) {

		    // restore Dij elements
		    add_Dij_element(iBixel,
				    nodes[iElement].index_,
				    nodes[iElement].influence_,
				    nodes[iElement].auxiliary_influence_);
		}
	    }
	}
	else {
	    // allocate vector of nodes to pull out all entries for a voxel
	    nodes.reserve(get_nBixels());

	    // loop over all voxels
	    for (unsigned int iVoxel=0;iVoxel<get_nVoxels();iVoxel++) {

		// clear nodes
		nodes.resize(0);

		// copy values
		for(DoseInfluenceMatrix::iterator iter = begin_voxel(iVoxel);
		    iter.get_voxelNo() == iVoxel; iter++) {

		    for(unsigned int iAux=0;iAux<get_nAuxiliaries();iAux++) {
			temp[iAux] = iter.get_influence(iAux+1);
		    }

		    nodes.push_back(DoseInfluenceMatrix::node(iter.get_bixelNo(),
							      iter.get_influence(),
							      temp));
		}

		// sort it
		sort(nodes.begin(), nodes.end());

		// check
		assert(nodes.size() == voxel_nEntries_[iVoxel]);

		// manipulate nEntries in order to use "add_dij_element" to restore entries
		voxel_nEntries_[iVoxel] = 0;
		nEntries_ = nEntries_ - nodes.size();

		for(unsigned int iElement=0;iElement<nodes.size();iElement++) {

		    // restore Dij elements
		    add_Dij_element(nodes[iElement].index_,
				    iVoxel,
				    nodes[iElement].influence_,
				    nodes[iElement].auxiliary_influence_);
		}
	    }
	}
    }
    // identify Dij as sorted
    is_sorted_ = true;
}

/**
 * Get the iterator that points at the first non-zero element of the matrix
 */
DoseInfluenceMatrix::iterator DoseInfluenceMatrix::begin()const
{
  return DoseInfluenceMatrix::iterator(*this);
}

DoseInfluenceMatrix::non_const_iterator DoseInfluenceMatrix::non_const_begin()
{
  return DoseInfluenceMatrix::non_const_iterator(*this);
}

/**
 * Get an iterator to the first entry with the desired bixelNo if it exists.
 * If the desired bixel does not have any non-zero matrix entries, the next
 * matrix entry will be used.  It is the user's responsibility to check if the
 * returned iterator has the desired bixelNo.  Note that the returned iterator
 * is not valid if the matrix is transposed.
 *
 * The Matrix will be untransposed if needed.
 *
 * @param bixelNo The desired bixelNo
 */
DoseInfluenceMatrix::iterator DoseInfluenceMatrix::begin_bixel(
    unsigned int bixelNo)
{
  // Make sure the matrix is not transposed
  ensure_not_transposed();

  return DoseInfluenceMatrix::iterator(*this, bixelNo);
}

DoseInfluenceMatrix::non_const_iterator DoseInfluenceMatrix::non_const_begin_bixel(
    unsigned int bixelNo)
{
  // Make sure the matrix is not transposed
  ensure_not_transposed();

  return DoseInfluenceMatrix::non_const_iterator(*this, bixelNo);
}
/**
 * Get an iterator to the first entry with the desired voxelNo if it exists.
 * If the desired voxel does not have any non-zero matrix entries, the next
 * matrix entry will be used.  It is the user's responsibility to check if
 * the returned iterator has the desired voxelNo.  Note that the returned
 * iterator is not valid if the matrix is untransposed.
 *
 * The Matrix will be transposed if needed.
 *
 * @param voxelNo The desired voxelNo
 */
DoseInfluenceMatrix::iterator DoseInfluenceMatrix::begin_voxel(
    unsigned int voxelNo)
{
  // Make sure the matrix is transposed
  ensure_transposed();

  return DoseInfluenceMatrix::iterator(*this, voxelNo);
}

DoseInfluenceMatrix::non_const_iterator DoseInfluenceMatrix::non_const_begin_voxel(
    unsigned int voxelNo)
{
  // Make sure the matrix is transposed
  ensure_transposed();

  return DoseInfluenceMatrix::non_const_iterator(*this, voxelNo);
}

/**
 * allocate memory for an auxiliary Dij
 *
 */
void DoseInfluenceMatrix::create_auxiliary_Dij()
{
    cout << "create auxiliary Dij: current number of auxiliaries: " << get_nAuxiliaries()+1 << endl;

    // create new vector for an auxiliary influence
    auxiliary_influence_.push_back( vector<float>(get_nEntries(),0) );

    // increment number of auxiliaries
    set_nAuxiliaries(get_nAuxiliaries()+1);

}

/**
 * add blank space at the end of each bixel or voxel
 * @param nEntries determines the amount of blank space to we added
 */
void DoseInfluenceMatrix::add_blank_space(const vector<unsigned int> &nEntries)
{
    unsigned int nEntriesTotal = 0;

    cout << "add blank space to Dij matrix" << endl;
    cout << "old number of entries: " << get_nEntries() << endl;

    // get total number of new entries
    for (unsigned int i=0; i<nEntries.size(); i++) {
	nEntriesTotal += nEntries.at(i);
    }

    cout << "number of entries to add: " << nEntriesTotal << endl;

    // resize the Dij matrix
    voxelNo_or_bixelNo_.resize(get_nEntries() + nEntriesTotal);
    influence_.resize(get_nEntries() + nEntriesTotal);
    for (unsigned int i=0; i<get_nAuxiliaries(); i++)
    {
	auxiliary_influence_[i].resize(get_nEntries() + nEntriesTotal);
    }

    if (!is_transposed() ) // not transposed
    {
	// make sure size of blank space vector equals the number of bixels
	assert( nEntries.size() == get_nBixels() );

	// create tempoary vector to store new bixel_start_index
	vector<unsigned int> temp_bixel_start_index(get_nBixels()+1,0);

	// calculate new bixel_start_index
	for (unsigned int iBixel=0; iBixel<get_nBixels(); iBixel++)
	{
	    temp_bixel_start_index.at(iBixel+1) = temp_bixel_start_index.at(iBixel) +
		(bixel_start_index_.at(iBixel+1) - bixel_start_index_.at(iBixel)) + nEntries.at(iBixel);
	}

	assert(temp_bixel_start_index.at(get_nBixels()) == voxelNo_or_bixelNo_.size());

	// move entries starting with the last bixel
	for (int iBixel=get_nBixels()-1; iBixel>=0; iBixel--)
	{
	    for (int iEntry=bixel_nEntries_[iBixel]-1; iEntry >=0; iEntry--)
	    {
		voxelNo_or_bixelNo_[temp_bixel_start_index[iBixel]+iEntry] = voxelNo_or_bixelNo_[bixel_start_index_[iBixel]+iEntry];
		influence_[temp_bixel_start_index[iBixel]+iEntry] = influence_[bixel_start_index_[iBixel]+iEntry];
		for (unsigned int i=0; i<get_nAuxiliaries(); i++)
		{
		    auxiliary_influence_[i][temp_bixel_start_index[iBixel]+iEntry] = auxiliary_influence_[i][bixel_start_index_[iBixel]+iEntry];
		}
	    }
	}

	// copy new start_bixel_index
	bixel_start_index_ = temp_bixel_start_index;

	// check
	assert( bixel_start_index_[get_nBixels()] == get_nEntries() + nEntriesTotal );
    }
    else // transposed
    {
	// make sure size of blank space vector equals the number of voxels
	assert( nEntries.size() == get_nVoxels() );

	// create tempoary vector to store new voxel_start_index
	vector<unsigned int> temp_voxel_start_index(get_nVoxels()+1,0);

	// calculate new voxel_start_index
	for (unsigned int iVoxel=0; iVoxel<get_nVoxels(); iVoxel++)
	{
	    temp_voxel_start_index.at(iVoxel+1) = temp_voxel_start_index.at(iVoxel)
		+ (voxel_start_index_.at(iVoxel+1) - voxel_start_index_.at(iVoxel)) + nEntries.at(iVoxel);
	}

	// move entries starting with the last voxel
	for (int iVoxel=get_nVoxels()-1; iVoxel>=0; iVoxel--)
	{
	    for (int iEntry=voxel_nEntries_[iVoxel]-1; iEntry >=0; iEntry--)
	    {
		voxelNo_or_bixelNo_[temp_voxel_start_index[iVoxel]+iEntry] = voxelNo_or_bixelNo_[voxel_start_index_[iVoxel]+iEntry];
		influence_[temp_voxel_start_index[iVoxel]+iEntry] = influence_[voxel_start_index_[iVoxel]+iEntry];
		for (unsigned int i=0; i<get_nAuxiliaries(); i++)
		{
		    auxiliary_influence_[i][temp_voxel_start_index[iVoxel]+iEntry] = auxiliary_influence_[i][voxel_start_index_[iVoxel]+iEntry];
		}
	    }
	}

	// copy new start_voxel_index
	voxel_start_index_ = temp_voxel_start_index;

	// check
	assert( voxel_start_index_[get_nVoxels()] == get_nEntries() + nEntriesTotal );
    }

    // set number of elements to the new size of the influence vector
    nEntries_ += nEntriesTotal;
    cout << "new number of entries: " << get_nEntries() << endl;
}


/**
 * remove all blank space from the Dij matrix
 *
 * @todo has not been tested properly
 */
void DoseInfluenceMatrix::remove_blank_space()
{
    unsigned int nElements = 0;

    // calculate total amount of blank space
    if (!is_transposed())
    {
	for (unsigned int i=0; i<get_nBixels(); i++)
	{
	    nElements += bixel_nEntries_.at(i);
	}
    }
    else
    {
	for (unsigned int i=0; i<get_nVoxels(); i++)
	{
	    nElements += voxel_nEntries_.at(i);
	}
    }

    if (nElements < get_nEntries())   // there is blank space to remove
    {
	cout << "remove blank space from Dij matrix" << endl;
	cout << "old number of entries: " << get_nEntries() << endl;

	if (!is_transposed())
	{
	    // create tempoary vector to store new bixel_start_index
	    vector<unsigned int> temp_bixel_start_index(get_nBixels()+1,0);

	    // calculate new bixel_start_index
	    for (unsigned int iBixel=0; iBixel<get_nBixels(); iBixel++)
	    {
		temp_bixel_start_index.at(iBixel+1) =
		    temp_bixel_start_index.at(iBixel) + bixel_nEntries_.at(iBixel);
	    }

	    // move entries starting with the first bixel
	    for (unsigned int iBixel=0; iBixel<get_nBixels(); iBixel++)
	    {
		for (unsigned int iEntry=0; iEntry<bixel_nEntries_.at(iBixel); iEntry++)
		{
		    voxelNo_or_bixelNo_[temp_bixel_start_index[iBixel]+iEntry] =
			voxelNo_or_bixelNo_[bixel_start_index_[iBixel]+iEntry];
		    influence_[temp_bixel_start_index[iBixel]+iEntry] =
			influence_[bixel_start_index_[iBixel]+iEntry];
		    for (unsigned int i=0; i<get_nAuxiliaries(); i++)
		    {
			auxiliary_influence_[i][temp_bixel_start_index[iBixel]+iEntry] =
			    auxiliary_influence_[i][bixel_start_index_[iBixel]+iEntry];
		    }
	    }
	}

	// copy new start_bixel_index
	bixel_start_index_ = temp_bixel_start_index;

	// check
	assert( bixel_start_index_[get_nBixels()] == nElements );

	}
	else // transposed
	{
	    // create tempoary vector to store new voxel_start_index
	    vector<unsigned int> temp_voxel_start_index(get_nVoxels()+1,0);

	    // calculate new voxel_start_index
	    for (unsigned int iVoxel=0; iVoxel<get_nVoxels(); iVoxel++)
	    {
		temp_voxel_start_index.at(iVoxel+1) =
		    temp_voxel_start_index.at(iVoxel) + voxel_nEntries_.at(iVoxel);
	    }

	    // move entries starting with the last voxel
	    for (unsigned int iVoxel=0; iVoxel<get_nVoxels(); iVoxel++)
	    {
		for (unsigned int iEntry=0; iEntry<voxel_nEntries_[iVoxel]; iEntry++)
		{
		    voxelNo_or_bixelNo_[temp_voxel_start_index[iVoxel]+iEntry] =
			voxelNo_or_bixelNo_[voxel_start_index_[iVoxel]+iEntry];
		    influence_[temp_voxel_start_index[iVoxel]+iEntry] =
			influence_[voxel_start_index_[iVoxel]+iEntry];
		    for (unsigned int i=0; i<get_nAuxiliaries(); i++)
		    {
			auxiliary_influence_[i][temp_voxel_start_index[iVoxel]+iEntry] =
			    auxiliary_influence_[i][voxel_start_index_[iVoxel]+iEntry];
		    }
		}
	    }

	    // copy new start_voxel_index
	    voxel_start_index_ = temp_voxel_start_index;

	    // check
	    assert( voxel_start_index_[get_nVoxels()] == nElements );
	}

	// set number of elements to the new size of the influence vector
	nEntries_ = nElements;
	cout << "new number of entries: " << get_nEntries() << endl;

	// resize the Dij matrix
	voxelNo_or_bixelNo_.resize(get_nEntries());
	influence_.resize(get_nEntries());
	for (unsigned int i=0; i<get_nAuxiliaries(); i++)
	{
	    auxiliary_influence_[i].resize(get_nEntries());
	}

    }
}


/**
 * Remove all entries with zero intenstity but leave blank space.  The vectors
 * that store the entries are not resized.
 *
 * @todo has not been tested properly
 */
void DoseInfluenceMatrix::remove_zero_entries()
{
    unsigned int nElements = 0;

    if (!is_transposed()) {

        // move entries starting with the first bixel
        for (unsigned int iBixel=0; iBixel<get_nBixels(); iBixel++) {
            // Remove zero entries and shift other entries
            int nRemoved = 0;
            unsigned int lead_entryNo = bixel_start_index_[iBixel];
            unsigned int follow_entryNo = lead_entryNo;
            unsigned int end_entryNo = lead_entryNo
                + bixel_nEntries_.at(iBixel);
            while(lead_entryNo < end_entryNo) {
                // Figure out if lead entry is zero
                bool is_zero = (influence_[lead_entryNo] == 0);

                // Check if all of the auxiliaries are zero
                for(unsigned int iAux=0; is_zero && iAux<get_nAuxiliaries();
                        iAux++) {
                    is_zero = (auxiliary_influence_[iAux][lead_entryNo] == 0);
                }

                // Remove the entry if necessary
                if(is_zero) {
                    nRemoved ++;
                    // Only increment the lead counter
                    lead_entryNo++;
                }
                else {
                    // Check if we need to shift the values
                    if(lead_entryNo != follow_entryNo) {
                        // Yup, so shift them
                        voxelNo_or_bixelNo_[follow_entryNo] =
                            voxelNo_or_bixelNo_[lead_entryNo];
                        influence_[follow_entryNo] =
                            influence_[lead_entryNo];
                        for(unsigned int iAux=0; iAux<get_nAuxiliaries();
                                iAux++) {
                            auxiliary_influence_[iAux][follow_entryNo] =
                                auxiliary_influence_[iAux][lead_entryNo];
                        }
                    }
                    // Increment both counters
                    lead_entryNo++;
                    follow_entryNo++;
                }
            }
            bixel_nEntries_.at(iBixel) -= nRemoved;
        }
    }
    else {
        // transposed

        // move entries starting with the first voxel
        for (unsigned int iVoxel=0; iVoxel<get_nVoxels(); iVoxel++) {
            // Remove zero entries and shift other entries
            int nRemoved = 0;
            unsigned int lead_entryNo = voxel_start_index_[iVoxel];
            unsigned int follow_entryNo = lead_entryNo;
            unsigned int end_entryNo = lead_entryNo
                + voxel_nEntries_.at(iVoxel);
            while(lead_entryNo < end_entryNo) {
                // Figure out if lead entry is zero
                bool is_zero = (influence_[lead_entryNo] == 0);

                // Check if all of the auxiliaries are zero
                for(unsigned int iAux=0; is_zero && iAux<get_nAuxiliaries();
                        iAux++) {
                    is_zero = (auxiliary_influence_[iAux][lead_entryNo] == 0);
                }

                // Remove the entry if necessary
                if(is_zero) {
                    nRemoved ++;
                    // Only increment the lead counter
                    lead_entryNo++;
                }
                else {
                    // Check if we need to shift the values
                    if(lead_entryNo != follow_entryNo) {
                        // Yup, so shift them
                        voxelNo_or_bixelNo_[follow_entryNo] =
                            voxelNo_or_bixelNo_[lead_entryNo];
                        influence_[follow_entryNo] =
                            influence_[lead_entryNo];
                        for(unsigned int iAux=0; iAux<get_nAuxiliaries();
                                iAux++) {
                            auxiliary_influence_[iAux][follow_entryNo] =
                                auxiliary_influence_[iAux][lead_entryNo];
                        }
                    }
                    // Increment both counters
                    lead_entryNo++;
                    follow_entryNo++;
                }
            }
            voxel_nEntries_.at(iVoxel) -= nRemoved;
        }
    }
}


/**
 * add multiple Dij elements to a bixel in the nominal Dij matrix,
 * elements will not be sorted,
 * auxiliaries will not be assigned a value
 */
void DoseInfluenceMatrix::add_Dij_elements_to_bixel(const unsigned int bixelNo, vector<unsigned int> &voxelNos, vector<float> &influence)
{
    assert( voxelNos.size() == influence.size() );
    assert( !is_transposed_ );

    // elements won't be sorted
    is_sorted_ = false;

/*
    // make sure matrix isn't transposed
    // Although this will untranspose the matrix so that the function will work,
    // realistically this function should only be called if the matrix isn't transposed
    // and blank space for the new elements was already created
    ensure_not_transposed();
*/
    // make sure there is enough blank space to hold the new elements
    if ( bixel_start_index_.at(bixelNo) + bixel_nEntries_.at(bixelNo) + voxelNos.size() > bixel_start_index_.at(bixelNo+1) )
    {
	// there is not enough blank space to hold all new elements, so enlarge the Dij matrix
	// this time consuming step should be avoided by creating enough blank space in advance
	cout << "not enough blank space at bixel " << bixelNo << " to hold " << voxelNos.size() << " new elements" << endl;

	// add blank space to this bixel
	vector<unsigned int> temp_nBlanks(get_nBixels(),0);
	temp_nBlanks.at(bixelNo) = voxelNos.size() - ( bixel_start_index_.at(bixelNo+1)
						       - bixel_start_index_.at(bixelNo)
						       - bixel_nEntries_.at(bixelNo) );
	add_blank_space(temp_nBlanks);
    }

    // loop over the new elements
    for (unsigned int iElement = 0; iElement < voxelNos.size(); iElement++)
    {
	voxelNo_or_bixelNo_[bixel_start_index_[bixelNo]+bixel_nEntries_[bixelNo]+iElement] = voxelNos[iElement];
	influence_[bixel_start_index_[bixelNo]+bixel_nEntries_[bixelNo]+iElement] = influence[iElement];
    }

    // increment number of entries
    bixel_nEntries_[bixelNo] += voxelNos.size();

    assert(bixel_start_index_[bixelNo]+bixel_nEntries_[bixelNo] <= bixel_start_index_[bixelNo+1]);
}


/**
 * add multiple Dij elements to a voxel in the nominal Dij matrix,
 * elements will not be sorted,
 * auxiliaries will not be assigned a value
 *
 * @todo this function has never been tested
 */
void DoseInfluenceMatrix::add_Dij_elements_to_voxel(const unsigned int voxelNo, vector<unsigned int> &bixelNos, vector<float> &influence)
{
    assert( bixelNos.size() == influence.size() );
    assert( is_transposed_ );

    // elements won't be sorted
    is_sorted_ = false;

/*
    // make sure matrix is transposed
    // Although this will transpose the matrix so that the function will work,
    // realistically this function should only be called if the matrix is transposed
    // and blank space for the new elements was already created
    ensure_transposed();
*/
    // make sure there is enough blank space to hold the new elements
    if ( voxel_start_index_.at(voxelNo) + voxel_nEntries_.at(voxelNo) + bixelNos.size() >= voxel_start_index_.at(voxelNo+1) )
    {
	// there is not enough blank space to hold all new elements, so enlarge the Dij matrix
	// this time consuming step should be avoided by creating enough blank space in advance
	cout << "not enough blank space at voxel " << voxelNo << " to hold " << bixelNos.size() << " new elements" << endl;

	// add blank space to this voxel
	vector<unsigned int> temp_nBlanks(get_nVoxels(),0);
	temp_nBlanks.at(voxelNo) = bixelNos.size() - ( voxel_start_index_.at(voxelNo+1)
						       - voxel_start_index_.at(voxelNo)
						       - voxel_nEntries_.at(voxelNo) );
	add_blank_space(temp_nBlanks);
    }

    // loop over the new elements
    for (unsigned int iElement = 0; iElement < bixelNos.size(); iElement++)
    {
	voxelNo_or_bixelNo_[voxel_start_index_[voxelNo]+voxel_nEntries_[voxelNo]+iElement] = bixelNos[iElement];
	influence_[voxel_start_index_[voxelNo]+voxel_nEntries_[voxelNo]+iElement] = influence[iElement];
    }

    // increment number of entries
    voxel_nEntries_[voxelNo] += bixelNos.size();
}


/**
 * Read auxiliary Dij matrices to file
 *
 *
 * @param adij_file_name File name of auxiliary Dij file.  Usually ends with
 * ".adij"
 * @param file_id_number Unique ID for file to read.  Used for verification
 */
void DoseInfluenceMatrix::read_auxiliary_Dijs(std::string adij_file_name, int file_id_number)
{

    FILE *dij_file = NULL;
    unsigned int nAuxiliaries;
    bool is_transposed;
    unsigned int uiDummy;
    int iDummy;

    std::cout << "Reading auxiliary dij file: " << adij_file_name << endl;

    // open file
    dij_file = fopen(adij_file_name.c_str(),"rb");
    if (dij_file == NULL) {
	std::cerr << "Can't open file: " << adij_file_name << std::endl;
        throw("Unable to open auxiliary dij file");
    }

    // remove blank space before
    remove_blank_space();

    // read the header
    fread(&iDummy, sizeof(int), 1, dij_file);
    if(iDummy != file_id_number) {
	std::cerr << "wrong identification number in auxiliary Dij file: " << adij_file_name << std::endl;
        throw("Error reading auxiliary Dijs");
    }

    // check number of entries
    fread(&uiDummy, sizeof(unsigned int), 1, dij_file);
    if(uiDummy != nEntries_) {
	std::cerr << "wrong number of entries in auxiliary Dij file: " << adij_file_name << std::endl;
        throw("Error reading auxiliary Dijs");
    }

    // allocate memory for auxiliaries
    fread(&nAuxiliaries, sizeof(unsigned int), 1, dij_file);
    assert( nAuxiliaries_ == nAuxiliaries);

    // check transpose status
    fread(&is_transposed, sizeof(bool), 1, dij_file);
    if(is_transposed) {
	ensure_transposed();
    }
    else {
	ensure_not_transposed();
    }

    // make sure elements are sorted
    ensure_sorted();

    // read voxel or bixel number
    for (size_t i=0; i<nEntries_; i++) {
	fread(&uiDummy, sizeof(unsigned int), 1, dij_file);
    	if (uiDummy != voxelNo_or_bixelNo_[i]) {
	    std::cerr << "wrong voxel/bixel index in auxiliary Dij file: " << adij_file_name << std::endl;
	    std::cerr << "expected: " << voxelNo_or_bixelNo_[i];
	    std::cerr << " read: " << uiDummy << std::endl;
	    throw("Error reading auxiliary Dijs");
	}
    }

    // read influence
    for (size_t i=0; i<nAuxiliaries_; i++) {
	for (size_t j=0; j<nEntries_; j++) {
	    fread(&auxiliary_influence_[i][j], sizeof(float), 1, dij_file);
	}
    }

    fclose(dij_file);
}

/**
 * write auxiliary Dij matrices to file
 *
 *
 * @param adij_file_name File name of auxiliary Dij file.  Usually ends with
 * ".adij"
 * @param file_id_number ID number to identify file.  (typically random)
 */
void DoseInfluenceMatrix::write_auxiliary_Dijs(
        std::string adij_file_name,
        int file_id_number)
{
    FILE *dij_file = NULL;

    std::cout << "Writing auxiliary dij file: " << adij_file_name << endl;

    // open file
    dij_file = fopen(adij_file_name.c_str(),"wb");
    if (dij_file == NULL) {
	std::cerr << "Can't open file: " << adij_file_name << std::endl;
        throw("Unable to open auxiliary dij file");
    }

    // remove blank space before
    remove_blank_space();

    // write the header
    fwrite(&file_id_number, sizeof(int), 1, dij_file);
    fwrite(&nEntries_, sizeof(unsigned int), 1, dij_file);
    fwrite(&nAuxiliaries_, sizeof(unsigned int), 1, dij_file);
    fwrite(&is_transposed_, sizeof(bool), 1, dij_file);

    // write voxel or bixel number
    for (size_t i=0; i<nEntries_; i++) {
	fwrite(&voxelNo_or_bixelNo_[i], sizeof(unsigned int), 1, dij_file);
    }

    // write influence
    for (size_t i=0; i<nAuxiliaries_; i++) {
	for (size_t j=0; j<nEntries_; j++) {
	    fwrite(&auxiliary_influence_[i][j], sizeof(float), 1, dij_file);
	}
    }

    fclose(dij_file);

    cout << "dose ..." << endl;
}

/**
 * virtual function
 *
 */
void DoseInfluenceMatrix::write_dij_files(string dij_file_root)
{
    cout << "WARNING: cannot write dij files since this function " << endl;
    cout << "is only implemented in derived classes" << endl;
}


// --------------------------------------------
// Functions for: DoseInfluenceMatrix::iterator
// --------------------------------------------

/**
 * Constructor
 *
 * @param rhs The DoseInfluenceMatrix to iterate over
 */
DoseInfluenceMatrix::iterator::iterator(
    const DoseInfluenceMatrix& rhs)
  : the_DoseInfluenceMatrix_(&rhs)
  , voxelNo_(0)
  , bixelNo_(0)
  , entryNo_(0)
{
  if(the_DoseInfluenceMatrix_->is_transposed()) {
    // The matrix is transposed, so we need to find the first voxelNo
    bixelNo_ = the_DoseInfluenceMatrix_->voxelNo_or_bixelNo_[entryNo_];
    while(voxelNo_ < the_DoseInfluenceMatrix_->nVoxels_ &&
        the_DoseInfluenceMatrix_->voxel_start_index_[voxelNo_+1]==entryNo_) {
      voxelNo_++;
    }
  }
  else {
    // The matrix is not transposed, so we need to find the first bixelNo
    voxelNo_ = the_DoseInfluenceMatrix_->voxelNo_or_bixelNo_[entryNo_];
    while(bixelNo_ < the_DoseInfluenceMatrix_->nBixels_ &&
        the_DoseInfluenceMatrix_->bixel_start_index_[bixelNo_+1]==entryNo_) {
      bixelNo_++;
    }
  }
}

/**
 * Initialize at either a row or a column of the DoseInfluenceMatrix.
 * Initialized to the end of the matrix if there are no entries in the
 * row.
 *
 * @param the_DoseInfluenceMatrix The DoseInfluenceMatrix to iterate over
 * @param bixelNo_or_voxelNo The bixelNo if the DoseInfluenceMatrix is
 * transposed or the voxelNo if it isn't.
 */
DoseInfluenceMatrix::iterator::iterator(
    const DoseInfluenceMatrix& the_DoseInfluenceMatrix,
    const unsigned int bixelNo_or_voxelNo)
  : the_DoseInfluenceMatrix_(&the_DoseInfluenceMatrix)
  , voxelNo_(0)
  , bixelNo_(0)
  , entryNo_(0)
{
  if(the_DoseInfluenceMatrix_->nEntries_ == 0) {
    voxelNo_ = the_DoseInfluenceMatrix_->nVoxels_;
    bixelNo_ = the_DoseInfluenceMatrix_->nBixels_;
    return;
  }

  if(the_DoseInfluenceMatrix_->is_transposed()) {
    // The matrix is transposed, so we are looking for a row
    voxelNo_ = bixelNo_or_voxelNo;
    entryNo_ = the_DoseInfluenceMatrix_->voxel_start_index_[voxelNo_];

    // Set to end of matrix if the asked for Voxel doesn't have any entries
    if(the_DoseInfluenceMatrix_->voxel_start_index_[voxelNo_+1]==entryNo_) {
      entryNo_ = the_DoseInfluenceMatrix_->nEntries_;
      voxelNo_ = the_DoseInfluenceMatrix_->nVoxels_;
      bixelNo_ = the_DoseInfluenceMatrix_->nBixels_;
    }
    else{
      bixelNo_ = the_DoseInfluenceMatrix_->voxelNo_or_bixelNo_[entryNo_];
    }
  }
  else {
    // The matrix is not transposed
    bixelNo_ = bixelNo_or_voxelNo;
    entryNo_ = the_DoseInfluenceMatrix_->bixel_start_index_[bixelNo_];
    // Set to end of matrix if the asked for Bixel doesn't have any entries
    if(the_DoseInfluenceMatrix_->bixel_start_index_[bixelNo_+1]==entryNo_) {
      entryNo_ = the_DoseInfluenceMatrix_->nEntries_;
      voxelNo_ = the_DoseInfluenceMatrix_->nVoxels_;
      bixelNo_ = the_DoseInfluenceMatrix_->nBixels_;
    }
    else {
      voxelNo_ = the_DoseInfluenceMatrix_->voxelNo_or_bixelNo_[entryNo_];
    }
  }
}


// ------------------------------------------------------
// Functions for: DoseInfluenceMatrix::non_const_iterator
// ------------------------------------------------------

/**
 * Constructor
 *
 * @param rhs The DoseInfluenceMatrix to iterate over
 */
DoseInfluenceMatrix::non_const_iterator::non_const_iterator(
    DoseInfluenceMatrix& rhs)
  : the_DoseInfluenceMatrix_(&rhs)
  , voxelNo_(0)
  , bixelNo_(0)
  , entryNo_(0)
{
  if(the_DoseInfluenceMatrix_->is_transposed()) {
    // The matrix is transposed, so we need to find the first voxelNo
    bixelNo_ = the_DoseInfluenceMatrix_->voxelNo_or_bixelNo_[entryNo_];
    while(voxelNo_ < the_DoseInfluenceMatrix_->nVoxels_ &&
        the_DoseInfluenceMatrix_->voxel_start_index_[voxelNo_+1]==entryNo_) {
      voxelNo_++;
    }
  }
  else {
    // The matrix is not transposed, so we need to find the first bixelNo
    voxelNo_ = the_DoseInfluenceMatrix_->voxelNo_or_bixelNo_[entryNo_];
    while(bixelNo_ < the_DoseInfluenceMatrix_->nBixels_ &&
        the_DoseInfluenceMatrix_->bixel_start_index_[bixelNo_+1]==entryNo_) {
      bixelNo_++;
    }
  }
}

/**
 * Initialize at either a row or a column of the DoseInfluenceMatrix.
 *
 * @param the_DoseInfluenceMatrix The DoseInfluenceMatrix to iterate over
 * @param bixelNo_or_voxelNo The bixelNo if the DoseInfluenceMatrix is
 * transposed or the voxelNo if it isn't.
 */
DoseInfluenceMatrix::non_const_iterator::non_const_iterator(
    DoseInfluenceMatrix& the_DoseInfluenceMatrix,
    const unsigned int bixelNo_or_voxelNo)
  : the_DoseInfluenceMatrix_(&the_DoseInfluenceMatrix)
  , voxelNo_(0)
  , bixelNo_(0)
  , entryNo_(0)
{
  if(the_DoseInfluenceMatrix_->nEntries_ == 0) {
    voxelNo_ = the_DoseInfluenceMatrix_->nVoxels_;
    bixelNo_ = the_DoseInfluenceMatrix_->nBixels_;
    return;
  }

  if(the_DoseInfluenceMatrix_->is_transposed()) {
    // The matrix is transposed, so we are looking for a row
    voxelNo_ = bixelNo_or_voxelNo;
    entryNo_ = the_DoseInfluenceMatrix_->voxel_start_index_[voxelNo_];
    // Set to end of matrix if the asked for Voxel doesn't have any entries
    if(the_DoseInfluenceMatrix_->voxel_start_index_[voxelNo_+1]==entryNo_) {
      entryNo_ = the_DoseInfluenceMatrix_->nEntries_;
      voxelNo_ = the_DoseInfluenceMatrix_->nVoxels_;
      bixelNo_ = the_DoseInfluenceMatrix_->nBixels_;
    }
    else {
      bixelNo_ = the_DoseInfluenceMatrix_->voxelNo_or_bixelNo_[entryNo_];
    }
  }
  else {
    // The matrix is not transposed
    bixelNo_ = bixelNo_or_voxelNo;
    entryNo_ = the_DoseInfluenceMatrix_->bixel_start_index_[bixelNo_];
    // Set to end of matrix if the asked for Bixel doesn't have any entries
    if(the_DoseInfluenceMatrix_->bixel_start_index_[bixelNo_+1]==entryNo_) {
      entryNo_ = the_DoseInfluenceMatrix_->nEntries_;
      voxelNo_ = the_DoseInfluenceMatrix_->nVoxels_;
      bixelNo_ = the_DoseInfluenceMatrix_->nBixels_;
    }
    else {
      voxelNo_ = the_DoseInfluenceMatrix_->voxelNo_or_bixelNo_[entryNo_];
    }
  }
}

/**
 * Get the maximum entry influence
 */
float DoseInfluenceMatrix::get_max_influence() const
{
    float max_influence = 0;


    for(DoseInfluenceMatrix::iterator iter = begin(); iter.not_at_end();
            iter++) {
        if(iter.get_influence() > max_influence) {
            max_influence = iter.get_influence();
        }
    }
}


/**
 * Do importance sampling
 *
 * All entries with influence less than the theshold will be included with
 * probability proportional to the influence.
 *
 * @param threshold The threshold below which the dose is sampled
 */
void DoseInfluenceMatrix::importance_sample(float threshold)
{

    // First change the values of the entries that need changing
    for(DoseInfluenceMatrix::non_const_iterator iter = non_const_begin();
            iter.not_at_end(); iter++) {
        if(iter.get_influence() < threshold) {
            if((iter.get_influence() / threshold)
                    >  float(std::rand()) / float(RAND_MAX)) {
                // Include entry
                iter.set_influence(threshold);
            } else {
                // Exclude entry
                iter.set_influence(0);
            }
        }
    }

    // Next, remove any entries with zero influence
    remove_zero_entries();

    // Finally, remove the blank space
    remove_blank_space();
}
