/**
 * @file CellSurvivalObjective.hpp
 * CellSurvivalObjective Class header
 *
 */

#ifndef CELLSURVIVALOBJECTIVE_HPP
#define CELLSURVIVALOBJECTIVE_HPP

#include "Objective.hpp"
#include "LQUncertaintyModel.hpp"
#include "Voi.hpp"
#include <cmath>

/**
 * Class CellSurvivalObjective is used minimize the total number of tumor cells
 * Input is an initial cell density.
 * Cell kill is modelled by the linear-quadratic model.
 */
class CellSurvivalObjective : public Objective
{

public:

  // Constructor
  CellSurvivalObjective(Voi*  the_voi,
						unsigned int objNo,
						string file_name,
						float maximum_clonogen_density,
						unsigned int nFractions,
						string uncertainty_type,
						float weight,
						unsigned int nVoxels);

  // Virtual destructor
  virtual ~CellSurvivalObjective();
  
  //
  // IntegralTumorDensityObjective specific functions
  //
  
  unsigned int get_nVoxels() const;

  //
  // Functions reimplemented from Objective
  //
  
  /// Prints a description of the objective
  //virtual void printOn(std::ostream& o) const;
  
  /// calculate objective
  double calculate_objective(const BixelVector & beam_weights,
			     DoseInfluenceMatrix & Dij,
			     bool use_voxel_sampling,
			     double &estimated_ssvo);
  
  /// calculate objective from dose vector
  double calculate_dose_objective(const DoseVector & the_dose);

  bool supports_gradient() const {return true;};
  
  /// calculate objective and gradient
  double calculate_objective_and_gradient(
        const BixelVector & beam_weights,
        DoseInfluenceMatrix & Dij,
        BixelVectorDirection & gradient,
        float gradient_multiplier,
        bool use_voxel_sampling,
        double &estimated_ssvo);

  /// calculate objective from surviving fraction
  virtual double calculate_obj_from_survival(vector<double> & survival) = 0;

  /// calculate derivative of objective with respect to surviving cells
  virtual double calculate_d_obj_by_survival(vector<double> & survival, unsigned int scenarioNo) = 0;

protected:
  // Default constructor not allowed
  CellSurvivalObjective();
  
  /// the corresponding VOI
  Voi *the_voi_;

  /// number of fractions
  unsigned int nFractions_;

  /// biological uncertainty model
  std::auto_ptr<LQUncertaintyModel> lq_uncertainty_;

};


/**
 * optimizes the logarithm of the expected number of surviving cells
 */
class LogSurvivalObjective : public CellSurvivalObjective
{

public:

  // Constructor
  LogSurvivalObjective(Voi*  the_voi,
					   unsigned int objNo,
					   string file_name,
					   float maximum_clonogen_density,
					   unsigned int nFractions,
					   string uncertainty_type,
					   float weight,
					   unsigned int nVoxels);
  
  // Virtual destructor
  //virtual ~LogSurvivalObjective();
  
  /// Prints a description of the objective
  void printOn(std::ostream& o) const;
  
  /// calculate objective from surviving fraction
  double calculate_obj_from_survival(vector<double> & survival);

  /// calculate derivative of objective with respect to surviving cells
  double calculate_d_obj_by_survival(vector<double> & survival, unsigned int scenarioNo);
};


/**
 * optimizes expectation of Poisson TCP model
 */
class PoissonTcpObjective : public CellSurvivalObjective
{

public:

  // Constructor
  PoissonTcpObjective(Voi*  the_voi,
					  unsigned int objNo,
					  string file_name,
					  float maximum_clonogen_density,
					  unsigned int nFractions,
					  string uncertainty_type,
					  float weight,
					  unsigned int nVoxels);
  
  // Virtual destructor
  //virtual ~PoissonTcpObjective();
  
  /// Prints a description of the objective
  void printOn(std::ostream& o) const;

  /// calculate objective from surviving fraction
  double calculate_obj_from_survival(vector<double> & survival);

  /// calculate derivative of objective with respect to surviving cells
  double calculate_d_obj_by_survival(vector<double> & survival, unsigned int scenarioNo);
};

#endif
