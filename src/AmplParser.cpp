/**
 * @file AmplParser.cpp
 * AmplParser class implementation
 *
 */

#include <sstream>
#include "AmplParser.hpp"

/**
 * constructor
 *
 * @param the_plan The plan to be optimized.
 * @param voiNos string containing the numbers of the vois that shall be included
 * @param use_voxel_sampling whether or not to sample the voxels in a voi
 *
 */
AmplParser::AmplParser(
        Plan* the_plan,
        string voiNos,
        bool use_voxel_sampling,
        string voi_sampling_fractions)
    : the_plan_(the_plan)
    , dat_file_name_("")
    , vois_to_include_()
    , voxels_to_include_(the_plan_->get_nVoxels(),false)
    , use_voxel_sampling_(use_voxel_sampling),
    voxel_sampling_fractions_()
    , nVoxels_()
    , nVois_()
    , nVoxelsInVoi_()
    , allVoxels_()
    , VoisVoxels_()
    , nBixels_()
    , nBeams_()
    , allBixels_()
    , allBeams_()
    , nBixelsInBeam_()
    , BeamsBixels_()
{
    cout << "creating AmplParser ..." << endl;

    // set ampl file names
    dat_file_name_ = the_plan_->get_name() + "_ampl.dat";

    // get relevant vois
    if(voiNos=="") {
	for(unsigned int iVoi=0; iVoi<the_plan_->get_nVois(); iVoi++) {
	    vois_to_include_.insert(iVoi);
	}
    }
    else{
	std::istringstream iss(voiNos);
	unsigned int temp;
	while(!(iss >> temp).fail()) {
	    // check number
	    if(temp>=the_plan_->get_nVois()) {
		throw(std::runtime_error("trying to include non-existing Voi in Ampl file"));
	    }
	    vois_to_include_.insert(temp);
	    cout << "including Voi " << temp << " ("
		 << the_plan_->get_voi(temp)->get_name() << ")" << endl; 
	}
    }

    // get voi samlping fractions (we already know that the voi numbers can be 
    // read correctly, so we don't test again
    if(voi_sampling_fractions!="") {
        std::istringstream iss(voiNos);
        std::istringstream vs_ss(voi_sampling_fractions);
        unsigned int temp_voiNo;
        float temp_frac;
        while(!(iss >> temp_voiNo).fail() && !(vs_ss >> temp_frac).fail()) {
            // check number
            if(temp_frac < 0 || temp_frac > 1) {
                throw(std::runtime_error("Ampl Voi Sampling fraction must be in [0,1]"));
            }
            voxel_sampling_fractions_[temp_voiNo] = temp_frac;
            cout << "Voi " << temp_voiNo << " sampling fraction "
                << temp_frac << endl; 
        }
    }

    if(use_voxel_sampling &&
            voxel_sampling_fractions_.size() != vois_to_include_.size()) {
        throw(std::runtime_error("Ampl Voxel Sampling fraction must be passed"
                    + string("on the command line")));
    }

    // extract information from the plan
    get_nVois();
    get_VoisVoxels();
    get_nBixels();
    get_allBixels();
    get_nBeams();
    get_allBeams();
    get_nBixelsInBeam();
    get_BeamsBixels();
}


/**
 * get number of voxels
 */
void AmplParser::get_nVois()
{
    char temp[100];

    sprintf(temp,"%d",the_plan_->get_nVois());    

    nVois_.first = "param numVois";
    nVois_.second = string(temp);
}

/**
 * get voxels in each voi
 */
void AmplParser::get_VoisVoxels()
{
    char temp[100];
    pair<string,string> temp_pair;
    unsigned int total_nVox=0;

    nVoxelsInVoi_.first = "param numVoxelsInVoi";
    nVoxelsInVoi_.second = "";

    allVoxels_.first = "set allVoxels";
    allVoxels_.second = "";

    // loop over the vois
    for(unsigned int iVoi=0; iVoi<the_plan_->get_nVois(); iVoi++) {

	Voi * the_voi = the_plan_->get_voi(iVoi);
	unsigned int nVox=0;
	sprintf(temp,"set VoisVoxels[%d]",iVoi+1);
	temp_pair.first = string(temp);
	temp_pair.second = "";

	// skip if not included
	if(vois_to_include_.find(iVoi) != vois_to_include_.end()) {
	    
	    // get sampling fraction
	    float sampling_threshold = 1;
	    if(use_voxel_sampling_) {
		sampling_threshold = voxel_sampling_fractions_[iVoi];
	    }

	    // loop over the voxels in this voi
	    for(unsigned int iVoxel=0; iVoxel<the_voi->get_nVoxels(); iVoxel++) {

		unsigned int voxelNo = the_voi->get_voxel(iVoxel);

		if(use_voxel_sampling_) {
		    float randomNo = (float)std::rand() / (float)RAND_MAX;
		    if(randomNo>sampling_threshold) {
			continue;
		    }
		}

		nVox++;
		sprintf(temp,"%d ",voxelNo+1);
		temp_pair.second += string(temp); 
		if(!voxels_to_include_.at(voxelNo)) {
		    voxels_to_include_.at(voxelNo) = true;
		    allVoxels_.second += string(temp);
		    total_nVox++;
		}
	    }
	}

	// store voxels
	sprintf(temp,"\n%d %d",iVoi+1,nVox);
	nVoxelsInVoi_.second += string(temp);
	VoisVoxels_.push_back(temp_pair);

    }
    // total number of voxels
    sprintf(temp,"%d",total_nVox);    
    nVoxels_.first = "param numVoxels";
    nVoxels_.second = string(temp);
}

/**
 * get number of bixels
 */
void AmplParser::get_nBixels()
{
    char temp[100];

    sprintf(temp,"%d",the_plan_->get_nBixels());    

    nBixels_.first = "param numBeamlets";
    nBixels_.second = string(temp);
}

/**
 * get all bixels
 */
void AmplParser::get_allBixels()
{
    char temp[100];

    allBixels_.first = "set allBeamlets";
    allBixels_.second = "";

    for(unsigned int i=0; i<the_plan_->get_nBixels(); i++) {
	sprintf(temp,"%d ",i+1);    
	allBixels_.second += string(temp);
    }
}

/**
 * get number of bixels
 */
void AmplParser::get_nBeams()
{
    char temp[100];

    sprintf(temp,"%d",the_plan_->get_nBeams());    

    nBeams_.first = "param numBeams";
    nBeams_.second = string(temp);
}

/**
 * get all beams
 */
void AmplParser::get_allBeams()
{
    char temp[100];

    allBeams_.first = "set allBeams";
    allBeams_.second = "";

    for(unsigned int i=0; i<the_plan_->get_nBeams(); i++) {
	sprintf(temp,"%d ",i+1);    
	allBeams_.second += string(temp);
    }
}

/**
 * get number of bixels in each beam
 */
void AmplParser::get_nBixelsInBeam()
{
    char temp[100];

    nBixelsInBeam_.first = "param numBixelsInBeam";
    nBixelsInBeam_.second = "";

    for(unsigned int i=0; i<the_plan_->get_nBeams(); i++) {
	sprintf(temp,"\n%d %d",i+1,the_plan_->get_geometry()->get_nBixels_in_beam(i));    
	nBixelsInBeam_.second += string(temp);
    }
}

/**
 * get number of bixels in each beam
 */
void AmplParser::get_BeamsBixels()
{
    char temp[100];
    pair<string,string> temp_pair;

    for(unsigned int iBeam=0; iBeam<the_plan_->get_nBeams(); iBeam++) {
	sprintf(temp,"set BeamsBixels[%d]",iBeam+1);
	temp_pair.first = string(temp);
	temp_pair.second = "";
	for(unsigned int iBixel=0; iBixel<the_plan_->get_nBixels(); iBixel++) {
	    if(the_plan_->get_geometry()->get_beamNo(iBixel) == iBeam) {
		sprintf(temp,"%d ",iBixel);
		temp_pair.second += string(temp); 
	    }
	}
	BeamsBixels_.push_back(temp_pair);
    }
}


/**
 * write AMPL .dat file
 */
void AmplParser::write_dat_file(string file_prefix)
{
    // Open file
    string file_name = file_prefix + dat_file_name_;
    dat_file_.reset(new std::ofstream(file_name.c_str(), std::ios::out));

    if(!dat_file_->good()) {
	throw(std::runtime_error("Error opening file for writing"));
    }

    cout << "writing plan to ampl .dat file" << endl;

    // number of voxels
    *dat_file_ << nVoxels_.first << ":= " << nVoxels_.second << ";" << endl;

    // number of vois
    *dat_file_ << nVois_.first << ":= " << nVois_.second << ";" << endl;

    // number of bixels
    *dat_file_ << nBixels_.first << ":= " << nBixels_.second << ";" << endl;

    // number of beams
    *dat_file_ << nBeams_.first << ":= " << nBeams_.second << ";" << endl;

    // number of bixels in each beams
    *dat_file_ << nBixelsInBeam_.first << ":= " << nBixelsInBeam_.second << ";" << endl;

    // number of voxels per voi
    *dat_file_ << nVoxelsInVoi_.first << ":= " << nVoxelsInVoi_.second << ";" << endl;

    // voxels in each voi
    for(unsigned int i=0; i<VoisVoxels_.size(); i++) {
	*dat_file_ << VoisVoxels_[i].first << ":= " << VoisVoxels_[i].second << ";" << endl;
    }

    // all voxels
    *dat_file_ << allVoxels_.first << ":= " << allVoxels_.second << ";" << endl;

    // all beams
    *dat_file_ << allBeams_.first << ":= " << allBeams_.second << ";" << endl;

    // bixels in each beam
    for(unsigned int i=0; i<BeamsBixels_.size(); i++) {
	*dat_file_ << BeamsBixels_[i].first << ":= " << BeamsBixels_[i].second << ";" << endl;
    }

    // all bixels
    *dat_file_ << allBixels_.first << ":= " << allBixels_.second << ";" << endl;

    // Dij matrix
    *dat_file_ << "param D:= ";
    char temp[100];
    DoseInfluenceMatrix::iterator iter = the_plan_->get_dose_influence_matrix()->begin();
    while(iter.not_at_end()) {
	if(voxels_to_include_[iter.get_voxelNo()]) {
	    sprintf(temp,"\n%d %d %e",iter.get_voxelNo()+1,iter.get_bixelNo()+1,iter.get_influence());
	    *dat_file_ << string(temp);
	}
	iter++;
    }
    *dat_file_ << ";";
}

