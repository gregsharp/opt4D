/**
 * @file: LinearMeanObjective.cpp
 * LinearMeanObjective Class implementation.
 *
 */


#include "LinearMeanObjective.hpp"

/**
 * Constructor
 */
LinearMeanObjective::LinearMeanObjective(Voi*  the_voi,
					 unsigned int objNo,
					 bool minimize,
					 float weight)
    : Objective(objNo,weight)
    , the_voi_(the_voi)
    , minimize_(minimize)
    , mean_dose_computer_()
{
}

/**
 * Destructor
 */
LinearMeanObjective::~LinearMeanObjective()
{
}

/**
 * initialize
 */
void LinearMeanObjective::initialize(DoseInfluenceMatrix& Dij)
{
    mean_dose_computer_.initialize(the_voi_,Dij);
    is_initialized_ = true;
}
  
/**
 * Prints a description of the objective on the given stream
 *
 * @param o The output stream to write to
 */
void LinearMeanObjective::printOn(std::ostream& o) const
{
    o << "OBJ " << objNo_ << ": "; 
    o << "VOI " << the_voi_->get_voiNo() << "(" << the_voi_->get_name() << ")";
    if(minimize_) {
	o << "\tminimizing mean dose" << endl;
    }
    else {
	o << "\tmaximizing mean dose" << endl;
    }
}

/**
 * calculate objective
 */
double LinearMeanObjective::calculate_objective(
    const BixelVector & beam_weights,
    DoseInfluenceMatrix & Dij,
    bool use_voxel_sampling,
    double &estimated_ssvo)
{
  float dose = mean_dose_computer_.calculate_mean_dose(beam_weights);
  return(weight_*dose);
}

/**
 * calculate objective from dose vector
 */
double LinearMeanObjective::calculate_dose_objective(const DoseVector & the_dose)
{
  unsigned int nVoxels = get_nVoxels();

  // loop over all voxels in this VOI and calculate mean dose
  float dose = 0;
  for(size_t iVoxel=0; iVoxel<nVoxels; iVoxel++) {
    dose += the_dose.get_voxel_dose(the_voi_->get_voxel(iVoxel));
  }

  return (weight_*dose/(float)nVoxels);
}

/**
 * prepare constraints for commercial solver interface
 */
void LinearMeanObjective::add_to_optimization_data_set(GenericOptimizationData & data, DoseInfluenceMatrix & Dij) const
{
  cout << "Creating generic optimization data for OBJ " << get_objNo() << endl;

  // only need to add the the coefficients in the objective
  for(unsigned int iBixel = 0; iBixel<Dij.get_nBixels(); iBixel++) {
    data.obj_coeffs_[iBixel] += weight_ * (mean_dose_computer_.mean_dose_contribution_[iBixel]); 
  }
}


/**
 * calculate objective and gradient
 */
double LinearMeanObjective::calculate_objective_and_gradient(const BixelVector & beam_weights,
															 DoseInfluenceMatrix & Dij,
															 BixelVectorDirection & gradient,
															 float gradient_multiplier,
															 bool use_voxel_sampling,
															 double &estimated_ssvo)
{
  for(unsigned int iBixel = 0; iBixel<Dij.get_nBixels(); iBixel++) {
	if(beam_weights[iBixel] > 0) {
	  gradient[iBixel] += gradient_multiplier * weight_ * (mean_dose_computer_.mean_dose_contribution_[iBixel]); 
	}
  }

  float dose = mean_dose_computer_.calculate_mean_dose(beam_weights);
  return(weight_*dose);
}


