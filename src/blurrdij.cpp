/**
 * @file blurrdij.cpp
 * The main file for blurrdij.
 *
 * blurrs the dose distribution of each beamlet
 * calculates an 'expected Dij' for motion applications
 *
 */

/**
 * \par Welcome to blurrdij
 *
 * blurrs the dose distribution of each beamlet
 * calculates an 'expected Dij' for motion applications
 */

#include <iostream>
using std::cout;
using std::cerr;
using std::endl;

#include <iomanip>
#include <exception>
#include <fstream>
#include <limits>
#include <tclap/CmdLine.h>
#include <string>
using std::string;

#include "DijFileParser.hpp"
#include "Geometry.hpp"
#include "DoseVector.hpp"


//void zblurr_dijfile(string in_file_name,
//		   string out_file_name,
//		   vector<int> voxelshifts, 
//		   vector<float> pdf);
void yblurr_dijfile(Geometry *geometry, 
		    string in_file_name,
		    string out_file_name,
		    unsigned int nVoxel_shift); 


/**
 * Main function to interpret command line options and run program.
 */
int main(int argc, char* argv[])
{

    // Wrap everything in a try block to catch TCLAP exceptions.
    try {

        /*
         * Defining the command line syntax
         * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
         *
         * We define the command line object, and add various switches and 
         * value arguments to it.  The CmdLine object parses the argv array 
         * based on the Arg objects that it contains.
         */

        // Define the command line object and insert a message
        // that describes the program. This is printed last in the help text.  
        // The second argument is the delimiter (usually space) and the last 
        // one is the version number.   
        TCLAP::CmdLine cmd("convertdij voxel coordinate system conversion tool for dij",
                ' ', "0.1");

        /*
         * TCLAP syntax
         * ^^^^^^^^^^^^
         *          
         * TCLAP reads two kinds of command line parameters, value arguments 
         * and flags.  Value argments are used to pass data to the program 
         * (such as file names).  Flags are used to pass true or false 
         * information to the program.
         *
         * SwitchArg
         * ---------
         * Switches are used to tell the program something true or false, but 
         * must be false by default, so the default behavior comes from not 
         * using any flags at all.
         *
         * SwitchArg constructor:
         *
         * TCLAP::SwitchArg::SwitchArg  ( const std::string &   flag,
         *                                const std::string &   name,
         *                                const std::string &   desc,
         *                                CmdLineInterface &    parser)
         *
         * Parameters:
         *     flag   - The one character flag that identifies this argument
         *              on the command line.
         *     name   - A one word name for the argument. Can be used as a
         *              long flag on the command line.
         *     desc   - A description of what the argument is for or does.
         *     parser - A CmdLine parser object to add this Arg to
         *
         *
         * ValueArg
         * --------
         * Value arguments can read any type data into a variable that you 
         * create.  Most arguments use this format.
         *
         * ValueArg constructor:
         *
         * template<class T>
         * TCLAP::ValueArg< T >::ValueArg (const std::string &  flag,
         *                                 const std::string &  name,
         *                                 const std::string &  desc,
         *                                 bool                 req,
         *                                 T                    value
         *                                 const std::string &  typeDesc,
         *                                 CmdLineInterface &   parser)
         *
         * Parameters:
         *     flag     - The one character flag that identifies this argument
         *                on the command line.
         *     name     - A one word name for the argument. Can be used as a
         *                long flag on the command line.
         *     desc     - A description of what the argument is for or does.
         *     req      - Whether the argument is required on the command line.
         *     value    - The default value assigned to this argument if it is
         *                not present on the command line.
         *     typeDesc - A short, human readable description of the type that
         *                this object expects. This is used in the generation 
         *                of the USAGE statement. The goal is to be helpful to 
         *                the end user of the program.
         *      parser  - A CmdLine parser object to add this Arg to
         *
         * UnlabledValueArg
         * ----------------
         *
         * The plan file name is read by an UnlabledValueArg, so any argument 
         * that is not processed by something else will be assumed to be the 
         * plan file name.  Don't add any other UnlabledValueArg arguments!!!
         *
         * UnlabledValueArg constructor:
         * 
         * template<class T>
         * TCLAP::ValueArg< T >::UnlabledValueArg (
         *                                 const std::string &  name,
         *                                 const std::string &  desc,
         *                                 bool                 req,
         *                                 T                    value
         *                                 const std::string &  typeDesc,
         *                                 CmdLineInterface &   parser)
         *
         *
         * MultiArg
         * --------
         *
         * If several values may be passed in for one argument, use a MultiArg, 
         * which returns a vector of values instead of a single one.
         *
         * MultiArg syntax:
         *
         * template<class T>
         * TCLAP::MultiArg< T >::MultiArg (const std::string &  flag,
         *                                 const std::string &  name,
         *                                 const std::string &  desc,
         *                                 bool                 req,
         *                                 const std::string &  typeDesc,
         *                                 CmdLineInterface &   parser)
         *
         * UnlabeldMultiArg
         * ----------------
         *
         *  Syntax:
         *
         *  template<class T>
         *  TCLAP::UnlabledMultiArg< T >::UnlabeledMultiArg (
         *              const std::string & name,
         *              const std::string & desc,
         *              bool                req,
         *              const std::string & typeDesc,
         *              CmdLineInterface &  parser,
         *              bool                ignoreable=false)
 	 *
         */

        /*
         * Add General options to cmd
         */

        // Input file prefix in case of processing a group of files
        TCLAP::ValueArg<std::string> in_file_prefix_arg("i",
                "infile_prefix","Input file prefix",
                false, "","dij file prefix",cmd);

        // Output file prefix in case of processing a group of files
        TCLAP::ValueArg<std::string> out_file_prefix_arg("o",
                "outfile_prefix","Output file prefix (defaults to expected_)",
                false, "", "dij file prefix",cmd);

        // Output file prefix in case of processing a group of files
        TCLAP::ValueArg<int> nVoxel_shift_arg("",
                "nvoxel_shift","number of voxels a point is shifted up and down",
                false, 1, "any positive float",cmd);

        // dimension information file
        TCLAP::ValueArg<std::string> dif_file_name_arg("d",
                "dif_file_name","dif file name",
                false, "","dif file name",cmd);

        // Parse the command line
        cmd.parse(argc, argv);

	// check if required arguments are provided
	if(!in_file_prefix_arg.isSet() || !dif_file_name_arg.isSet()) {
	    cout << "you have to provide the dif file name and the dij file root" << endl;
	    throw(std::runtime_error("input arguments missing"));
	}

	string in_file_prefix = in_file_prefix_arg.getValue();

	// Find out file prefix
	string out_file_prefix;
	if(out_file_prefix_arg.isSet()) {
	  out_file_prefix = out_file_prefix_arg.getValue();
	} else {
	  out_file_prefix = "expected_" + in_file_prefix;
	}
	
	// create geometry class to read dif file
        string dif_file_name = dif_file_name_arg.getValue();
	Geometry geometry = Geometry(dif_file_name);

	// number of voxels to shift
	int nVoxel_shift = 1;
	if(nVoxel_shift_arg.isSet()) {
	    nVoxel_shift = nVoxel_shift_arg.getValue();
	}
	
	unsigned int instanceNo = 0;
	unsigned int beamNo = 0;
	while(true) {
	  
	  // Find file names
	  string in_file_name;
	  string out_file_name;
	  char cbeam[10];
	  sprintf(cbeam,"%d",beamNo + 1);
	  if(instanceNo == 0) {
	    in_file_name = in_file_prefix
	      + "_" + string(cbeam) + ".dij";
	    out_file_name = out_file_prefix
	      + "_" + string(cbeam) + ".dij";
	  } else {
	    char cinstance[10];
	    sprintf(cinstance,"%d",instanceNo);
	    in_file_name = in_file_prefix
	      + "_" + string(cinstance) 
	      + "_" + string(cbeam) + ".dij";
	    out_file_name = out_file_prefix
	      + "_" + string(cinstance) 
	      + "_" + string(cbeam) + ".dij";
	  }
	  
	  try {
	    yblurr_dijfile(&geometry, in_file_name, out_file_name, nVoxel_shift);
	    beamNo++;
	  }
	  catch (DijFileParser::exception & e) {
	    if(beamNo == 0) {
	      // We're done since we're at an instance with no beams
	      break;
	    } else {
	      // We found at least one beam before failing, so go to 
	      // next instance
	      beamNo = 0;
	      instanceNo++;
	    }
	  }
	}
    }
    catch (TCLAP::ArgException &e) { // catch any exceptions
      std::cerr << "error: " << e.error() << " for arg " << e.argId()
		<< std::endl;
    }
    catch (std::exception & e) { // Catch other exceptions
      std::cerr << "Error caught in blurrdij.cpp, main(): " << e.what()
		<< std::endl;
    }
    
    return(0);
}



/**
 * blurr Dij in sup-inf direction (y-direction in konrad format)
 */
void yblurr_dijfile(Geometry *geometry, 
		    string in_file_name,
		    string out_file_name,
		    unsigned int nVoxel_shift)
{
    bool verbose = true;

    DijFileParser::header in_head;
    DijFileParser::header out_head;
    DijFileParser::bixel in_bixel;
    DijFileParser::bixel out_bixel;

    // Open infile for reading
    DijFileParser in_file(in_file_name);

    // get header
    in_head = in_file.get_header();

    cout << "working on " << in_file_name << endl; 
    cout << "number of beamlets: " << in_head.npb_ << endl;
    cout << "amount of blurring: " << nVoxel_shift << " (number of voxels)" << endl;

    // copy header
    out_head = in_head;

    // Open outfile for writing
    DijFileParser out_file(out_file_name, out_head);

    // create dose vector
    DoseVector dose_vector(geometry->get_nVoxels());

    // create mask for contained voxels
    vector<bool> inDij(geometry->get_nVoxels());

    // Process each bixel
    for(int iBixel = 0; iBixel < in_head.npb_; iBixel++) {

        // Read bixel
        in_file.read_next_bixel(in_bixel);

	// initialize new bixel
	out_bixel = in_bixel;

	// fill dose cube
	dose_vector.reset_dose();
	inDij.assign(inDij.size(),false);
	for (unsigned int iEntry = 0; iEntry<in_bixel.voxelNo_.size(); iEntry++) {
	  
	  dose_vector.add_dose( in_bixel.voxelNo_[iEntry], in_bixel.value_[iEntry] );
	  inDij.at(in_bixel.voxelNo_[iEntry]) = true;
	}

	// identify locations of shifted voxels to be included in the Dij
	for (unsigned int iEntry = 0; iEntry<in_bixel.voxelNo_.size(); iEntry++) {

	  int voxelNo = in_bixel.voxelNo_[iEntry];
	  Voxel_Subscripts subscripts = geometry->convert_to_Voxel_Subscripts( VoxelIndex(voxelNo) );
	  
	  // loop over shifts
	  int iShift = -1*nVoxel_shift;
	  while (iShift<(int)(nVoxel_shift+1)) {

	    Voxel_Subscripts subscripts_shifted;
	    subscripts_shifted = subscripts;
	    subscripts_shifted.iY_ = subscripts_shifted.iY_ + iShift;

	    if (geometry->is_inside(subscripts_shifted)){
	      unsigned int voxelNo_shifted = geometry->convert_to_VoxelIndex(subscripts_shifted).index_;

	      if(!inDij.at(voxelNo_shifted)) {
		inDij.at(voxelNo_shifted) = true;
		out_bixel.voxelNo_.push_back(voxelNo_shifted);
		out_bixel.value_.push_back(0);
		out_bixel.nVox_++;
	      }
	    }
	    iShift = iShift + 1;
	  } // end loop over iShift
	}

	// now calculate influence values
	for (unsigned int iEntry = 0; iEntry<out_bixel.voxelNo_.size(); iEntry++) {

	  int voxelNo = out_bixel.voxelNo_[iEntry];
	  Voxel_Subscripts subscripts = geometry->convert_to_Voxel_Subscripts( VoxelIndex(voxelNo) );
	  float tmpdose = 0; 
	  
	  // loop over shifts
	  int iShift = -1*nVoxel_shift;
	  while (iShift<(int)(nVoxel_shift+1)) {

	    Voxel_Subscripts subscripts_shifted;
	    subscripts_shifted = subscripts;
	    subscripts_shifted.iY_ = subscripts_shifted.iY_ + iShift;

	    if ( !geometry->is_inside(subscripts_shifted) ){
	      // outside dose grid, return dose of unshifted voxel as an approximation
	      tmpdose = tmpdose + dose_vector.get_voxel_dose(voxelNo);
	    }
	    else {
	      // get shifted voxel index
	      unsigned int voxelNo_shifted = geometry->convert_to_VoxelIndex(subscripts_shifted).index_;
	      tmpdose = tmpdose + dose_vector.get_voxel_dose(voxelNo_shifted);
	    }
	    iShift = iShift + 1;
	  } // end loop over iShift

	  out_bixel.value_[iEntry] = (short)(tmpdose/(float)(2*nVoxel_shift+1));
	} // end loop over iEntry

        // Write out bixel
        out_file.write_next_bixel(out_bixel);
    
    } // end loop over bixels

    if(verbose) {
      cout << "last bixel:" << endl;
      cout << "original: " << in_bixel.nVox_ << " voxels" << endl;
      cout << "blurred: " << out_bixel.nVox_ << " voxels" << endl;
    }

    // Completed successfully, so report success
    cout << "done" << endl; 
}
  




/**
 * blurr Dij in sup-inf direction (y-direction in konrad format)
 */
void yblurr_dijfile_old(Geometry *geometry, 
		    string in_file_name,
		    string out_file_name,
		    unsigned int nVoxel_shift)
{
    DijFileParser::header in_head;
    DijFileParser::header out_head;
    DijFileParser::bixel in_bixel;
    DijFileParser::bixel out_bixel;

    // Open infile for reading
    DijFileParser in_file(in_file_name);

    // get header
    in_head = in_file.get_header();

    cout << "working on " << in_file_name << endl; 
    cout << "number of beamlets: " << in_head.npb_ << endl;

    // copy header
    out_head = in_head;

    // Open outfile for writing
    DijFileParser out_file(out_file_name, out_head);

    // create dose vector
    DoseVector dose_vector(geometry->get_nVoxels());

    // scenario weights
    //float prob = 1.0 / (float)nVoxel_shift;

    // voxel subscript buffer
    //    Voxel_Subscripts subscripts;

    cout << nVoxel_shift << endl;

    // Process each bixel
    for(int iBixel = 0; iBixel < in_head.npb_; iBixel++) {

        // Read bixel
        in_file.read_next_bixel(in_bixel);

	out_bixel = in_bixel;

	// zero dose
	dose_vector.reset_dose();

	// fill dose cube
	for (unsigned int iEntry = 0; iEntry<in_bixel.voxelNo_.size(); iEntry++) {
	  
	  dose_vector.add_dose( in_bixel.voxelNo_[iEntry], in_bixel.value_[iEntry] );
	  
	}

	for (unsigned int iEntry = 0; iEntry<in_bixel.voxelNo_.size(); iEntry++) {

	  int voxelNo = in_bixel.voxelNo_[iEntry];
	  Voxel_Subscripts subscripts = geometry->convert_to_Voxel_Subscripts( VoxelIndex(voxelNo) );
	  float tmpdose = 0; 
	  
	  // cout << iBixel << voxelNo << endl; 
	  int iShift = -1*nVoxel_shift;

	  while (iShift<(int)(nVoxel_shift+1)) {

	    Voxel_Subscripts subscripts_shifted;
	    subscripts_shifted = subscripts;
	    subscripts_shifted.iY_ = subscripts_shifted.iY_ + iShift;

	    //cout << iBixel << voxelNo << iShift << (int)(nVoxel_shift+1) << endl; 
	    
	    if ( !geometry->is_inside(subscripts_shifted) ){
	      //cout << "if " << iBixel << voxelNo << iShift << (int)(nVoxel_shift+1) << endl; 

	      // outside dose grid, return dose of unshifted voxel as an approximation
	      tmpdose = tmpdose + dose_vector.get_voxel_dose(voxelNo);
	    }
	    else {
	      //cout << "else " << iBixel << voxelNo << iShift << (int)(nVoxel_shift+1) << endl; 

	      // get shifted voxel index
	      unsigned int voxelNo_shifted = geometry->convert_to_VoxelIndex(subscripts_shifted).index_;
	      	      
	      //cout << "else " << iBixel << voxelNo << voxelNo_shifted << endl; 

	      //if (geometry->is_air(voxelNo_shifted)) {
	      //cout << "elseif " << iBixel << voxelNo << iShift << (int)(nVoxel_shift+1) << endl; 

		// outside patient, return dose of unshifted voxel as an approximation
		//tmpdose += dose_vector.get_voxel_dose(voxelNo);
	      //}
	      //else {
	      //cout << "elseelse " << iBixel << voxelNo << iShift << (int)(nVoxel_shift+1) << endl; 
		// get dose in shifted voxel
		tmpdose = tmpdose + dose_vector.get_voxel_dose(voxelNo_shifted);
		//cout << iShift << " " << tmpdose << endl; 
		//}
	    }
	    iShift = iShift + 1;
	  } // end loop over iShift

	  out_bixel.value_[iEntry] = (short)(tmpdose/(float)(2*nVoxel_shift+1));
	  out_bixel.voxelNo_[iEntry] = in_bixel.voxelNo_[iEntry]; 

	} // end loop over iEntry

        // Write out bixel
        out_file.write_next_bixel(out_bixel);
    
    } // end loop over bixels

    // Completed successfully, so report success
    cout << "done" << endl; 
}
  

