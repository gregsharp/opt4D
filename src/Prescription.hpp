/* $Id: Prescription.hpp,v 1.1.1.1 2006/03/03 22:06:11 bmartin Exp $ */

/**
 * @file Prescription.hpp
 * Prescription Class header
 */

#ifndef PRESCRIPTION_HPP
#define PRESCRIPTION_HPP

#include <vector>
using std::vector;
#include <string>
using std::string;

/**
 * Class Prescription holds dose prescription for individual organs. DVH and/or
 * EUD prescriptions are allowed.
 *
 * @todo Add the ability for multiple weights per dvh constraint
 */
class Prescription
{

public:
  Prescription();
  ~Prescription();

  void set_weight_over(const float weight);
  void set_weight_under(const float weight);

  inline float get_weight_over()
                { return(weight_over_); };
  inline float get_weight_under()
                { return(weight_under_); };

  inline bool is_DVH_type() { return(is_DVH_); };
  void add_max_DVH_point(const float dose, const float volume);
  void add_min_DVH_point(const float dose, const float volume);

  inline float get_max_DVH_dose(const unsigned int is)
                { return(max_DVH_dose_[is]); };
  inline float get_max_DVH_volume(const unsigned int is)
                { return(max_DVH_volume_[is]); };
  inline float get_min_DVH_dose(const unsigned int is)
                { return(min_DVH_dose_[is]); };
  inline float get_min_DVH_volume(const unsigned int is)
                { return(min_DVH_volume_[is]); };

  inline unsigned int get_max_DVH_points()
                { return(max_DVH_dose_.size()); };
  inline unsigned int get_min_DVH_points()
                { return(min_DVH_dose_.size()); };

  inline bool is_min_type() { return(is_min_); };
  inline bool is_max_type() { return(is_max_); };
  void set_max_dose(const float max_dose);
  void set_min_dose(const float min_dose);

  inline float get_max_dose() { return(max_dose_); };
  inline float get_min_dose() { return(min_dose_); };

  inline bool is_EUD_type() { return(is_EUD_); };
  void set_max_EUD(const float EUD);
  void set_min_EUD(const float EUD);

  inline float get_max_EUD_dose() { return(max_EUD_dose_); };
  // inline float get_max_EUD_p()    { return(max_EUD_p_);    };
  inline float get_min_EUD_dose() { return(min_EUD_dose_); };
  // inline float get_min_EUD_p()    { return(min_EUD_p_);    };



private:
  float weight_over_;
  float weight_under_;

  float is_DVH_, is_min_, is_max_, is_EUD_;      ///< type of prescription
  vector<float> max_DVH_dose_;
  vector<float> max_DVH_volume_;  ///< vector of upper DVH limits
  vector<float> min_DVH_dose_;
  vector<float> min_DVH_volume_;  ///< vector of lower DVH limits
  float max_dose_;   ///< upper minmax limit
  float min_dose_;   ///< lower minmax limit (for target volume)
  float max_EUD_dose_;   ///< upper EUD limit
  // float max_EUD_p_;    
  float min_EUD_dose_;   ///< lower EUD limit (for target volume)
  // float min_EUD_p_; 

};

#endif
