
/**
 * @file VoiParser.cpp
 *
 * Function definitions for VoiParser class.
 *
 *  $Id: VoiParser.cpp,v 1.2 2008/04/11 21:44:41 bmartin Exp $
 */

#include <fstream>
#include <iomanip>
#include <stdexcept>
#include <cstdlib>

#include "VoiParser.hpp"

/**
 * Construct from a vector of vectors of indices (used for writing)
 *
 * @param voi_indice each voi is a vector of indices
 */
VoiParser::VoiParser(
        const vector<vector< unsigned int> > & voi_indices,
        bool include_air,
        unsigned int nVoxels,
        unsigned int nNonair_voxels)
    : voi_indices_(voi_indices),
    include_air_(include_air),
    nVoxels_(nVoxels),
    nNonair_voxels_(nNonair_voxels)
{
}


/**
 * Constructor to read from a file
 *
 * @param file_name The file to read
 */
VoiParser::VoiParser(
        const string & file_name,
        bool include_air)
: voi_indices_(),
    include_air_(include_air),
    nVoxels_(0),
    nNonair_voxels_(0)
{
    read_file(file_name);
}


/**
 * Read a file
 */
void VoiParser::read_file(const string & file_name)
{
    //
    // Clear all class variables
    //

    voi_indices_.resize(0);
    nVoxels_ = 0;
    nNonair_voxels_ = 0;

    //
    // Read file and count the number of voxels in each structure.
    //

    // Attempt to open the file
    std::ifstream infile;
    infile.open(file_name.c_str(), std::ios::in | std::ios::binary);
    if (!infile.is_open()) {
        string temp = "Can't open file: " + file_name;
        std::cout << temp << std::endl;
        throw(std::runtime_error(temp));
    }
    std::cout << "Reading VOI data from file: " << file_name << std::endl;

    // Read each voxel from file
    char aChar;
    unsigned int nVois = 0;
    std::vector<size_t> nVoxelsList;   // Lists the number of voxels per voi
    while(infile.get(aChar)) {
        nVoxels_++;

        // Convert value to volume of intrest index
        unsigned int voiNo = static_cast<unsigned int>(aChar)+1;
        if (voiNo==128) {
            voiNo = 0;           // air
        } else {
            nNonair_voxels_++;   // body
        }

        // Expand list if a new voi is found
        while(nVois <= voiNo) {
            nVois++;
            nVoxelsList.push_back(0);
        }

        // Count new voxel in voi
        nVoxelsList[voiNo]++;
    }

    // Stop if there aren't any vois present
    if(nVois == 0) {
        return;
    }

    //
    // Done with first pass of file, so go back to start.
    //
    infile.close();
    infile.clear();
    infile.open(file_name.c_str(), std::ios::in | std::ios::binary);
    if (infile.bad()) {
        std::cout << "Can't open file: " << file_name << std::endl;
        exit(-1);
    }

    //
    // Reserve space for voxels within Vois, excluding air
    //
    voi_indices_.resize(nVois);
    if(include_air_) {
        voi_indices_[0].reserve(nVoxelsList[0]);
    }

    for (unsigned int iVoi=1; iVoi<nVois; iVoi++) {
        voi_indices_[iVoi].reserve(nVoxelsList[iVoi] );
    }

    //
    // Read and store voxel indices for each voi
    //

    unsigned int iVoxel = 0;
    while(infile.get(aChar)) {
        unsigned int voiNo = (int)aChar+1;
        if (voiNo==128) {
            voiNo = 0; // air
            if(include_air_) {
                voi_indices_[voiNo].push_back(iVoxel);
            }
        }
        else {
            voi_indices_[voiNo].push_back(iVoxel);
        }

        iVoxel++;
    }
    infile.close();

    // Print total numer of volumes of interest found
    std::cout << "Found " << nVois <<
        " Voi (including non-classified 0-VOI)" << std::endl;
    std::cout << "Found " << nNonair_voxels_ << " non-air voxels out of "
        << nVoxels_ << " voxels" << std::endl;
}


// Write a voi file
void VoiParser::write_file(
        const string & file_name
        ) const
{
}

