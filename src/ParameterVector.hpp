
/**
 * @file ParameterVector.hpp
 * ParameterVector Class Definition.
 * <pre>
 * Ver
 * </pre>
 */

#ifndef PARAMETERVECTOR_HPP
#define PARAMETERVECTOR_HPP

#include <vector>
using std::vector;
#include <string>
using std::string;
#include <assert.h>
#include <stdexcept>
#include <exception>
#include <iostream>
#include <memory>
#include <limits>
#include <stdlib.h>     
#include <cstdlib>

//using std::rand;
//using std::RAND_MAX;


// Predefine class before including others
class ParameterVector;
class BixelVector;
class BixelVectorDirection;

#include "ParameterVectorDirection.hpp"

enum ParameterVectorType { BIXEL_VECTOR_TYPE, DAO_VECTOR_TYPE, DIRECT_VECTOR_TYPE, SQRRT_BIXEL_VECTOR_TYPE };

/// Print ParameterType
inline std::ostream& operator<< ( std::ostream& o,  const enum ParameterVectorType pType ) {
    switch ( pType ) {
        case BIXEL_VECTOR_TYPE:
            return o << "Bixel Vector";
        case DAO_VECTOR_TYPE:
            return o << "Dao Vector";
        case DIRECT_VECTOR_TYPE:
            return o << "Direct Parameter Vector";
        case SQRRT_BIXEL_VECTOR_TYPE:
            return o << "Square Root Bixel Vector";
        default:
            throw std::logic_error("Invalid enum value.");
    }
};

/**
 * Class ParameterVector handles individual parameters used in an optimization.
 */
class ParameterVector {

  public:

    /// Constructor
    ParameterVector( const unsigned int &nParams = 0, const float &value = 0.0f, const float &lowBound = 0.0f,
        const float &highBound = std::numeric_limits<float>::infinity(), const bool &isActive = true,
        const float &step_size = 0.0f );

    /// Destructor
    virtual ~ParameterVector() {};

    // Overloaded operators
    float& operator[]( const unsigned ip );
    float operator[]( const unsigned ip ) const;
    /*ParameterVector operator+=( const ParameterVector &rhs );
    ParameterVector operator+=( const ParameterVectorDirection &rhs );
    ParameterVector& operator-=( const ParameterVector &rhs );
    ParameterVector& operator-=( const ParameterVectorDirection &rhs );
    ParameterVector& operator*=( const float& factor );
    ParameterVector& operator*=( const double& factor );
    ParameterVector& operator*=( const vector<float>& rhs );
    ParameterVector& operator*=( const vector<double>& rhs );
    ParameterVector& operator*=( const ParameterVector& rhs );
    ParameterVector& operator++();
    ParameterVector& operator--();*/


    /**
     * Test if parameter vectors are equal.
     * Test will return false if there are different number of parameters or if there are any differences in :
     * values, lower bounds OR high bounds.
     * I.E. The values in the parameters can be the same but the test will return false if the bounds are different.
     */
    inline virtual bool operator==( const ParameterVector& rhs ) const {
        return ( nParams_ == rhs.nParams_ ) && ( value_ == rhs.value_ ) && ( lowBound_ == rhs.lowBound_ ) &&
                ( highBound_ == rhs.highBound_ ) && ( isActive_ == rhs.isActive_ );
    };


    /**
     * Test if parameter vectors are equal.
     * Test will return true if there are different number of parameters or if there are any differences in :
     * values, lower bounds OR high bounds.
     * I.E. The values in the parameters can be the same and the test will still return true if the bounds are different.
     */
    inline virtual bool operator!=( const ParameterVector& rhs ) const {
        return !( operator==( rhs ) );
    };

    virtual float get_value( const int ip ) const;
    virtual float get_lowBound( const int ip ) const;
    virtual float get_highBound( const int ip ) const;
    virtual bool get_isActive( const int ip ) const;
    virtual float get_step_size( const int ip ) const;

    virtual void set_value( const int ip, const float value );
    virtual void set_lowBound( const int ip, const float lowBound );
    virtual void set_highBound( const int ip, const float highBound );
    virtual void set_isActive( const int ip, const bool isActive );
    virtual void set_step_size( const int ip, const float step_size );

    virtual void add_value( const int ip, const float value_inc );
    virtual void add_value( const ParameterVector &pv );
    virtual void add_value( const ParameterVectorDirection &pvd );
    virtual void add_scaled_parameter_vector( const ParameterVector &pv, const float scale_factor );
    virtual void add_scaled_parameter_vector( const ParameterVector &pv, const double scale_factor );
    virtual void add_scaled_parameter_vector_direction( const ParameterVectorDirection &pvd, const float scale_factor );
    virtual void add_scaled_parameter_vector_direction( const ParameterVectorDirection &pvd, const double scale_factor );

    virtual void clear_values();
    virtual void scale_values( const float scale_factor );
    virtual void scale_values( const double scale_factor );

    // TODO (dualta#1#): The intensity functions need to be added but only used for dose normalisation purposes.
    //virtual float get_intensity(const int pi) const = 0;
    virtual void scale_intensities( float scale_factor ) = 0;
    virtual void set_zero_intensities() = 0;
    virtual void set_unit_intensities() = 0;

    /**
     * Swap the contents of two ParameterVectors.
     * This is very fast.
     *
     * @param rhs The ParameterVector to swap with the current one
     */
    virtual void swap( ParameterVector & rhs ) {
        unsigned int temp_nParams = nParams_;
        nParams_ = rhs.nParams_;
        rhs.nParams_ = temp_nParams;

        value_.swap( rhs.value_ );
        lowBound_.swap( rhs.lowBound_ );
        highBound_.swap( rhs.highBound_ );
        isActive_.swap( rhs.isActive_ );
        step_size_.swap( rhs.step_size_ );
    }


    /**
     * Add noise to parameter values.
     */
    inline void add_noise( float scale_factor ) {
        assert(scale_factor >= 0);
        for ( unsigned int pi = 0; pi < get_nParameters(); pi++ ) {
		    value_[pi] += scale_factor*rand()/(float)RAND_MAX;
        }
    };

	/**
     * Get number of parameters in the vector.
     *
     * @return the number of paramaters in the vector.
     */
    inline unsigned int get_nParameters() const {
        return nParams_;
    }


    /**
     * Get number of bixels in the bixel vector required to represent this parameter vector.
     *
     * @return the number of bixels in the equivalent bixel vector.
     */
    virtual unsigned int get_nBixels() const = 0;


    /**
     * Get number of parameters in the vector.
     *
     * @return the number of paramaters in the vector.
     */
    inline unsigned int size() const {
        return nParams_;
    }


    /**
     * Find out if the parameter set is physically imposible.
     * Check for parameter values which are lower than lower bound or higher than higher bound.
     *
     * @returns true if the parameter vector is impossible to deliver.
     */
    inline virtual bool is_impossible() const {
        for ( unsigned int ip = 0; ip < value_.size(); ip++ )
            if ( value_[ip] < lowBound_[ip] || value_[ip] > highBound_[ip] )
                return true;

        // If we made it this far, no entries are impossible
        return false;
    }

    /**
     * Make sure the parameter values are within the permissible range
     */
    virtual void makePossible() {
        for ( unsigned int iP = 0; iP < nParams_; iP++ ) {
            if ( value_[iP] < lowBound_[iP] )
                value_[iP] = lowBound_[iP];
            if ( value_[iP] > highBound_[iP] )
                value_[iP] = highBound_[iP];
        }
    }

    /**
     * Get the beam number
     */
    virtual unsigned int get_beamNo( const int &ip ) const = 0;


	/**
     * Get number of non-zero (> epsilon) parameters in the vector.
     */
    inline unsigned int get_nNonzero_parameters(const float epsilon) const;

	/**
     * Get mean parameter value
     */
    inline float get_mean_parameter_value() const;

    /**
     * Get a pointer to a copy of the object.
     * N.B. Careful the calling function is responsible for deleting the returned object from memory.
     */
    virtual std::auto_ptr<ParameterVector> get_copy() const = 0;

    /// Read and write files
    virtual void load_parameter_files( string parameters_file_root ) = 0;
    virtual void load_parameter_file( string parameters_file_name, unsigned int beamNo ) = 0;
    virtual void write_parameter_files( string parameters_file_root ) const = 0;
    virtual void write_parameter_file( string parameters_file_name, const unsigned int beamNo ) const = 0;

    /**
     * Reserve space for a ParameterVector with nParams elements.
     * Requests that the capacity of the allocated storage space for the elements of the vector container be at least
     * enough to hold n elements.
     *
     * @param nParams    The number of elements that the vector is required to store without reallocation.
     */
    virtual void reserve_nParams( const unsigned int nParams ) {
        value_.reserve( nParams );
        lowBound_.reserve( nParams );
        highBound_.reserve( nParams );
        isActive_.reserve( nParams );
        step_size_.reserve( nParams );
    }


    /**
     * Adds a new element at the end of the vector, after its current last element.
     * The content of this new element is initialized with the set value, lowBound, highBound and isActive values.
     *
     * @param value     The value for the new ParameterVector element.
     * @param lowBound  The lower Bound for the new ParameterVector element.
     * @param highBound  The upper Bound for the new ParameterVector element.
     * @param isActive  The activity flag for the new ParameterVector element.
     * @param step_size The default step by which the value should be incremented.
     */
    virtual void push_back_parameter( const float &value, const float &lowBound, const float &highBound,
            const bool &isActive, const float &step_size ) {
        nParams_++;
        value_.push_back( value );
        lowBound_.push_back( lowBound );
        highBound_.push_back( highBound );
        isActive_.push_back( isActive );
        step_size_.push_back( step_size );
    }


	/**
	 * Translate the ParameterVector into a BixelVector
	 * Warning calling function should free returned object.
	 */
	virtual BixelVector * translateToBixelVector() const = 0;


	/**
	 * Translate the BixelVector into a ParameterVector
	 */
	virtual void translateBixelVector(BixelVector & bv) = 0;


	/**
	 * Translate the gradient in terms of bixel parameters to the gradient in terms of
	 * the parameters used to describe plan.
	 */
	virtual ParameterVectorDirection translateBixelGradient( BixelVectorDirection & bvd ) const = 0;

	/**
	 * Translate the hessian in terms of bixel parameters to the hessian in terms of
	 * the parameters used to describe plan.
	 */
	virtual vector<float> translateBixelHessian( vector<float> & hv ) const;


    /**
     * Get the current type of parameter vector
     *
     * @return the parameter vector type as an enum
     */
    virtual ParameterVectorType getVectorType() const = 0;

    /// Print Parameter vector Type
    friend std::ostream& operator<< ( std::ostream& o,  const enum ParameterVectorType pType );

  protected:

    /// number of parameters
    unsigned int nParams_;

    /// parameter value
    vector<float> value_;

    /// parameter minimum allowed value
    vector<float> lowBound_;

    /// parameter maximum allowed value
    vector<float> highBound_;

    /// parameter is active (i.e. is affecting the open aperature).
    vector<bool> isActive_;

    /// parameter default increment.
    vector<float> step_size_;

};


/**
 * Over-ride bracket to access parameter element at index.
 */
inline float& ParameterVector::operator[]( const unsigned ip ) {
    return value_[ip];
}


/**
 * Over-ride bracket to access parameter element at index.
 */
inline float ParameterVector::operator[]( const unsigned ip ) const {
    return value_[ip];
}


//**
// * Overload the prefix version of ++.
// * Increment values by default step size.
// */
//ParameterVector& ParameterVector::operator++()
//{
//    for (unsigned int pi=0; pi<nParams_; pi++)
//    {
//        value_[pi] += step_size_[pi];
//    }
//    return *this;
//}
//
//
//**
// * Overload the prefix version of ++.
// * Increment values by default step size.
// */
//ParameterVector& ParameterVector::operator--()
//{
//    for (unsigned int pi=0; pi<nParams_; pi++)
//    {
//        value_[pi] -= step_size_[pi];
//    }
//    return *this;
//}


/**
 * Get the parameter element value at index.
 */
inline float ParameterVector::get_value( const int ip ) const {
    return value_[ip];
    //assert(value_[ip] >= lowBound_[ip] && value_[ip] <= highBound_[ip]);
}


/**
 * Set the value of the parameter element at an index.
 */
inline void ParameterVector::set_value( const int ip, const float value ) {
    value_[ip] = value;
    //assert(value_[ip] >= lowBound_[ip] && value_[ip] <= highBound_[ip]);
}


/**
 * Get the lower bound for the permitted values of the parameter element at index.
 */
inline float ParameterVector::get_lowBound( const int ip ) const {
    return lowBound_[ip];
}


/**
 * Set the lower bound for the permitted values of the parameter element at index.
 */
inline void ParameterVector::set_lowBound( const int ip, const float lowBound ) {
    lowBound_[ip] = lowBound;
}


/**
 * Get the upper bound for the permitted values of the parameter element at index.
 */
inline float ParameterVector::get_highBound( const int ip ) const {
    return highBound_[ip];
}


/**
 * Set the upper bound for the permitted values of the parameter element at index.
 */
inline void ParameterVector::set_highBound( const int ip, const float highBound ) {
    highBound_[ip] = highBound;
}


/**
 * Get the activity flag for the parameter element at index.
 */
inline bool ParameterVector::get_isActive( const int ip ) const {
    return isActive_[ip];
}


/**
 * Set the activity flag for the parameter element at index.
 */
inline void ParameterVector::set_isActive( const int ip, const bool isActive ) {
    isActive_[ip] = isActive;
}


/**
 * Get the step size for the parameter element at index.
 */
inline float ParameterVector::get_step_size( const int ip ) const {
    return step_size_[ip];
}


/**
 * Set the step size for the parameter element at index.
 */
inline void ParameterVector::set_step_size( const int ip, const float step_size ) {
    step_size_[ip] = step_size;
}


/**
 * Add a value to the parameter element value at index.
 */
inline void ParameterVector::add_value( const int ip, const float value_inc ) {
    value_[ip] += value_inc;
    //assert(value_[ip] >= lowBound_[ip] && value_[ip] <= highBound_[ip]);
}


/**
 * Reset all parameter values to the lower bound (normally zero).
 */
inline void ParameterVector::clear_values() {
    assert(nParams_ == value_.size());
    for ( unsigned int ip = 0; ip < value_.size(); ip++ ) {
        value_[ip] = lowBound_[ip];
    }
}

/**
 * get number of nonzero parameters
 */
inline unsigned int ParameterVector::get_nNonzero_parameters(const float epsilon) const {
    assert(nParams_ == value_.size());
	unsigned int count = 0;
    for ( unsigned int ip = 0; ip < value_.size(); ip++ ) {
	  if( value_[ip] > epsilon ) {
		count++;
	  }
	}
	return count;
}

/**
 * get mean parameter value
 */
inline float ParameterVector::get_mean_parameter_value() const {
    assert(nParams_ == value_.size());
    assert(nParams_ > 0);
	float mean = 0;
    for ( unsigned int ip = 0; ip < value_.size(); ip++ ) {
	  mean += value_[ip];
	}
	return (mean / (float)nParams_);
}


#endif // PARAMETERVECTOR_HPP
