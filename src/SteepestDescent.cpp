
/**
 * @file SteepestDescent.cpp
 * SteepestDescent Class implementation.
 * 
 * $Id: SteepestDescent.cpp,v 1.1 2007/09/20 19:03:08 bmartin Exp $
 */


#include "SteepestDescent.hpp"


/**
 * Constructor: initialize options
 *
 */
SteepestDescent::SteepestDescent(
	SteepestDescent::options options, ///< The options
	unsigned int nParams ///< The number of parameters
	)
:   Optimizer(nParams),
    options_(options)
{
}


/*
 * SteepestDescent functions
 * ^^^^^^^^^^^^^^^^^^^^^^^^^
 */

/// Calculate the direction based on just the gradient
void SteepestDescent::calculate_direction(
	const ParameterVectorDirection & gradient,
	const ParameterVector & param)
{
    the_direction_ = gradient;
    the_direction_ *= -options_.step_size_;
}


/// Print out options
void SteepestDescent::print_options(std::ostream& o) const
{
    o << options_;
}


/*
 * SteepestDescent::options functions
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 */

/**
 * Print options to stream
 */
std::ostream& operator<< (std::ostream& o, const SteepestDescent::options& opt)
{
    o << "step_size_ = " << opt.step_size_ << "\n";
    return o;
}

