/**
 * @file LinearMeanObjective.hpp
 * LinearMeanObjective Class header
 *
 */

#ifndef LINEARMEANOBJECTIVE_HPP
#define LINEARMEANOBJECTIVE_HPP

#include "Objective.hpp"
#include "Voi.hpp"
#include <cmath>
#include "FastMeanDoseComputer.hpp"

/**
 * Class LinearMeanObjective implements an objective to maximize or maximize 
 * the mean dose to a VOI. 
 *
 * Supports: external solver interface
 */
class LinearMeanObjective : public Objective
{

public:
  
  // Constructor
  LinearMeanObjective(Voi*  the_voi,
		      unsigned int objNo,
		      bool minimize,
		      float weight);
  
  // destructor
  ~LinearMeanObjective();
  
  /// calculate objective
  double calculate_objective(const BixelVector & beam_weights,
			     DoseInfluenceMatrix & Dij,
			     bool use_voxel_sampling,
			     double &estimated_ssvo);
  
  /// calculate objective from dose vector
  double calculate_dose_objective(const DoseVector & the_dose);

  /// Find out how many voxels are in this objective
  unsigned int get_nVoxels() const;
  
  /// Print a description of the objective
  void printOn(std::ostream& o) const;

  /// initialize the objective
  void initialize(DoseInfluenceMatrix& Dij);


  //
  // specific functions for gradient descent
  //

  bool supports_gradient() const {return true;};
  
  double calculate_objective_and_gradient(const BixelVector & beam_weights,
										  DoseInfluenceMatrix & Dij,
										  BixelVectorDirection & gradient,
										  float gradient_multiplier,
										  bool use_voxel_sampling,
										  double &estimated_ssvo);

  //
  // specific functions for commercial solver interface
  //

  bool supports_external_solver() const {return true;};
  
  /// returns total number of constraints
  unsigned int get_nGeneric_constraints() const {return(0);};

  /// returns total number of auxiliary variables
  unsigned int get_nAuxiliary_variables() const {return(0);};

  /// extract data for commercial solver
  void add_to_optimization_data_set(GenericOptimizationData & data, 
				    DoseInfluenceMatrix & Dij) const;



private:

  // Default constructor not allowed
  LinearMeanObjective();

  /// Class to hold precalculated mean dose contributions
  FastMeanDoseComputer mean_dose_computer_;

  /// the corresponding VOI
  Voi *the_voi_;

  /// true if we minimize the mean dose, false if we maximize
  bool minimize_;

};

/*
 * Inline functions
 */


inline
unsigned int LinearMeanObjective::get_nVoxels() const
{
  return the_voi_->get_nVoxels();
}



#endif
