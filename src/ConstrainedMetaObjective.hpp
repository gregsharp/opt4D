/**
 * @file ConstrainedMetaObjective.hpp
 * ConstrainedMetaObjective Class header
 *
 */

#ifndef CONSTRAINEDMETAOBJECTIVE_HPP
#define CONSTRAINEDMETAOBJECTIVE_HPP

#include <iostream>
#include <cmath>
#include <map>

#include "BixelVector.hpp"
#include "DoseInfluenceMatrix.hpp"
#include "Objective.hpp"
#include "MetaObjective.hpp"
#include "Constraint.hpp"

/**
 * Class ConstrainedMetaObjective implements a class to set a hard constrained on the maximum value of a meta objective
 * This only works for uncertainty models with discrete error scenarios
 *
 */
class ConstrainedMetaObjective : public Constraint 
{

public:
	
/// constructor
	ConstrainedMetaObjective(unsigned int consNo,
							 MetaObjective* obj,
							 float bound,
							 float initial_penalty);

  /// destructor, resposible for deleting the associated objective
  ~ConstrainedMetaObjective();
  
  
  /// Print a description of the constraint
  void printOn(std::ostream& o) const;



	//
	// specific functions for augmented lagrangian approach
	//
	
	/// augmented lagrangian is supported if the objective supports gradient
	bool supports_augmented_lagrangian() const;
	
	/// calculate the augmented lagrangian function
	double calculate_aug_lagrangian(const BixelVector & beam_weights,
									DoseInfluenceMatrix & Dij,
									double & constraint,
									double & merit);
	
	/// calculate the augmented lagrangian function and its gradient
	double calculate_aug_lagrangian_and_gradient(const BixelVector & beam_weights,
												 DoseInfluenceMatrix & Dij,
												 BixelVectorDirection & gradient,
												 float gradient_multiplier);
	
	/// update lagrange parameter
	void update_lagrange_multipliers(const BixelVector & beam_weights,
									 DoseInfluenceMatrix & Dij);
	
	/// update penalty parameter
	void update_penalty(const BixelVector & beam_weights,
						DoseInfluenceMatrix & Dij, 
						float tol, 
						float multiplier);
	

private:

  // Default constructor not allowed
  ConstrainedMetaObjective();
	
	/// associated objective
	MetaObjective* the_obj_;
	
	/// bound for the objective value
	float bound_;
	
	/// penalty factor
	float penalty_;
	
	/// initial penalty factor
	float initial_penalty_;
	
	/// lagrange multiplier
	float lagrange_;
	
};


 

#endif
