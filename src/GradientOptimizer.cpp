/**
 * @file GradientOptimizer.cpp
 * GradientOptimizer Class implementation.
 *
 */

#include <fstream>
#include <iostream>
using std::cerr;
#include <vector>
using std::vector;
#include <queue>
using std::priority_queue;
#include <set>
using std::set;
#include <cmath>
#include <limits>
#include <algorithm>
using std::fill;

#include "GradientOptimizer.hpp"

/**
 * Constructor: initialize variables.
 *
 * @param thisplan The plan to be optimized.
 */
GradientOptimizer::GradientOptimizer(Plan* thisplan, DoseInfluenceMatrix* dij, ParameterVector* parameters, 
									 string log_file_name, Optimization::options options)
  : Optimization(thisplan,log_file_name,options),
	the_dij_(dij), the_parameters_(parameters), 
	scale_factor_(0), default_step_size_(0), optTypeInd_(0),
	parameter_direction_(parameters->get_nParameters()), gradient_(parameters->get_nParameters())
{
  cout << "creating gradient optimizer for " << options_.optimization_type_.back() << endl;

  // set optimization type
  if(options_.optimization_type_.size() > 1) {
	optTypeInd_ = 1;
  }
}



/**
 * reset the pointer to the parameter vector to be optimized
 */
void GradientOptimizer::reset(ParameterVector* parameters)
{
  // rest parameters
  the_parameters_ = parameters;

  // reset state
  reset();
}


/**
 * reset internal stuff
 */
void GradientOptimizer::reset()
{
  // make sure gradient vector has the right dimension
  parameter_direction_.resize(the_parameters_->get_nParameters());
  gradient_.resize(the_parameters_->get_nParameters());

  // set zero
  parameter_direction_.clear_values();
  gradient_.clear_values();

  // delete backup parameter vectors
  last_parameter_vector_.reset();
  scratch_parameter_vector_.reset();
}


/**
 * Initialize the optimization loop.
 * Determine value of scale factor and find initial parameter values.
 */
void GradientOptimizer::initialize() {
    using std::cout;
    using std::endl;

	/*
    if (scale_factor_ == 0) {
        scale_factor_ = calculate_scale_factor();

        // Adjust the scale factor by the multiplier
        default_step_size_ = scale_factor_ * options_.step_size_multiplier_;

        if (options_.step_size_multiplier_ != 1.0) {
            cout << "Default step size: " << default_step_size_ << endl;
        }
    } else {
        // Adjust the scale factor by the multiplier
        default_step_size_ = scale_factor_ * options_.step_size_multiplier_;
    }
	*/
}


/**
 * Calculate scale factor
 */
double GradientOptimizer::calculate_scale_factor() {
    using std::cout;

    double scale_factor = 1.0;

    // Optimization:
    cout << "Calculating optimization scaling factor..." << endl;

    // find K = first iteration, initial guess: do one run to determine the required normalization.
    // This is an approximation of the largest eigenvalue of the influence matrix.

    // Backup the intensities in the plan
	scratch_parameter_vector_ = the_parameters_->get_copy();

    // Set all beam intensity values to zero
    scratch_parameter_vector_->set_zero_intensities();
      
	if ( the_parameters_->getVectorType() == SQRRT_BIXEL_VECTOR_TYPE ) {
	  scratch_parameter_vector_->set_unit_intensities();
	}

    // Calculate gradient of zero dose
    const bool use_voxel_sampling = false;
    const bool use_scenario_sampling = false;
    const std::vector<unsigned int> nScenario_samples( the_plan_->get_nMeta_objectives(), 1 );
    objective_ = the_plan_->calculate_objective_and_gradient( *the_dij_, *scratch_parameter_vector_, gradient_, multi_objective_, last_ssvo_, 
															  use_voxel_sampling, use_scenario_sampling, nScenario_samples );

    cout << "Zero dose objective: " << objective_ << endl;

	// make step in gradient direction
    scratch_parameter_vector_->add_scaled_parameter_vector_direction(-1 * gradient_,float(1.0));
    if (options_.project_on_bound_constraints_) {
      scratch_parameter_vector_->makePossible();
    }

    // Calculate resulting dose
    DoseVector dose_vector(the_plan_->get_nVoxels());
	// the_plan_->calculate_dose(dose_vector,*scratch_parameter_vector_);
	BixelVector * bv = scratch_parameter_vector_->translateToBixelVector();
	the_dij_->dose_forward(dose_vector,*bv);
	delete bv;

    float target_dose = 0., prescribed_target_dose=0.;
    unsigned int nVoxels;

    // for all VOIs in target find average dose and average prescribed dose
    for (unsigned int iVOI=0; iVOI<the_plan_->get_nVois(); iVOI++) {
        const Voi *voi = the_plan_->get_voi(iVOI);
        if (voi->is_target()) {
            // Find number of voxels in target
            nVoxels = voi->get_nVoxels();

            // Add up the total prescribed dose to this VOI
            prescribed_target_dose += nVoxels * voi->get_desired_dose();

            // Add up the total actual dose to this VOI
            for (unsigned int iVoxel=0; iVoxel<nVoxels; iVoxel++) {
                target_dose += dose_vector.get_voxel_dose(voi->get_voxel(iVoxel));
            }

            cout << "Using voi " << iVOI << " (" << nVoxels
            << " voxels) for normalization."
            << endl;
        }
    }

    scale_factor = prescribed_target_dose / target_dose;
    cout << "Target dose: " << target_dose << ", Prescribed dose: " <<  prescribed_target_dose
        << ", Scale factor: " << scale_factor << "\n";

    return scale_factor;
}


/**
 * Optimize plan using selected algorithm.  Stops when
 * either a minimum improvement or a maximum number of iteration is reached.
 */
void GradientOptimizer::optimize() {
    // Restart the timer
    timer_.restart();

    // Save the options to the log
    log_.add_optimization_contents("options", options_);

    // Save the objectives to the log
    std::ostringstream s;
    the_plan_->print_objective_info(s);
    log_.add_optimization_contents("objectives", s.str());

	// optimize
	use_optimizer();

	// set the parameter vector in the plan class
	//the_plan_->set_parameter_vector(*the_parameters_->get_copy());
}


/**
 * Optimize plan through gradient descent.
 * Stops when either a minimum improvement or a maximum number of iteration is reached.
 * Uses sampling if options_.use_voxel_sampling_ is true.
 */
void GradientOptimizer::use_optimizer() {
    //
    //  Determine scale factor and initial step size
    //

    // Calculate scale factor if needed
    if (scale_factor_ == 0) {
        cout << "About to calculate scale factor" << endl;
        scale_factor_ = calculate_scale_factor();
        cout << "Scale factor: " << scale_factor_ << endl;
    }

    // Adjust the default step size by the multiplier
    default_step_size_ = scale_factor_ * options_.step_size_multiplier_;
	//    if (options_.step_size_multiplier_ != 1.0) {
        cout << "Default step size: " << default_step_size_ << endl;
		//    }

	//	scale_factor_ = 1;


    // Verify that nScenarios_per_step is valid.
    if (options_.nScenario_samples_per_step_.size() != the_plan_->get_nMeta_objectives()) {
        options_.nScenario_samples_per_step_.reserve(the_plan_->get_nMeta_objectives());

        if (options_.nScenario_samples_per_step_.size() == 0) {
            options_.nScenario_samples_per_step_.push_back(1);
        }

        // Copy from the first meta-objective to latter ones
        while (options_.nScenario_samples_per_step_.size() < the_plan_->get_nMeta_objectives()) {
            options_.nScenario_samples_per_step_.push_back(options_.nScenario_samples_per_step_[1]);
        }

        // Check if there were too many meta-objectives were supplied
        if (options_.nScenario_samples_per_step_.size() > the_plan_->get_nMeta_objectives()) {
            cout << "WARNING: too many 'I' parameters on command line." << endl;
            options_.nScenario_samples_per_step_.resize(the_plan_->get_nMeta_objectives());
        }
    }


    // start the iteration with the default step size
    double step_size = options_.step_size_multiplier_;//default_step_size_;//options_.step_size_multiplier_;

    // Count each time the step size is reduced
    int step_reduction_counter = 0;

    //
    // Create optimizer object
    //

    unsigned int nParameters = the_parameters_->get_nParameters();

    // Holder for optimizer object
    std::auto_ptr<Optimizer> the_optimizer;

    // Create object
    switch ( options_.optimization_type_[optTypeInd_]) {
        case STEEPEST_DESCENT: {
            SteepestDescent::options options;
            options.step_size_ = default_step_size_;
            the_optimizer.reset(new SteepestDescent(options, nParameters));
        }
        break;
        case LBFGS: {
            // Make sure strong wolfe conditions are used
		    options_.use_strong_wolfe_ = true;

            // Make sure that voxel sampling is turned off
            if (options_.use_voxel_sampling_) {
                throw std::runtime_error("Stochastic optimization cannot be used with L-BFGS.");
            }

            //step_size *= scale_factor_;
            Lbfgs::options options = options_.lbfgs_options_;
            the_optimizer.reset(new Lbfgs(options, nParameters));
        }
        break;
        case DELTA_BAR_DELTA: {
            DeltaBarDelta::options options = options_.delta_bar_delta_options_;
            options.step_size_ = scale_factor_;
            the_optimizer.reset(new DeltaBarDelta(options, nParameters));
        }
        break;
        case CONJUGATE_GRADIENT: {
            // Make sure that optimal step size is turned on
            options_.use_optimal_steps_ = true;

            // Make sure that voxel sampling is turned off
            if (options_.use_voxel_sampling_) {
                throw std::runtime_error("Stochastic optimization cannot be used with conjugate gradient.");
            }

            step_size *= scale_factor_;
            the_optimizer.reset(new ConjugateGradient(nParameters));
        }
        break;
        default:
		  cout << "optimization type: " << options_.optimization_type_[optTypeInd_] << endl;
		  throw(std::logic_error( "GradientOptimizer.use_optimizer() does not support that optimization type."));
    }


    //
    // Create event queues and fill with initial events
    //

    priority_queue<Optimization::event> step_queue;
    priority_queue<Optimization::event> time_queue;
    vector<OptimizationEventType> final_actions;
    set<OptimizationEventType> current_events;

    // Add stopping conditions
    if (options_.max_steps_ >= 0) {
        step_queue.push(Optimization::event(STOP, options_.max_steps_));
    }

    if (options_.max_time_ >= 0) {
        time_queue.push(Optimization::event(STOP, options_.max_time_));
    }

    if (options_.stopping_fraction_ > 0) {
        // Add event to start using stopping fraction (ensuring at least
        // min_steps steps are done
        if (options_.min_steps_ > 2) {
            time_queue.push(Optimization::event(ACTIVATE_STOP_FRACT, options_.min_steps_));
        } else {
            time_queue.push(Optimization::event(ACTIVATE_STOP_FRACT, 2));
        }
    }

    // Add extra step events
    for (unsigned int i = 0; i < options_.step_events_.size(); i++) {
        step_queue.push(options_.step_events_[i]);
    }

    // Add extra time events
    for (unsigned int i = 0; i < options_.time_events_.size(); i++) {
        time_queue.push(options_.time_events_[i]);
    }

    // Add final events
    for (unsigned int i = 0; i < options_.final_events_.size(); i++) {
        final_actions.push_back(options_.final_events_[i]);
    }

    // Add event to update sampling if appropriate
    if (options_.use_voxel_sampling_) {
        if (options_.use_adaptive_sampling_) {
            Optimization::event tmp(UPDATE_SAMPLING, 0, options_.as_update_period_);
            step_queue.push(tmp);
        }
    }

    //
    // Inizialize adaptive sampling if needed
    //
    if (options_.use_voxel_sampling_ && options_.use_adaptive_sampling_) {
        init_adaptive_sampling();
    }

    //
    // Initialize State variables for optimization
    //

    // Step counter (increments when the beam weights are changed)
    int stepNo = 0;
    //if ( optTypeInd_ == 0 ) {
        totStepNo_ = 0;
		//}

    // Time at start of the curent step (> 0 on step 1)
    double step_start_time = 0;

    // True if the fractional improvement stopping criteria is active
    bool stop_fraction_active = false;

    // True if we are finished
    bool stopped = false;

    // True if we should calculate adaptive sampling on the current step
    bool update_sampling_now = false;

    // True if we should calculate the true objective on this step
    bool calculate_true_objective_now = false;

    // True if we should estimate the true objective on this step
    bool estimate_true_objective_now = false;

    // The objective value on the previous step (invalid if stepNo = 0)
    double previous_objective = 0;

    unsigned int nDigits = 0;
    unsigned int tmpMaxSteps = options_.max_steps_;
    while ( tmpMaxSteps > 0 ) {
        tmpMaxSteps /= 10;
        nDigits++;
    }
    char prefixFormat[6];
    sprintf( prefixFormat, "%%0%ud_", nDigits );

    //
    // Loop over each step
    //
    if ( optTypeInd_ == 0 ) {
        timer_.restart();
    }

    while (!stopped) {

        // Record step start time
        step_start_time = timer_.get_time();

        //
        // Get current events from queues
        // ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        //

        // Ensure that all previous events have been used
        if (!current_events.empty()) {
            throw std::logic_error("Current events not empty.");
        }

        // Get current actions from step_queue
        harvest_events(step_queue, totStepNo_, current_events);

        // Get current actions from time_queue
        harvest_events(time_queue, step_start_time, current_events);

        // Check if the queues are empty
        if (time_queue.empty() && step_queue.empty() && current_events.empty()) {
            // The queues are empty so we need to stop to prevent an
            // infinite loop
            cout << "Empty event queues found." << endl;
            stopped = true;
        }

        // Print out debugging info
        if (options_.verbosity_ == 3) {
            // Print out top event in each queue if needed
            cout << "Start of step " << totStepNo_ << ", top time event: " << time_queue.top()
                    << ", top step event: " << step_queue.top() << "\n";

            // Print out current events
            cout << "Current events:" << current_events << "\n";
        }

        //
        // Process beginning of the step events
        // ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        //
        bool skip_updating_parameters = false;

        // Find out if we should calculate adaptive sampling on the current step
        if (current_events.count(UPDATE_SAMPLING) > 0)  {
            update_sampling_now = options_.use_adaptive_sampling_;
            current_events.erase(UPDATE_SAMPLING);
        } else {
            update_sampling_now = false;
        }

        // Check for an explicit STOP event
        if (current_events.count(STOP) > 0) {
            // Explicit stop found skip subsequent optimizers
            optTypeInd_ = options_.optimization_type_.size();
            stopped = true;
            current_events.erase(STOP);
        }

        // Check for an explicit SWITCH_OPTIMIZER event
        if (current_events.count(SWITCH_OPTIMIZER) > 0) {
            // Explicit switch optimizer instruction found
            stopped = true;
            current_events.erase(SWITCH_OPTIMIZER);
        }

        bool seqApThisOp = false;
        // Check for an explicit SEQUENCE_APERTURES event
        if (current_events.count(SEQUENCE_APERTURES) > 0) {
            // Explicit sequence apertures instruction found
            call_aperture_sequencer();
            current_events.erase(SEQUENCE_APERTURES);
            skip_updating_parameters = true;
            seqApThisOp = true;
        }

        // Check if stopping fraction needs to be activated
        if (!stop_fraction_active && (current_events.count(ACTIVATE_STOP_FRACT) > 0)) {
            stop_fraction_active = true;
            current_events.erase(ACTIVATE_STOP_FRACT);
        }

        // Reduce the step size if needed
        if (current_events.count(DECREASE_STEP_SIZE) > 0) {
            // TODO (dualta#1#): Change handling of step size
            step_reduction_counter++;
            step_size = options_.step_size_multiplier_ * 1/step_reduction_counter;
            log_.add_step_attribute("step_size_multiplier", step_size);
            current_events.erase(DECREASE_STEP_SIZE);
        }

        // Increase the adaptive sampling fraction if needed
        if (current_events.count(INCREASE_AS_SAMPLING_FRACTION) > 0) {
            if (options_.as_min_sampling_fraction_ > 0) {
                options_.as_sampling_fraction_ +=
                    options_.as_min_sampling_fraction_;
            } else {
                options_.as_sampling_fraction_ += 0.01;
            }

            log_.add_step_attribute("as_sampling_fraction", options_.as_sampling_fraction_);
            current_events.erase(INCREASE_AS_SAMPLING_FRACTION);
        }

        // Increase the scenario samples per step if needed
        if (current_events.count(INCREASE_SCENARIO_SAMPLES) > 0) {
            for (unsigned int i = 0; i < options_.nScenario_samples_per_step_.size(); i++) {
                options_.nScenario_samples_per_step_[i]++;
            }
            log_.add_step_attribute("scenarios_per_step", options_.nScenario_samples_per_step_);
            current_events.erase(INCREASE_SCENARIO_SAMPLES);
        }


        // Set the parameter values to zero
        if (current_events.count(SET_ZERO_PARAMETERS) > 0) {
            the_parameters_->set_zero_intensities();
            skip_updating_parameters  = true;
            std::cout << "Cleared parameter values\n";
            current_events.erase(SET_ZERO_PARAMETERS);
        }

        // Set all of the parameter values to one
        if (current_events.count(SET_UNIT_PARAMETERS) > 0) {
            the_parameters_->set_unit_intensities();
            skip_updating_parameters  = true;
            std::cout << "Set default parameter values\n";
            current_events.erase(SET_UNIT_PARAMETERS);
        }

        //
        // Calculate the new beam weights
        // ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        //
        // This is not done on the first pass because the gradient is not
        // available yet and because we might want to process some events.
        //
 
		// backup parameter vector
		last_parameter_vector_ = the_parameters_->get_copy();

        if (stepNo > 0 && !skip_updating_parameters) {
            // Calculate the direction
            the_optimizer->calculate_direction(gradient_, *the_parameters_);

            // Calculate the directional derivative if requested
            if (current_events.count(CALCULATE_DIRECTIONAL_DERIVATIVE) > 0) {
                log_.add_step_attribute("directional_derivative", the_optimizer->get_direction().dot_product(gradient_));
                current_events.erase(CALCULATE_DIRECTIONAL_DERIVATIVE);
            }

            // Apply step to beam weights
            if (options_.use_optimal_steps_) {
                if (options_.use_voxel_sampling_) {
                    throw std::logic_error( "used optimal steps with stochastic optimization");
                }

                // Find optimal step size
                step_size = find_optimal_step(the_optimizer->get_direction(), step_size, options_.linesearch_steps_);

                // Add step to each beam
                make_step(the_optimizer->get_direction(), step_size);
            } else if (options_.use_strong_wolfe_) {
                if (options_.use_voxel_sampling_) {
                    throw std::logic_error( "used strong wolfe steps with stochastic optimization");
                }
                // Find step size satisfying the strong Wolfe conditions
                step_size = find_strong_wolfe_step(the_optimizer->get_direction(), 1.0);

                // Add step to each beam
                make_step(the_optimizer->get_direction(), step_size);
            } else if (options_.use_diminishing_step_sizes_) {
                // Add step to each beam
                make_step(the_optimizer->get_direction(), step_size * 100 / (stepNo + 99));
            }  else {
                // Constant step size
                // Add step to each beam
			    step_size = default_step_size_;
                make_step(the_optimizer->get_direction(), step_size);
            }

            // Project onto bound constraints (that's the default)
            if (options_.project_on_bound_constraints_) {
              the_parameters_->makePossible();
            }

            cout << "Step size: " << step_size << endl;

        } else if (skip_updating_parameters) {
            skip_updating_parameters = false;
        }

        // Store last objective
        previous_objective = objective_;

        //  update lagrange multipliers in this step
        if (current_events.count(UPDATE_LAGRANGE_MULTIPLIERS) > 0) {
		  the_plan_->update_lagrange_multipliers(*the_dij_,*the_parameters_);
		  current_events.erase(UPDATE_LAGRANGE_MULTIPLIERS);
		  
		  // reset the objective history for LBFGS optimizer
		  double obj = the_plan_->calculate_objective_and_gradient( *the_dij_, *last_parameter_vector_, gradient_, multi_objective_, last_ssvo_, 
		  															options_.use_voxel_sampling_, options_.use_scenario_sampling_,
		  															options_.nScenario_samples_per_step_ );
		  cout << "obj: " << obj << endl;
		  the_optimizer->reset_optimizer(gradient_);
		}

        //  update lagrange multipliers in this step
        if (current_events.count(UPDATE_PENALTY) > 0) {
		  the_plan_->update_penalty(*the_dij_,*the_parameters_,options_.constraint_tolerance_,options_.constraint_penalty_multiplier_);
		  current_events.erase(UPDATE_PENALTY);
		  
		  // reset the objective history for LBFGS optimizer
		  double obj = the_plan_->calculate_objective_and_gradient( *the_dij_, *last_parameter_vector_, gradient_, multi_objective_, last_ssvo_, 
																	options_.use_voxel_sampling_, options_.use_scenario_sampling_,
																	options_.nScenario_samples_per_step_ );

		  the_optimizer->reset_optimizer(gradient_);
		}		  

        //
        // Calculate next objective and gradient
        // ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        //

		// We only need to calculate or estimate the gradient
		objective_ = the_plan_->calculate_objective_and_gradient( *the_dij_, *the_parameters_, gradient_, multi_objective_, last_ssvo_, 
																  options_.use_voxel_sampling_, options_.use_scenario_sampling_,
																  options_.nScenario_samples_per_step_ );
	
        // Now correctly adjust sampling if needed
        if (options_.use_voxel_sampling_ && update_sampling_now) {
            adjust_sampling_rate();
        }

        //
        // Check stopping criteria
        // ^^^^^^^^^^^^^^^^^^^^^^^
        //

        // Check for infinite objective
        if (objective_ == std::numeric_limits<double>::infinity()) {
            for (unsigned int iObj = 0; iObj < multi_objective_.size(); iObj++) {
                cout << "Obj " << iObj << "=" << multi_objective_[iObj] << "\n";
            }
            cout << "Reached infinite objective, optimization stopped" << endl;
            stopped = true;
        }

        // Check if stopping fraction is satisfied
        if (stepNo>0 && stop_fraction_active && ((previous_objective-objective_)
                < (options_.stopping_fraction_*objective_))) {
            // Stopping fraction reached
            stopped = true;
        }

        // Check if there was exactly zero progress
        if (stepNo > 1 && (previous_objective == objective_)) {
            // Zero change in objective
            stopped = true;
        }

        //
        // Process end-of-step events
        // ^^^^^^^^^^^^^^^^^^^^^^^^^^
        //

        // Read in final actions if we're done
        if (stopped) {
            // We're done, so activate the final actions
            //current_events.insert(final_actions.begin(), final_actions.end());

            // Print out debugging info
            if (options_.verbosity_ == 3) {
                cout << "Added final events: " << final_actions << "\n";
            }
        }

        // Find prefix for any written files
        // Add step number to prefix unless we're finished
        string temp_prefix = options_.out_file_prefix_;
        if (!stopped) {
            char c_num[50];
            sprintf(c_num, prefixFormat, totStepNo_);
            temp_prefix += c_num;
        }

        // Write out bwf files if needed
        if (current_events.count(WRITE_PARAMETER_FILES) > 0) {
            string s = temp_prefix + string("beam");
            the_plan_->write_parameter_files(s);
            log_.add_step_attribute("bwf_file_root", s);
            current_events.erase(WRITE_PARAMETER_FILES);
        }

        // Write out ct-dose files if needed (and dose and dvh files as a side
        // effect)
        if (current_events.count(WRITE_CT_DOSE) > 0) {
            the_plan_->write_dose(temp_prefix, true);
            log_.add_step_attribute("ct_dose_file_name", temp_prefix + "CT_dose.dat");
            log_.add_step_attribute("dose_file_name", temp_prefix + "dose.dat");
            log_.add_step_attribute("dvh_file_name", temp_prefix + "DVH.dat");
            current_events.erase(WRITE_DOSE);
            current_events.erase(WRITE_CT_DOSE);
            current_events.erase(WRITE_DVH);
        }

        // Write out dose files if needed (and dvh files as a side effect)
        if (current_events.count(WRITE_DOSE) > 0) {
            the_plan_->write_dose(temp_prefix, false);
            log_.add_step_attribute("dose_file_name", temp_prefix + "dose.dat");
            log_.add_step_attribute("dvh_file_name", temp_prefix + "DVH.dat");
            current_events.erase(WRITE_DOSE);
            current_events.erase(WRITE_DVH);
        }

        // Write out dvh files if needed
        if (current_events.count(WRITE_DVH) > 0) {
            string s = temp_prefix + string("DVH.dat");
            the_plan_->write_DVH(s);
            log_.add_step_attribute("dvh_file_name", s);
            current_events.erase(WRITE_DVH);
        }

        // Write out beam dose files if needed
        if (current_events.count(WRITE_BEAM_DOSE) > 0) {
            the_plan_->write_beam_dose(temp_prefix);
            log_.add_step_attribute("beam_dose_file_prefix", temp_prefix);
            current_events.erase(WRITE_BEAM_DOSE);
        }

        // Write out scenario dose files if needed
        if (current_events.count(WRITE_SCENARIO_DOSE) > 0) {
            the_plan_->write_scenario_dose(temp_prefix);
            log_.add_step_attribute("scenario_dose_file_prefix", temp_prefix);
            current_events.erase(WRITE_SCENARIO_DOSE);
        }

        // Write out expected dose files if needed
        if (current_events.count(WRITE_EXPECTED_DOSE) > 0) {
            the_plan_->write_expected_dose(temp_prefix, options_.expected_dose_nScenarios_);
            log_.add_step_attribute("expected_dose_file_prefix", temp_prefix);
            current_events.erase(WRITE_EXPECTED_DOSE);
        }

        // Write out auxiliary dose files if needed
        if (current_events.count(WRITE_AUX_DOSE) > 0) {
            the_plan_->write_auxiliary_dose(temp_prefix);
            log_.add_step_attribute("auxiliary_dose_file_prefix", temp_prefix);
            current_events.erase(WRITE_AUX_DOSE);
        }

        // Write out auxiliary dose files if needed
        if (current_events.count(WRITE_INST_DOSE) > 0) {
            the_plan_->write_instance_dose(temp_prefix);
            log_.add_step_attribute("instance_dose_file_prefix", temp_prefix);
            current_events.erase(WRITE_INST_DOSE);
        }

        // Write out variance files if needed
        if (current_events.count(WRITE_VARIANCE) > 0) {
            the_plan_->write_variance(temp_prefix);
            log_.add_step_attribute("variance_file_prefix", temp_prefix);
            current_events.erase(WRITE_VARIANCE);
        }

        // Write out objective value samples needed
        if (current_events.count(WRITE_OBJ_SAMPLES) > 0) {
            the_plan_->write_objective_samples(temp_prefix);
            log_.add_step_attribute("obj_samples_file_prefix", temp_prefix);
            current_events.erase(WRITE_OBJ_SAMPLES);
        }

        // Write out dose samples needed
        if (current_events.count(WRITE_DOSE_SAMPLES) > 0) {
            // Vector to hold dose
            DoseVector sample_dose(the_plan_->get_nVoxels());

            for ( unsigned int i = 0; i < options_.write_dose_samples_nDoses_; i++ ) {
                the_plan_->calculate_random_scenario_dose(sample_dose);

                string dose_sample_file_name;
                char c_num[50];
                sprintf(c_num, "%04du_", i);
                dose_sample_file_name = temp_prefix + "dose_sample_" + c_num + "dose.dat";

                sample_dose.write_dose( dose_sample_file_name);
                log_.add_step_contents( "dose_sample_file", dose_sample_file_name );
            }

            current_events.erase(WRITE_DOSE_SAMPLES);
        }

        // True if we should calculate the true objective on this step
        if (current_events.count(CALCULATE_TRUE_OBJECTIVE) > 0) {
            calculate_true_objective_now = true;
            current_events.erase(CALCULATE_TRUE_OBJECTIVE);
        } else {
            calculate_true_objective_now = false;
        }

        // True if we should estimate the true objective on this step
        if (current_events.count(ESTIMATE_TRUE_OBJECTIVE) > 0) {
            estimate_true_objective_now = true;
            current_events.erase(ESTIMATE_TRUE_OBJECTIVE);
        } else {
            estimate_true_objective_now = false;
        }




        //
        // Log results and finish step
        // ^^^^^^^^^^^^^^^^^^^^^^^^^^^
        //
        cout << "Completed iteration: " << totStepNo_ << " - objective function: " << objective_ << "   difference "
                << (previous_objective-objective_)/objective_*100 << endl;
        cout << "Elapsed time: " << step_start_time << "\n";

        // Log results
        if (options_.use_voxel_sampling_) {
            log_.add_step_attribute("est_obj",objective_);
            log_.add_step_attribute("est_multi_obj",multi_objective_);

            if (calculate_true_objective_now) {
                timer_.pause();
                vector<double> temp_mo, temp_ssvo;
                const vector<unsigned int> nScenario_samples(the_plan_->get_nMeta_objectives(), 1);
                double true_objective = the_plan_->calculate_objective( *the_dij_, *the_parameters_, temp_mo, temp_ssvo, false, false, nScenario_samples );
                cout << "True objective: " << true_objective << "\n";
                log_.add_step_attribute("obj", true_objective);
                log_.add_step_attribute("multi_obj", temp_mo);
                log_.add_step_attribute("multi_ssvo", temp_ssvo);
                timer_.resume();
            }

            if (options_.use_adaptive_sampling_) {
                log_.add_step_attribute("sampling_fraction", the_plan_->get_objective_sampling_fractions());
            }
        } else if (the_plan_->get_nConstraints() > 0) {
		    double merit, objective, constraint, lagrangian;
			vector<double> temp_multi_obj, temp_multi_cons;
		    merit = the_plan_->calculate_merit_function(*the_dij_, *the_parameters_, lagrangian, objective, temp_multi_obj,
														constraint, temp_multi_cons);
			log_.add_step_attribute("obj", objective);
			log_.add_step_attribute("cons", constraint);
			log_.add_step_attribute("merit", merit);
			log_.add_step_attribute("lag", lagrangian);
			log_.add_step_attribute("multi_obj", temp_multi_obj);
			log_.add_step_attribute("multi_cons", temp_multi_cons);
		} else {
            // Not using stochastic optimization
            log_.add_step_attribute("obj", objective_);
            log_.add_step_attribute("multi_obj", multi_objective_);
        }

        if (estimate_true_objective_now) {
            timer_.pause();
            std::vector<double> estimated_true_multi_objective;
            std::vector<double> estimated_true_ssvo;
            std::vector<double> estimated_variance;
            double estimated_true_objective = the_plan_->estimate_true_objective( *the_dij_, *the_parameters_, 
                    estimated_true_multi_objective,
                    estimated_true_ssvo, estimated_variance,
                    options_.use_voxel_sampling_ & options_.estimate_sample_voxels_,
                    options_.use_voxel_sampling_ & options_.estimate_sample_voxels_,
                    options_.estimate_max_samples_, options_.estimate_min_samples_, options_.estimate_max_uncertainty_ );

            cout << "Estimated true objective: " << estimated_true_objective << "\n";
            cout << "Estimated true multi objective: " << estimated_true_multi_objective << "\n";
            log_.add_step_attribute("est_true_obj", estimated_true_objective);
            log_.add_step_attribute("est_true_multi_obj", estimated_true_multi_objective);
            log_.add_step_attribute("est_true_multi_ssvo", estimated_true_ssvo);
            log_.add_step_attribute("est_true_multi_var", estimated_variance);
            timer_.resume();
        }

        // Finish off log for step
        log_.finish_step(step_start_time);

        // increment step counter
        stepNo ++;
        totStepNo_ ++;
    }

    if (timer_.get_ignored_time() != 0)
        cout << "Ignored time: " << timer_.get_ignored_time() << "s\n";
}



/// Clear the direction of all beamlets
void GradientOptimizer::clear_direction() {
    // start with zero value for all parameters in all instances
    parameter_direction_.clear_values();
}


/**
 * Applies step in descent direction to plan
 *
 * @param direction The direction in which the step should be applied
 * @param step_size The size of the step to be applied
 */
void GradientOptimizer::make_step( const ParameterVectorDirection & direction,  double step_size ) {
    // Add step to each beam
    the_parameters_->add_scaled_parameter_vector_direction(direction, step_size);
}

/**
 * Applies step in descent direction to plan
 *
 * @param direction The direction in which the step should be applied
 * @param step_size The size of the step to be applied
 */
void GradientOptimizer::apply_step( const ParameterVectorDirection & direction,  double step_size ) {
    // Add step to each beam
    the_plan_->add_intensity_step(direction, step_size);

    // Project onto bound constraints (that's the default)
    if (options_.project_on_bound_constraints_) {
      the_plan_->project_on_parameter_bounds();
    }
}


/**
 * Test step in descent direction, then reset intensities.
 * Note that the dose, DVH, etc are not invalid.
 *
 * @param direction The direction in which the step should be applied
 * @param step_size The size of the step to be applied
 */
double GradientOptimizer::test_step( const ParameterVectorDirection & direction,  double step_size ) {
    scratch_parameter_vector_ = the_parameters_->get_copy();
    scratch_parameter_vector_->add_scaled_parameter_vector_direction( direction, step_size );
    if(options_.project_on_bound_constraints_) {
      scratch_parameter_vector_->makePossible();
    }
    vector<double> temp_mo;
    vector<double> temp_ssvo;
    const vector<unsigned int> nScenario_samples( the_plan_->get_nMeta_objectives(), 1 );
    double obj = the_plan_->calculate_objective( *the_dij_, *scratch_parameter_vector_, temp_mo, temp_ssvo, false, false, nScenario_samples );
    return obj;
}


/**
 * Test the estimated step in descent direction, then reset intensities.
 * Note same as test_step() function but voxel sampling is used.
 *
 * @param direction The direction in which the step should be applied
 * @param step_size The size of the step to be applied
 */
double GradientOptimizer::test_estimated_step( const ParameterVectorDirection & direction,  double step_size ) {
    scratch_parameter_vector_ = the_parameters_->get_copy();
    scratch_parameter_vector_->add_scaled_parameter_vector_direction( direction, step_size );
    if(options_.project_on_bound_constraints_) {
      scratch_parameter_vector_->makePossible();
    }
    vector<double> temp_mo;
    vector<double> temp_ssvo;
    const vector<unsigned int> nScenario_samples( the_plan_->get_nMeta_objectives(), 1 );
    double obj = the_plan_->calculate_objective(*the_dij_, *scratch_parameter_vector_, temp_mo, temp_ssvo, true, false, nScenario_samples);
    return obj;
}


/**
 * Test step in descent direction, calculate objective and gradient.
 * Called from alternate_find_optimal_step()
 *
 * @param direction The direction in which the step should be applied
 * @param step_size The size of the step to be applied
 */
double GradientOptimizer::test_step_gradient( const ParameterVectorDirection & direction,  double step_size,
        vector<double> * temp_mo,  vector<double> * temp_ssvo,  ParameterVectorDirection * gradient,
        const vector<unsigned int> &nScenario_samples ) {
    scratch_parameter_vector_ = the_parameters_->get_copy();
    scratch_parameter_vector_->add_scaled_parameter_vector_direction( direction, step_size );
    if(options_.project_on_bound_constraints_) {
      scratch_parameter_vector_->makePossible();
    }

    double obj = the_plan_->calculate_objective_and_gradient( *the_dij_, *scratch_parameter_vector_, *gradient, 
															  *temp_mo, *temp_ssvo,
															  false, false, nScenario_samples );
    return obj;
}

/**
 * calculates objective and directional derivative after taking a test step
 * Called from find_strong_wolfe_step()
 */
double GradientOptimizer::test_wolfe_step( const ParameterVectorDirection & direction,  
									  double step_size, 
									  double & derivative) 
{
    scratch_parameter_vector_ = the_parameters_->get_copy();
    scratch_parameter_vector_->add_scaled_parameter_vector_direction( direction, step_size );

    if(options_.project_on_bound_constraints_) {
      scratch_parameter_vector_->makePossible();
    }

    ParameterVectorDirection gradient(the_parameters_->get_nParameters());
    gradient.clear_values();

    vector<double> temp_mo;

    double obj = the_plan_->calculate_objective_and_gradient( *the_dij_, *scratch_parameter_vector_, gradient, temp_mo );

    derivative = direction.dot_product(gradient) / sqrt(direction.dot_product(direction));

    return obj;
}

/**
 * Subroutine for find_strong_wolfe_step()
 */
double GradientOptimizer::strong_wolfe_zoom( const ParameterVectorDirection & direction, 
					const double cur_obj,
					const double cur_deriv,
					const double initial_best_obj,
					const double initial_best_step,
					const double initial_bound )
{
  double best_step = initial_best_step;
  double best_obj = initial_best_obj;
  double bound = initial_bound;

  double c1 = options_.wolfe_condition_c1_;
  double c2 = options_.wolfe_condition_c2_;

  unsigned int iStep = 0;

  while(iStep < options_.linesearch_steps_)
    {
      iStep++;

      double mid_deriv = 0;
      double mid_step = 0.5*(best_step+bound);
      double mid_obj = test_wolfe_step(direction, mid_step, mid_deriv);

      cout << mid_step << " " << mid_obj << " " << cur_obj << " " << mid_deriv << " " << cur_deriv<< endl;

      if( mid_obj > cur_obj+c1*mid_step*cur_deriv  ||  mid_obj > best_obj ) {
		bound = mid_step;
      }
      else {
		if( fabs(mid_deriv) <= -1*c2*cur_deriv ) {
		  return mid_step;
		}
		if( mid_deriv*(bound-best_step) >= 0 ) {
		  bound = best_step;
		}
		best_step = mid_step;
		best_obj = mid_obj;
      }
    }
  
  // maximum number of steps exceeded
  //cout << "Error in strong_wolfe_zoom()" << endl;
  //throw Optimization::exception("Error finding step size");
  cout << "WARNING: didn't find step size satisfying the wolfe conditions" << endl;

  // return step size that led to the smallest objective value
  return(best_step);
}


/**
 * Finds a step size that satisfies the strong Wolfe conditions
 *
 * @param direction The direction to search in
 * @param guess_step_size The step size to use as the initial guess
 * @param iterations Maximum numberof line search steps
 *
 * This implements the algorithm described in Nocedal&Wright, Numerical Optimization, 2006, p60-61
 */
double GradientOptimizer::find_strong_wolfe_step( const ParameterVectorDirection & direction,
											 const double guess_step_size)
{

  cout << "finding strong wolfe step size" << endl;

  double min_step = 0;
  double mid_step = guess_step_size;

  double cur_obj, min_obj, mid_obj, prev_obj;
  double cur_deriv, mid_deriv;

  double c1 = options_.wolfe_condition_c1_;
  double c2 = options_.wolfe_condition_c2_;

  unsigned int iStep = 0;

  // initialize current objective and directional derivative
  // pause timer since this could be made more efficient
  timer_.pause();
  cur_obj = test_wolfe_step(direction, 0, cur_deriv);  // this also sets cur_deriv
  min_obj = cur_obj;  
  timer_.resume();

  cout << "find_strong_wolfe_step: cur_obj=" << cur_obj << endl;

  // make sure that the proposed direction is a descent direction
  if(cur_deriv > 0){
    cout << "Error in find_strong_wolfe_step()" << endl;
    cout << "Search direction is not a descent direction" << endl;
    throw Optimization::exception("Error finding step size");
  }

  while(iStep <= options_.linesearch_steps_) 
    {
      iStep++;

	  // stop timer here for one dose calculation: with an optimized implementation the result of 
	  // this dose calculation could be stored to avoid a recalculation later
	  if(iStep==1) timer_.pause();
      mid_obj = test_wolfe_step(direction, mid_step, mid_deriv);  // this also sets mid_deriv
	  if(iStep==1) timer_.resume();

      // test sufficient decrease condition
      if( mid_obj > cur_obj+c1*mid_step*cur_deriv  ||  mid_obj > min_obj ) {
		cout << "Sufficient decrease not fulfilled" << endl;
		return strong_wolfe_zoom(direction, cur_obj, cur_deriv, min_obj, min_step, mid_step);
      }

      // test curvature condition
      if( fabs(mid_deriv) <= -c2*cur_deriv ) {
		return mid_step;
      }

      // test if derivative is positive
      if( mid_deriv >= 0 ) {
		cout << "Derivative positive" << endl;
		return strong_wolfe_zoom(direction, cur_obj, cur_deriv, mid_obj, mid_step, min_step);
      }
    
      // if none of the above conditions are fulfilled, increase the step size
      min_step = mid_step;
      min_obj = mid_obj;
      mid_step = 2.0*mid_step;
    }

  // no step size found that satifies strong wolfe conditions
  cout << "WARNING: No step size found that satifys the strong Wolfe conditions" << endl;

  // return largest stepsize investigated
  return min_step;
}



/**
 * Finds the minimizing step size in the given direction
 *
 * @param direction The direction to search in
 * @param guess_step_size The step size to use as the initial guess
 * @param iterations How many times to search for the optimal point
 */
double GradientOptimizer::find_optimal_step( const ParameterVectorDirection & direction, const double guess_step_size,
                                        const int iterations ) {
    if (options_.use_alternate_linesearch_) {
        return alternate_find_optimal_step(direction, guess_step_size, iterations);
    }

    double min_step, mid_step, max_step;
    double min_objective, mid_objective, max_objective;
    double temp_step, temp_objective;
    int calculations_left = iterations;

    // The algorithm here works by establishing three step sizes - min_step, mid_step, and max_step - with the
    // corresponding objective.  We will first find values such that mid_objective is below both min_objeceive and
    // max_objective.  This ensures that the optimal step size is between min_step and max_step.  Once established,
    // we guarantee that this holds at every step in the search.  We will then add points at the location of the
    // quadratic approximation of the minimum.  After finding each new point we adjust to ensure that the property
    // still holds.

    // Initialize minimum with current information
    min_step = 0;
    min_objective = test_step(direction, 0);

    temp_step = guess_step_size;
    temp_objective = test_step(direction, temp_step);
    --calculations_left;

    if (temp_objective > min_objective) {
        // The guess works as a max_step
        if (options_.verbosity_ >= 2)
            cout << "The guess works as a max_step " << temp_step << " " << temp_objective << "\n";
        max_step = temp_step;
        max_objective = temp_objective;

        // Now find mid_step
        mid_step = (min_step + max_step)/2;
        mid_objective = test_step(direction, mid_step);
        --calculations_left;

        // Test if mid step is small enough
        while (mid_objective >= min_objective && (calculations_left > 0)) {
            // Use max as mid
            max_step = mid_step;
            max_objective = mid_objective;

            // Now find new mid_step
            mid_step = (min_step + max_step)/2;
            mid_objective = test_step(direction, mid_step);
            --calculations_left;
        }
    } else {
        // Guess works as middle step, need to find max step
        if (options_.verbosity_ >= 2)
            cout << "The guess works as a mid_step " << temp_step << " " << temp_objective << "\n";
        mid_step = temp_step;
        mid_objective = temp_objective;

        max_step = mid_step * 2;
        max_objective = test_step(direction, max_step);
        --calculations_left;

        // Test if max_step is big enough
        while (mid_objective > max_objective && (calculations_left > 0)) {
            mid_step = max_step;
            mid_objective = max_objective;

            max_step = mid_step * 2;
            max_objective = test_step(direction, max_step);
            --calculations_left;
        }
    }

    if (calculations_left > 0) {
        calculations_left = iterations;
    }

    while (calculations_left > 0) {
        if (options_.verbosity_ >= 2)
            cout << "alphas - " << min_step << " < " << mid_step << " < " << max_step
            << " Objectives: " << min_objective << ", " << mid_objective << ", " << max_objective << "\n";

        // The following conditions are now true:
        //      min_step < mid_step < max_step
        //      min_objective > mid_objective
        //      max_objective > mid_objective
        assert(min_step < mid_step && mid_step < max_step);
        assert(mid_objective <= min_objective);
        assert(mid_objective <= max_objective);

        // Place temp_step at the minimum of the quadratic polynomial that goes through min_step, mid_step, and max_step
        // (from _Nonlinear Programming_ by Dimitry P. Bertsekas, pg. 725)
        temp_step = 0.5 *
            (   min_objective * (max_step * max_step - mid_step * mid_step)
                + mid_objective * (min_step * min_step - max_step * max_step)
                + max_objective * (mid_step * mid_step - min_step * min_step) )
                    / ( min_objective * (max_step - mid_step) + mid_objective * (min_step - max_step)
                       + max_objective * (mid_step - min_step) );

        if ((temp_step >= max_step) || (temp_step <= min_step) || (temp_step == mid_step)) {
            // The algorithm has broken down, so go with the best alpha found thus far.
            // This typically is caused by a rounding error in the above function when min_step is very close to either
            // max_step or min_step.
            break;
        }

        temp_objective = test_step(direction, temp_step);
        --calculations_left;

        if (temp_step > mid_step) {
            // Point added between mid and max
            if (temp_objective < mid_objective) {
                // Temp is the new mid
                min_step = mid_step;
                min_objective = mid_objective;

                mid_step = temp_step;
                mid_objective = temp_objective;
            } else {
                // Temp is the new max
                max_step = temp_step;
                max_objective = temp_objective;
            }
        } else {
            // Point added is between min and mid
            if (temp_objective < mid_objective)
            {
                // mid is the new max
                max_step = mid_step;
                max_objective = mid_objective;

                mid_step = temp_step;
                mid_objective = temp_objective;
            } else {
                // Temp is the new min
                min_step = temp_step;
                min_objective = temp_objective;
            }
        }
    }

    // Mid step is the optimal found
    if (options_.verbosity_ >= 3)
        cout << "Step size: " << mid_step << "\n";

    // Output debugging info
    if (options_.verbosity_ >= 3) {
        cout << "Alpha: ";
        for (int i = 0; i < 10; i++)
            cout << "\t" << (mid_step * i) / 5;
        cout << "\nF: ";
        for (int i = 0; i < 10; i++)
            cout << "\t" << test_step(direction, (mid_step * i) / 5);
        cout << endl;
    }

    return mid_step;
}


/**
 * Finds the minimizing step size with the current sampling
 *
 * @param guess_step_size The step size to use as the initial guess
 * @param iterations How many times to search for the optimal point
 */
double GradientOptimizer::find_estimated_optimal_step( const ParameterVectorDirection & direction,
    const double guess_step_size, const int iterations ) {
    double min_step, mid_step, max_step;
    double min_objective, mid_objective, max_objective;
    double temp_step, temp_objective;
    int calculations_left = 20;

    // The algorithm here works by establishing three step sizes - min_step,
    // mid_step, and max_step - with the corresponding objective.  We will
    // first find values such that mid_objective is below both min_objeceive and
    // max_objective.  This ensures that the optimal step size is between
    // min_step and max_step.  Once established, we guarantee that this holds at
    // every step in the search.  We will then add points at the location of the
    // quadratic approximation of the minimum.  After finding each new point we
    // adjust to ensure that the property still holds.

    // Initialize minimum with current information
    min_step = 0;
    min_objective = test_estimated_step(direction, 0);

    temp_step = guess_step_size;
    temp_objective = test_estimated_step(direction, temp_step);
    --calculations_left;

    if (temp_objective >= min_objective) {
        // The guess works as a max_step
        if (options_.verbosity_ >= 2)
            cout << "The guess works as a max_step "
            << temp_step << " " << temp_objective << "\n";
        max_step = temp_step;
        max_objective = temp_objective;

        // Now find mid_step
        mid_step = (min_step + max_step)/2;
        mid_objective = test_estimated_step(direction, mid_step);
        --calculations_left;

        // Test if mid step is small enough
        while (mid_objective >= min_objective && (calculations_left > 0)) {
            // Use max as mid
            max_step = mid_step;
            max_objective = mid_objective;

            // Now find new mid_step
            mid_step = (min_step + max_step)/2;
            mid_objective = test_estimated_step(direction, mid_step);
            --calculations_left;
        }
    } else {
        // Guess works as middle step, need to find max step
        if (options_.verbosity_ >= 2)
            cout << "The guess works as a mid_step " << temp_step << " " << temp_objective << "\n";
        mid_step = temp_step;
        mid_objective = temp_objective;

        max_step = mid_step * 2;
        max_objective = test_estimated_step(direction, max_step);
        --calculations_left;

        // Test if max_step is big enough
        while (mid_objective > max_objective && (calculations_left > 0)) {
            mid_step = max_step;
            mid_objective = max_objective;

            max_step = mid_step * 2;
            max_objective = test_estimated_step(direction, max_step);
            --calculations_left;
        }
    }

    if (calculations_left > 0) {
        calculations_left = iterations;
    }

    while (calculations_left > 0) {
        if (options_.verbosity_ >= 2)
            cout << "alphas - " << min_step << " < " << mid_step << " < " << max_step
                << " Objectives: " << min_objective << ", " << mid_objective << ", " << max_objective << "\n";

        // The following conditions are now true:
        // min_step < mid_step < max_step
        // min_objective > mid_objective
        // max_objective > mid_objective
        assert(min_step < mid_step && mid_step < max_step);
        assert(mid_objective <= min_objective);
        assert(mid_objective <= max_objective);

        // Place temp_step at the minimum of the quadratic polynomial that goes
        // through min_step, mid_step, and max_step
        // (from _Nonlinear Programming_ by Dimitry P. Bertsekas, pg. 725)
        temp_step = 0.5 * ( min_objective * (max_step * max_step - mid_step * mid_step)
                      + mid_objective * (min_step * min_step - max_step * max_step)
                      + max_objective * (mid_step * mid_step - min_step * min_step))
                    / (min_objective * (max_step - mid_step)
                        + mid_objective * (min_step - max_step) + max_objective * (mid_step - min_step));

        if ((temp_step >= max_step) || (temp_step <= min_step) || (temp_step == mid_step)) {
            // The algorithm has broken down, so go with the best alpha found
            // thus far.  This typically is a rounding error in the above
            // function when min_step is very close to either max_step or
            // min_step.
            break;
        }


        temp_objective = test_estimated_step(direction, temp_step);
        --calculations_left;

        if (temp_step > mid_step) {
            // Point added between mid and max
            if (temp_objective < mid_objective) {
                // Temp is the new mid
                min_step = mid_step;
                min_objective = mid_objective;

                mid_step = temp_step;
                mid_objective = temp_objective;
            } else {
                // Temp is the new max
                max_step = temp_step;
                max_objective = temp_objective;
            }
        } else {
            // Point added is between min and mid
            if (temp_objective < mid_objective) {
                // mid is the new max
                max_step = mid_step;
                max_objective = mid_objective;

                mid_step = temp_step;
                mid_objective = temp_objective;
            } else {
                // Temp is the new min
                min_step = temp_step;
                min_objective = temp_objective;
            }
        }
    }

    // Mid step is the optimal found
    if (options_.verbosity_ >= 2)
        cout << "Step size: " << mid_step << "\n";
    return mid_step;
}


/**
 * Finds the minimizing step size
 *
 * @param guess_step_size The step size to use as the initial guess
 * @param iterations How many times to search for the optimal point
 */
double GradientOptimizer::alternate_find_optimal_step( const ParameterVectorDirection & direction,
        const double guess_step_size, const int iterations ) {

    double min_step, max_step;
    double min_objective, max_objective;
    double min_slope, max_slope;
    double temp_step, temp_objective, temp_slope;
    int calculations_left = iterations;
    ParameterVectorDirection temp_gradient(the_parameters_->get_nParameters());
    vector<double> temp_mo, temp_ssvo;
    const vector<unsigned int> nScenario_samples(the_plan_->get_nMeta_objectives(), 1);

    // Initialize minimum with current information
    // Recalculates objective_ and gradient in order to make sure they are consistent with the later ones.
    min_step = 0;

    min_objective = test_step_gradient( direction,  0.0, &temp_mo, &temp_ssvo, &temp_gradient, nScenario_samples);
    min_slope = temp_gradient.dot_product(direction);

    min_objective = objective_;
    min_slope = gradient_.dot_product(direction);

    // Stop if we're already at an optimal point or if we're headed up hill
    if (objective_ == 0 || min_slope >= 0) {
        return 0;
    }

    // Take a guess at second point
    temp_step = guess_step_size;
    temp_objective = test_step_gradient(direction,  temp_step, &temp_mo, &temp_ssvo, &temp_gradient, nScenario_samples);
    temp_slope = temp_gradient.dot_product(direction);

    while (temp_objective < min_objective && temp_slope < 0) {
        // Test point was not far enough, so use it as new first point and try again to find second point
        min_step = temp_step;
        min_objective = temp_objective;
        min_slope = temp_slope;

        temp_step *= 2;
        temp_objective = test_step_gradient( direction,  temp_step, &temp_mo, &temp_ssvo, &temp_gradient,
                    nScenario_samples );
        temp_slope = temp_gradient.dot_product(direction);

        if (options_.verbosity_ >= 2)
            cout << "Finding max_step " << temp_step << " " << temp_objective << " " << temp_slope << "\n";
    }

    max_step = temp_step;
    max_objective = temp_objective;
    max_slope = temp_slope;

    if (options_.verbosity_ >= 2) {
        cout << "Inital min_step " << min_step << " " << min_objective << " " << min_slope << "\n";
        cout << "Inital max_step " << max_step << " " << max_objective << " " << max_slope << "\n";
    }

    assert(min_step < max_step);
    assert(min_slope < 0);
    assert((max_slope >= 0) | (max_objective >= min_objective));

    // We now have the conditions so we can guarantee the optimal point is beetween min_step and max_step.
    // We'll use a cubic polynomial fit to find new points to test.

    while (calculations_left > 0) {
        --calculations_left;

        // Calculate new step to test
        double z = (3  * ((min_objective - max_objective) / (max_step - min_step))) + min_slope + max_slope;
        double w = sqrt(z*z - min_slope * max_slope);
        temp_step = max_step - ((max_step - min_step) * (max_slope + w - z) / (max_slope - min_slope + 2*w));

        // Calculate the corresponding objective and slope
        temp_objective = test_step_gradient( direction,  temp_step, &temp_mo, &temp_ssvo, &temp_gradient,
                    nScenario_samples );
        temp_slope = temp_gradient.dot_product(direction);

        if (temp_slope >= 0 || temp_objective >= min_objective) {
            if (options_.verbosity_ >= 2)
                cout << "New max step " << temp_step << " " << temp_objective << " " << temp_slope << "\n";
            max_step = temp_step;
            max_objective = temp_objective;
            max_slope = temp_slope;
        } else if (temp_slope < 0 && temp_objective < min_objective) {
            if (options_.verbosity_ >= 2)
                cout << "New max step " << temp_step << " " << temp_objective << " " << temp_slope << "\n";
            min_step = temp_step;
            min_objective = temp_objective;
            min_slope = temp_slope;
        } else {
            if (options_.verbosity_ >= 2)
                cout << "Converged " << temp_step << " " << temp_objective << " " << temp_slope << "\n";
            break;
        }

    }

    temp_objective = test_step(direction, temp_step);

    // Max step is the optimal found
    if (options_.verbosity_ >= 1)
        cout << "Step size: " << max_step << "\n";

    return(max_step);
}




