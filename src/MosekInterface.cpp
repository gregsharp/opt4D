/**
 * @file: MosekInterface.cpp
 * MosekInterface Class implementation.
 *
 */

#include "MosekInterface.hpp"

#ifndef HAVE_MOSEK

// Mosek not available

MosekInterface::MosekInterface(Plan* the_plan)
    : the_plan_(the_plan)
{
}

MosekInterface::~MosekInterface()
{
}

void MosekInterface::optimize()
{
    cout << "Function call_mosek() was called but macro HAVE_MOSEK was not defined at compilation" << endl;
    cout << "Not optimizing the plan" << endl;
}

#else

// Mosek available

// printstr function 
static void MSKAPI printstr(void *handle, char str[]) { 
  printf("%s",str); 
} 

/*
 * Constructor
 */
MosekInterface::MosekInterface(Plan* the_plan)
    : the_plan_(the_plan)
    , task_(NULL) 
    , env_(NULL)
    , nConstraints_(0)
    , nVar_(0)
    , data_()
    , status_(MSK_RES_OK)
{
    // Initialize the environment
    status_ = MSK_makeenv(&env_, NULL, NULL, NULL, NULL); /* Initialize the environment */  
    if ( status_==MSK_RES_OK ) 
	MSK_initenv (env_); 

    // Create a task object linked to the environment env. 
    if ( status_==MSK_RES_OK ) 
	status_ = MSK_maketask (env_, 0,0, &task_); 

    // extract the coefficients for objective and constraints from the plan
    extract_mosek_data();
}

MosekInterface::~MosekInterface()
{
  cout << "calling destructor of MosekInterface" << endl;
}


void MosekInterface::extract_mosek_data()
{
    cout << "extracting data for Mosek interface" << endl;

    // get total number of constraints
    nConstraints_ = the_plan_->get_nGeneric_constraints();

    cout << "Total number of constraints: " << nConstraints_ << endl;

    // get total number of auxiliary variables
    unsigned int nVarAux = the_plan_->get_nAuxiliary_variables();

    // get number of intrinsic variables
    unsigned int nVarInt = the_plan_->get_nBixels();

    // total number of variables
    nVar_ = nVarInt + nVarAux;

    cout << "Total number of variables: " << nVar_;
    cout << " (" << nVarInt << " intrinsic, " << nVarAux << " auxiliary)" << endl;

    // initialize the data structure
    data_.initialize(nVarInt,nVarAux,nConstraints_);

    // extract data from the plan
    the_plan_->get_generic_optimization_data(data_);

    cout << "Total number of nonzero elements in constraint matrix: " << data_.nNonZeros_ << endl;
}


MSKboundkeye MosekInterface::convert_bound_type(BoundType type)
{
  switch(type)
    {
    case FREE:
      return MSK_BK_FR;
    case LOWERBOUND:
      return MSK_BK_LO;
    case UPPERBOUND:
      return MSK_BK_UP;
    case TWOSIDED:
      return MSK_BK_RA;
    case EQUALITY:
      return MSK_BK_FX;
    default:
      cout << "Unknown bound type detected" << endl;
      throw(std::runtime_error("Error when preparing data for Mosek interface"));
    }
}

void MosekInterface::pass_data_to_mosek()
{
  cout << "passing data to mosek..." << endl;

  /* Create the optimization task. */  
  if ( status_==MSK_RES_OK ) {
    status_ = MSK_maketask(env_,nConstraints_,nVar_,&task_); 
  }

  if ( status_==MSK_RES_OK ) { 
    // Direct the environment log stream to the 'printstr' function. 
    MSK_linkfunctoenvstream(env_,MSK_STREAM_LOG,NULL,printstr); 
  } 
  if ( status_==MSK_RES_OK ) { 
    // Direct the task log stream to the 'printstr' function. 
    MSK_linkfunctotaskstream(task_,MSK_STREAM_LOG,NULL,printstr); 
  } 

  // set problem size
  if (status_ == MSK_RES_OK) 
    status_ = MSK_putmaxnumvar(task_,nVar_); 

  if (status_ == MSK_RES_OK) 
    status_ = MSK_putmaxnumcon(task_,nConstraints_); 

  if (status_ == MSK_RES_OK) 
    status_ = MSK_putmaxnumanz(task_,data_.nNonZeros_); 
 
  // Append empty constraints. The constraints will initially have no bounds. 
  if ( status_ == MSK_RES_OK ) 
    status_ = MSK_append(task_,MSK_ACC_CON,nConstraints_); 

  // Append variables. The variables will initially be fixed at zero (x=0). 
  if ( status_ == MSK_RES_OK ) 
    status_ = MSK_append(task_,MSK_ACC_VAR,nVar_); 

  for(unsigned int iVar=0; iVar<nVar_ && status_==MSK_RES_OK; iVar++)
    {
      // Set the objective function coefficient 
      if(status_ == MSK_RES_OK) 
	status_ = MSK_putcj(task_,iVar,data_.obj_coeffs_[iVar]); 

      // Set the bounds on variable 
      if(status_ == MSK_RES_OK) 
	status_ = MSK_putbound(task_, MSK_ACC_VAR, /* Put bounds on variables.*/  
			       iVar, /* Index of variable.*/  
			       convert_bound_type(data_.var_bound_type_[iVar]), /* Bound key.*/  
			       data_.lower_var_bound_[iVar], /* Numerical value of lower bound.*/  
			       data_.upper_var_bound_[iVar]); /* Numerical value of upper bound.*/ 
    }

  for(unsigned int iCons=0; iCons<nConstraints_ && status_==MSK_RES_OK; iCons++) 
    { 
      // Set the bounds on constraints. 
      if(status_ == MSK_RES_OK) 
	status_ = MSK_putbound(task_, MSK_ACC_CON, /* Put bounds on constraints.*/  
			       iCons, /* Index of constraint.*/  
			       convert_bound_type(data_.constraints_[iCons].type_), /* Bound key.*/  
			       data_.constraints_[iCons].lower_bound_, /* Numerical value of lower bound.*/  
			       data_.constraints_[iCons].upper_bound_); /* Numerical value of upper bound.*/  
      
      // Input constraint coefficients
      if(status_ == MSK_RES_OK) 
	status_ = MSK_putavec(task_, MSK_ACC_CON, /* Input row of A.*/  
			      iCons, /* Row index.*/  
			      data_.constraints_[iCons].nNonZeros_, /* Number of non-zeros in row i.*/  
			      data_.constraints_[iCons].index_, /* Pointer to column indexes of row i.*/  
			      data_.constraints_[iCons].value_); /* Pointer to Values of row i.*/ 
    }

  if (status_ != MSK_RES_OK) { 
    // In case of an error print error code and description. 
    char symname[MSK_MAX_STR_LEN]; 
    char desc[MSK_MAX_STR_LEN]; 
    
    printf("An error occurred while setting up the problem.\n"); 
    MSK_getcodedisc (status_, symname, desc); 
    printf("Error %s - '%s'\n",symname,desc); 
  } 
}

void MosekInterface::optimize()
{
  // create Mosek structures
  if(status_ == MSK_RES_OK) 
    pass_data_to_mosek();

  cout << "passing succeeded" << endl;

  if ( status_==MSK_RES_OK ) { 
    // Direct the task log stream to the 'printstr' function. 
    MSK_linkfunctotaskstream(task_,MSK_STREAM_LOG,NULL,printstr); 
  } 

  // Minimize objective function.
  if (status_ == MSK_RES_OK) {
    status_ = MSK_putobjsense(task_, MSK_OBJECTIVE_SENSE_MINIMIZE); 
  }

  // Run optimizer   
  status_ = MSK_optimize(task_); 

  // Print a summary containing information about the solution for debugging purposes. 
  MSK_solutionsummary (task_,MSK_STREAM_LOG); 
  
  if ( status_!=MSK_RES_OK ) {
    cout << "An error occured during optimization" << endl;
  }
  else {
    MSKsolstae solsta; // solution status
    double xx[nVar_];   // optimal solution
    
    MSK_getsolutionstatus (task_, MSK_SOL_BAS, NULL, &solsta); 
    
    switch(solsta) 
      { 
      case MSK_SOL_STA_OPTIMAL: 
      case MSK_SOL_STA_NEAR_OPTIMAL: 
	MSK_getsolutionslice(task_, 
			     MSK_SOL_BAS, /* Request the basic solution. */  
			     MSK_SOL_ITEM_XX,/* Which part of solution. */  
			     0, /* Index of first variable. */  
			     nVar_, /* Index of last variable+1. */  
			     xx); 
	
	// set beam weights
	for(unsigned int i=0; i<the_plan_->get_nBixels(); i++) {
	  the_plan_->set_parameter_vector(i,(float)xx[i]);
	}
	
	break;
	
      case MSK_SOL_STA_DUAL_INFEAS_CER: 
      case MSK_SOL_STA_PRIM_INFEAS_CER: 
      case MSK_SOL_STA_NEAR_DUAL_INFEAS_CER: 
      case MSK_SOL_STA_NEAR_PRIM_INFEAS_CER: 
	cout << "Primal or dual infeasibility certificate found" << endl;
	break; 
	
      case MSK_SOL_STA_UNKNOWN: 
	cout << "The status of the solution could not be determined" << endl; 
	break; 
	
      default: 
	cout << "Other solution status." << endl; 
	break; 
      }
  }
}





#endif
