/**
 * @file: RangeSetupUncertaintyModel.cpp
 * RangeSetupUncertaintyModel Class implementation.
 *
 */

#include "RangeSetupUncertaintyModel.hpp"
#include <sstream>

const float EPSILON = 0.000001;


// ---------------------------------------------------------------------
// Range and Setup uncertainty
// ---------------------------------------------------------------------

/**
 * constructor
 */
RangeSetupUncertaintyModel::RangeSetupUncertaintyModel(
        DoseInfluenceMatrix &Dij, 
        const Geometry *the_geometry, 
        const DoseDeliveryModel::options *options)
    : ShiftBasedErrorModel(the_geometry),
    use_shift_correction_(options->setup_use_improved_sdca_),
    use_randomized_rounding_(options->setup_use_randomized_rounding_),
    use_linear_interpolation_(options->setup_use_linear_interpolation_),
    merge_shifts_(options->setup_merge_shifts_),
    setup_model_(
            the_geometry,
            options->gaussian_setup_options_),
    surface_normal_(options->setup_surface_normals_)
{
    // create range uncertainty model
    the_range_uncertainty_.reset( new GeneralAuxiliaryRangeModel(Dij,the_geometry,options));

    // create setup uncertainty model
    the_setup_uncertainty_ .reset( new SetupUncertaintyModel(the_geometry,options));
}


/**
 * generate sample scenario
 */
void RangeSetupUncertaintyModel::generate_random_scenario(
        RandomScenario &random_scenario) const
{
    // setup error
    the_setup_uncertainty_->generate_random_scenario(random_scenario);
    
    // generate range scenario
    the_range_uncertainty_->generate_random_auxiliaries(random_scenario);

    // set the auxiliary numbers in the RigidDoseShift objects
    for(vector<RigidDoseShift>::iterator iter = random_scenario.rigid_shifts_.begin();
	iter != random_scenario.rigid_shifts_.end();
	iter++) {
        (*iter).set_bixel_auxiliaryNos(random_scenario.auxiliaryNos_);
    }
}

/**
 * store a dose evaluation scenario
 */
bool RangeSetupUncertaintyModel::set_dose_evaluation_scenario(
        map<string,string> scenario_map)
{
    float x,y,z,dummy,prob;
    string sx,sy,sz;
    unsigned int beamNo;
    vector<float> rangeShifts(the_geometry_->get_nBeams(),0);
    vector<unsigned int> instanceNos(the_geometry_->get_nBeams(),0);
    RandomScenario scenario;

    // ----------------------- probability -----------------------------------

    std::istringstream iss_p(scenario_map["prob"]);

    if(!(iss_p >> dummy).fail()) {
	prob = dummy;
    }
    else {
	return false;
    }

    // ----------------------- setup -----------------------------------

    std::istringstream iss_s(scenario_map["setup"]);

    // read the string
    if((iss_s >> sx >> x >> sy >> y >> sz >> z).fail()) {
	return false;
    }
    if(!(sx=="x" && sy=="y" && sz=="z")) {
	return false;
    }

    // read shifts successfully 
    Shift_in_Patient_Coord shift = Shift_in_Patient_Coord(x,y,z);

    vector<Shift_in_Patient_Coord> shifts(1,shift);
    
    // set values in scenario
    the_setup_uncertainty_->set_shifts_in_scenario(scenario,shifts);


    // ----------------------- range -----------------------------------

    std::istringstream iss_r(scenario_map["range"]);

    // read the beam number
    while(!(iss_r >> beamNo).fail() 
	  && beamNo <= the_geometry_->get_nBeams()
	  && beamNo >= 1) {
	
	// read the range shift for this beam
	if(!(iss_r >> dummy).fail()) {
	    rangeShifts[beamNo-1] = dummy;
	}
	else {
	    return false;
	}
    }

    // convert range shifts to instanceNo
    for(unsigned int iBixel=0; iBixel<the_geometry_->get_nBixels(); iBixel++) {
	unsigned int iBeam = the_geometry_->get_beamNo(iBixel);
	instanceNos[iBeam] = the_range_uncertainty_->get_auxiliary_for_range_shift(iBixel,rangeShifts[iBeam]);
    }

    // resize auxiliary vector
    scenario.auxiliaryNos_.resize(the_geometry_->get_nBixels(),0);

    // there is a range shift for every beam, now create auxiliary vector
    for(unsigned int iBixel=0; iBixel<the_geometry_->get_nBixels(); iBixel++) {
	scenario.auxiliaryNos_[iBixel] = 
	    the_range_uncertainty_->get_AuxiliaryDijNo(instanceNos[the_geometry_->get_beamNo(iBixel)]);
    }

    // set the auxiliary numbers in the RigidDoseShift objects
    for(vector<RigidDoseShift>::iterator iter = scenario.rigid_shifts_.begin();
	iter != scenario.rigid_shifts_.end();
	iter++) {
        (*iter).set_bixel_auxiliaryNos(scenario.auxiliaryNos_);
    }

    // ---------------------- store scenario --------------------------

    dose_eval_scenarios_.push_back(scenario);
    dose_eval_pdf_.push_back(prob);
    nDose_eval_scenarios_++;

    // print info to screen	
    cout << "set dose evaluation scenario " << dose_eval_scenarios_.size()
        << endl;
    cout << "Probability: " << prob << endl;
    cout << "Patient shift: " << shift << endl;
    cout << "effective shift / voxel subscript shifts per beam:" << endl;

    for(unsigned int i=0; i<scenario.rigid_shifts_.size(); i++) {
	cout << "beam " << i << ": " << scenario.rigid_shifts_[i].shift_
            << " / " << scenario.rigid_shifts_[i].subscript_shift_ << endl;
    }

    cout << "Range shifts: " << endl;

    for(unsigned int iBeam = 0; iBeam<the_geometry_->get_nBeams(); iBeam++) {
	for(unsigned int iBixel = 0; iBixel<the_geometry_->get_nBixels(); iBixel++) {
	    if(the_geometry_->get_beamNo(iBixel) == iBeam) {
		cout << "beam " << iBeam << ": wanted " << rangeShifts[iBeam] 
		     << ", get " << the_range_uncertainty_->get_RangeShift(instanceNos[iBeam],iBixel)
		     << "(instance " << instanceNos[iBeam] << ")" << endl;
		break;
	    }
	}
    }

    return(true);
}


/**
 * calculate voxel dose for a scenario
float RangeSetupUncertaintyModel::calculate_voxel_dose(
    unsigned int voxelNo,	
    const BixelVector &bixel_grid,                  
    DoseInfluenceMatrix &dij,
    const RandomScenario &random_scenario) const
{
  assert(random_scenario.shifts_.size() == random_scenario.bixelNos_.size());
  assert(random_scenario.auxiliaryNos_.size() == bixel_grid.get_nBixels());

  float dose_buf = 0;
  Voxel_Subscripts voxelSub;
  
  // requires sorted Dij entries
  // (bixelNos must be sorted too, but is not checked here)
  dij.ensure_sorted();

  // loop over the different shifts
  for(unsigned int iShift=0;iShift<random_scenario.shifts_.size();iShift++) {

      unsigned int iBixel = 0;

      // don't shift voxel if not possible
      unsigned int shiftVoxelNo = voxelNo;

      // get subscripts of voxel
      voxelSub = the_geometry_->convert_to_Voxel_Subscripts(VoxelIndex(voxelNo));
      //add shift 
      voxelSub = voxelSub + random_scenario.subscript_shifts_[iShift];

      // check whether it's inside dose cube and not air
      if(the_geometry_->is_valid_voxel(voxelSub)) {
	  // shift voxel
	  shiftVoxelNo = the_geometry_->convert_to_VoxelIndex(voxelSub).index_;
      }

      // loop over the Dij entries
      if(random_scenario.all_shifts_use_all_bixels_) {
          DoseInfluenceMatrix::iterator iter = dij.begin_voxel(shiftVoxelNo);
          while(iter.get_voxelNo() == shiftVoxelNo) {
	      dose_buf += bixel_grid[iter.get_bixelNo()] * 
		  iter.get_influence(random_scenario.auxiliaryNos_[iter.get_bixelNo()]);
              iter++;
          }
      } else {
          DoseInfluenceMatrix::iterator iter = dij.begin_voxel(shiftVoxelNo);
          while(iter.get_voxelNo() == shiftVoxelNo) {
              // extract beamlets that correspond to this voxel shift
              while(random_scenario.bixelNos_[iShift][iBixel]<iter.get_bixelNo()) {
                  iBixel++;
              }		  
              if(iter.get_bixelNo() == random_scenario.bixelNos_[iShift][iBixel]) {
                  dose_buf += bixel_grid[iter.get_bixelNo()] * 
                      iter.get_influence(random_scenario.auxiliaryNos_[iter.get_bixelNo()]);
                  iBixel++;
              }
              if(iBixel >= random_scenario.bixelNos_[iShift].size()) { 
                  // found all contributing bixels
                  break;
              }
              iter++;
          }
      }
  }  
  
  return dose_buf;
}
 */


/**
 * calculate gradient contribution of the voxel for a scenario
void RangeSetupUncertaintyModel::calculate_voxel_dose_gradient(
        unsigned int voxelNo,	
        DoseInfluenceMatrix &dij,
        const RandomScenario &random_scenario,
        BixelVectorDirection &gradient,
        float gradient_multiplier) const
{
    assert(random_scenario.shifts_.size() == random_scenario.bixelNos_.size());

    Voxel_Subscripts voxelSub;
	
    // requires sorted Dij entries
    dij.ensure_sorted();

    // loop over the different shifts
    for(unsigned int iShift=0;iShift<random_scenario.shifts_.size();iShift++) {
	    
	unsigned int iBixel = 0;
	
	// don't shift voxel if not possible
	unsigned int shiftVoxelNo = voxelNo;
	
	// get subscripts of voxel
	voxelSub = the_geometry_->convert_to_Voxel_Subscripts(VoxelIndex(voxelNo));
	//add shift 
	voxelSub = voxelSub + random_scenario.subscript_shifts_[iShift];

	// check whether it's inside dose cube and not air
	if(the_geometry_->is_valid_voxel(voxelSub)) {
	    // shift voxel
	    shiftVoxelNo = the_geometry_->convert_to_VoxelIndex(voxelSub).index_;
	}
	    
	// loop over the Dij entries
        if(random_scenario.all_shifts_use_all_bixels_) {
            DoseInfluenceMatrix::iterator iter = dij.begin_voxel(shiftVoxelNo);
            while(iter.get_voxelNo() == shiftVoxelNo) {
                gradient[iter.get_bixelNo()] += gradient_multiplier *
                    iter.get_influence(random_scenario.auxiliaryNos_[
                            iter.get_bixelNo()]);
                iter++;
            }
        } else {
            DoseInfluenceMatrix::iterator iter = dij.begin_voxel(shiftVoxelNo);
            while(iter.get_voxelNo() == shiftVoxelNo) {
                // extract beamlets that correspond to this voxel shift
                while(random_scenario.bixelNos_[iShift][iBixel]<iter.get_bixelNo()) {
                    iBixel++;
                }		  
                if(iter.get_bixelNo() == random_scenario.bixelNos_[iShift][iBixel]) {
                    gradient[iter.get_bixelNo()] += gradient_multiplier *
                        iter.get_influence(random_scenario.auxiliaryNos_[
                                iter.get_bixelNo()]);
                    iBixel++;
                }
                if(iBixel >= random_scenario.bixelNos_[iShift].size()) { 
                    // found all contributing bixels
                    break;
                }
                iter++;
            }
        }
    }
}
 */


