/*
 *  DaoVector.cpp
 */

#include "DaoVector.hpp"
#include "KonradBixelVector.hpp"
#include "SquareRootBixelVector.hpp"
#include <iostream>
#include <fstream>
#include <iomanip>

const float APERTURE_DOSE_CUTOFF = 1E-10;


/**
 * Constructor
 * Creates an empty DAO vector with no apertures
 */
DaoVector::DaoVector(Geometry* geometry, DoseInfluenceMatrix* bixel_dij)
  : ParameterVector()
  , geometry_(geometry)
  , nBeams_(geometry->get_nBeams())
  , test_apertures_()
  , nApertures_(0)
  , apertures_()
  , aperture_dij_()
  , bixel_dij_(bixel_dij)
  , bixel_vector_(geometry->get_nBixels())
{
  cout << "DaoVector constructor called" << endl;

  // create test apertures for every beam: those are used to solve the pricing problems
  for(unsigned int iBeam=0; iBeam<geometry->get_nBeams(); iBeam++) {
	test_apertures_.push_back(Aperture(geometry,iBeam));
  }

  // reserve some memory for the aperture dij

  // total number of entries in the bixel Dij
  unsigned int nEntries = bixel_dij_->get_nEntries();

  // for now reserve the same amount of memory for the aperture dij
  aperture_dij_.initialize(bixel_dij_->get_nVoxels(), nEntries);

}

/**
 * Destructor
 */
//DaoVector::~DaoVector()
//{
//  delete bixel_vector_;
//}


/**
 * Translate the ParameterVector into a BixelVector
 * Warning calling function should free returned object.
 */
BixelVector* DaoVector::translateToBixelVector() const
{
	synchronize_bixel_vector();
	return new BixelVector(bixel_vector_);
}


/**
 * get a copy
 */
std::auto_ptr<ParameterVector> DaoVector::get_copy() const
{
  std::auto_ptr<ParameterVector> temp;
  temp.reset(new DaoVector(*this));
  return temp;
}

/**
 * get the aperture weights as a BixelVector
 */
std::auto_ptr<ParameterVector> DaoVector::get_aperture_BixelVector() const
{
  std::auto_ptr<ParameterVector> temp;
  temp.reset( new BixelVector(get_nApertures()) );
  temp->add_value(*this);
  return temp;
}


/**
 * get the aperture weights as a SquareRootBixelVector
 */
std::auto_ptr<ParameterVector> DaoVector::get_aperture_SquareRootBixelVector() const
{
  std::auto_ptr<ParameterVector> temp;
  temp.reset( new SquareRootBixelVector(0.0, get_nApertures()) );

  std::auto_ptr<ParameterVector> bv = get_aperture_BixelVector();
  temp->translateBixelVector(*(static_cast<BixelVector *>(bv.get())));

  return temp;
}


/**
 * Translate the gradient in terms of bixels to aperture weight gradients
 */
ParameterVectorDirection DaoVector::translateBixelGradient(BixelVectorDirection & bixel_gradient ) const
{

  ParameterVectorDirection aperture_gradient(nApertures_);

  for (unsigned int iA=0; iA<nApertures_; iA++) 
	{
	  aperture_gradient.set_value(iA, apertures_[iA].calculate_aperture_weight_gradient(bixel_gradient));
	}

  return aperture_gradient;
}

void DaoVector::synchronize(ParameterVector * aperture_weights)
{
  BixelVector * temp = aperture_weights->translateToBixelVector();

  // set the aperture weights in the parameter vector
  clear_values();
  add_value(*temp); 

  // update the bixel vector
  synchronize_bixel_vector();

  delete temp;
}


/**
 * determine the effective bixelvector from the current apertures
 */
void DaoVector::synchronize_bixel_vector() const
{
  // crear bixel vector
  bixel_vector_.clear_values();

  // add apertures
  for (unsigned int iA=0; iA<nApertures_; iA++) 
	{
	  apertures_[iA].add_aperture_to_bixel_vector(bixel_vector_, get_value(iA));
	}
}

/**
 * add an aperture to an aperture-Dij matrix
 * this will add entries to the Dij matrix, assuming that the aperture already exists with no entries
 */
void DaoVector::add_aperture_to_dij(unsigned int apNo)
{
  cout << "adding aperture to dij: ";

  // number of voxels
  unsigned int nVoxels = bixel_dij_->get_nVoxels();

  // create zero intensity bixel vector
  BixelVector temp_bixels = apertures_[apNo].get_unit_intensity_bixel_vector();

  // calculate dose
  DoseVector temp_dose = DoseVector(nVoxels);
  bixel_dij_->dose_forward(temp_dose,temp_bixels);

  temp_dose.write_dose("junk");

  // create sparse vector
  vector<unsigned int> voxelNos; voxelNos.reserve(nVoxels);
  vector<float> influence; influence.reserve(nVoxels);

  for (unsigned int iVoxel=0; iVoxel<nVoxels; iVoxel++) {
	if(temp_dose[iVoxel] >= APERTURE_DOSE_CUTOFF) {
	  voxelNos.push_back(iVoxel);
	  influence.push_back(temp_dose[iVoxel]);
	}
  }

  cout << "nEntries: " << voxelNos.size() << endl;

  // add dose values to the aperture Dij
  aperture_dij_.add_aperture(apNo, voxelNos, influence);
}


/**
 * find most promising aperture by solving the pricing problem
 * and add to the set of apertures
 */
 bool DaoVector::add_best_aperture(const BixelVectorDirection bixel_gradient)
 {
   float opt_price = 0;
   unsigned int opt_aperture = 0;

   for(unsigned int iBeam=0;iBeam<nBeams_;iBeam++)
	 {
	   // solve pricing problem for every beam
	   float price = test_apertures_[iBeam].solve_pricing_problem(bixel_gradient);

	   cout << "beam: " << iBeam << " price: " << price << endl;

	   if(price < opt_price) {
		 opt_price = price;
		 opt_aperture = iBeam;
	   }
	 }
   
   if(opt_price >= 0) {
	 cout << "No useful aperture found" << endl;
	 return false;
   }

   // now add aperture
   apertures_.push_back(test_apertures_[opt_aperture]);
   nApertures_++;
   apertures_[nApertures_-1].set_bixel_vector();

   // add aperture to dij
   add_aperture_to_dij(nApertures_-1);

   // add new weight variable to parameter vector
   this->push_back_parameter(1, 0, std::numeric_limits<float>::infinity(), true, 1);

   return true;
 }


void DaoVector::scale_intensities(float scale_factor)
{
  scale_values(scale_factor);
}

void DaoVector::set_zero_intensities()
{
  assert(nParams_ == value_.size());
  for ( unsigned int ip = 0; ip < value_.size(); ip++ ) {
	value_[ip] = 0;
  }
}


void DaoVector::set_unit_intensities()
{
  assert(nParams_ == value_.size());
  for ( unsigned int ip = 0; ip < value_.size(); ip++ ) {
	value_[ip] = 1;
  }
}

unsigned int DaoVector::get_nBixels () const 
{
  return bixel_vector_.get_nBixels();
}

unsigned int DaoVector::get_beamNo (const int &ip) const
{
  return apertures_[ip].get_beamNo();
}

ParameterVector * DaoVector::get_bixel_vector()
{
  synchronize_bixel_vector();
  return &bixel_vector_;
}

void DaoVector::translateBixelVector (BixelVector &bv)
{
  cout << "DaoVector::translateBixelVector not applicable\n" << endl;
  throw( std::runtime_error( "Function not implemented."));
}

void DaoVector::load_parameter_files (string parameters_file_root)
{
  cout << "DaoVector::load_parameter_files not implemented\n" << endl;
  throw( std::runtime_error( "Function not implemented."));
}

void DaoVector::load_parameter_file (string parameters_file_root, unsigned int beamNo)
{
  cout << "DaoVector::load_parameter_file not implemented\n" << endl;
  throw( std::runtime_error( "Function not implemented."));
}

void DaoVector::write_parameter_files (string parameters_file_root) const
{
  float eps = 0.000001;

  synchronize_bixel_vector();

  std::auto_ptr<ParameterVector> temp;

  // create Konrad bixel vector
  temp.reset(new KonradBixelVector(*bixel_dij_,geometry_,1,geometry_->get_nBeams()));
  temp->set_zero_intensities();
  temp->add_value(bixel_vector_);

  // write effective bixel weight files containing all apertures
  temp->write_parameter_files(parameters_file_root);

  // aperture weight file name
  string ap_file_name = parameters_file_root + "_apertureweights.dat";
  std::ofstream apfile(ap_file_name.c_str());
  cout << "Aperture weights: " << endl;

  // write a bixel vector file for every aperture
  for(unsigned int iAp=0; iAp<nApertures_; iAp++) {
	temp->set_zero_intensities(); 
	temp->add_value(apertures_[iAp].get_unit_intensity_bixel_vector());
	temp->scale_intensities(std::max(eps,get_value(iAp)));

	// write effective bixel weight file
    char cbeam[10];
    char cap[10];
    string bwf_file_name;
	unsigned int beamNo = apertures_[iAp].get_beamNo();
	sprintf(cbeam,"%d",(int)beamNo+1);
	sprintf(cap,"%d",(int)iAp+1);
	bwf_file_name = parameters_file_root + "_" + string(cbeam)  + "_ap_" + string(cap) + ".bwf";
	temp->write_parameter_file(bwf_file_name,beamNo);

	// aperture weight
	cout << "aperture " << iAp << " beam " << beamNo << " weight " << get_value(iAp) << endl; 
	apfile << iAp << " " << beamNo << " " << get_value(iAp) << "\n";
  }


  apfile.close();


}

void DaoVector::write_parameter_file (string parameters_file_root, const unsigned int beamNo) const
{
  cout << "DaoVector::write_parameter_file not implemented\n" << endl;
  throw( std::runtime_error( "Function not implemented."));
}

